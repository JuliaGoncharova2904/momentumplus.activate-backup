﻿using System;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Core;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Activate.Web.Models.People;
using System.Web;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Controllers
{
    [Authorise(Roles = RoleAuthHelper.Employees_Packages_General)]
    public abstract class EmployeeController : PackageController
    {
        private EmployeeCore EmployeeCore;
        #region IOC

        public EmployeeController(
            IAuditService auditService,
            IContactService contactService,
            IMasterListService masterListService,
            IPackageService packageService,
            IPeopleService peopleService,
            IPositionService positionService,
            IProjectService projectService,
            IRoleService roleService,
            ITemplateService templateService,
            IHandoverService handoverService,
            IUserService userService,
            IWorkplaceService workplaceService,
            EmployeeCore employeeCore,
            PackageCore packageCore
        )
            : base(
                  auditService,
                  contactService,
                  masterListService,
                  packageService,
                  positionService,
                  projectService,
                  templateService,
                  userService,
                  workplaceService,
                  packageCore
              )
        {
            EmployeeCore = employeeCore;
        }

        #endregion

        #region ACTIONS

        #region Index

        /// <summary>
        /// Render the employees index view based on the given parameters.
        /// </summary>
        [HttpGet]
        public virtual ActionResult Index(
            Guid? id = null,
            string Query = "",
            int PageNo = 1,
            MomentumPlus.Core.Models.People.User.SortColumn SortColumn = MomentumPlus.Core.Models.People.User.SortColumn.LastName,
            bool SortAscending = true,
            Package.Status? packageStatus = null,
            bool retired = false
        )
        {
            var employeesViewModel = EmployeeCore.GetEmployeesViewModel(
                id,
                Query,
                PageNo,
                SortColumn,
                SortAscending,
                packageStatus,
                retired
            );

            AppSession.Current.EmployeeFormReturnUrl = Request.RawUrl;
            AppSession.Current.PackageFormReturnUrl = Request.RawUrl;
            AppSession.Current.PackageExamineReturnUrl = Request.RawUrl;

            // Auto-select the first workplace if one exists
            if (employeesViewModel.Users != null && employeesViewModel.Users.Any() && employeesViewModel.SelectedUserId == null)
            {
                employeesViewModel.SelectedUserId = employeesViewModel.Users.First().ID;
                Mapper.Map(PackageService.GetPackagesByUserId(employeesViewModel.Users.First().ID, packageStatus, retired)
                    .OrderBy(p => p.UserOwner.LastName)
                    .ThenBy(p => p.UserOwner.FirstName), 
                    employeesViewModel.Packages);
            }

            return View(employeesViewModel);
        }

        #endregion

        #region CreateEmployee

        /// <summary>
        /// Create employee form.
        /// </summary>
        [Authorise(Roles = RoleAuthHelper.Employee_Create)]
        [HttpGet]
        public ActionResult CreateEmployee()
        {
            var viewModel = EmployeeCore.PopulateUserViewModel(new UserViewModel(), GetCurrentUser());
            viewModel.DisplayTooltips = true;
            viewModel.HidePosition = true;
            return View("EmployeeForm", viewModel);
        }

        /// <summary>
        /// Handle employee form submission.
        /// </summary>
        [Authorise(Roles = RoleAuthHelper.Employee_Create)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateEmployee(UserViewModel viewModel, HttpPostedFileBase profileImage)
        {
         
           if (viewModel.DefaultPositionId.HasValue)
            {
                var position = PositionService.GetByPositionId(viewModel.DefaultPositionId.Value);
                if (position != null)
                {
                    viewModel.Position = position.Name;
                    ModelState.Clear();
                    TryValidateModel(viewModel);
                }
            }

            var positionState = ModelState["Position"];
            if (positionState != null)
            {
                foreach (var error in positionState.Errors)
                {
                    ModelState.AddModelError("DefaultPositionId", error.ErrorMessage);
                }
                ModelState.Remove("Position");
            }

            if (string.IsNullOrEmpty(viewModel.Email))
            {
                ModelState.AddModelError("Email", "The Login/Email * field is required.");
            }

            if (string.IsNullOrWhiteSpace(viewModel.Password))
            {
                ModelState.AddModelError("Password", "The Password * field is required.");
            }

            if (string.IsNullOrWhiteSpace(viewModel.PasswordConfirm))
            {
                ModelState.AddModelError("PasswordConfirm", "The Confirm Password * field is required.");
            }

            if (!ModelState.IsValid)
            {
                viewModel = EmployeeCore.PopulateUserViewModel(viewModel, GetCurrentUser());
                viewModel.HidePosition = true;
                return View("EmployeeForm", viewModel);
            }

            try
            {
                if (UserService.Exists(viewModel.Email))
                {
                    ModelState.AddModelError("Email", "This User already exists!");
                }

                if (!ModelState.IsValid)
                {
                    viewModel = EmployeeCore.PopulateUserViewModel(viewModel, GetCurrentUser());
                    viewModel.HidePosition = true;
                    return View("EmployeeForm", viewModel);
                }

                var user = EmployeeCore.CreateEmployee(viewModel, profileImage);
                return GetEmployeeSuccessResult(user.ID, "User" + " [" + user.FirstName + "] " + "was successfully created.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Warning, "Something went wrong. Please try again");
            }

            return Redirect(AppSession.Current.EmployeeFormReturnUrl);
        }

        #endregion

        #region EditEmployee

        /// <summary>
        /// Edit employee form.
        /// </summary>
        [HttpGet]
        public ActionResult EditEmployee(Guid id)
        {
            var user = UserService.GetByUserId(id);

            if (user == null)
            {
                AddAlert(AlertViewModel.AlertType.Warning, "Sorry, but this user does not exist.");
                return RedirectToAction(AppSession.Current.EmployeeFormReturnUrl);
            }

            var currentUser = GetCurrentUser();

            // iHandover Agent/Administrator/Executive cannot create/edit iHandover Admins
            if ((currentUser.Role.RoleParsed == Role.RoleEnum.iHandoverAgent ||
                currentUser.Role.RoleParsed == Role.RoleEnum.Administrator ||
                currentUser.Role.RoleParsed == Role.RoleEnum.Executive) &&
                user.Role.RoleParsed == Role.RoleEnum.iHandoverAdmin)
            {
                AddAlert(AlertViewModel.AlertType.Warning, "You do not have permission to edit this user.");
                return Redirect(AppSession.Current.EmployeeFormReturnUrl);
            }

            // HR Manager can only create/edit Line Managers and Users
            if (currentUser.Role.RoleParsed == Role.RoleEnum.HRManager &&
                !(user.Role.RoleParsed == Role.RoleEnum.LineManager || user.Role.RoleParsed == Role.RoleEnum.User))
            {
                AddAlert(AlertViewModel.AlertType.Warning, "You do not have permission to edit this user.");
                return Redirect(AppSession.Current.EmployeeFormReturnUrl);
            }

            // Line manager can only edit users
            if (currentUser.Role.RoleParsed == Role.RoleEnum.LineManager &&
                user.Role.RoleParsed != Role.RoleEnum.User)
            {
                AddAlert(AlertViewModel.AlertType.Warning, "You do not have permission to edit this user.");
                return Redirect(AppSession.Current.EmployeeFormReturnUrl);
            }

            var viewModel = Mapper.Map<UserViewModel>(user);
            viewModel = EmployeeCore.PopulateUserViewModel(viewModel, currentUser);
            viewModel.ReadOnly = RoleAuthHelper.IsReadOnly("Employee_Edit_Get");
            AppSession.Current.ChangePasswordFormReturnUrl = Request.RawUrl;

            viewModel.IsAdminEdit = true;
            viewModel.HidePosition = true;
            return View("EmployeeForm", viewModel);
        }

        /// <summary>
        /// Edits an existing user in the database.
        /// </summary>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditEmployee(UserViewModel viewModel, HttpPostedFileBase profileImage)
        {
            try
            {
                // Get an existing user from the database by id
                var user = UserService.GetByUserId(viewModel.ID);

                if (viewModel.DefaultPositionId.HasValue)
                {
                    var position = PositionService.GetByPositionId(viewModel.DefaultPositionId.Value);
                    if (position != null)
                    {
                        viewModel.Position = position.Name;
                        ModelState.Clear();
                        TryValidateModel(viewModel);
                    }
                }

                var positionState = ModelState["Position"];
                if (positionState != null)
                {
                    foreach (var error in positionState.Errors)
                    {
                        ModelState.AddModelError("DefaultPositionId", error.ErrorMessage);
                    }
                    ModelState.Remove("Position");
                }

                if (string.IsNullOrEmpty(viewModel.Email))
                {
                    ModelState.AddModelError("Email", "The Login/Email * field is required.");
                }

                // If the model is not valid, do not save, but return
                if (!ModelState.IsValid)
                {
                    // Populate an existing view model
                    viewModel = EmployeeCore.PopulateUserViewModel(viewModel, GetCurrentUser());
                    viewModel.HidePosition = true;
                    return View("EmployeeForm", viewModel);
                }

                EmployeeCore.UpdateEmployee(user, viewModel, profileImage);

                return GetEmployeeSuccessResult(user.ID, "User" + " [" + user.FirstName + "] " + "was successfully updated.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Warning, "Something went wrong. Please try again");
            }

            return Redirect(AppSession.Current.EmployeeFormReturnUrl);
        }

        #endregion

        #region RetireEmployee/ResumeEmployee

        [HttpGet]
        public ActionResult RetireEmployee(Guid id)
        {
            try
            {
                UserService.Retire(id);
                AddAlert(AlertViewModel.AlertType.Success, "Employee retired successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error retiring employee.");
            }

            return RedirectToAction("EditEmployee", new { id = id });
        }

        [HttpGet]
        public ActionResult ResumeEmployee(Guid id)
        {
            try
            {
                UserService.Restore(id);
                AddAlert(AlertViewModel.AlertType.Success, "Employee resumed successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error resuming employee.");
            }

            return RedirectToAction("EditEmployee", new { id = id });
        }

        #endregion

        #region Change Password (GET & POST)

        [HttpGet]
        public ActionResult ChangePasswordAdmin(Guid id)
        {
            return View(new ChangePasswordAdminViewModel { UserId = id });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePasswordAdmin(ChangePasswordAdminViewModel viewModel)
        {
            var user = UserService.GetByUserId(viewModel.UserId);

            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            try
            {
                var success = UserService.ChangePassword(viewModel.UserId, viewModel.NewPassword);

                if (success)
                {
                    AddAlert(AlertViewModel.AlertType.Success, "Password changed successfully.");
                }
                else
                {
                    AddAlert(AlertViewModel.AlertType.Danger, "Something went wrong, please try again.");
                }
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Something went wrong, please try again.");
            }

            return Redirect(AppSession.Current.ChangePasswordFormReturnUrl);
        }

        #endregion

        #endregion

        #region HELPERS

        private ActionResult GetEmployeeSuccessResult(Guid employeeId, string successMessage)
        {
            // Redirect to a specific action depending on the submit button pressed
            switch (Request.Form["Redirect"])
            {
                case "CreatePackage":
                    return RedirectToAction("CreatePackage", new { selectedUserId = employeeId });
            }

            AddAlert(AlertViewModel.AlertType.Success, successMessage);
            return Redirect(AppSession.Current.EmployeeFormReturnUrl);
        }

        #endregion
    }
}
