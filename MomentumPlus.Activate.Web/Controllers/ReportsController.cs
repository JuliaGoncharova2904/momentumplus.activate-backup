﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using AutoMapper;
using MomentumPlus.Activate.Services;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Core.Home;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Activate.Web.Models.People;
using MomentumPlus.Activate.Web.Core;
using MomentumPlus.Activate.Web.Extensions;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.Moderation;
using MomentumPlus.Core.Models.People;
using System.Net;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models.Activate;

namespace MomentumPlus.Activate.Web.Controllers
{
    public class ReportsController : TaskController
    {
        private RelationshipsCore RelationshipsCore;

        #region IOC

        private IContactService ContactService;
        private IDocumentLibraryService DocumentLibraryService;
        private IHandoverService HandoverService;
        private OnboardingCore Onboarding;
        private IMeetingService MeetingService;
        private IModuleService ModuleService;
        private IAuditService AuditService;
        private IPeopleService PeopleService;

        public IProjectService ProjectService { get; set; }

        public ReportsController(
            IAuditService auditService,
            IDocumentLibraryService documentLibraryService,
            IHandoverService handoverService,
            IMasterListService masterListService,
            IModuleService moduleService,
            IPackageService packageService,
            ITaskService taskService,
            ITopicService topicService,
            ITopicGroupService topicGroupService,
            ITopicQuestionService topicQuestionService,
            IUserService userService,
            IMeetingService meetingService,
            IWorkplaceService workplaceService,
            IPeopleService peopleService,
            IProLinkService proLink,
            IProjectService projectService,
            IContactService contactService,
            OnboardingCore onboardingCore,
            RelationshipsCore relationshipsCore,
            TaskCore taskCore,
            IPositionService positionService,
            TopicCore topicCore
        )
            : base(
                  auditService,
                  handoverService,
                  masterListService,
                  packageService,
                  taskService,
                  topicService,
                  topicGroupService,
                  topicQuestionService,
                  userService,
                  workplaceService,
                  proLink,
                  projectService,
                  contactService,
                  taskCore,
                  positionService,
                  topicCore
              )
        {
            RelationshipsCore = relationshipsCore;

            DocumentLibraryService = documentLibraryService;
            ContactService = contactService;
            HandoverService = handoverService;
            Onboarding = onboardingCore;
            MeetingService = meetingService;
            ModuleService = moduleService;
            HandoverService = handoverService;
            AuditService = auditService;
            PeopleService = peopleService;
        }

        #endregion

        #region General Action Methods

        /// <summary>
        /// Render the reports home view.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return RedirectToAction(
                "HandoverReports",
                "Reports"
            );
        }

        public ActionResult ViewAdditionalReport(Guid id, bool ancestor = false)
        {
            AdditionalReportViewModel reportViewModel = BuildRAGReportModel(id, ancestor, true);

            AppSession.Current.TaskFormReturnUrl = Request.RawUrl;
            AppSession.Current.TopicFormReturnUrl = Request.RawUrl;
            AppSession.Current.VoiceMessageManagerReturnUrl = Request.RawUrl;
            AppSession.Current.AttachmentManagerReturnUrl = Request.RawUrl;
            AppSession.Current.MeetingFormReturnUrl = Request.RawUrl;

            return View("RAGReports/RAGReport", reportViewModel);
        }

        public ActionResult DownloadAdditionalReport(Guid id, bool ancestor = false)
        {

            AdditionalReportViewModel reportViewModel = BuildRAGReportModel(id, ancestor);
            byte[] pdfBytes = GenerateAdditionalPdf(reportViewModel);
            return File(pdfBytes, "application/pdf", "Momentum Plus Leaver Report.pdf");
        }

        #endregion

        #region General Helpers

        /// <summary>
        /// generate the pdf report for the Additional Reports
        /// </summary>
        /// <param name="reportViewModel"></param>
        /// <returns></returns>
        private byte[] GenerateAdditionalPdf(AdditionalReportViewModel reportViewModel)
        {
            reportViewModel.IsPdf = true;

            var converter = new NReco.PdfGenerator.HtmlToPdfConverter
            {
                CustomWkHtmlPageArgs = "--footer-center [page]/[toPage]",
                GenerateToc = true,
                TocHeaderText = "Contents",
                Orientation = NReco.PdfGenerator.PageOrientation.Landscape,
                Zoom = 0.8F
            };

            PdfReport pdfReport = new PdfReport()
            {
                HtmlContent = RenderViewToString("RAGReports/RAGReport", reportViewModel),
                PdfConvertor = converter
            };

            return pdfReport.GetReport();
        }

        /// <summary>
        /// Render a Razor view to a string.
        /// </summary>
        /// <param name="viewName">The name of the view.</param>
        /// <param name="model">The model to pass into the view when rendering.</param>
        /// <returns></returns>
        private string RenderViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        private int SetPercentageComplete(int target, int actual)
        {
            double actualDivTarget = 0;
            if (target == 0)
            {
                //leave 0;
            }
            else
            {
                actualDivTarget = (double)actual / (double)target;
            }
            int percentageOfTarget = Convert.ToInt32((actualDivTarget) * 100);
            return percentageOfTarget;
        }

        private int PercentageOfNumbers(int target, int actual)
        {
            double actualDivTarget = 0;
            if (target == 0)
            {
                //leave 0;
            }
            else
            {
                actualDivTarget = (double)actual / (double)target;
            }
            int percentageOfTarget = Convert.ToInt32((actualDivTarget) * 100);
            return percentageOfTarget;
        }

        /// <summary>
        /// build the module view models for this module
        /// </summary>
        /// <param name="package"></param>
        /// <param name="reportViewModel"></param>
        /// <returns></returns>
        private ICollection<ModuleViewModel> ModuleGather(Package package, AdditionalReportViewModel reportViewModel, bool isRAG = false)
        {
            try
            {
                //(date created + duration days) <- difference between this day and today
                //theHandover = reportViewModel.Package.Created
                double theHandover = 0;
                if (reportViewModel.Package.Handover != null)
                {
                    theHandover = ((reportViewModel.Package.Handover.LeavingPeriod.LastDay ?? DateTime.Now) - DateTime.Now).TotalDays;

                    reportViewModel.HandoverCountDownDaysTotal = (Convert.ToInt32(theHandover) > 0) ? Convert.ToInt32(theHandover) : 0;

                    reportViewModel.HandoverCountdownWeeks = (theHandover > 0) ? Convert.ToInt32(Math.Floor(theHandover / 7)) : 0;
                    reportViewModel.HandoverCountdownDays = (theHandover > 0) ? Convert.ToInt32(theHandover % 7) : 0;
                    reportViewModel.TargetCompletionDate = (reportViewModel.Package.Handover.LeavingPeriod.LastDay ?? DateTime.Now)
                        .AddDays(-5).ToShortDateString();
                }
                else
                {
                    if (isRAG)
                        AddAlert(AlertViewModel.AlertType.Danger, "The Handover for this package is unset");
                }

                reportViewModel.TargetAttachments = Helpers.AccountConfig.Settings.TargetAttachments;
                reportViewModel.TargetAverageWordCount = Helpers.AccountConfig.Settings.TargetAverageWordcount;
                reportViewModel.TargetVolume = Helpers.AccountConfig.Settings.TargetVolume;


                ICollection<Module> modules = new List<Module>();
                foreach (var mod in package.Modules)
                {
                    modules.Add(mod);
                }

                var moduleViewModels = Mapper.Map<IEnumerable<Module>,
                    ICollection<ModuleViewModel>>(modules);
                foreach (var mod in moduleViewModels)
                {
                    mod.ActualTotalWordCount = ExtensionMethods.CountWords(ModuleService.GetByModuleId(mod.ID));
                    mod.ActualAverageWordCount = ExtensionMethods.GetModuleWordAverage(ModuleService.GetByModuleId(mod.ID));
                    ICollection<TopicGroupViewModel> topicGroups = new List<TopicGroupViewModel>();
                    foreach (var topicGroupViewModel in mod.TopicGroups)
                    {
                        topicGroups.Add(topicGroupViewModel);
                    }

                    mod.Modified = ModuleService.GetDateLastEntry(mod.ID);

                    //Actual Values
                    mod.ActualAttachments = ExtensionMethods.GetNumberOfAttachmentsAndVoiceMessages(ModuleService.GetByModuleId(mod.ID));
                    mod.ActualVolume = ModuleService.GetTopicTaskCount(mod.ID);
                    mod.ActualApproved = ExtensionMethods.CountItemsApproved(ModuleService.GetByModuleId(mod.ID));

                    // Target Values 
                    mod.TargetAttachments = reportViewModel.TargetAttachments;
                    mod.TargetAverageWordCount = reportViewModel.TargetAverageWordCount;
                    mod.TargetVolume = reportViewModel.TargetVolume;
                    mod.TargetCompletionDate = reportViewModel.TargetCompletionDate;

                    //RAG Valuez
                    mod.RagAttachments = SetRagStatus(mod.TargetAttachments, mod.ActualAttachments);
                    mod.RagVolume = SetRagStatus(mod.TargetVolume, mod.ActualVolume);
                    mod.RagWordCount = SetRagStatus(mod.TargetAverageWordCount, mod.ActualAverageWordCount);
                    mod.RagApproved = SetRagStatus(mod.ActualVolume, mod.ActualApproved);

                    //Percent Values
                    mod.PercentageAttachments = SetPercentageComplete(mod.TargetAttachments, mod.ActualAttachments);
                    mod.PercentageVolume = SetPercentageComplete(mod.TargetVolume, mod.ActualVolume);
                    mod.PercentageWordCount = SetPercentageComplete(mod.TargetAverageWordCount, mod.ActualAverageWordCount);
                    mod.PercentageApproved = SetPercentageComplete(mod.ActualVolume, mod.ActualApproved);
                }

                return moduleViewModels;
            }
            catch (Exception e)
            {
                LogException(e);
                AddAlert(AlertViewModel.AlertType.Danger, "Error creating report.");
                return null;
            }
        }

        private ContactViewModel ContactIndividualGather(Package package, AdditionalReportViewModel reportViewModel)
        {
            ContactViewModel ModelCont = new ContactViewModel();
            ModelCont.ActualTotalWordCount = ExtensionMethods.CountContactWords(package);
            ModelCont.ActualAverageWordCount = ExtensionMethods.GetContactWordAverage(package);
            ModelCont.ActualAttachments = ExtensionMethods.GetNumberOfContactAttachmentsAndVoiceMessages(package);
            ModelCont.ActualVolume = ExtensionMethods.GetContactCount(package);
            ModelCont.ActualApproved = ExtensionMethods.CountContactItemsApproves(package);

            //RAG Values
            ModelCont.RagAttachments = SetRagStatus(ModelCont.TargetAttachments, ModelCont.ActualAttachments);
            ModelCont.RagVolume = SetRagStatus(ModelCont.TargetVolume, ModelCont.ActualVolume);
            ModelCont.RagWordCount = SetRagStatus(ModelCont.TargetAverageWordCount, ModelCont.ActualAverageWordCount);
            ModelCont.RagApproved = SetRagStatus(ModelCont.ActualVolume, ModelCont.ActualApproved);

            //Percent Values
            ModelCont.PercentageAttachments = SetPercentageComplete(ModelCont.TargetAttachments, ModelCont.ActualAttachments);
            ModelCont.PercentageVolume = SetPercentageComplete(ModelCont.TargetVolume, ModelCont.ActualVolume);
            ModelCont.PercentageWordCount = SetPercentageComplete(ModelCont.TargetAverageWordCount, ModelCont.ActualAverageWordCount);
            ModelCont.PercentageApproved = SetPercentageComplete(ModelCont.ActualVolume, ModelCont.ActualApproved);

            ModelCont.UserLastViewed = package.RelationshipsLastViewed;

            if (package.Contacts != null && package.Contacts.Any())
            {
                ModelCont.Modified = package.Contacts.Where(c => !c.Managed).Max(c => c.Created.Value);
            }

            return ModelCont;
        }

        private ICollection<ContactViewModel> ContactGather(Package package, AdditionalReportViewModel reportViewModel)
        {
            try
            {
                ICollection<Contact> contacts = new List<Contact>();
                foreach (var cont in package.Contacts)
                {
                    contacts.Add(cont);
                }
                var contactViewModels = Mapper.Map<IEnumerable<Contact>,
                    ICollection<ContactViewModel>>(contacts);
                foreach (var cont in contactViewModels)
                {
                    cont.ActualTotalWordCount = ExtensionMethods.CountWords(ContactService.GetById(cont.ID));
                    cont.ActualAverageWordCount = cont.ActualTotalWordCount;
                    cont.ActualAttachments = ExtensionMethods.GetNumberOfAttachmentsAndVoiceMessages(ContactService.GetById(cont.ID));
                    cont.Modified = ContactService.GetById(cont.ID).Modified;

                    cont.TargetAttachments = reportViewModel.TargetAttachments;
                    cont.TargetAverageWordCount = reportViewModel.TargetAverageWordCount;
                    cont.TargetVolume = reportViewModel.TargetVolume;
                    cont.TargetCompletionDate = reportViewModel.TargetCompletionDate;

                    cont.RagAttachments = SetRagStatus(cont.TargetAttachments, cont.ActualAttachments);
                    cont.RagVolume = SetRagStatus(cont.TargetVolume, cont.ActualVolume);
                    cont.RagWordCount = SetRagStatus(cont.TargetAverageWordCount, cont.ActualAverageWordCount);
                    cont.RagApproved = SetRagStatus(cont.ActualVolume, cont.ActualApproved);

                    cont.PercentageAttachments = SetPercentageComplete(cont.TargetAttachments, cont.ActualAttachments);
                    cont.PercentageVolume = SetPercentageComplete(cont.TargetVolume, cont.ActualVolume);
                    cont.PercentageWordCount = SetPercentageComplete(cont.TargetAverageWordCount, cont.ActualAverageWordCount);
                    cont.PercentageApproved = SetPercentageComplete(cont.ActualVolume, cont.ActualApproved);
                }

                return contactViewModels;
            }
            catch (Exception e)
            {
                LogException(e);
                AddAlert(AlertViewModel.AlertType.Danger, "Error creating report.");
                return null;
            }
        }

        #endregion

        #region ExitSurvey PDF Report

        #region Action Methods

        public ActionResult ExitSurveyReports()
        {
            IEnumerable<Package> packages = PackageService.GetByPackageStatus(Package.Status.Triggered, false)
                        .Where(p => p.Handover.LeavingPeriod.Locked.HasValue && p.Handover.LeavingPeriod.LastDay.HasValue && p.Handover.LeavingPeriod.LastDay.Value > System.DateTime.Now)
                        .OrderBy(p => p.UserOwner.FullName);
            ExitSurveyService exitSurveyService = new ExitSurveyService(new MomentumContext());
            User currentUser = GetCurrentUser();
            List<ReportExitSurveyViewModel> model = new List<ReportExitSurveyViewModel>();
            foreach (var package in packages)
            {
                ReportExitSurveyViewModel report = new ReportExitSurveyViewModel
                {
                    UserImageId = package.UserOwner.Contact.ImageId,
                    UserOwnerId = package.UserOwnerId,
                    PackageId = package.ID,
                    UserOwnerName = package.UserOwner.FullName,
                    LineManagerName = package.UserLineManager.FullName,
                    LastDateOfWork = package.Handover != null
                        ? package.Handover.LeavingPeriod.LastDay.Value.ToStringShortFormat()
                        : string.Empty,
                    ExitSurveyId = package.ExitSurveyTemplateId.HasValue
                        ? package.ExitSurveyTemplate.ExitSurveyId.ToString()
                        : string.Empty,
                    ExitSurveyName = package.ExitSurveyTemplateId.HasValue
                        ? package.ExitSurveyTemplate.ExitSurvey.Name
                        : string.Empty,
                    HasAccess = ((currentUser.Role.RoleName == RoleAuthHelper.LineManager &&
                                  package.SubmitExitSurvey == SubmitExitSurvey.HRManagerSubmit)
                                 || (currentUser.Role.RoleName == RoleAuthHelper.HRManager &&
                                     package.SubmitExitSurvey == SubmitExitSurvey.UserSubmit)
                                 || (currentUser.Role.RoleName == RoleAuthHelper.HRManager &&
                                     package.SubmitExitSurvey == SubmitExitSurvey.HRManagerSubmit)
                                 || currentUser.ID == package.UserOwnerId),
                    ReadMode = currentUser.Role.RoleName == RoleAuthHelper.Administrator || currentUser.Role.RoleName == RoleAuthHelper.iHandoverAdmin || currentUser.Role.RoleName == RoleAuthHelper.Admin_General
                };
                var sectionTotalCount = package.ExitSurveyTemplateId.HasValue
                    ? exitSurveyService.GetSurveyByID(package.ExitSurveyTemplate.ExitSurveyId).Sections.Count
                    : 0;
                int completeSectionCount = 0;
                bool hasAnsweredQustion = false;
                if (package.ExitSurveyTemplateId.HasValue)
                {
                    foreach (var section in package.ExitSurveyTemplate.ExitSurvey.Sections)
                    {
                        int completeQuestionCount = 0;
                        foreach (var question in section.Questions)
                        {
                            if (question.ExitSurveyAnswers.Any(a => a.UserId == package.UserOwnerId && a.UserAnswerJSON != null))
                            {
                                completeQuestionCount++;
                            }
                        }
                        if (completeQuestionCount > 0)
                        {
                            hasAnsweredQustion = true;
                        }
                        if (completeQuestionCount == section.Questions.Count)
                        {
                            completeSectionCount++;
                        }
                    }
                    report.CounterStatus = completeSectionCount + "/" + sectionTotalCount;
                }
                if (!package.ExitSurveyTemplateId.HasValue)
                {
                    report.Status = "Not Requested";
                }
                if (!hasAnsweredQustion)
                {
                    report.Status = "Not Started";
                }
                else
                {
                    switch (package.SubmitExitSurvey)
                    {
                        case SubmitExitSurvey.NotSubmit:
                            report.Status = "In Progress";
                            break;
                        case SubmitExitSurvey.UserSubmit:
                            report.Status = "Awaiting HR Approval";
                            break;
                        case SubmitExitSurvey.HRManagerSubmit:
                            report.Status = "Complete";
                            break;
                        default:
                            report.Status = "N/A";
                            report.CounterStatus = "N/A";
                            break;
                    }
                }
                //if (sectionTotalCount > 0 && report.Status == "Not Requested")
                //{
                //    report.Status = "N/A";
                //    report.CounterStatus = "N/A";
                //}
                model.Add(report);
            }
            return View("~/Views/Reports/ExitSurveyReport/Index.cshtml", model);
        }

        public ActionResult ViewExitSurveyReport(Guid exitSurveyId)
        {
            Package package = GetCurrentPackage();
            ExitSurveyService exitSurveyService = new ExitSurveyService(new MomentumPlus.Core.DataSource.MomentumContext());

            ExitSurvey exitSurvey = exitSurveyService.GetSurveyByID(exitSurveyId);
            if (exitSurvey != null)
            {
                ExitSurveyReportViewModel model = new ExitSurveyReportViewModel
                {
                    IsPdf = false,
                    Package = package,
                    ExitSurveyId = exitSurveyId,
                    Pickup = package.Handover != null && package.Handover.OnboardingPeriod.Package != null
                                                ? package.Handover.OnboardingPeriod.Package.UserOwner : null
                };

                return View("~/Views/ExitSurvey/ViewReportInterview.cshtml", model);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        public ActionResult DownloadExitSurveyReport(Guid packageId, Guid exitSurveyId)
        {
            Package package = PackageService.GetByPackageId(packageId);
            ExitSurveyReportViewModel model = new ExitSurveyReportViewModel
            {
                IsPdf = true,
                Package = package,
                ExitSurveyId = exitSurveyId,
                Pickup = package.Handover != null && package.Handover.OnboardingPeriod.Package != null
                                        ? package.Handover.OnboardingPeriod.Package.UserOwner : null
            };
            byte[] pdfBytes = GenerateExitSurveyReportPdf(model);
            var filename = model.Package.UserOwner.FullName +
                "-Activate-" + DateTime.Now.ToShortDateString() + ".pdf";
            return File(pdfBytes, "application/pdf", filename);
        }


        #endregion

        #region Helper

        private byte[] GenerateExitSurveyReportPdf(ExitSurveyReportViewModel model)
        {
            var converter = new NReco.PdfGenerator.HtmlToPdfConverter
            {
                CustomWkHtmlPageArgs = "--footer-center [page]/[toPage]",
                GenerateToc = true,
                TocHeaderText = "Contents",
            };

            PdfReport pdfReport = new PdfReport
            {
                HtmlCover = RenderViewToString("~/Views/ExitSurvey/Cover.cshtml", model),
                HtmlContent = RenderViewToString("~/Views/ExitSurvey/ViewReportInterview.cshtml", model),
                PdfConvertor = converter
            };

            return pdfReport.GetReport();
        }

        #endregion

        #endregion

        #region Handover Report

        #region Action Methods

        /// <summary>
        /// Render the handover reports index view.
        /// </summary>
        public ActionResult HandoverReports(ReportViewModel.SortColumn sortColumn = ReportViewModel.SortColumn.LastName, bool sortAscending = true)
        {

            //  var packages = GetPackages();
            var packages = GetPackagesForReportsIndexView();


            if (packages.Count() == 1 && !packages.First().AncestorPackageId.HasValue && AppSession.Current.ViewAsUser == null)
            {
                return RedirectToAction("ViewHandoverReport", new { id = packages.First().ID });
            }

            if (sortColumn == ReportViewModel.SortColumn.LastName)
            {
                packages = packages.OrderByWithDirection(s => s.UserOwner.LastName, sortAscending);
            }
            else if (sortColumn == ReportViewModel.SortColumn.FirstName)
            {
                packages = packages.OrderByWithDirection(s => s.UserOwner.FirstName, sortAscending);
            }
            else if (sortColumn == ReportViewModel.SortColumn.Position)
            {
                packages = packages.OrderByWithDirection(s => s.Position.Name, sortAscending);
            }
            else if (sortColumn == ReportViewModel.SortColumn.Workplace)
            {
                packages = packages.OrderByWithDirection(s => s.Workplace.Name, sortAscending);
            }

            foreach (Package package in packages)
            {
                StringBuilder sb = new StringBuilder();

                sb.Append(package.UserOwner.FirstName);
                sb.Append(" ");
                sb.Append(package.UserOwner.LastName);
                sb.Append(" - ");
                sb.Append(package.Position.Name);
                sb.Append(" - ");
                sb.Append(package.Workplace.Name);
                package.Name = sb.ToString();
            }

            var reportViewModel = new ReportViewModel { Packages = packages, SortAscending = sortAscending };
            return View("HandoverReports/Index", reportViewModel);
        }

        /// <summary>
        /// Render the report web view.
        /// </summary>
        /// <param name="id">The ID of the package.</param>
        /// <param name="ancestor">Generate the package ancestor report.</param>
        public ActionResult ViewHandoverReport(Guid id, bool ancestor = false)
        {
            HandoverReportViewModel viewModel = BuildHandoverReportViewModel(id, ancestor);

            AppSession.Current.TaskFormReturnUrl = Request.RawUrl;
            AppSession.Current.TopicFormReturnUrl = Request.RawUrl;
            AppSession.Current.VoiceMessageManagerReturnUrl = Request.RawUrl;
            AppSession.Current.AttachmentManagerReturnUrl = Request.RawUrl;
            AppSession.Current.MeetingFormReturnUrl = Request.RawUrl;

            if ((User.IsInRole(RoleAuthHelper.HRManager) || User.IsInRole(RoleAuthHelper.LineManager) || User.IsInRole(RoleAuthHelper.Administrator) || User.IsInRole(RoleAuthHelper.iHandoverAdmin) || User.IsInRole(RoleAuthHelper.iHandoverAgent))
                && HandoverService.IsLeaverPeriodLocked(viewModel.Package.ID))
            {
                ViewBag.Tasklist = true;
            }

            return View("~/Views/Reports/HandoverReports/HandoverReport.cshtml", viewModel);
        }

        /// <summary>
        /// Download PDF handover report.
        /// </summary>
        /// <param name="id">The ID of the package.</param>
        /// <param name="ancestor">Generate the package ancestor report.</param>
        public ActionResult DownloadHandoverReport(Guid id, bool ancestor = false)
        {
            var viewModel = BuildHandoverReportViewModel(id, ancestor, true);
            byte[] pdfBytes = GenerateHandoverReportPdf(viewModel);
            var filename = viewModel.Package.UserOwner.FullName +
                "-Activate-" + DateTime.Now.ToShortDateString() + ".pdf";
            return File(pdfBytes, "application/pdf", filename);
        }

        public ActionResult GoToCloneView(Guid id)
        {
            var topic = TopicService.GetByTopicId(id);
            var currentPackageId = GetCurrentPackageId();

            if (topic.SourceId.HasValue && currentPackageId.HasValue)
            {
                var onboardingPeriod = HandoverService.GetOnboardingPeriodForPackage(currentPackageId.Value);

                if (onboardingPeriod != null && onboardingPeriod.ViewPreviousPackageContent)
                {
                    var onboarderTopic = TopicService.GetByTopicSourceId(topic.SourceId.Value, currentPackageId.Value);

                    if (onboarderTopic != null)
                    {
                        return RedirectToAction("EditTopic", new { id = onboarderTopic.ID, prevTopic = true });
                    }
                }
            }

            return RedirectToAction("ViewTopic", new { id = id });
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Build the handover report view model for a given package.
        /// </summary>
        private HandoverReportViewModel BuildHandoverReportViewModel(Guid packageId, bool ancestor = false, bool pdf = false)
        {
            bool showAll = ShowAll(AppSession.Current.CurrentPackageId);

            var package = PackageService.GetByPackageId(packageId);

            if (ancestor && package.AncestorPackageId.HasValue)
            {
                package = PackageService.GetByPackageId(package.AncestorPackageId.Value);
            }

            var pickup = package.Handover != null && package.Handover.OnboardingPeriod.Package != null ?
                         package.Handover.OnboardingPeriod.Package.UserOwner :
                         null;

            var isLeavingPeriodLocked = HandoverService.IsLeaverPeriodLocked(package.ID);
            var isOnboardingPeriodLocked = HandoverService.IsOnboarderPeriodLocked(package.ID);

            var graphNodes = RelationshipsCore.GetGraphNodes(package, ContactService.GetCompanies(package.ID), isLeavingPeriodLocked);
            var defaultCompanyId = MasterListService.GetDefaultCompanyId();

            var criticalTopicGroups = GetCriticalTopicGroups(package.ID, showAll);
            var experiencesTopicGroups = GetExperiencesTopicGroups(package.ID, showAll);
            var tasks = GetTasks(package.ID, showAll);
            var staffContacts = GetStaffContacts(package.ID, showAll);
            var projectInstances = GetProjectInstances(package.ID, showAll);

            DateTime? onboarderReviewDateExperiences = null;
            DateTime? onboarderReviewDateCritical = null;
            List<AuditLog> onboarderLastViewedTopicsExperiences = new List<AuditLog>();
            List<AuditLog> onboarderLastViewedTopicsCritical = new List<AuditLog>();

            if (package.Handover != null &&
                package.Handover.OnboardingPeriod != null &&
                package.Handover.OnboardingPeriod.Package != null)
            {
                var packageOnboarder =
                    package.Handover.OnboardingPeriod.Package;

                onboarderReviewDateExperiences =
                    HandoverService.GetOnboarderReviewDateExperiences(packageOnboarder.ID);

                onboarderReviewDateCritical =
                    HandoverService.GetOnboarderReviewDateCritical(packageOnboarder.ID);

                onboarderLastViewedTopicsExperiences =
                    GetOnboarderLastViewedTopics(packageOnboarder.UserOwnerId, experiencesTopicGroups.ToList());

                onboarderLastViewedTopicsCritical =
                    GetOnboarderLastViewedTopics(packageOnboarder.UserOwnerId, criticalTopicGroups.ToList());
            }

            List<Meeting> meetings;
            List<Guid> meetingTypesFilter = new List<Guid>() { Guid.Parse("E72D15EF-BD12-49AB-B15B-269EDF6A891B") };

            if (package.Handover != null &&
                package.Handover.OnboardingPeriod != null &&
                package.Handover.OnboardingPeriod.Locked.HasValue &&
                package.Handover.OnboardingPeriod.Package != null)
            {
                var packageOnboarder =
                    package.Handover.OnboardingPeriod.Package;

                meetings = MeetingService.GetByPackageId(packageOnboarder.ID, Meeting.SortColumn.Start, true)
                    .Where(m => m.Start >= package.Handover.OnboardingPeriod.StartPoint).ToList();
            }
            else
            {
                meetings = MeetingService.GetByPackageId(packageId, Meeting.SortColumn.Name, true,
                    meetingTypeIdsFilter: meetingTypesFilter).ToList();
            }

            var model = new HandoverReportViewModel
            {
                IsPdf = pdf,
                IsAncestor = ancestor,
                Package = package,
                Pickup = pickup,
                Meetings = meetings,
                GraphNodes = graphNodes,
                DefaultCompanyId = defaultCompanyId,
                CriticalTopicGroups = criticalTopicGroups,
                ExperiencesTopicGroups = experiencesTopicGroups,
                Tasks = tasks,
                StaffContacts = staffContacts,
                ProjectInstances = projectInstances,
                OnboarderReviewDateExperiences = onboarderReviewDateExperiences,
                OnboarderReviewDateCritical = onboarderReviewDateCritical,
                OnboarderLastViewedTopicsCritical = onboarderLastViewedTopicsCritical,
                OnboarderLastViewedTopicsExperiences = onboarderLastViewedTopicsExperiences,
                ShowCritical = criticalTopicGroups.Any(),
                ShowExperiences = experiencesTopicGroups.Any(),
                ShowMeetings = meetings.Any(),
                ShowPeople = staffContacts.Any(),
                ShowProjects = projectInstances.Any(),
                ShowTasks = tasks.Any(),
                IsLeavingPeriodLocked = isLeavingPeriodLocked,
                IsOnboardingPeriodLocked = isOnboardingPeriodLocked
            };

            var currentPackage = GetCurrentPackage();
            var user = GetCurrentUser();
            if ((currentPackage != null && user != null) && currentPackage.UserOwnerId == user.ID && ancestor)
            {
                model.IsOnboarder = true;
            }
            else
            {
                model.IsOnboarder = false;
            }

            if (model.Pickup != null) // HR/LM/Leaver viewing the onboarder report - point to Leaver's PeopleCard
            {
                if (!model.IsOnboarder)
                {
                    var a = PackageService.GetByPackageId(package.ID);
                    SetCurrentPackage(a);
                }
                else // Point to onboarder's PeopleCard
                {
                    var onboardingPeriod = HandoverService.GetOnboardingPeriodForPackage(currentPackage.ID);

                    if (onboardingPeriod != null && onboardingPeriod.ViewPreviousPackageContent)
                    {
                        model.ProjectInstances = GetProjectInstances(currentPackage.ID, true).Where(p => p.PeriodId.HasValue);
                        model.StaffContacts = GetStaffContacts(currentPackage.ID, true).Where(s => s.PeriodId.HasValue);
                    }
                }
            }

            ViewBag.TopicFlag = new Dictionary<Topic, PackageFlag>();
            ViewBag.TasksFlag = new Dictionary<Task, PackageFlag>();
            if (!model.IsOnboarder)
            {
                foreach (var topicGroup in model.CriticalTopicGroups)
                {
                    foreach (var topic in topicGroup.Topics)
                    {
                        ViewBag.TopicFlag[topic] = FlaggingService.GetTopicFlag(topic);
                    }
                }

                foreach (var task in model.Tasks)
                {
                    ViewBag.TasksFlag[task] = FlaggingService.GetTaskFlag(task, packageId);
                }
            }

            return model;
        }

        private IList<TopicGroup> GetCriticalTopicGroups(Guid packageId, bool showAll = false)
        {
            return GetTopicGroups(packageId, Module.ModuleType.Critical, showAll);
        }

        private IList<TopicGroup> GetExperiencesTopicGroups(Guid packageId, bool showAll = false)
        {
            return GetTopicGroups(packageId, Module.ModuleType.Experiences, showAll);
        }

        private IList<TopicGroup> GetTopicGroups(Guid packageId, Module.ModuleType moduleType, bool showAll = false)
        {
            var topicGroups = TopicGroupService.GetByPackageId(packageId)
                .Where(tg => tg.ParentModule.Type == moduleType).ToList();

            if (!showAll)
            {
                foreach (var topicGroup in topicGroups)
                {
                    topicGroup.Topics = topicGroup.Topics.Where(Moderated.Approved<Topic>().Compile()).ToList();
                }
            }

            //remove Topic Groups without topics
            topicGroups.RemoveAll(tg => !tg.Topics.Any());

            return topicGroups;
        }

        private IList<Task> GetTasks(Guid packageId, bool showAll = false)
        {
            var tasks = TaskService.GetAllByPackageId(packageId);

            if (!showAll)
            {
                tasks = tasks.Where(Moderated.Approved<Task>().Compile());
            }

            return tasks.ToList();
        }

        private IList<Contact> GetStaffContacts(Guid packageId, bool showAll = false)
        {
            var staffContacts = ContactService.GetStaffContacts(packageId, Person.SortColumn.FirstName);

            if (!showAll)
            {
                staffContacts = staffContacts.Where(Moderated.Approved<Contact>().Compile());
            }

            return staffContacts.ToList();
        }

        private IList<Contact> GetStaffContactsApproved(Guid packageId, bool showAll = false)
        {
            var staffContacts = ContactService.GetStaffContacts(packageId, Person.SortColumn.FirstName).Where(c => c.IsApproved());

            if (!showAll)
            {
                staffContacts = staffContacts.Where(Moderated.Approved<Contact>().Compile());
            }

            return staffContacts.ToList();
        }

        private IList<ProjectInstance> GetProjectInstances(Guid packageId, bool showAll = false)
        {
            var projectInstances = ProjectService.GetProjectInstances(packageId, null, ProjectInstance.SortColumn.Name,
                true, approved: null).Where(p => p.Project != null);

            if (!showAll)
            {
                projectInstances = projectInstances.Where(Moderated.Approved<ProjectInstance>().Compile());
            }

            return projectInstances.OrderByDescending(p => p.Priority).ToList();
        }

        private IList<ProjectInstance> GetProjectInstancesApproved(Guid packageId, bool showAll = false)
        {
            var projectInstances = ProjectService.GetProjectInstances(packageId, null, ProjectInstance.SortColumn.Name,
                true, true).Where(p => p.Project != null);

            if (!showAll)
            {
                projectInstances = projectInstances.Where(Moderated.Approved<ProjectInstance>().Compile());
            }

            return projectInstances.ToList();
        }

        private bool ShowAll(Guid? packageId)
        {
            bool isOnborder = packageId.HasValue && HandoverService.IsOnboarding(packageId.Value);
            bool isLeavingLocked = packageId.HasValue && HandoverService.IsLeaverPeriodLocked(packageId.Value);

            return !isOnborder
                    || isLeavingLocked
                    || User.IsInRole(RoleAuthHelper.LineManager)
                    || User.IsInRole(RoleAuthHelper.HRManager)
                    || User.IsInRole(RoleAuthHelper.Administrator)
                    || User.IsInRole(RoleAuthHelper.iHandoverAdmin)
                    || User.IsInRole(RoleAuthHelper.iHandoverAgent);
        }

        private List<AuditLog> GetOnboarderLastViewedTopics(Guid userIdOnboarder, List<TopicGroup> topicGroups)
        {
            var auditLogs = new List<AuditLog>();

            foreach (TopicGroup topicGroup in topicGroups)
            {
                if (topicGroup.Topics != null)
                {
                    var list = AuditService.LastViewed<Topic>(userIdOnboarder,
                        topicGroup.Topics.Select(t => t.ID).ToList());
                    if (list.Any())
                        auditLogs.AddRange(list);
                }
            }

            return auditLogs;
        }



        /// <summary>
        /// Generate the handover PDF report for a given report view model.
        /// </summary>
        /// <param name="reportViewModel"></param>
        /// <returns></returns>
        private byte[] GenerateHandoverReportPdf(HandoverReportViewModel reportViewModel)
        {
            var converter = new NReco.PdfGenerator.HtmlToPdfConverter
            {
                CustomWkHtmlPageArgs = "--footer-center [page]/[toPage]",
                GenerateToc = true,
                TocHeaderText = "Contents",
            };

            PdfReport pdfReport = new PdfReport
            {
                HtmlCover = RenderViewToString("HandoverReports/Cover", reportViewModel),
                HtmlContent = RenderViewToString("HandoverReports/HandoverReport", reportViewModel),
                PdfConvertor = converter
            };

            return pdfReport.GetReport();
        }


        public class PdfReport
        {
            public string HtmlCover { get; set; }
            public string HtmlContent { get; set; }

            public NReco.PdfGenerator.HtmlToPdfConverter PdfConvertor { get; set; }

            private string Key
            {
                get { return Md5Helper.GetHash(HtmlCover + HtmlContent); }
            }

            private byte[] GeneratePdf()
            {
                var pdf =
                    PdfConvertor.GeneratePdf(HtmlContent, HtmlCover);

                return pdf;
            }

            public byte[] GetReport()
            {
                return CacheHelper.GetCached(Key, GeneratePdf);

            }
        }

        #endregion

        #endregion

        #region Things To Do Report

        #region Action Methods

        /// <summary>
        /// Render the Things To Do reports index view.
        /// </summary>
        public ActionResult ThingsToDoReports(ReportViewModel.SortColumn sortColumn = ReportViewModel.SortColumn.LastName, bool sortAscending = true)
        {
            var packages = GetPackagesForReportsIndexView();
            //  var packages = GetPackages();


            if (packages.Count() == 1 && !packages.First().AncestorPackageId.HasValue && AppSession.Current.ViewAsUser == null)
            {
                return RedirectToAction("ViewThingsToDoReport", new { id = packages.First().ID });
            }

            if (sortColumn == ReportViewModel.SortColumn.LastName)
            {
                packages = packages.OrderByWithDirection(s => s.UserOwner.LastName, sortAscending);
            }
            else if (sortColumn == ReportViewModel.SortColumn.FirstName)
            {
                packages = packages.OrderByWithDirection(s => s.UserOwner.FirstName, sortAscending);
            }
            else if (sortColumn == ReportViewModel.SortColumn.Position)
            {
                packages = packages.OrderByWithDirection(s => s.Position.Name, sortAscending);
            }
            else if (sortColumn == ReportViewModel.SortColumn.Workplace)
            {
                packages = packages.OrderByWithDirection(s => s.Workplace.Name, sortAscending);
            }

            foreach (Package package in packages)
            {
                StringBuilder sb = new StringBuilder();

                sb.Append(package.UserOwner.FirstName);
                sb.Append(" ");
                sb.Append(package.UserOwner.LastName);
                sb.Append(" - ");
                sb.Append(package.Position.Name);
                sb.Append(" - ");
                sb.Append(package.Workplace.Name);
                package.Name = sb.ToString();
            }

            var reportViewModel = new ReportViewModel { Packages = packages, SortAscending = sortAscending };

            return View("ThingsToDoReports/Index", reportViewModel);
        }

        /// <summary>
        /// Render the report web view.
        /// </summary>
        /// <param name="id">The ID of the package.</param>
        /// <param name="ancestor">Generate the package ancestor report.</param>
        public ActionResult ViewThingsToDoReport(Guid id, bool ancestor = false)
        {
            ThingsToDoReportViewModel viewModel = BuildThingsToDoReportViewModel(id, ancestor);

            AppSession.Current.TaskFormReturnUrl = Request.RawUrl;
            AppSession.Current.TopicFormReturnUrl = Request.RawUrl;
            AppSession.Current.VoiceMessageManagerReturnUrl = Request.RawUrl;
            AppSession.Current.AttachmentManagerReturnUrl = Request.RawUrl;
            AppSession.Current.MeetingFormReturnUrl = Request.RawUrl;

            if ((User.IsInRole(RoleAuthHelper.HRManager)
                    || User.IsInRole(RoleAuthHelper.Administrator)
                    || User.IsInRole(RoleAuthHelper.iHandoverAdmin)
                    || User.IsInRole(RoleAuthHelper.iHandoverAgent))
                && HandoverService.IsLeaverPeriodLocked(viewModel.Package.ID))
            {
                ViewBag.Tasklist = true;
            }

            return View("ThingsToDoReports/ThingsToDoReport", viewModel);
        }

        /// <summary>
        /// Download PDF handover report.
        /// </summary>
        /// <param name="id">The ID of the package.</param>
        /// <param name="ancestor">Generate the package ancestor report.</param>
        public ActionResult DownloadThingsToDoReport(Guid id, bool ancestor = false)
        {
            var viewModel = BuildThingsToDoReportViewModel(id, ancestor, true);
            byte[] pdfBytes = GenerateThingsToDoReportPdf(viewModel);
            var filename = viewModel.Package.UserOwner.FullName +
                "-Activate-" + DateTime.Now.ToShortDateString() + ".pdf";
            return File(pdfBytes, "application/pdf", filename);
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Build the Things to do report view model for a given package.
        /// </summary>
        private ThingsToDoReportViewModel BuildThingsToDoReportViewModel(Guid packageId, bool ancestor = false, bool pdf = false)
        {
            bool showAll = ShowAll(GetCurrentPackageId());

            var package = PackageService.GetByPackageId(packageId);

            if (ancestor && package.AncestorPackageId.HasValue)
            {
                package = PackageService.GetByPackageId(package.AncestorPackageId.Value);
            }

            var pickup = package.Handover != null && package.Handover.OnboardingPeriod.Package != null ?
                         package.Handover.OnboardingPeriod.Package.UserOwner :
                         null;

            var isLeavingPeriodLocked = HandoverService.IsLeaverPeriodLocked(package.ID);
            var isOnboardingPeriodLocked = HandoverService.IsOnboarderPeriodLocked(package.ID);

            var graphNodes = RelationshipsCore.GetGraphNodes(package, ContactService.GetCompanies(package.ID), isLeavingPeriodLocked);
            var defaultCompanyId = MasterListService.GetDefaultCompanyId();

            var tasks = GetTasks(package.ID, showAll);

            Guid? onboarderPackageId = null;
            if (package.Handover != null)
            {
                var handover = HandoverService.GetById(package.Handover.ID);
                var onboarderPackage = handover.OnboardingPeriod.Package;
                if (onboarderPackage != null)
                {
                    onboarderPackageId = onboarderPackage.ID;
                }
            }

            DateTime? onboarderReviewDateExperiences = null;
            DateTime? onboarderReviewDateCritical = null;
            DateTime? onboarderReviewDateProjects = null;
            DateTime? onboarderReviewDatePeople = null;
            if (onboarderPackageId.HasValue)
            {
                onboarderReviewDateExperiences = HandoverService.GetOnboarderReviewDateExperiences(onboarderPackageId.Value);
                onboarderReviewDateCritical = HandoverService.GetOnboarderReviewDateCritical(onboarderPackageId.Value);
                onboarderReviewDateProjects = HandoverService.GetOnboarderReviewDateProjects(onboarderPackageId.Value);
                onboarderReviewDatePeople = HandoverService.GetOnboarderReviewDatePeople(onboarderPackageId.Value);
            }

            List<Meeting> meetings;
            List<Guid> meetingTypesFilter = new List<Guid>() { Guid.Parse("E72D15EF-BD12-49AB-B15B-269EDF6A891B") };

            if (package.Handover != null &&
                package.Handover.OnboardingPeriod != null &&
                package.Handover.OnboardingPeriod.Locked.HasValue &&
                package.Handover.OnboardingPeriod.Package != null)
            {
                var packageOnboarder =
                    package.Handover.OnboardingPeriod.Package;

                meetings = MeetingService.GetByPackageId(packageOnboarder.ID, Meeting.SortColumn.Start, true).ToList();
            }
            else
            {
                meetings = MeetingService.GetByPackageId(packageId, Meeting.SortColumn.Name, true,
                    meetingTypeIdsFilter: meetingTypesFilter).ToList();
            }

            return new ThingsToDoReportViewModel
            {
                IsPdf = pdf,
                IsAncestor = ancestor,
                Package = package,
                Pickup = pickup,
                Meetings = meetings,
                GraphNodes = graphNodes,
                DefaultCompanyId = defaultCompanyId,
                Tasks = tasks,
                OnboarderReviewDateExperiences = onboarderReviewDateExperiences,
                OnboarderReviewDateCritical = onboarderReviewDateCritical,
                OnboarderReviewDatePeople = onboarderReviewDatePeople,
                OnboarderReviewDateProjects = onboarderReviewDateProjects,
                IsLeavingPeriodLocked = isLeavingPeriodLocked,
                IsOnboardingPeriodLocked = isOnboardingPeriodLocked
            };
        }

        /// <summary>
        /// Generate the handover PDF report for a given report view model.
        /// </summary>
        /// <param name="reportViewModel"></param>
        /// <returns></returns>
        private byte[] GenerateThingsToDoReportPdf(ThingsToDoReportViewModel reportViewModel)
        {
            var converter = new NReco.PdfGenerator.HtmlToPdfConverter
            {
                CustomWkHtmlPageArgs = "--footer-center [page]/[toPage]",
                GenerateToc = true,
                TocHeaderText = "Contents"
            };

            PdfReport pdfReport = new PdfReport
            {
                HtmlCover = RenderViewToString("ThingsToDoReports/Cover", reportViewModel),
                HtmlContent = RenderViewToString("ThingsToDoReports/ThingsToDoReport", reportViewModel),
                PdfConvertor = converter
            };

            return pdfReport.GetReport();
        }

        #endregion

        #endregion

        #region Subjectivity Report

        #region Action Methods

        [Authorise(Roles = RoleAuthHelper.Reports_SubjectivityReports)]
        public ActionResult SubjectivityReports()
        {
            var packages = GetPackagesForReportsIndexView();

            var reportViewModel = new ReportViewModel { Packages = packages };
            return View("SubjectivityReports/Index", reportViewModel);
        }



        [Authorise(Roles = RoleAuthHelper.Reports_SubjectivityReports)]
        public ActionResult ViewGlobalSubjectivityReport()
        {
            AppSession.Current.TopicFormReturnUrl = Request.RawUrl;
            AppSession.Current.TaskFormReturnUrl = Request.RawUrl;
            SubjectivityReportViewModel subjectivityReportViewModel = new SubjectivityReportViewModel();
            return View("SubjectivityReports/GlobalSubjectivityReport", subjectivityReportViewModel);
        }

        [Authorise(Roles = RoleAuthHelper.Reports_SubjectivityReports)]
        public ActionResult ViewSubjectivityReport()
        {
            SubjectivityReportViewModel subjectivityReportViewModel = BuildSubjectivityReportModel();

            return View("SubjectivityReports/SubjectivityReport", subjectivityReportViewModel);
        }

        [Authorise(Roles = RoleAuthHelper.Reports_SubjectivityReports)]
        public ActionResult DownloadSubjectivityReport()
        {
            SubjectivityReportViewModel subjectivityReportViewModel = BuildSubjectivityReportModel();
            byte[] pdfBytes = GenerateSubjectivityPdf(subjectivityReportViewModel);
            return File(pdfBytes, "application/pdf", "Momentum Plus Subjectivity Report.pdf");
        }

        #endregion

        #region Helper Methods

        public SubjectivityReportViewModel BuildSubjectivityReportModel()
        {
            SubjectivityReportViewModel subjectivityReportViewModel = new SubjectivityReportViewModel();

            subjectivityReportViewModel.IsPdf = false;

            var SubjectiveWords = new List<SubjectiveWordViewModel>();

            foreach (var subjectiveWord in ExtensionMethods.GetSubjectiveWords(MasterListService))
            {
                SubjectiveWords.Add(new SubjectiveWordViewModel
                {
                    Keyword = subjectiveWord,
                    Topics = new List<TopicViewModel>(),
                    Tasks = new List<TaskViewModel>(),
                    People = new List<StaffViewModel>(),
                    Projects = new List<ProjectInstanceViewModel>(),
                    Users = new List<UserViewModel>(),
                    UseCount = 0,
                    NumberOfUsers = 0
                });
            }

            subjectivityReportViewModel.Packages = Mapper.Map<IEnumerable<Package>,
                ICollection<PackageViewModel>>(PackageService.GetAllWithPermissions());

            List<TopicViewModel> allMyTopics = new List<TopicViewModel>();
            List<TaskViewModel> allMyTasks = new List<TaskViewModel>();
            List<StaffViewModel> allMyPeople = new List<StaffViewModel>();
            List<ProjectInstanceViewModel> allMyProjects = new List<ProjectInstanceViewModel>();

            // Count package subjectivity
            foreach (var package in subjectivityReportViewModel.Packages)
            {
                foreach (var topic in TopicService.GetByPackageId(package.ID))
                {
                    var myMappedTopic = Mapper.Map<TopicViewModel>(topic);

                    myMappedTopic.ParentPackageName = package.Name;
                    var mySubjectivity = topic.CountSubjectivity(MasterListService);
                    if (mySubjectivity > 0)
                    {
                        package.SubjectivityCount += mySubjectivity;
                        package.CountTopicsTasksWithSubjectivity++;

                        SubjectiveWords = MapSubjectiveWords(SubjectiveWords, topic, package.UserOwner);
                    }
                    myMappedTopic.Subjectivity = mySubjectivity;

                    package.TotalWordCount += topic.CountWords();
                    package.TotalTopicsTasks++;

                    allMyTopics.Add(myMappedTopic);
                    foreach (var task in TaskService.GetAllByTopicId(topic.ID))
                    {

                        var myMappedTask = Mapper.Map<TaskViewModel>(task);

                        myMappedTask.ParentPackageName = package.Name;
                        mySubjectivity = task.CountSubjectivity(MasterListService);
                        if (mySubjectivity > 0)
                        {
                            package.SubjectivityCount += mySubjectivity;
                            package.CountTopicsTasksWithSubjectivity++;
                            SubjectiveWords = MapSubjectiveWords(SubjectiveWords, task, package.UserOwner);
                        }
                        myMappedTask.Subjectivity = mySubjectivity;

                        package.TotalWordCount += task.CountWords();

                        package.TotalTopicsTasks++;

                        allMyTasks.Add(myMappedTask);
                    }//end foreach task
                }//end foreach topic

                // Count staff (managed people) subjectivity
                foreach (var person in PeopleService.GetStaffsByPackageId(package.ID).ToList())
                {
                    var myMappedStaff = Mapper.Map<StaffViewModel>(person);

                    var mySubjectivity = person.CountSubjectivity(MasterListService);
                    if (mySubjectivity > 0)
                    {
                        package.SubjectivityCount += mySubjectivity;
                        package.CountTopicsTasksWithSubjectivity++;

                        SubjectiveWords = MapSubjectiveWordsPeople(SubjectiveWords, person, package.UserOwner);
                    }
                    myMappedStaff.NotesSubjectivity = mySubjectivity;

                    package.TotalWordCount += person.CountWords();
                    package.TotalTopicsTasks++;

                    myMappedStaff.Package = PackageService.GetByStaffId(myMappedStaff.ID);
                    myMappedStaff.PackageId = myMappedStaff.Package.ID;

                    allMyPeople.Add(myMappedStaff);
                }

                // Count relationship tasks subjectivity
                foreach (var person in ContactService.GetByPackageId(package.ID).ToList())
                {
                    foreach (var task in TaskService.GetAllByContactId(person.ID))
                    {
                        var myMappedTask = Mapper.Map<TaskViewModel>(task);

                        myMappedTask.ParentPackageName = package.Name;
                        var mySubjectivity = task.CountSubjectivity(MasterListService);
                        if (mySubjectivity > 0)
                        {
                            package.SubjectivityCount += mySubjectivity;
                            package.CountTopicsTasksWithSubjectivity++;
                            SubjectiveWords = MapSubjectiveWords(SubjectiveWords, task, package.UserOwner);
                        }
                        myMappedTask.Subjectivity = mySubjectivity;

                        package.TotalWordCount += task.CountWords();

                        package.TotalTopicsTasks++;

                        allMyTasks.Add(myMappedTask);
                    }
                }

                foreach (var project in ProjectService.GetProjectInstances(package.ID).ToList())
                {
                    var myMappedProject = Mapper.Map<ProjectInstanceViewModel>(project);

                    var mySubjectivity = project.CountSubjectivity(MasterListService);
                    if (mySubjectivity > 0)
                    {
                        package.SubjectivityCount += mySubjectivity;
                        package.CountTopicsTasksWithSubjectivity++;

                        SubjectiveWords = MapSubjectiveWords(SubjectiveWords, project, package.UserOwner);
                    }
                    myMappedProject.NotesSubjectivity = mySubjectivity;

                    package.TotalWordCount += project.CountWords();
                    package.TotalTopicsTasks++;

                    allMyProjects.Add(myMappedProject);
                    foreach (var task in TaskService.GetAllByProjectInstanceId(project.ID))
                    {
                        var myMappedTask = Mapper.Map<TaskViewModel>(task);

                        myMappedTask.ParentPackageName = package.Name;
                        mySubjectivity = task.CountSubjectivity(MasterListService);
                        if (mySubjectivity > 0)
                        {
                            package.SubjectivityCount += mySubjectivity;
                            package.CountTopicsTasksWithSubjectivity++;
                            SubjectiveWords = MapSubjectiveWords(SubjectiveWords, task, package.UserOwner);
                        }
                        myMappedTask.Subjectivity = mySubjectivity;

                        package.TotalWordCount += task.CountWords();

                        package.TotalTopicsTasks++;

                        allMyTasks.Add(myMappedTask);
                    }
                }

                package.SubjectivityVsWordCount = PercentageOfNumbers(package.TotalWordCount, package.SubjectivityCount);
                package.SubjectivityTopicsTasks = PercentageOfNumbers(package.TotalTopicsTasks, package.CountTopicsTasksWithSubjectivity);
            }//end foreach package

            subjectivityReportViewModel.Topics = allMyTopics;
            subjectivityReportViewModel.Tasks = allMyTasks;
            subjectivityReportViewModel.People = allMyPeople;
            subjectivityReportViewModel.Projects = allMyProjects;

            var newList = SubjectiveWords.OrderBy(x => x.UseCount).ThenBy(y => y.Keyword).Reverse().Take(50).ToList();
            subjectivityReportViewModel.SubjectiveWords = newList;

            return subjectivityReportViewModel;
        }

        private List<SubjectiveWordViewModel> MapSubjectiveWords(List<SubjectiveWordViewModel> viewModel, Topic topic, User user)
        {
            var subjectiveWords = topic.InspectSubjectivity(MasterListService);
            foreach (var word in subjectiveWords)
            {
                var mySubjWord = viewModel.Find(kw => kw.Keyword.Equals(word));
                var search = mySubjWord.Users.FirstOrDefault(p => p.ID == user.ID);
                if (search == null)
                {
                    mySubjWord.NumberOfUsers++;
                    mySubjWord.Users.Add(Mapper.Map<UserViewModel>(user));
                }
                mySubjWord.Topics.Add(Mapper.Map<TopicViewModel>(topic));
                mySubjWord.UseCount++;
            }
            return viewModel;
        }

        private List<SubjectiveWordViewModel> MapSubjectiveWords(List<SubjectiveWordViewModel> viewModel, Task task, User user)
        {
            var subjectiveWords = task.InspectSubjectivity(MasterListService);
            foreach (var word in subjectiveWords)
            {
                var mySubjWord = viewModel.Find(kw => kw.Keyword.Equals(word));
                var search = mySubjWord.Users.FirstOrDefault(p => p.ID == user.ID);
                if (search == null)
                {
                    mySubjWord.NumberOfUsers++;
                    mySubjWord.Users.Add(Mapper.Map<UserViewModel>(user));
                }
                mySubjWord.Tasks.Add(Mapper.Map<TaskViewModel>(task));
                mySubjWord.UseCount++;
            }
            return viewModel;

        }

        private List<SubjectiveWordViewModel> MapSubjectiveWordsRelationship(List<SubjectiveWordViewModel> viewModel, Contact person, User user)
        {
            var subjectiveWords = person.InspectSubjectivityRelationship(MasterListService);
            foreach (var word in subjectiveWords)
            {
                var mySubjWord = viewModel.Find(kw => kw.Keyword.Equals(word));
                var search = mySubjWord.Users.FirstOrDefault(p => p.ID == user.ID);
                if (search == null)
                {
                    mySubjWord.NumberOfUsers++;
                    mySubjWord.Users.Add(Mapper.Map<UserViewModel>(user));
                }
                mySubjWord.People.Add(Mapper.Map<StaffViewModel>(person));
                mySubjWord.UseCount++;
            }
            return viewModel;
        }

        private List<SubjectiveWordViewModel> MapSubjectiveWordsPeople(List<SubjectiveWordViewModel> viewModel, Contact person, User user)
        {
            var subjectiveWords = person.InspectSubjectivityPeople(MasterListService);
            foreach (var word in subjectiveWords)
            {
                var mySubjWord = viewModel.Find(kw => kw.Keyword.Equals(word));
                var search = mySubjWord.Users.FirstOrDefault(p => p.ID == user.ID);
                if (search == null)
                {
                    mySubjWord.NumberOfUsers++;
                    mySubjWord.Users.Add(Mapper.Map<UserViewModel>(user));
                }
                mySubjWord.People.Add(Mapper.Map<StaffViewModel>(person));
                mySubjWord.UseCount++;
            }
            return viewModel;
        }

        private List<SubjectiveWordViewModel> MapSubjectiveWords(List<SubjectiveWordViewModel> viewModel, ProjectInstance project, User user)
        {
            var subjectiveWords = project.InspectSubjectivity(MasterListService);
            foreach (var word in subjectiveWords)
            {
                var mySubjWord = viewModel.Find(kw => kw.Keyword.Equals(word));
                var search = mySubjWord.Users.FirstOrDefault(p => p.ID == user.ID);
                if (search == null)
                {
                    mySubjWord.NumberOfUsers++;
                    mySubjWord.Users.Add(Mapper.Map<UserViewModel>(user));
                }
                mySubjWord.Projects.Add(Mapper.Map<ProjectInstanceViewModel>(project));
                mySubjWord.UseCount++;
            }
            return viewModel;
        }

        private byte[] GenerateSubjectivityPdf(SubjectivityReportViewModel reportViewModel)
        {
            reportViewModel.IsPdf = true;

            var converter = new NReco.PdfGenerator.HtmlToPdfConverter
            {
                CustomWkHtmlPageArgs = "--footer-center [page]/[toPage]",
                GenerateToc = true,
                TocHeaderText = "Contents",
                Orientation = NReco.PdfGenerator.PageOrientation.Landscape,
                Zoom = 0.8F
            };

            PdfReport pdfReport = new PdfReport
            {
                HtmlContent = RenderViewToString("SubjectivityReports/SubjectivityReport", reportViewModel),
                PdfConvertor = converter
            };


            return pdfReport.GetReport();
        }

        #endregion

        #endregion

        #region Progress Report

        #region Action Methods

        /// <summary>
        /// Ongoing Reports tab/Ongoing Reports list view
        /// </summary>
        /// <param name="sortColumn"></param>
        /// <param name="sortAscending"></param>
        /// <returns></returns>
        public ActionResult ProgressReports(ReportViewModel.SortColumn sortColumn = ReportViewModel.SortColumn.LastName, bool sortAscending = true)
        {
            var packages = GetPackagesForReportsIndexView();

            // Only show In Progress/Ongoing reports
            packages = (from p in packages
                        where p.PackageStatus == Package.Status.InProgress
                        select p);

            if (packages.Count() == 1 && !packages.First().AncestorPackageId.HasValue && AppSession.Current.ViewAsUser == null)
            {
                return RedirectToAction("ViewProgressReport", new { id = packages.First().ID });
            }

            if (sortColumn == ReportViewModel.SortColumn.LastName)
            {
                packages = packages.OrderByWithDirection(s => s.UserOwner.LastName, sortAscending);
            }
            else if (sortColumn == ReportViewModel.SortColumn.FirstName)
            {
                packages = packages.OrderByWithDirection(s => s.UserOwner.FirstName, sortAscending);
            }
            else if (sortColumn == ReportViewModel.SortColumn.Position)
            {
                packages = packages.OrderByWithDirection(s => s.Position.Name, sortAscending);
            }
            else if (sortColumn == ReportViewModel.SortColumn.Workplace)
            {
                packages = packages.OrderByWithDirection(s => s.Workplace.Name, sortAscending);
            }

            var reportViewModel = new ReportViewModel { Packages = packages, SortAscending = sortAscending };
            return View("ProgressReports/Index", reportViewModel);
        }

        public ActionResult ViewProgressReport(Guid id, bool ancestor = false)
        {
            AdditionalReportViewModel reportViewModel = BuildRAGReportModel(id, ancestor);

            AppSession.Current.TaskFormReturnUrl = Request.RawUrl;
            AppSession.Current.TopicFormReturnUrl = Request.RawUrl;
            AppSession.Current.VoiceMessageManagerReturnUrl = Request.RawUrl;
            AppSession.Current.AttachmentManagerReturnUrl = Request.RawUrl;
            AppSession.Current.MeetingFormReturnUrl = Request.RawUrl;

            return View("ProgressReports/ProgressReport", reportViewModel);
        }

        public ActionResult DownloadProgressReport(Guid id, bool ancestor = false)
        {
            AdditionalReportViewModel reportViewModel = BuildRAGReportModel(id, ancestor);
            byte[] pdfBytes = GenerateProgressReportPdf(reportViewModel);
            return File(pdfBytes, "application/pdf", "Momentum Plus Ongoing Report.pdf");
        }

        #endregion

        #region Helper Methods

        private byte[] GenerateProgressReportPdf(AdditionalReportViewModel reportViewModel)
        {
            reportViewModel.IsPdf = true;

            var converter = new NReco.PdfGenerator.HtmlToPdfConverter
            {
                CustomWkHtmlPageArgs = "--footer-center [page]/[toPage]",
                GenerateToc = true,
                TocHeaderText = "Contents",
                Orientation = NReco.PdfGenerator.PageOrientation.Landscape,
                Zoom = 0.8F
            };

            PdfReport pdfReport = new PdfReport
            {
                HtmlContent = RenderViewToString("ProgressReports/ProgressReport", reportViewModel),
                PdfConvertor = converter
            };

            return pdfReport.GetReport();
        }

        #endregion

        #endregion

        #region RAG (Leaver) Report

        #region Action Methods

        public ActionResult RagReports(ReportViewModel.SortColumn sortColumn = ReportViewModel.SortColumn.LastName, bool sortAscending = true)
        {
            var packages = GetPackagesForReportsIndexView();

            if (packages.Count() == 1 && !packages.First().AncestorPackageId.HasValue && AppSession.Current.ViewAsUser == null)
            {
                return RedirectToAction("ViewAdditionalReport", new { id = packages.First().ID });
            }

            // Only triggered packages
            packages = packages.Where(p => p.Handover != null && p.Handover.LeavingPeriod.Locked.HasValue);

            if (sortColumn == ReportViewModel.SortColumn.LastName)
            {
                packages = packages.OrderByWithDirection(s => s.UserOwner.LastName, sortAscending);
            }
            else if (sortColumn == ReportViewModel.SortColumn.FirstName)
            {
                packages = packages.OrderByWithDirection(s => s.UserOwner.FirstName, sortAscending);
            }
            else if (sortColumn == ReportViewModel.SortColumn.Position)
            {
                packages = packages.OrderByWithDirection(s => s.Position.Name, sortAscending);
            }
            else if (sortColumn == ReportViewModel.SortColumn.Workplace)
            {
                packages = packages.OrderByWithDirection(s => s.Workplace.Name, sortAscending);
            }

            var reportViewModel = new ReportViewModel { Packages = packages, SortAscending = sortAscending };
            return View("RAGReports/Index", reportViewModel);
        }



        #endregion

        #region Helper Methods

        public AdditionalReportViewModel BuildRAGReportModel(Guid packageId, bool ancestor, bool isRAG = false)
        {
            AdditionalReportViewModel reportViewModel = new AdditionalReportViewModel { Package = PackageService.GetByPackageId(packageId) };
            var package = PackageService.GetByPackageId(packageId);

            if (ancestor)
            {
                reportViewModel.IsAncestor = true;
                reportViewModel.Pickup = Onboarding.GetPackageOwner(package.ID);
                package = PackageService.GetByPackageId(package.AncestorPackageId.Value);
                reportViewModel.Leaver = Onboarding.GetPackageOwner(package.ID);
            }
            else
            {
                reportViewModel.Leaver = Onboarding.GetPackageOwner(package.ID);
            }

            reportViewModel.Modules = ModuleGather(reportViewModel.Package, reportViewModel, isRAG);
            reportViewModel.Contacts = ContactGather(reportViewModel.Package, reportViewModel);
            reportViewModel.ContactReport = ContactIndividualGather(reportViewModel.Package, reportViewModel);

            reportViewModel.Package.Name = PackageService.GetPackageName(package.ID);

            reportViewModel.People = StaffGather(package.ID, reportViewModel);
            reportViewModel.Projects = ProjectGather(package.ID, reportViewModel);

            if (reportViewModel != null)
            {
                return reportViewModel;
            }
            AddAlert(AlertViewModel.AlertType.Danger, "This package is incomplete");
            return null;
        }

        private ProjectReportViewModel ProjectGather(Guid id, AdditionalReportViewModel reportViewModel)
        {
            try
            {
                double theHandover = 0;
                if (reportViewModel.Package.Handover != null)
                {
                    theHandover = ((reportViewModel.Package.Handover.LeavingPeriod.LastDay ?? DateTime.Now) - DateTime.Now).TotalDays;

                    reportViewModel.HandoverCountDownDaysTotal = (Convert.ToInt32(theHandover) > 0) ? Convert.ToInt32(theHandover) : 0;

                    reportViewModel.HandoverCountdownWeeks = (theHandover > 0) ? Convert.ToInt32(Math.Floor(theHandover / 7)) : 0;
                    reportViewModel.HandoverCountdownDays = (theHandover > 0) ? Convert.ToInt32(theHandover % 7) : 0;
                    reportViewModel.TargetCompletionDate = (reportViewModel.Package.Handover.LeavingPeriod.LastDay ?? DateTime.Now)
                        .AddDays(-5).ToShortDateString();
                }

                ICollection<ProjectInstance> projects = new List<ProjectInstance>();
                foreach (var project in ProjectService.GetProjectInstances(id))
                {
                    projects.Add(project);
                }

                var viewModel = new ProjectReportViewModel();

                foreach (var project in projects)
                {
                    viewModel.ActualTotalWordCount += ExtensionMethods.CountWords(ProjectService.GetProjectInstanceById(project.ID));

                    //Actual Values
                    viewModel.ActualAttachments +=
                        ExtensionMethods.GetNumberOfAttachmentsAndVoiceMessages(ProjectService.GetProjectInstanceById(project.ID));

                    viewModel.ActualVolume += project.Tasks.Count();

                    viewModel.ActualApproved +=
                        ExtensionMethods.CountItemsApproved(ProjectService.GetProjectInstanceById(project.ID));
                }

                viewModel.ActualVolume += ProjectService.GetProjectInstances(id).Count();
                if (viewModel.ActualVolume > 0)
                {
                    viewModel.ActualAverageWordCount = (viewModel.ActualTotalWordCount / viewModel.ActualVolume);
                }
                else
                {
                    viewModel.ActualAverageWordCount = 0;
                }

                // Target Values 
                viewModel.TargetAttachments = reportViewModel.TargetAttachments;
                viewModel.TargetAverageWordCount = reportViewModel.TargetAverageWordCount;
                viewModel.TargetVolume = reportViewModel.TargetVolume;
                viewModel.TargetCompletionDate = reportViewModel.TargetCompletionDate;

                //RAG Valuez
                viewModel.RagAttachments = SetRagStatus(viewModel.TargetAttachments, viewModel.ActualAttachments);
                viewModel.RagVolume = SetRagStatus(viewModel.TargetVolume, viewModel.ActualVolume);
                viewModel.RagWordCount = SetRagStatus(viewModel.TargetAverageWordCount, viewModel.ActualAverageWordCount);
                viewModel.RagApproved = SetRagStatus(viewModel.ActualVolume, viewModel.ActualApproved);

                //Percent Values
                viewModel.PercentageAttachments = SetPercentageComplete(viewModel.TargetAttachments, viewModel.ActualAttachments);
                viewModel.PercentageVolume = SetPercentageComplete(viewModel.TargetVolume, viewModel.ActualVolume);
                viewModel.PercentageWordCount = SetPercentageComplete(viewModel.TargetAverageWordCount, viewModel.ActualAverageWordCount);
                viewModel.PercentageApproved = SetPercentageComplete(viewModel.ActualVolume, viewModel.ActualApproved);

                viewModel.Modified = ProjectService.GetDateLastEntry(id);
                viewModel.UserLastViewed = PackageService.GetByPackageId(id).ProjectsLastViewed;

                return viewModel;
            }
            catch (Exception e)
            {
                LogException(e);
                AddAlert(AlertViewModel.AlertType.Danger, "Error creating report.");
                return null;
            }
        }

        public StaffReportViewModel StaffGather(Guid id, AdditionalReportViewModel reportViewModel)
        {
            try
            {
                double theHandover = 0;
                if (reportViewModel.Package.Handover != null)
                {
                    theHandover = ((reportViewModel.Package.Handover.LeavingPeriod.LastDay ?? DateTime.Now) - DateTime.Now).TotalDays;

                    reportViewModel.HandoverCountDownDaysTotal = (Convert.ToInt32(theHandover) > 0) ? Convert.ToInt32(theHandover) : 0;

                    reportViewModel.HandoverCountdownWeeks = (theHandover > 0) ? Convert.ToInt32(Math.Floor(theHandover / 7)) : 0;
                    reportViewModel.HandoverCountdownDays = (theHandover > 0) ? Convert.ToInt32(theHandover % 7) : 0;
                    reportViewModel.TargetCompletionDate = (reportViewModel.Package.Handover.LeavingPeriod.LastDay ?? DateTime.Now)
                        .AddDays(-5).ToShortDateString();
                }

                ICollection<Contact> people = new List<Contact>();
                foreach (var person in PeopleService.GetStaffsByPackageId(id))
                {
                    people.Add(person);
                }

                var viewModel = new StaffReportViewModel();

                foreach (var person in people)
                {
                    viewModel.ActualTotalWordCount += ExtensionMethods.CountWords(PeopleService.GetStaff(person.ID));

                    //Actual Values
                    viewModel.ActualAttachments +=
                        ExtensionMethods.GetNumberOfAttachmentsAndVoiceMessages(PeopleService.GetStaff(person.ID));
                }

                viewModel.ActualApproved = PeopleService.CountStaffsApproved(id);
                viewModel.ActualVolume = PeopleService.GetStaffsByPackageId(id).Count();
                if (viewModel.ActualVolume > 0)
                {
                    viewModel.ActualAverageWordCount = (viewModel.ActualTotalWordCount / viewModel.ActualVolume);
                }
                else
                {
                    viewModel.ActualAverageWordCount = 0;
                }

                // Target Values 
                viewModel.TargetAttachments = reportViewModel.TargetAttachments;
                viewModel.TargetAverageWordCount = reportViewModel.TargetAverageWordCount;
                viewModel.TargetVolume = reportViewModel.TargetVolume;
                viewModel.TargetCompletionDate = reportViewModel.TargetCompletionDate;

                //RAG Valuez
                viewModel.RagAttachments = SetRagStatus(viewModel.TargetAttachments, viewModel.ActualAttachments);
                viewModel.RagVolume = SetRagStatus(viewModel.TargetVolume, viewModel.ActualVolume);
                viewModel.RagWordCount = SetRagStatus(viewModel.TargetAverageWordCount, viewModel.ActualAverageWordCount);
                viewModel.RagApproved = SetRagStatus(viewModel.ActualVolume, viewModel.ActualApproved);

                //Percent Values
                viewModel.PercentageAttachments = SetPercentageComplete(viewModel.TargetAttachments, viewModel.ActualAttachments);
                viewModel.PercentageVolume = SetPercentageComplete(viewModel.TargetVolume, viewModel.ActualVolume);
                viewModel.PercentageWordCount = SetPercentageComplete(viewModel.TargetAverageWordCount, viewModel.ActualAverageWordCount);
                viewModel.PercentageApproved = SetPercentageComplete(viewModel.ActualVolume, viewModel.ActualApproved);

                viewModel.Modified = PeopleService.GetDateLastEntry(id);
                viewModel.UserLastViewed = PackageService.GetByPackageId(id).PeopleLastViewed;

                return viewModel;
            }
            catch (Exception e)
            {
                LogException(e);
                AddAlert(AlertViewModel.AlertType.Danger, "Error creating report.");
                return null;
            }
        }

        private string SetRagStatus(int target, int actual, int upperBound = 70, int lowerBound = 30)
        {
            string RagStatus = "rag-green";
            var actualDivTarget = (double)actual / (double)target;
            var percentageOfTarget = (actualDivTarget) * 100;
            if (actual < target)
            {
                if (percentageOfTarget <= upperBound)
                {
                    RagStatus = "rag-amber";
                    if (percentageOfTarget <= lowerBound)
                    {
                        RagStatus = "rag-red";
                    }
                }
            }
            if (target == 0)
            {
                //Approved can be 0, appears as green without this check
                RagStatus = "rag-red";
            }
            return RagStatus;
        }

        #endregion

        #endregion

        #region Document Report

        #region Action Methods

        [Authorise(Roles = RoleAuthHelper.Reports_SubjectivityReports)]
        public ActionResult DocumentLibraryReports()
        {
            var currentUser = GetCurrentUser();
            var documents = DocumentLibraryService.GetAll();

            var reportViewModel = new DocumentReportViewModel();
            reportViewModel.Documents = Mapper.Map<IEnumerable<DocumentLibrary>,
            ICollection<DocumentLibraryFormViewModel>>(documents);

            AppSession.Current.DocumentReportReturnUrl = Request.RawUrl;

            return View("DocumentLibraryReports/Index", reportViewModel);
        }

        [Authorise(Roles = RoleAuthHelper.Reports_SubjectivityReports)]
        public ActionResult ViewDocumentLibraryReport(Guid id)
        {
            DocumentLibraryFormViewModel reportViewModel = BuildDocumentReportModel(id);

            AppSession.Current.TaskFormReturnUrl = Request.RawUrl;
            AppSession.Current.TopicFormReturnUrl = Request.RawUrl;
            AppSession.Current.VoiceMessageManagerReturnUrl = Request.RawUrl;
            AppSession.Current.AttachmentManagerReturnUrl = Request.RawUrl;
            AppSession.Current.MeetingFormReturnUrl = Request.RawUrl;

            return View("DocumentLibraryReports/Report", reportViewModel);
        }

        #endregion

        #region Helper Methods

        private DocumentLibraryFormViewModel BuildDocumentReportModel(Guid id)
        {
            var document = DocumentLibraryService.GetByID(id);

            DocumentLibraryFormViewModel viewModel = Mapper.Map<DocumentLibrary, DocumentLibraryFormViewModel>(document);

            foreach (var task in viewModel.Tasks)
            {
                var package = PackageService.GetByTaskId(task.ID);

                if (package != null)
                {
                    task.UserOwnerName = package.UserOwner.FirstName + " " + package.UserOwner.LastName;
                }
            }

            foreach (var contact in viewModel.Contacts)
            {
                var package = PackageService.GetByContactId(contact.ID);

                if (package != null)
                {

                    contact.UserOwnerName = package.UserOwner.FirstName + " " + package.UserOwner.LastName;
                }
            }

            foreach (var meeting in viewModel.Meetings)
            {
                var package = PackageService.GetByTopicId(meeting.ID);

                if (package != null)
                {
                    meeting.UserOwnerName = package.UserOwner.FirstName + " " + package.UserOwner.LastName;
                }
            }

            foreach (var topic in viewModel.Topics)
            {
                var package = PackageService.GetByTopicId(topic.ID);

                if (package != null)
                {
                    topic.UserOwnerName = package.UserOwner.FirstName + " " + package.UserOwner.LastName;
                }
            }

            //foreach (var trigger in viewModel.Triggers)
            //{
            //    var package = PackageService.GetByTriggerId(trigger.ID);

            //    if (package != null)
            //    {
            //       trigger.UserOwnerName = package.UserOwner.FirstName + " " + package.UserOwner.LastName;
            //    }
            //}

            foreach (var instance in viewModel.ProjectInstances)
            {
                var package = PackageService.GetByProjectInstanceId(instance.ID);

                if (package != null)
                {
                    instance.UserOwnerName = package.UserOwner.FirstName + " " + package.UserOwner.LastName;
                }
            }

            foreach (var person in viewModel.Staffs)
            {
                var package = PackageService.GetByStaffId(person.ID);

                if (package != null)
                {
                    person.UserOwnerName = package.UserOwner.FirstName + " " + package.UserOwner.LastName;
                }
            }

            return viewModel;
        }

        #endregion

        #endregion

        #region Helpers

        private IEnumerable<Package> GetPackagesForReportsIndexView()
        {
            var packages = PackageService.GetAllPackagesByUser(AppSession.Current.ViewAsUser);
            var currentUserPackages = PackageService.GetPackagesByUserId(AppSession.Current.ViewAsUser.ID);
            return packages.Union(currentUserPackages).OrderBy(s => s.UserOwner.FirstName);
        }

        #endregion
    }
}
