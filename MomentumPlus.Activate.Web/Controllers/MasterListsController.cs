﻿using System.Web.Mvc;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Helpers;

namespace MomentumPlus.Activate.Web.Controllers
{
    [Authorise(Roles = RoleAuthHelper.MasterLists_Controller_General)]
    public class MasterListsController : BaseController
    {
        public ActionResult Index()
        {
            return RedirectToAction(
                "Index",
                "Systems",
                new { area = "Framework" }
            );
        }

    }
}
