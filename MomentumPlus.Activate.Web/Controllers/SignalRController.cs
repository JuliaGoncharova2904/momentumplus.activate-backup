﻿using MomentumPlus.Activate.Services.Interfaces;

namespace MomentumPlus.Activate.Web.Controllers
{
    public class SignalRController
    {
        #region IOC

        private IVoiceMessageService VoiceMessageService;

        public SignalRController(
            IVoiceMessageService voiceMessageService
        )
        {
            VoiceMessageService = voiceMessageService;
        }

        #endregion
    }
}