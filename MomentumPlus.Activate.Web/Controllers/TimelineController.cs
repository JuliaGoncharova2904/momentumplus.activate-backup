﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using MomentumPlus.Activate.Services.Helpers;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.Handover;

namespace MomentumPlus.Activate.Web.Controllers
{
    public class TimelineController : BaseController
    {
        #region IOC

        private readonly IPackageService PackageService;
        private readonly IMeetingService MeetingService;
        private readonly IAccountConfigService AccountConfigService;
        private readonly IHandoverService HandoverService;

        public TimelineController(IMeetingService meetingService, IMasterListService masterListService,
            IPackageService packageService, IAuditService auditService, IAccountConfigService accountConfigService, IHandoverService handoverService)
        {
            MeetingService =
                meetingService;

            PackageService =
                packageService;

            AuditService =
                auditService;

            AccountConfigService =
                accountConfigService;

            HandoverService =
                handoverService;
        }

        #endregion


        //[Authorise(Roles = RoleAuthHelper.Timeline_Controller)]
        //public ActionResult Index()
        //{
        //    return RedirectToAction("Ongoing");
        //}

        [Authorise(Roles = RoleAuthHelper.Timeline_Controller)]
        public ActionResult Ongoing()
        {
            List<TimelineViewModel> meetings = new List<TimelineViewModel>();
            IEnumerable<Package> packages = PackageService.GetByPackageStatus(Package.Status.InProgress).ToList();
            TimelineViewModel timeline;

            foreach (var package in packages)
            {
                timeline = PopulateTimeline(package.ID);
                Ongoing(timeline);
                meetings.Add(timeline);
            }

            IEnumerable<TimelineViewModel> model = meetings;

            AppSession.Current.PackageExamineReturnUrl = Request.RawUrl;
            AppSession.Current.MeetingFormReturnUrl = Request.RawUrl;

            ViewBag.hideTimelineActionControl = false;

            return View("~/Views/Timeline/Ongoing.cshtml", model);
        }

        [Authorise(Roles = RoleAuthHelper.Timeline_Controller)]
        public ActionResult Handover()
        {
            var timelines = new List<TimelineViewModel>();
            var handovers = HandoverService.GetActiveHandovers();

            foreach (var handover in handovers)
            {
                var leaver = PopulateTimeline(handover.Package.ID);
                Leaver(leaver, handover);
                timelines.Add(leaver);

                if (!handover.OnboardingPeriod.Locked.HasValue) continue;
                var onboarder = PopulateTimeline(HandoverService.IsOnboarding(handover.OnboardingPeriod.PackageId.Value) ? handover.OnboardingPeriod.Package.ID : handover.OnboardingPeriod.PackageId.Value);
                Onboarder(onboarder);
                timelines.Add(onboarder);
            }

            IEnumerable<TimelineViewModel> model = timelines;

            AppSession.Current.PackageExamineReturnUrl = Request.RawUrl;
            AppSession.Current.MeetingFormReturnUrl = Request.RawUrl;

            ViewBag.hideTimelineActionControl = false;
            
            return View("~/Views/Timeline/Handover.cshtml", model);
        }

        // GET: Timeline
        public ActionResult GetTimeline(Guid packageId)
        {
            Package package = PackageService.GetByPackageId(packageId);
            TimelineViewModel model;
            model = PopulateTimeline(packageId);

            if (HandoverService.IsLeaverPeriodLocked(packageId))
            {
                Leaver(model, package.Handover);
            }
            else if (HandoverService.IsOnboarding(packageId))
            {
                Onboarder(model);
            }
            else
            {
                Ongoing(model);
            }

            ViewBag.hideTimelineActionControl = true;

            model.TimelineOwner = string.Empty;

            return View("~/Views/Shared/_Timeline.cshtml", model);
        }

        private TimelineViewModel PopulateTimeline(Guid packageId)
        {
            Package package = PackageService.GetByPackageId(packageId);
            var timeline = new TimelineViewModel();

            timeline.ID = packageId;
            timeline.AncestorId = PackageService.GetByPackageId(packageId).AncestorPackageId;
            timeline.Title = package.DisplayName;
            timeline.TimelineOwner = package.UserOwner.FullName;

            // Set the URLs 
            timeline.Download = "/Home/Meetings/DownloadAllVCalenders";
            timeline.CreateMeeting = "/Home/Relationships/CreateMeetingForPackage";
            timeline.ViewMeeting = "/Home/Meetings/EditMeeting";

            PopulateMeetings(timeline);

            return timeline;
        }

        private void PopulateMeetings(TimelineViewModel timeline)
        {
            Mapper.CreateMap<Meeting, TimelineMeetingViewModel>();
            var meetings = MeetingService.GetByPackageId(timeline.ID).Where(m => m.Start != null);
            timeline.Events = new Dictionary<string, IEnumerable<TimelineMeetingViewModel>>();
            var events = Mapper.Map<IEnumerable<TimelineMeetingViewModel>>(meetings);
            var dates = new List<string>();
            // Find the unique dates
            foreach (var eventItem in events)
            {
                if (!dates.Contains(eventItem.Start.ToShortDateString()))
                {
                    dates.Add(eventItem.Start.ToShortDateString());
                }
            }

            // Attach the events to their corresponding dates
            var eventsCollection = new List<TimelineMeetingViewModel>();
            foreach (var date in dates)
            {
                foreach (var eventItem in events.Where(e => e.Start.ToShortDateString().Equals(date)).OrderBy(e => e.Start))
                {
                    TimelineMeetingViewModel mappedEvent = Mapper.Map<TimelineMeetingViewModel>(eventItem);
                    eventsCollection.Add(mappedEvent);
                }
                timeline.Events.Add(date, eventsCollection);
            }
        }

        private void Onboarder(TimelineViewModel timeline)
        {
            timeline.StartDate = Convert.ToDateTime(HandoverService.GetOnboardingPeriodForPackage(timeline.ID).StartPoint);
            timeline.EndDate = Convert.ToDateTime(HandoverService.GetOnboardingPeriodForPackage(timeline.ID).LastDay);
            timeline.Status = Package.TimelineStatus.Onboarder.ToString();
            timeline.Report = "/Reports/ViewHandoverReport";
            timeline.Title += " (Onboarding)";
            if (timeline.StartDate > SystemTime.Now)
            {
                timeline.Title += " - Pending";
            }
            if (timeline.EndDate < SystemTime.Now)
            {
                timeline.Title += " - Complete";
            }
        }

        private void Leaver(TimelineViewModel timeline, Handover handover)
        {
            timeline.StartDate = Convert.ToDateTime(handover.LeavingPeriod.StartPoint);
            timeline.EndDate = Convert.ToDateTime(handover.LeavingPeriod.LastDay);
            timeline.Status = Package.TimelineStatus.Leaver.ToString();
            timeline.Report = "/Reports/ViewHandoverReport";
            if (timeline.EndDate <= SystemTime.Now)
            {
                timeline.Title += " (Leaving) - Complete";
            }
            else if (timeline.StartDate > SystemTime.Now)
            {
                timeline.Title += " (Leaving) - Pending";
            }
            else
            {
                timeline.Title += " (Leaving)";
            }
        }

        private void Ongoing(TimelineViewModel timeline)
        {
            // Calculate the start and ends dates of the current fiscal year 
            var account = AccountConfigService.Get();
            var diff = SystemTime.Now.Month - account.FiscalYearStartMonth;

            if (diff >= 0)
            {
                timeline.StartDate = new DateTime(SystemTime.Now.Year, account.FiscalYearStartMonth, 1);
            }
            else
            {
                timeline.StartDate = new DateTime(SystemTime.Now.Year - 1, account.FiscalYearStartMonth, 1);
            }

            timeline.EndDate = timeline.StartDate.AddYears(1).AddDays(-1);
            timeline.Status = Package.TimelineStatus.Ongoing.ToString();
        }
    }
}