﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Helpers;

namespace MomentumPlus.Activate.Web.Controllers
{
    public class RequestController : BaseController
    {
        #region IOC

        private IRequestService RequestService;

        public RequestController(IRequestService requestService)
        {
            RequestService = requestService;
        }

        #endregion

        public ActionResult CancelRequest(Guid id)
        {
            try
            {
                RequestService.DeleteRequest(id);
                AddAlert(Models.AlertViewModel.AlertType.Success, SaveSuccessMessage);
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(Models.AlertViewModel.AlertType.Success, SaveErrorMessage);
            }

            return Redirect(AppSession.Current.CancelRequestReturnUrl);
        }
    }
}