﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;
using NLog;
using MomentumPlus.Activate.Web.Models.Moderation;
using MomentumPlus.Activate.Web.Models.People;
using MvcSiteMapProvider;
using MvcSiteMapProvider.Collections.Specialized;
using MomentumPlus.Core.Models.Moderation;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.People;

namespace MomentumPlus.Activate.Web.Controllers
{
    /// <summary>
    /// Base controller that all other controllers
    /// in the application are to inherit from.
    /// </summary>
    [Authorise]
    public abstract class BaseController : Controller
    {
        private static Logger Logger = LogManager.GetCurrentClassLogger();

        protected string SaveErrorMessage = "Unable to save, if this error persists, please contact info@ihandover.co";
        protected string SaveSuccessMessage = "Saved/Updated Successfully";

        #region IOC

        public IAuditService AuditService { get; set; }
        public IMasterListService MasterListService { get; set; }
        public IPackageService PackageService { get; set; }
        public IUserService UserService { get; set; }

        //Needs to be public for DI to work
        public IFlaggingService FlaggingService { get; set; }
        public IHandoverService _HandoverService { get; set; }

        protected BaseController() { }

        protected BaseController(
            IAuditService auditService,
            IMasterListService masterListService,
            IPackageService packageService,
            IUserService userService
        )
        {
            AuditService = auditService;
            MasterListService = masterListService;
            PackageService = packageService;
            UserService = userService;
        }

        #endregion

        #region EVENTS

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            SetPackageFlagging();
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (!filterContext.IsChildAction)
            {
                ViewBag.CurrUser = GetCurrentUser();
            }
            base.OnActionExecuted(filterContext);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            LogException(filterContext.Exception);
            base.OnException(filterContext);
        }

        #endregion

        #region HELPERS

        #region Users

        /// <summary>
        /// Get the currently logged in user
        /// </summary>
        /// <returns></returns>
        protected User GetCurrentUser()
        {
            return UserService.GetByUsername(HttpContext.User.Identity.Name);
        }

        #endregion

        #region Packages

        /// <summary>
        /// Get the ID of the package being viewed.
        /// </summary>
        protected Guid? GetCurrentPackageId()
        {
            if (AppSession.Current.ExaminePackageId.HasValue)
            {
                CheckAndSetPackagePermission(AppSession.Current.ExaminePackageId);
                return AppSession.Current.ExaminePackageId;
            }

            if (!AppSession.Current.CurrentPackageId.HasValue)
            {
                var currentUser = GetCurrentUser();

                if (currentUser == null || PackageService == null)
                    return null;

                var packages = PackageService.GetPackagesByUserId(currentUser.ID);

                var package = packages.FirstOrDefault();

                if (package != null)
                {
                    CheckAndSetPackagePermission(package.ID);
                    SetCurrentPackage(package);
                }
            }


            CheckAndSetPackagePermission(AppSession.Current.CurrentPackageId);
            return AppSession.Current.CurrentPackageId;
        }
        /// <summary>
        /// Get package by user id
        /// </summary>
        /// <returns></returns>
        protected Package GetPackage(Guid UserID)
        {
            var packageObj = PackageService.GetByUserOwnerId(UserID);
            var package = packageObj.FirstOrDefault();
            if (package.ID != null)
            {
                return PackageService.GetByPackageId(package.ID);
            }

            return null;
        }
        /// <summary>
        /// Get the current package
        /// </summary>
        /// <returns></returns>
        protected Package GetCurrentPackage()
        {
            var packageId = GetCurrentPackageId();

            if (packageId.HasValue)
            {
                return PackageService.GetByPackageId(packageId.Value);
            }

            return null;
        }

        /// <summary>
        /// Check Package Permission based on package Template
        /// </summary>
        /// <returns></returns>
        protected void CheckAndSetPackagePermission(Guid? packageId)
        {
            if (packageId.HasValue)
            {
                var package = PackageService.GetByPackageId(packageId.Value);

                if (package != null)
                {
                    var packagePosition = package.Position;
                    AppSession.Current.ExitSurveyEnabled = (package.ExitSurvey.HasValue && package.ExitSurveyTemplateId.HasValue ? true : false);
                    if (packagePosition != null && !packagePosition.Deleted.HasValue)
                    {
                        var positionTemplate = packagePosition.Template;

                        if (positionTemplate != null && !positionTemplate.Deleted.HasValue)
                        {
                            AppSession.Current.ExperiencesEnabled = positionTemplate.ExperiencesEnabled;
                            AppSession.Current.ProjectsEnabled = positionTemplate.ProjectsEnabled;
                            AppSession.Current.PeopleManagementEnabled = positionTemplate.PeopleManagementEnabled;
                            
                        }
                        else
                        {
                            AppSession.Current.ProjectsEnabled = package.ProjectsEnabled;
                            AppSession.Current.PeopleManagementEnabled = package.PeopleManagementEnabled;
                            AppSession.Current.CurrentPackageHasAncestor = package.AncestorPackageId.HasValue;
                        }
                    }
                }
            }

        }




        /// <summary>
        /// Set the current package on the session
        /// </summary>
        /// <param name="package"></param>
        protected void SetCurrentPackage(Package package)
        {
            if (package != null)
            {
                AppSession.Current.CurrentPackageId = package.ID;
                //AppSession.Current.ExperiencesEnabled = package.ExperiencesEnabled;
                AppSession.Current.RelationshipsEnabled = true;
                //AppSession.Current.ProjectsEnabled = package.ProjectsEnabled;
                //AppSession.Current.PeopleManagementEnabled = package.PeopleManagementEnabled;
                AppSession.Current.CurrentPackageHasAncestor = package.AncestorPackageId.HasValue;
            }
        }

        /// <summary>
        /// Set the flags on the sitemap nodes for the current package
        /// </summary>
        private void SetPackageFlagging()
        {
            var packageId = GetCurrentPackageId();
            //if (packageId.HasValue && _HandoverService.IsLeaverPeriodLocked(packageId.Value))
            if (packageId.HasValue)
            {
                ISiteMapNodeCollection childNodes = null;
                var packageNode = SiteMaps.Current.RootNode.ChildNodes.FirstOrDefault(n => n.Title == "My Pack");
                if (packageNode != null)
                    childNodes = packageNode.ChildNodes;

                if (!ControllerContext.IsChildAction && childNodes != null && childNodes.Any())
                {
                    foreach (var node in childNodes)
                    {
                        var flag = GetFlagForNode(node, packageId.Value);
                        if (flag != null)
                        {
                            node.Attributes["flag"] = flag;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Get the flag to display in the navigation for a given sitemap node
        /// </summary>
        /// <param name="node"></param>
        /// <param name="packageId"></param>
        /// <returns></returns>
        public PackageFlag GetFlagForNode(ISiteMapNode node, Guid packageId)
        {
            PackageFlag flag = new PackageFlag();
            Role.RoleEnum userOwnerRole = GetCurrentPackage().UserOwner.Role.RoleParsed;
            bool isAdmin = userOwnerRole == Role.RoleEnum.Administrator || userOwnerRole == Role.RoleEnum.iHandoverAdmin;

            if (!node.Attributes.ContainsKey("flag"))
            {
                switch (node.Controller)
                {
                    case "Onboarding":
                        flag = FlaggingService.GetOnboardingModuleFlag(packageId, isAdmin);
                        break;
                    case "Critical":
                        flag = FlaggingService.GetPackageFlagForCriticalModule(packageId, isAdmin);
                        break;
                    case "Experiences":
                        flag = FlaggingService.GetExperiencesModuleFlag(packageId, isAdmin);
                        break;
                    case "Relationships":
                        flag = FlaggingService.GetRelationshipsModuleFlag(packageId, isAdmin);
                        break;
                    case "Projects":
                        flag = FlaggingService.GetProjectsModuleFlag(packageId, isAdmin);
                        break;
                    case "People":
                        flag = FlaggingService.GetPeopleModuleFlag(packageId, isAdmin);
                        break;
                    case "Tasks":
                        flag = FlaggingService.GetTasksModuleFlag(packageId, isAdmin);
                        break;
                    case "ExitSurvey":
                        flag = FlaggingService.GetExitSurveyModuleFlag(packageId, isAdmin);
                        break;
                }
                var package = PackageService.GetByPackageId(packageId);
                if (_HandoverService.IsOnboarding(packageId))
                {
                    flag.PackageStatus = "Onboarding";
                }
                else if (_HandoverService.IsLeaving(packageId))
                {
                    flag.PackageStatus = "Leaving";
                }
                else if (package != null && package.Deleted.HasValue)
                {
                    flag.PackageStatus = "Retired";
                }
                else if (_HandoverService.HasLeft(packageId))
                {
                    flag.PackageStatus = "Finalise";
                }
                else
                {
                    flag.PackageStatus = "Ongoing";
                }
            }

            return flag;
        }

        #endregion

        #region Moderation

        /// <summary>
        /// Set the moderation fields on a view model from the given moderated entity and package
        /// </summary>
        public void SetModeratedViewModelFields(
            Moderated moderated,
            ModeratedViewModel viewModel,
            Package package
        )
        {
            var user = GetCurrentUser();
            var moderateList = moderated.Moderations.OrderBy(m => m.Created);

            viewModel.UserIsClientAdmin = user.Role.RoleParsed == Role.RoleEnum.Administrator ||
                user.Role.RoleParsed == Role.RoleEnum.iHandoverAdmin ||
                user.Role.RoleParsed == Role.RoleEnum.iHandoverAgent;

            viewModel.UserIsHrManager = user.ID == package.UserHRManagerId;
            viewModel.UserIsLineManager = user.ID == package.UserLineManagerId;

            if (moderated.Moderations == null || !moderated.Moderations.Any())
            {
                return;
            }

            var lineManagerModeration = moderateList.LastOrDefault(m =>
                m.OnBehalfOfRoleId == package.UserLineManager.RoleId
            );

            if (lineManagerModeration != null)
            {
                viewModel.LineManagerDecision = lineManagerModeration.Decision;
                viewModel.LineManagerNotes = lineManagerModeration.Notes;
            }

            var hrManagerModeration = moderateList.LastOrDefault(m =>
                m.OnBehalfOfRoleId == package.UserHRManager.RoleId
            );

            if (hrManagerModeration != null)
            {
                viewModel.HrManagerDecision = hrManagerModeration.Decision;
                viewModel.HrManagerNotes = hrManagerModeration.Notes;
            }
        }

        /// <summary>
        /// Set the moderation fields on a moderated entity from the given view model and package
        /// </summary>
        public void UpdateModerated(
            Moderated moderated,
            ModeratedViewModel viewModel,
            Package package
        )
        {
            var user = GetCurrentUser();

            moderated.Moderations = moderated.Moderations ?? new List<Moderation>();

            IEnumerable<Moderation> moderations = moderated.Moderations.ToList().OrderBy(m => m.Created);

            if (user.Role.RoleParsed == Role.RoleEnum.Administrator ||
                user.Role.RoleParsed == Role.RoleEnum.iHandoverAdmin ||
                user.Role.RoleParsed == Role.RoleEnum.iHandoverAgent ||
                user.Role.RoleParsed == Role.RoleEnum.HRManager ||
                user.Role.RoleParsed == Role.RoleEnum.LineManager)
            {

                if (viewModel.LineManagerDecision.HasValue)
                {

                    var moderation = moderations.LastOrDefault(m => m.OnBehalfOfRoleId == package.UserLineManager.RoleId)
                        ?? new Moderation()
                        {
                            UserId = user.ID,
                            CurrentRoleId = user.RoleId.Value
                        };

                    moderation.Decision = viewModel.LineManagerDecision.Value;
                    moderation.Notes = viewModel.LineManagerNotes;
                    moderation.OnBehalfOfRoleId = package.UserLineManager.RoleId;
                    moderation.Created = DateTime.Now;

                    if (moderated.Moderations.All(m => m.OnBehalfOfRoleId != moderation.OnBehalfOfRoleId))
                    {
                        moderated.Moderations.Add(moderation);
                    }
                }

                if (viewModel.HrManagerDecision.HasValue)
                {
                    var moderation = moderations.LastOrDefault(m => m.OnBehalfOfRoleId == package.UserHRManager.RoleId)
                        ?? new Moderation()
                        {
                            UserId = user.ID,
                            CurrentRoleId = user.RoleId.Value
                        };

                    moderation.Decision = viewModel.HrManagerDecision.Value;
                    moderation.Notes = viewModel.HrManagerNotes;
                    moderation.OnBehalfOfRoleId = package.UserHRManager.RoleId;
                    moderation.Created = DateTime.Now;

                    if (moderated.Moderations.All(m => m.OnBehalfOfRoleId != moderation.OnBehalfOfRoleId))
                    {
                        moderated.Moderations.Add(moderation);
                    }
                }
            }
        }

        #region Staff Moderation

        /// <summary>
        /// Set the moderation fields on a staff view model from the given contact and package
        /// </summary>
        /// <param name="moderated"></param>
        /// <param name="viewModel"></param>
        /// <param name="package"></param>
        public void SetModeratedViewModelFields(
             Contact moderated,
             StaffViewModel viewModel,
             Package package
         )
        {
            var user = GetCurrentUser();

            viewModel.UserIsClientAdmin = user.Role.RoleParsed == Role.RoleEnum.Administrator ||
                user.Role.RoleParsed == Role.RoleEnum.iHandoverAdmin;

            viewModel.UserIsHrManager = user.ID == package.UserHRManagerId;
            viewModel.UserIsLineManager = user.ID == package.UserLineManagerId;

            if (moderated.Moderations == null || !moderated.Moderations.Any())
            {
                return;
            }

            var lineManagerModeration = moderated.Moderations.FirstOrDefault(m =>
                m.OnBehalfOfRole != null && m.OnBehalfOfRole.RoleParsed == Role.RoleEnum.LineManager
            );

            if (lineManagerModeration != null)
            {
                viewModel.LineManagerDecision = lineManagerModeration.Decision;
                viewModel.LineManagerNotes = lineManagerModeration.Notes;
            }

            var hrManagerModeration = moderated.Moderations.FirstOrDefault(m =>
                m.OnBehalfOfRole != null && m.OnBehalfOfRole.RoleParsed == Role.RoleEnum.HRManager
            );

            if (hrManagerModeration != null)
            {
                viewModel.HrManagerDecision = hrManagerModeration.Decision;
                viewModel.HrManagerNotes = hrManagerModeration.Notes;
            }
        }

        /// <summary>
        /// Update the moderation fields on a contact from a staff view model and package
        /// </summary>
        /// <param name="moderated"></param>
        /// <param name="viewModel"></param>
        /// <param name="package"></param>
        public void UpdateModerated(
            Contact moderated,
            StaffViewModel viewModel,
            Package package
        )
        {
            var user = GetCurrentUser();

            moderated.Moderations = moderated.Moderations ?? new List<Moderation>();

            IEnumerable<Moderation> moderations = moderated.Moderations.Where(m => m.User.ID == user.ID).ToList();

            if (user.Role.RoleParsed == Role.RoleEnum.Administrator ||
                user.Role.RoleParsed == Role.RoleEnum.iHandoverAdmin ||
                user.Role.RoleParsed == Role.RoleEnum.iHandoverAgent ||
                user.Role.RoleParsed == Role.RoleEnum.HRManager ||
                user.Role.RoleParsed == Role.RoleEnum.LineManager)
            {

                if (viewModel.LineManagerDecision.HasValue)
                {
                    var moderation = moderations.FirstOrDefault(m => m.OnBehalfOfRoleId == package.UserLineManager.RoleId)
                        ?? new Moderation()
                        {
                            UserId = user.ID,
                            CurrentRoleId = user.RoleId.Value
                        };

                    moderation.Decision = viewModel.LineManagerDecision.Value;
                    moderation.Notes = viewModel.LineManagerNotes;
                    moderation.OnBehalfOfRoleId = package.UserLineManager.RoleId;

                    if (!moderated.Moderations.Contains(moderation))
                    {
                        moderated.Moderations.Add(moderation);
                    }
                }

                if (viewModel.HrManagerDecision.HasValue)
                {
                    var moderation = moderations.FirstOrDefault(m => m.OnBehalfOfRoleId == package.UserHRManager.RoleId)
                        ?? new Moderation()
                        {
                            UserId = user.ID,
                            CurrentRoleId = user.RoleId.Value
                        };

                    moderation.Decision = viewModel.HrManagerDecision.Value;
                    moderation.Notes = viewModel.HrManagerNotes;
                    moderation.OnBehalfOfRoleId = package.UserHRManager.RoleId;

                    if (!moderated.Moderations.Contains(moderation))
                    {
                        moderated.Moderations.Add(moderation);
                    }
                }
            }
        }

        #endregion

        #endregion

        #region Logging

        /// <summary>
        /// Write an exception to the log with optional additional data
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="additionalInfo"></param>
        protected void LogException(Exception ex, string additionalInfo = null)
        {
            const string errorString = "{0}.{1}\r\n{2}";
            const string errorStringAdditionalDetails = "{0}.{1}\r\n{2}. \r\n\r\nAdditional Information provided: {3}";

            StackTrace stackTrace = new StackTrace();
            var callingMethod = stackTrace.GetFrame(1).GetMethod();

            Logger.Error(string.Format(additionalInfo == null ? errorString : errorStringAdditionalDetails, callingMethod.DeclaringType.FullName,
                callingMethod.Name, ex.ToString(), additionalInfo));
        }

        #endregion

        #region Alerts

        /// <summary>
        /// Add an alert to be shown on the next page loaded.
        /// </summary>
        /// <param name="type">The type of the alert.</param>
        /// <param name="message">Alert message.</param>
        protected void AddAlert(AlertViewModel.AlertType type, string message)
        {
            var alert = new AlertViewModel
            {
                Type = type,
                Message = message
            };

            List<AlertViewModel> alerts = TempData.ContainsKey("Alerts")
                ? (List<AlertViewModel>)TempData["Alerts"]
                : new List<AlertViewModel>();

            alerts.Insert(0, alert);

            TempData["Alerts"] = alerts;
        }

        #endregion

        #endregion
    }
}
