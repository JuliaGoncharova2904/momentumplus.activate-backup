﻿using System;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Activate.Web.Models.Handover;
using MomentumPlus.Activate.Web.Attributes;
using System.Collections.Generic;
using MomentumPlus.Activate.Services.Helpers;
using AccountConfig = MomentumPlus.Activate.Web.Helpers.AccountConfig;
using MomentumPlus.Core.Models.Handover;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Controllers
{
    public class HandoverController : BaseController
    {
        #region Dependency Injection

        public HandoverController(IAuditService auditService, IHandoverService handoverService,
            IMasterListService masterListService, IModerationService moderationService,
            IPackageService packageService, IUserService userService)
            : base(auditService, masterListService, packageService, userService)
        {
            HandoverService = handoverService;
            ModerationService = moderationService;
        }

        private IHandoverService HandoverService { get; set; }
        private IModerationService ModerationService { get; set; }

        #endregion

        #region Create

        /// <summary>
        ///     Create Handover - GET
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorise(Roles = RoleAuthHelper.Handover_Controller)]
        [HttpGet]
        public ActionResult Create(Guid id)
        {
            var viewModel = new HandoverViewModel
            {
                LeavingPeriodViewModel = new LeavingPeriodViewModel { NoticePeriod = LeavingPeriod.Notice.OneMonth },
                OnboardingPeriodViewModel = new OnboardingPeriodViewModel()
            };

            ViewBag.StarterExpanded = (viewModel.OnboardingPeriodViewModel.IsPrioritySaved || viewModel.OnboardingPeriodViewModel.PackageId.HasValue);
            PopulateHandoverViewModel(viewModel, id);

            return View("~/Views/Handover/HandoverForm.cshtml", viewModel);
        }

        /// <summary>
        ///     Create Handover - POST
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [Authorise(Roles = RoleAuthHelper.Handover_Controller)]
        [HttpPost]
        public ActionResult Create(HandoverViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    PopulateHandoverViewModel(viewModel, viewModel.LeavingPeriodViewModel.PackageId.Value);

                    ViewBag.StarterExpanded = (viewModel.OnboardingPeriodViewModel.IsPrioritySaved || viewModel.OnboardingPeriodViewModel.PackageId.HasValue);

                    return View("~/Views/Handover/HandoverForm.cshtml", viewModel);
                }
                
                Package leaverPackage = PackageService.GetByPackageId(viewModel.LeavingPeriodViewModel.PackageId.Value);
                PackageService.Update(leaverPackage);

                var handover = Mapper.Map<Handover>(viewModel);
                handover.LeavingPeriod.Priorities = new LeaverPriorities();
                handover.OnboardingPeriod.Priorities = new OnboarderPriorities();
                handover = HandoverService.Add(handover);

                TakeAction(viewModel.SubmitAction, handover);

                AppSession.Current.PrioritiesToolsReturnUrl = Url.Action("Edit", new { id = handover.ID });
                if (viewModel.SubmitAction == HandoverViewModel.Action.LeaverPriorities)
                {
                    return RedirectToAction("LeaverPriorities", new { id = handover.ID });
                }
                if (viewModel.SubmitAction == HandoverViewModel.Action.OnboarderPriorities)
                {
                    return RedirectToAction("OnboarderPriorities", new { id = handover.ID });
                }

                return RedirectToAction("Edit", new { id = handover.ID });
            }
            catch (Exception exception)
            {
                LogException(exception);
                AddAlert(AlertViewModel.AlertType.Danger, SaveErrorMessage);
            }

            return Redirect(AppSession.Current.HandoverFormReturnUrl);
        }

        #endregion

        #region Edit

        /// <summary>
        ///     Edit Handover - GET
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorise(Roles = RoleAuthHelper.Handover_Controller)]
        [HttpGet]
        public ActionResult Edit(Guid id)
        {
            var handover = HandoverService.GetById(id);
            var viewModel = Mapper.Map<HandoverViewModel>(handover);
            PopulateHandoverViewModel(viewModel, id, handover);
            ViewBag.StarterExpanded = (viewModel.OnboardingPeriodViewModel.IsPrioritySaved || viewModel.OnboardingPeriodViewModel.PackageId.HasValue);
            AppSession.Current.PackageExamineReturnUrl = Request.RawUrl;

            return View("~/Views/Handover/HandoverForm.cshtml", viewModel);
        }

        /// <summary>
        ///     Edit Handover - POST
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [Authorise(Roles = RoleAuthHelper.Handover_Controller)]
        [HttpPost]
        public ActionResult Edit(HandoverViewModel viewModel)
        {
            try
            {
                Handover handover = HandoverService.GetById(viewModel.ID);
                ViewBag.StarterExpanded = (viewModel.OnboardingPeriodViewModel.IsPrioritySaved || viewModel.OnboardingPeriodViewModel.PackageId.HasValue);
                
                Package leaverPackage = PackageService.GetByPackageId(viewModel.LeavingPeriodViewModel.PackageId.Value);
                leaverPackage.ExitSurvey = viewModel.LeavingPeriodViewModel.ExitSurvey;
                PackageService.Update(leaverPackage);
                
                if (!ModelState.IsValid)
                {
                    PopulateHandoverViewModel(viewModel, viewModel.LeavingPeriodViewModel.PackageId.Value, handover);
                    return View("~/Views/Handover/HandoverForm.cshtml", viewModel);
                }

                MapViewModelToHandover(viewModel, handover);
                handover.Package.ExitSurvey = viewModel.LeavingPeriodViewModel.ExitSurvey;
                HandoverService.Update(handover);

                TakeAction(viewModel.SubmitAction, handover);

                AppSession.Current.PrioritiesToolsReturnUrl = Request.RawUrl;
                if (viewModel.SubmitAction == HandoverViewModel.Action.LeaverPriorities)
                {
                    return RedirectToAction("LeaverPriorities", new { id = handover.ID });
                }
                if (viewModel.SubmitAction == HandoverViewModel.Action.OnboarderPriorities)
                {
                    return RedirectToAction("OnboarderPriorities", new { id = handover.ID });
                }

                return RedirectToAction("Edit", new { id = handover.ID });
            }
            catch (Exception exception)
            {
                LogException(exception);
                AddAlert(AlertViewModel.AlertType.Danger, SaveErrorMessage);
            }

            return Redirect(AppSession.Current.HandoverFormReturnUrl);
        }

        #endregion

        #region Cancel

        /// <summary>
        ///     Cancel Handover - GET
        /// </summary>
        /// <returns></returns>
        [Authorise(Roles = RoleAuthHelper.Handover_Controller)]
        [HttpGet]
        public ActionResult Cancel(Guid id)
        {
            try
            {
                HandoverService.Delete(id);
                AddAlert(AlertViewModel.AlertType.Success, SaveSuccessMessage);
                return Redirect(AppSession.Current.HandoverFormReturnUrl);
            }
            catch (Exception exception)
            {
                LogException(exception);
                AddAlert(AlertViewModel.AlertType.Danger, SaveErrorMessage);
                return RedirectToAction("Edit", id);
            }
        }

        #endregion

        #region Priorities

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorise(Roles = RoleAuthHelper.Handover_Controller)]
        public ActionResult LeaverPriorities(Guid id)
        {
            var handover = HandoverService.GetById(id);
            if (handover == null)
                return HttpNotFound();

            var model = Mapper.Map<LeaverPrioritiesViewModel>(handover.LeavingPeriod.Priorities);
            model.HandoverId = id;
            model.IsLocked = handover.LeavingPeriod.Locked.HasValue;

            PopulateLeaverPrioritiesViewModel(model, handover.Package);

            return View("~/Views/Handover/LeaverPriorities.cshtml", model);
        }

        [Authorise(Roles = RoleAuthHelper.Handover_Controller)]
        [HttpPost]
        public ActionResult LeaverPriorities(LeaverPrioritiesViewModel model)
        {
            var handover = HandoverService.GetById(model.HandoverId);
            if (handover == null)
                return HttpNotFound();

            Mapper.Map(model, handover.LeavingPeriod.Priorities);
            handover.LeavingPeriod.Priorities.Saved = SystemTime.Now;
            HandoverService.Update(handover);

            return Redirect(AppSession.Current.PrioritiesToolsReturnUrl);
        }

        public void PopulateLeaverPrioritiesViewModel(LeaverPrioritiesViewModel model, Package package)
        {
            model.UserName = package.UserOwner.FullName;

            model.PackageExperiencesDisabled = !package.ExperiencesEnabled;
            model.PackagePeopleDisabled = !package.PeopleManagementEnabled;
            model.PackageProjectsDisabled = !package.ProjectsEnabled;

            model.PeriodOptions = new List<SelectListItem>() {
                new SelectListItem() { Text = "Start", Value = DuePeriod.Start.ToString() },
                new SelectListItem() { Text = "Mid", Value = DuePeriod.Mid.ToString() },
                new SelectListItem() { Text = "End", Value = DuePeriod.End.ToString() },
            };

        }

        [Authorise(Roles = RoleAuthHelper.Handover_Controller)]
        public ActionResult OnboarderPriorities(Guid id)
        {
            var handover = HandoverService.GetById(id);
            if (handover == null)
                return HttpNotFound();

            var model = Mapper.Map<OnboarderPrioritiesViewModel>(handover.OnboardingPeriod.Priorities);
            model.HandoverId = id;
            model.PackageExperiencesDisabled = !handover.Package.ExperiencesEnabled;
            model.PackagePeopleDisabled = !handover.Package.PeopleManagementEnabled;
            model.PackageProjectsDisabled = !handover.Package.ProjectsEnabled;
            model.IsLocked = handover.OnboardingPeriod.Locked.HasValue;

            PopulateOnboardersPrioritiesViewModel(model);

            return View("~/Views/Handover/OnboarderPriorities.cshtml", model);
        }

        [Authorise(Roles = RoleAuthHelper.Handover_Controller)]
        [HttpPost]
        public ActionResult OnboarderPriorities(OnboarderPrioritiesViewModel model)
        {
            var handover = HandoverService.GetById(model.HandoverId);
            if (handover == null)
                return HttpNotFound();

            Mapper.Map(model, handover.OnboardingPeriod.Priorities);
            handover.OnboardingPeriod.Priorities.Saved = SystemTime.Now;
            HandoverService.Update(handover);

            return Redirect(AppSession.Current.PrioritiesToolsReturnUrl);
        }

        public void PopulateOnboardersPrioritiesViewModel(OnboarderPrioritiesViewModel model)
        {
            var list = new List<SelectListItem>();
            var handover = HandoverService.GetById(model.HandoverId);

            for (var i = AccountConfig.Settings.EarliestStartWeek; i <= handover.OnboardingPeriod.Duration; i++)
            {
                list.Add(new SelectListItem()
                {
                    Text = string.Format("Week {0}", i),
                    Value = i.ToString()
                });
            }

            model.PeriodOptions = list;
        }

        #endregion

        /// <summary>
        ///     Accept Handover - GET
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Accept(Guid id)
        {
            try
            {
                HandoverService.Accept(id);
                AddAlert(AlertViewModel.AlertType.Success, "Handover has been accepted.");
            }
            catch (Exception exception)
            {
                LogException(exception);
                AddAlert(AlertViewModel.AlertType.Danger, SaveErrorMessage);
            }

            return Redirect(Request.UrlReferrer.ToString());
        }

        #region Helpers

        /// <summary>
        ///     Populate view model
        /// </summary>
        /// <param name="viewModel"></param>
        /// <param name="leaverPackageId"></param>
        private void PopulateHandoverViewModel(HandoverViewModel viewModel, Guid leaverPackageId, Handover handover = null)
        {
            if (handover != null)
            {
                if (handover.LeavingPeriod.Locked.HasValue)
                {
                    viewModel.LeavingPeriodViewModel = Mapper.Map<LeavingPeriodViewModel>(handover.LeavingPeriod);
                    viewModel.LeavingPeriodViewModel.IsLocked = true;
                }

                if (handover.OnboardingPeriod.Locked.HasValue)
                {
                    viewModel.OnboardingPeriodViewModel = Mapper.Map<OnboardingPeriodViewModel>(handover.OnboardingPeriod);
                    viewModel.OnboardingPeriodViewModel.IsLocked = true;
                }

                viewModel.LeavingPeriodViewModel.IsPrioritySaved = handover.LeavingPeriod.Priorities != null &&
                                                                   handover.LeavingPeriod.Priorities.Saved.HasValue;

                viewModel.OnboardingPeriodViewModel.IsPrioritySaved = handover.OnboardingPeriod.Priorities != null &&
                                                                      handover.OnboardingPeriod.Priorities.Saved.HasValue;
            }

            var leaverPackage = PackageService.GetByPackageId(leaverPackageId);
            viewModel.LeavingPeriodViewModel.PackageName = leaverPackage.DisplayName;
            viewModel.LeavingPeriodViewModel.PackageId = leaverPackageId;
            viewModel.OnboardingPeriodViewModel.LeaverPackageId = leaverPackage.ID;
            viewModel.OnboardingPeriodViewModel.ItemsTotal = HandoverService.GetTotalItemsCount(leaverPackageId);
            viewModel.OnboardingPeriodViewModel.ItemsApproved = HandoverService.GetApprovedItemsCount(leaverPackageId);
            viewModel.OnboardingPeriodViewModel.ItemsRejected = HandoverService.GetRejectedItemsCount(leaverPackageId);
            viewModel.LeavingPeriodViewModel.ExitSurvey = leaverPackage.ExitSurvey.HasValue ? leaverPackage.ExitSurvey.Value : false;

            viewModel.OnboardingPeriodViewModel.Packages = PackageService.GetAll().Select(p =>
                new SelectListItem { Text = p.DisplayName, Value = p.ID.ToString() }
                ).OrderBy(i => i.Text);

            viewModel.OnboardingPeriodViewModel.Durations = AppSettings.OnboardDurations.Select(d =>
                new SelectListItem { Text = string.Format("{0} weeks", d), Value = d.ToString() }
                );
        }

        /// <summary>
        ///     Map the properties of the view model to the handover object.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <param name="handover"></param>
        private void MapViewModelToHandover(HandoverViewModel viewModel, Handover handover)
        {
            if (viewModel.LeavingPeriodViewModel != null && !handover.LeavingPeriod.Locked.HasValue)
            {
                handover.LeavingPeriod.NoticePeriod = viewModel.LeavingPeriodViewModel.NoticePeriod;
                handover.LeavingPeriod.LastDay = viewModel.LeavingPeriodViewModel.LastDay;
                handover.LeavingPeriod.ExitSurvey = viewModel.LeavingPeriodViewModel.ExitSurvey;
                //handover.LeavingPeriod.HandoverSupportRequested = viewModel.LeavingPeriodViewModel.HandoverSupportRequested;
            }

            if (viewModel.OnboardingPeriodViewModel != null && !handover.OnboardingPeriod.Locked.HasValue)
            {
                //handover.OnboardingPeriod.HandoverReportFinalised =
                //    viewModel.OnboardingPeriodViewModel.HandoverReportFinalised;
                //handover.OnboardingPeriod.NewProfileCreated = viewModel.OnboardingPeriodViewModel.NewProfileCreated;
                handover.OnboardingPeriod.PackageId = viewModel.OnboardingPeriodViewModel.PackageId;
                handover.OnboardingPeriod.Duration = viewModel.OnboardingPeriodViewModel.Duration;
                handover.OnboardingPeriod.StartPoint = viewModel.OnboardingPeriodViewModel.StartPoint;
                //handover.OnboardingPeriod.HandoverSupportRequested = viewModel.OnboardingPeriodViewModel.HandoverSupportRequested;
                handover.OnboardingPeriod.HandoverTasksFinalised =
                    viewModel.OnboardingPeriodViewModel.HandoverTasksFinalised;
                handover.OnboardingPeriod.ViewPreviousPackageContent =
                    viewModel.OnboardingPeriodViewModel.ViewPreviousPackageContent;
                handover.OnboardingPeriod.CarryOverRelationships =
                    viewModel.OnboardingPeriodViewModel.CarryOverRelationships;
                handover.OnboardingPeriod.CarryOverProjects = viewModel.OnboardingPeriodViewModel.CarryOverProjects;
            }
        }

        /// <summary>
        ///     Take a specific action on a handover based on the submit button pressed.
        /// </summary>
        /// <param name="submitAction"></param>
        /// <param name="handover"></param>
        private void TakeAction(HandoverViewModel.Action submitAction, Handover handover)
        {
            switch (submitAction)
            {
                case HandoverViewModel.Action.Save:
                    AddAlert(AlertViewModel.AlertType.Success, "Handover saved successfully.");
                    break;
                case HandoverViewModel.Action.LockLeaving:
                    HandoverService.LockLeavingPeriod(handover);

                    if (handover.LeavingPeriod.HandoverSupportRequested)
                    {
                        if (!EmailHelper.SendKITSLeaverMessage(handover, GetDomainUrl()))
                            AddAlert(AlertViewModel.AlertType.Warning, "Unable to send Handover Support request email.");
                    }

                    AddAlert(AlertViewModel.AlertType.Success, "Leaving period locked successfully.");
                    break;
                case HandoverViewModel.Action.UnlockLeaving:
                    HandoverService.UnlockLeavingPeriod(handover);
                    AddAlert(AlertViewModel.AlertType.Success, "Leaving period unlocked successfully.");
                    break;
                case HandoverViewModel.Action.LockOnboarding:
                    HandoverService.LockOnboardingPeriod(handover);

                    if (handover.OnboardingPeriod.HandoverSupportRequested)
                    {
                        if (!EmailHelper.SendKITSOnboardMessage(handover, GetDomainUrl()))
                            AddAlert(AlertViewModel.AlertType.Warning, "Unable to send Handover Support request email.");
                    }

                    AddAlert(AlertViewModel.AlertType.Success, "Onboard period locked successfully.");
                    break;
                case HandoverViewModel.Action.UnlockOnboarding:
                    HandoverService.UnlockOnboardingPeriod(handover);
                    AddAlert(AlertViewModel.AlertType.Success, "Onboard period unlocked successfully.");
                    break;
            }
        }

        private string GetDomainUrl()
        {
            return Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host + (Request.Url.IsDefaultPort ? "" : ":" + Request.Url.Port);
        }

        #endregion
    }
}