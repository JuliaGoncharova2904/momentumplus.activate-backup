﻿using System;
using System.Linq;
using System.Web.Mvc;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Core.Authorization;
using MomentumPlus.Core.Models.People;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Controllers
{
    public class AccountController : BaseController
    {
        #region IOC

        public AccountController(
            IUserService userService,
            IPackageService packageService
        )
        {
            UserService = userService;
            PackageService = packageService;
        }

        #endregion

        #region ACTIONS

        /// <summary>
        /// Render login view or redirect to home if already logged in.
        /// </summary>
        /// <param name="r">URL to return to on successful login</param>
        /// <returns></returns>
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Login(string r)
        {
            if (User.Identity.IsAuthenticated)
            {
                AddAlert(AlertViewModel.AlertType.Warning, "You're already logged in.");
                return Redirect("/");
            }
            return View(new LoginViewModel { ReturnUrl = r });
        }

        /// <summary>
        /// Handle login form submission
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model)
        {
            User user;
            Session.Clear();
            try
            {
                user = UserService.GetByUsername(model.UserName);

                if (user == null)
                {
                    throw new Exception(string.Format("ILLEGAL ACCESS ATTEMPT: user attempted to login as {0}, but this account did not exist", model.UserName));
                }
            }
            catch (Exception e)
            {
                LogException(e);
                ModelState.AddModelError("", "The user name or password provided is incorrect.");
                return View(model);
            }

            if (ModelState.IsValid && WebSecurity.Login(model.UserName, model.Password, model.RememberMe))
            {
                AppSession.Current.CurrentUser = user;
				HttpContext.Session.Timeout = 10080;//TODO: set timeout for session (for now 7 days)
                var packages = PackageService.GetPackagesByUserId(user.ID);

                // Redirect to the appropriate view based on whether the user has packages
                // or is a manager/admin.
                if (packages.Any())
                {
                    var currentPackage = packages.FirstOrDefault();
                    SetCurrentPackage(currentPackage);
                    return RedirectToLocal(model.ReturnUrl, "Index", "Summary", new { area = "Home" });
                }
                else if (user.Role != null &&
                    (user.Role.RoleParsed == Role.RoleEnum.HRManager ||
                    user.Role.RoleParsed == Role.RoleEnum.LineManager ||
                    user.Role.RoleParsed == Role.RoleEnum.Administrator ||
                    user.Role.RoleParsed == Role.RoleEnum.iHandoverAgent ||
                    user.Role.RoleParsed == Role.RoleEnum.Executive ||
                    user.Role.RoleParsed == Role.RoleEnum.iHandoverAdmin))
                {
                    return RedirectToLocal(model.ReturnUrl, "Index", "MyTeam");
                }
                else
                {
                    return RedirectToLocal(model.ReturnUrl, "NoPackages", "Account", new { area = "" });
                }
            }
            if (user.IsLockedOut)
            {
                ModelState.AddModelError("", "The user account is locked. Please speak to your iHandover Consultant.");
            }
            else
            {
                ModelState.AddModelError("", "The user name or password provided is incorrect.");
            }

            Request.RequestContext.HttpContext.Response.AddHeader("Cache-Control", "no-cache, max-age=0, must-revalidate, no-store");

            return View(model);
        }

        /// <summary>
        /// Log off current user and redirect to login page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult LogOff()
        {
            var user = GetCurrentUser();
            AppSession.Current.CurrentUser = null;

            WebSecurity.Logout();
            Session.Clear();

            return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// Render "No Packages" view for users without packages or redirect
        /// to team page for iHandover admins 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult NoPackages()
        {
            // Redirect the user to the My Team page if they are iHandover Admin
            var user = GetCurrentUser();
            if (user.Role.RoleName.Equals(RoleAuthHelper.iHandoverAdmin))
            {
                return RedirectToAction("Index", "MyTeam");
            }

            return View();
        }

        /// <summary>
        /// Render forgotten password form
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult ForgottenPassword()
        {
            return View(new ForgottenPasswordViewModel());
        }

        /// <summary>
        /// Handle forgotten password form submission
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ForgottenPassword(ForgottenPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = UserService.GetByUsername(model.Email);

            if (user != null)
            {
                try
                {
                    // Generate reset GUID for user
                    var resetGuid = Guid.NewGuid();
                    user.PasswordResetGuid = resetGuid;
                    UserService.UpdateUser(user);

                    // Send password reset email
                    var resetUrl = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Action("ResetPassword", new { id = resetGuid }));
                    EmailHelper.SendPasswordResetMessage(user, resetUrl);

                    AddAlert(AlertViewModel.AlertType.Success, "A password reset email has been sent to your email account, please check your email to continue the process.");
                }
                catch (Exception ex)
                {
                    LogException(ex);
                    AddAlert(AlertViewModel.AlertType.Danger, "Something went wrong, please try again.");
                }
            }
            else
            {
                AddAlert(AlertViewModel.AlertType.Danger, "No user with that email address exists.");
            }

            return RedirectToAction("Login");
        }

        /// <summary>
        /// Render reset password form or redirect to login if the reset GUID
        /// is wrong.
        /// </summary>
        /// <param name="id">The password reset ID generated previously</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult ResetPassword(Guid id)
        {
            var user = UserService.GetByPasswordResetGuid(id);

            if (user != null)
            {
                return View(new ResetPasswordViewModel { PasswordResetGuid = id });
            }

            return RedirectToAction("Login");
        }

        /// <summary>
        /// Handle reset password form submission
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = UserService.GetByPasswordResetGuid(model.PasswordResetGuid);

            if (user != null)
            {
                try
                {
                    UserService.ResetPassword(user.ID, model.NewPassword);
                    AddAlert(AlertViewModel.AlertType.Success, "Password changed successfully, you may now log in.");
                }
                catch (Exception ex)
                {
                    LogException(ex);
                    AddAlert(AlertViewModel.AlertType.Danger, "Something went wrong, please try again.");
                }
            }

            return RedirectToAction("Login");
        }

        #endregion

        #region HELPERS

        /// <summary>
        /// Redirect to a local URL or a default action if the
        /// URL is not local.
        /// </summary>
        /// <param name="returnUrl">Local return URL</param>
        /// <param name="defaultActionName">Default action name</param>
        /// <param name="defaultControllerName">Default controller name</param>
        /// <param name="defaultRouteValues">Default route values</param>
        /// <returns></returns>
        private ActionResult RedirectToLocal(
            string returnUrl,
            string defaultActionName,
            string defaultControllerName,
            object defaultRouteValues = null
        )
        {
            if (returnUrl != "/" && Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(defaultActionName, defaultControllerName,
                    defaultRouteValues);
            }
        }

        #endregion
    }
}
