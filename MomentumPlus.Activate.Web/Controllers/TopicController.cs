﻿using System;
using System.Linq;
using System.Web.Mvc;
using MomentumPlus.Activate.Services;
using MomentumPlus.Activate.Services.Helpers;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Core;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Core.Models.Moderation;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Controllers
{
    public class TopicController : BaseController
    {
        private TopicCore TopicCore;

        #region IOC

        protected ITaskService TaskService;
        protected ITopicGroupService TopicGroupService;
        protected ITopicService TopicService;
        protected ITopicQuestionService TopicQuestionService;
        protected IWorkplaceService WorkplaceService;
        protected IProLinkService ProLinkService;
        protected IPositionService PositionService;


        /// <summary>
        /// Construct a new <c>TopicController</c> with the given service
        /// implementations.
        /// </summary>
        public TopicController(
            IAuditService auditService,
            IHandoverService handoverService,
            IMasterListService masterListService,
            IPackageService packageService,
            ITaskService taskService,
            ITopicGroupService topicGroupService,
            ITopicService topicService,
            ITopicQuestionService topicQuestionService,
            IUserService userService,
            IWorkplaceService workplaceService,
            IProLinkService proLinkService,
            IPositionService positionService,
            TopicCore topicCore
        )
            : base(
                  auditService,
                  masterListService,
                  packageService,
                  userService
              )
        {
            TopicCore = topicCore;

            PositionService = positionService;
            TaskService = taskService;
            TopicGroupService = topicGroupService;
            TopicService = topicService;
            TopicQuestionService = topicQuestionService;
            WorkplaceService = workplaceService;
            ProLinkService = proLinkService;
        }

        #endregion

        #region ACTIONS

        #region ViewTopic

        [HttpGet]
        public ActionResult ViewTopic(Guid id)
        {
            // Get topic, group, and package
            var topic = TopicService.GetByTopicId(id);
            var topicGroup = TopicGroupService.GetByTopicId(topic.ID);

            // Populate view model
            var topicViewModel = TopicCore.GetModel(topicGroup, topic);
            TopicCore.GetAnswers(topicGroup, topic, topicViewModel);
            topicViewModel.ReadOnly = true;

            // Only show approved tasks
            topicViewModel.Tasks =
                topicViewModel.Tasks.Where(t => t.Moderations.Any(m => m.Decision == ModerationDecision.Approved));

            AuditService.RecordActivity<Topic>(AuditLog.EventType.Viewed, topic.ID);

            // Set return URLs for managers
            AppSession.Current.VoiceMessageManagerReturnUrl = Request.RawUrl;
            AppSession.Current.AttachmentManagerReturnUrl = Request.RawUrl;
            AppSession.Current.TaskManagerReturnUrl = Request.RawUrl;
            AppSession.Current.TaskFormReturnUrl = Request.RawUrl;

            return View("TopicForm", topicViewModel);
        }

        #endregion

        #region CreateTopic

        /// <summary>
        /// Render the topic form in create mode.
        /// </summary>
        /// <param name="id">The topic group ID.</param>
        [Authorise("TopicGroup")]
        [HttpGet]
        public ActionResult CreateTopic(Guid id, Guid? processGroupId = null)
        {
            if (TempData["InProLink"] != null && (bool)TempData["InProLink"])
                TempData.Keep("InProLink");

            var topicViewModel = TopicCore.GetNewTopicViewModel(id, GetCurrentUser());

            topicViewModel.ProcessGroupId = processGroupId;

            if (processGroupId.HasValue)
            {
                var processGroup = ProLinkService.GetProcessGroup(processGroupId.Value);

                ViewBag.FormHeading = string.Format("{0} - {1} - Create Process", processGroup.Function.Name, processGroup.Name);
            }

            return View("TopicForm", topicViewModel);
        }

        /// <summary>
        /// Handle new topic submission.
        /// </summary>
        [Authorise("TopicGroup")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateTopic(TopicViewModel topicViewModel)
        {
            if (TempData["InProLink"] != null && (bool)TempData["InProLink"])
                TempData.Keep("InProLink");

            // Retrieve topic group
            var topicGroup = TopicGroupService.GetByTopicGroupId(topicViewModel.TopicGroupId);

            if (Exists(Guid.Empty, topicGroup.ID, topicViewModel.Name) || !ModelState.IsValid)
            {
                // Model invalid - repopulate select lists and role
                var package = PackageService.GetByTopicGroupId(topicViewModel.TopicGroupId);
                TopicCore.PopulateTopicViewModel(topicGroup, topicViewModel);
                topicViewModel.FrameworkItem = package == null;

                if (topicViewModel.ProcessGroupId.HasValue)
                {
                    var processGroup = ProLinkService.GetProcessGroup(topicViewModel.ProcessGroupId.Value);

                    ViewBag.FormHeading = string.Format("{0} - {1} - Create Process", processGroup.Function.Name, processGroup.Name);
                }

                return View("TopicForm", topicViewModel);
            }

            try
            {
                var topic = TopicCore.CreateTopic(topicGroup, topicViewModel);

                var returnUrl = Url.Action("EditTopic", new { id = topic.ID });
                AppSession.Current.VoiceMessageManagerReturnUrl = returnUrl;
                AppSession.Current.AttachmentManagerReturnUrl = returnUrl;
                AppSession.Current.TaskManagerReturnUrl = returnUrl;
                AppSession.Current.TaskFormReturnUrl = returnUrl;

                return GetSuccessResult(topic.ID, "Topic added successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error adding topic");
            }

            return Redirect(AppSession.Current.TopicFormReturnUrl);
        }

        #endregion

        #region EditTopic

        /// <summary>
        /// Render the topic form in edit mode.
        /// </summary>
        /// <param name="id">The topic ID.</param>
        [Authorise("Topic")]
        [HttpGet]
        public ActionResult EditTopic(Guid id, bool prevTopic = false)
        {
            if (TempData["InProLink"] != null && (bool)TempData["InProLink"])
                TempData.Keep("InProLink");

            var topic = TopicService.GetByTopicId(id);
            var topicViewModel = TopicCore.GetTopicViewModel(topic, prevTopic, GetCurrentUser());

            var package = PackageService.GetByTopicId(id);
            if (package != null)
            {
                SetModeratedViewModelFields(topic, topicViewModel, package);
                topicViewModel.ShowLineManagerSection = true;
                topicViewModel.ShowHrManagerSection = true;

                if (PackageService.GetAllToDoItems(package.ID).Any(t => GeneralHelpers.GetUrlId(t.Url) == topic.ID.ToString()) && package.UserOwnerId == GetCurrentUser().ID)
                {
                    var toDoItems = PackageService.GetAllToDoItems(package.ID).Where(t => GeneralHelpers.GetUrlId(t.Url) == topic.ID.ToString());
                    foreach (var item in toDoItems)
                    {
                        item.DismissDate = SystemTime.Now;
                        PackageService.UpdateToDo(item);
                    }
                }
            }

            if (prevTopic)
            {
                ViewData["HideSideNav"] = true;
            }

            // Set return URLs for managers
            AppSession.Current.VoiceMessageManagerReturnUrl = Request.RawUrl;
            AppSession.Current.AttachmentManagerReturnUrl = Request.RawUrl;
            AppSession.Current.TaskManagerReturnUrl = Request.RawUrl;
            AppSession.Current.TaskFormReturnUrl = Request.RawUrl;
            AppSession.Current.PackageTransferFormReturnUrl = Request.RawUrl;

            AuditService.RecordActivity<Topic>(AuditLog.EventType.Viewed, topic.ID);

            if (topicViewModel.ProcessGroupId.HasValue)
            {
                var processGroup = ProLinkService.GetProcessGroup(topicViewModel.ProcessGroupId.Value);

                ViewBag.FormHeading = string.Format("{0} - {1} - Create Process", processGroup.Function.Name, processGroup.Name);
            }

            return View("~/Views/Shared/TopicForm.cshtml", topicViewModel);
        }

        /// <summary>
        /// Handle edited topic submission.
        /// </summary>
        [Authorise("Topic")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTopic(TopicViewModel topicViewModel)
        {
            if (TempData["InProLink"] != null && (bool)TempData["InProLink"])
                TempData.Keep("InProLink");

            // Get the topic being edited
            var topic = TopicService.GetByTopicId(topicViewModel.ID);

            // Retrieve topic group and package for the submitted topic
            var topicGroup = TopicGroupService.GetByTopicGroupId(topicViewModel.TopicGroupId);
            var package = PackageService.GetByTopicGroupId(topicGroup.ID);

            bool topicExists = topic.Name != topicViewModel.Name && Exists(topic.ID, topicGroup.ID, topicViewModel.Name);

            if (topicExists || !ModelState.IsValid)
            {
                // Invalid - repopulate dropdown values
                TopicCore.PopulateTopicViewModel(topicGroup, topicViewModel);
                topicViewModel.FrameworkItem = package == null;

                if (package != null)
                {
                    SetModeratedViewModelFields(topic, topicViewModel, package);
                    topicViewModel.ShowLineManagerSection = true;
                    topicViewModel.ShowHrManagerSection = true;
                }

                topicViewModel.NotesSubjectivity = topic.CountSubjectivity(MasterListService);
                topicViewModel.NotesWordCount = topic.CountWords();
                topicViewModel.HasOpenTasks = topic.Tasks.Any(t => !t.Complete);

                if (topicViewModel.ProcessGroupId.HasValue)
                {
                    var processGroup = ProLinkService.GetProcessGroup(topicViewModel.ProcessGroupId.Value);

                    ViewBag.FormHeading = string.Format("{0} - {1} - Create Process", processGroup.Function.Name, processGroup.Name);
                }

                return View("TopicForm", topicViewModel);
            }

            try
            {
                UpdateModerated(topic, topicViewModel, package);
                TopicCore.UpdateTopic(package, topicGroup, topic, topicViewModel, GetCurrentUser());
                return GetSuccessResult(topic.ID, "Topic updated successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error updating topic.");
            }

            return Redirect(AppSession.Current.TopicFormReturnUrl);
        }

        #endregion

        #region RetireTopic/ResumeTopic

        [Authorise("Topic")]
        [HttpGet]
        public ActionResult RetireTopic(Guid id)
        {
            try
            {
                TopicService.Retire(id);
                AddAlert(AlertViewModel.AlertType.Success, "Topic retired successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error retiring topic.");
            }

            return RedirectToAction("EditTopic", new { id = id });
        }

        [Authorise("Topic")]
        [HttpGet]
        public ActionResult ResumeTopic(Guid id)
        {
            try
            {
                TopicService.Restore(id);
                AddAlert(AlertViewModel.AlertType.Success, "Topic resumed successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error resuming topic.");
            }

            return RedirectToAction("EditTopic", new { id = id });
        }

        #endregion

        #endregion

        #region HELPERS

        private ActionResult GetSuccessResult(Guid topicId, string successMessage)
        {
            var redirectTo = Request.Form["Redirect"];
            Guid taskId = Guid.Empty;

            if (redirectTo != null && redirectTo.StartsWith("EditTask"))
            {
                var temp = redirectTo.Split('/');
                redirectTo = temp[0];
                taskId = new Guid(temp[1]);
            }

            // Redirect to a specific action depending on the submit button pressed
            switch (redirectTo)
            {
                case "VoiceMessages":
                    return RedirectToAction("Index", "VoiceMessage", new { area = "", id = topicId, type = ItemType.Topic });
                case "Attachments":
                    return RedirectToAction("Index", "Attachments", new { area = "", id = topicId, type = ItemType.Topic });
                case "Tasks":
                    return RedirectToAction("Index", "TaskManager", new { area = "", id = topicId });
                case "AddTask":
                    return RedirectToAction("CreateTask", "Task", new { area = "", id = topicId });
                case "EditTask":
                    return RedirectToAction("EditTask", "Task", new { area = "", id = taskId });
                case "None":
                    return RedirectToAction("EditTopic", new { id = topicId });
            }

            AddAlert(AlertViewModel.AlertType.Success, successMessage);
            return Redirect(AppSession.Current.TopicFormReturnUrl);
        }

        /// <summary>
        /// Check a topic name is already being used by one of the topics
        /// in a topic group. If so then an error is added to the model
        /// state for the topic Name field.
        /// </summary>
        /// <param name="topicId">Topic id</param>
        /// <param name="topicGroupId">The topic group GUID.</param>
        /// <param name="topicName">The name of the topic.</param>
        /// <returns>Whether or not the topic already exists.</returns>
        private bool Exists(Guid topicId, Guid topicGroupId, string topicName)
        {
            if (TopicService.Exists(topicId, topicGroupId, topicName))
            {
                ModelState.AddModelError("Name", "A topic with this name already exists.");
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion
    }
}
