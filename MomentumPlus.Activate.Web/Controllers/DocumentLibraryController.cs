﻿using System;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using MomentumPlus.Activate.Services;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Core;
using MomentumPlus.Activate.Web.Extensions;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Controllers
{
    public class DocumentLibraryController : BaseController
    {
        private DocumentLibraryCore DocumentLibraryCore;

        #region IOC

        private IDocumentLibraryService DocumentLibraryService;

        public DocumentLibraryController(
            IAuditService auditService,
            IContactService contactService,
            IDocumentLibraryService documentLibraryService,
            IMasterListService masterListService,
            IMeetingService meetingService,
            IPackageService packageService,
            IPeopleService peopleService,
            IPositionService positionService,
            IProjectService projectService,
            ITaskService taskService,
            ITopicService topicService,
            IHandoverService handoverService,
            IUserService userService,
            DocumentLibraryCore documentLibraryCore
        )
            : base(
                  auditService,
                  masterListService,
                  packageService,
                  userService
              )
        {
            DocumentLibraryCore = documentLibraryCore;

            DocumentLibraryService = documentLibraryService;
        }

        #endregion

        #region ACTIONS

        #region Index

        public ActionResult Index(
            string Query = null,
            Guid? PositionId = null,
            int pageNo = 1,
            DocumentLibrary.SortColumn sortColumn = DocumentLibrary.SortColumn.Created,
            bool sortAscending = true,
            Guid? ItemId = null,
            ItemType? ItemType = null,
            bool retired = false
        )
        {
            var model = DocumentLibraryCore.GetDocumentLibraryViewModel(
                Query,
                PositionId,
                pageNo,
                sortColumn,
                sortAscending,
                ItemId,
                ItemType,
                retired,
                GetCurrentPackageId()
            );

            AppSession.Current.AttachmentFormReturnUrl = Request.RawUrl;
            AppSession.Current.DocumentLibraryFormReturnUrl = Request.RawUrl;
            AppSession.Current.DocumentReportReturnUrl = Request.RawUrl;

            return View(model);
        }

        #endregion

        #region Create

        [Authorise(Roles = RoleAuthHelper.ABSOLUTE_GENERAL_ACCESS)]
        [HttpGet]
        public ActionResult Create(Guid? id)
        {
            DocumentLibraryFormViewModel viewModel = new DocumentLibraryFormViewModel
            {
                ItemId = id,
            };

            DocumentLibraryCore.PopulateDocLibFormViewModel(viewModel);

            AppSession.Current.AttachmentManagerReturnUrl = Request.RawUrl;

            return View("DocumentLibraryForm", viewModel);
        }

        [Authorise(Roles = RoleAuthHelper.ABSOLUTE_GENERAL_ACCESS)]
        [HttpPost]
        public ActionResult Create(DocumentLibraryFormViewModel viewModel, HttpPostedFileBase theFile)
        {
            // Ensure that either a file or file location was entered
            if (theFile == null && string.IsNullOrWhiteSpace(viewModel.FilePath))
            {
                var errorMessage = "You must select a file to upload or specify a file location.";
                ModelState.AddModelError("PostedFile", errorMessage);
                ModelState.AddModelError("FilePath", errorMessage);
            }
            else if (theFile != null && !theFile.IsValid())
            {
                ModelState.AddModelError("PostedFile", "This file type is not supported.");
            }

            if (!ModelState.IsValid)
            {
                DocumentLibraryCore.PopulateDocLibFormViewModel(viewModel);
                return View("DocumentLibraryForm", viewModel);
            }

            try
            {
                var documentId = DocumentLibraryCore.CreateDocument(viewModel, theFile);
                return GetSuccessResult(documentId, "Document created successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error creating Document.");
                DocumentLibraryCore.PopulateDocLibFormViewModel(viewModel);
                return View("DocumentLibraryForm", viewModel);
            }
        }

        #endregion

        #region Edit

        [Authorise(Roles = RoleAuthHelper.ABSOLUTE_GENERAL_ACCESS)]
        [HttpGet]
        public ActionResult Edit(Guid id)
        {
            DocumentLibrary document = DocumentLibraryService.GetByID(id);
            DocumentLibraryFormViewModel viewModel = Mapper.Map<DocumentLibrary, DocumentLibraryFormViewModel>(document);

            DocumentLibraryCore.PopulateDocLibFormViewModel(viewModel);

            AppSession.Current.AttachmentManagerReturnUrl = Request.RawUrl;

            AuditService.RecordActivity<DocumentLibrary>(AuditLog.EventType.Viewed, document.ID);

            return View("DocumentLibraryForm", viewModel);
        }

        [Authorise(Roles = RoleAuthHelper.ABSOLUTE_GENERAL_ACCESS)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DocumentLibraryFormViewModel viewModel, HttpPostedFileBase theFile)
        {
            var document = DocumentLibraryService.GetByID(viewModel.ID);
            viewModel.Attachment = Mapper.Map<AttachmentViewModel>(document.Attachment);

            // Ensure that either a file or file location was entered or that a file already exists
            // for the document.
            if (theFile == null &&
                (document.Attachment == null || !document.Attachment.FileId.HasValue) &&
                 string.IsNullOrWhiteSpace(viewModel.FilePath))
            {
                var errorMessage = "You must select a file to upload or specify a file location.";
                ModelState.AddModelError("PostedFile", errorMessage);
                ModelState.AddModelError("FilePath", errorMessage);
            }
            else if (theFile != null && !theFile.IsValid())
            {
                ModelState.AddModelError("PostedFile", "This file type is not supported.");
            }

            if (!ModelState.IsValid)
            {
                DocumentLibraryCore.PopulateDocLibFormViewModel(viewModel);
                return View("DocumentLibraryForm", viewModel);
            }
           
            DocumentLibraryCore.UpdateDocument(document, viewModel, theFile);
            return GetSuccessResult(viewModel.ID, "Document updated successfully.");
        }

        #endregion

        #region Add

        public ActionResult Add(Guid id, Guid itemId, ItemType itemType)
        {
            try
            {
                DocumentLibraryCore.Add(id, itemId, itemType);
                AddAlert(AlertViewModel.AlertType.Success, "Added sucessfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error adding link.");
            }

            return Redirect(AppSession.Current.DocumentLibraryReturnUrl);
        }

        #endregion

        #endregion

        #region HELPERS

        private ActionResult GetSuccessResult(Guid documentId, string successMessage)
        {
            // Redirect to a specific action depending on the submit button pressed
            switch (Request.Form["Redirect"])
            {
                case "Attachments":
                    return RedirectToAction("Index", "Attachments", new { area = "", id = documentId, type = ItemType.Document });
            }

            AddAlert(AlertViewModel.AlertType.Success, successMessage);
            return RedirectToAction("Index");
        }

        #endregion
    }
}
