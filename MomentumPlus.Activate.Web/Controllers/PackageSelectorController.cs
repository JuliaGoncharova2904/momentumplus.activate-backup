﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Controllers
{
    public class PackageSelectorController : BaseController
    {
        #region IOC

        /// <summary>
        /// PackageDropdownController Constructor
        /// </summary>
        public PackageSelectorController(
            IAuditService auditService,
            IMasterListService masterListService,
            IPackageService packageService,
            IUserService userService
        )
            : base(
                  auditService,
                  masterListService,
                  packageService,
                  userService
              )
        {
        }

        #endregion

        #region ACTIONS

        #region Index

        [ChildActionOnly]
        public ActionResult Index()
        {
            // Create a new PackageSelectorViewModel view model
            PackageSelectorViewModel viewModel = new PackageSelectorViewModel();
            if (AppSession.Current.ExaminePackageId.HasValue)
            {
                // Examine package mode - show the package being examined
                Package package = PackageService.GetByPackageId(AppSession.Current.ExaminePackageId.Value);
                viewModel.SelectedPackage = package.DisplayName;
                viewModel.ExamineMode = true;
            }
            else
            {
                var user = GetCurrentUser();
                // Get packages for the returned user
                IEnumerable<Package> packages = PackageService.GetPackagesByUserId(user.ID);
                // Check if the CurrentPackageId in the current session has value
                if (AppSession.Current.CurrentPackageId.HasValue)
                {
                    // If true, get the package and set the selected package name
                    viewModel.SelectedPackage = PackageService.GetByPackageId(AppSession.Current.CurrentPackageId.Value).Name;
                    viewModel.Packages =
                        Mapper.Map<IEnumerable<Package>, IEnumerable<PackageViewModel>>(
                            packages.Where(p => p.ID != AppSession.Current.CurrentPackageId.Value));
                }
                else if (!AppSession.Current.CurrentPackageId.HasValue && packages != null && packages.Any())
                {
                    // Else, set the CurrentPackageId the id of the first found package
                    Guid packageId = packages.FirstOrDefault().ID;
                    AppSession.Current.CurrentPackageId = packageId;
                    viewModel.SelectedPackage = PackageService.GetByPackageId(packageId).Name;
                    viewModel.Packages = Mapper.Map<IEnumerable<Package>, IEnumerable<PackageViewModel>>(packages.Where(p => p.ID != AppSession.Current.CurrentPackageId.Value));
                }
            }
            return PartialView("~/Views/PackageSelector/Index.cshtml", viewModel);
        }

        #endregion

        #region SelectPackage

        [HttpGet]
        public ActionResult SelectPackage(Guid id)
        {
            var package = PackageService.GetByPackageId(id);
            SetCurrentPackage(package);
            return RedirectToAction("Index", "Summary", new { area = "Home" });
        }

        #endregion

        #region ExaminePackage

        [HttpGet]
        public ActionResult ExaminePackage(Guid id)
        {
            AppSession.Current.ExaminePackageId = id;
            var package = PackageService.GetByPackageId(id);
            AppSession.Current.ExperiencesEnabled = package.ExperiencesEnabled;
            AppSession.Current.RelationshipsEnabled = true;
            AppSession.Current.ProjectsEnabled = package.ProjectsEnabled;
            AppSession.Current.PeopleManagementEnabled = package.PeopleManagementEnabled;
            AppSession.Current.CurrentPackageHasAncestor = package.AncestorPackageId.HasValue;
            return RedirectToAction("Index", "Summary", new { area = "Home" });
        }

        #endregion

        #region ExitExamineMode

        [HttpGet]
        public ActionResult ExitExamineMode()
        {
            AppSession.Current.ExaminePackageId = null;

            SetCurrentPackage(GetCurrentPackage());

            if (!string.IsNullOrEmpty(AppSession.Current.PackageExamineReturnUrl))
            {
                return Redirect(AppSession.Current.PackageExamineReturnUrl);
            }
            else
            {
                return Redirect("/");
            }
        }

        #endregion

        #region PackageDetails

        [ChildActionOnly]
        public ActionResult PackageDetails()
        {
            if (AppSession.Current.ExaminePackageId.HasValue)
            {
                var package = PackageService.GetByPackageId(AppSession.Current.ExaminePackageId.Value);
                return View(package);
            }

            return new EmptyResult();
        }

        #endregion

        #endregion
    }
}