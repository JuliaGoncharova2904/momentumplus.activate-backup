﻿using System.Web.Mvc;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Helpers;

namespace MomentumPlus.Activate.Web.Controllers
{
    public class HomeController : BaseController
    {

        public HomeController(
                IAuditService auditService,
                IMasterListService masterListService,
                IPackageService packageService,
                IUserService userService
            )
            : base(
                auditService,
                masterListService,
                packageService,
                userService
            ){ }

        /// <summary>
        /// Redirects to the <c>Index</c> action of the <c>Summary</c>
        /// controller in the <c>Home</c> area.
        /// </summary>
        /// <returns>The redirect result object.</returns>
        public ActionResult Index()
        {
            AppSession.Current.ExaminePackageId = null;
            AppSession.Current.CurrentPackageId = null;
            SetCurrentPackage(GetCurrentPackage());

            return RedirectToAction(
                "Index",
                "Summary",
                new { area = "Home" }
            );
        }
    }
}
