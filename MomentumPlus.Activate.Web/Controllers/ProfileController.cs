﻿using System.Web.Mvc;

namespace MomentumPlus.Activate.Web.Controllers
{
    public class ProfileController : BaseController
    {
        /// <summary>
        /// Redirects to the <c>Index</c> action of the <c>Profile</c>
        /// controller in the <c>Profile</c> area.
        /// </summary>
        /// <returns>The redirect result object.</returns>
        public ActionResult Index()
        {
            return RedirectToAction(
                "Index",
                "Profile",
                new { area = "Profile" }
            );
        }

    }
}
