﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls.Expressions;
using AutoMapper;
using MomentumPlus.Activate.Services;
using MomentumPlus.Activate.Services.Helpers;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Core;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;
using AccountConfig = MomentumPlus.Activate.Web.Helpers.AccountConfig;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.Moderation;

namespace MomentumPlus.Activate.Web.Controllers
{
    public class TaskController : TopicController
    {
        private TaskCore TaskCore;
        private readonly IPackageService PackageService;
        private readonly IHandoverService HandoverService;
        private readonly IProjectService ProjectService;
        private readonly IContactService ContactService;

        #region IOC

        /// <summary>
        /// Construct a new <c>TaskController</c> with the given service
        /// implementations.
        /// </summary>
        public TaskController(
            IAuditService auditService,
            IHandoverService handoverService,
            IMasterListService masterListService,
            IPackageService packageService,
            ITaskService taskService,
            ITopicService topicService,
            ITopicGroupService topicGroupService,
            ITopicQuestionService topicQuestionService,
            IUserService userService,
            IWorkplaceService workplaceService,
            IProLinkService proLink,
            IProjectService projectService,
            IContactService contactService,
            TaskCore taskCore,
            IPositionService positionService,
            TopicCore topicCore
        )
            : base(
                  auditService,
                  handoverService,
                  masterListService,
                  packageService,
                  taskService,
                  topicGroupService,
                  topicService,
                  topicQuestionService,
                  userService,
                  workplaceService,
                  proLink,
                  positionService,
                  topicCore
              )
        {
            TaskCore = taskCore;
            PackageService = packageService;
            HandoverService = handoverService;
            ProjectService = projectService;
            ContactService = contactService;
        }

        #endregion

        #region ACTIONS

        #region ViewTask

        [HttpGet]
        public ActionResult ViewTask(Guid id, bool onboarding = false)
        {
            var task = TaskService.GetByTaskId(id);
            Reviewable parent = null;
            var package = GetPackage(task, ref parent);

            var taskViewModel = Mapper.Map<TaskViewModel>(task);
            TaskCore.PopulateTaskViewModel(taskViewModel, GetCurrentUser(), parent, task);
            taskViewModel.ReadOnly = true;
            taskViewModel.OnboarderMode = onboarding;

            AppSession.Current.AttachmentManagerReturnUrl = Request.RawUrl;
            AppSession.Current.VoiceMessageManagerReturnUrl = Request.RawUrl;

            AuditService.RecordActivity<Task>(AuditLog.EventType.Viewed, task.ID);

            return View("TaskForm", taskViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ViewTask(TaskViewModel taskViewModel)
        {
            Reviewable parent = null;
            Package package = null;
            var task = TaskService.GetByTaskId(taskViewModel.ID);

            if (!ModelState.IsValid)
            {
                package = GetPackage(task, ref parent);

                TaskCore.PopulateTaskViewModel(taskViewModel, GetCurrentUser(), parent, task);
                taskViewModel.ReadOnly = true;

                return View("TaskForm", taskViewModel);
            }

            try
            {
                task.Notes = taskViewModel.Notes;
                task.Complete = taskViewModel.Complete;

                task.CompleteDate = task.Complete ? SystemTime.Now : (DateTime?)null;


                TaskService.Update(task);
                AuditService.RecordActivity<Task>(AuditLog.EventType.Modified, task.ID);

                AddAlert(AlertViewModel.AlertType.Success, "Task updated successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error updating task.");
            }

            return Redirect(AppSession.Current.TaskFormReturnUrl);
        }

        #endregion

        #region CreateTask

        /// <summary>
        /// Render the task form in create mode.
        /// </summary>
        /// <param name="id">The topic ID.</param>

        [HttpGet]
        public ActionResult CreateTask(Guid id, TaskViewModel.ParentType parentType = TaskViewModel.ParentType.Topic)
        {
            if (TempData["InProLink"] != null && (bool)TempData["InProLink"])
                TempData.Keep("InProLink");

            var taskViewModel = new TaskViewModel();
            Reviewable parent;
            if (parentType == TaskViewModel.ParentType.Topic)
            {
                parent = new Topic();
                parent = TopicService.GetByTopicId(id);
            }
            else if (parentType == TaskViewModel.ParentType.Contact)
            {
                parent = new Contact();
                parent = ContactService.GetById(id);
            }
            else
            {
                parent = new ProjectInstance();
                parent = ProjectService.GetProjectInstanceById(id);
            }

            if (Request.Url.ToString().ToLower().Contains("framework"))
            {
                taskViewModel.FrameworkItem = true;
            }

            TaskCore.PopulateTaskViewModel(taskViewModel, GetCurrentUser(), parent);

            return View("TaskForm", taskViewModel);
        }

        /// <summary>
        /// Handle new task submission.
        /// </summary>

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateTask(TaskViewModel taskViewModel)
        {
            Reviewable parent = null;
            if (taskViewModel.TopicId != null)
            {
                parent = new Topic();
                parent = TopicService.GetByTopicId(taskViewModel.TopicId.Value);
            }
            else if (taskViewModel.ContactId != null)
            {
                parent = new Contact();
                parent = ContactService.GetById(taskViewModel.ContactId.Value);
            }
            else
            {
                parent = new ProjectInstance();
                parent = ProjectService.GetProjectInstanceById(taskViewModel.ProjectInstanceId.Value);
            }

            TaskCore.PopulateTaskViewModel(taskViewModel, GetCurrentUser(), parent);

            if (!ModelState.IsValid || TaskService.IsTaskNameUnique(taskViewModel.Name, parent))
            {

                if (TempData["InProLink"] != null && (bool)TempData["InProLink"])
                    TempData.Keep("InProLink");

                if (TaskService.IsTaskNameUnique(taskViewModel.Name, parent))
                    ModelState.AddModelError("Name", @"A task with this name already exists");

                return View("TaskForm", taskViewModel);
            }

            try
            {
                var task = TaskCore.CreateTask(parent, taskViewModel, (TempData["InProLink"] != null && (bool)TempData["InProLink"]));

                var returnUrl = Url.Action("EditTask", new { id = task.ID });

                // best solution to moment :(
                Response.Cookies.Add(new HttpCookie("editUrl", returnUrl));

                AppSession.Current.VoiceMessageManagerReturnUrl = returnUrl;
                AppSession.Current.AttachmentManagerReturnUrl = returnUrl;

                return GetSuccessResult(task.ID, "Task added successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error adding task.");
            }

            return Redirect(AppSession.Current.TaskFormReturnUrl);
        }

        #endregion

        #region EditTask

        /// <summary>
        /// Render the task form in edit mode.
        /// </summary>
        /// <param name="id">The task ID.</param>
        [Authorise("Task")]
        [HttpGet]
        public ActionResult EditTask(Guid id, bool onboarding = false)
        {
            Reviewable parent = null;
            var task = TaskService.GetByTaskId(id);

            var package = GetPackage(task, ref parent);

            var taskViewModel = Mapper.Map<TaskViewModel>(task);
            TaskCore.PopulateTaskViewModel(taskViewModel, GetCurrentUser(), parent, task);

            if (package != null)
            {
                SetModeratedViewModelFields(task, taskViewModel, package);
                taskViewModel.ShowLineManagerSection = true;
                taskViewModel.ShowHrManagerSection = true;

                if (PackageService.GetAllToDoItems(package.ID).Any(t => GeneralHelpers.GetUrlId(t.Url) == task.ID.ToString()) && package.UserOwnerId == GetCurrentUser().ID)
                {
                    var toDoItems = PackageService.GetAllToDoItems(package.ID).Where(t => GeneralHelpers.GetUrlId(t.Url) == task.ID.ToString());
                    foreach (var item in toDoItems)
                    {
                        item.DismissDate = SystemTime.Now;
                        PackageService.UpdateToDo(item);
                    }
                }
            }

            taskViewModel.OnboarderMode = onboarding;

            taskViewModel.ReadOnly = RoleAuthHelper.IsReadOnly("Task_Edit_Get");

            AppSession.Current.AttachmentManagerReturnUrl = Request.RawUrl;
            AppSession.Current.VoiceMessageManagerReturnUrl = Request.RawUrl;
            AppSession.Current.PackageTransferFormReturnUrl = Request.RawUrl;

            AuditService.RecordActivity<Task>(AuditLog.EventType.Viewed, task.ID);
            //var tempTask = TaskService.GetByTaskId(id);
            return View("TaskForm", taskViewModel);
        }

        /// <summary>
        /// Handle edited task submission.
        /// </summary>
        [Authorise("Task")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTask(TaskViewModel taskViewModel)
        {
            Reviewable parent = null;
            var task = TaskService.GetByTaskId(taskViewModel.ID);

            var package = GetPackage(task, ref parent);

            var currentUser = GetCurrentUser();

            var earliestStatWeek = AccountConfig.Settings.EarliestStartWeek;
            if (taskViewModel.WeekDue < earliestStatWeek && taskViewModel.WeekDue > TaskCore.MaxWeekDue)
            {
                ModelState.AddModelError("WeekDue", string.Format("Week Due must be between {0} and {1}", earliestStatWeek, TaskCore.MaxWeekDue));
            }

            TaskCore.PopulateTaskViewModel(taskViewModel, currentUser, parent, task);

            if (!ModelState.IsValid)
            {
                if (package != null)
                {
                    SetModeratedViewModelFields(task, taskViewModel, package);
                    taskViewModel.ShowLineManagerSection = true;
                    taskViewModel.ShowHrManagerSection = true;
                }

                return View("TaskForm", taskViewModel);
            }

            try
            {
                UpdateModerated(task, taskViewModel, package);
                TaskCore.UpdateTask(task, taskViewModel, currentUser);
                return GetSuccessResult(task.ID, "Task updated successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error updating task.");
            }

            return Redirect(AppSession.Current.TaskFormReturnUrl);
        }

        #endregion

        #region UpdateTask

        /// <summary>
        /// Update a task item
        /// </summary>
        /// <param name="id">The task ID.</param>
        /// <param name="newWeek">The new week number.</param>
        [HttpPost]
        public bool Update(Guid id, int newWeek)
        {
            Task task = TaskService.GetByTaskId(id);
            task.WeekDue = newWeek;
            TaskService.Update(task);

            return true;
        }

        #endregion

        #region RetireTask/ResumeTask

        [Authorise("Task")]
        [HttpGet]
        public ActionResult RetireTask(Guid id)
        {
            try
            {
                TaskService.Retire(id);
                AddAlert(AlertViewModel.AlertType.Success, "Task retired successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error retiring task.");
            }

            return RedirectToAction("EditTask", new { id = id });
        }

        [Authorise("Task")]
        [HttpGet]
        public ActionResult ResumeTask(Guid id)
        {
            try
            {
                TaskService.Restore(id);
                AddAlert(AlertViewModel.AlertType.Success, "Task resumed successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error resuming task.");
            }

            return RedirectToAction("EditTask", new { id = id });
        }

        #endregion

        public ActionResult Prioritisation(Guid id)
        {
            var package = PackageService.GetByPackageId(id);
            var tasks = TaskService.GetAllByPackageId(id);
            var viewmodel = new TasksViewModel();
            var handover = package.Handover != null ? HandoverService.GetById(package.Handover.ID) : null;

            viewmodel = new TasksViewModel { ItemID = package.ID, Tasks = tasks };

            if (HandoverService.IsOnboarderPeriodLocked(id))
                viewmodel.Start = (DateTime)package.Handover.OnboardingPeriod.StartPoint;

            ViewBag.Duration = handover != null ? handover.OnboardingPeriod.Duration : HandoverService.GetOnboardingDuration(id);
            ViewBag.ReadOnly = HandoverService.IsOnboarderPeriodLocked(id);

            return View("Prioritisation", viewmodel);
        }

        #endregion

        #region HELPERS

        private ActionResult GetSuccessResult(Guid taskId, string successMessage)
        {
            // Redirect to a specific action depending on the submit button pressed
            switch (Request.Form["Redirect"])
            {
                case "VoiceMessages":
                    return RedirectToAction("Index", "VoiceMessage", new { area = "", id = taskId, type = ItemType.Task });
                case "Attachments":
                    return RedirectToAction("Index", "Attachments", new { area = "", id = taskId, type = ItemType.Task });
            }

            AddAlert(AlertViewModel.AlertType.Success, successMessage);
            return Redirect(AppSession.Current.TaskFormReturnUrl);
        }

        private Package GetPackage(Task task, ref Reviewable parent)
        {
            if (task.ParentTopicId != null)
            {
                parent = new Topic();
                parent = TopicService.GetByTopicId(task.ParentTopicId.Value);
                return PackageService.GetByTopicId(parent.ID);
            }
            else if (task.ContactId != null)
            {
                parent = new Contact();
                parent = ContactService.GetById(task.ContactId.Value);
                return PackageService.GetByContactId(parent.ID);
            }
            else
            {
                parent = new ProjectInstance();
                parent = ProjectService.GetProjectInstanceById(task.ProjectInstanceId.Value);
                return PackageService.GetByProjectInstanceId(parent.ID);
            }
        }

        #endregion
    }
}
