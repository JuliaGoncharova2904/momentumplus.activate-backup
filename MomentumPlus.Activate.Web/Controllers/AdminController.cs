﻿using System.Web.Mvc;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Helpers;

namespace MomentumPlus.Activate.Web.Controllers
{
    [Authorise(Roles = RoleAuthHelper.Admin_General)]
    public class AdminController : BaseController
    {
        public AdminController(
            IAuditService auditService,
            IMasterListService masterListService,
            IPackageService packageService,
            IUserService userService
        )
            : base(
                    auditService,
                    masterListService,
                    packageService,
                    userService
                )
        {

        }

        /// <summary>
        /// Redirects to the <c>Index</c> action of the <c>Settings</c>
        /// controller in the <c>Admin</c> area.
        /// </summary>
        /// <returns>The redirect result object.</returns>
        public ActionResult Index()
        {
            return RedirectToAction(
                "Index",
                "Settings",
                new { area = "Admin" }
            );
        }

    }
}
