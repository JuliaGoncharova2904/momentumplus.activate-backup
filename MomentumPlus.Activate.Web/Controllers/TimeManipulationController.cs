﻿using System;
using System.Web.Mvc;
using MomentumPlus.Activate.Services.Helpers;
using MomentumPlus.Activate.Web.Models;

namespace MomentumPlus.Activate.Web.Controllers
{
    public class TimeManipulationController : Controller
    {
        [ChildActionOnly]
        public ActionResult TimeManipulationTool()
        {
            var viewModel = new TimeManipulationToolViewModel { DateTime = SystemTime.Now };
            return View("_TimeManipulationTool", viewModel);
        }

        public ActionResult UpdateDateTime(TimeManipulationToolViewModel viewModel)
        {
            var datetime = viewModel.DateTime;

            if (datetime.HasValue)
                SystemTime.Now = datetime.Value;

            string url = (Request != null && Request.UrlReferrer != null) ?
                Request.UrlReferrer.AbsolutePath : string.Empty;

            return Redirect(url);
        }
    }
}