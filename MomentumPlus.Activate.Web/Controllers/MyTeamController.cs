﻿using System.Web.Mvc;
using MomentumPlus.Activate.Services;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Core.Home;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Activate.Web.Models.Home;

namespace MomentumPlus.Activate.Web.Controllers
{
    [Authorise(Roles = RoleAuthHelper.MyPeople_General)]
    public class MyTeamController : BaseController
    {
        /// <summary>
        /// Number of triggers to display in the Approve Triggers list.
        /// </summary>
        private const int TriggersPageSize = 5;

        /// <summary>
        /// Number of triggers to display in the Aged Summary list.
        /// </summary>
        private const int AgedPageSize = 5;

        private SummaryCore Summary;

        public MyTeamController(
            IAuditService auditService,
            IContactService contactService,
            IMasterListService masterListService,
            IPackageService packageService,
            ITaskService taskService,
            IUserService userService,
            SummaryCore summaryCore
        )
            : base(
                  auditService,
                  masterListService,
                  packageService,
                  userService
              )
        {
            Summary = summaryCore;
        }

        public ActionResult Index(
            int triggersPageNo = 1,
            TriggerItemViewModel.SortColumn triggerSort = TriggerItemViewModel.SortColumn.NameOwner,
            bool triggerSortAscending = true,
            int agedPageNo = 1,
            AgedItemViewModel.SortColumn agedSort = AgedItemViewModel.SortColumn.NameOwner,
            bool agedSortAscending = true
        )
        {
            var triggers = Summary.GetTriggers(triggersPageNo, TriggersPageSize,
                triggerSort, triggerSortAscending);

            triggers.Sort = triggerSort;
            triggers.SortAscending = triggerSortAscending;

            var aged = Summary.GetAged(agedPageNo, AgedPageSize,
                agedSort, agedSortAscending);

            aged.Sort = agedSort;
            aged.SortAscending = agedSortAscending;

            AppSession.Current.PackageExamineReturnUrl = Request.RawUrl;
            AppSession.Current.TriggerFormReturnUrl = Request.RawUrl;
            AppSession.Current.HandoverFormReturnUrl = Request.RawUrl;

            return View("~/Views/MyTeam/Index.cshtml", new MyPeopleViewModel
            {
                Unapproved = triggers,
                Aged = aged,
            });
        }

    }
}
