﻿using System.Web.Mvc;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Helpers;

namespace MomentumPlus.Activate.Web.Controllers
{
    [Authorise(Roles = RoleAuthHelper.Framework_General)]
    public class FrameworkController : BaseController
    {
        /// <summary>
        /// Redirects to the <c>Critical</c> action of the <c>Framework</c>
        /// controller in the <c>Framework</c> area.
        /// </summary>
        /// <returns>The redirect result object.</returns>
        public ActionResult Index()
        {
            return RedirectToAction(
                "Critical",
                "Framework",
                new { area = "Framework" }
            );
        }

    }
}
