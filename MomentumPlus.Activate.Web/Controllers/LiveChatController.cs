﻿using System;
using System.Web;
using System.Web.Mvc;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;

namespace MomentumPlus.Activate.Web.Controllers
{
    public class LiveChatController : BaseController
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [XFrameOptionsFilter(true)]
        public ActionResult Chat()
        {
            var viewModel = new LiveChatViewModel
            {
                ChatService = AccountConfig.Settings.ChatService,

                ChatRooms = string.IsNullOrEmpty(AccountConfig.Settings.ChatRooms) ?
                            new string[] { } :
                            AccountConfig.Settings.ChatRooms.Split(','),

                ServerName = new Uri(AccountConfig.Settings.ChatService).Host,

                Nickname = HttpContext.User.Identity.Name
            };

            return View(viewModel);
        }
    }
}
