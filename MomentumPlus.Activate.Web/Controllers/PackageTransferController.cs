﻿using System;
using System.Linq;
using System.Web.Mvc;
using MomentumPlus.Activate.Services;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;

namespace MomentumPlus.Activate.Web.Controllers
{
    [Authorise(Roles = RoleAuthHelper.PackageTransfer_Controller_General)]
    public class PackageTransferController : BaseController
    {
        #region IOC

        private ITaskService TaskService;
        private ITopicService TopicService;
        private ITopicGroupService TopicGroupService;

        public PackageTransferController(
            IAuditService auditService,
            IMasterListService masterListService,
            IPackageService packageService,
            ITaskService taskService,
            ITopicService topicService,
            ITopicGroupService topicGroupService,
            IUserService userService
        )
            : base(
                  auditService,
                  masterListService,
                  packageService,
                  userService
              )
        {
            TaskService = taskService;
            TopicService = topicService;
            TopicGroupService = topicGroupService;
        }

        #endregion

        #region ACTIONS

        #region Index

        /// <summary>
        /// Render package transfer form.
        /// </summary>
        /// <param name="topicId">The topic ID if a topic is being transferred.</param>
        /// <param name="taskId">The task ID if a task is being transferred.</param>
        [HttpGet]
        public ActionResult Index(Guid? topicId = null, Guid? taskId = null, Guid? targetPackageId = null)
        {
            var viewModel = new PackageTransferViewModel
            {
                TopicId = topicId,
                TaskId = taskId,
                TargetPackageId = targetPackageId
            };

            PopulatePackageTransferViewModel(viewModel);

            return View(viewModel);
        }

        /// <summary>
        /// Handle package transfer form submission.
        /// </summary>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(PackageTransferViewModel viewModel)
        {
            try
            {
                // Ensure a target topic group or topic in the target package is specified
                if (!viewModel.TargetParentTopicGroupId.HasValue && !viewModel.TargetParentTopicId.HasValue)
                {
                    if (viewModel.TopicId.HasValue)
                    {
                        ModelState.AddModelError("TargetParentTopicGroupId", "Please select a target topic group.");
                    }
                    else if (viewModel.TaskId.HasValue)
                    {
                        ModelState.AddModelError("TargetParentTopicId", "Please select a target topic.");
                    }
                }

                if (!ModelState.IsValid)
                {
                    PopulatePackageTransferViewModel(viewModel);
                    return View(viewModel);
                }

                TaskService taskService = TaskService as TaskService;
                TopicService topicService = TopicService as TopicService;
                TopicGroupService topicGroupService = TopicGroupService as TopicGroupService;

                if (viewModel.TargetParentTopicGroupId.HasValue)
                {
                    var targetTopicGroup = topicGroupService.GetByTopicGroupId(viewModel.TargetParentTopicGroupId.Value);
                    var newTopic = topicService.Clone(viewModel.TopicId.Value);


                    if (targetTopicGroup.Topics.Any(t => t.Name == newTopic.Name))
                    {
                        return RedirectToAction("TopicConfirmation", new { topicGroupId = viewModel.TargetParentTopicGroupId, topicId = viewModel.TopicId });
                    }

                    newTopic.ParentTopicGroup = null;
                    newTopic.ParentTopicGroupId = targetTopicGroup.ID;
                    targetTopicGroup.Topics.Add(newTopic);
                    topicGroupService.Update(targetTopicGroup);
                }
                else if (viewModel.TargetParentTopicId.HasValue)
                {
                    var newTask = taskService.Clone(viewModel.TaskId.Value);
                    var targetTopic = topicService.GetByTopicId(viewModel.TargetParentTopicId.Value);
                    targetTopic.Tasks.Add(newTask);
                    topicService.Update(targetTopic);
                }

                AddAlert(AlertViewModel.AlertType.Success, "Item transferred successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Something went wrong. Please try again.");
            }

            return Redirect(AppSession.Current.PackageTransferFormReturnUrl);
        }

        #endregion

        public ActionResult TopicConfirmation(Guid topicGroupId, Guid topicId)
        {
            var topic = TopicService.GetByTopicId(topicId);

            var model = new TopicConfirmationViewModel()
            {
                OldName = topic.Name,
                NewName = topic.Name,
                TopicId = topicId,
                TopicGroupId = topicGroupId
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult TopicConfirmation(TopicConfirmationViewModel model)
        {
            var topic = TopicService.GetByTopicId(model.TopicId);
            var topicGroup = TopicGroupService.GetByTopicGroupId(model.TopicGroupId);

            if ((model.Action == "rename" && TopicGroupService.HasTopicWithName(model.TopicGroupId, model.NewName)) || string.IsNullOrWhiteSpace(model.Action))
            {
                if (!string.IsNullOrWhiteSpace(model.Action))
                    ModelState.AddModelError("NewName", "This name already exists");

                model.OldName = topic.Name;
                return View(model);
            }
            
            var newTopic = topic.DeepClone();

            switch (model.Action)
            {
                case "overwrite":
                    var duplicate = topicGroup.Topics.FirstOrDefault(t => t.Name == topic.Name);
                    if (duplicate != null)
                    {
                        topicGroup.Topics.Remove(duplicate);
                        TopicService.Delete(duplicate.ID);
                    }
                    break;
                case "rename":
                    newTopic.Name = model.NewName;
                    break;
            }
            newTopic.ParentTopicGroup = topicGroup;
            newTopic.ParentTopicGroupId = topicGroup.ID;
            topicGroup.Topics.Add(newTopic);

            TopicGroupService.Update(topicGroup);

            AddAlert(AlertViewModel.AlertType.Success, "Item copied successfully.");

            return Redirect(AppSession.Current.PackageTransferFormReturnUrl);
        }

        #endregion

        #region HELPERS

        private void PopulatePackageTransferViewModel(PackageTransferViewModel viewModel)
        {
            // Get details of source package/topic/task to display in view
            if (viewModel.TopicId.HasValue)
            {
                var topic = TopicService.GetByTopicId(viewModel.TopicId.Value);

                if (topic != null)
                {
                    var package = PackageService.GetByTopicId(viewModel.TopicId.Value);
                    viewModel.SourceDetails = package.DisplayName + " / " + topic.Name;
                }
            }
            else if (viewModel.TaskId.HasValue)
            {
                var task = TaskService.GetByTaskId(viewModel.TaskId.Value);

                if (task != null)
                {
                    var package = PackageService.GetByTaskId(viewModel.TaskId.Value);
                    viewModel.SourceDetails = package.DisplayName + " / " + task.Name;
                }
            }

            // Get target topic groups/topics in the selected target package
            if (viewModel.TargetPackageId.HasValue && viewModel.TopicId.HasValue)
            {
                viewModel.TopicGroups = TopicGroupService.GetByPackageId(viewModel.TargetPackageId.Value).Select(t =>
                    new SelectListItem { Text = t.Name, Value = t.ID.ToString() }
                );
            }
            else if (viewModel.TargetPackageId.HasValue && viewModel.TaskId.HasValue)
            {
                viewModel.Topics = TopicService.GetByPackageId(viewModel.TargetPackageId.Value).Select(t =>
                    new SelectListItem { Text = t.Name, Value = t.ID.ToString() }
                );
            }

            // Packages dropdown data
            viewModel.Packages = PackageService.GetAll().Select(p =>
                new SelectListItem { Text = p.DisplayName, Value = p.ID.ToString() }
            );
        }

        #endregion
    }
}
