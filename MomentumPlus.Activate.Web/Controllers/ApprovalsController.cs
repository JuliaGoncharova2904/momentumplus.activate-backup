﻿using System;
using System.Linq;
using System.Web.Mvc;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Models;
using AutoMapper;
using MomentumPlus.Activate.Web.Core;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models.Requests;
using MomentumPlus.Activate.Services;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Core.Models.Requests;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.Moderation;

namespace MomentumPlus.Activate.Web.Controllers
{
    [Authorise(Roles = RoleAuthHelper.Approvals_General)]
    public class ApprovalsController : BaseController
    {
        private ProjectCore ProjectCore;

        #region IOC

        private IContactService ContactService;
        private IModerationService ModerationService;
        private IPeopleService PeopleService;
        private IProjectService ProjectService;
        private IRequestService RequestService;

        public ApprovalsController(
            IAuditService auditService,
            IContactService contactService,
            IImageService imageService,
            IMasterListService masterListService,
            IModerationService moderationService,
            IPackageService packageService,
            IPeopleService peopleService,
            IProjectService projectService,
            IRequestService requestService,
            IUserService userService
        )
            : base(
                auditService,
                masterListService,
                packageService,
                userService
            )
        {
            ProjectCore = new ProjectCore(
                contactService,
                imageService,
                masterListService,
                packageService,
                projectService,
                requestService,
                moderationService
            );

            ContactService = contactService;
            ModerationService = moderationService;
            PeopleService = peopleService;
            ProjectService = projectService;
            RequestService = requestService;
        }

        #endregion

        #region ACTIONS

        /// <summary>
        /// Render approval index page, optionally filtered by package
        /// </summary>
        /// <param name="packageId">An optional package GUID</param>
        /// <returns></returns>
        public ActionResult Index(Guid? packageId = null)
        {
            // Create view model with package ID set and package dropdown data
            var viewModel = new ApprovalsViewModel
            {
                PackageId = packageId,
                Packages = PackageService.GetAllWithPermissions().ToList().Select(
                    p => new SelectListItem {Text = p.DisplayName, Value = p.ID.ToString()}
                    )
            };

            // Get the current user ID
            var currentUser = GetCurrentUser();
            Guid? currentUserId = currentUser.ID;

            if (currentUser.Role.RoleParsed == Role.RoleEnum.iHandoverAdmin ||
                currentUser.Role.RoleParsed == Role.RoleEnum.iHandoverAgent ||
                currentUser.Role.RoleParsed == Role.RoleEnum.Administrator)
            {
                // Set user ID to null for admins so that approvals for all packages
                // can be shown.
                currentUserId = null;
            }

            // Get requests relating to projects
            viewModel.NewProjectRequests
                = ModerationService.GetAllNotModerated<NewProjectRequest>(currentUserId, packageId);
            viewModel.ProjectInstanceTransferRequests
                = ModerationService.GetAllNotModerated<ProjectInstanceTransferRequest>(currentUserId, packageId);
            viewModel.ProjectInstanceRoleChangeRequests
                = ModerationService.GetAllNotModerated<ProjectInstanceRoleChangeRequest>(currentUserId, packageId);

            // Requests relating to staff
            viewModel.StaffTransferRequests
                = ModerationService.GetAllNotModerated<StaffTransferRequest>(currentUserId, packageId);

            // Get total count of items for approval
            viewModel.ItemsForApproval =
                viewModel.NewProjectRequests.Count() +
                viewModel.ProjectInstanceTransferRequests.Count() +
                viewModel.ProjectInstanceRoleChangeRequests.Count() +
                viewModel.StaffTransferRequests.Count();

            AppSession.Current.ProjectFormReturnUrl = Request.RawUrl;
            AppSession.Current.ProjectTransferFormReturnUrl = Request.RawUrl;
            AppSession.Current.ProjectInstanceRoleChangeFormReturnUrl = Request.RawUrl;

            return View(viewModel);
        }

        #region People

        /// <summary>
        /// Render person transfer request form
        /// </summary>
        /// <param name="id">GUID of the transfer request</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult PersonTransferRequest(Guid id)
        {
            var staffTransferRequest = ModerationService.GetById<StaffTransferRequest>(id);
            var viewModel = Mapper.Map<StaffTransferRequestViewModel>(staffTransferRequest);

            PopulateStaffTransferRequestViewModel(viewModel, staffTransferRequest);

            return View("StaffTransferForm", viewModel);
        }

        /// <summary>
        /// Handle transfer request form submission
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PersonTransferRequest(StaffTransferRequestViewModel viewModel)
        {
            var staffTransferRequest = PeopleService.GetStaffTransferRequestById(viewModel.ID);

            if (!ModelState.IsValid)
            {
                PopulateStaffTransferRequestViewModel(viewModel, staffTransferRequest);
                return View("StaffTransferForm", viewModel);
            }

            try
            {
             
                UpdateModerated(staffTransferRequest, viewModel, staffTransferRequest.Package);

                PeopleService.TransferStaff(staffTransferRequest, viewModel.DestinationContactId);

                AddAlert(AlertViewModel.AlertType.Success, SaveSuccessMessage);
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, SaveErrorMessage);
            }

            return RedirectToAction("Index");
        }

        #endregion

        #region Projects

        #region NewProject

        /// <summary>
        /// Render new project request form
        /// </summary>
        /// <param name="id">GUID of the project request</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult NewProject(Guid id)
        {
            var request = RequestService.GetRequest<NewProjectRequest>(id);

            if (request == null)
                return HttpNotFound();

            NewProjectRequestViewModel viewModel;
            if (AppSession.Current.FormViewModel != null && AppSession.Current.FormViewModel is NewProjectRequestViewModel && (AppSession.Current.FormViewModel as NewProjectRequestViewModel).Id == id)
            {
                // Set view model to view model from session
                viewModel = AppSession.Current.FormViewModel as NewProjectRequestViewModel;
                AppSession.Current.FormViewModel = null;
            }
            else
            {
                // Create new view model for the project
                var projectViewModel = new ProjectViewModel()
                {
                    Name = request.SuggestedName,
                    ProjectId = request.SuggetestedProjectId,
                };

                // Set project manager in view model to the appropriate contact
                if (request.ProjectInstance.Role != null && request.ProjectInstance.Role.Title == "Project Manager")
                {
                    var contact = ContactService.GetById(request.ProjectInstance.Package.UserOwnerId);

                    if (contact != null)
                    {
                        projectViewModel.ProjectManagerId = contact.ID;
                        projectViewModel.ProjectManager = Mapper.Map<ContactViewModel>(contact);
                    }

                }

                // Create view model for the project request
                viewModel = new NewProjectRequestViewModel()
                {
                    Id = id,
                    Project = projectViewModel
                };
            }

            ProjectCore.PopulateProjectViewModel(viewModel.Project);

            AppSession.Current.VoiceMessageManagerReturnUrl = Request.RawUrl;
            AppSession.Current.AttachmentManagerReturnUrl = Request.RawUrl;
            AppSession.Current.ContactFormReturnUrl = Request.RawUrl;

            return View(viewModel);
        }

        /// <summary>
        /// Handle new project request form submission
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NewProject(NewProjectRequestViewModel viewModel)
        {
            if (Request.Form["QuickAddRelationship"] != null)
            {
                return QuickAddRelationship(viewModel);
            }

            var request = RequestService.GetRequest<NewProjectRequest>(viewModel.Id);

            if (request == null)
                return HttpNotFound();

            var user = GetCurrentUser();

            if (user.Role.RoleParsed != Role.RoleEnum.Administrator &&
                user.Role.RoleParsed != Role.RoleEnum.iHandoverAdmin &&
                user.Role.RoleParsed != Role.RoleEnum.LineManager)
            {
                ModelState.AddModelError("", "You don't have permissions.");
            }

            if (!ModelState.IsValid && viewModel.Decision == ModerationDecision.Approved)
            {
                ProjectCore.PopulateProjectViewModel(viewModel.Project);

                return View(viewModel);
            }

            try
            {
                // Add new moderation for the new project request
                var moderation = new Moderation()
                {
                    CurrentRoleId = user.RoleId.Value,
                    UserId = user.ID,
                    Decision = viewModel.Decision.Value
                };

                request.Moderations.Add(moderation);

                // If approved then create the new project
                if (viewModel.Decision == ModerationDecision.Approved)
                {
                    var newProject = ProjectCore.CreateProject(viewModel.Project);

                    request.ProjectInstance.Project = newProject;               
                }

                RequestService.UpdateRequest(request);

                AddAlert(AlertViewModel.AlertType.Success, SaveSuccessMessage);
                return Redirect(AppSession.Current.ProjectFormReturnUrl);
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, SaveErrorMessage);
            }

            return Redirect(AppSession.Current.ProjectFormReturnUrl);
        }

        #endregion

        /// <summary>
        /// Save the view model in the session and redirect to the relationship form in framework
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private ActionResult QuickAddRelationship(NewProjectRequestViewModel model)
        {
            AppSession.Current.FormViewModel = model;

            return RedirectToAction("CreateRelationship", "Framework", new { Area = "Framework" });
        }

        #region ProjectTransferRequest

        /// <summary>
        /// Render project instance transfer request form
        /// </summary>
        /// <param name="id">GUID of the project transfer request</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ProjectTransferRequest(Guid id)
        {
            var projectInstanceTransferRequest = ModerationService.GetById<ProjectInstanceTransferRequest>(id);
            var viewModel = Mapper.Map<ProjectInstanceTransferRequestViewModel>(projectInstanceTransferRequest);

            PopulateProjectInstanceTransferRequestViewModel(viewModel, projectInstanceTransferRequest);

            return View("ProjectTransferForm", viewModel);
        }

        /// <summary>
        /// Handle submission of the project transfer request form
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ProjectTransferRequest(ProjectInstanceTransferRequestViewModel viewModel)
        {
            // Get project instance to be transferred along with the transfer request
            var projectInstance = ProjectService.GetProjectInstanceById(viewModel.ProjectInstanceId.Value);
            var projectInstanceTransferRequest
                    = ModerationService.GetById<ProjectInstanceTransferRequest>(viewModel.ID);

            if (!ModelState.IsValid)
            {
                PopulateProjectInstanceTransferRequestViewModel(viewModel, projectInstanceTransferRequest);

                viewModel.ProjectInstance = projectInstance;

                return View("ProjectTransferForm", viewModel);
            }

            try
            {
                UpdateModerated(projectInstanceTransferRequest, viewModel, projectInstanceTransferRequest.Package);

                if (projectInstanceTransferRequest.IsApproved())
                {
                    projectInstanceTransferRequest.Complete();
                }

                ModerationService.UpdateModerated(projectInstanceTransferRequest);

                AddAlert(AlertViewModel.AlertType.Success, SaveSuccessMessage);
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, SaveErrorMessage);
            }

            return Redirect(AppSession.Current.ProjectTransferFormReturnUrl);
        }

        #endregion

        #region ProjectRoleChangeRequest

        /// <summary>
        /// Render project role change request form
        /// </summary>
        /// <param name="id">GUID of the project role change request</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ProjectRoleChangeRequest(Guid id)
        {
            var projectInstanceRoleChangeRequest
                = ModerationService.GetById<ProjectInstanceRoleChangeRequest>(id);

            var viewModel = Mapper.Map<ProjectInstanceRoleChangeRequestViewModel>(projectInstanceRoleChangeRequest);

            SetModeratedViewModelFields(projectInstanceRoleChangeRequest, viewModel, projectInstanceRoleChangeRequest.Package);
            viewModel.ShowLineManagerSection = true;
            viewModel.ShowHrManagerSection = true;

            return View("ProjectInstanceRoleChangeRequestForm", viewModel);
        }

        /// <summary>
        /// Handle submission of project role change request form
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ProjectRoleChangeRequest(ProjectInstanceRoleChangeRequestViewModel viewModel)
        {
            var projectInstanceRoleChangeRequest
                    = ModerationService.GetById<ProjectInstanceRoleChangeRequest>(viewModel.ID);

            if (!ModelState.IsValid)
            {
                viewModel = Mapper.Map<ProjectInstanceRoleChangeRequestViewModel>(projectInstanceRoleChangeRequest);

                SetModeratedViewModelFields(projectInstanceRoleChangeRequest, viewModel, projectInstanceRoleChangeRequest.Package);
                viewModel.ShowLineManagerSection = true;
                viewModel.ShowHrManagerSection = true;
            }

            try
            {
                UpdateModerated(projectInstanceRoleChangeRequest, viewModel, projectInstanceRoleChangeRequest.Package);

                if (projectInstanceRoleChangeRequest.IsApproved())
                {
                    projectInstanceRoleChangeRequest.Complete();

                    var contact = ContactService.GetById(projectInstanceRoleChangeRequest.Package.UserOwnerId);

                    if (contact != null)
                    {
                        projectInstanceRoleChangeRequest.ProjectInstance.Project.ProjectManagerId = contact.ID;
                    }
                    else
                    {
                        //contact = new Contact { PersonId = projectInstanceRoleChangeRequest.Package.UserOwnerId, IsFramework = true };
                        //projectInstanceRoleChangeRequest.ProjectInstance.Project.ProjectManager = contact;
                    }
                }

                ModerationService.UpdateModerated(projectInstanceRoleChangeRequest);

                AddAlert(AlertViewModel.AlertType.Success, SaveSuccessMessage);
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, SaveErrorMessage);
            }

            return Redirect(AppSession.Current.ProjectInstanceRoleChangeFormReturnUrl);
        }

        #endregion

        #endregion

        #endregion

        #region HELPERS

        /// <summary>
        /// Load staff transfer request view model with the data required to render the view.
        /// </summary>
        /// <param name="viewModel">Staff transfer request view model</param>
        /// <param name="staffTransferRequest">staff transfer request entity</param>
        private void PopulateStaffTransferRequestViewModel(StaffTransferRequestViewModel viewModel, StaffTransferRequest staffTransferRequest)
        {
            viewModel.Packages = PackageService.GetAll().Where(p => p.ID != staffTransferRequest.SourceId && p.PeopleManagementEnabled).OrderBy(p => p.DisplayName).Select(p =>
                new SelectListItem { Text = p.DisplayName, Value = p.ID.ToString() }
            );

            viewModel.Contacts = ContactService.GetByPackageId(staffTransferRequest.DestinationId.Value).Select(c =>
                new SelectListItem { Text = c.FullName, Value = c.ID.ToString() }
            );

            viewModel.ShowLineManagerSection = true;
            viewModel.ShowHrManagerSection = true;
            viewModel.ApprovalMode = true;

            SetModeratedViewModelFields(staffTransferRequest, viewModel, staffTransferRequest.Package);
        }

        /// <summary>
        /// Load project transfer request view model with the data required to render the view.
        /// </summary>
        /// <param name="viewModel">Project transfer request view model</param>
        /// <param name="projectInstanceTransferRequest">Project transfer request entity</param>
        private void PopulateProjectInstanceTransferRequestViewModel(
            ProjectInstanceTransferRequestViewModel viewModel,
            ProjectInstanceTransferRequest projectInstanceTransferRequest
        )
        {
            viewModel.Packages = PackageService.GetAll().Where(p => p.ID != projectInstanceTransferRequest.SourceId && p.ProjectsEnabled).OrderBy(p => p.DisplayName).Select(p =>
                new SelectListItem { Text = p.DisplayName, Value = p.ID.ToString() }
            );

            SetModeratedViewModelFields(projectInstanceTransferRequest, viewModel, projectInstanceTransferRequest.Package);
            viewModel.ShowLineManagerSection = true;
        }

        [Obsolete]
        private ActionResult GetProjectInstanceSucessResult(Guid projectInstanceId, string successMessage)
        {
            // Redirect to a specific action depending on the submit button pressed
            switch (Request.Form["Redirect"])
            {
                case "VoiceMessages":
                    return RedirectToAction("Index", "VoiceMessage", new { area = "", id = projectInstanceId, type = ItemType.ProjectInstance });
                case "Attachments":
                    return RedirectToAction("Index", "Attachments", new { area = "", id = projectInstanceId, type = ItemType.ProjectInstance });
            }

            AddAlert(AlertViewModel.AlertType.Success, successMessage);
            return Redirect(AppSession.Current.ProjectInstanceFormReturnUrl);
        }

        #endregion
    }
}
