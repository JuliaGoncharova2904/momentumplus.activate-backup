﻿using System.Web.Mvc;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Helpers;

namespace MomentumPlus.Activate.Web.Controllers
{
    [Authorise(Roles = RoleAuthHelper.Packages_Controller_General)]
    public class PackagesController : BaseController
    {

        public PackagesController(IAuditService auditService,
            IMasterListService masterListService,
            IPackageService packageService,
            IUserService userService) : base(auditService,
            masterListService,
            packageService,
            userService)
        {
        }

        /// <summary>
        /// Redirects to the <c>Index</c> action of the <c>Employees</c>
        /// controller in the <c>Packages</c> area.
        /// </summary>
        /// <returns>The redirect result object.</returns>
        public ActionResult Index()
        {
            return RedirectToAction(
                "Index",
                "Workplaces",
                new { area = "Packages" }
            );
        }

    }
}
