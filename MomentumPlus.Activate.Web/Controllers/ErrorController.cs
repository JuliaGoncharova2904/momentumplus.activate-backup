﻿using System;
using System.Text;
using System.Web;
using MomentumPlus.Activate.Web.Models;
using System.Web.Mvc;
using NLog.Fluent;
using WebGrease.Activities;

namespace MomentumPlus.Activate.Web.Controllers
{
    public class ErrorController : Controller
    {
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        public ViewResult Error404()
        {
            var model = new Error();
            var html = new StringBuilder();

            model.Title = "Page Not Found";

            var message = new TagBuilder("p");
            message.SetInnerText("The page you are looking for could not be found.");

            html.Append(message);

            model.Message = new HtmlString(html.ToString()); ;

            return View("Error", model);
        }

        public ViewResult Error404_1()
        {
            var model = new Error();
            var html = new StringBuilder();

            model.Title = "File Not Found";

            var message = new TagBuilder("p");
            message.SetInnerText("The file path you requested does not refer to a valid file.");

            html.Append(message);

            model.Message = new HtmlString(html.ToString()); ;

            return View("Error", model);
        }

        public ViewResult Error500()
        {
            var model = new Error();
            var html = new StringBuilder();

            model.Title = "A problem has occurred";
            
            var message = new TagBuilder("p");
            message.SetInnerText("Please report this error to your iHandover Consultant.");
            html.Append(message);

            model.Message = new HtmlString(html.ToString()); ;

            return View("Error", model);
        }
    }
}
