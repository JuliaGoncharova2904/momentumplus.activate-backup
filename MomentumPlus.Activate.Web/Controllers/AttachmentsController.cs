﻿using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MomentumPlus.Activate.Services;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Core;
using MomentumPlus.Activate.Web.Extensions;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Controllers
{
    /// <summary>
    /// Controller for attachment management for all entities
    /// that can have attachments.
    /// </summary>
    public class AttachmentsController : BaseController
    {
        private AttachmentCore AttachmentCore;
        private DocumentLibraryCore DocumentLibraryCore;

        #region IOC

        private IAttachmentService AttachmentService;
        private IFileService FileService;

        /// <summary>
        /// Construct a new <c>TopicController</c> with the given service
        /// implementations.
        /// </summary>
        public AttachmentsController(
            IAttachmentService attachmentService,
            IAuditService auditService,
            IContactService contactService,
            IDocumentLibraryService documentLibraryService,
            IFileService fileService,
            IMasterListService masterListService,
            IMeetingService meetingService,
            IPackageService packageService,
            IPeopleService peopleService,
            IProjectService projectService,
            IPositionService positionService,
            ITaskService taskService,
            ITopicService topicService,
            IHandoverService handoverService,
            IUserService userService,
            AttachmentCore attachmentCore,
            DocumentLibraryCore documentLibraryCore
        ) : base(
            auditService,
            masterListService,
            packageService,
            userService)
        {
            AttachmentCore = attachmentCore;

            DocumentLibraryCore = documentLibraryCore;

            AttachmentService = attachmentService;
            FileService = fileService;
        }

        #endregion

        #region ACTIONS

        #region Index

        /// <summary>
        /// Render the attachment manager view for the given item.
        /// </summary>
        /// <param name="id">The ID of the item.</param>
        /// <param name="type">The <c>ItemType</c> corresponding to the item.</param>
        [Authorise("Attachment")]
        [HttpGet]
        public ActionResult Index(Guid id, ItemType type)
        {;
            var attachments = AttachmentCore.GetAttachmentsViewModel(id, type, GetCurrentPackage());

            AppSession.Current.AttachmentFormReturnUrl = Request.RawUrl;
            AppSession.Current.DocumentLibraryReturnUrl = Request.RawUrl;

            return View(attachments);
        }

        #endregion

        #region Create

        /// <summary>
        /// Render the attachment form in create mode.
        /// </summary>
        /// <param name="id">The ID of item to add an attachment to.</param>
        /// <param name="type">The <c>ItemType</c> corresponding to the item.</param>
        [Authorise("Attachment")]
        [HttpGet]
        public ActionResult Create(Guid id, ItemType type)
        {
            AttachmentViewModel viewModel = new AttachmentViewModel
            {
                ItemId = id,
                ItemType = type,
                FileTypes = AttachmentHelper.AllowedFileTypes,
                ChooseFileUpload = true
            };

            return View("AttachmentForm", viewModel);
        }

        /// <summary>
        /// Handle create attachment form submission.
        /// </summary>
        /// <param name="attachmentViewModel">The model containing the submitted form data.</param>
        /// <param name="theFile">The uploaded file.</param>
        [Authorise("Attachment")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AttachmentViewModel attachmentViewModel, HttpPostedFileBase theFile)
        {
            attachmentViewModel.FileTypes = AttachmentHelper.AllowedFileTypes.ConvertAll(f => f.ToLower());

            // Ensure that either a file or file location was entered
            if (theFile == null && string.IsNullOrWhiteSpace(attachmentViewModel.FilePath))
            {
                var errorMessage = "You must select a file to upload or specify a file location.";
                ModelState.AddModelError("PostedFile", errorMessage);
                ModelState.AddModelError("FilePath", errorMessage);
            }
            else if (theFile != null && !attachmentViewModel.FileTypes.Contains(theFile.FileName.Substring(theFile.FileName.LastIndexOf(".")).ToLower()))
            {
                // Check the file type is valid
                ModelState.AddModelError("PostedFile", @"This file type is not supported.");
            }

            if (attachmentViewModel.FilePath != null)
            {
                // Check for invalid characters in file path
                var invalidChars = Path.GetInvalidPathChars();
                var invalidCharsFound = false;
                foreach (var character in attachmentViewModel.FilePath.Where(character => invalidChars.Contains(character)))
                {
                    invalidCharsFound = true;
                }
                if (invalidCharsFound)
                {
                    ModelState.AddModelError("FilePath", @"Please enter a file path that does not contain illegal characters.");
                }
            }
            
            if (!ModelState.IsValid)
            {
                return View("AttachmentForm", attachmentViewModel);
            }

            try
            {
                AttachmentCore.CreateAttachment(attachmentViewModel, theFile);
                AddAlert(AlertViewModel.AlertType.Success, "Added successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error adding attachment.");
            }

            return Redirect(AppSession.Current.AttachmentFormReturnUrl);
        }

        #endregion

        #region Edit

        /// <summary>
        /// Render the attachment form in edit mode.
        /// </summary>
        /// <param name="id">The ID of item attachment.</param>
        /// <param name="itemId">The ID of item to add an attachment to.</param>
        /// <param name="type">The <c>ItemType</c> corresponding to the item.</param>
        [Authorise("AttachmentEdit")]
        [HttpGet]
        public ActionResult Edit(Guid id, Guid itemId, ItemType type)
        {
            var viewModel = AttachmentCore.GetAttachmentViewModel(id, itemId, type);
            return View("AttachmentForm", viewModel);
        }

        /// <summary>
        /// Handle edit attachment form submission.
        /// </summary>
        /// <param name="attachmentViewModel">The model containing the submitted form data.</param>
        [Authorise("AttachmentEdit")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AttachmentViewModel attachmentViewModel, HttpPostedFileBase theFile)
        {
            Attachment attachment = AttachmentService.GetByID(attachmentViewModel.ID);
            attachmentViewModel.FileTypes = AttachmentHelper.AllowedFileTypes;

            // Ensure that either a file or file location was entered
            if (theFile == null && !attachmentViewModel.FileId.HasValue && string.IsNullOrWhiteSpace(attachmentViewModel.FilePath))
            {
                var errorMessage = "You must select a file to upload or specify a file location.";
                ModelState.AddModelError("PostedFile", errorMessage);
                ModelState.AddModelError("FilePath", errorMessage);
            }
            else if (theFile != null && !theFile.IsValid())
            {
                ModelState.AddModelError("PostedFile", "This file type is not supported.");
            }

            if (!ModelState.IsValid)
            {
                return View("AttachmentForm", attachmentViewModel);
            }

            try
            {
                AttachmentCore.UpdateAttachment(attachment, attachmentViewModel, theFile);
                AddAlert(AlertViewModel.AlertType.Success, "Updated successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error editing attachment.");
            }

            return Redirect(AppSession.Current.AttachmentFormReturnUrl);
        }

        #endregion

        #region Download

        /// <summary>
        /// Download an attachment.
        /// </summary>
        /// <param name="id">The attachment ID.</param>
        [HttpGet]
        public ActionResult Download(Guid id)
        {
            var attachment = AttachmentService.GetByID(id);

            // If the attachment was linked from the document library then return the source attachment
            if (attachment.InLibrary && attachment.SourceId.HasValue)
            {
                attachment = AttachmentService.GetByID(attachment.SourceId.Value);
            }

            var file = FileService.GetFile(attachment.FileId.Value);
            return File(file.BinaryData, MimeTypeMap.GetMimeType(file.FileType), attachment.FileName);
        }

        #endregion

        #region RetireAttachment/ResumeAttachment

        /// <summary>
        /// Retire an attachment
        /// </summary>
        /// <param name="id">The attachment GUID</param>
        /// <param name="itemId">The attachment parent ID</param>
        /// <param name="type">Attachment parent type</param>
        /// <returns></returns>
        [Authorise("AttachmentEdit")]
        [HttpGet]
        public ActionResult RetireAttachment(Guid id, Guid itemId, ItemType type)
        {
            try
            {
                AttachmentService.Retire(id);
                AddAlert(AlertViewModel.AlertType.Success, "Attachment retired successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error retiring attachment.");
            }

            return RedirectToAction("Edit", new { id = id, itemId = itemId, type = type });
        }

        /// <summary>
        /// Resume an attachment
        /// </summary>
        /// <param name="id">The attachment GUID</param>
        /// <param name="itemId">The attachment parent ID</param>
        /// <param name="type">Attachment parent type</param>
        [Authorise("AttachmentEdit")]
        [HttpGet]
        public ActionResult ResumeAttachment(Guid id, Guid itemId, ItemType type)
        {
            try
            {
                AttachmentService.Restore(id);
                AddAlert(AlertViewModel.AlertType.Success, "Attachment resumed successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error resuming attachment.");
            }

            return RedirectToAction("Edit", new { id = id, itemId = itemId, type = type });
        }

        #endregion

        #region Unlink

        /// <summary>
        /// Unlink a library attachment from an entity
        /// </summary>
        /// <param name="id">Attachment ID</param>
        /// <param name="itemId">Attachment parent ID</param>
        /// <param name="type">Attachment parent type</param>
        /// <returns></returns>
        [Authorise("AttachmentEdit")]
        [HttpGet]
        public ActionResult Unlink(Guid id, Guid itemId, ItemType type)
        {
            try
            {
                DocumentLibraryCore.UnlinkDocument(id, itemId, type);
                AddAlert(AlertViewModel.AlertType.Success, "Link removed.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error removing link.");
            }

            return RedirectToAction("Index", new { id = itemId, type = type });
        }

        #endregion

        #endregion
    }
}
