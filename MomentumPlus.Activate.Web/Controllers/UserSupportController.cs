﻿using System.Web.Mvc;

namespace MomentumPlus.Activate.Web.Controllers
{
    public class UserSupportController : BaseController
    {
        /// <summary>
        /// Redirects to the <c>Index</c> action of the <c>UserSupport</c>
        /// controller in the <c>UserSupport</c> area.
        /// </summary>
        /// <returns>The redirect result object.</returns>
        public ActionResult Index()
        {
            return RedirectToAction(
                "Index",
                "UserSupport",
                new { area = "UserSupport" }
            );
        }

    }
}
