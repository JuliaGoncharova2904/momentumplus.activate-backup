﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Extensions;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Controllers
{
    [Authorise(Roles = RoleAuthHelper.MasterLists_General)]
    public class MasterListController : BaseController
    {
        #region CONSTANTS

        private const int PageSize = 8;

        #endregion

        #region IOC

        protected MasterList.ListType ListType;
        protected string SingularListType;

        public MasterListController(
            IAuditService auditService,
            IMasterListService masterListService,
            IPackageService packageService,
            IUserService userService,
            MasterList.ListType listType
        )
            : base(
                  auditService,
                  masterListService,
                  packageService,
                  userService
              )
        {
            ListType = listType;
            SingularListType = ListType.GetDescription().Singularise();
        }

        #endregion

        #region ACTIONS

        #region Index

        /// <summary>
        /// Main Index Action.
        /// </summary>
        [HttpGet]
        public ActionResult Index(
            string Query = "",
            int PageNo = 1,
            MasterList.SortColumn SortColumn = MasterList.SortColumn.Name,
            bool SortAscending = true
        )
        {
            IEnumerable<MasterList> results = MasterListService.Search(
                ListType, Query, PageNo, PageSize, SortColumn, SortAscending);

            IEnumerable<MasterListViewModel> viewModels = Mapper.Map<IEnumerable<MasterList>, IEnumerable<MasterListViewModel>>(results);

            MasterListsViewModel viewModel = new MasterListsViewModel()
            {
                Type = this.ListType,
                SingularListType = this.SingularListType,
                List = viewModels,
                PageNo = PageNo,
                PageSize = PageSize,
                TotalCount = MasterListService.SearchCount(ListType, Query),
                SortAscending = SortAscending
            };

            AppSession.Current.MasterListFormReturnUrl = Request.RawUrl;

            return View("MasterLists", viewModel);
        }

        #endregion

        #region Create

        [HttpGet]
        public ActionResult Create()
        {
            return View("MasterListForm", new MasterListViewModel()
                {
                    Type = this.ListType,
                    SingularListType = this.SingularListType
                });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(
            MasterListViewModel viewModel)
        {
            Validate(viewModel);

            if (!ModelState.IsValid)
            {
                return View("MasterListForm", viewModel);
            }

            MasterList newMasterList = new MasterList();

            if (viewModel.Type == MasterList.ListType.Companies &&
                viewModel.AdditionalField3 == true)
            {
                IEnumerable<MasterList> companies = MasterListService.GetAll(MasterList.ListType.Companies);
                bool isDefault = true;
                foreach (var company in companies)
                {
                    if (!String.IsNullOrWhiteSpace(company.AdditionalField3) && Boolean.Parse(company.AdditionalField3) == true)
                    {
                        ModelState.AddModelError("AdditionalField3", "The company \"" + company.Name + "\" is already a default company. Please navigate to the company's edit form and untick the checkbox.");

                        return View("MasterListForm", viewModel);
                    }
                    else
                    {
                        isDefault = false;
                    }
                }

                if (!isDefault)
                {
                    newMasterList.AdditionalField3 = viewModel.AdditionalField3.ToString();
                }
            }

            // Validate keyword to see if it already exists
            if (viewModel.Type == MasterList.ListType.Keywords)
            {
                var exists =
                    MasterListService.GetAll(MasterList.ListType.Keywords)
                        .Any(k => k.Name.Trim().ToLower() == viewModel.Name.Trim().ToLower());

                if (exists)
                {
                    ModelState.AddModelError("Name", string.Format("Keyword '{0}' already exists", viewModel.Name.Trim()));

                    return View("MasterListForm", viewModel);
                }
            }


            Mapper.Map<MasterListViewModel, MasterList>(viewModel, newMasterList);

            try
            {
                MasterListService.Add(newMasterList);

                AddAlert(AlertViewModel.AlertType.Success, SingularListType + " [" + newMasterList.Name + "] " + "was successfully created.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Warning, "Something went wrong. Please try again");
            }

            return RedirectToAction("Index");
        }

        #endregion

        #region Edit

        [HttpGet]
        public ActionResult Edit(Guid id)
        {
            MasterList existingMasterList = MasterListService.GetByMasterListId(id);

            if (existingMasterList == null)
            {
                AddAlert(AlertViewModel.AlertType.Warning, "Sorry, but this " + SingularListType + " does not exist.");

                return RedirectToAction("Index", ListType.ToString());
            }

            MasterListViewModel viewModel = Mapper.Map<MasterListViewModel>(existingMasterList);
            viewModel.Type = this.ListType;
            viewModel.SingularListType = this.SingularListType;

            viewModel.ReadOnly = RoleAuthHelper.IsReadOnly("MasterList_Edit_Get");

            return View("MasterListForm", viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MasterListViewModel viewModel)
        {
            Validate(viewModel);

            if (!ModelState.IsValid)
            {
                return View("MasterListForm", viewModel);
            }

            MasterList existingMasterList = MasterListService.GetByMasterListId(viewModel.ID);

            if (viewModel.Type == MasterList.ListType.Companies &&
                viewModel.AdditionalField3 == true)
            {
                IEnumerable<MasterList> companies = MasterListService.GetAll(MasterList.ListType.Companies);
                bool isDefault = true;
                foreach (var company in companies)
                {
                    if (!String.IsNullOrWhiteSpace(company.AdditionalField3) && Boolean.Parse(company.AdditionalField3) == true)
                    {
                        ModelState.AddModelError("AdditionalField3", "The company \"" + company.Name + "\" is already a default company. Please navigate to the company's edit form and untick the checkbox.");

                        return View("MasterListForm", viewModel);
                    }
                    else
                    {
                        isDefault = false;
                    }
                }

                if (!isDefault)
                {
                    existingMasterList.AdditionalField3 = viewModel.AdditionalField3.ToString();
                }
            }

            Mapper.Map<MasterListViewModel, MasterList>(viewModel, existingMasterList);

            try
            {
                MasterListService.Update(existingMasterList);

                AddAlert(AlertViewModel.AlertType.Success, SingularListType + " [" + existingMasterList.Name + "] " + "was successfully updated.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Warning, "Something went wrong. Please try again");
            }

            return RedirectToAction("Index");
        }

        #endregion

        #endregion

        #region HELPERS

        /// <summary>
        /// Master list item validation method - override this method in child
        /// controllers to add list-specific validation. Within this method
        /// errors can be added to the ModelState object which will be picked
        /// up and displayed in the view.
        /// </summary>
        /// <param name="name"></param>
        protected virtual void Validate(MasterListViewModel model)
        {
        }

        #endregion
    }
}
