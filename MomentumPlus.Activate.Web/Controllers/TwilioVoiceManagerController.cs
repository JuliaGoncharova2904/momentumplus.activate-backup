﻿using System;
using System.Net;
using System.Web.Http;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Controllers
{
    /// <summary>
    /// API Controller for accepting and managing incoming binary data.
    /// 
    /// The VoiceMessageController that manages the M+ Views can be found at
    /// MomentumPlus.Activate.Web.Controllers.VoiceMessageController.cs
    /// </summary>
    public class TwilioVoiceManagerController : ApiController
    {
        #region IOC

        private IVoiceMessageService VoiceMessageService;
        private IFileService FileService;

        public TwilioVoiceManagerController(
            IFileService fileService,
            IVoiceMessageService voiceMessageService
        )
        {
            VoiceMessageService = voiceMessageService;
            FileService = fileService;
        }

        #endregion

        /// <summary>
        /// Returns if the client is alive or not
        /// </summary>
        /// <returns></returns>
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public bool IsAlive(bool alive)
        {
            return true;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public bool SendSMS(string mySecretKey)
        {
            return false;
        }

        /// <summary>
        /// Accepts a notification that a single voice message exists.
        /// </summary>
        /// <param name="PinID">Guid representing the Pin# of the recording</param>
        /// <param name="RecURL">publically accessible URL to download the item from</param>
        /// <param name="Duration">length of the recording in seconds</param>
        /// <param name="DateRecorded">Date & time that the recording was created, parsed into DateTime? later</param>
        /// <returns></returns>
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public bool AcceptNotification(
            Guid PinID, string RecURL, int Duration, string DateRecorded)
        {
            try
            {
                using (WebClient client = new WebClient())
                {
                    if (ManageIncomingData(PinID, RecURL, Duration, DateRecorded, client))
                    {

                        return true;
                    }
                    return false;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        /// <summary>
        /// method to manage data being sent to Momentum Plus by the Core Service Twilio
        /// applciation. downlaods the bin data and stores it inside of the database. 
        /// </summary>
        /// <param name="CoreServicePinGuid">Local ID of the voice message in this database</param>
        /// <param name="RecURL">Public Recording URL</param>
        /// <param name="Duration">length in seconds</param>
        /// <param name="DateRecorded">Date recorded as a string - needs to be converted to DateTime?</param>
        /// <param name="client">Web Client to be used</param>
        /// <returns></returns>
        protected bool ManageIncomingData(Guid CoreServicePinGuid, string RecURL, int Duration, string DateRecorded, WebClient client)
        {
            try
            {
                byte[] audioDataBytes = null;
                VoiceMessage voiceMessageToModify = null;

                try
                {
                    //check to see if the voice message exists inside of this database.
                    voiceMessageToModify = VoiceMessageService.GetVoiceMessageByCoreServiceGuid(CoreServicePinGuid);
                    if (voiceMessageToModify == null)
                    {
                        return false;
                    }
                    if (voiceMessageToModify.AudioData)
                    {
                        //this item already has audio data, and is being re-notified

                        return false;
                    }
                }
                catch (Exception e)
                {
                    //there was no item in this db with that guid. 
                    return false;
                }

                //temp store the data into audioDataBytes
                try
                {
                    audioDataBytes = client.DownloadData(RecURL);
                }
                catch (Exception e)
                {
                    return false;
                }



                voiceMessageToModify.DurationSeconds = Duration;
                var file = new MomentumPlus.Core.Models.File();

                file.BinaryData = audioDataBytes;
                file.FileType = ".mp3";

                try
                {
                    //update the database with the bin data for this file
                    FileService.AddFile(file);
                    FileService.Update(file);
                }
                catch (Exception e)
                {
                    //didn't work
                    return false;
                }

                voiceMessageToModify.FileId = file.ID;

                if (voiceMessageToModify.HasExistingData == false)
                {
                    //set has existing data to true so that SignalR can pick it up
                    //and let the user know
                    voiceMessageToModify.AudioData = true;
                    voiceMessageToModify.RequiresNotification = true;
                    voiceMessageToModify.HasExistingData = true;
                }
                try
                {
                    //update the database with the VoiceMessage record
                    VoiceMessageService.Update(voiceMessageToModify);
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                //something went wrong
                return false;
            }
        }
    }
}
