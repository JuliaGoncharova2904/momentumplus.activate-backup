﻿using System.Web.Mvc;

namespace MomentumPlus.Activate.Web.Controllers
{
    public class FaqController : BaseController
    {
        /// <summary>
        /// Redirects to the <c>Index</c> action of the <c>User</c>
        /// controller in the <c>Faq</c> area.
        /// </summary>
        /// <returns>The redirect result object.</returns>
        public ActionResult Index()
        {
            return RedirectToAction(
                "Index",
                "User",
                new { area = "Faq" }
            );
        }

    }
}
