﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.People;

namespace MomentumPlus.Activate.Web.Controllers
{
    public class ViewAsSelectorController : BaseController
    {
        #region IOC

        public ViewAsSelectorController(
            IAuditService auditService,
            IMasterListService masterListService,
            IPackageService packageService,
            IUserService userService
        ) : base(
                auditService,
                masterListService,
                packageService,
                userService
            )
        {
        }

        #endregion

        [ChildActionOnly]
        public ActionResult ViewAsSelector()
        {
            var currentUser = GetCurrentUser();
            var isAdmin = currentUser.Role.RoleParsed == Role.RoleEnum.Administrator ||
                currentUser.Role.RoleParsed == Role.RoleEnum.Executive ||
                currentUser.Role.RoleParsed == Role.RoleEnum.iHandoverAdmin ||
                currentUser.Role.RoleParsed == Role.RoleEnum.iHandoverAgent;

            List<User> users = new List<User>();
            string selectedUser = "";

            if (isAdmin)
            {
                var user = new User();
                user.Contact = new Contact();
                user.FirstName = "Me";
                users.Add(user);

                users.AddRange(UserService.GetAll().Where(u =>
                    u.Role.RoleParsed == Role.RoleEnum.HRManager ||
                    u.Role.RoleParsed == Role.RoleEnum.LineManager ||
                    u.Role.RoleParsed == Role.RoleEnum.User
                ).ToList().OrderBy(u => u.FullName));

                if (AppSession.Current.ViewAsUser != null)
                {
                    selectedUser = AppSession.Current.ViewAsUser.FullName;
                }
            }

            return View(new ViewAsSelectorViewModel
                {
                    Users = users,
                    IsAdmin = isAdmin,
                    SelectedUser = selectedUser,
                });
        }

        [Authorise(Roles = RoleAuthHelper.ABSOLUTE_GENERAL_ACCESS)]
        [HttpGet]
        public ActionResult SelectUser(Guid id)
        {
            AppSession.Current.ViewAsUser = UserService.GetByUserId(id);
            return Redirect(Request.UrlReferrer.ToString());
        }

        [Authorise(Roles = RoleAuthHelper.ABSOLUTE_GENERAL_ACCESS)]
        [HttpGet]
        public ActionResult DeselectUser()
        {
            AppSession.Current.ViewAsUser = null;
            return Redirect(Request.UrlReferrer.ToString());
        }
    }
}
