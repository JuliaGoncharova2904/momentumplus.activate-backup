﻿using System;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Core;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Controllers
{
    public abstract class PositionController : PackageController
    {
        private PositionCore PositionCore;

        #region IOC

        public PositionController(
            IAuditService auditService,
            IContactService contactService,
            IMasterListService masterListService,
            IPackageService packageService,
            IPositionService positionService,
            IProjectService projectService,
            ITemplateService templateService,
            IHandoverService handoverService,
            IUserService userService,
            IWorkplaceService workplaceService,
            PositionCore positionCore,
            PackageCore packageCore
        )
            : base(
                  auditService,
                  contactService,
                  masterListService,
                  packageService,
                  positionService,
                  projectService,
                  templateService,
                  userService,
                  workplaceService,
                  packageCore
              )
        {
            PositionCore = positionCore;
        }

        #endregion

        #region ACTIONS

        #region Index

        /// <summary>
        /// Render the positions index view based on the given parameters.
        /// </summary>
        [HttpGet]
        public ActionResult Index(
            Guid? id = null,
            string Query = "",
            int PageNo = 1,
            Position.SortColumn SortColumn = Position.SortColumn.Name,
            bool SortAscending = true,
            Package.Status? packageStatus = null,
            bool retired = false
        )
        {
            var currentUser = GetCurrentUser();

            var positionsViewModel = PositionCore.GetPositionsViewModel(
                id,
                Query,
                PageNo,
                SortColumn,
                SortAscending,
                packageStatus,
                retired,
                currentUser
            );

            AppSession.Current.PositionFormReturnUrl = Request.RawUrl;
            AppSession.Current.PackageFormReturnUrl = Request.RawUrl;
            AppSession.Current.PackageExamineReturnUrl = Request.RawUrl;

            // Auto-select the first workplace if one exists
            if (positionsViewModel.Positions != null && positionsViewModel.Positions.Any() && positionsViewModel.SelectedPositionId == null)
            {
                positionsViewModel.SelectedPositionId = positionsViewModel.Positions.First().ID;
                Mapper.Map(PackageService.GetByPositionId(positionsViewModel.Positions.First().ID, packageStatus, retired)
                    .OrderBy(p => p.UserOwner.LastName)
                    .ThenBy(p => p.UserOwner.FirstName),
                    positionsViewModel.Packages);
            }

            return View(positionsViewModel);
        }

        #endregion

        #region CreatePosition

        /// <summary>
        /// Create position form.
        /// </summary>
        [Authorise(Roles = RoleAuthHelper.Position_Controller_Create)]
        [HttpGet]
        public ActionResult CreatePosition()
        {
            var viewModel = PositionCore.PopulatePositionViewModel(new PositionViewModel());
            return View("PositionForm", viewModel);
        }

        /// <summary>
        /// Handle create position form submission.
        /// </summary>
        [Authorise(Roles = RoleAuthHelper.Position_Controller_Create)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreatePosition(PositionViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                viewModel = PositionCore.PopulatePositionViewModel(viewModel);
                return View("PositionForm", viewModel);
            }

            try
            {
                var tempName = viewModel.Name;
                var positionExists = PositionService.Exists(tempName);

                if (positionExists)
                {
                    viewModel = PositionCore.PopulatePositionViewModel(viewModel);
                    ModelState.AddModelError("Name", "This Position already exists!");
                    return View("PositionForm", viewModel);
                }

                var newPosition = PositionCore.CreatePosition(viewModel);

                AddAlert(AlertViewModel.AlertType.Success, "Position" + " [" + newPosition.Name + "] " + "was successfully created.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Warning, "Something went wrong. Please try again");
            }

            return Redirect(AppSession.Current.PositionFormReturnUrl);
        }

        #endregion

        #region EditPosition

        /// <summary>
        /// Edit position form.
        /// </summary>
        /// <param name="id">The Position ID</param>
        [HttpGet]
        public ActionResult EditPosition(Guid id)
        {
            var position = PositionService.GetByPositionId(id);

            if (position == null)
            {
                AddAlert(AlertViewModel.AlertType.Warning, "Sorry, but this position does not exist.");
                return RedirectToAction("Index", "Positions");
            }

            var viewModel = Mapper.Map<PositionViewModel>(position);
            viewModel = PositionCore.PopulatePositionViewModel(viewModel);
            viewModel.ReadOnly = RoleAuthHelper.IsReadOnly("Position_Edit_Get");
            return View("PositionForm", viewModel);
        }

        /// <summary>
        /// Handle edit position form submission.
        /// </summary>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditPosition(PositionViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                viewModel = PositionCore.PopulatePositionViewModel(viewModel);
                return View("PositionForm", viewModel);
            }

            try
            {
                var position = PositionCore.UpdatePosition(viewModel);
                AddAlert(AlertViewModel.AlertType.Success, "Position" + " [" + position.Name + "] " + "was successfully updated.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Warning, "Something went wrong. Please try again");
            }

            return Redirect(AppSession.Current.PositionFormReturnUrl);
        }

        #endregion

        #endregion
    }
}