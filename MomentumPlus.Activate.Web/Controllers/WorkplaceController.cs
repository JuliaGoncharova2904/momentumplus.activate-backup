﻿using System;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Core;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Controllers
{
    public class WorkplaceController : PackageController
    {
        private WorkplaceCore WorkplaceCore;

        #region IOC

        public WorkplaceController(
            IAuditService auditService,
            IContactService contactService,
            IMasterListService masterListService,
            IPackageService packageService,
            IPositionService positionService,
            IProjectService projectService,
            ITemplateService templateService,
            IHandoverService handoverService,
            IUserService userService,
            IWorkplaceService workplaceService,
            WorkplaceCore workplaceCore,
            PackageCore packageCore
        )
            : base(
                  auditService,
                  contactService,
                  masterListService,
                  packageService,
                  positionService,
                  projectService,
                  templateService,
                  userService,
                  workplaceService,
                  packageCore)
        {
            WorkplaceCore = workplaceCore;
        }

        #endregion

        #region ACTIONS

        #region Index

        /// <summary>
        /// Render the workplaces index view based on the given parameters.
        /// </summary>
        [HttpGet]
        public ActionResult Index(
            Guid? id = null,
            string Query = "",
            int PageNo = 1,
            Workplace.SortColumn SortColumn = Workplace.SortColumn.Name,
            bool SortAscending = true,
            Package.Status? packageStatus = null,
            bool retired = false)
        {
            var workplacesViewModel = WorkplaceCore.GetWorkplacesViewModel(
                id,
                Query,
                PageNo,
                SortColumn,
                SortAscending,
                packageStatus,
                retired,
                GetCurrentUser()
            );

            AppSession.Current.WorkplaceFormReturnUrl = Request.RawUrl;
            AppSession.Current.PackageFormReturnUrl = Request.RawUrl;
            AppSession.Current.PackageExamineReturnUrl = Request.RawUrl;

            // Auto-select the first workplace if one exists
            if (workplacesViewModel.Workplaces != null && workplacesViewModel.Workplaces.Any() && workplacesViewModel.SelectedWorkplaceId == null)
            {
                workplacesViewModel.SelectedWorkplaceId = workplacesViewModel.Workplaces.First().ID;
                Mapper.Map(PackageService.GetByWorkplaceId(workplacesViewModel.Workplaces.First().ID, packageStatus, retired)
                    .OrderBy(t => t.UserOwner.LastName)
                    .ThenBy(t => t.UserOwner.FirstName),
                    workplacesViewModel.Packages);
            }

            return View(workplacesViewModel);
        }

        #endregion

        #region CreateWorkplace

        /// <summary>
        /// Create workplace form.
        /// </summary>
        [Authorise(Roles = RoleAuthHelper.Workplace_Controller_Create)]
        [HttpGet]
        public ActionResult CreateWorkplace()
        {
            var viewModel = new WorkplaceViewModel();
            return View("WorkplaceForm", viewModel);
        }

        /// <summary>
        /// Create workplace form submission.
        /// </summary>
        [Authorise(Roles = RoleAuthHelper.Workplace_Controller_Create)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateWorkplace(WorkplaceViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View("WorkplaceForm", viewModel);
            }

            try
            {
                var tempName = viewModel.Name;
                var workplaceExists = WorkplaceService.Exists(tempName);

                if (workplaceExists)
                {
                    ModelState.AddModelError("Name", "This Workplace already exists!");
                    return View("WorkplaceForm", viewModel);
                }

                var newWorkplace = WorkplaceCore.CreateWorkplace(viewModel);
                AddAlert(AlertViewModel.AlertType.Success, "Workplace" + " [" + newWorkplace.Name + "] " + "was successfully created.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Warning, "Something went wrong. Please try again");
            }

            return Redirect(AppSession.Current.WorkplaceFormReturnUrl);
        }

        #endregion

        #region EditWorkplace

        /// <summary>
        /// Render edit workplace form.
        /// </summary>
        /// <param name="id">The Workplace ID</param>
        [HttpGet]
        public ActionResult EditWorkplace(Guid id)
        {
            var workplace = WorkplaceService.GetByWorkplaceId(id);

            if (workplace == null)
            {
                AddAlert(AlertViewModel.AlertType.Warning, "Sorry, but this workplace does not exist.");
                return RedirectToAction("Index", "Workplaces");
            }

            // Create a new view model
            var viewModel = Mapper.Map<WorkplaceViewModel>(workplace);
            viewModel.ReadOnly = RoleAuthHelper.IsReadOnly("Workplace_Edit_Get");
            return View("WorkplaceForm", viewModel);
        }

        /// <summary>
        /// Handle edit workplace form submission.
        /// </summary>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditWorkplace(WorkplaceViewModel viewModel)
        {
            // If the model is not valid, do not save, but return
            if (!ModelState.IsValid)
            {
                return View("WorkplaceForm", viewModel);
            }

            try
            {
                var workplace = WorkplaceCore.UpdateWorkplace(viewModel);
                AddAlert(AlertViewModel.AlertType.Success, "Workplace" + " [" + workplace.Name + "] " + "was successfully updated.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Warning, "Something went wrong. Please try again");
            }

            return Redirect(AppSession.Current.WorkplaceFormReturnUrl);
        }

        #endregion

        #region GetWorkplaceInfo

        [Authorise(Roles = RoleAuthHelper.Workplace_Controller_GetInfo)]
        [HttpGet]
        public ActionResult GetWorkplaceInfo(Guid id)
        {
            var workplace = WorkplaceService.GetByWorkplaceId(id);

            if (workplace != null)
            {
                return Json(new
                {
                    address = workplace.Address,
                    mapLink = workplace.MapLink
                }, JsonRequestBehavior.AllowGet);
            }

            return null;
        }

        #endregion

        #endregion
    }
}
