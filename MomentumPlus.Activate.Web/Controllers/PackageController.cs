﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Core;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;
using NLog;

namespace MomentumPlus.Activate.Web.Controllers
{
    [Authorise(Roles = RoleAuthHelper.Package_Controller_General)]
    public class PackageController : BaseController
    {
        private readonly Logger _logger = LogManager.GetLogger("PerformanceMonitoring");

        private PackageCore PackageCore;

        #region IOC

        protected IPositionService PositionService;
        protected ITemplateService TemplateService;
        protected IWorkplaceService WorkplaceService;

        public PackageController(
            IAuditService auditService,
            IContactService contactService,
            IMasterListService masterListService,
            IPackageService packageService,
            IPositionService positionService,
            IProjectService projectService,
            ITemplateService templateService,
            IUserService userService,
            IWorkplaceService workplaceService,
            PackageCore packageCore
        )
            : base(
                  auditService,
                  masterListService,
                  packageService,
                  userService
              )
        {
            PackageCore = packageCore;

            PositionService = positionService;
            TemplateService = templateService;
            WorkplaceService = workplaceService;
        }

        #endregion

        #region ACTIONS

        #region CreatePackage

        /// <summary>
        /// Create package form view.
        /// </summary>
        [Authorise(Roles = RoleAuthHelper.Package_Controller_Create)]
        [EnforcePackageLimit]
        [HttpGet]
        public ActionResult CreatePackage(
            Guid? selectedUserId = null,
            Guid? selectedWorkplaceId = null,
            Guid? selectedPositionId = null
        )
        {
            var viewModel = PackageCore.PopulatePackageViewModel(
                selectedUserId,
                selectedWorkplaceId,
                selectedPositionId
            );
            viewModel.StartDate = DateTime.Now;
            return View("PackageForm", viewModel);
        }

        /// <summary>
        /// Create package form submission.
        /// </summary>
        [Authorise(Roles = RoleAuthHelper.Package_Controller_Create)]
        [EnforcePackageLimit]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreatePackage(PackageViewModel viewModel)
        {
            _logger.Debug("Creating Package");

            if (!ModelState.IsValid)
            {
                var pvm = PackageCore.PopulatePackageViewModel(viewModel: viewModel);
                return View("PackageForm", pvm);
            }

            if (viewModel.ContactIds == null)
            {
                viewModel.ContactIds = new List<Guid>();
            }

            // Add LM and HR to the clone list if they aren't already there
            if (viewModel.UserLineManagerId.HasValue && !viewModel.ContactIds.Contains(viewModel.UserLineManagerId.Value))
            {
                viewModel.ContactIds.Add(viewModel.UserLineManagerId.Value);
            }

            if (viewModel.UserHRManagerId.HasValue && !viewModel.ContactIds.Contains(viewModel.UserHRManagerId.Value))
            {
                viewModel.ContactIds.Add(viewModel.UserHRManagerId.Value);
            }

            try
            {
                var newPackage = PackageCore.CreatePackage(viewModel);

                AddAlert(AlertViewModel.AlertType.Success, "Package" + " [" + newPackage.DisplayName + "] " + "was successfully created.");

                var currentUser = GetCurrentUser();
                var packageOwner = UserService.GetByUserId(viewModel.UserOwnerId.Value);

                // If the package created belong to the current user then set this as the
                // current package.
                if (currentUser.ID == packageOwner.ID)
                {
                    SetCurrentPackage(newPackage);
                }
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Warning, "Something went wrong. Please try again");
            }

            _logger.Debug("Creating Package - Complete");

            return Redirect(AppSession.Current.PackageFormReturnUrl);
        }

        #endregion

        #region EditPackage

        /// <summary>
        /// Edit an existing package.
        /// </summary>
        /// <param name="id">The Package ID</param>
        [Authorise("Package", Roles = RoleAuthHelper.Package_Controller_Edit_GET)]
        [HttpGet]
        public ActionResult EditPackage(Guid id)
        {
            var package = PackageService.GetByPackageId(id);

            if (package == null)
            {
                AddAlert(AlertViewModel.AlertType.Warning, "Sorry, but this package does not exist.");
                return RedirectToAction("Index");
            }

            var viewModel = PackageCore.PopulatePackageViewModel(viewModel: Mapper.Map<PackageViewModel>(package));
            viewModel.ReadOnly = RoleAuthHelper.IsReadOnly("Package_Edit_Get");

            if (package.AncestorPackageId.HasValue)
            {
                viewModel.Ancestor = Mapper.Map<PackageViewModel>(PackageService.GetByPackageId(package.AncestorPackageId.Value));
            }

            AppSession.Current.TriggerFormReturnUrl = Request.RawUrl;
            AppSession.Current.HandoverFormReturnUrl = Request.RawUrl;
            AppSession.Current.HandoverFormReturnUrl = Request.RawUrl;

            ViewBag.IsLineManager = (GetCurrentUser().ID == package.UserLineManagerId) ? "true" : "false";
            return View("PackageForm", viewModel);
        }

        /// <summary>
        /// Edit package form submission.
        /// </summary>
        [Authorise("Package")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditPackage(PackageViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                var pvm = PackageCore.PopulatePackageViewModel(viewModel: viewModel);
                return View("PackageForm", pvm);
            }

            try
            {
                var package = PackageCore.UpdatePackage(viewModel);
                AddAlert(AlertViewModel.AlertType.Success, "Package" + " [" + package.DisplayName + "] " + "was successfully updated.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Warning, "Something went wrong. Please try again");
            }

            return Redirect(AppSession.Current.PackageFormReturnUrl);
        }

        #endregion

        #region RetirePackage/ResumePackage

        [Authorise("Package")]
        [HttpGet]
        public ActionResult RetirePackage(Guid id)
        {
            try
            {
                PackageService.Retire(id);
                AddAlert(AlertViewModel.AlertType.Success, "Package retired successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error retiring package.");
            }

            return RedirectToAction("EditPackage", new { id = id });
        }

        [Authorise("Package")]
        [EnforcePackageLimit]
        [HttpGet]
        public ActionResult ResumePackage(Guid id)
        {
            try
            {
                PackageService.Restore(id);
                AddAlert(AlertViewModel.AlertType.Success, "Package resumed successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error resuming package.");
            }

            return RedirectToAction("EditPackage", new { id = id });
        }

        #endregion

        #region PackageDefaults

        [HttpGet]
        public ActionResult GetEnabledModules(Guid positionId)
        {
            var position = PositionService.GetByPositionId(positionId);

            var result = new Dictionary<string, object>();
            result.Add("Relationships", true);
            result.Add("Projects", position.Template.ProjectsEnabled);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion

        [HttpGet]
        public ActionResult GetDetails(Guid id)
        {
            var person = PackageService.GetByPackageId(id).UserOwner;
            if (person != null)
            {
                return Json(new { name = person.FullName, email = person.Username }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null);
            }
        }
        #endregion
    }
}
