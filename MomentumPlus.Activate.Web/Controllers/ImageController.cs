﻿using System;
using System.Web.Mvc;
using MomentumPlus.Activate.Services;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Controllers
{
    public class ImageController : Controller
    {

        protected IImageService ImageService;

        public ImageController(IImageService imageService) : base()
        {
            ImageService = imageService;
        }

        public ActionResult Get(Guid? id)
        {
            if (!id.HasValue)
                return HttpNotFound();

            Image img = ImageService.GetImage(id.Value);

            if (img == null)
                return HttpNotFound();

            return File(img.BinaryData, img.ContentType);
        }

        public ActionResult GetUserImage(Guid userId)
        {
            UserService userService = new UserService(new MomentumContext());

            var user = userService.GetByUserId(userId);
            if (user.Contact.ImageId.HasValue)
            {
                Image img = ImageService.GetImage(user.Contact.ImageId.Value);

                return File(img.BinaryData, img.ContentType);
            }
            return null;
        }
    }
}
