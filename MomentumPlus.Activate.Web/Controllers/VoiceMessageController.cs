﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Lib.Web.Mvc;
using MomentumPlus.Activate.Services;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.CoreServiceVoiceApp;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Activate.Web.Extensions;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Controllers
{
    public class VoiceMessageController : BaseController
    {
        #region IOC

        private IVoiceMessageService VoiceMessageService;
        private IContactService ContactService;
        private IFileService FileService;
        private IMeetingService MeetingService;
        private IPeopleService PeopleService;
        private IProjectService ProjectService;
        private ISMSMessagesService SMSMessageService;
        private ITaskService TaskService;
        private ITopicService TopicService;
        private IHandoverService HandoverService;

        /// <summary>
        /// constructs a new <c>VoiceMessageController</c> with the given
        /// services
        /// </summary>
        public VoiceMessageController(
            IVoiceMessageService voiceMessageService,
            IContactService contactService,
            IFileService fileService,
            IMeetingService meetingService,
            IPackageService packageService,
            IPeopleService peopleService,
            IProjectService projectService,
            ISMSMessagesService sMSMessageService,
            ITaskService taskService,
            ITopicService topicService,
            IHandoverService handoverService,
            IUserService userService
        )
        {

            VoiceMessageService = voiceMessageService;
            FileService = fileService;

            ContactService = contactService;
            MeetingService = meetingService;
            PackageService = packageService;
            PeopleService = peopleService;
            ProjectService = projectService;
            SMSMessageService = sMSMessageService;
            TaskService = taskService;
            TopicService = topicService;
            HandoverService = handoverService;
            UserService = userService;
        }

        #endregion

        #region ACTIONS

        #region Index

        [Authorise("VoiceMessage")]
        [HttpGet]
        public ActionResult Index(Guid id, ItemType type)
        {

            ICollection<VoiceMessage> voiceMessages = new List<VoiceMessage>();
            string itemName = null;
            Package package = null;
            var currentPackage = GetCurrentPackage();

            switch (type)
            {
                case ItemType.Contact:
                    var contact = ContactService.GetById(id);
                    voiceMessages = contact.VoiceMessages;
                    itemName = contact.FullName;
                    break;
                case ItemType.Meeting:
                    var meeting = MeetingService.GetByMeetingId(id);
                    voiceMessages = meeting.VoiceMessages;
                    itemName = meeting.Name;
                    break;
                case ItemType.Task:
                    var task = TaskService.GetByTaskId(id);
                    voiceMessages = task.VoiceMessages;
                    itemName = task.Name;
                    package = PackageService.GetByTaskId(id);
                    break;
                case ItemType.Topic:
                    var topic = TopicService.GetByTopicId(id);
                    voiceMessages = topic.VoiceMessages;
                    itemName = topic.Name;
                    package = PackageService.GetByTopicId(id);
                    break;
                case ItemType.Project:
                    var project = ProjectService.GetProjectById(id);
                    voiceMessages = project.VoiceMessages;
                    itemName = project.Name;
                    break;
                case ItemType.ProjectInstance:
                    var projectInstance = ProjectService.GetProjectInstanceById(id);
                    voiceMessages = projectInstance.VoiceMessages;
                    itemName = projectInstance.Project != null ? projectInstance.Project.Name : projectInstance.NewProjectRequest.SuggestedName;
                    break;
                case ItemType.Staff:
                    var staff = PeopleService.GetStaff(id);
                    voiceMessages = staff.VoiceMessages;
                    //itemName = staff.FullName;
                    break;
            }

            foreach (var message in voiceMessages)
            {
                if (message.AudioData)
                {
                    message.HasExistingData = true;
                }
            }

            voiceMessages = voiceMessages.OrderBy(vm => vm.Created).ToList();

            var voiceMessageViewModels = Mapper.Map<IEnumerable<VoiceMessage>,
                IEnumerable<VoiceMessageViewModel>>(voiceMessages);
            AppSession.Current.VoiceMessageFormReturnUrl = Request.RawUrl;

            var vmpn = ConfigurationManager.AppSettings["VoiceMessagePhoneNumber"];

            // foreach item that has a null recording, let the user know that they have a null recording
            int i = 0;
            foreach (var message in voiceMessageViewModels)
            {
                if (message.HasExistingData == false && message.CreationMethod == 2)
                {
                    i++;
                }
            }
            if (i > 0)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("You have ");
                sb.Append(i);
                sb.Append(" Active PIN");
                if (i > 1)
                {
                    sb.Append("s");
                }
                sb.Append(" please call ");
                sb.Append(vmpn);
                sb.Append(" to record a new voice message");

                AddAlert(AlertViewModel.AlertType.Info, sb.ToString());
            }

            // When true will hide edit links from onboarder
            var onboarderView = package != null && currentPackage != null && currentPackage.IsOnboarderFor(package);

            return View(new VoiceMessagesViewModel
            {
                VoiceMessages = voiceMessageViewModels,
                ItemId = id,
                ItemType = type,
                ItemName = itemName,
                VoiceMessagePhoneNumber = vmpn,
                OnboarderView = onboarderView
            });

        }

        #endregion

        #region Create

        #region CreateFromUpload

        [Authorise("VoiceMessage")]
        [HttpGet]
        public ActionResult Create(Guid id, ItemType type)
        {
            return View("VoiceMessageForm", new VoiceMessageViewModel
            {
                ItemId = id,
                ItemType = type
            });
        }

        /// <summary>
        /// handle creation via upload of audio files
        /// </summary>
        /// <param name="voiceMessageViewModel"></param>
        /// <param name="theFile"></param>
        /// <returns></returns>
        [Authorise("VoiceMessage")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(VoiceMessageViewModel voiceMessageViewModel, HttpPostedFileBase PostedFile)
        {
            if (PostedFile == null)
            {
                ModelState.AddModelError("PostedFile", "You must select the file to upload");
                return View("VoiceMessageForm", voiceMessageViewModel);
            }

            if (!ModelState.IsValid)
            {
                return View("VoiceMessageForm", voiceMessageViewModel);
            }

            if (PostedFile.IsValid())
            {
                try
                {
                    System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
                    PostedFile.InputStream.CopyTo(memoryStream);
                    MomentumPlus.Core.Models.File file = new MomentumPlus.Core.Models.File()
                    {
                        BinaryData = memoryStream.ToArray(),
                        FileType = System.IO.Path.GetExtension(PostedFile.FileName)
                    };

                    VoiceMessage voiceMessage = new VoiceMessage
                    {
                        ID = Guid.NewGuid(),
                        FileId = file.ID,
                        Data = file,
                        Name = voiceMessageViewModel.Name,
                        SearchTags = voiceMessageViewModel.Tags,
                        HasExistingData = true,
                        CreationMethod = VoiceMessage.CreateMethod.Upload
                    };
                    switch (voiceMessageViewModel.ItemType)
                    {
                        case ItemType.Contact:
                            var contact = ContactService.GetById(voiceMessageViewModel.ItemId);
                            contact.VoiceMessages.Add(voiceMessage);
                            ContactService.Update(contact);
                            break;
                        case ItemType.Task:
                            var task = TaskService.GetByTaskId(voiceMessageViewModel.ItemId);
                            task.VoiceMessages.Add(voiceMessage);
                            TaskService.Update(task);
                            break;
                        case ItemType.Meeting:
                            var meeting = MeetingService.GetByMeetingId(voiceMessageViewModel.ItemId);
                            meeting.VoiceMessages.Add(voiceMessage);
                            MeetingService.Update(meeting);
                            break;
                        case ItemType.Topic:
                            var topic = TopicService.GetByTopicId(voiceMessageViewModel.ItemId);
                            topic.VoiceMessages.Add(voiceMessage);
                            TopicService.Update(topic);
                            break;
                        case ItemType.Project:
                            var project = ProjectService.GetProjectById(voiceMessageViewModel.ItemId);
                            project.VoiceMessages.Add(voiceMessage);
                            ProjectService.Update(project);
                            break;
                        case ItemType.ProjectInstance:
                            var projectInstance = ProjectService.GetProjectInstanceById(voiceMessageViewModel.ItemId);
                            projectInstance.VoiceMessages.Add(voiceMessage);
                            ProjectService.Update(projectInstance);
                            break;
                        case ItemType.Staff:
                            var staff = PeopleService.GetStaff(voiceMessageViewModel.ItemId);
                            staff.VoiceMessages.Add(voiceMessage);
                           // PeopleService.UpdatePerson<Staff>(staff);
                            break;
                    }
                    AddAlert(AlertViewModel.AlertType.Success, "Voice Message added successfully");


                }
                catch (Exception e)
                {
                    LogException(e);
                    AddAlert(AlertViewModel.AlertType.Danger, "Error adding Voice Message.");
                }
            }
            else
            {
                ModelState.AddModelError("PostedFile", "This file type is not supported.");

                return View("VoiceMessageForm", voiceMessageViewModel);
            }

            return Redirect(AppSession.Current.VoiceMessageFormReturnUrl);
        }

        #endregion

        #region CreateFromPC

        [Authorise("VoiceMessage")]
        [HttpGet]
        public ActionResult CreateFromPC(Guid id, ItemType type)
        {
            return View("CreateTypes/CreateFromPCForm", new VoiceMessageByPCViewModel
                {
                    ItemId = id,
                    ItemType = type
                });
        }

        /// <summary>
        /// creates a pin number with the CoreService, and sends the user straight to the
        /// softphone area 
        /// </summary>
        /// <param name="voiceMessageViewModel"></param>
        /// <returns></returns>
        [Authorise("VoiceMessage")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateFromPC(VoiceMessageByPCViewModel voiceMessageViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View("CreateTypes/CreateFromPCForm", voiceMessageViewModel);
            }
            else
            {
                var voiceMessage = Mapper.Map<VoiceMessage>(voiceMessageViewModel);
                try
                {
                    ICollection<VoiceMessage> voiceMessages = new List<VoiceMessage>();

                    Guid my_TwilioAccount;
                    string my_TwilioEndpoint;

                    CallManager _CallManager = new CallManager();
                    try
                    {
                        my_TwilioAccount = new Guid(MomentumPlus.Activate.Web.Helpers.AccountConfig.Settings.TwilioAccount);
                    }
                    catch (Exception ex)
                    {
                        LogException(ex, "This application has an invalid or no associated Twilio Account");
                        AddAlert(AlertViewModel.AlertType.Danger, "The voice message recording service is currently unavailable. Please try again soon");
                        return Redirect(AppSession.Current.VoiceMessageFormReturnUrl);
                    }
                    try
                    {
                        my_TwilioEndpoint = MomentumPlus.Activate.Web.Helpers.AccountConfig.Settings.TwilioEndpoint;
                        if (string.IsNullOrEmpty(my_TwilioEndpoint))
                        {
                            throw new Exception("Twilio Endpoint is not set, Twilio will not work with an invalid endpoint");
                        }
                    }
                    catch (Exception e)
                    {
                        LogException(e);
                        AddAlert(AlertViewModel.AlertType.Danger, "The voice message recording service is currently unavailable. Please try again soon");
                        return Redirect(AppSession.Current.VoiceMessageFormReturnUrl);
                    }

                    //var pinDetails = 

                    var pinDetails = _CallManager.CreateNewCall(my_TwilioAccount, my_TwilioEndpoint);
                    voiceMessage.ID = Guid.NewGuid();
                    voiceMessage.CreationMethod = VoiceMessage.CreateMethod.VoipPhone;
                    voiceMessage.CoreService_Guid = pinDetails.PinID;
                    voiceMessage.CoreService_Pin = pinDetails.PinNumber;

                    voiceMessageViewModel.pinNumber = pinDetails.PinNumber;
                    voiceMessageViewModel.SearchTags = voiceMessage.SearchTags;
                    //twilio sections


                    //CoreServiceVoiceApp.CallManager CSVA = new CallManager();
                    //voiceMessage.TwilioToken = CSVA.GetJsForTwilioVoipPhone().GeneratedCapabilities;

                    switch (voiceMessageViewModel.ItemType)
                    {
                        case ItemType.Contact:
                            var contact = ContactService.GetById(voiceMessageViewModel.ItemId);
                            contact.VoiceMessages.Add(voiceMessage);
                            ContactService.Update(contact);
                            voiceMessages = contact.VoiceMessages;
                            break;
                        case ItemType.Task:
                            var task = TaskService.GetByTaskId(voiceMessageViewModel.ItemId);
                            task.VoiceMessages.Add(voiceMessage);
                            TaskService.Update(task);
                            voiceMessages = task.VoiceMessages;
                            break;
                        case ItemType.Topic:
                            var topic = TopicService.GetByTopicId(voiceMessageViewModel.ItemId);
                            topic.VoiceMessages.Add(voiceMessage);
                            TopicService.Update(topic);
                            voiceMessages = topic.VoiceMessages;
                            break;
                        case ItemType.Meeting:
                            var meeting = MeetingService.GetByMeetingId(voiceMessageViewModel.ItemId);
                            meeting.VoiceMessages.Add(voiceMessage);
                            MeetingService.Update(meeting);
                            break;
                        case ItemType.Project:
                            var project = ProjectService.GetProjectById(voiceMessageViewModel.ItemId);
                            project.VoiceMessages.Add(voiceMessage);
                            ProjectService.Update(project);
                            voiceMessages = project.VoiceMessages;
                            break;
                        case ItemType.ProjectInstance:
                            var projectInstance = ProjectService.GetProjectInstanceById(voiceMessageViewModel.ItemId);
                            projectInstance.VoiceMessages.Add(voiceMessage);
                            ProjectService.Update(projectInstance);
                            voiceMessages = projectInstance.VoiceMessages;
                            break;
                        case ItemType.Staff:
                            var staff = PeopleService.GetStaff(voiceMessageViewModel.ItemId);
                            staff.VoiceMessages.Add(voiceMessage);
                            //PeopleService.UpdatePerson<Staff>(staff);
                            voiceMessages = staff.VoiceMessages;
                            break;
                    }
                    voiceMessageViewModel.VoiceMessages = Mapper.Map<IEnumerable<VoiceMessage>,
                        IEnumerable<VoiceMessageViewModel>>(voiceMessages);

                }
                catch (Exception ex)
                {
                    LogException(ex);
                    AddAlert(AlertViewModel.AlertType.Danger, "The voice message recording service is currently unavailable. Please try again soon");
                    return Redirect(AppSession.Current.VoiceMessageFormReturnUrl);
                }

                //CoreServiceVoiceApp. _VA = new CoreServiceVoiceApp();
                //CoreServiceVoiceApp.TwilioToken token = CoreServiceVoiceApp.GetJsForTwilioVoipPhone();
                AddAlert(AlertViewModel.AlertType.Info, "We are connecting you to your recording area, please wait");
                return View("VoipFone", voiceMessageViewModel);

                //return View("CreateTypes/CreateFromPCForm", voiceMessageViewModel);
            }
        }
        #endregion

        #region CreateFromPhone

        [Authorise("VoiceMessage")]
        [HttpGet]
        public ActionResult CreateFromPhone(Guid id, ItemType type)
        {
            return View("CreateTypes/CreateFromPhoneForm", new VoiceMessageByPhoneViewModel
            {
                ItemId = id,
                ItemType = type
            });
        }

        [Authorise("VoiceMessage")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateFromPhone(VoiceMessageByPhoneViewModel voiceMessageViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View("CreateTypes/CreateFromPhoneForm", voiceMessageViewModel);
            }
            else
            {
                /// attempts to get in touch with the CoreService and request a pin number
                var voiceMessage = Mapper.Map<VoiceMessage>(voiceMessageViewModel);
                try
                {
                    Guid my_TwilioAccount;
                    string my_TwilioEndpoint;

                    CallManager _CallManager = new CallManager();
                    try
                    {
                        my_TwilioAccount = new Guid(MomentumPlus.Activate.Web.Helpers.AccountConfig.Settings.TwilioAccount);
                    }
                    catch (Exception ex)
                    {
                        LogException(ex, "This application has an invalid or no associated Twilio Account");
                        AddAlert(AlertViewModel.AlertType.Danger, "The voice message recording service is currently unavailable. Please try again soon");
                        return Redirect(AppSession.Current.VoiceMessageFormReturnUrl);
                    }
                    try
                    {
                        my_TwilioEndpoint = MomentumPlus.Activate.Web.Helpers.AccountConfig.Settings.TwilioEndpoint;
                        if (string.IsNullOrEmpty(my_TwilioEndpoint))
                        {
                            throw new Exception("Twilio Endpoint is not set");
                        }
                    }
                    catch (Exception e)
                    {
                        LogException(e);
                        AddAlert(AlertViewModel.AlertType.Danger, "The voice message recording service is currently unavailable. Please try again soon");
                        return Redirect(AppSession.Current.VoiceMessageFormReturnUrl);
                    }

                    var pinDetails = _CallManager.CreateNewCall(my_TwilioAccount, my_TwilioEndpoint);
                    voiceMessage.ID = Guid.NewGuid();
                    voiceMessage.CreationMethod = VoiceMessage.CreateMethod.PhoneCall;
                    voiceMessage.CoreService_Guid = pinDetails.PinID;
                    voiceMessage.CoreService_Pin = pinDetails.PinNumber;
                    voiceMessageViewModel.SearchTags = voiceMessage.SearchTags;

                    switch (voiceMessageViewModel.ItemType)
                    {
                        case ItemType.Contact:
                            var contact = ContactService.GetById(voiceMessageViewModel.ItemId);
                            contact.VoiceMessages.Add(voiceMessage);
                            ContactService.Update(contact);
                            break;
                        case ItemType.Task:
                            var task = TaskService.GetByTaskId(voiceMessageViewModel.ItemId);
                            task.VoiceMessages.Add(voiceMessage);
                            TaskService.Update(task);
                            break;
                        case ItemType.Topic:
                            var topic = TopicService.GetByTopicId(voiceMessageViewModel.ItemId);
                            topic.VoiceMessages.Add(voiceMessage);
                            TopicService.Update(topic);
                            break;
                        case ItemType.Meeting:
                            var meeting = MeetingService.GetByMeetingId(voiceMessageViewModel.ItemId);
                            meeting.VoiceMessages.Add(voiceMessage);
                            MeetingService.Update(meeting);
                            break;
                        case ItemType.Project:
                            var project = ProjectService.GetProjectById(voiceMessageViewModel.ItemId);
                            project.VoiceMessages.Add(voiceMessage);
                            ProjectService.Update(project);
                            break;
                        case ItemType.ProjectInstance:
                            var projectInstance = ProjectService.GetProjectInstanceById(voiceMessageViewModel.ItemId);
                            projectInstance.VoiceMessages.Add(voiceMessage);
                            ProjectService.Update(projectInstance);
                            break;
                        case ItemType.Staff:
                            var staff = PeopleService.GetStaff(voiceMessageViewModel.ItemId);
                            staff.VoiceMessages.Add(voiceMessage);
                           // PeopleService.UpdatePerson<Staff>(staff);
                            break;
                    }
                }
                catch (Exception ex)
                {
                    LogException(ex);
                    AddAlert(AlertViewModel.AlertType.Danger, "The voice message recording service is currently unavailable. Please try again soon");
                    return Redirect(AppSession.Current.VoiceMessageFormReturnUrl);
                }

                ///redirect to view of just this one call with a pin number attached
                /// 
                AddAlert(AlertViewModel.AlertType.Success, "Successfully created your PIN: " + voiceMessage.CoreService_Pin);
                return Redirect(AppSession.Current.VoiceMessageFormReturnUrl);
                //return View("SingleVoiceMessage", voiceMessageViewModel);


                ///do logic...
                ///request a new pin number, 
                ///
                /// create a new voice message, including the tags etc etc for this blah

                //return View("CreateTypes/CreateFromPhoneForm", voiceMessageViewModel);
            }
        }

        #region SendSMS

        [Authorise("VoiceMessage")]
        [HttpGet]
        public ActionResult SendSMSMessage(Guid id, ItemType type, Guid itemId)
        {
            Package package = null;
            //get voice message's user
            switch (type)
            {
                case ItemType.Contact:
                    package = PackageService.GetByContactId(itemId);
                    break;
                case ItemType.Meeting:
                    var meeting = MeetingService.GetByMeetingId(itemId);
                    package = PackageService.GetByContactId(meeting.ParentContact.ID);
                    break;
                case ItemType.Task:
                    package = PackageService.GetByTaskId(itemId);
                    break;
                case ItemType.Topic:
                    package = PackageService.GetByTopicId(itemId);
                    break;
                case ItemType.ProjectInstance:
                    package = PackageService.GetByProjectInstanceId(itemId);
                    break;
                case ItemType.Staff:
                    package = PackageService.GetByStaffId(itemId);
                    break;
            }
            var user = package.UserOwner;
            if (user.SMSMessagesToday > Convert.ToInt32(MomentumPlus.Activate.Web.Helpers.AccountConfig.Settings.NumberSMSPerUserPerDay))
            {
                AddAlert(AlertViewModel.AlertType.Danger, "The user " + user.FirstName + " " + user.LastName + " has already received the maximum number of SMS messages today(" + MomentumPlus.Activate.Web.Helpers.AccountConfig.Settings.NumberSMSPerUserPerDay + ")");
                return Redirect(AppSession.Current.VoiceMessageFormReturnUrl);
            }
            try
            {
                var SMSToSend = new SMSMessages();
                var SMSToSendUser = AppSession.Current.CurrentUser;
                var SMSToSendNumber = SMSToSendUser.Mobile;
                var SMSFromSendNumber = MomentumPlus.Activate.Web.Helpers.AccountConfig.Settings.TwilioPhoneNumber;
                var message = "Please call " + SMSFromSendNumber + " using PIN: " + VoiceMessageService.GetByID(id).CoreService_Pin + " to record a voice message";

                SMSMessageService.SendSMS(SMSToSendUser, SMSToSendNumber, SMSFromSendNumber, message);


                user.LastSMSMessage = DateTime.Now;
                user.SMSMessagesToday++;
                //user = UserService.UpdateUser(user);

                AddAlert(AlertViewModel.AlertType.Success, "SMS message is being sent to " + SMSToSendNumber);
                return Redirect(AppSession.Current.VoiceMessageFormReturnUrl);
            }
            catch (Exception e)
            {
                LogException(e);
                AddAlert(AlertViewModel.AlertType.Danger, "The SMS message could not be sent at this time");
                return Redirect(AppSession.Current.VoiceMessageFormReturnUrl);
            }
        }

        #endregion

        #endregion

        #endregion

        #region PLAY

        //[HttpPost]
        [HttpGet]
        //[ValidateAntiForgeryToken]
        /// <summary>
        /// Plays an audio file
        /// </summary>
        /// <param name="_CoreServiceGuid"></param>
        /// <returns></returns>
        public ActionResult Play(Guid id)
        {
            VoiceMessageService _vms = VoiceMessageService as VoiceMessageService;
            Services.FileService _fs = FileService as Services.FileService;

            using (var client = new WebClient())
            {
                var voiceMessageToReturn =
                    _vms.GetByID(id);

                if (voiceMessageToReturn != null && voiceMessageToReturn.FileId != null)
                {
                    var theGuid = new Guid(voiceMessageToReturn.FileId.ToString());
                    var fileToSend = _fs.GetFile(theGuid);
                    byte[] audioDataBytes = fileToSend.BinaryData;
                    if (audioDataBytes != null)
                    {
                        MemoryStream myStream = new MemoryStream(audioDataBytes);
                        RangeFileStreamResult fsr = new RangeFileStreamResult(myStream,
                            DocumentHelper.GetFileType(fileToSend.FileType), voiceMessageToReturn.Name, DateTime.Now);
                        return fsr;
                    }
                    AddAlert(AlertViewModel.AlertType.Danger, "This voice message is unavailable to download, if you have recently completed the call, it may take up to 10 minutes");
                    return Redirect(AppSession.Current.VoiceMessageFormReturnUrl);
                }

                else
                {
                    AddAlert(AlertViewModel.AlertType.Danger, "One or more voice messages are currently unavailable to play. Please try again soon.");
                    return Redirect(AppSession.Current.VoiceMessageFormReturnUrl);
                }
            }
        }

        [HttpGet]
        public ActionResult Download(Guid id)
        {
            Services.VoiceMessageService _vms = VoiceMessageService as Services.VoiceMessageService;
            Services.FileService _fs = FileService as Services.FileService;

            // get attachment stream from FileByID
            var voiceMessage = _vms.GetByID(id);
            var theGuid = new Guid(voiceMessage.FileId.ToString());
            var voiceMessageContent = _fs.GetFile(voiceMessage.FileId.Value);
            var voiceMessageFileExtension = _fs.GetFile(voiceMessage.FileId.Value).FileType;


            //Response.AppendHeader("Content-Disposition", "attachment");
            Response.AppendHeader("filename", voiceMessage.Name + voiceMessage);// + voiceMessageContent.FileType);

            var fileType = DocumentHelper.GetFileType(voiceMessageContent.FileType);

            var qualifiedFileName = voiceMessage.Name + voiceMessageFileExtension;
            return File(voiceMessageContent.BinaryData, DocumentHelper.GetFileType(voiceMessageContent.FileType), qualifiedFileName);


        }

        #endregion

        #region DELETE

        /// <summary>
        /// Deletes an item from the voice message repository.
        /// RedirectsToAction Index, needs to redirect
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Delete(Guid id, Guid itemID, ItemType itemType)
        {
            VoiceMessageService.Delete(id);
            AddAlert(AlertViewModel.AlertType.Success, "Voice Message removed successfully.");
            return RedirectToAction("Index", new { id = itemID, type = itemType });
        }

        #endregion

        #region EDIT

        [Authorise("VoiceMessageEdit")]
        [HttpGet]
        public ActionResult Edit(Guid id, Guid itemID, ItemType type)
        {
            try
            {
                // Get voice message from the database
                VoiceMessage vm = VoiceMessageService.GetByID(id);

                // Populate the view model
                VoiceMessageViewModel viewModel = Mapper.Map<VoiceMessage, VoiceMessageViewModel>(vm);
                viewModel.ItemId = itemID;
                viewModel.ItemType = type;
                viewModel.Tags = vm.SearchTags;
                viewModel.HasExistingData = vm.HasExistingData || vm.AudioData;
                return View("VoiceMessageForm", viewModel);
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Warning, "Something went wrong.");
                return RedirectToAction("Index", new { id = itemID, type = type });
            }
        }

        [Authorise("VoiceMessageEdit")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(VoiceMessageViewModel voiceMessageViewModel, HttpPostedFileBase PostedFile)
        {
            var voiceMessage = VoiceMessageService.GetByID(voiceMessageViewModel.ID);

            if (!ModelState.IsValid)
            {
                voiceMessageViewModel.HasExistingData = voiceMessage.HasExistingData || voiceMessage.AudioData;
                return View("VoiceMessageForm", voiceMessageViewModel);
            }
            try
            {
                voiceMessage.Name = voiceMessageViewModel.Name;
                voiceMessage.SearchTags = voiceMessageViewModel.Tags;

                if (PostedFile != null)
                {
                    string fileType = PostedFile.ContentType;
                    bool fileIsValid = DocumentHelper.CheckValidFileByMimeType(fileType);
                    if (fileIsValid)
                    {
                        // Copy the submitted file input stream to the memory stream
                        System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
                        PostedFile.InputStream.CopyTo(memoryStream);

                        // Create a file from the submitted memory stream
                        MomentumPlus.Core.Models.File file = new MomentumPlus.Core.Models.File()
                        {
                            BinaryData = memoryStream.ToArray(),
                            FileType = DocumentHelper.GetFileMimeType(fileType)
                        };

                        // Update the attachment
                        voiceMessage.FileId = file.ID;
                        voiceMessage.Data = file;
                    }
                    else
                    {
                        ModelState.AddModelError("PostedFile", "This file type is not supported.");

                        return View("VoiceMessageForm", voiceMessageViewModel);
                    }
                }

                switch (voiceMessageViewModel.ItemType)
                {
                    case ItemType.Contact:
                        Contact contact = ContactService.GetById(voiceMessageViewModel.ItemId);
                        UpdateVoiceMessage(contact.VoiceMessages, voiceMessage, PostedFile);
                        ContactService.Update(contact);
                        break;

                    case ItemType.Meeting:
                        Meeting meeting = MeetingService.GetByMeetingId(voiceMessageViewModel.ItemId);
                        UpdateVoiceMessage(meeting.VoiceMessages, voiceMessage, PostedFile);
                        MeetingService.Update(meeting);
                        break;

                    case ItemType.Task:
                        Task task = TaskService.GetByTaskId(voiceMessageViewModel.ItemId);
                        UpdateVoiceMessage(task.VoiceMessages, voiceMessage, PostedFile);
                        TaskService.Update(task);
                        break;

                    case ItemType.Topic:
                        Topic topic = TopicService.GetByTopicId(voiceMessageViewModel.ItemId);
                        UpdateVoiceMessage(topic.VoiceMessages, voiceMessage, PostedFile);
                        TopicService.Update(topic);
                        break;

                    case ItemType.Project:
                        Project project = ProjectService.GetProjectById(voiceMessageViewModel.ItemId);
                        UpdateVoiceMessage(project.VoiceMessages, voiceMessage, PostedFile);
                        ProjectService.Update(project);
                        break;

                    case ItemType.ProjectInstance:
                        ProjectInstance projectInstance = ProjectService.GetProjectInstanceById(voiceMessageViewModel.ItemId);
                        UpdateVoiceMessage(projectInstance.VoiceMessages, voiceMessage, PostedFile);
                        ProjectService.Update(projectInstance);
                        break;

                    case ItemType.Staff:
                        Contact staff = PeopleService.GetStaff(voiceMessageViewModel.ItemId);
                        UpdateVoiceMessage(staff.VoiceMessages, voiceMessage, PostedFile);
                      //  PeopleService.UpdatePerson<Staff>(staff);
                        break;
                }
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error editing Voice Message.");
            }

            return Redirect(AppSession.Current.VoiceMessageFormReturnUrl);
        }

        #endregion

        #endregion

        #region HELPERS

        private void UpdateVoiceMessage(IEnumerable<VoiceMessage> voiceMessages, VoiceMessage newVoiceMessage, HttpPostedFileBase file)
        {
            var voiceMessage = voiceMessages.FirstOrDefault(v => v.ID == newVoiceMessage.ID);

            voiceMessage.Name = newVoiceMessage.Name;
            voiceMessage.SearchTags = newVoiceMessage.SearchTags;

            if (file != null)
            {
                voiceMessage.FileId = newVoiceMessage.FileId;
                voiceMessage.Data = newVoiceMessage.Data;
            }
        }

        #endregion
    }
}
