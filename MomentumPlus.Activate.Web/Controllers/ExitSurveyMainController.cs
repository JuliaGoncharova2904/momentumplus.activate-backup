﻿using System.Collections.Generic;
using System.Web.Mvc;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Helpers;
using System.Linq;
using MomentumPlus.Activate.Services;
using MomentumPlus.Core.Models;
using System;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Core.DataSource;

namespace MomentumPlus.Activate.Web.Controllers
{
    public class ExitSurveyMainController : BaseController
    {
        [Authorise(Roles = RoleAuthHelper.ABSOLUTE_GENERAL_ACCESS)]
        public ActionResult Index()
        {
            return RedirectToAction(
                "Index",
                "ExitSurveyTemplate"
            );
        }

        [HttpGet]
        public ActionResult ModalExitSurvey(Guid packageId)
        {
            ExitSurveyService exitSurveyService = new ExitSurveyService(new MomentumContext());

            IEnumerable<ExitSurvey> exitSurveys = exitSurveyService.GetAll();

            ExitSurveyLeavingPeriodViewModel model = new ExitSurveyLeavingPeriodViewModel
            {
                PackageId = packageId,
                ExitSurveys = exitSurveys.Select(e => new SelectListItem
                {
                    Text = e.Name,
                    Value = e.ID.ToString()
                }).ToList(),
                IsExists = PackageService.HasExitSurveyTemplate(packageId)
            };

            Package package = PackageService.GetByPackageId(model.PackageId);

            if (package.ExitSurveyTemplateId.HasValue)
            {
                model.ExitSurveyId = package.ExitSurveyTemplate.ExitSurveyId;
            }
            return PartialView("~/Views/ExitSurvey/Shared/_ModalLeavingPeriod.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ModalExitSurvey(ExitSurveyLeavingPeriodViewModel model)
        {
            ExitSurveyService exitSurveyService = new ExitSurveyService(new MomentumContext());
            if (ModelState.IsValid)
            {
                Package package = PackageService.GetByPackageId(model.PackageId);
                if (package.ExitSurveyTemplateId.HasValue)
                {
                    if (exitSurveyService.GetTemplateByExitSurvey(model.ExitSurveyId) != null)
                    {
                        ExitSurveyTemplate exitSurveyTemplate =
                            exitSurveyService.GetTemplateByExitSurvey(model.ExitSurveyId);
                        package.ExitSurveyTemplateId = exitSurveyTemplate.ID;
                        package.ExitSurvey = true;
                        PackageService.Update(package);
                        model.ExitSurveyId = model.ExitSurveyId;
                    }
                    else
                    {
                        ExitSurveyTemplate newExitSurveyTemplate =
                            CreateExitSurveyTemplate(package.Name, model.PackageId, model.ExitSurveyId);
                        if (newExitSurveyTemplate != null)
                        {
                            package.ExitSurvey = true;
                            package.ExitSurveyTemplateId = newExitSurveyTemplate.ID;
                            PackageService.Update(package);
                        }
                    }
                }
                else
                {
                    if (exitSurveyService.GetTemplateByExitSurvey(model.ExitSurveyId) != null)
                    {
                        ExitSurveyTemplate exitSurveyTemplate =
                            exitSurveyService.GetTemplateByExitSurvey(model.ExitSurveyId);
                        package.ExitSurveyTemplateId = exitSurveyTemplate.ID;
                        package.ExitSurvey = true;
                        PackageService.Update(package);
                        model.ExitSurveyId = model.ExitSurveyId;
                    }
                    else
                    {
                        ExitSurveyTemplate newExitSurveyTemplate =
                            CreateExitSurveyTemplate(package.Name, model.PackageId, model.ExitSurveyId);
                        if (newExitSurveyTemplate != null)
                        {
                            package.ExitSurvey = true;
                            package.ExitSurveyTemplateId = newExitSurveyTemplate.ID;
                            PackageService.Update(package);
                        }
                    }
                }
                return Content("ok");
            }
            IEnumerable<ExitSurvey> exitSurveys = exitSurveyService.GetAll();
            model.ExitSurveys = exitSurveys.Select(e => new SelectListItem
            {
                Text = e.Name,
                Value = e.ID.ToString()
            }).ToList();
            ModelState.AddModelError("ExitSurveyId", "Exit Survey is required");
            return PartialView("~/Views/ExitSurvey/Shared/_ModalLeavingPeriod.cshtml", model); 
        }

        private ExitSurveyTemplate CreateExitSurveyTemplate(string Name, Guid packageId, Guid exitSurveyId)
        {
            ExitSurveyService exitSurveyService = new ExitSurveyService(new MomentumContext());
            List<Package> packages = new List<Package>();
            Package package = PackageService.GetByPackageId(packageId);

            packages.Add(package);
            

            ExitSurveyTemplate exitSurveyTemplate = new ExitSurveyTemplate()
            {
                ID = Guid.NewGuid(),
                Name = Name,
                ExitSurveyId = exitSurveyId,

            };

            MomentumContext context = new MomentumContext();
            context.ExitSurveyTemplate.Add(exitSurveyTemplate);
            context.SaveChanges();

            return exitSurveyService.GetExitSurveyTemplateById(exitSurveyTemplate.ID);
        }

        [HttpPost]
        public ActionResult AddPackageToExitSurveyTemplate(Guid? packageId, Guid? id)
        {
            if (packageId.HasValue && id.HasValue)
            {
                ExitSurveyService exitSurveyService = new ExitSurveyService(new MomentumContext());

                var template = exitSurveyService.GetTemplateByExitSurvey(id.Value);
                var pack = PackageService.GetByPackageId(packageId.Value);
                pack.ExitSurveyTemplateId = template.ID;
                PackageService.Update(pack);
            }
            return null;
        }
    }
}