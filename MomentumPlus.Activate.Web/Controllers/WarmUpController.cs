﻿using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Core.Models;
using System.Web.Mvc;

namespace MomentumPlus.Activate.Web.Controllers
{
    public class WarmUpController : Controller
    {
        private IModuleService moduleService;

        public WarmUpController(IModuleService moduleService)
        {
            this.moduleService = moduleService;

            var moduleCritical = moduleService.GetFrameworkModel(Module.ModuleType.Critical);
            var moduleCriticalClone = moduleService.Clone(moduleCritical.ID);

            var moduleExperiences = moduleService.GetFrameworkModel(Module.ModuleType.Experiences);
            var moduleExperienceClone = moduleService.Clone(moduleExperiences.ID);
        }
    }
}