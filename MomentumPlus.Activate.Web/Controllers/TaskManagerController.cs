﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using MomentumPlus.Activate.Services;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Controllers
{
    public class TaskManagerController : BaseController
    {
        #region IOC

        private ITaskService TaskService;
        private ITopicService TopicService;

        public TaskManagerController(
            IAuditService auditService,
            IMasterListService masterListService,
            IPackageService packageService,
            ITaskService taskService,
            ITopicService topicService,
            IUserService userService)
            :
            base(
                auditService,
                masterListService,
                packageService,
                userService)
        {
            TaskService = taskService;
            TopicService = topicService;
        }

        #endregion

        #region ACTIONS

        /// <summary>
        /// displays a list of tasks for a given topic with links to tasks and back to the 
        /// topic sent from
        /// </summary>
        /// <param name="id"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        [Authorise("Topic")]
        [HttpGet]
        public ActionResult Index(Guid id)
        {
            if (TempData["InProLink"] != null && (bool)TempData["InProLink"])
                TempData.Keep("InProLink");

            ICollection<Task> tasks = new List<Task>();

            var topic = TopicService.GetByTopicId(id);
            tasks = topic.Tasks;

            AppSession.Current.TaskFormReturnUrl = Request.RawUrl;

            return View("Index", new TasksViewModel
            {
                TopicName = topic.Name,
                Tasks = tasks,
                ItemID = id,
                ReadOnly = RoleAuthHelper.IsReadOnly("Topic_View_Index")
            });
        }

        #endregion
    }
}
