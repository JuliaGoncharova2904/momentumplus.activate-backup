﻿using System.Web.Mvc;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Helpers;

namespace MomentumPlus.Activate.Web.Controllers
{
    [Authorise(Roles = RoleAuthHelper.Management_General)]
    public class ManagementController : BaseController
    {
        /// <summary>
        /// Redirects to the <c>Index</c> action of the <c>InUse</c>
        /// controller in the <c>Management</c> area.
        /// </summary>
        /// <returns>The redirect result object.</returns>
        public ActionResult Index()
        {
            return RedirectToAction(
                "Index",
                "InUse",
                new { area = "Management" }
            );
        }

    }
}
