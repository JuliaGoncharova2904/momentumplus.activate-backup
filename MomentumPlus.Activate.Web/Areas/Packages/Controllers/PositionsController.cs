﻿using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Controllers;
using MomentumPlus.Activate.Web.Core;
using MomentumPlus.Activate.Web.Helpers;

namespace MomentumPlus.Activate.Web.Areas.Packages.Controllers
{
    [Authorise(Roles = RoleAuthHelper.Positions_Packages_General)]
    public class PositionsController : PositionController
    {
        #region IOC

        public PositionsController(
            IAuditService auditService,
            IContactService contactService,
            IMasterListService masterListService,
            IPackageService packageService,
            IPositionService positionService,
            IProjectService projectService,
            ITemplateService templateService,
            IHandoverService handoverService,
            IUserService userService,
            IWorkplaceService workplaceService,
            PositionCore positionCore,
            PackageCore packageCore
        )
            : base(
                  auditService,
                  contactService,
                  masterListService,
                  packageService,
                  positionService,
                  projectService,
                  templateService,
                  handoverService,
                  userService,
                  workplaceService,
                  positionCore,
                  packageCore
              )
        {
        }

        #endregion
    }
}
