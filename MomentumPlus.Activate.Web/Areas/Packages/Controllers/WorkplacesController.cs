﻿using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Controllers;
using MomentumPlus.Activate.Web.Core;
using MomentumPlus.Activate.Web.Helpers;

namespace MomentumPlus.Activate.Web.Areas.Packages.Controllers
{
    [Authorise(Roles = RoleAuthHelper.Workplaces_Packages_General)]
    public class WorkplacesController : WorkplaceController
    {
        #region IOC

        public WorkplacesController(
            IAuditService auditService,
            IContactService contactService,
            IMasterListService masterListService,
            IPackageService packageService,
            IPositionService positionService,
            IProjectService projectService,
            ITemplateService templateService,
            IHandoverService handoverService,
            IUserService userService,
            IWorkplaceService workplaceService,
            WorkplaceCore workplaceCore,
            PackageCore packageCore
        )
            : base(
                  auditService,
                  contactService,
                  masterListService,
                  packageService,
                  positionService,
                  projectService,
                  templateService,
                  handoverService,
                  userService,
                  workplaceService,
                  workplaceCore,
                  packageCore
              )
        {
        }

        #endregion
    }
}
