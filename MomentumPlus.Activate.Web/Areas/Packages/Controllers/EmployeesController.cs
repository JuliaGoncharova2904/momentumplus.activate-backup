﻿using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Controllers;
using MomentumPlus.Activate.Web.Core;
using MomentumPlus.Activate.Web.Helpers;

namespace MomentumPlus.Activate.Web.Areas.Packages.Controllers
{
    [Authorise(Roles = RoleAuthHelper.Employees_Packages_General)]
    public class EmployeesController : EmployeeController
    {
        #region IOC

        public EmployeesController(
            IAuditService auditService,
            IContactService contactService,
            IMasterListService masterListService,
            IPackageService packageService,
            IPeopleService peopleService,
            IPositionService positionService,
            IProjectService projectService,
            IRoleService roleService,
            ITemplateService templateService,
            IHandoverService handoverService,
            IUserService userService,
            IWorkplaceService workplaceService,
            EmployeeCore employeeCore,
            PackageCore packageCore
        )
            : base(
                  auditService,
                  contactService,
                  masterListService,
                  packageService,
                  peopleService,
                  positionService,
                  projectService,
                  roleService,
                  templateService,
                  handoverService,
                  userService,
                  workplaceService,
                  employeeCore,
                  packageCore
              )
        {
        }

        #endregion
    }
}
