﻿using System.Web.Mvc;

namespace MomentumPlus.Activate.Web.Areas.Packages
{
    public class PackagesAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Packages";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Packages_default",
                "Packages/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
