﻿using System.Web.Mvc;
using MomentumPlus.Activate.Web.Areas.Management.Models;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Controllers;
using MomentumPlus.Activate.Web.Helpers;

namespace MomentumPlus.Activate.Web.Areas.Management.Controllers
{
    [Authorise(Roles = RoleAuthHelper.Admin_General)]
    public class DashboardController : BaseController
    {

        public static int SavingTab = 1;

        public ActionResult Index()
        {
            return RedirectToAction("ListView", new { Tab = ListViewModel.ListTab.InUse });
        }

        public ActionResult ListView(ListViewModel model)
        {
            ViewBag.hideTimelineActionControl = false;

            return View("~/Areas/Management/Views/Dashboard/ListView.cshtml", model);
        }

        public ActionResult TimelineView(TimelineViewModel model)
        {
            ViewBag.hideTimelineActionControl = false;

            return View("~/Areas/Management/Views/Dashboard/TimelineView.cshtml", model);
        }
    }
}