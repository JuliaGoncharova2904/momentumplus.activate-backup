﻿using System.Web.Mvc;
using MomentumPlus.Activate.Services;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Areas.Management.Models;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Controllers;
using MomentumPlus.Activate.Web.Core.Management;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models.Home;

namespace MomentumPlus.Activate.Web.Areas.Management.Controllers
{
    [Authorise(Roles = RoleAuthHelper.InHandover_General)]
    public class InHandoverController : BaseController
    {
        #region CONSTANTS

        /// <summary>
        /// Number of exiting packages to display per page.
        /// </summary>
        private const int ExitingPageSize = 4;

        /// <summary>
        /// Number of onboarding packages to display per page.
        /// </summary>
        private const int OnboardingPageSize = 4;

        #endregion

        #region IOC

        private InHandoverCore InHandoverCore;

        public InHandoverController(
            IPackageService packageService,
            ITaskService taskService,
            IMasterListService masterListService,
            InHandoverCore inHandoverCore
        )
        {
            InHandoverCore = inHandoverCore;
        }

        #endregion

        public ActionResult Index(
            int exitingPageNo = 1,
            InHandoverExitingItemViewModel.SortColumn exitingSortColumn = InHandoverExitingItemViewModel.SortColumn.NameOnboarder,
            bool exitingSortAscending = true,
            int onboardingPageNo = 1,
            InHandoverOnboardingItemViewModel.SortColumn onboardingSortColumn = InHandoverOnboardingItemViewModel.SortColumn.NameNewStart,
            bool onboardingSortAscending = true
        )
        {
            InHandoverExitingViewModel exitingModel = InHandoverCore.GetExiting(exitingPageNo, ExitingPageSize, exitingSortColumn, exitingSortAscending);
            InHandoverOnboardingViewModel onboardingModel = InHandoverCore.GetOnboarding(onboardingPageNo, OnboardingPageSize, onboardingSortColumn, onboardingSortAscending);

            AppSession.Current.PackageExamineReturnUrl = Request.RawUrl;
            AppSession.Current.TriggerFormReturnUrl = Request.RawUrl;
            AppSession.Current.HandoverFormReturnUrl = Request.RawUrl;

            return View("~/Areas/Management/Views/InHandover/Index.cshtml", new InHandoverViewModel
                {
                    Exiting = exitingModel,
                    Onboarding = onboardingModel
                });
        }
    }
}
