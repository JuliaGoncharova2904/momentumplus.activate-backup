﻿using System.Web.Mvc;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Controllers;
using MomentumPlus.Activate.Web.Core.Management;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models.Home;

namespace MomentumPlus.Activate.Web.Areas.Management.Controllers
{
    [Authorise(Roles = RoleAuthHelper.InUse_General)]
    public class InUseController : BaseController
    {
        #region CONSTANTS

        /// <summary>
        /// Number of in use packages to display per page.
        /// </summary>
        private const int PageSize = 8;

        #endregion

        #region IOC

        private InUseCore InUseCore;

        public InUseController(
            IMasterListService masterListService,
            IPackageService packageService,
            InUseCore inUseCore
        )
        {
            InUseCore = inUseCore;
        }

        #endregion

        public ActionResult Index(
            int pageNo = 1,
            InUseOnboarderItemViewModel.SortColumn sortColumn = InUseOnboarderItemViewModel.SortColumn.LastUpdated,
            bool sortAscending = true
        )
        {
            InUseOnboarderViewModel model = InUseCore.GetOnboarders(pageNo, PageSize, sortColumn, sortAscending);

            model.SortAscending = sortAscending;

            AppSession.Current.PackageExamineReturnUrl = Request.RawUrl;
            AppSession.Current.TriggerFormReturnUrl = Request.RawUrl;
            AppSession.Current.HandoverFormReturnUrl = Request.RawUrl;

            return View("~/Areas/Management/Views/InUse/Index.cshtml", model);
        }
    }
}
