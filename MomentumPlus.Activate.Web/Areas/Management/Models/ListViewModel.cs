﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MomentumPlus.Activate.Web.Models.Home;

namespace MomentumPlus.Activate.Web.Areas.Management.Models
{
    public class ListViewModel
    {

        public ListViewModel()
        {
            PageNo = 1;
            SortColumn = InUseOnboarderItemViewModel.SortColumn.LastUpdated;
            SortAscending = true;
            Tab = ListTab.InUse;

            ExitingPageNo = 1;
            ExitingSortColumn = InHandoverExitingItemViewModel.SortColumn.NameOnboarder;
            ExitingSortAscending = true;
            OnboardingPageNo = 1;
            OnboardingSortColumn = InHandoverOnboardingItemViewModel.SortColumn.NameNewStart;
            OnboardingSortAscending = true;
        }

        public enum ListTab
        {
            InUse,
            InHandover,
            Ongoing,
            Handover
        }

        public ListTab Tab { get; set; }
        public int PageNo { get; set; }
        public InUseOnboarderItemViewModel.SortColumn SortColumn { get; set; }
        public bool SortAscending { get; set; }


        public int ExitingPageNo { get; set; }
        public InHandoverExitingItemViewModel.SortColumn ExitingSortColumn { get; set; }
        public bool ExitingSortAscending { get; set; }
        public int OnboardingPageNo { get; set; }
        public InHandoverOnboardingItemViewModel.SortColumn OnboardingSortColumn { get; set; }
        public bool OnboardingSortAscending { get; set; }

    }
}