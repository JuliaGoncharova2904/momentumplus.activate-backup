﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MomentumPlus.Activate.Web.Areas.Management.Models
{
    public class TimelineViewModel
    {

        public enum TimelineTab
        {
            Ongoing,
            Handover,
            InUse,
            InHandover
        }

        public TimelineViewModel()
        {
            Tab = TimelineTab.Ongoing;
        }

        public TimelineTab Tab { get; set; }

    }
}