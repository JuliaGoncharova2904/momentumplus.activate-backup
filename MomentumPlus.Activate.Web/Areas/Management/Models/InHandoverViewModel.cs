﻿using MomentumPlus.Activate.Web.Models.Home;

namespace MomentumPlus.Activate.Web.Areas.Management.Models
{
    public class InHandoverViewModel
    {
        public InHandoverExitingViewModel Exiting { get; set; }
        public InHandoverOnboardingViewModel Onboarding { get; set; }
    }
}