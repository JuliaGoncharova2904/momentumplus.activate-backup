﻿using System;
using System.Web.Mvc;
using System.Web.Security;
using AutoMapper;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Controllers;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Activate.Web.Models.People;
using MomentumPlus.Activate.Web.Core;
using System.Web;
using MomentumPlus.Activate.Services.Helpers;
using MomentumPlus.Core.Models.People;

namespace MomentumPlus.Activate.Web.Areas.Profile.Controllers
{
    public class ProfileController : BaseController
    {
        private PersonCore PersonCore;

        #region Dependency Injection (IoC)

        /// <summary>
        /// ProfileController Constructor
        /// </summary>
        public ProfileController(
            IAuditService auditService,
            IMasterListService masterListService,
            IPackageService packageService,
            IPeopleService peopleService,
            IUserService userService,
            PersonCore personCore)
            : base(
                auditService,
                masterListService,
                packageService,
                userService
        )
        {
            PersonCore = personCore;
        }

        #endregion

        #region Index (General)

        /// <summary>
        /// Main Index Action. Gets the currently logged in user and maps it to the view model
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            var user = UserService.GetByUsername(HttpContext.User.Identity.Name);

            UserViewModel viewModel = Mapper.Map<User, UserViewModel>(user);
            viewModel.UserMode = true;
            viewModel.EnableTimeManipulationTool = AppSession.Current.EnableTimeManipulationTool;

            if (!AppSession.Current.EnableTimeManipulationTool)
                SystemTime.Clear();

            PersonCore.PopulatePersonViewModel(viewModel);

            AppSession.Current.EmployeeFormReturnUrl = Request.RawUrl;
            AppSession.Current.ChangePasswordFormReturnUrl = Request.RawUrl;

            return View("EmployeeForm", viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(UserViewModel viewModel, HttpPostedFileBase profileImage)
        {
            try
            {
                var user = UserService.GetByUsername(HttpContext.User.Identity.Name);

                // If the model is not valid, do not save, but return
                if (!ModelState.IsValid)
                {
                    viewModel.UserMode = true;

                    PersonCore.PopulatePersonViewModel(viewModel);

                    return View("EmployeeForm", viewModel);
                }

                // Update the user
                user.Phone = viewModel.Phone;
                user.Mobile = viewModel.Mobile;
                user.Email = viewModel.Email;
                user.Notes = viewModel.Notes;
                user.DisplayTooltips = viewModel.DisplayTooltips;
                AppSession.Current.EnableTimeManipulationTool = viewModel.EnableTimeManipulationTool;

                PersonCore.UpdatePersonFields(user, viewModel, profileImage);

                // Update an existing user in the database
                UserService.UpdateUser(user);

                // Update user stored in session with the new information
                AppSession.Current.CurrentUser = user;

                AddAlert(AlertViewModel.AlertType.Success, "Profile successfully updated.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Warning, "Something went wrong. Please try again");
            }

            return Redirect(AppSession.Current.EmployeeFormReturnUrl);
        }

        #endregion

        #region Change Password (GET & POST)

        [HttpGet]
        public ActionResult ChangePassword(Guid id)
        {
            return View(new ChangePasswordViewModel { UserId = id });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(ChangePasswordViewModel viewModel)
        {
            var user = UserService.GetByUserId(viewModel.UserId);
            if (!Membership.ValidateUser(user.Username, viewModel.CurrentPassword))
            {
                ModelState.AddModelError("CurrentPassword", @"The Current Password entered was incorrect.");
            }
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            try
            {
                var success = UserService.ChangePassword(viewModel.UserId, viewModel.CurrentPassword, viewModel.NewPassword);

                if (success)
                {
                    AddAlert(AlertViewModel.AlertType.Success, "Password changed successfully.");
                }
                else
                {
                    AddAlert(AlertViewModel.AlertType.Danger, "Something went wrong, please try again.");
                }
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Something went wrong, please try again.");
            }

            return Redirect(AppSession.Current.ChangePasswordFormReturnUrl);
        }

        #endregion
    }
}
