﻿using System.Web.Mvc;

namespace MomentumPlus.Activate.Web.Areas.Faq
{
    public class FaqAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Faq";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Faq_default",
                "Faq/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
