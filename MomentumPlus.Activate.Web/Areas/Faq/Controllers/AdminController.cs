﻿using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Helpers;

namespace MomentumPlus.Activate.Web.Areas.Faq.Controllers
{
    [Authorise(Roles = RoleAuthHelper.ABSOLUTE_GENERAL_ACCESS)]
    public class AdminController : FaqController
    {
    }
}
