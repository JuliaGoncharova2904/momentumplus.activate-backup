﻿using System.IO;
using System.Web.Mvc;
using System.Xml.Serialization;
using MomentumPlus.Activate.Web.Areas.Faq.Models;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Controllers;
using MomentumPlus.Activate.Web.Helpers;

namespace MomentumPlus.Activate.Web.Areas.Faq.Controllers
{
    public class FaqController : BaseController
    {
        /// <summary>
        /// Render user FAQ
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public ActionResult Index()
        {
            var faqCollection = ReadFaq("~/Faqs/UserFaq.xml");
            return View(faqCollection);
        }

        /// <summary>
        /// Render admin FAQ
        /// </summary>
        /// <returns></returns>
        [Authorise(Roles = RoleAuthHelper.ABSOLUTE_GENERAL_ACCESS)]
        [HttpGet]
        public ActionResult Admin()
        {
            var faqCollection = ReadFaq("~/Faqs/AdminFaq.xml");
            return View("Index", faqCollection);
        }

        /// <summary>
        /// Deserialize an XML file to <c>FaqCollection</c> object.
        /// </summary>
        /// <param name="path">The path of the XML file.</param>
        /// <returns>An <c>FaqCollection</c> object.</returns>
        private FaqCollection ReadFaq(string path)
        {
            var userFaqPath = Server.MapPath(path);
            var reader = new StreamReader(userFaqPath);
            var serializer = new XmlSerializer(typeof(FaqCollection));
            var faqCollection = (FaqCollection)serializer.Deserialize(reader);
            return faqCollection;
        }
    }
}
