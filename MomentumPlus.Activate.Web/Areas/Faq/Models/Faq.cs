﻿using System;
using System.Xml.Serialization;

namespace MomentumPlus.Activate.Web.Areas.Faq.Models
{
    [Serializable()]
    public class Faq
    {
        [XmlElement("Question")]
        public string Question { get; set; }

        [XmlElement("Answer")]
        public string Answer { get; set; }
    }

    [Serializable()]
    [XmlRoot("FaqCollection")]
    public class FaqCollection
    {
        [XmlArray("Faqs")]
        [XmlArrayItem("Faq", typeof(Faq))]
        public Faq[] Faqs { get; set; }
    }
}