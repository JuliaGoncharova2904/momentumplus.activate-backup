﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Activate.Web.Areas.Framework.Models
{
    public class FrameworkTemplateViewModel
    {
        public Template Template { get; set; }
        public IEnumerable<SelectListItem> Templates { get; set; }

        [Display(Name = "Template")]
        public Guid? SelectedTemplate { get; set; }

        public IEnumerable<TemplateItemViewModel> TemplateItemsCriticalTopicGroups { get; set; }
        public IEnumerable<TemplateItemViewModel> TemplateItemsCriticalTopics { get; set; }
        public IEnumerable<TemplateItemViewModel> TemplateItemsCriticalTasks { get; set; }

        public IEnumerable<TemplateItemViewModel> TemplateItemsExperiencesTopicGroups { get; set; }
        public IEnumerable<TemplateItemViewModel> TemplateItemsExperiencesTopics { get; set; }
        public IEnumerable<TemplateItemViewModel> TemplateItemsExperiencesTasks { get; set; }

        [Display(Name = "Destination Template")]
        public Guid? DestinationTemplate { get; set; }

        public bool HideControls { get; set; }

        public IEnumerable<SelectListItem> ProLinkFunctions { get; set; }
    }
}