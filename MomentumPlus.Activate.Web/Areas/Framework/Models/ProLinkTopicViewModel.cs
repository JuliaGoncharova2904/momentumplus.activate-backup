﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MomentumPlus.Activate.Web.Areas.Framework.Models
{
    public class ProLinkTopicViewModel
    {

        public Guid ID { get; set; }

        public string Name { get; set; }

        public Guid? ProcessGroupId { get; set; }

        public bool IsProLink { get; set; }

    }
}