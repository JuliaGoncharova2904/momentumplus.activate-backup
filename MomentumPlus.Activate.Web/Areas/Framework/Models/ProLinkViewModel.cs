﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MomentumPlus.Activate.Web.Areas.Framework.Models
{
    public class ProLinkViewModel
    {

        public IEnumerable<FunctionViewModel> Functions { get; set; }
        public IEnumerable<ProLinkTopicViewModel> Topics { get; set; }
        public Guid BussinesProcessesTopicGroupID { get; set; }

    }
}