﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Activate.Web.Areas.Framework.Models
{
    public class FrameworkViewModel
    {
        public IEnumerable<TopicGroup> TopicGroups { get; set; }
        public IEnumerable<Topic> Topics { get; set; }
        public IEnumerable<Task> Tasks { get; set; }

        public Guid? ActiveTopicGroup { get; set; }
        public Guid? ActiveTopic { get; set; }

        public int PageSize { get; set; }
        public int TopicPageNo { get; set; }
        public int TopicCount { get; set; }
        public int TaskPageNo { get; set; }
        public int TaskCount { get; set; }

        public bool RetiredTopics { get; set; }
        public bool RetiredTasks { get; set; }
    }
}