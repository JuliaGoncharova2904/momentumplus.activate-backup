﻿using MomentumPlus.Core.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Activate.Web.Areas.Framework.Models
{
    public class FrameworkProjectsViewModel
    {
        public IEnumerable<Project> Projects { get; set; }

        [Display(Name = "View by Project Status")]
        public int? ProjectStatus { get; set; }
        public IEnumerable<SelectListItem> Statuses { get; set; }

        [Display(Name = "Order By")]
        public Project.SortColumn OrderBy { get; set; }

        [Display(Name = "Sort Ascending")]
        public bool SortAscending { get; set; }

        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }

        [Display(Name = "Retired Projects")]
        public bool RetiredProjects { get; set; }
    }
}