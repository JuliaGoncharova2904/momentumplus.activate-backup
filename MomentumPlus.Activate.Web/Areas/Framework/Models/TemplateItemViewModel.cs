﻿using MomentumPlus.Core.Models;
using System;

namespace MomentumPlus.Activate.Web.Areas.Framework.Models
{
    public class TemplateItemViewModel 
    {
        public int ReferenceId { get; set; }

        public int SourceReferenceId { get; set; }
        public int? SourceParentReferenceId { get; set; }

        public string Name { get; set; }

        public int? FunctionReferenceId { get; set; }
        public bool IsCore { get; set; }

        public TemplateItem.TemplateItemType Type { get; set; }

        public bool Enabled { get; set; }

        public TopicGroup.TopicType? ParentTopicType { get; set; }
        public string ParentTopicName { get; set; }

        public bool IsNew { get; set; }
        public DateTime? Deleted { get; set; }
    }
}
