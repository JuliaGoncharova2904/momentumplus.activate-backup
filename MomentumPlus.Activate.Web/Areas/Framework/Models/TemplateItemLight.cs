﻿using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Areas.Framework.Models
{
    /// <summary>
    /// Lightweight template item.
    /// </summary>
    public class TemplateItemLight
    {
        /// <summary>
        /// Source reference ID.
        /// </summary>
        public int I { get; set; }

        /// <summary>
        /// Source parent reference ID.
        /// </summary>
        public int? P { get; set; }

        /// <summary>
        /// Item type.
        /// </summary>
        public TemplateItem.TemplateItemType T { get; set; }

        /// <summary>
        /// New item?
        /// </summary>
        public bool N { get; set; }
    }
}