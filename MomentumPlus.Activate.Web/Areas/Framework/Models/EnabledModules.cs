﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MomentumPlus.Activate.Web.Areas.Framework.Models
{
    /// <summary>
    /// Lightweight enabled modules.
    /// </summary>
    public class EnabledModules
    {
        /// <summary>
        /// Experiences
        /// </summary>
        public bool E { get; set; }

        /// <summary>
        /// Relationships
        /// </summary>
        public bool R { get; set; }

        /// <summary>
        /// Projects
        /// </summary>
        public bool Pr { get; set; }

        /// <summary>
        /// People management
        /// </summary>
        public bool Pe { get; set; }
    }
}