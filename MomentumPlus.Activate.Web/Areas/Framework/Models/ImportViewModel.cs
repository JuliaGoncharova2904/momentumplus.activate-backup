﻿using System.ComponentModel.DataAnnotations;
using MomentumPlus.Activate.Web.Models;

namespace MomentumPlus.Activate.Web.Areas.Framework.Models
{
    public class ImportViewModel
    {
        [Display(Name = "Item Type")]
        public ItemType ItemType { get; set; }
    }
}