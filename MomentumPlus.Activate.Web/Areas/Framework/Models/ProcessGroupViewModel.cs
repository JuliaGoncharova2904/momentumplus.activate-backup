﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Activate.Web.Areas.Framework.Models
{
    public class ProcessGroupViewModel
    {

        public Guid? ID { get; set; }

        public Guid? FunctionId { get; set; }

        public string FunctionName { get; set; }

        [Display(Name="Display Name *")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        public IEnumerable<ProLinkTopicViewModel> Topics { get; set; }

    }
}