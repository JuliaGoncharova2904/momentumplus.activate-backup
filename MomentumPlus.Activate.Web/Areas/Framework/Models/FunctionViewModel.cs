﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Activate.Web.Areas.Framework.Models
{
    public class FunctionViewModel
    {

        public Guid? ID { get; set; }

        [Display(Name="Display Name *")]
        [Required]
        public string Name { get; set; }

        public IEnumerable<ProcessGroupViewModel> ProcessGroups { get; set; }

    }
}