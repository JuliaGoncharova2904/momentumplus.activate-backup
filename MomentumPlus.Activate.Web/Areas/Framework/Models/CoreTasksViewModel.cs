﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Activate.Web.Areas.Framework.Models
{
    public class CoreTasksViewModel
    {
        public Guid? CoreTopicGroupId { get; set; }
        public IEnumerable<Task> CoreTasks { get; set; }
    }
}