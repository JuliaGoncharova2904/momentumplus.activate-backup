﻿using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.People;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Activate.Web.Areas.Framework.Models
{
    public class FrameworkRelationshipsViewModel
    {
        public IEnumerable<Contact> Contacts { get; set; }

        [Display(Name = "Order By")]
        public Person.SortColumn OrderBy { get; set; }

        [Display(Name = "Sort Ascending")]
        public bool SortAscending { get; set; }

        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }

        [Display(Name = "Retired Relationships")]
        public bool RetiredRelationships { get; set; }
    }
}