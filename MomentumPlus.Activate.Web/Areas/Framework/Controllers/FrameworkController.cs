﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using MomentumPlus.Activate.Services;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Controllers;
using MomentumPlus.Activate.Web.Core.Framework;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Activate.Web.Areas.Framework.Models;
using MomentumPlus.Activate.Web.Core;
using MomentumPlus.Activate.Web.Core.Home;
using MomentumPlus.Activate.Web.Models.Requests;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.People;

namespace MomentumPlus.Activate.Web.Areas.Framework.Controllers
{
    [Authorise(Roles = RoleAuthHelper.Framework_General)]
    public class FrameworkController : TaskController
    {
        private ModuleCore ModuleCore;
        private ProjectCore ProjectCore;
        private RelationshipsCore RelationshipsCore;

        #region IOC

        private IContactService ContactService;
        private IModuleService ModuleService;
        private IProjectService ProjectService;

        /// <summary>
        /// Create a new <c>FrameworkController</c> instance with the
        /// given service implementations.
        /// </summary>
        public FrameworkController(
            IAuditService auditService,
            IContactService contactService,
            IHandoverService handoverService,
            IImageService imageService,
            IMasterListService masterListService,
            IMeetingService meetingService,
            IModuleService moduleService,
            IPackageService packageService,
            IPeopleService peopleService,
            IProjectService projectService,
            IRequestService requestService,
            ITaskService taskService,
            ITopicGroupService topicGroupService,
            ITopicQuestionService topicQuestionService,
            ITopicService topicService,
            IUserService userService,
            IWorkplaceService workplaceService,
            IModerationService moderationService,
            IProLinkService proLink,
            ModuleCore moduleCore,
            RelationshipsCore relationshipsCore,
            ProjectCore projectCore,
            TaskCore taskCore,
IPositionService positionService,
            TopicCore topicCore
        )
            : base(
                  auditService,
                  handoverService,
                  masterListService,
                  packageService,
                  taskService,
                  topicService,
                  topicGroupService,
                  topicQuestionService,
                  userService,
                  workplaceService,
                  proLink,
                  projectService,
            contactService,
                  taskCore,
positionService,
                  topicCore
              )
        {
            ModuleCore = moduleCore;

            ProjectCore = projectCore;

            RelationshipsCore = relationshipsCore;

            ContactService = contactService;
            ModuleService = moduleService;
            ProjectService = projectService;
        }

        #endregion

        #region ACTIONS

        [HttpGet]
        public ActionResult Index()
        {
            return RedirectToAction("Critical");
        }

        public ActionResult CreateTopic()
        {
            return null;
        }

        #region Critical & Experiences

        /// <summary>
        /// Render critical framework view
        /// </summary>
        /// <param name="topicGroup">ID of currently selected topic group</param>
        /// <param name="topicPageNo">Number of current topic page</param>
        /// <param name="topic">ID of currently selected topic</param>
        /// <param name="taskPageNo">Number of current task page</param>
        /// <param name="retiredTopics">If true will only show retired topics</param>
        /// <param name="retiredTasks">If true will only show retired tasks</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Critical(
            Guid? topicGroup = null,
            int topicPageNo = 1,
            Guid? topic = null,
            int taskPageNo = 1,
            bool retiredTopics = false,
            bool retiredTasks = false)
        {
            return GetFrameworkView(Module.ModuleType.Critical, topicGroup, topicPageNo, topic, taskPageNo, retiredTopics, retiredTasks);
        }

        [HttpGet]
        public ActionResult Experiences(
            Guid? topicGroup = null,
            int topicPageNo = 1,
            Guid? topic = null,
            int taskPageNo = 1,
            bool retiredTopics = false,
            bool retiredTasks = false
        )
        {
            return GetFrameworkView(Module.ModuleType.Experiences, topicGroup, topicPageNo, topic, taskPageNo, retiredTopics, retiredTasks);
        }

        #region EditTopicGroup

        /// <summary>
        /// Render the topic group form in edit mode.
        /// </summary>
        /// <param name="id">The ID of the group being edited.</param>
        [HttpGet]
        public ActionResult EditTopicGroup(Guid id)
        {
            var topicGroup = TopicGroupService.GetByTopicGroupId(id);
            var topicGroupViewModel = Mapper.Map<TopicGroupViewModel>(topicGroup);
            topicGroupViewModel.ReadOnly = RoleAuthHelper.IsReadOnly("TopicGroup_Edit_Get");
            return View("TopicGroupForm", topicGroupViewModel);
        }

        /// <summary>
        /// Handle topic group form submission.
        /// </summary>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTopicGroup(TopicGroupViewModel topicGroupViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View("TopicGroupForm", topicGroupViewModel);
            }

            try
            {
                // Get the topic group being edited and set it's properties
                var topicGroup = TopicGroupService.GetByTopicGroupId(topicGroupViewModel.ID);
                topicGroup.Title = topicGroupViewModel.Title;
                topicGroup.Notes = topicGroupViewModel.Notes;

                // Update the topic group in the database and record activity in the audit trail
                TopicGroupService.Update(topicGroup);
                AuditService.RecordActivity<TopicGroup>(AuditLog.EventType.Modified, topicGroup.ID);

                AddAlert(AlertViewModel.AlertType.Success, "Topic group updated successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error updating topic group.");
            }

            return Redirect(AppSession.Current.TopicGroupFormReturnUrl);
        }


        #endregion

        #region Core Tasks

        [HttpGet]
        public ActionResult CoreTasks()
        {
            var critical = ModuleService.GetFrameworkModel(Module.ModuleType.Critical);

            var coreTasks = from tg in critical.TopicGroups
                            from to in tg.Topics
                            from ta in to.Tasks
                            where ta.IsCore
                            select ta;

            AppSession.Current.TaskFormReturnUrl = Request.RawUrl;

            return View(new MomentumPlus.Activate.Web.Areas.Framework.Models.CoreTasksViewModel
            {
                CoreTasks = coreTasks.OrderBy(t => t.ParentTopic.Name)
            });
        }

        [HttpGet]
        public ActionResult AddCoreTask(Guid? coreTopicGroupId)
        {
            AddAlert(AlertViewModel.AlertType.Info, "Select a topic in the Essentials module to add a Core Task.");
            return RedirectToAction("Critical", new { topicGroup = coreTopicGroupId });
        }

        #endregion

        #region Relationships

        [HttpGet]
        public ActionResult Relationships(
            Person.SortColumn orderBy = Person.SortColumn.FirstName,
            bool sortAscending = true,
            int pageNo = 1,
            bool retiredRelationships = false
        )
        {
            var pageSize = 10;

            var viewModel = new FrameworkRelationshipsViewModel
            {
                OrderBy = orderBy,
                SortAscending = sortAscending,
                PageNo = pageNo,
                PageSize = pageSize,
                RetiredRelationships = retiredRelationships
            };

            viewModel.Contacts = ContactService.GetFrameworkContacts(
                pageNo,
                pageSize,
                orderBy,
                sortAscending,
                retiredRelationships
            );

            viewModel.TotalCount = ContactService.GetAllFramework(retiredRelationships).Count();

            AppSession.Current.ContactFormReturnUrl = Request.RawUrl;

            return View(viewModel);
        }

        #region CreateRelationship

        /// <summary>
        /// Render the relationship form in create mode.
        /// </summary>
        [HttpGet]
        public ActionResult CreateRelationship()
        {
            var viewModel = RelationshipsCore.GetNewContactViewModel();
            RelationshipsCore.PopulateContactViewModel(viewModel, isFramework: true);
            viewModel.IsFramework = true;
            return View("ContactForm", viewModel);
        }

        /// <summary>
        /// Handle relationship form submission.
        /// </summary>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateRelationship(ContactViewModel viewModel, HttpPostedFileBase profileImage)
        {
            if (viewModel.Person.Email != null && ContactService.EmailExists(viewModel.Person.Email))
            {
                ModelState.AddModelError("", @"This e-mail already exists. Please use another e-mail address.");
            }

            if (!viewModel.CompanyGuid.IsNullOrEmpty())
            {
                var companyId = new Guid(viewModel.CompanyGuid);

                viewModel.Person.CompanyId = companyId;
                viewModel.Person.Company = MasterListService.GetCompany(companyId);
            }



            if (!ModelState.IsValid)
            {
                RelationshipsCore.PopulateContactViewModel(viewModel, isFramework: true);
                viewModel.IsFramework = true;
                return View("ContactForm", viewModel);
            }

            try
            {
                var contact = RelationshipsCore.CreateContact(viewModel, profileImage: profileImage);

                var returnUrl = Url.Action("EditRelationship", new { id = contact.ID });
                AppSession.Current.VoiceMessageManagerReturnUrl = returnUrl;
                AppSession.Current.AttachmentManagerReturnUrl = returnUrl;

                if (AppSession.Current.FormViewModel != null)
                {
                    if (AppSession.Current.FormViewModel is ProjectViewModel)
                        (AppSession.Current.FormViewModel as ProjectViewModel).ProjectManagerId = contact.ID;
                    else if (AppSession.Current.FormViewModel is ProjectInstanceViewModel)
                        (AppSession.Current.FormViewModel as ProjectInstanceViewModel).Project.ProjectManagerId = contact.ID;
                    else if (AppSession.Current.FormViewModel is NewProjectRequestViewModel)
                        (AppSession.Current.FormViewModel as NewProjectRequestViewModel).Project.ProjectManagerId = contact.ID;
                }

                return GetContactSuccessResult(contact, "Contact created successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error creating contact.");
            }

            return RedirectToAction("Relationships");
        }

        #endregion

        #region EditRelationship

        /// <summary>
        /// Render the relationship form in edit mode.
        /// </summary>
        /// <param name="id">The ID of the Contact.</param>
        [HttpGet]
        public ActionResult EditRelationship(Guid id)
        {
            var contact = ContactService.GetById(id);
            var contactViewModel = RelationshipsCore.GetContactViewModel(contact);

            AuditService.RecordActivity<Contact>(AuditLog.EventType.Viewed, contact.ID);

            AppSession.Current.AttachmentManagerReturnUrl = Request.RawUrl;
            AppSession.Current.VoiceMessageManagerReturnUrl = Request.RawUrl;

            return View("ContactForm", contactViewModel);
        }

        /// <summary>
        /// Handle edit relationship form submission.
        /// </summary>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditRelationship(ContactViewModel contactViewModel, HttpPostedFileBase profileImage)
        {
            var contact = ContactService.GetById(contactViewModel.ID);


            if (contactViewModel.Person.Email != null && contactViewModel.Person.Email != contact.Person.Email && ContactService.EmailExists(contactViewModel.Person.Email))
            {
                ModelState.AddModelError("", @"This e-mail already exists. Please use another e-mail address.");
            }



            if (!ModelState.IsValid)
            {
                RelationshipsCore.PopulateContactViewModel(contactViewModel, isFramework: true);

                contactViewModel.NotesSubjectivity = contact.CountSubjectivity(MasterListService);
                contactViewModel.NotesWordCount = contact.CountWords();

                ViewData["HideSideNav"] = true;
                return View("ContactForm", contactViewModel);
            }

            try
            {
                RelationshipsCore.UpdateContact(contact, contactViewModel, profileImage: profileImage);
                return GetContactSuccessResult(contact, "Contact updated successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error updating contact.");
            }

            return RedirectToAction("Index");
        }

        #endregion

        #region Retire/Resume

        /// <summary>
        /// Retire the contact with the given ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult RetireRelationship(Guid id)
        {
            try
            {
                ContactService.Retire(id);
                AddAlert(AlertViewModel.AlertType.Success, "Contact retired successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Something went wrong, please try again.");
            }

            return Redirect(AppSession.Current.ContactFormReturnUrl);
        }

        /// <summary>
        /// Resume the contact with the given ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ResumeRelationship(Guid id)
        {
            try
            {
                ContactService.Restore(id);
                AddAlert(AlertViewModel.AlertType.Success, "Contact resumed successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Something went wrong, please try again.");
            }

            return Redirect(AppSession.Current.ContactFormReturnUrl);
        }

        #endregion

        #endregion

        #region Projects

        /// <summary>
        /// Render projects index view
        /// </summary>
        /// <param name="projectStatus">Project status to filter by</param>
        /// <param name="orderBy">Column to sort by</param>
        /// <param name="sortAscending">Whether to sort ascending or descending</param>
        /// <param name="pageNo">The number of the page to show</param>
        /// <param name="retiredProjects">If true will show retired projects only</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Projects(
            int? projectStatus = null,
            Project.SortColumn orderBy = Project.SortColumn.Name,
            bool sortAscending = true,
            int pageNo = 1,
            bool retiredProjects = false
        )
        {
            var pageSize = 10;

            var viewModel = new FrameworkProjectsViewModel
            {
                ProjectStatus = projectStatus,
                OrderBy = orderBy,
                SortAscending = sortAscending,
                PageNo = pageNo,
                PageSize = pageSize
            };

            viewModel.Projects = ProjectService.GetProjects(
                projectStatus,
                pageNo,
                pageSize,
                orderBy,
                sortAscending,
                retiredProjects
            );

            viewModel.TotalCount = ProjectService.CountProjects(projectStatus, retiredProjects);
            viewModel.Statuses = ProjectCore.GetProjectStatusesSelectList();

            AppSession.Current.ProjectFormReturnUrl = Request.RawUrl;

            return View(viewModel);
        }

        /// <summary>
        /// Render project form in create mode.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult CreateProject()
        {
            ProjectViewModel viewModel;
            if (AppSession.Current.FormViewModel != null && AppSession.Current.FormViewModel is ProjectViewModel)
            {
                viewModel = AppSession.Current.FormViewModel as ProjectViewModel;
                AppSession.Current.FormViewModel = null;
            }
            else
            {
                viewModel = new ProjectViewModel();
            }
            ProjectCore.PopulateProjectViewModel(viewModel);
            return View("ProjectForm", viewModel);
        }

        /// <summary>
        /// Handle new project form submission.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateProject(ProjectViewModel viewModel)
        {
            if (Request.Form["QuickAddRelationship"] != null)
            {
                AppSession.Current.ContactFormReturnUrl = Request.RawUrl;
                return QuickAddRelationship(viewModel);
            }

            if (!ModelState.IsValid)
            {
                ProjectCore.PopulateProjectViewModel(viewModel);
                return View("ProjectForm", viewModel);
            }

            try
            {
                var project = ProjectCore.CreateProject(viewModel);

                var returnUrl = Url.Action("EditProject", new { id = project.ID });
                AppSession.Current.VoiceMessageManagerReturnUrl = returnUrl;
                AppSession.Current.AttachmentManagerReturnUrl = returnUrl;

                return GetProjectSuccessResult(project.ID, SaveSuccessMessage);
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, SaveErrorMessage);
            }

            return Redirect(AppSession.Current.ProjectFormReturnUrl);
        }

        /// <summary>
        /// Render project form in edit mode.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult EditProject(Guid id)
        {
            ProjectViewModel viewModel;
            if (AppSession.Current.FormViewModel != null && AppSession.Current.FormViewModel is ProjectViewModel && (AppSession.Current.FormViewModel as ProjectViewModel).ID == id)
            {
                viewModel = AppSession.Current.FormViewModel as ProjectViewModel;
                AppSession.Current.FormViewModel = null;
            }
            else
            {
                var project = ProjectService.GetProjectById(id);
                viewModel = Mapper.Map<ProjectViewModel>(project);
            }
            ProjectCore.PopulateProjectViewModel(viewModel);

            AppSession.Current.ContactFormReturnUrl = Request.RawUrl;
            AppSession.Current.VoiceMessageManagerReturnUrl = Request.RawUrl;
            AppSession.Current.AttachmentManagerReturnUrl = Request.RawUrl;

            return View("ProjectForm", viewModel);
        }

        /// <summary>
        /// Handle edit project form submission.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditProject(ProjectViewModel viewModel)
        {
            if (Request.Form["QuickAddRelationship"] != null)
            {
                return QuickAddRelationship(viewModel);
            }

            if (!ModelState.IsValid)
            {
                ProjectCore.PopulateProjectViewModel(viewModel);
                return View("ProjectForm", viewModel);
            }

            try
            {
                var project = ProjectCore.UpdateProject(viewModel);
                return GetProjectSuccessResult(project.ID, SaveSuccessMessage);
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, SaveErrorMessage);
            }

            return Redirect(AppSession.Current.ProjectFormReturnUrl);
        }

        [ChildActionOnly]
        public ActionResult QuickAddRelationship(ProjectViewModel model)
        {
            AppSession.Current.FormViewModel = model;

            return RedirectToAction("CreateRelationship");
        }

        #region RetireProject/ResumeProject

        /// <summary>
        /// Retire the project with the given ID
        /// </summary>
        /// <param name="id">The project GUID</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult RetireProject(Guid id)
        {
            try
            {
                ProjectService.Retire(id);
                AddAlert(AlertViewModel.AlertType.Success, "Project retired successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error retiring project.");
            }

            return RedirectToAction("EditProject", new { id = id });
        }

        /// <summary>
        /// Resume the project with the given ID
        /// </summary>
        /// <param name="id">The project GUID</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ResumeProject(Guid id)
        {
            try
            {
                ProjectService.Restore(id);
                AddAlert(AlertViewModel.AlertType.Success, "Project resumed successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error resuming project.");
            }

            return RedirectToAction("EditProject", new { id = id });
        }

        #endregion

        #endregion

        #endregion

        #region HELPERS

        /// <summary>
        /// Get the framework view for a given module in the default template.
        /// </summary>
        private ActionResult GetFrameworkView(
            Module.ModuleType moduleType,
            Guid? topicGroup,
            int topicPageNo,
            Guid? topic,
            int taskPageNo,
            bool retiredTopics,
            bool retiredTasks
        )
        {
            var viewModel = ModuleCore.GetFrameworkViewModel(
                moduleType,
                topicGroup,
                topicPageNo,
                topic,
                taskPageNo,
                retiredTopics,
                retiredTasks
            );

            AppSession.Current.TopicGroupFormReturnUrl = Request.RawUrl;
            AppSession.Current.TopicFormReturnUrl = Request.RawUrl;
            AppSession.Current.TaskFormReturnUrl = Request.RawUrl;

            return View("Framework", viewModel);
        }

        /// <summary>
        /// Get action result for a sucessful submission of the contact form depending on
        /// the submit button pressed.
        /// </summary>
        /// <param name="contact">The updated Contact</param>
        /// <param name="successMessage">The message to use in the success alert</param>
        /// <returns></returns>
        private ActionResult GetContactSuccessResult(Contact contact, string successMessage)
        {
            switch (Request.Form["Redirect"])
            {
                case "VoiceMessages":
                    return RedirectToAction("Index", "VoiceMessage", new { area = "", id = contact.ID, type = ItemType.Contact });
                case "Attachments":
                    return RedirectToAction("Index", "Attachments", new { area = "", id = contact.ID, type = ItemType.Contact });
                case "AddAnother":
                    return RedirectToAction("CreateRelationship");
            }

            AddAlert(AlertViewModel.AlertType.Success, successMessage);
            return Redirect(AppSession.Current.ContactFormReturnUrl);
        }

        /// <summary>
        /// Get action result for a sucessful submission of the project form depending on
        /// the submit button pressed.
        /// </summary>
        /// <param name="projectId">The project GUID</param>
        /// <param name="successMessage">The message to use in the success alert</param>
        /// <returns></returns>
        private ActionResult GetProjectSuccessResult(Guid projectId, string successMessage)
        {
            // Redirect to a specific action depending on the submit button pressed
            switch (Request.Form["Redirect"])
            {
                case "VoiceMessages":
                    return RedirectToAction("Index", "VoiceMessage", new { area = "", id = projectId, type = ItemType.Project });
                case "Attachments":
                    return RedirectToAction("Index", "Attachments", new { area = "", id = projectId, type = ItemType.Project });
                case "None":
                    return RedirectToAction("EditProject", new { id = projectId });
            }

            AddAlert(AlertViewModel.AlertType.Success, successMessage);
            return Redirect(AppSession.Current.ProjectFormReturnUrl);
        }

        #endregion

        #endregion
    }
}
