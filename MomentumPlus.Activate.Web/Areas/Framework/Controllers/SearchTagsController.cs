﻿using System.Linq;
using System.Web.Mvc;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Controllers;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Areas.Framework.Controllers
{
    [Authorise(Roles = RoleAuthHelper.SearchTags_General)]
    public class SearchTagsController : MasterListController
    {
        public SearchTagsController(
            IAuditService auditService,
            IMasterListService masterListService,
            IPackageService packageService,
            IUserService userService
        )
            : base(
                auditService,
                masterListService,
                packageService,
                userService,
                MasterList.ListType.SearchTags
            )
        {
        }

        /// <summary>
        /// Get search tag suggestions based on a given query
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [Authorise(Roles = RoleAuthHelper.SearchTags_GetSuggestions)]
        [HttpGet]
        public ActionResult GetSuggestions(string query)
        {
            var matches = MasterListService.Search(MasterList.ListType.SearchTags,
                query, 1, 5, MasterList.SortColumn.Name, true);

            if (matches != null && matches.Any())
            {
                return Json(matches.Select(m => new { value = m.Name }),
                    JsonRequestBehavior.AllowGet);
            }

            return null;
        }
    }
}
