﻿using System;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Controllers;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Areas.Framework.Controllers
{
    [Authorise(Roles = RoleAuthHelper.Websites_General)]
    public class WebsitesController : MasterListController
    {
        public WebsitesController(
            IAuditService auditService,
            IMasterListService masterListService,
            IPackageService packageService,
            IUserService userService
        )
            : base(
                auditService,
                masterListService,
                packageService,
                userService,
                MasterList.ListType.Websites
            )
        {
        }

        /// <summary>
        /// Checks if the value entered in the Name field is a valid URI.
        /// </summary>
        protected override void Validate(Web.Models.MasterListViewModel model)
        {
            if (!Uri.IsWellFormedUriString(model.AdditionalField1, UriKind.RelativeOrAbsolute))
            {
                ModelState.AddModelError("AdditionalField1", "Please enter a valid URL.");
            }
        }
    }
}
