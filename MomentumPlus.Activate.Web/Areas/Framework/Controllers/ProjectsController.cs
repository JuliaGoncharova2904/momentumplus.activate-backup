﻿using System;
using System.Web.Mvc;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Controllers;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Areas.Framework.Controllers
{
    [Authorise(Roles = RoleAuthHelper.Projects_General)]
    public class ProjectsController : MasterListController
    {
        public ProjectsController(
            IAuditService auditService,
            IMasterListService masterListService,
            IPackageService packageService,
            IUserService userService
        )
            : base(
                auditService,
                masterListService,
                packageService,
                userService,
                MasterList.ListType.Projects
            )
        {
        }

        /// <summary>
        /// Get basic info for the project with the given ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorise(Roles = RoleAuthHelper.Projects_GetProjectInfo)]
        [HttpGet]
        public ActionResult GetProjectInfo(Guid id)
        {
            var project = MasterListService.GetByMasterListId(id);

            if (project != null)
            {
                return Json(new
                {
                    projectSponsor = project.AdditionalField2
                }, JsonRequestBehavior.AllowGet);
            }

            return null;
        }
    }
}
