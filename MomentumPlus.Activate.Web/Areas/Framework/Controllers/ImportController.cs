﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MomentumPlus.Activate.Services.Helpers;
using MomentumPlus.Activate.Web.Areas.Framework.Models;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Controllers;
using MomentumPlus.Activate.Web.Core;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;

namespace MomentumPlus.Activate.Web.Areas.Framework.Controllers
{
    [Authorise(Roles = RoleAuthHelper.ABSOLUTE_GENERAL_ACCESS)]
    public class ImportController : BaseController
    {
        #region IOC

        public ImportController()
        {
        }

        #endregion

        #region ACTIONS

        /// <summary>
        /// Render import form.
        /// </summary>
        [HttpGet]
        public ActionResult Index()
        {
            var viewModel = new ImportViewModel();
            return View(viewModel);
        }

        /// <summary>
        /// Handle import form submission.
        /// </summary>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(ImportViewModel viewModel, HttpPostedFileBase file)
        {
            try
            {
                // Ensure file was submitted and has the correct type
                if (file == null || Path.GetExtension(file.FileName).ToLower() != ".csv")
                {
                    ModelState.AddModelError("file", "Please select a CSV file to upload.");
                }
                else
                {
                    List<string> results = null;

                    switch (viewModel.ItemType)
                    {
                        case ItemType.Task:
                            using (TaskImporter importer = new TaskImporter())
                            {
                                results = importer.Import(file.InputStream);
                            }

                            break;
                        case ItemType.Topic:
                            using (TopicImporter importer = new TopicImporter())
                            {
                                results = importer.Import(file.InputStream);
                            }

                            break;
                        case ItemType.MasterList:
                            using (MasterListImporter importer = new MasterListImporter())
                            {
                                results = importer.Import(file.InputStream);
                            }

                            break;
                        case ItemType.Relationship:
                            using (RelationshipImporter importer = new RelationshipImporter())
                            {
                                results = importer.Import(file.InputStream);
                            }

                            break;
                    }

                    if (results == null)
                    {
                        ModelState.AddModelError("ItemType", "Import currently not supported for type: " + viewModel.ItemType);
                    }
                    else if (results.Any())
                    {
                        results.ForEach(r => ModelState.AddModelError("file", r));
                    }
                }

                // If there were errors then re-render the view
                if (!ModelState.IsValid)
                {
                    return View(viewModel);
                }

                AddAlert(AlertViewModel.AlertType.Success, string.Format("Items imported successfully (Ref: {0}).", SystemTime.Now));
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Something went wrong, please try again.");
            }

            return RedirectToAction("Index");
        }

        #endregion
    }
}
