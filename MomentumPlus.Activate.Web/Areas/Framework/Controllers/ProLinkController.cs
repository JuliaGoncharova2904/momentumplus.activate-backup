﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MomentumPlus.Activate.Web.Controllers;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Areas.Framework.Models;
using AutoMapper;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.ProLink;

namespace MomentumPlus.Activate.Web.Areas.Framework.Controllers
{
    public class ProLinkController : BaseController
    {
        private IProLinkService ProLinkService;
        private IModuleService ModuleService;

        /// <summary>
        /// Construct a new <c>ProLinkController</c> with the given service
        /// implementations.
        /// </summary>
        public ProLinkController(
            IAuditService auditService,
            IMasterListService masterListService,
            IPackageService packageService,
            IUserService userService,
            IProLinkService proLink,
            IModuleService moduleService
        )
            : base(
                  auditService,
                  masterListService,
                  packageService,
                  userService
              )
        {
            ProLinkService = proLink;
            ModuleService = moduleService;
        }

        // Render pro-link index view
        // GET: /Framework/ProLink/

        public ActionResult Index()
        {
            if (!MomentumPlus.Activate.Web.Helpers.AccountConfig.Settings.ProLinkEnabled)
                return new HttpStatusCodeResult(404);

            // Set return URLs for forms to the current URL
            AppSession.Current.FunctionFormReturnUrl = Request.RawUrl;
            AppSession.Current.ProcessGroupFormReturnUrl = Request.RawUrl;
            AppSession.Current.TopicFormReturnUrl = Request.RawUrl;

            TempData["InProLink"] = true;

            // Build view model with pro-link items
            ProLinkViewModel model = new ProLinkViewModel()
            {
                Functions = ProLinkService.GetAllFrameworkFunctions()
                                            .OrderBy(f => f.Name)
                                            .Select(f =>
                                            {
                                                f.ProcessGroups = f.ProcessGroups
                                                    .Select(pg =>
                                                    {
                                                        pg.Topics = pg.Topics.Where(t => PackageService.GetByTopicId(t.ID) == null && !t.Deleted.HasValue).OrderBy(t => t.Name).ToList();
                                                        return pg;
                                                    })
                                                    .OrderBy(pg => pg.Name).ToList();
                                                return Mapper.Map<FunctionViewModel>(f);
                                            }),

                BussinesProcessesTopicGroupID = ModuleService.GetByModuleType(Module.ModuleType.Critical)
                                                                .TopicGroups.First(tg => tg.TopicGroupType == TopicGroup.TopicType.Process).ID
            };

            return View(model);
        }

        #region Function

        /// <summary>
        /// Render pro-link function create form
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult CreateFunction()
        {
            if (!MomentumPlus.Activate.Web.Helpers.AccountConfig.Settings.ProLinkEnabled)
                return new HttpStatusCodeResult(404);

            var model = new FunctionViewModel();

            return View("FunctionForm", model);
        }

        /// <summary>
        /// Handle submission of pro-link create form
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateFunction([Bind(Exclude = "ID")]FunctionViewModel model)
        {
            if (!MomentumPlus.Activate.Web.Helpers.AccountConfig.Settings.ProLinkEnabled)
                return new HttpStatusCodeResult(404);

            if (!ModelState.IsValid)
            {
                return View("FunctionForm", model);
            }

            try
            {
                // Add new pro-link function the database
                var function = Mapper.Map<Function>(model);
                function.IsFramework = true;

                ProLinkService.Add(function);

                AddAlert(AlertViewModel.AlertType.Success, "Function added successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error adding function.");
            }
            return Redirect(AppSession.Current.FunctionFormReturnUrl);
        }

        /// <summary>
        /// Render pro-link function edit form for the given function
        /// </summary>
        /// <param name="id">Function GUID</param>
        /// <returns></returns>
        public ActionResult UpdateFunction(Guid? id)
        {
            if (!MomentumPlus.Activate.Web.Helpers.AccountConfig.Settings.ProLinkEnabled)
                return new HttpStatusCodeResult(404);

            if (!id.HasValue)
            {
                AddAlert(AlertViewModel.AlertType.Danger, "Invalid function id.");
                return Redirect(AppSession.Current.FunctionFormReturnUrl);
            }

            var function = ProLinkService.GetFunction(id.Value);
            if (function == null)
            {
                AddAlert(AlertViewModel.AlertType.Danger, "Function with given id does not exist.");
                return Redirect(AppSession.Current.FunctionFormReturnUrl);
            }

            var model = Mapper.Map<FunctionViewModel>(function);

            return View("FunctionForm", model);
        }

        /// <summary>
        /// Update a pro-link function
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult UpdateFunction(FunctionViewModel model)
        {
            if (!MomentumPlus.Activate.Web.Helpers.AccountConfig.Settings.ProLinkEnabled)
                return new HttpStatusCodeResult(404);

            var function = ProLinkService.GetFunction(model.ID.Value);
            if (function == null)
            {
                AddAlert(AlertViewModel.AlertType.Danger, "Function with given id does not exist.");
                return Redirect(AppSession.Current.FunctionFormReturnUrl);
            }

            function = Mapper.Map(model, function);

            try
            {
                ProLinkService.Update(function);
                AddAlert(AlertViewModel.AlertType.Success, "Function updated successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error updating function.");
            }

            return Redirect(AppSession.Current.FunctionFormReturnUrl);
        }

        #endregion

        #region ProcessGroup

        /// <summary>
        /// Render process group create form
        /// </summary>
        /// <param name="id">Parent function GUID</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult CreateProcessGroup(Guid? id)
        {
            if (!MomentumPlus.Activate.Web.Helpers.AccountConfig.Settings.ProLinkEnabled)
                return new HttpStatusCodeResult(404);

            if (!id.HasValue)
            {
                AddAlert(AlertViewModel.AlertType.Danger, "Process group must have a parent function.");
                return Redirect(AppSession.Current.ProcessGroupFormReturnUrl);
            }

            var function = ProLinkService.GetFunction(id.Value);

            var model = new ProcessGroupViewModel();
            model.FunctionId = id.Value;
            model.FunctionName = function.Name;

            return View("ProcessGroupForm", model);
        }

        /// <summary>
        /// Handle submission of process group create form
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateProcessGroup([Bind(Exclude = "ID")]ProcessGroupViewModel model)
        {
            if (!MomentumPlus.Activate.Web.Helpers.AccountConfig.Settings.ProLinkEnabled)
                return new HttpStatusCodeResult(404);

            if (model.FunctionId.HasValue)
            {
                if (ProLinkService.Exists(model.FunctionId.Value, model.Name))
                {
                    ModelState.AddModelError("Name", "Process Group with this name already exists.");
                }
            }


            if (!ModelState.IsValid)
            {
                return View("ProcessGroupForm", model);
            }

            try
            {
                var processGroup = Mapper.Map<ProcessGroup>(model);

                var newProcessGroup = ProLinkService.Add(processGroup);

                string CookieName = "selectedProcessGroup";
                HttpCookie selectedProcessGroupCookie = HttpContext.Request.Cookies[CookieName] ?? new HttpCookie(CookieName);
                selectedProcessGroupCookie.Expires = DateTime.Now.AddDays(1);
                selectedProcessGroupCookie.Path = "/Framework";
                selectedProcessGroupCookie.Value = newProcessGroup.ID.ToString();
                HttpContext.Response.Cookies.Add(selectedProcessGroupCookie);



                AddAlert(AlertViewModel.AlertType.Success, "Process group added successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error adding process group.");
            }

            return Redirect(AppSession.Current.ProcessGroupFormReturnUrl);
        }

        /// <summary>
        /// Render process group edit form
        /// </summary>
        /// <param name="id">Process group GUID</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult UpdateProcessGroup(Guid? id)
        {
            if (!MomentumPlus.Activate.Web.Helpers.AccountConfig.Settings.ProLinkEnabled)
                return new HttpStatusCodeResult(404);

            if (!id.HasValue)
            {
                AddAlert(AlertViewModel.AlertType.Danger, "Invalid process group id.");
                return Redirect(AppSession.Current.ProcessGroupFormReturnUrl);
            }

            var processGroup = ProLinkService.GetProcessGroup(id.Value);
            if (processGroup == null)
            {
                AddAlert(AlertViewModel.AlertType.Danger, "Process group with given id does not exist.");
                return Redirect(AppSession.Current.ProcessGroupFormReturnUrl);
            }

            var model = Mapper.Map<ProcessGroupViewModel>(processGroup);

            return View("ProcessGroupForm", model);
        }

        /// <summary>
        /// Handle submission of process group edit form
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult UpdateProcessGroup(ProcessGroupViewModel model)
        {
            if (!MomentumPlus.Activate.Web.Helpers.AccountConfig.Settings.ProLinkEnabled)
                return new HttpStatusCodeResult(404);

            var processGroup = ProLinkService.GetProcessGroup(model.ID.Value);
            if (processGroup == null)
            {
                AddAlert(AlertViewModel.AlertType.Danger, "Process group with given id does not exist.");
                return Redirect(AppSession.Current.ProcessGroupFormReturnUrl);
            }

            //processGroup = Mapper.Map(model, processGroup);

            try
            {
                ProLinkService.UpdateById(model.ID.Value, model.Name, model.Description);
                AddAlert(AlertViewModel.AlertType.Success, "Process group updated successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error updating process group.");
            }

            return Redirect(AppSession.Current.ProcessGroupFormReturnUrl);
        }

        #endregion
    }
}
