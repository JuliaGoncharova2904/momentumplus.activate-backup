﻿using System;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Areas.Framework.Models;
using MomentumPlus.Activate.Web.Controllers;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;
using System.Collections.Generic;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Areas.Framework.Controllers
{
    [Authorise(Roles = RoleAuthHelper.Template_General)]
    public class TemplateController : BaseController
    {
        #region IOC

        private readonly ITemplateService TemplateService;
        private readonly IProLinkService ProLinkService;

        public TemplateController(
            IAuditService auditService,
            IMasterListService masterListService,
            IPackageService packageService,
            ITemplateService templateService,
            IUserService userService,
            IProLinkService proLink
        )
            : base(
                auditService,
                masterListService,
                packageService,
                userService)
        {
            TemplateService = templateService;
            ProLinkService = proLink;
        }

        #endregion

        #region ACTIONS

        #region Index

        /// <summary>
        /// Template manager index view.
        /// </summary>
        [HttpGet]
        public ActionResult Index(Guid? selectedTemplate = null)
        {
            var viewModel = GetFrameworkTemplateViewModel(selectedTemplate);
            AppSession.Current.TemplateFormReturnUrl = Request.RawUrl;
            return View(viewModel);
        }

        #endregion

        #region Transfer

        /// <summary>
        /// Template transfer view.
        /// </summary>
        [HttpGet]
        public ActionResult Transfer(Guid? selectedTemplate = null)
        {
            var viewModel = GetFrameworkTemplateViewModel(selectedTemplate);

            // Only display items enabled in the template and set enabled to false by default for the transfer
            viewModel.TemplateItemsCriticalTopicGroups = viewModel.TemplateItemsCriticalTopicGroups.Where(i => i.Enabled).ToList();
            viewModel.TemplateItemsCriticalTopicGroups.Each(i => i.Enabled = false);
            viewModel.TemplateItemsCriticalTopics = viewModel.TemplateItemsCriticalTopics.Where(i => i.Enabled).ToList();
            viewModel.TemplateItemsCriticalTopics.Each(i => i.Enabled = false);
            viewModel.TemplateItemsCriticalTasks = viewModel.TemplateItemsCriticalTasks.Where(i => i.Enabled).ToList();
            viewModel.TemplateItemsCriticalTasks.Each(i => i.Enabled = false);

            viewModel.TemplateItemsExperiencesTopicGroups = viewModel.TemplateItemsExperiencesTopicGroups.Where(i => i.Enabled).ToList();
            viewModel.TemplateItemsExperiencesTopicGroups.Each(i => i.Enabled = false);
            viewModel.TemplateItemsExperiencesTopics = viewModel.TemplateItemsExperiencesTopics.Where(i => i.Enabled).ToList();
            viewModel.TemplateItemsExperiencesTopics.Each(i => i.Enabled = false);
            viewModel.TemplateItemsExperiencesTasks = viewModel.TemplateItemsExperiencesTasks.Where(i => i.Enabled).ToList();
            viewModel.TemplateItemsExperiencesTasks.Each(i => i.Enabled = false);

            return View(viewModel);
        }

        #endregion

        #region UpdateTemplateItems

        /// <summary>
        /// Update the template items and enabled modules in a given template.
        /// </summary>
        [HttpPost]
        public void UpdateTemplateItems(
            Guid templateId,
            ICollection<TemplateItemLight> lightTemplateItemsCritical,
            ICollection<TemplateItemLight> lightTemplateItemsExperiences,
            EnabledModules enabledModules,
            bool updatePackages = false
        )
        {
            if (lightTemplateItemsCritical != null)
            {
                var templateItemsCritical = lightTemplateItemsCritical.Select(i =>
                    new TemplateItem
                    {
                        SourceReferenceId = i.I,
                        SourceParentReferenceId = i.P,
                        Enabled = true,
                        Type = i.T,
                        IsNew = i.N
                    });

                TemplateService.Save(templateId, templateItemsCritical.ToList(), Module.ModuleType.Critical, updatePackages);
            }
            else
            {
                TemplateService.Save(templateId, new List<TemplateItem>(), Module.ModuleType.Critical);
            }

            if (enabledModules != null)
            {
                var template = TemplateService.GetByTemplateId(templateId);
                template.ExperiencesEnabled = enabledModules.E;
                template.ProjectsEnabled = enabledModules.Pr;
                template.PeopleManagementEnabled = enabledModules.Pe;



                TemplateService.UpdateTemplate(template);

                if (template.ExperiencesEnabled && lightTemplateItemsExperiences != null)
                {
                    var templateItemsExperiences = lightTemplateItemsExperiences.Select(i =>
                        new TemplateItem
                        {
                            SourceReferenceId = i.I,
                            SourceParentReferenceId = i.P,
                            Enabled = true,
                            Type = i.T,
                            IsNew = i.N
                        });

                    TemplateService.Save(templateId, templateItemsExperiences.ToList(), Module.ModuleType.Experiences, updatePackages);
                }
                else
                {
                    TemplateService.Save(templateId, new List<TemplateItem>(), Module.ModuleType.Experiences);
                }
            }
        }

        #endregion

        #region TransferTemplateItems

        /// <summary>
        /// Transfer items from one template to another.
        /// </summary>
        [HttpPost]
        public void TransferTemplateItems(
            Guid templateIdSource,
            Guid templateIdDest,
            IEnumerable<int> sourceReferenceIdsCritical,
            IEnumerable<int> sourceReferenceIdsExperiences,
            IEnumerable<int> sourceReferenceIdsProjects)
        {
            if (sourceReferenceIdsCritical != null)
            {
                TemplateService.TransferTemplateItemsCritical(templateIdSource,
                     sourceReferenceIdsCritical, templateIdDest);
            }

            if (sourceReferenceIdsExperiences != null)
            {
                TemplateService.TransferTemplateItemsCritical(templateIdSource,
                    sourceReferenceIdsExperiences, templateIdDest);
            }

            if (sourceReferenceIdsProjects != null)
            {
                TemplateService.TransferTemplateItemsProjects(templateIdSource,
                    sourceReferenceIdsProjects, templateIdDest);
            }
        }

        #endregion

        #region CreateTemplate

        /// <summary>
        /// Render the template form in create mode.
        /// </summary>
        [EnforceTemplateLimit]
        [HttpGet]
        public ActionResult CreateTemplate()
        {
            return View("TemplateForm", new TemplateViewModel());
        }

        /// <summary>
        /// Handle template create form submission.
        /// </summary>
        [EnforceTemplateLimit]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateTemplate(TemplateViewModel templateViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View("TemplateForm", templateViewModel);
            }

            try
            {
                var templateId = TemplateService.Add(templateViewModel.Name,
                    templateViewModel.DurationDays, templateViewModel.Notes);

                var templateItemsExperiences = TemplateService.GetById(templateId, Module.ModuleType.Experiences);
                templateItemsExperiences.Each(i => i.Enabled = true);
                TemplateService.Save(templateId, templateItemsExperiences.ToList(), Module.ModuleType.Experiences);

                AuditService.RecordActivity<Template>(AuditLog.EventType.Created, templateId);
                AddAlert(AlertViewModel.AlertType.Success, "Template added successfully.");
                return RedirectToAction("Index", new { selectedTemplate = templateId });
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error adding template.");
            }

            return Redirect(AppSession.Current.TemplateFormReturnUrl);
        }

        #endregion

        #region EditTemplate

        /// <summary>
        /// Render the template form in edit mode.
        /// </summary>
        /// <param name="id">The ID of the template being edited.</param>
        [HttpGet]
        public ActionResult EditTemplate(Guid id)
        {
            var template = TemplateService.GetByTemplateId(id);
            var templateViewModel = Mapper.Map<TemplateViewModel>(template);
            return View("TemplateForm", templateViewModel);
        }

        /// <summary>
        /// Handle template edit form submission.
        /// </summary>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTemplate(TemplateViewModel templateViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View("TemplateForm", templateViewModel);
            }

            try
            {
                TemplateService.UpdateTemplate(templateViewModel.ID, templateViewModel.Name,
                    templateViewModel.DurationDays, templateViewModel.Notes);

                AddAlert(AlertViewModel.AlertType.Success, "Template updated successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error updating template.");
            }

            return Redirect(AppSession.Current.TemplateFormReturnUrl);
        }

        #endregion

        #region RetireTemplate/ResumeTemplate

        /// <summary>
        /// Retire the template with the given ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult RetireTemplate(Guid id)
        {
            try
            {
                TemplateService.Retire(id);
                AddAlert(AlertViewModel.AlertType.Success, "Template retired successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error retiring template.");
            }

            return RedirectToAction("EditTemplate", new { id = id });
        }

        /// <summary>
        /// Resume the template with the given ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [EnforceTemplateLimit]
        [HttpGet]
        public ActionResult ResumeTemplate(Guid id)
        {
            try
            {
                TemplateService.Restore(id);
                AddAlert(AlertViewModel.AlertType.Success, "Template resumed successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error resuming template.");
            }

            return RedirectToAction("EditTemplate", new { id = id });
        }

        #endregion

        #endregion

        #region HELPERS

        /// <summary>
        /// Get the model for template manager views for a specific template.
        /// </summary>
        private FrameworkTemplateViewModel GetFrameworkTemplateViewModel(Guid? selectedTemplate = null)
        {
            var viewModel = new FrameworkTemplateViewModel();
            var templates = TemplateService.GetAll().ToList();
            templates.AddRange(TemplateService.GetAll(true));

            // Template selector dropdown
            viewModel.Templates = templates.Select(t =>
                new SelectListItem
                {
                    Text = t.Name + (t.Deleted.HasValue ? " (RETIRED)" : ""),
                    Value = t.ID.ToString()
                }).ToList();

            (viewModel.Templates as List<SelectListItem>).Insert(0, new SelectListItem()
            {
                Text = "Select Template",
                Value = "",
                Selected = true,
                Disabled = true
            });

            // If no template is specified then hide the template controls
            if (!selectedTemplate.HasValue)
            {
                viewModel.HideControls = true;
            }

            if (selectedTemplate.HasValue)
            {
                viewModel.SelectedTemplate = selectedTemplate.Value;

                var template = templates.FirstOrDefault(t => t.ID == selectedTemplate.Value);
                viewModel.Template = template;

                // Don't allow editing of deleted templates
                if (template != null && template.Deleted.HasValue)
                {
                    viewModel.HideControls = true;
                    AddAlert(AlertViewModel.AlertType.Warning, "This template is retired, you must resume it before you can use it.");
                }
                else
                {
                    var templateItemsCritical = TemplateService.GetById(selectedTemplate.Value, Module.ModuleType.Critical);
                    var templateItemsExperiences = TemplateService.GetById(selectedTemplate.Value, Module.ModuleType.Experiences);

                    var templateItemsCriticalTopicGroups = templateItemsCritical
                        .Where(i => i.Type == TemplateItem.TemplateItemType.TopicGroup)
                        .OrderBy(i => i.Name);
                    viewModel.TemplateItemsCriticalTopicGroups = Mapper.Map<IEnumerable<TemplateItem>,
                        IEnumerable<TemplateItemViewModel>>(templateItemsCriticalTopicGroups);

                    var templateItemsCriticalTopics = templateItemsCritical
                        .Where(i => i.Type == TemplateItem.TemplateItemType.Topic)
                        .OrderBy(i => i.Name);
                    viewModel.TemplateItemsCriticalTopics = Mapper.Map<IEnumerable<TemplateItem>,
                        IEnumerable<TemplateItemViewModel>>(templateItemsCriticalTopics);

                    var templateItemsCriticalTasks = templateItemsCritical
                        .Where(i => i.Type == TemplateItem.TemplateItemType.Task)
                        .OrderBy(i => i.Name);
                    viewModel.TemplateItemsCriticalTasks = Mapper.Map<IEnumerable<TemplateItem>,
                        IEnumerable<TemplateItemViewModel>>(templateItemsCriticalTasks);

                    // If Pro-Link enabled...
                    if (Helpers.AccountConfig.Settings.ProLinkEnabled)
                    {
                        var businessProcessesTopics = ProLinkService.GetAllBusinessProcessTopics().ToList();
                        foreach (var criticalItem in viewModel.TemplateItemsCriticalTopics.ToList())
                        {
                            var topic = businessProcessesTopics.FirstOrDefault(t => t.ReferenceId == criticalItem.SourceReferenceId);
                            if (topic != null)
                            {
                                if (topic.ProcessGroup != null && topic.ProcessGroup.Function != null)
                                {
                                    criticalItem.Name = string.Format("{0} - {1} - {2}",
                                        topic.ProcessGroup.Function.Name,
                                        topic.ProcessGroup.Name, topic.Name);

                                    criticalItem.FunctionReferenceId =
                                        topic.ProcessGroup.Function.ReferenceId;

                                    foreach (var task in viewModel.TemplateItemsCriticalTasks)
                                    {
                                        if (task.SourceParentReferenceId == topic.ReferenceId)
                                        {
                                            task.Name = string.Format("{0} - {1} - {2} - {3}",
                                                topic.ProcessGroup.Function.Name,
                                                topic.ProcessGroup.Name, topic.Name, task.Name);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    var templateItemsExperiencesTopicGroups = templateItemsExperiences
                        .Where(i => i.Type == TemplateItem.TemplateItemType.TopicGroup)
                        .OrderBy(i => i.Name);
                    viewModel.TemplateItemsExperiencesTopicGroups = Mapper.Map<IEnumerable<TemplateItem>,
                        IEnumerable<TemplateItemViewModel>>(templateItemsExperiencesTopicGroups);

                    var templateItemsExperiencesTopics = templateItemsExperiences
                        .Where(i => i.Type == TemplateItem.TemplateItemType.Topic)
                        .OrderBy(i => i.Name);
                    viewModel.TemplateItemsExperiencesTopics = Mapper.Map<IEnumerable<TemplateItem>,
                        IEnumerable<TemplateItemViewModel>>(templateItemsExperiencesTopics);

                    var templateItemsExperiencesTasks = templateItemsExperiences
                        .Where(i => i.Type == TemplateItem.TemplateItemType.Task)
                        .OrderBy(i => i.Name);
                    viewModel.TemplateItemsExperiencesTasks = Mapper.Map<IEnumerable<TemplateItem>,
                        IEnumerable<TemplateItemViewModel>>(templateItemsExperiencesTasks);
                }
            }

            // If Pro-Link enabled...
            if (Helpers.AccountConfig.Settings.ProLinkEnabled)
            {
                viewModel.ProLinkFunctions = ProLinkService.GetAllFrameworkFunctions()
                                                            .Select(f => new SelectListItem()
                                                            {
                                                                Text = f.Name,
                                                                Value = f.ReferenceId.ToString()
                                                            });
            }

            return viewModel;
        }

        #endregion
    }
}
