﻿using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Controllers;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Areas.Framework.Controllers
{
    [Authorise(Roles = RoleAuthHelper.Assets_General)]
    public class AssetsController : MasterListController
    {
        public AssetsController(
            IAuditService auditService,
            IMasterListService masterListService,
            IPackageService packageService,
            IUserService userService
        )
            : base(
                  auditService,
                  masterListService,
                  packageService,
                  userService,
                  MasterList.ListType.Assets
              )
        {
        }
    }
}
