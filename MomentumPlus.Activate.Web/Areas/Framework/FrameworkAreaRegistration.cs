﻿using System.Web.Mvc;

namespace MomentumPlus.Activate.Web.Areas.Framework
{
    public class FrameworkAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Framework";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            // Pro-Link Routes Begin

            context.MapRoute(
                "ProLink_CreateProcess",
                "Framework/ProLink/CreateProcess/{id}",
                new { controller = "Framework", action = "CreateTopic", id = UrlParameter.Optional },
                namespaces: new[] { "MomentumPlus.Activate.Web.Areas.Framework.Controllers" }
            );

            context.MapRoute(
                "ProLink_EditProcess",
                "Framework/ProLink/EditProcess/{id}",
                new { controller = "Framework", action = "EditTopic", id = UrlParameter.Optional },
                namespaces: new[] { "MomentumPlus.Activate.Web.Areas.Framework.Controllers" }
            );

            // Pro-Link Routes End

            context.MapRoute(
                "Framework_default",
                "Framework/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "MomentumPlus.Activate.Web.Areas.Framework.Controllers" }
            );
        }
    }
}
