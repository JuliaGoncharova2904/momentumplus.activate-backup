﻿using System.Collections.Generic;
using System.Globalization;
using System.Web.Mvc;
using AutoMapper;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Controllers;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Activate.Web.Properties;
using System.IO;
using System.Linq;
using System.Web;
using MomentumPlus.Activate.Services;
using AccountConfig = MomentumPlus.Core.Models.AccountConfig;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Areas.Admin.Controllers
{
    [Authorise(Roles = RoleAuthHelper.Settings_General)]
    public class SettingsController : BaseController
    {
        #region IOC

        private IAccountConfigService AccountConfigService;
        private IModuleService ModuleService;
        private ITemplateService TemplateService;
        private ITaskService TaskService;

        public SettingsController(
            IAccountConfigService accountConfigService,
            IModuleService moduleService,
            ITemplateService templateService,
            ITaskService taskService,
            IAuditService auditService,
            IMasterListService masterListService,
            IPackageService packageService,
            IUserService userService
        )
            : base(
                      auditService,
                      masterListService,
                      packageService,
                      userService
                  )
        {
            AccountConfigService = accountConfigService;
            PackageService = packageService;
            ModuleService = moduleService;
            TemplateService = templateService;
            TaskService = taskService;
        }

        #endregion

        #region ACTIONS

        /// <summary>
        /// Render settings form with the default settings populated
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            // Get the AccountConfig entity model from the server
            AccountConfig accountConfigEntityModel = AccountConfigService.Get();
            // Map the AccountConfig entity model to the AccountConfigViewModel domain view model
            AccountConfigViewModel accountConfigViewModel = Mapper.Map<AccountConfig, AccountConfigViewModel>(accountConfigEntityModel);

            accountConfigViewModel.TemplatesCount = TemplateService.Count();
            accountConfigViewModel.PackagesCount = PackageService.Count();

            // Read in application settings from config
            accountConfigViewModel.ProjectVersion = Settings.Default.ProjectVersion;

            accountConfigViewModel.EarliestStartWeekListItems = GetEarliestStartWeekListItems(accountConfigEntityModel.EarliestStartWeek);
            accountConfigViewModel.FiscalYearStartMonthListItems = GetFiscalYearStartMonthListItems(accountConfigEntityModel.FiscalYearStartMonth);

            // If limits are not set use defaults from config
            if (!accountConfigViewModel.TemplateLimit.HasValue) accountConfigViewModel.TemplateLimit = Settings.Default.DefaultTemplateLimit;
            if (!accountConfigViewModel.PackageLimit.HasValue) accountConfigViewModel.PackageLimit = Settings.Default.DefaultPackageLimit;

            accountConfigViewModel.ReadOnly = RoleAuthHelper.IsReadOnly("Admin_Settings_Index");
            return View("~/Areas/Admin/Views/Settings/Index.cshtml", accountConfigViewModel);
        }

        /// <summary>
        /// Handle settings form submission
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(AccountConfigViewModel viewModel)
        {
            viewModel.EarliestStartWeekListItems = GetEarliestStartWeekListItems(viewModel.EarliestStartWeek);
            viewModel.FiscalYearStartMonthListItems = GetFiscalYearStartMonthListItems(viewModel.FiscalYearStartMonth);

            if (!ModelState.IsValid)
            {
                return View("~/Areas/Admin/Views/Settings/Index.cshtml", viewModel);
            }

            // Get an AccountConfig object from the server and update it with the data from the AccountConfigViewModel
            AccountConfig ac = AccountConfigService.Get();

            // Update company logo
            if (viewModel.UploadedLogo != null)
            {
                Stream stream = viewModel.UploadedLogo.InputStream;
                System.Drawing.Image image = System.Drawing.Image.FromStream(stream);

                if ((image.Height > 150 && image.Width > 400) && (viewModel.UploadedLogo.ContentType != "image/jpeg" || viewModel.UploadedLogo.ContentType != "image/png"))
                {
                    ModelState.AddModelError("UploadedLogo", @"Image must be .png, .jpeg, .jpg and 400x150px");
                    return View("~/Areas/Admin/Views/Settings/Index.cshtml", viewModel);
                }

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    image.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Gif);
                    ac.Logo = ac.Logo ?? new Image();
                    ac.Logo.BinaryData = memoryStream.ToArray();
                    ac.Logo.FileType = Path.GetExtension(viewModel.UploadedLogo.FileName);
                    ac.Logo.ContentType = viewModel.UploadedLogo.ContentType;
                }
            }

            ac.TenantFriendlyName = viewModel.TenantFriendlyName;
            ac.TaskAlertDaysMedium = viewModel.TaskAlertDaysMedium;
            ac.TaskAlertDaysHigh = viewModel.TaskAlertDaysHigh;
            ac.TopicAlertDaysMedium = viewModel.TopicAlertDaysMedium;
            ac.TopicAlertDaysHigh = viewModel.TopicAlertDaysHigh;

            ac.FileExtensions = viewModel.FileExtensions;

            ac.TargetAverageWordcount = viewModel.TargetAverageWordcount;
            ac.TargetVolume = viewModel.TargetVolume;
            ac.TargetAttachments = viewModel.TargetAttachments;

            ac.UserInternalContacts = viewModel.UserInternalContacts;
            ac.ProjectIdFormat = viewModel.ProjectIdFormat;

            ac.EarliestStartWeek = viewModel.EarliestStartWeek;
            ac.FiscalYearStartMonth = viewModel.FiscalYearStartMonth;

            ac.HandoverConsultant = viewModel.HandoverConsultant;
            ac.TechnicalSupport = viewModel.TechnicalSupport;

            viewModel.Logo = ac.Logo;

            if (viewModel.RemoveLogo)
            {
                ac.Logo = null;
                viewModel.Logo = null;
            }

            // Send the updated AccountConfig to the database
            AccountConfigService.Update(ac);
            Helpers.AccountConfig.Settings = ac;

            TaskService.UpdateTaskWeekDue(ac.EarliestStartWeek);

            // Generate meetings for the year
            if (viewModel.Action == "GenerateMeetings")
            {
                var onboarding = PackageService.GetByPackageStatus(Package.Status.New).ToList();
                var ongoing = PackageService.GetByPackageStatus(Package.Status.InProgress).ToList();
                var packages = onboarding.Union(ongoing);

                foreach (var package in packages)
                {
                    PackageService.CreateMidAndEndReviewMeetings(package, ac.FiscalYearStartMonth);
                }
                
            }
            
            AddAlert(AlertViewModel.AlertType.Success, "Account configuration saved successfully.");

            return View("~/Areas/Admin/Views/Settings/Index.cshtml", viewModel);
        }

        public void ValidateContactsImport(HttpPostedFileBase file, ModelStateDictionary ModelState)
        {
            if (Path.GetExtension(file.FileName).ToLower() != ".csv")
            {
                ModelState.AddModelError("ContactImportFile", @"Please select a CSV file to upload.");
            }


        }

        #endregion

        /// <summary>
        /// Get the items to populate the earliest start week dropdown
        /// </summary>
        /// <param name="currentItem"></param>
        /// <returns></returns>
        private IEnumerable<SelectListItem> GetEarliestStartWeekListItems(int currentItem)
        {
            var earliestStartWeekListItems = new List<SelectListItem>();
            for (var i = 1; i <= 3; i++)
            {
                earliestStartWeekListItems.Add(new SelectListItem()
                {
                    Text = string.Format("Week {0}", i),
                    Value = i.ToString(),
                    Selected = i == currentItem
                });
            }
            return earliestStartWeekListItems;
        }

        /// <summary>
        /// Get the items to populate the fiscal year start month dropdown
        /// </summary>
        /// <param name="currentItem"></param>
        /// <returns></returns>
        private IEnumerable<SelectListItem> GetFiscalYearStartMonthListItems(int currentItem)
        {
            var fiscalYearStartMonthListItems = new List<SelectListItem>();
            for (var i = 1; i <= 12; i++)
            {
                fiscalYearStartMonthListItems.Add(new SelectListItem()
                {
                    Text = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(i),
                    Value = i.ToString(),
                    Selected = i == currentItem
                });
            }
            return fiscalYearStartMonthListItems;
        }
    }
}
