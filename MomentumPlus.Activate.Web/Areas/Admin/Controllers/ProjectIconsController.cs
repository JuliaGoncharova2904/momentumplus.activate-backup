﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Controllers;
using MomentumPlus.Activate.Web.Models;
using ImageViewModel = MomentumPlus.Activate.Web.Areas.Admin.Models.ImageViewModel;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Areas.Admin.Controllers
{
    public class ProjectIconsController : BaseController
    {

        private readonly IImageService ImageService;
        private readonly IFileService FileService;

        public ProjectIconsController(
            IImageService imageService, 
            IFileService fileService, 
            IAuditService auditService,
            IMasterListService masterListService,
            IPackageService packageService,
            IUserService userService) : base(
                    auditService,
                    masterListService,
                    packageService,
                    userService
                )
        {
            ImageService = imageService;
            FileService = fileService;
        }

        /// <summary>
        /// Render the project icon index view
        /// </summary>
        /// <param name="retired">
        /// If true will show retired icons only
        /// </param>
        /// <returns>The index view</returns>
        public ActionResult Index(bool retired = false)
        {
            IEnumerable<Image> images = ImageService.GetDefaultImages(retired).OrderBy(i => i.Title);

            ViewBag.Retired = retired;

            return View(images);
        }

        /// <summary>
        /// Render project icon create form
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        /// <summary>
        /// Handle project icon create form submission
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(ImageViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // Read uploaded image into memory and create new image entity
            Image newImage = null;
            using (MemoryStream memoryStream = new MemoryStream())
            {
                model.UploadedImage.InputStream.CopyTo(memoryStream);

                newImage = new Image();
                newImage.BinaryData = memoryStream.ToArray();
                newImage.FileType = Path.GetExtension(model.UploadedImage.FileName);
                newImage.ContentType = model.UploadedImage.ContentType;

                newImage.Title = model.Title;
                newImage.IsDefault = true;
            }

            // Insert the image into the database and alert user
            FileService.AddFile(newImage);

            AddAlert(AlertViewModel.AlertType.Success, "Image added sucessfully.");

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Render project icon edit form for the icon with the
        /// given ID.
        /// </summary>
        /// <param name="id">The project icon GUID</param>
        /// <returns>The project edit form view</returns>
        [HttpGet]
        public ActionResult Edit(Guid id)
        {
            var image = ImageService.GetImage(id);

            if (image == null)
            {
                return HttpNotFound();
            }

            var model = new ImageViewModel()
            {
                Title = image.Title,
                Src = image.ToString(),
                Id = image.ID,
                Retired = image.Deleted.HasValue
            };

            return View(model);
        }

        /// <summary>
        /// Handle submission of project icon form
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ImageViewModel model)
        {
            ModelState.Remove("UploadedImage");
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // Get project icon from the database
            Image image = ImageService.GetImage(model.Id);
            image.Title = model.Title;

            if (model.UploadedImage != null)
            {
                // Read uploaded image into memory and update the entity
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    model.UploadedImage.InputStream.CopyTo(memoryStream);

                    image.BinaryData = memoryStream.ToArray();
                    image.FileType = Path.GetExtension(model.UploadedImage.FileName);
                    image.ContentType = model.UploadedImage.ContentType;
                }
            }

            // Update image in the database and show success alert to user
            FileService.Update(image);
           
            AddAlert(AlertViewModel.AlertType.Success, "Image saved sucessfully.");

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Retire the project icon with the given ID
        /// </summary>
        /// <param name="id">The project icon GUID</param>
        /// <returns>A redirect to the Index action</returns>
        public ActionResult Retire(Guid id)
        {
            // Get project icon from the database
            var image = ImageService.GetImage(id);

            if (image == null)
            {
                return HttpNotFound();
            }

            // Mark as deleted and alert user
            image.Deleted = DateTime.Now;

            FileService.Update(image);

            AddAlert(AlertViewModel.AlertType.Success, "Image retired sucessfully.");

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Resume the project icon with the given ID
        /// </summary>
        /// <param name="id">The project icon GUID</param>
        /// <returns>A redirect to the Index action</returns>
        public ActionResult Resume(Guid id)
        {
            // Get project icon from the database
            var image = ImageService.GetImage(id);

            if (image == null)
            {
                return HttpNotFound();
            }

            // Undelete and alert user
            image.Deleted = null;

            FileService.Update(image);

            AddAlert(AlertViewModel.AlertType.Success, "Image resumed sucessfully.");

            return RedirectToAction("Index");
        }

    }
}