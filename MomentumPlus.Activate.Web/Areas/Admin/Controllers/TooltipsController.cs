﻿using System.Web.Mvc;
using AutoMapper;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Controllers;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Activate.Web.Properties;
using AccountConfig = MomentumPlus.Core.Models.AccountConfig;

namespace MomentumPlus.Activate.Web.Areas.Admin.Controllers
{
    [Authorise(Roles = RoleAuthHelper.Settings_General)]
    public class TooltipsController : BaseController
    {
        #region IOC

        private IAccountConfigService AccountConfigService;
        private IModuleService ModuleService;
        private ITemplateService TemplateService;

        public TooltipsController(
            IAccountConfigService accountConfigService,
            IModuleService moduleService,
            IPackageService packageService,
            ITemplateService templateService,
            IAuditService auditService,
            IMasterListService masterListService,
            IUserService userService
        )
            : base(
                      auditService,
                      masterListService,
                      packageService,
                      userService
                  )
        {
            AccountConfigService = accountConfigService;
            PackageService = packageService;
            ModuleService = moduleService;
            TemplateService = templateService;
        }

        #endregion

        #region ACTIONS

        /// <summary>
        /// Render the tooltips form with the tooltips populated
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            // Get the AccountConfig entity model from the server
            AccountConfig accountConfigEntityModel = AccountConfigService.Get();
            // Map the AccountConfig entity model to the AccountConfigViewModel domain view model
            AccountConfigViewModel accountConfigViewModel = Mapper.Map<AccountConfig, AccountConfigViewModel>(accountConfigEntityModel);
            accountConfigViewModel.TemplatesCount = TemplateService.Count();
            accountConfigViewModel.PackagesCount = PackageService.Count();

            // Read in application settings from config
            accountConfigViewModel.ProjectVersion = Settings.Default.ProjectVersion;

            // If limits are not set use defaults from config
            if (!accountConfigViewModel.TemplateLimit.HasValue) accountConfigViewModel.TemplateLimit = Settings.Default.DefaultTemplateLimit;
            if (!accountConfigViewModel.PackageLimit.HasValue) accountConfigViewModel.PackageLimit = Settings.Default.DefaultPackageLimit;

            accountConfigViewModel.ReadOnly = RoleAuthHelper.IsReadOnly("Admin_Settings_Index");
            return View(accountConfigViewModel);
        }

        /// <summary>
        /// Handle tooltips form submission
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(AccountConfigViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            // Get an AccountConfig object from the server and update it with the data from the AccountConfigViewModel
            AccountConfig ac = AccountConfigService.Get();

            //tooltips
            ac.SummaryToDoMenu = viewModel.SummaryToDoMenu;
            ac.SummaryProgessMenu = viewModel.SummaryProgessMenu;
            ac.CriticalBusinessProcessMenu = viewModel.CriticalBusinessProcessMenu;
            ac.CriticalSystemsMenu = viewModel.CriticalSystemsMenu;
            ac.CriticalSkillsTrainingMenu = viewModel.CriticalSkillsTrainingMenu;
            ac.CriticalToolsEquipMenu = viewModel.CriticalToolsEquipMenu;
            ac.CriticalProjectsMenu = viewModel.CriticalProjectsMenu;
            ac.CriticalStrategicAspectsMenu = viewModel.CriticalStrategicAspectsMenu;
            ac.CriticalRegulatoryComplianceMenu = viewModel.CriticalRegulatoryComplianceMenu;
            ac.CriticalAnythingElseMenu = viewModel.CriticalAnythingElseMenu;
            ac.RelationshipAddNewMenu = viewModel.RelationshipAddNewMenu;
            ac.RelationshipIntroScheduleMenu = viewModel.RelationshipIntroScheduleMenu;
            ac.ExperiencesThreeBestThingsHeader = viewModel.ExperiencesThreeBestThingsHeader;
            ac.ExperiencesNotSoGoodexpMenu = viewModel.ExperiencesNotSoGoodexpMenu;
            ac.ExperiencesTeamAcheivementsMenu = viewModel.ExperiencesTeamAcheivementsMenu;
            ac.ExperiencesOtherThingsMenu = viewModel.ExperiencesOtherThingsMenu;
            ac.ExperiencesAchievementsMadeMenu = viewModel.ExperiencesAchievementsMadeMenu;
            ac.ExperiencesNeedtoChangeMenu = viewModel.ExperiencesNeedtoChangeMenu;
            ac.TasksCoreTasksCoreTasksMenuHeader = viewModel.TasksCoreTasksCoreTasksMenuHeader;
            ac.TasksCoreTasksAdditionalTasksMenu = viewModel.TasksCoreTasksAdditionalTasksMenu;
            ac.TasksOtherTasksCriticalTasksMenu = viewModel.TasksOtherTasksCriticalTasksMenu;
            ac.TasksOtherTasksRelationshipTasksMenu = viewModel.TasksOtherTasksRelationshipTasksMenu;
            ac.TasksOtherTasksExperienceTasksMenu = viewModel.TasksOtherTasksExperienceTasksMenu;
            ac.TasksOtherTasksProjectTasksMenu = viewModel.TasksOtherTasksProjectTasksMenu;
            ac.TopicTitleHeader = viewModel.TopicTitleHeader;
            ac.TopicItemTypeTopicMenu = viewModel.TopicItemTypeTopicMenu;
            ac.TopicNotesMenu = viewModel.TopicNotesMenu;
            ac.TopicVoiceMessagesMenu = viewModel.TopicVoiceMessagesMenu;
            ac.TopicAttachmentsMenu = viewModel.TopicAttachmentsMenu;
            ac.TopicProcessInteractionMenu = viewModel.TopicProcessInteractionMenu;
            ac.TopicInhouseTrainerMenu = viewModel.TopicInhouseTrainerMenu;
            ac.TopicFitForPurposeMenu = viewModel.TopicFitForPurposeMenu;
            ac.TopicLinkedProjectMenu = viewModel.TopicLinkedProjectMenu;
            ac.TopicSearchTagsMenu = viewModel.TopicSearchTagsMenu;
            ac.TopicTasksMenu = viewModel.TopicTasksMenu;
            ac.TopicEmployeeCopyRequestMenu = viewModel.TopicEmployeeCopyRequestMenu;
            ac.TopicReadyforReviewMenu = viewModel.TopicReadyforReviewMenu;
            ac.TopicLineManagerApprovalMenu = viewModel.TopicLineManagerApprovalMenu;
            ac.TopicHRManagerApprovalMenu = viewModel.TopicHRManagerApprovalMenu;

            ac.CreateTaskFormMenu = viewModel.CreateTaskFormMenu;
            ac.CreateTaskFromTopic = viewModel.CreateTaskFromTopic;
            ac.CreateTaskFromName = viewModel.CreateTaskFromName;
            ac.CreateTaskFromHandover = viewModel.CreateTaskFromHandover;
            ac.CreateTaskFromNotes = viewModel.CreateTaskFromNotes;
            ac.CreateTaskFromVoiceMessages = viewModel.CreateTaskFromVoiceMessages;
            ac.CreateTaskFromAttachments = viewModel.CreateTaskFromAttachments;
            ac.CreateTaskFromWeekDue = viewModel.CreateTaskFromWeekDue;
            ac.CreateTaskFromFixedDateTime = viewModel.CreateTaskFromFixedDateTime;
            ac.CreateTaskFromPriority = viewModel.CreateTaskFromPriority;
            ac.CreateTaskFromSearchTags = viewModel.CreateTaskFromSearchTags;
            ac.CreateTaskFromReadyForReview = viewModel.CreateTaskFromReadyForReview;
            ac.CreateTaskFromLineManagerApproval = viewModel.CreateTaskFromLineManagerApproval;
            ac.CreateTaskFromHRManagerApproval = viewModel.CreateTaskFromHRManagerApproval;

            ac.TriggerHeader = viewModel.TriggerHeader;
            ac.TriggerLeaverMenu = viewModel.TriggerLeaverMenu;
            ac.TriggerTriggerMenu = viewModel.TriggerTriggerMenu;
            ac.TriggerFinishDateMenu = viewModel.TriggerFinishDateMenu;
            ac.TriggerPickUpMenu = viewModel.TriggerPickUpMenu;
            ac.TriggerDurationMenu = viewModel.TriggerDurationMenu;
            ac.TriggerNotesMenu = viewModel.TriggerNotesMenu;
            ac.TriggerKeyContactMenu = viewModel.TriggerKeyContactMenu;

            ac.AudioManagerLinkButton = viewModel.AudioManagerLinkButton;
            ac.VoiceMessageManagerLinkButton = viewModel.VoiceMessageManagerLinkButton;

            ac.AudioManagerTitleHeader = viewModel.AudioManagerTitleHeader;
            ac.AudioManagerTitleRightHeader = viewModel.AudioManagerTitleRightHeader;
            ac.AudioManagerAudioPlayerHeader = viewModel.AudioManagerAudioPlayerHeader;
            ac.AudioManagerRecordfromPCButton = viewModel.AudioManagerRecordfromPCButton;
            ac.AudioManagerUploadButton = viewModel.AudioManagerUploadButton;
            ac.AudioManagerUsePhoneButton = viewModel.AudioManagerUsePhoneButton;
            ac.FileUploaderNameMenu = viewModel.FileUploaderNameMenu;
            ac.FileUploaderSearchTagsMenu = viewModel.FileUploaderSearchTagsMenu;
            ac.FileUploaderSelectFileMenu = viewModel.FileUploaderSelectFileMenu;
            ac.AttachmentManagerTitleMenu = viewModel.AttachmentManagerTitleMenu;
            ac.AttachmentManagerTitleRightMenu = viewModel.AttachmentManagerTitleRightMenu;
            ac.AttachmentManagerUploadMenu = viewModel.AttachmentManagerUploadMenu;

            ac.TriggerHeader = viewModel.TriggerHeader;
            ac.TriggerLeaverMenu = viewModel.TriggerLeaverMenu;
            ac.TriggerTriggerMenu = viewModel.TriggerTriggerMenu;
            ac.TriggerFinishDateMenu = viewModel.TriggerFinishDateMenu;
            ac.TriggerPickUpMenu = viewModel.TriggerPickUpMenu;
            ac.TriggerDurationMenu = viewModel.TriggerDurationMenu;
            ac.TriggerNotesMenu = viewModel.TriggerNotesMenu;
            ac.TriggerKeyContactMenu = viewModel.TriggerKeyContactMenu;

            ac.MyPeopleLivePackagesHeader = viewModel.MyPeopleLivePackagesHeader;
            ac.MyPeopleAgedSummaryHeader = viewModel.MyPeopleAgedSummaryHeader;

            ac.ProfileHeader = viewModel.ProfileHeader;
            ac.ProfileSurnameMenu = viewModel.ProfileSurnameMenu;
            ac.ProfileFirstNameMenu = viewModel.ProfileFirstNameMenu;
            ac.ProfileDirectDialMenu = viewModel.ProfileDirectDialMenu;
            ac.ProfileMobileMenu = viewModel.ProfileMobileMenu;
            ac.ProfileLoginEmailMenu = viewModel.ProfileLoginEmailMenu;
            ac.ProfilePasswordMenu = viewModel.ProfilePasswordMenu;
            ac.ProfileNotesMenu = viewModel.ProfileNotesMenu;

            ac.IMDashboardInUseHeader = viewModel.IMDashboardInUseHeader;
            ac.InHandoverExitingHeader = viewModel.InHandoverExitingHeader;
            ac.InHandoverOnboardingHeader = viewModel.InHandoverOnboardingHeader;

            ac.MasterListsSystemsHeader = viewModel.MasterListsSystemsHeader;
            ac.MasterListsExpertUsersHeader = viewModel.MasterListsExpertUsersHeader;
            ac.MasterListsOrganisationContactsHeader = viewModel.MasterListsOrganisationContactsHeader;
            ac.MasterListsProjectsHeader = viewModel.MasterListsProjectsHeader;
            ac.MasterListsWebsitesHeader = viewModel.MasterListsWebsitesHeader;
            ac.MasterListsAssetsHeader = viewModel.MasterListsAssetsHeader;
            ac.MasterListsCompaniesHeader = viewModel.MasterListsCompaniesHeader;
            ac.MasterListsSearchTagsHeader = viewModel.MasterListsSearchTagsHeader;
            ac.MasterListsSubjectivityHeader = viewModel.MasterListsSubjectivityHeader;

            ac.ModulesTopicGroupHeader = viewModel.ModulesTopicGroupHeader;
            ac.ModulesTopicsHeader = viewModel.ModulesTopicsHeader;
            ac.ModulesTasksHeader = viewModel.ModulesTasksHeader;
            ac.ModulesRelationshipDiagramHeader = viewModel.ModulesRelationshipDiagramHeader;
            ac.ModulesCoreTasksHeader = viewModel.ModulesCoreTasksHeader;

            ac.TemplateTopicGroupHeader = viewModel.TemplateTopicGroupHeader;
            ac.TemplateTopicsHeader = viewModel.TemplateTopicsHeader;
            ac.TemplateTasksHeader = viewModel.TemplateTasksHeader;
            ac.TemplateRelationshipDiagramHeader = viewModel.TemplateRelationshipDiagramHeader;

            ac.ImportImportDataHeader = viewModel.ImportImportDataHeader;

            ac.PackagesWorkplacesHeader = viewModel.PackagesWorkplacesHeader;
            ac.PackagesPackagesHeader = viewModel.PackagesPackagesHeader;
            ac.PackagesPositionsHeader = viewModel.PackagesPositionsHeader;
            ac.PackagesEmployeesHeader = viewModel.PackagesEmployeesHeader;

            ac.DocumentLibraryDocumentsHeader = viewModel.DocumentLibraryDocumentsHeader;

            ac.ReportsMainReportHeader = viewModel.ReportsMainReportHeader;
            ac.ReportsAdditionalReportHeader = viewModel.ReportsAdditionalReportHeader;

            // Project Form
            ac.ProjectFormHeader = viewModel.ProjectFormHeader;
            ac.ProjectFormName = viewModel.ProjectFormName;
            ac.ProjectFormProjectId = viewModel.ProjectFormProjectId;
            ac.ProjectFormStart = viewModel.ProjectFormStart;
            ac.ProjectFormEnd = viewModel.ProjectFormEnd;
            ac.ProjectFormProjectManager = viewModel.ProjectFormProjectManager;
            ac.ProjectFormImage = viewModel.ProjectFormImage;
            ac.ProjectFormStatus = viewModel.ProjectFormStatus;
            ac.ProjectFormBudget = viewModel.ProjectFormBudget;
            ac.ProjectFormProjectSpaceLocation = viewModel.ProjectFormProjectSpaceLocation;
            ac.ProjectFormDescription = viewModel.ProjectFormDescription;
            ac.ProjectFormMyRole = viewModel.ProjectFormMyRole;
            ac.ProjectFormHandoverNotes = viewModel.ProjectFormHandoverNotes;

            // Project Card
            ac.ProjectCardFormHeader = viewModel.ProjectCardFormHeader;
            ac.ProjectCardFormMyRole = viewModel.ProjectCardFormMyRole;
            ac.ProjectCardFormHandoverNotes = viewModel.ProjectCardFormHandoverNotes;
            ac.ProjectCardFormPriority = viewModel.ProjectCardFormPriority;
            
            // Project Transfer Form
            ac.ProjectTransferFormSourcePackage = viewModel.ProjectTransferFormSourcePackage;
            ac.ProjectTransferFormSourceProject = viewModel.ProjectTransferFormSourceProject;
            ac.ProjectTransferFormTargetPackage = viewModel.ProjectTransferFormTargetPackage;

            // Approval Form
            ac.ModerationFormLineManagerDecision = viewModel.ModerationFormLineManagerDecision;
            ac.ModerationFormHrManagerDecision = viewModel.ModerationFormHrManagerDecision;

            // Person Form
            ac.PersonFormTitle = viewModel.PersonFormTitle;
            ac.PersonFormFirstName = viewModel.PersonFormFirstName;
            ac.PersonFormLastName = viewModel.PersonFormLastName;
            ac.PersonFormImage = viewModel.PersonFormImage;
            ac.PersonFormCompany = viewModel.PersonFormCompany;
            ac.PersonFormPosition = viewModel.PersonFormPosition;
            ac.PersonFormEmail = viewModel.PersonFormEmail;
            ac.PersonFormPhone = viewModel.PersonFormPhone;
            ac.PersonFormDirect = viewModel.PersonFormDirect;
            ac.PersonFormImportance = viewModel.PersonFormImportance;
            ac.PersonFormNote = viewModel.PersonFormNote;

            ac.StaffFormHeader = viewModel.StaffFormHeader;
            ac.StaffFormUser = viewModel.StaffFormUser;
            ac.StaffFormSearchTags = viewModel.StaffFormSearchTags;
            ac.StaffFormEmploymentStatus = viewModel.StaffFormEmploymentStatus;
            ac.StaffFormFutureTraining = viewModel.StaffFormFutureTraining;
            ac.StaffFormManagementNotes = viewModel.StaffFormManagementNotes;

            AccountConfigService.Update(ac);
            MomentumPlus.Activate.Web.Helpers.AccountConfig.Settings = ac;

            AddAlert(AlertViewModel.AlertType.Success, "Account configuration saved successfully.");

            return View(viewModel);
        }

        /// <summary>
        /// Reset tooltips to their default values
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ResetToDefault(AccountConfigViewModel viewModel)
        {
            AccountConfig ac = AccountConfigService.Get();

            ac.SummaryToDoMenu = "This is a list of tasks you must complete as part of your handover. To mark them as complete, go into the task and check the Ready for Review box.";
            ac.SummaryProgessMenu = "This tells you how much of your profile is completed. To increase the number, mark completed processes and tasks 'Ready for Review'.";
            ac.CriticalBusinessProcessMenu = "These are the processes that you follow in your role. You can edit them by clicking the pencil or add a new one by clicking the white +.";
            ac.CriticalSystemsMenu = "These are the systems you use in your role. You can edit them by pressing the pencil, or add a new one by clicking the white + button.";
            ac.CriticalSkillsTrainingMenu = "These are the formal qualifications or soft skills you've developed that are necessary for your role. You can edit them by pressing the pencil or add a new one by pressing the white + button.";
            ac.CriticalToolsEquipMenu = "These are the important tools or pieces of equipment you use in your role. You can edit them by pressing the pencil, or add a new one by clicking the white + button.";
            ac.CriticalProjectsMenu = "This is a list of recently completed and on-going projects in which you're involved. You can edit them by pressing the pencil, or add a new one by clicking the white + button.";
            ac.CriticalStrategicAspectsMenu = "Here you discuss important strategic information relating to your role. You can edit them by pressing the pencil, or add a new one by clicking the white + button.";
            ac.CriticalRegulatoryComplianceMenu = "This is a list of important regulatory, legal or compliance processes linked to your role. You can edit them by pressing the pencil, or add a new one by clicking the white + button.";
            ac.CriticalAnythingElseMenu = "Here you can add any other important processes not covered in the other headings. You can edit them by pressing the pencil, or add a new one by clicking the white + button.";
            ac.RelationshipAddNewMenu = "Here you can add contact details and notes regarding important people relating to your role. ";
            ac.RelationshipIntroScheduleMenu = "Here you can arrange introductions for the inheritor of this role to the important people, both internal and external, with whom you work. ";
            ac.ExperiencesThreeBestThingsHeader = "Here you can reflect on some of the best things that happened to you during your time in this role. You can edit them by pressing the pencil, or add a new one by clicking the white + button.";
            ac.ExperiencesNotSoGoodexpMenu = "Here you can reflect on some of the negative experiences that you feel the role inheritor ought to know. You can edit them by pressing the pencil, or add a new one by clicking the white + button.";
            ac.ExperiencesTeamAcheivementsMenu = "Here you can discuss some achievements you earned with your team. This could include charity work or a successful project. You can edit them by pressing the pencil, or add a new one by clicking the white + button.";
            ac.ExperiencesOtherThingsMenu = "This is where you can discuss some other things that don't necessarily fit into the other achievement headings. You can edit them by pressing the pencil, or add a new one by clicking the white + button.";
            ac.ExperiencesAchievementsMadeMenu = "Here you can reflect on some of your personal achievements such as exceeding a target or improving a process. You can edit them by pressing the pencil, or add a new one by clicking the white + button.";
            ac.ExperiencesNeedtoChangeMenu = "If you feel the role inheritor should be aware of some things that need to change in the future you should put that here. You can edit them by pressing the pencil, or add a new one by clicking the white + button.";
            ac.TopicTitleHeader = "This is where you can edit the topic.";
            ac.TopicItemTypeTopicMenu = "This is the title of the topic.";
            ac.TopicNotesMenu = "Here you can add a description of the topic.";
            ac.TopicVoiceMessagesMenu = "This will take you to the voice manager if you want to leave a voice message by phone or through your computer microphone.";
            ac.TopicAttachmentsMenu = "This will take you to the attachment manager where you can upload documents or leave file path links.";
            ac.TopicProcessInteractionMenu = "This is how often you perform this process. If the options don't reflect the true interaction timescale, a best guess will do.";
            ac.TopicInhouseTrainerMenu = "This is the person whom the role inheritor should contact for help or training on this process if necessary. ";
            ac.TopicFitForPurposeMenu = "If the process is tried and tested, we recommend you set it to 'It works'. If it needs improvement, an extra box will appear to ask you to expand on why this process needs improvement.";
            ac.TopicLinkedProjectMenu = "If there's a project linked to this process, you can link them here.";
            ac.TopicSearchTagsMenu = "Here you can assign search tags to this process. This is for searching purposes - relevant keywords make good search tags. You need to press enter after every tag to store it.";
            ac.TopicTasksMenu = "This will take you to the task list assigned to this process.";
            ac.TopicEmployeeCopyRequestMenu = "You can request a copy of this process by clicking this button.";
            ac.TopicReadyforReviewMenu = "If you feel you've completed this topic form, you can click here so a line manager or HR manager can approve it.";
            ac.TopicLineManagerApprovalMenu = "Here you can see if your line manager has approved your topic.";
            ac.TopicHRManagerApprovalMenu = "Here you can see if your HR manager has approved your topic.";
            ac.TopicSystemFrequencyOfUse = "";
            ac.TopicSystemExpertUser = "";
            ac.TopicSystemImportanceRating = "";
            ac.TopicSystemWebsiteAddress = "";
            ac.TopicFutureTrainingTrainingFrequency = "";
            ac.TopicFutureTrainingInHouseTrainer = "";
            ac.TopicTrainingRequired = "";
            ac.TopicEquipmentFrequencyOfUse = "";
            ac.TopicEquipmentConditionOfEquipment = "";
            ac.TopicEquipmentExperiencedContact = "";
            ac.TopicProjectProjecctID = "";
            ac.TopicProjectProjectSponsor = "";
            ac.TopicProjectProjectManager = "";
            ac.TopicProjectBudgetSpend = "";
            ac.TopicProjectProjectStatus = "";
            ac.TopicProjectPerceivedSuccess = "";
            ac.TopicProjectProjectEndDate = "";
            ac.TopicStrategicAspectsReviewFrequency = "";
            ac.TopicStrategicAspectsQuality = "";
            ac.TopicRegCompDocumentName = "";
            ac.TopicRegCompDocumentLocation = "";
            ac.TopicRegCompPrimaryContact = "";
            ac.TopicRegCompWebsiteAddress = "";
            ac.TopicLocEnvLocation = "";
            ac.TopicLocEnvAddress = "";
            ac.TopicLocEnvMapLink = "";
            ac.TopicExperiencesWhat = "";
            ac.TopicExperiencesWhen = "";
            ac.TopicExperiencesKeyContact = "";
            ac.TasksCoreTasksCoreTasksMenuHeader = "This is a list of your core tasks from business processes, including their priority. ";
            ac.TasksCoreTasksAdditionalTasksMenu = "This is a list of your tasks from other essential processes. ";
            ac.TasksOtherTasksCriticalTasksMenu = "";
            ac.TasksOtherTasksRelationshipTasksMenu = "This is a list of introductions you have scheduled.";
            ac.TasksOtherTasksExperienceTasksMenu = "This is a list of tasks assigned within Achievements.";
            ac.TriggerHeader = "Can't find these? Return later";
            ac.TriggerLeaverMenu = "Can't find these? Return later";
            ac.TriggerTriggerMenu = "Can't find these? Return later";
            ac.TriggerFinishDateMenu = "This is when the trigger will end.";
            ac.TriggerPickUpMenu = "Can't find these? Return later";
            ac.TriggerDurationMenu = "This is how long the trigger will last.";
            ac.TriggerNotesMenu = "Can't find these? Return later";
            ac.TriggerKeyContactMenu = "Can't find these? Return later";
            ac.CreateTaskFormMenu = "Here you can add a new task";
            ac.CreateTaskFromTopic = "This is the process topic in which you're adding a task.";
            ac.CreateTaskFromName = "Here you can name the task.";
            ac.CreateTaskFromHandover = "This is what actually needs to be done - the task itself - by the person inheriting the role.";
            ac.CreateTaskFromNotes = "Any extra notes relating to this task can be added here, for example people to contact for help.";
            ac.CreateTaskFromVoiceMessages = "You can leave a voice message against a task here.";
            ac.CreateTaskFromAttachments = "You can attach a file to this task here.";
            ac.CreateTaskFromWeekDue = "This is the number of weeks after the new starter begins working when they should complete this task.";
            ac.CreateTaskFromFixedDateTime = "If there's a fixed time or date by which this task must be completed, you can enter that here.";
            ac.CreateTaskFromPriority = "You can indicate how important this task is here.";
            ac.CreateTaskFromSearchTags = "Here you can assign search tags to this task. This is for searching purposes - relevant keywords make good search tags. You need to press enter after every tag to store it.";
            ac.CreateTaskFromReadyForReview = "If you feel you've completed this topic form, you can click here so a line manager or HR manager can approve it.";
            ac.CreateTaskFromLineManagerApproval = "Here you can see if your line manager has approved your topic.";
            ac.CreateTaskFromHRManagerApproval = "Here you can see if your HR manager has approved your topic.";
            ac.AudioManagerLinkButton = "This will take you to the audio manager.";
            ac.VoiceMessageManagerLinkButton = "Here you can record a voice message by phone or by PC.";
            ac.AudioManagerTitleHeader = "Audio manager.";
            ac.AudioManagerTitleRightHeader = "Can't find these? Return later";
            ac.AudioManagerAudioPlayerHeader = "Here you can play back old messages and record new ones by clicking on the record options below.";
            ac.AudioManagerRecordfromPCButton = "This will let you record a message from your computer's microphone. You may need to allow Momentum+ access.";
            ac.AudioManagerUploadButton = "This will allow you to upload a pre-existing audio file on your computer.";
            ac.AudioManagerUsePhoneButton = "Clicking this will allow you to leave a message from your phone. It will give you a PIN which you must use";
            ac.FileUploaderNameMenu = "You'll need to give your file a meaningful name here.";
            ac.FileUploaderSearchTagsMenu = "Here you can assign search tags to this file. This is for searching purposes - relevant keywords make good search tags. You need to press enter after every tag to store it.";
            ac.FileUploaderSelectFileMenu = "This will open a browser so you can find your file.";
            ac.AttachmentManagerTitleMenu = "This is the attachment manager.";
            ac.AttachmentManagerTitleRightMenu = "This is the attachment manager.";
            ac.AttachmentManagerUploadMenu = "You can select the file to upload from here.";
            ac.MyPeopleLivePackagesHeader = "These are all  packages currently in use throughout your company.";
            ac.MyPeopleAgedSummaryHeader = "This shows an update history of all packages currently in use.";
            ac.ProfileHeader = "This form contains your account information.";
            ac.ProfileSurnameMenu = "Your surname should be entered here.";
            ac.ProfileFirstNameMenu = "Enter your first name here.";
            ac.ProfileDirectDialMenu = "You should enter a direct line number here, including any extensions and area codes.";
            ac.ProfileMobileMenu = "Enter the mobile number here.";
            ac.ProfileLoginEmailMenu = "This determines your username for Momentum+ - we recommend you use your standard internal email.";
            ac.ProfilePasswordMenu = "You can change your password here.";
            ac.ProfileNotesMenu = "If you'd like to leave any notes, they can be added here.";
            ac.IMDashboardInUseHeader = "These are all packages currently assigned to you as a manager.";
            ac.InHandoverExitingHeader = "These are all triggered packages currently in the process of exiting";
            ac.InHandoverOnboardingHeader = "These are inherited packages currently in use by new users.";
            ac.MasterListsSystemsHeader = "You can enter details of all your company's systems here.";
            ac.MasterListsExpertUsersHeader = "Expert users are points of contact regarding your company's systems.";
            ac.MasterListsOrganisationContactsHeader = "This is a list of frequently used contacts for your organisation.";
            ac.MasterListsProjectsHeader = "You can enter details of on-going or recently completed projects within your organisation.";
            ac.MasterListsWebsitesHeader = "You can enter websites associated with systems or regulatory bodies here.";
            ac.MasterListsAssetsHeader = "You can enter details of your organisation's assests such as heavy equipment, sites, buildings, etc.";
            ac.MasterListsCompaniesHeader = "If your organisation deals with external contractors, councils or other parties, you can enter their names here.";
            ac.MasterListsSearchTagsHeader = "This can be used to build a body of frequently used search tags.";
            ac.MasterListsSubjectivityHeader = "This is a list of emotive words that your employees may use that may signal concern, such as swear words or emotive terms such as hate or depressing.";
            ac.ModulesTopicGroupHeader = "This separates all the topics into business processes, systems and so on.";
            ac.ModulesTopicsHeader = "These are the topics. You can edit the wording using the pencil.";
            ac.ModulesTasksHeader = "These are the tasks associated with each process that can be edited using the pencil.";
            ac.ModulesRelationshipDiagramHeader = "This a preview of the relationship map.";
            ac.ModulesCoreTasksHeader = "This is a list of all the tasks against every business process.";
            ac.TemplateTopicGroupHeader = "Here you can configure all the templates for your organisation.";
            ac.TemplateTopicsHeader = "Here you can select topic by checking the boxes next to the titles.";
            ac.TemplateTasksHeader = "Here you can select tasks by checking the boxes next to the titles. To view the tasks you need to click on the title of the respective process.";
            ac.TemplateRelationshipDiagramHeader = "This is the relationship diagram for the template.";
            ac.ImportImportDataHeader = "Here you can upload topics and tasks to the framework using a .csv file configured properly. A full explanation is available in the FAQ.";
            ac.PackagesWorkplacesHeader = "Here is a list of all packages according to workplace.";
            ac.PackagesPackagesHeader = "You can sort packages by triggered, live and new here.";
            ac.PackagesPositionsHeader = "Packages are sorted here by position.";
            ac.PackagesEmployeesHeader = "Packages are sorted by employee here.";
            ac.DocumentLibraryDocumentsHeader = "This is the document library. Employees can select files from this library via a topic and attach it.";
            ac.ReportsMainReportHeader = "This is the handover report, containing a summary of all the information in your package. You can download a copy by clicking the download pdf button.";
            ac.ReportsAdditionalReportHeader = "These are your leaver reports and subjectivity reports, which provide a bit more detail of the tasks and topics you've entered such as word counts and amount of subjectivity words used, etc. ";

            AccountConfigService.Update(ac);
            MomentumPlus.Activate.Web.Helpers.AccountConfig.Settings = ac;

            AddAlert(AlertViewModel.AlertType.Success, "Account configuration saved successfully.");

            return RedirectToAction("Index");
        }

        #endregion
    }
}
