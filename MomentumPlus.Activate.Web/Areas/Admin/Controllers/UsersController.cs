﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Controllers;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Activate.Web.Core;
using MomentumPlus.Activate.Web.Models.People;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.People;

namespace MomentumPlus.Activate.Web.Areas.Admin.Controllers
{
    [Authorise(Roles = RoleAuthHelper.Users_General)]
    public class UsersController : EmployeeController
    {
        #region CONSTANTS

        private const int PageSize = 8;

        #endregion

        #region IOC

        /// <summary>
        /// UsersController Constructor
        /// </summary>
        public UsersController(
            IAuditService auditService,
            IContactService contactService,
            IMasterListService masterListService,
            IUserService userService,
            IRoleService roleService,
            IPackageService packageService,
            IPeopleService peopleService,
            IPositionService positionService,
            IProjectService projectService,
            ITemplateService templateService,
            IHandoverService handoverService,
            IWorkplaceService workplaceService,
            EmployeeCore employeeCore,
            PackageCore packageCore
        )
            : base(
                  auditService,
                  contactService,
                  masterListService,
                  packageService,
                  peopleService,
                  positionService,
                  projectService,
                  roleService,
                  templateService,
                  handoverService,
                  userService,
                  workplaceService,
                  employeeCore,
                  packageCore
              )
        {
        }

        #endregion

        #region ACTIONS

        /// <summary>
        /// Main Index Action. Gets all users from the database
        /// </summary>
        [HttpGet]
        public override ActionResult Index(
            Guid? id = null,
            string Query = "",
            int PageNo = 1,
            User.SortColumn SortColumn = MomentumPlus.Core.Models.People.User.SortColumn.LastName,
            bool SortAscending = true,
            Package.Status? packageStatus = null,
            bool retired = false
        )
        {
            IEnumerable<User> results = UserService.Search(Query, PageNo, PageSize, SortColumn, SortAscending, retired);

            IEnumerable<UserViewModel> viewModels = Mapper.Map<IEnumerable<User>, IEnumerable<UserViewModel>>(results);

            EmployeesViewModel viewModel = new EmployeesViewModel
            {
                Users = viewModels,
                SortAscending = SortAscending,
                Retired = retired,
                PageSize = PageSize,
                PageNo = PageNo,
                TotalCount = UserService.Count(retired)
            };

            if (id != null)
            {
                User selectedUser = UserService.GetByUserId(id.Value);
                viewModel.SelectedUser = Mapper.Map<UserViewModel>(selectedUser);
                viewModel.SelectedUserId = id.Value;
            }

            AppSession.Current.EmployeeFormReturnUrl = Request.RawUrl;
            viewModel.ReadOnly = RoleAuthHelper.IsReadOnly("Admin_Users_Settings_Index_Get");
            return View(viewModel);
        }

        #endregion
    }
}
