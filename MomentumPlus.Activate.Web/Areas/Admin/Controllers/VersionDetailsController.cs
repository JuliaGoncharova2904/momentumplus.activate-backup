﻿using System.Web.Mvc;
using AutoMapper;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Controllers;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Activate.Web.Properties;
using AccountConfig = MomentumPlus.Core.Models.AccountConfig;

namespace MomentumPlus.Activate.Web.Areas.Admin.Controllers
{
    [Authorise(Roles = RoleAuthHelper.iHandoverAdmin)]
    public class VersionDetailsController : BaseController
    {

        #region IOC

        private IAccountConfigService AccountConfigService;

        public VersionDetailsController(
            IAccountConfigService accountConfigService,
            IAuditService auditService,
            IMasterListService masterListService,
            IPackageService packageService,
            IUserService userService)
            : base(
                auditService,
                masterListService,
                packageService,
                userService
                )
        {
            AccountConfigService = accountConfigService;
        }

        #endregion

        #region ACTIONS

        /// <summary>
        /// Render the version details form with the fields populated
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            // Get the AccountConfig entity model from the server
            AccountConfig accountConfigEntityModel =
                AccountConfigService.Get();

            // Map the AccountConfig entity model to the AccountConfigViewModel domain view model
            AccountConfigViewModel accountConfigViewModel = Mapper.Map<
                AccountConfig,
                AccountConfigViewModel>(accountConfigEntityModel);

            // If limits are not set use defaults from config
            if (!accountConfigViewModel.TemplateLimit.HasValue) accountConfigViewModel.TemplateLimit = Settings.Default.DefaultTemplateLimit;
            if (!accountConfigViewModel.PackageLimit.HasValue) accountConfigViewModel.PackageLimit = Settings.Default.DefaultPackageLimit;

            return View(accountConfigViewModel);
        }

        /// <summary>
        /// Handle version details form submission
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(AccountConfigViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            // Get an AccountConfig object from the server and update it with the data from the AccountConfigViewModel
            AccountConfig ac = AccountConfigService.Get();
            ac.TenantFriendlyName = viewModel.TenantFriendlyName;
            ac.PurchaseDate = viewModel.PurchaseDate;
            ac.LicenceEndDate = viewModel.LicenceEndDate;
            ac.ProcessSupportTier = viewModel.ProcessSupportTier;
            ac.TemplateLimit = viewModel.TemplateLimit;
            ac.PackageLimit = viewModel.PackageLimit;
            ac.AccountManager = viewModel.AccountManager;
            
            ac.ProLinkEnabled = ac.ProLinkEnabled || viewModel.ProLinkEnabled;

            ac.TwilioAccount = viewModel.TwilioAccount;
            ac.TwilioEndpoint = viewModel.TwilioEndpoint;
            ac.TwilioPhoneNumber = viewModel.TwilioPhoneNumber;
            ac.NumberSMSPerUserPerDay = viewModel.NumberSMSPerUserPerDay;

            ac.ChatService = viewModel.ChatService;
            ac.ChatRooms = viewModel.ChatRooms;

            // Send the updated AccountConfig to the server
            AccountConfigService.Update(ac);
            MomentumPlus.Activate.Web.Helpers.AccountConfig.Settings = ac;

            AddAlert(AlertViewModel.AlertType.Success, "Account configuration saved successfully.");

            return View(viewModel);
        }

        #endregion
    }
}
