﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using MomentumPlus.Activate.Web.Attributes;

namespace MomentumPlus.Activate.Web.Areas.Admin.Models
{
    public class ImageViewModel
    {
        [Required]
        public string Title { get; set; }

        [Required]
        [FileExtensions(Extensions = "png,jpeg,jpg", ErrorMessage = "Allowed file extensions are .png, .jpeg and .jpg")]
        [Display(Name = "File")]
        [MaxFileSize(1)]
        public HttpPostedFileBase UploadedImage { get; set; }

        public string Src { get; set; }

        public Guid Id { get; set; }

        public bool Retired { get; set; }

    }
}