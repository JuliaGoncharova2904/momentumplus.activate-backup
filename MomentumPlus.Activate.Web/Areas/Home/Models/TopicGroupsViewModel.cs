﻿using System.Collections.Generic;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Areas.Home.Models
{
    public class TopicGroupsViewModel : BaseViewModel
    {
        public IEnumerable<TopicGroup> TopicGroups { get; set; }
        public TopicGroup TopicGroup { get; set; }
        public IEnumerable<TopicViewModel> Topics { get; set; }
        public List<TopicViewModel> TopicsTemplate { get; set; }
        public int PageSize { get; set; }
        public int PageNo { get; set; }
        public int TopicCount { get; set; }
        public bool SortAscending { get; set; }
        public List<int> ActiveTopicGroups { get; set; }
        public string Cookie { get; set; }
        public IDictionary<TopicGroup, PackageFlag> TopicGroupFlags { get; set; }
    }
}
