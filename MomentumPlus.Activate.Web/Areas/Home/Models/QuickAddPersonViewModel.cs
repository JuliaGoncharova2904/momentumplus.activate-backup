﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Activate.Web.Areas.Home.Models
{
    public class QuickAddPersonViewModel
    {
        public Guid? ContactId { get; set; }
        public List<SelectListItem> Contacts { get; set; }
        public Guid? RoleId { get; set; }

        [Required]
        [Display (Name=@"Employment Type *")]
        public string EmploymentStatus  { get; set; } 
    }
}