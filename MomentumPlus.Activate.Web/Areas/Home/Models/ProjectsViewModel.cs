﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MomentumPlus.Activate.Web.Models;
using System.Web.Mvc;
using MomentumPlus.Core.Models.Requests;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Areas.Home.Models
{
    public class ProjectsViewModel
    {
        public IEnumerable<ProjectInstanceViewModel> ProjectInstances { get; set; }
        public IEnumerable<NewProjectRequest> NewProjectRequests { get; set; }
        public IEnumerable<ProjectInstanceTransferRequest> UnapprovedProjectTransferRequests { get; set; }
        public IEnumerable<ProjectInstanceRoleChangeRequest> UnapprovedProjectInstanceRoleChangeRequests { get; set; }

        public int UnapprovedCount { get; set; }

        [Display(Name = "Order By")]
        public ProjectInstance.SortColumn OrderBy { get; set; }

        [Display(Name = "Sort Ascending")]
        public bool SortAscending { get; set; }

        [Display(Name = "Project Status")]
        public int? ProjectStatus { get; set; }
        public IEnumerable<SelectListItem> Statuses { get; set; }

        [Display(Name = "Projects Per Page")]
        public int ProjectsPerPage { get; set; }

        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }

        public bool RetiredProjects { get; set; }
    }
}