﻿using MomentumPlus.Activate.Web.Areas.Home.Controllers;
using MomentumPlus.Activate.Web.Models.Home;

namespace MomentumPlus.Activate.Web.Areas.Home.Models
{
    public class OtherTasksViewModel
    {
        public TasksCriticalViewModel TasksCritical { get; set; }
        public TasksRelationshipsViewModel TasksRelationships { get; set; }
        public TasksExperiencesViewModel TasksExperiences { get; set; }
        public TasksProjectsViewModel TasksProjects { get; set; }
        public bool SortAscending { get; set; }
        public TasksController.TaskType TaskType { get; set; }
    }
}