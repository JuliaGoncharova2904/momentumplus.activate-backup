﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MomentumPlus.Activate.Web.Areas.Home.Models
{
    public class QuickAddRelationshipViewModel
    {
        [Required(ErrorMessage = "Please select a Relationship.")]
        public Guid? ContactId { get; set; }
        public List<SelectListItem> Contacts { get; set; }
    }
}