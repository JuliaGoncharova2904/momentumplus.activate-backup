﻿using MomentumPlus.Core.Models;
using System.Collections.Generic;

namespace MomentumPlus.Activate.Web.Areas.Home.Models
{
    public class SearchViewModel
    {
        public string SearchQuery { get; set; }

        public bool IncludeTopics { get; set; }
        public bool IncludeTopicAttachments { get; set; }
        public bool IncludeTopicVoiceMessages { get; set; }
        public bool IncludeTasks { get; set; }
        public bool IncludeTaskAttachments { get; set; }
        public bool IncludeTaskVoiceMessages { get; set; }

        public IEnumerable<Topic> Topics { get; set; }
        public IEnumerable<Attachment> TopicAttachments { get; set; }
        public IEnumerable<VoiceMessage> TopicVoiceMessages { get; set; }
        public IEnumerable<Task> Tasks { get; set; }
        public IEnumerable<Attachment> TaskAttachments { get; set; }
        public IEnumerable<VoiceMessage> TaskVoiceMessages { get; set; }

        public bool NoFiltersApplied { get; set; }
    }
}