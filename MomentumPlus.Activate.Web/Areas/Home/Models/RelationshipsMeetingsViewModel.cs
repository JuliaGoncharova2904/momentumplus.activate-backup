﻿namespace MomentumPlus.Activate.Web.Areas.Home.Models
{
    public class RelationshipsMeetingsViewModel
    {
        public RelationshipsViewModel Meetings { get; set; }
        public bool SortAscending { get; set; }
    }
}