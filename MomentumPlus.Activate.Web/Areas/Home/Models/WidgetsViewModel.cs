﻿using MomentumPlus.Core.Models;
using System;

namespace MomentumPlus.Activate.Web.Areas.Home.Models
{
    public class WidgetsViewModel
    {
        public Guid PackageId { get; set; }

        public string Test  { get; set; }



        public Package.Widget? Widget1 { get; set; }
        public Package.Widget? Widget2 { get; set; }
        public Package.Widget? Widget3 { get; set; }
    }
}