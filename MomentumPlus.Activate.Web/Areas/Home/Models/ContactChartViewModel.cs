﻿using MomentumPlus.Core.Models;
using System;
using System.Linq;

namespace MomentumPlus.Activate.Web.Areas.Home.Models
{
    public class ContactChartViewModel
    {
        public Guid ID { get; set; }
        public Guid? CompanyId { get; set; }
        public string GroupName { get; set; }
        public Contact.Type RelationshipType { get; set; }
        public string Name { get; set; }
        public string Position { get; set; }
        public bool Managed { get; set; }
        public bool IntroRequired { get; set; }
        public Guid? IndirectRelationshipContactId { get; set; }
        public string IndirectRelationship { get; set; }
        public Contact.Importance RelationshipImportance { get; set; }
        public string ContactDetails { get; set; }
        public int? ChartX { get; set; }
        public int? ChartY { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Guid? ImageId { get; set; }
        public bool OtherCompany { get; set; }
        public string OtherCompanyName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Company { get; set; }

        public bool RootNode { get; set; }

        public string Initials
        {
            get
            {
                var initials = "";

                if (!string.IsNullOrEmpty(this.Name))
                {
                    foreach (var word in this.Name.Split().Where(s => !string.IsNullOrWhiteSpace(s)))
                    {
                        initials += word.First();
                    }
                }

                return initials;
            }
        }
    }
}