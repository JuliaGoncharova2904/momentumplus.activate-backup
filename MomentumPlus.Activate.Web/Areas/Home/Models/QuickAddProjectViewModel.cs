﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Attributes;

namespace MomentumPlus.Activate.Web.Areas.Home.Models
{
    public class QuickAddProjectViewModel
    {
        public Guid? ProjectInstanceId { get; set; }

        public bool SelectExisting { get; set; }

        public IEnumerable<SelectListItem> ExistingProjects { get; set; }

        public Guid? ExistingProjectId { get; set; }

        public bool Declined { get; set; }

        [Display(Name = "Project Name *")]
        [StringLength(30, MinimumLength=3)]
        public string ProjectName { get; set; }

        [Display(Name = "Project ID")]
        [ProjectId]
        public string ProjectId { get; set; }
    }
}