﻿using System.Collections.Generic;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Areas.Home.Models
{
    public class MeetingsViewModel : BaseViewModel
    {
        public IEnumerable<Meeting> Meetings;

        public bool SortAscending { get; set; }
    }
}