﻿using MomentumPlus.Activate.Web.Areas.Home.Controllers;
using MomentumPlus.Activate.Web.Models.Home;

namespace MomentumPlus.Activate.Web.Areas.Home.Models
{
    public class CoreTasksViewModel
    {
        public TasksCoreViewModel TasksCore { get; set; }
        public TasksCoreViewModel TasksAdditional { get; set; }
        public bool SortAscending { get; set; }
        public TasksController.TaskType? TaskType { get; set; }
    }
}