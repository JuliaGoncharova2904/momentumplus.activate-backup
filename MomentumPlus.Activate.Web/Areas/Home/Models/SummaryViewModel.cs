﻿using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Activate.Web.Models.Home;
using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MomentumPlus.Activate.Web.Areas.Home.Models
{
    public class SummaryViewModel : BaseViewModel
    {
        #region Standard User

        public ToDoViewModel ToDo { get; set; }
        public ProgressViewModel Progress { get; set; }

        public int StaffTransferRequestCount { get; set; }
        public int NewProjectInstanceCount { get; set; }
        public int ProjectInstanceTransferRequestCount { get; set; }
        public int ProjectInstanceRoleChangeRequestCount { get; set; }

        public Guid PackageId { get; set; }

        public Package Package { get; set; }

        public IEnumerable<SelectListItem> AllowedWidgetTypes { get; set; }

        public bool ShowApprovals { get; set; }


        #endregion
    }
}