﻿using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.People;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Activate.Web.Areas.Home.Models
{
    public class PeopleViewModel
    {
        [Display(Name = "Search by Name, Position or Company")]
        public string SearchTerm { get; set; }

        [Display(Name = "Order By")]
        public Person.SortColumn OrderBy { get; set; }

        [Display(Name = "Sort Ascending")]
        public bool SortAscending { get; set; }

        public bool Retired { get; set; }

        public IEnumerable<Contact> StaffContacts { get; set; }

        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }

        public Guid? DefaultCompanyId { get; set; }
    }
}