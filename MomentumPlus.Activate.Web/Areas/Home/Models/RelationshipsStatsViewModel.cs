﻿namespace MomentumPlus.Activate.Web.Areas.Home.Models
{
    public class RelationshipsStatsViewModel
    {
        public int PeopleInternal { get; set; }
        public int PeopleExternal { get; set; }
        public int Meetings { get; set; }

        public string DefaultCompany { get; set; }
        public string OtherCompanies { get; set; }
    }
}