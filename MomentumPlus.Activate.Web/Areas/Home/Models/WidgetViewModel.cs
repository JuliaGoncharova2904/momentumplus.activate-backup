﻿using System;
using System.Collections.Generic;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Areas.Home.Models
{
    public class WidgetViewModel
    {
        #region Shared

        public Package.Widget WidgetType { get; set; }
        public Guid PackageId { get; set; }
        public int WidgetPosition { get; set; }

        #endregion

        #region My Attachments

        public IEnumerable<BaseModel> Attachments { get; set; } 

        #endregion

        #region My People

        public IEnumerable<Contact> StaffContacts { get; set; }
        public Guid? DefaultCompanyId { get; set; }
        public bool ShowGoToPackage { get; set; }
        public string Index { get; set; }

        #endregion

        #region My Projects

        public IEnumerable<ProjectInstanceViewModel> ProjectInstances { get; set; } 

        #endregion
    }
}