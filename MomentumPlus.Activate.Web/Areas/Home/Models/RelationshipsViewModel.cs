﻿using System;
using System.Collections.Generic;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Areas.Home.Models
{
    public class RelationshipsViewModel : BaseViewModel
    {
        public IEnumerable<string> SelectedCompanies { get; set; }
        public IEnumerable<string> Companies { get; set; }
        public Guid? DefaultCompanyId { get; set; }

        public Guid PackageOwnerId { get; set; }
        public IDictionary<Guid, ContactChartViewModel> GraphNodes { get; set; }

        public int ActivePageNo { get; set; }
        public int ActiveCount { get; set; }
        public int RetiredPageNo { get; set; }
        public int RetiredCount { get; set; }
        public int ManagementCount { get; set; }
        public int ContactsPageSize { get; set; }
        public int ManagementPageNo { get; set; }
        public int IntroPageNo { get; set; }
        public int IntroPageSize { get; set; }
        public int IntroCount { get; set; }

        public IEnumerable<Meeting> Meetings { get; set; }
        public IEnumerable<Contact> ActiveContacts { get; set; }
        public IEnumerable<Contact> RetiredContacts { get; set; }
        public IEnumerable<Contact> Management { get; set; }
        public IDictionary<Contact, PackageFlag> Flags { get; set; }
        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int Count { get; set; }

        public bool ShowMeetings { get; set; }
        public bool IsLeaver { get; set; }
    }
}