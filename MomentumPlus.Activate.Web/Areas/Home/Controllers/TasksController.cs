﻿using System;
using System.Web.Mvc;
using MomentumPlus.Activate.Services;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Areas.Home.Models;
using MomentumPlus.Activate.Web.Controllers;
using MomentumPlus.Activate.Web.Core.Home;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models.Home;
using MomentumPlus.Activate.Web.Core;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Areas.Home.Controllers
{
    /// <summary>
    /// Controller for the Tasks module.
    /// </summary>
    public class TasksController : TaskController
    {
        public enum TaskType
        {
            Core, Additional, Critical, Relationships, Experiences, Project, None
        }

        #region CONSTANTS

        /// <summary>
        /// Number of tasks to display in the Core Tasks list.
        /// </summary>
        private const int CoreTasksPageSize = 8;

        /// <summary>
        /// Number of tasks to display in the Other Tasks list.
        /// </summary>
        private const int OtherTasksPageSize = 8;

        private IFlaggingService FlaggingService;

        #endregion

        #region IOC

        private TasksCore Tasks;

        /// <summary>
        /// Construct a new <c>TasksController</c> with the given service
        /// implementations.
        /// </summary>
        public TasksController(
            IAuditService auditService,
            IHandoverService handoverService,
            IMasterListService masterListService,
            IPackageService packageService,
            ITaskService taskService,
            ITopicService topicService,
            ITopicGroupService topicGroupService,
            ITopicQuestionService topicQuestionService,
            IUserService userService,
            IWorkplaceService workplaceService,
            IProLinkService proLink,
            IProjectService projectService,
            IContactService contactService,
            IFlaggingService flaggingService,
            TasksCore tasksCore,
            TaskCore taskCore,
            IPositionService positionService,
            TopicCore topicCore
        )
            : base(
                  auditService,
                  handoverService,
                  masterListService,
                  packageService,
                  taskService,
                  topicService,
                  topicGroupService,
                  topicQuestionService,
                  userService,
                  workplaceService,
                  proLink,
                  projectService,
                  contactService,
                  taskCore,
                  positionService,
                  topicCore
              )
        {
            FlaggingService = flaggingService;
            Tasks = tasksCore;
        }

        #endregion

        #region ACTIONS

        /// <summary>
        /// Redirect to the Core action.
        /// </summary>
        [HttpGet]
        public ActionResult Index()
        {
            return RedirectToAction("Core");
        }

        /// <summary>
        /// Render the Core Tasks view.
        /// </summary>
        [HttpGet]
        public ActionResult Core(
            TaskType taskType = TaskType.Critical,
            int pageNo = 1,
            TasksCriticalItemViewModel.SortColumn tasksCriticalSortColumn = TasksCriticalItemViewModel.SortColumn.NameTask,
            TasksExperiencesItemViewModel.SortColumn tasksExperiencesSortColumn = TasksExperiencesItemViewModel.SortColumn.NameTask,
            bool sortAscending = true
        )
        {
            Guid? sessionPackageId = GetCurrentPackageId();

            OtherTasksViewModel otherTasksViewModel = new OtherTasksViewModel();

            TasksCriticalViewModel tasksCriticalViewModel = Tasks.GetCritical(
            sessionPackageId.Value, true, pageNo, OtherTasksPageSize, tasksCriticalSortColumn, sortAscending);
            otherTasksViewModel.TasksCritical = tasksCriticalViewModel;

            TasksExperiencesViewModel tasksExperiencesViewModel = Tasks.GetExperience(
            sessionPackageId.Value, true, pageNo, OtherTasksPageSize, tasksExperiencesSortColumn, sortAscending);
            otherTasksViewModel.TasksExperiences = tasksExperiencesViewModel;

            otherTasksViewModel.SortAscending = sortAscending;
            otherTasksViewModel.TaskType = taskType;

            AppSession.Current.TaskFormReturnUrl = Request.RawUrl;

            return View("~/Areas/Home/Views/Tasks/Core.cshtml", otherTasksViewModel);
        }

        /// <summary>
        /// Render the Other Tasks view.
        /// </summary>
        [HttpGet]
        public ActionResult Other(
            TaskType taskType = TaskType.Critical,
            int criticalPageNo = 1,
            int relationshipPageNo = 1,
            int experiencePageNo = 1,
            int projectPageNo = 1,
            TasksCriticalItemViewModel.SortColumn tasksCriticalSortColumn = TasksCriticalItemViewModel.SortColumn.NameTask,
            TasksRelationshipsItemViewModel.SortColumn tasksRelationshipsSortColumn = TasksRelationshipsItemViewModel.SortColumn.NameTask,
            TasksExperiencesItemViewModel.SortColumn tasksExperiencesSortColumn = TasksExperiencesItemViewModel.SortColumn.NameTask,
            TasksProjectsItemViewModel.SortColumn tasksProjectsSortColumn = TasksProjectsItemViewModel.SortColumn.NameTask,
            bool sortAscending = true
        )
        {
            Guid? sessionPackageId = GetCurrentPackageId();

            OtherTasksViewModel otherTasksViewModel = new OtherTasksViewModel();

            TasksCriticalViewModel tasksCriticalViewModel = Tasks.GetCritical(
            sessionPackageId.Value, false, criticalPageNo, OtherTasksPageSize, tasksCriticalSortColumn, sortAscending);
            otherTasksViewModel.TasksCritical = tasksCriticalViewModel;

            TasksRelationshipsViewModel tasksRelationshipsViewModel = Tasks.GetRelationships(
            sessionPackageId.Value, TopicGroup.Category.Other, relationshipPageNo, OtherTasksPageSize, tasksRelationshipsSortColumn, sortAscending);
            otherTasksViewModel.TasksRelationships = tasksRelationshipsViewModel;

            TasksExperiencesViewModel tasksExperiencesViewModel = Tasks.GetExperience(
            sessionPackageId.Value, false, experiencePageNo, OtherTasksPageSize, tasksExperiencesSortColumn, sortAscending);
            otherTasksViewModel.TasksExperiences = tasksExperiencesViewModel;

            TasksProjectsViewModel tasksProjectsViewModel = Tasks.GetProjects(
            sessionPackageId.Value, false, projectPageNo, OtherTasksPageSize, tasksProjectsSortColumn, sortAscending);
            otherTasksViewModel.TasksProjects = tasksProjectsViewModel;
            
            otherTasksViewModel.SortAscending = sortAscending;
            otherTasksViewModel.TaskType = taskType;

            AppSession.Current.TaskFormReturnUrl = Request.RawUrl;
            AppSession.Current.MeetingFormReturnUrl = Request.RawUrl;

            return View("~/Areas/Home/Views/Tasks/Other.cshtml", otherTasksViewModel);
        }

        #endregion
    }
}
