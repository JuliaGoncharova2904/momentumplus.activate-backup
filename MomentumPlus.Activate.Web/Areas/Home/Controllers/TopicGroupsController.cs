﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MomentumPlus.Activate.Services;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Areas.Home.Models;
using MomentumPlus.Activate.Web.Controllers;
using MomentumPlus.Activate.Web.Core;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.Moderation;
using System;

namespace MomentumPlus.Activate.Web.Areas.Home.Controllers
{
    public abstract class TopicGroupsController : TopicController
    {
        #region IOC

        protected IModuleService ModuleService;
        protected IFlaggingService FlaggingService;

        /// <summary>
        /// Construct a new <c>TopicGroupsController</c> with the given service
        /// implementations.
        /// </summary>
        public TopicGroupsController(
            IAuditService auditService,
            IHandoverService handoverService,
            IMasterListService masterListService,
            IModuleService moduleService,
            IPackageService packageService,
            ISystemService systemService,
            ITaskService taskService,
            ITopicGroupService topicGroupService,
            ITopicService topicService,
            ITopicQuestionService topicQuestionService,
            IUserService userService,
            IWorkplaceService workplaceService,
            IProLinkService proLink,
            IFlaggingService flaggingService,
            IPositionService positionService,
            TopicCore topicCore
        ) : base(
                auditService,
                handoverService,
                masterListService,
                packageService,
                taskService,
                topicGroupService,
                topicService,
                topicQuestionService,
                userService,
                workplaceService,
                proLink,
                positionService,
                topicCore
            )
        {
            ModuleService = moduleService;
            FlaggingService = flaggingService;
        }

        #endregion

        #region HELPERS

        /// <summary>
        /// Get the topic groups index page for a module.
        /// </summary>
        /// <param name="moduleType">The module type.</param>
        /// <param name="cookie">The name of the cookie used to remember display preferences.</param>
        public ActionResult GetTopicGroupsView(Module.ModuleType moduleType, string cookie)
        {
            var packageId = GetCurrentPackageId();
            if (!packageId.HasValue)
                return RedirectToAction("Index");

            var module = ModuleService.GetByModuleType(moduleType, packageId.Value);
            var viewModel = new TopicGroupsViewModel { Cookie = cookie };

            if (module != null)
            {
                viewModel.TopicGroups = GetTopicGroups(packageId.Value, moduleType, true); 

                AuditService.RecordActivity<Module>(AuditLog.EventType.Viewed, module.ID);
            }
            if (_HandoverService.IsLeaverPeriodLocked(packageId.Value))
            {
                viewModel.TopicGroupFlags = new Dictionary<TopicGroup, PackageFlag>();
                ViewBag.TopicFlag = new Dictionary<Topic, PackageFlag>();
                if (viewModel.TopicGroups != null)
                    foreach (var topicGroup in viewModel.TopicGroups)
                    {
                        viewModel.TopicGroupFlags[topicGroup] = FlaggingService.GetTopicGroupFlag(topicGroup);
                        foreach (var topic in topicGroup.Topics)
                        {
                            ViewBag.TopicFlag[topic] = FlaggingService.GetTopicFlag(topic);
                        }
                    }
            }
            // Determine which topic groups are active based on cookie
            var activeGroupsCookie = Request.Cookies[cookie];
            viewModel.ActiveTopicGroups = new List<int>();

            if (activeGroupsCookie != null)
            {
                if (!string.IsNullOrWhiteSpace(activeGroupsCookie.Value))
                {
                    viewModel.ActiveTopicGroups.AddRange(activeGroupsCookie.Value.Split(',').Select(x => int.Parse(x)));
                }
            }
            else
            {
                viewModel.ActiveTopicGroups.Add(0);
                Response.SetCookie(new HttpCookie(cookie, string.Join(",", viewModel.ActiveTopicGroups)));
            }

            AppSession.Current.TopicFormReturnUrl = Request.RawUrl;
            AppSession.Current.AttachmentManagerReturnUrl = Request.RawUrl;
            AppSession.Current.VoiceMessageManagerReturnUrl = Request.RawUrl;
            AppSession.Current.TaskManagerReturnUrl = Request.RawUrl;

            return View("~/Areas/Home/Views/Shared/TopicGroups.cshtml", viewModel);
        }


        private IList<TopicGroup> GetTopicGroups(Guid packageId, Module.ModuleType moduleType, bool showAll = false)
        {
            var topicGroups = TopicGroupService.GetByPackageId(packageId)
                .Where(tg => tg.ParentModule.Type == moduleType).ToList();

            if (!showAll)
            {
                foreach (var topicGroup in topicGroups)
                {
                    topicGroup.Topics = topicGroup.Topics.Where(Moderated.Approved<Topic>().Compile()).ToList();
                }
            }
            
            return topicGroups;
        }

        #endregion
    }
}
