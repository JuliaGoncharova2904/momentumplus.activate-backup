﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MomentumPlus.Activate.Services;
using MomentumPlus.Activate.Services.Helpers;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Areas.Home.Models;
using MomentumPlus.Activate.Web.Controllers;
using MomentumPlus.Activate.Web.Core;
using MomentumPlus.Activate.Web.Extensions;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models.Home;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.Requests;

namespace MomentumPlus.Activate.Web.Areas.Home.Controllers
{
    public class SummaryController : TopicController
    {
        #region CONSTANTS

        /// <summary>
        ///     Number of topics to display in the To Do list.
        /// </summary>
        private const int ToDoPageSize = 8;

        #endregion

        #region ACTIONS

        /// <summary>
        ///     Render the summary page for the logged in user.
        /// </summary>

        public ActionResult BackToSummary(
            int toDoPageNo = 1
            )
        {
            var user = GetCurrentUser();
            var packageObj = GetCurrentPackage();
            if (user.ID == packageObj.UserOwnerId)
            {
                var package= GetPackage(user.ID);
                return RedirectToAction(
                "Index",
                "Summary",
                new { area = "Home" });
                //return StandardUserSummary(package.ID, toDoPageNo);
            } else {
                return RedirectToAction(
                "Index",
                "Summary",
                new { area = "Home" });
                //return StandardUserSummary(packageObj.ID, toDoPageNo);
            } 
            
        }

        public ActionResult Index(
            int toDoPageNo = 1,
            int triggersPageNo = 1,
            int agedPageNo = 1
            )
        {
            AppSession.Current.TaskFormReturnUrl = Request.RawUrl;
            AppSession.Current.TopicFormReturnUrl = Request.RawUrl;
            AppSession.Current.AttachmentManagerReturnUrl = Request.RawUrl;
            AppSession.Current.VoiceMessageManagerReturnUrl = Request.RawUrl;
            AppSession.Current.StaffFormReturnUrl = Request.RawUrl;
            AppSession.Current.ContactFormReturnUrl = Request.RawUrl;
            AppSession.Current.ProjectInstanceFormReturnUrl = Request.RawUrl;
            AppSession.Current.MeetingFormReturnUrl = Request.RawUrl;

            var packageId = GetCurrentPackageId();


            var user = GetCurrentUser();

            if (user.Role.RoleName.Equals(RoleAuthHelper.iHandoverAdmin))
            {
                RedirectToAction("Index", "MyTeam");
            }

            if (!packageId.HasValue)
            {
                return RedirectToAction("NoPackages", "Account", new { area = "" });
            }

            if (packageId != null)
            {
                if (!HandoverService.IsOnboarding(packageId.Value) &&
                    !HandoverService.IsLeaverPeriodLocked(packageId.Value))
                {
                    TopicsToDoPrompt(packageId.Value);
                    TasksToDoPrompt(packageId.Value);
                }
            }

            if (GetCurrentUser() != null &&
                (GetCurrentUser().ID == PackageService.GetByPackageId(packageId.Value).UserOwnerId))
            {
                ViewBag.PackageOwner = true;
            }

            // Check if in package examine mode and render standard view for the package
            if (AppSession.Current.ExaminePackageId.HasValue)
            {
                return StandardUserSummary(AppSession.Current.ExaminePackageId.Value, toDoPageNo);
            }

            AuditService.RecordActivity<Package>(AuditLog.EventType.Viewed, packageId.Value);

            AppSession.Current.MeetingFormReturnUrl = Request.RawUrl;
            return StandardUserSummary(packageId.Value, toDoPageNo);
        }

        #endregion

        #region IOC

        private readonly IModerationService ModerationService;
        private readonly IHandoverService HandoverService;
        private readonly IAccountConfigService AccountConfigService;

        /// <summary>
        ///     Construct a new <c>SummaryController</c> with
        ///     the given service implementations.
        /// </summary>
        public SummaryController(
            IAuditService auditService,
            IHandoverService handoverService,
            IPackageService packageService,
            IMasterListService masterListService,
            IModerationService moderationService,
            ITaskService taskService,
            ITopicGroupService topicGroupService,
            ITopicService topicService,
            ITopicQuestionService topicQuestionService,
            IUserService userService,
            IWorkplaceService workplaceService,
            IProLinkService proLinkService,
            IAccountConfigService accountConfigService,
            IPositionService positionService,
            TopicCore topicCore
            )
            : base(
                auditService,
                handoverService,
                masterListService,
                packageService,
                taskService,
                topicGroupService,
                topicService,
                topicQuestionService,
                userService,
                workplaceService,
                proLinkService,
                positionService,
                topicCore
                )
        {
            ModerationService = moderationService;
            HandoverService = handoverService;
            AccountConfigService = accountConfigService;
        }

        #endregion

        #region HELPERS

        /// <summary>
        ///     Render the standard user summary view.
        /// </summary>
        /// <param name="packageId">The user's package ID.</param>
        /// <param name="pageNo">The page number.</param>
        private ActionResult StandardUserSummary(Guid packageId, int pageNo)
        {
            var itemsTodo = PackageService.GetToDoItems(packageId, pageNo, ToDoPageSize);
            var itemsTodoCount = PackageService.GetToDoItemsCount(packageId);
                        
            var toDo = new ToDoViewModel
            {
                Items = itemsTodo,
                TotalCount = itemsTodoCount,
                PageNo = pageNo,
                PageSize = ToDoPageSize
            };

            var currentUser = GetCurrentUser();
            Guid? currentUserId = currentUser.ID;

            var userRole = currentUser.Role.RoleParsed;

            if (userRole == Role.RoleEnum.iHandoverAdmin ||
                userRole == Role.RoleEnum.iHandoverAgent ||
                userRole == Role.RoleEnum.Administrator)
            {
                currentUserId = null;
            }

            var newProjectInstanceCount =
                ModerationService.GetAllNotModerated<NewProjectRequest>(currentUserId, packageId).Count();
            var projectInstanceTransferRequestCount =
                ModerationService.GetAllNotModerated<ProjectInstanceTransferRequest>(currentUserId, packageId).Count();
            var projectInstanceRoleChangeRequestCount =
                ModerationService.GetAllNotModerated<ProjectInstanceRoleChangeRequest>(currentUserId, packageId).Count();
            var staffTransferRequestCount =
                ModerationService.GetAllNotModerated<StaffTransferRequest>(currentUserId, packageId).Count();

            var package = PackageService.GetByPackageId(packageId);
            ProgressViewModel progress = null;

            if (package.Handover != null && package.Handover.LeavingPeriod.Locked.HasValue)
            {
                progress = new ProgressViewModel
                {
                    Approved = (int)(HandoverService.GetApprovedItemsPercentage(packageId) * 100),
                    Ready = (int)(HandoverService.GetReadyItemsPrecentage(packageId) * 100)
                };
            }

            var allowedWidgetTypes = new List<SelectListItem>
            {
                new SelectListItem
                {
                    Text = Package.Widget.MyAttachments.GetDescription(),
                    Value = Package.Widget.MyAttachments.ToString()
                }
            };

            if (package.PeopleManagementEnabled)
            {
                allowedWidgetTypes.Add(new SelectListItem
                {
                    Text = Package.Widget.MyPeople.GetDescription(),
                    Value = Package.Widget.MyPeople.ToString()
                });
            }

            if (package.ProjectsEnabled)
            {
                allowedWidgetTypes.Add(new SelectListItem
                {
                    Text = Package.Widget.MyProjects.GetDescription(),
                    Value = Package.Widget.MyProjects.ToString()
                });
            }

            var showApprovals = currentUser.Role.RoleParsed != Role.RoleEnum.User &&
                                (staffTransferRequestCount > 0 ||
                                 newProjectInstanceCount > 0 ||
                                 projectInstanceTransferRequestCount > 0 ||
                                 projectInstanceRoleChangeRequestCount > 0);

            if (package.ExitSurveyTemplateId.HasValue && package.ExitSurvey.HasValue)
            {

                ExitSurveyService exitSurveyService = new ExitSurveyService(context: new MomentumPlus.Core.DataSource.MomentumContext());
                ExitSurveyTemplate exitSurveyTemplate = exitSurveyService.GetExitSurveyTemplateById(GetCurrentPackage().ExitSurveyTemplateId.Value);
                AppSession.Current.ExitSurveyId = exitSurveyTemplate.ExitSurveyId;
                AppSession.Current.ExitSurveyEnabled = true;

            }
            else
            {
                AppSession.Current.ExitSurveyEnabled = false;
            }

            //MvcSiteMapProvider.SiteMaps.Current.CurrentNode.Attributes.Add("SummaryViewModel", new SummaryViewModel
            //{
            //    ToDo = toDo,
            //    Progress = progress,
            //    NewProjectInstanceCount = newProjectInstanceCount,
            //    ProjectInstanceTransferRequestCount = projectInstanceTransferRequestCount,
            //    ProjectInstanceRoleChangeRequestCount = projectInstanceRoleChangeRequestCount,
            //    StaffTransferRequestCount = staffTransferRequestCount,
            //    PackageId = packageId,
            //    Package = package,
            //    AllowedWidgetTypes = allowedWidgetTypes,
            //    ShowApprovals = showApprovals
            //});



            return View("StandardUser", new SummaryViewModel
            {
                ToDo = toDo,
                Progress = progress,
                NewProjectInstanceCount = newProjectInstanceCount,
                ProjectInstanceTransferRequestCount = projectInstanceTransferRequestCount,
                ProjectInstanceRoleChangeRequestCount = projectInstanceRoleChangeRequestCount,
                StaffTransferRequestCount = staffTransferRequestCount,
                PackageId = packageId,
                Package = package,
                AllowedWidgetTypes = allowedWidgetTypes,
                ShowApprovals = showApprovals
            });
        }

        public void TasksToDoPrompt(Guid packageId)
        {
            var settings = AccountConfigService.Get();
            var tasks = TaskService.GetAllByPackageId(packageId);
            List<Task> mediumTasks = new List<Task>();
            List<Task> highTasks = new List<Task>();

            foreach (var task in tasks)
            {
                var days = settings.TaskAlertDaysHigh;
                if (task.UserLastViewed.HasValue)
                {
                    days = (int)(SystemTime.Now - task.UserLastViewed.Value).TotalDays;
                }
                if (days >= settings.TaskAlertDaysHigh)
                {
                    highTasks.Add(task);
                }
                if (days >= settings.TaskAlertDaysMedium)
                {
                    mediumTasks.Add(task);
                }
            }

            var toDoList = PackageService.GetAllToDoItems(packageId).ToList();
            List<ToDoItem> dismissed = new List<ToDoItem>(); // Previously dismissed
            List<ToDoItem> liveToDos = new List<ToDoItem>(); // Currently active

            foreach (var highTask in highTasks)
            {
                var list = toDoList.Where(t => GeneralHelpers.GetUrlId(t.Url) == highTask.ID.ToString()).ToList();

                foreach (var item in list)
                {
                    if (item.Notes == "Medium" &&
                        (highTask.UserLastViewed > item.DismissDate || item.DismissDate == null))
                    {
                        item.DismissDate = SystemTime.Now;
                        PackageService.UpdateToDo(item);
                    }
                    else
                    {
                        if (item.DismissDate == null)
                        {
                            liveToDos.Add(item);
                        }
                        if (item.DismissDate.HasValue && !highTask.UserLastViewed.HasValue ||
                            item.DismissDate > highTask.UserLastViewed ||
                            (item.UserLastViewed.HasValue &&
                             (SystemTime.Now - item.UserLastViewed.Value).TotalDays <= settings.TaskAlertDaysMedium))
                        {
                            dismissed.Add(item);
                        }
                    }
                }

                // If a high exists since the UserLastViewed and has been dismissed, don't do anything
                if (!dismissed.Any() && !liveToDos.Any() &&
                    (highTask.Created.HasValue &&
                     (SystemTime.Now - highTask.Created.Value).TotalDays > settings.TaskAlertDaysHigh))
                {
                    PackageService.AddToDo(new ToDoItem
                    {
                        Title =
                            string.Format("Your '{0}' task is over {1} days out of date. Click here to view it.",
                                highTask.Name, settings.TaskAlertDaysHigh),
                        Url = string.Format("{0}{1}", "/Task/EditTask/", highTask.ID),
                        VisibleDate = SystemTime.Now,
                        PackageId = packageId,
                        Notes = "High"
                    });
                }

                liveToDos.Clear();
                dismissed.Clear();
            }

            // Redo the list to take account of added items
            toDoList = PackageService.GetAllToDoItems(packageId).ToList();

            foreach (var mediumTask in mediumTasks)
            {
                var list = toDoList.Where(t => GeneralHelpers.GetUrlId(t.Url) == mediumTask.ID.ToString()).ToList();

                foreach (var item in list)
                {
                    if (item.DismissDate == null || item.DismissDate > mediumTask.UserLastViewed)
                    {
                        liveToDos.Add(item);
                    }
                    if (item.DismissDate.HasValue && !mediumTask.UserLastViewed.HasValue)
                    {
                        dismissed.Add(item);
                    }
                }

                if (!dismissed.Any() && !liveToDos.Any() &&
                    (mediumTask.Created.HasValue &&
                     (SystemTime.Now - mediumTask.Created.Value).TotalDays > settings.TaskAlertDaysMedium))
                {
                    PackageService.AddToDo(new ToDoItem
                    {
                        Title =
                            string.Format("Your '{0}' task is over {1} days out of date. Click here to view it.",
                                mediumTask.Name, settings.TaskAlertDaysMedium),
                        Url = string.Format("{0}{1}", "/Task/EditTask/", mediumTask.ID),
                        VisibleDate = SystemTime.Now,
                        PackageId = packageId,
                        Notes = "Medium"
                    });
                }

                liveToDos.Clear();
                dismissed.Clear();
            }
        }

        public void TopicsToDoPrompt(Guid packageId)
        {
            var settings = AccountConfigService.Get();
            var topics = TopicService.GetByPackageId(packageId);
            List<Topic> mediumTopics = new List<Topic>();
            List<Topic> highTopics = new List<Topic>();

            foreach (var topic in topics)
            {
                var days = settings.TopicAlertDaysHigh;
                if (topic.UserLastViewed.HasValue)
                {
                    days = (int)(SystemTime.Now - topic.UserLastViewed.Value).TotalDays;
                }
                if (days >= settings.TopicAlertDaysHigh)
                {
                    highTopics.Add(topic);
                }
                if (days >= settings.TopicAlertDaysMedium)
                {
                    mediumTopics.Add(topic);
                }
            }

            var toDoList = PackageService.GetAllToDoItems(packageId).ToList();
            List<ToDoItem> dismissed = new List<ToDoItem>(); // Previously dismissed
            List<ToDoItem> liveToDos = new List<ToDoItem>(); // Currently active

            foreach (var highTopic in highTopics)
            {
                var list = toDoList.Where(t => GeneralHelpers.GetUrlId(t.Url) == highTopic.ID.ToString()).ToList();

                foreach (var item in list)
                {
                    if (item.Notes == "Medium" &&
                        (highTopic.UserLastViewed > item.DismissDate || item.DismissDate == null))
                    {
                        item.DismissDate = SystemTime.Now;
                        PackageService.UpdateToDo(item);
                    }
                    else
                    {
                        if (item.DismissDate == null)
                        {
                            liveToDos.Add(item);
                        }
                        if (item.DismissDate.HasValue && !highTopic.UserLastViewed.HasValue ||
                            item.DismissDate > highTopic.UserLastViewed ||
                            (item.UserLastViewed.HasValue &&
                             (SystemTime.Now - item.UserLastViewed.Value).TotalDays <= settings.TopicAlertDaysMedium))
                        {
                            dismissed.Add(item);
                        }
                    }
                }

                // If a high exists since the UserLastViewed and has been dismissed, don't do anything
                if (!dismissed.Any() && !liveToDos.Any() &&
                    (highTopic.Created.HasValue &&
                     (SystemTime.Now - highTopic.Created.Value).TotalDays > settings.TopicAlertDaysHigh))
                {
                    PackageService.AddToDo(new ToDoItem
                    {
                        Title =
                            string.Format("Your '{0}' topic is over {1} days out of date. Click here to view it.",
                                highTopic.Name, settings.TopicAlertDaysHigh),
                        Url = string.Format("{0}{1}", "/Home/Critical/EditTopic/", highTopic.ID),
                        VisibleDate = SystemTime.Now,
                        PackageId = packageId,
                        Notes = "High"
                    });
                }

                liveToDos.Clear();
                dismissed.Clear();
            }

            // Redo the list to take account of added items
            toDoList = PackageService.GetAllToDoItems(packageId).ToList();

            foreach (var mediumTopic in mediumTopics)
            {
                var list = toDoList.Where(t => GeneralHelpers.GetUrlId(t.Url) == mediumTopic.ID.ToString()).ToList();

                foreach (var item in list)
                {
                    if (item.DismissDate == null || item.DismissDate > mediumTopic.UserLastViewed)
                    {
                        liveToDos.Add(item);
                    }
                    if (item.DismissDate.HasValue && !mediumTopic.UserLastViewed.HasValue)
                    {
                        dismissed.Add(item);
                    }
                }

                if (!dismissed.Any() && !liveToDos.Any() &&
                    (mediumTopic.Created.HasValue &&
                     (SystemTime.Now - mediumTopic.Created.Value).TotalDays > settings.TopicAlertDaysMedium))
                {
                    PackageService.AddToDo(new ToDoItem
                    {
                        Title =
                            string.Format("Your '{0}' topic is over {1} days out of date. Click here to view it.",
                                mediumTopic.Name, settings.TopicAlertDaysMedium),
                        Url = string.Format("{0}{1}", "/Home/Critical/EditTopic/", mediumTopic.ID),
                        VisibleDate = SystemTime.Now,
                        PackageId = packageId,
                        Notes = "Medium"
                    });
                }

                liveToDos.Clear();
                dismissed.Clear();
            }
        }

        #endregion
    }
}