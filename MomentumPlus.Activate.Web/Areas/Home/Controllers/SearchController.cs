﻿using System.Web.Mvc;
using MomentumPlus.Activate.Services;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Areas.Home.Models;
using MomentumPlus.Activate.Web.Controllers;
using MomentumPlus.Activate.Web.Core;
using MomentumPlus.Activate.Web.Helpers;

namespace MomentumPlus.Activate.Web.Areas.Home.Controllers
{
    public class SearchController : TaskController
    {
        /// <summary>
        /// Construct a new <c>SearchController</c> instance with
        /// the given service implementations.
        /// </summary>
        public SearchController(
            IAuditService auditService,
            IHandoverService handoverService,
            IMasterListService masterListService,
            IPackageService packageService,
            ITaskService taskService,
            ITopicService topicService,
            ITopicGroupService topicGroupService,
            ITopicQuestionService topicQuestionService,
            IUserService userService,
            IWorkplaceService workplaceService,
            IProLinkService proLink,
            IProjectService projectService,
            IContactService contactService,
            TaskCore taskCore,
IPositionService positionService,
            TopicCore topicCore
        )
            : base(
                  auditService,
                  handoverService,
                  masterListService,
                  packageService,
                  taskService,
                  topicService,
                  topicGroupService,
                  topicQuestionService,
                  userService,
                  workplaceService,
                  proLink,
                  projectService,
                  contactService,
                  taskCore,
positionService,
                  topicCore
              )
        {
        }

        /// <summary>
        /// Render search results page.
        /// </summary>
        [HttpGet]
        public ActionResult Index(SearchViewModel model)
        {
            var packageId = GetCurrentPackageId();

            if (packageId.HasValue && !string.IsNullOrEmpty(model.SearchQuery))
            {
                // Search on everything when there are no filters in use
                model.NoFiltersApplied = !(model.IncludeTopics || model.IncludeTopicAttachments || model.IncludeTopicVoiceMessages ||
                                           model.IncludeTasks || model.IncludeTaskAttachments || model.IncludeTaskVoiceMessages);

                // Search topics
                if (model.IncludeTopics || model.NoFiltersApplied)
                {
                    model.Topics = TopicService.Search(model.SearchQuery, packageId.Value);
                }

                // Search topic attachments
                if (model.IncludeTopicAttachments || model.NoFiltersApplied)
                {
                    model.TopicAttachments = TopicService.SearchAttachments(model.SearchQuery, packageId.Value);
                }

                // Search topic voice messages
                if (model.IncludeTopicVoiceMessages || model.NoFiltersApplied)
                {
                    model.TopicVoiceMessages = TopicService.SearchVoiceMessages(model.SearchQuery, packageId.Value);
                }

                // Search tasks
                if (model.IncludeTasks || model.NoFiltersApplied)
                {
                    model.Tasks = TaskService.Search(model.SearchQuery, packageId.Value);
                }

                // Search task attachments
                if (model.IncludeTaskAttachments || model.NoFiltersApplied)
                {
                    model.TaskAttachments = TaskService.SearchAttachments(model.SearchQuery, packageId.Value);
                }

                // Search task voice messages
                if (model.IncludeTaskVoiceMessages || model.NoFiltersApplied)
                {
                    model.TaskVoiceMessages = TaskService.SearchVoiceMessages(model.SearchQuery, packageId.Value);
                }
            }

            AppSession.Current.AttachmentManagerReturnUrl = Request.RawUrl;
            AppSession.Current.TaskFormReturnUrl = Request.RawUrl;
            AppSession.Current.TaskManagerReturnUrl = Request.RawUrl;
            AppSession.Current.TopicFormReturnUrl = Request.RawUrl;

            return View(model);
        }

        /// <summary>
        /// Render search control component within a view.
        /// </summary>
        [ChildActionOnly]
        public ActionResult SearchControl()
        {
            var model = new SearchViewModel { SearchQuery = Request.QueryString["SearchQuery"] };
            return View(model);
        }
    }
}
