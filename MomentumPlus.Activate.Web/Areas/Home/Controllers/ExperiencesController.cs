﻿using System.Web.Mvc;
using MomentumPlus.Activate.Services;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Core;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Areas.Home.Controllers
{
    /// <summary>
    /// Controller for the Experiences module.
    /// </summary>
    public class ExperiencesController : TopicGroupsController
    {
        #region IOC

        /// <summary>
        /// Construct a new <c>ExperiencesController</c> with the given service
        /// implementations.
        /// </summary>
        public ExperiencesController(
            IAuditService auditService,
            IHandoverService handoverService,
            IMasterListService masterListService,
            IModuleService moduleService,
            IPackageService packageService,
            ISystemService systemService,
            ITaskService taskService,
            ITopicGroupService topicGroupService,
            ITopicService topicService,
            ITopicQuestionService topicQuestionService,
            IUserService userService,
            IWorkplaceService workplaceService,
            IProLinkService proLink,
            IFlaggingService flaggingService,
            IPositionService positionService,
            TopicCore topicCore
        )
            : base(
                auditService,
                handoverService,
                masterListService,
                moduleService,
                packageService,
                systemService,
                taskService,
                topicGroupService,
                topicService,
                topicQuestionService,
                userService,
                workplaceService,
                proLink,
                flaggingService,
                positionService,
                topicCore
            )
        {
        }

        #endregion

        #region ACTIONS

        /// <summary>
        /// Render the Experiences module landing page.
        /// </summary>
        [HttpGet]
        public ActionResult Index()
        {
            return GetTopicGroupsView(Module.ModuleType.Experiences, "0D75106F-CBB0-489A-98B9-B1694AB56205");
        }

        #endregion
    }
}
