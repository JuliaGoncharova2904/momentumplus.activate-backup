﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using AutoMapper;
using MomentumPlus.Activate.Services.Helpers;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Areas.Home.Models;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Controllers;
using MomentumPlus.Activate.Web.Core.Home;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Areas.Home.Controllers
{
    public class MeetingsController : BaseController
    {
        #region IOC

        private readonly MeetingsCore MeetingsCore;

        private readonly IContactService ContactService;
        private readonly IMeetingService MeetingService;
        private readonly IPackageService PackageService;
        private readonly IHandoverService HandoverService;

        public MeetingsController(IMeetingService meetingService, IPackageService packageService, IHandoverService handoverService, IMasterListService masterListService,
            IContactService contactService, IAuditService auditService, MeetingsCore meetingsCore, IUserService userService) : base(auditService, masterListService, packageService, userService)
        {
            MeetingService =
                meetingService;

            PackageService =
                packageService;

            HandoverService =
                handoverService;

            ContactService =
                contactService;

            AuditService =
                auditService;

            MeetingsCore = meetingsCore;
        }

        #endregion

        #region ACTIONS

        // GET: Home/Meetings
        public ActionResult Index()
        {
            return RedirectToAction("ViewMeetings");
        }

        /// <summary>
        /// Render meetings index view
        /// </summary>
        /// <param name="sortColumn">Column to sort meetings by</param>
        /// <param name="sortAscending">Whether to sort ascending or descending</param>
        /// <returns></returns>
        public ActionResult ViewMeetings(Meeting.SortColumn sortColumn = Meeting.SortColumn.Start,
            bool sortAscending = true)
        {
            var meetingsViewModel =
                new MeetingsViewModel();

            var packageId =
                GetCurrentPackageId();

            if (packageId.HasValue)
            {
                meetingsViewModel.Meetings
                    = MeetingService.GetByPackageId(packageId.Value, 1, 1000, sortColumn, sortAscending, createdAfter: SystemTime.Now);

                meetingsViewModel.SortAscending = sortAscending;

            }

            return View("ViewMeetings", meetingsViewModel);
        }

        /// <summary>
        /// Handle create meeting form submission
        /// </summary>
        /// <param name="meetingViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateMeeting(MeetingViewModel meetingViewModel)
        {
            if (!ModelState.IsValid)
            {
                PopulateMeetingViewModel(meetingViewModel);
                return View("MeetingForm", meetingViewModel);
            }

            try
            {
                var meeting = MeetingsCore.CreateMeeting(meetingViewModel);

                var returnUrl = Url.Action("EditMeeting", new { id = meeting.ID });
                AppSession.Current.VoiceMessageManagerReturnUrl = returnUrl;
                AppSession.Current.AttachmentManagerReturnUrl = returnUrl;

                return GetMeetingSuccessResult(meeting.ID, "Meeting added successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error adding meeting.");
            }

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Render form for creating project intro meeting
        /// </summary>
        /// <param name="id">The ID of the project instance to create the meeting for</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult CreateProjectIntroMeeting(Guid id)
        {
            var meetingViewModel = new MeetingViewModel { PackageId = GetCurrentPackageId().Value };
            PopulateMeetingViewModel(meetingViewModel);
            meetingViewModel.MeetingTypeId = new Guid("60F4668E-2167-4D70-B917-6A8853311B31"); // Slap me for this
            meetingViewModel.Start = null;
            meetingViewModel.ProjectInstanceId = id;

            meetingViewModel.TargetWeeksList = GetTargetWeekListItems(null);

            return View("ProjectIntroForm", meetingViewModel);
        }

        /// <summary>
        /// Handle submission of project intro meeting form
        /// </summary>
        /// <param name="meetingViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateProjectIntroMeeting(MeetingViewModel meetingViewModel)
        {
            if (!ModelState.IsValid || meetingViewModel.Start.HasValue && !IsMeetingDateValid(meetingViewModel.Start.Value, PackageService.GetByPackageId(meetingViewModel.PackageId)))
            {
                if (meetingViewModel.Start.HasValue && !IsMeetingDateValid(meetingViewModel.Start.Value,
                    PackageService.GetByPackageId(meetingViewModel.PackageId)))
                {
                    ModelState.AddModelError("Start Date",
                        "The meeting start date must be within the employee's employment period");
                }

                PopulateMeetingViewModel(meetingViewModel);

                meetingViewModel.TargetWeeksList = GetTargetWeekListItems(meetingViewModel.TargetWeek);

                return View("ProjectIntroForm", meetingViewModel);
            }

            try
            {
                var meeting = MeetingsCore.CreateMeeting(meetingViewModel);

                var returnUrl = Url.Action("EditProjectIntroMeeting", new { id = meeting.ID });
                AppSession.Current.VoiceMessageManagerReturnUrl = returnUrl;
                AppSession.Current.AttachmentManagerReturnUrl = returnUrl;

                return GetMeetingSuccessResult(meeting.ID, "Meeting added successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error adding meeting.");
            }

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Render edit project intro meeting form
        /// </summary>
        /// <param name="id">The meeting GUID</param>
        /// <returns></returns>
        [Authorise("Meeting")]
        [HttpGet]
        public ActionResult EditProjectIntroMeeting(Guid id)
        {
            var meeting = MeetingService.GetByMeetingId(id);

            var meetingViewModel = Mapper.Map<MeetingViewModel>(meeting);
            meetingViewModel.PackageId = PackageService.GetByMeetingId(id).ID;
            PopulateMeetingViewModel(meetingViewModel);

            meetingViewModel.TargetWeeksList = GetTargetWeekListItems(meeting.TargetWeek);

            AppSession.Current.VoiceMessageManagerReturnUrl = Request.RawUrl;
            AppSession.Current.AttachmentManagerReturnUrl = Request.RawUrl;

            AuditService.RecordActivity<Meeting>(AuditLog.EventType.Viewed, meeting.ID);

            return View("ProjectIntroForm", meetingViewModel);
        }

        /// <summary>
        /// Handle submission of edit project intro meeting form
        /// </summary>
        /// <param name="meetingViewModel"></param>
        /// <returns></returns>
        [Authorise("Meeting")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditProjectIntroMeeting(MeetingViewModel meetingViewModel)
        {
            if (!ModelState.IsValid)
            {
                PopulateMeetingViewModel(meetingViewModel);

                meetingViewModel.TargetWeeksList = GetTargetWeekListItems(meetingViewModel.TargetWeek);

                return View("ProjectIntroForm", meetingViewModel);
            }

            try
            {
                var meeting = MeetingsCore.UpdateMeeting(meetingViewModel);
                return GetMeetingSuccessResult(meeting.ID, "Meeting updated successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error updating meeting.");
            }

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Retire a meeting with the given ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorise("Meeting")]
        public ActionResult Retire(Guid id)
        {
            var meeting = MeetingService.GetByMeetingId(id);
            if (meeting == null || meeting.Deleted.HasValue || meeting.MeetingTypeId != new Guid("60F4668E-2167-4D70-B917-6A8853311B31")) // slap me
            {
                return HttpNotFound();
            }

            meeting.Retire();
            MeetingService.Update(meeting);

            AddAlert(AlertViewModel.AlertType.Success, "Meeting retired successfully.");
            return Redirect(AppSession.Current.MeetingFormReturnUrl);
        }

        /// <summary>
        /// Resume a meeting with the given ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorise("Meeting")]
        public ActionResult Resume(Guid id)
        {
            var meeting = MeetingService.GetByMeetingId(id);
            if (meeting == null || !meeting.Deleted.HasValue || meeting.MeetingTypeId != new Guid("60F4668E-2167-4D70-B917-6A8853311B31")) // slap me
            {
                return HttpNotFound();
            }

            meeting.Restore();
            MeetingService.Update(meeting);

            AddAlert(AlertViewModel.AlertType.Success, "Meeting resumed successfully.");
            return Redirect(AppSession.Current.MeetingFormReturnUrl);
        }

        /// <summary>
        ///     Handle edited meeting submission.
        /// </summary>
        [Authorise("Meeting")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditMeeting(MeetingViewModel meetingViewModel)
        {
            if (!ModelState.IsValid)
            {
                PopulateMeetingViewModel(meetingViewModel);
                return View("MeetingForm", meetingViewModel);
            }

            try
            {
                var meeting = MeetingsCore.UpdateMeeting(meetingViewModel);
                return GetMeetingSuccessResult(meeting.ID, "Meeting updated successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error updating meeting.");
            }

            return RedirectToAction("Index");
        }

        #region View/Edit Meeting AJAX

        /// <summary>
        /// Update a meeting via AJAX
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        [HttpPost]
        public bool UpdateMeeting(MeetingViewModel vm)
        {
            try
            {
                var meeting = MeetingsCore.UpdateMeeting(vm);
                return true;
            }
            catch (Exception ex)
            {
                LogException(ex);
                return false;
            }
        }

        /// <summary>
        /// Render quick edit form for meetings
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetMeeting(Guid id)
        {
            var meeting = MeetingService.GetByMeetingId(id);
            MeetingViewModel model = Mapper.Map<MeetingViewModel>(meeting);
            model.PackageId = PackageService.GetByMeetingId(id).ID;
            PopulateMeetingViewModelLists(model);

            return PartialView("_MeetingQuickEdit", model);
        }

        #endregion

        #region VCalenders

        /// <summary>
        /// Download all VCalendars for a given package or the current package if passed null
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult DownloadAllVCalenders(Guid? id)
        {
            var packageId = id ??
                GetCurrentPackageId();
            
            var meetings =
                    MeetingService.GetByPackageId(packageId.Value);

            var vCalenders = meetings.Where(m => m.Start.HasValue && m.End.HasValue).Select(meeting => (VCalander)meeting).ToList();

            var stringData =
                VCalander.GetMeetings(vCalenders);

            var stream =
                new MemoryStream(Encoding.UTF8.GetBytes(stringData ?? string.Empty));

            string fileName =
                string.Format("{0} Meetings.ics", DateTime.Now.ToString("yyyy-MM-dd_hhmm"));

            return File(stream, "text/calendar", fileName);
        }

        /// <summary>
        /// Download the VCalendar for the meeting with the given ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult DownloadVCalender(Guid id)
        {
            var meeting =
                MeetingService.GetByMeetingId(id);

            var stringData =
                VCalander.GetMeeting(meeting);

            var stream =
                new MemoryStream(Encoding.UTF8.GetBytes(stringData ?? string.Empty));

            string fileName =
                string.Format("{0} Meeting.ics", DateTime.Now.ToString("yyyy-MM-dd_hhmm"));

            return File(stream, "text/calendar", fileName);
        }

        #endregion

        #endregion

        #region helpers

        /// <summary>
        ///     Render the meeting form in edit mode.
        /// </summary>
        /// <param name="id">The meeting ID.</param>
        [Authorise("Meeting")]
        [HttpGet]
        public ActionResult EditMeeting(Guid id)
        {
            var meeting = MeetingService.GetByMeetingId(id);

            var meetingViewModel = Mapper.Map<MeetingViewModel>(meeting);
            PopulateMeetingViewModel(meetingViewModel);
            meetingViewModel.ReadOnly = RoleAuthHelper.IsReadOnly("Meeting_Edit_Get");

            AppSession.Current.VoiceMessageManagerReturnUrl = Request.RawUrl;
            AppSession.Current.AttachmentManagerReturnUrl = Request.RawUrl;

            AuditService.RecordActivity<Meeting>(AuditLog.EventType.Viewed, meeting.ID);

            if (meetingViewModel.MeetingTypeId == new Guid("E72D15EF-BD12-49AB-B15B-269EDF6A891B")) // Slap me for this
            {
                meetingViewModel.TargetWeeksList = GetTargetWeekListItems(meetingViewModel.TargetWeek);
                return View("~/Areas/Home/Views/Relationships/IntroMeetingForm.cshtml", meetingViewModel);
            }
            return View("MeetingForm", meetingViewModel);
        }

        /// <summary>
        /// Populate a meeting view model with the data required for rendering dropdowns
        /// </summary>
        /// <param name="meetingViewModel"></param>
        private void PopulateMeetingViewModelLists(MeetingViewModel meetingViewModel)
        {
            meetingViewModel.MeetingTypeItems = MeetingService.GetMeetingTypes().OrderBy(x => x.Order).Select(m =>
                new SelectListItem
                {
                    Text = m.Title,
                    Value = m.ID.ToString()
                });

            meetingViewModel.DurationMinItems = new List<SelectListItem>
            {
                new SelectListItem {Text = "30 mins", Value = "30"},
                new SelectListItem {Text = "1 hour", Value = "60"},
                new SelectListItem {Text = "2 hours", Value = "120"},
                new SelectListItem {Text = "3 hours", Value = "180"},
                new SelectListItem {Text = "4 hours", Value = "240"}
            };
        }

        /// <summary>
        /// Get the data for target week dropdown
        /// </summary>
        /// <param name="currentValue"></param>
        /// <returns></returns>
        private static IEnumerable<SelectListItem> GetTargetWeekListItems(int? currentValue)
        {
            var earliestStartWeek = 1;

            currentValue = currentValue ?? earliestStartWeek;

            var weekDueListItems = new List<SelectListItem>();
            for (var i = earliestStartWeek; i <= Core.TaskCore.MaxWeekDue; i++)
            {
                weekDueListItems.Add(new SelectListItem()
                {
                    Text = string.Format("Week {0}", i),
                    Value = i.ToString(),
                    Selected = i == currentValue
                });
            }

            return weekDueListItems;
        }

        /// <summary>
        /// Populate meeting view model with the data necessary to render the form
        /// </summary>
        /// <param name="meetingViewModel"></param>
        private void PopulateMeetingViewModel(MeetingViewModel meetingViewModel)
        {
            var currentPackageId = GetCurrentPackageId().Value;

            meetingViewModel.Contacts = ContactService.GetByPackageId(currentPackageId).Select(c =>
                new SelectListItem
                {
                    Text = c.Name,
                    Value = c.ID.ToString()
                });

            meetingViewModel.MeetingTypeItems = MeetingService.GetMeetingTypes().OrderBy(x => x.Order).Select(m =>
                new SelectListItem
                {
                    Text = m.Title,
                    Value = m.ID.ToString()
                });

            meetingViewModel.DurationMinItems = new List<SelectListItem>
            {
                new SelectListItem {Text = "30 mins", Value = "30"},
                new SelectListItem {Text = "1 hour", Value = "60"},
                new SelectListItem {Text = "2 hours", Value = "120"},
                new SelectListItem {Text = "3 hours", Value = "180"},
                new SelectListItem {Text = "4 hours", Value = "240"}
            };
        }

        /// <summary>
        /// Get an action result for a sucessful submission of the metting form
        /// </summary>
        /// <param name="meetingId"></param>
        /// <param name="successMessage"></param>
        /// <returns></returns>
        private ActionResult GetMeetingSuccessResult(Guid meetingId, string successMessage)
        {
            // Redirect to a specific action depending on the submit button pressed
            switch (Request.Form["Redirect"])
            {
                case "VoiceMessages":
                    return RedirectToAction("Index", "VoiceMessage",
                        new { area = "", id = meetingId, type = ItemType.Meeting });
                case "Attachments":
                    return RedirectToAction("Index", "Attachments",
                        new { area = "", id = meetingId, type = ItemType.Meeting });
            }

            AddAlert(AlertViewModel.AlertType.Success, successMessage);
            return Redirect(AppSession.Current.MeetingFormReturnUrl);
        }

        /// <summary>
        /// Check if a date is valid for a meeting given the package
        /// start date, last day, and status.
        /// </summary>
        /// <param name="date"></param>
        /// <param name="package"></param>
        /// <returns></returns>
        private bool IsMeetingDateValid(DateTime date, Package package)
        {
            bool inFuture = date > SystemTime.Now;
            if (HandoverService.IsOnboarding(package.ID))
            {
                return date >= package.StartDate && inFuture;
            }
            else if (HandoverService.IsLeaving(package.ID))
            {
                return date <= package.Handover.LeavingPeriod.LastDay && inFuture;
            }
            else
            {
                return date >= package.StartDate && inFuture;
            }
        }

        #endregion
    }
}