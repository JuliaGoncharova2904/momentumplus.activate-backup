﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using MomentumPlus.Activate.Services;
using MomentumPlus.Activate.Services.Helpers;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Areas.Home.Models;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Controllers;
using MomentumPlus.Activate.Web.Core.Home;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.People;

namespace MomentumPlus.Activate.Web.Areas.Home.Controllers
{
    /// <summary>
    ///     Controller for the Relationships module.
    /// </summary>
    public class RelationshipsController : BaseController
    {
        #region IOC

        private readonly MeetingsCore MeetingsCore;
        private readonly RelationshipsCore RelationshipsCore;

        private readonly IContactService ContactService;
        private readonly IMeetingService MeetingService;
        private readonly IHandoverService HandoverService;
        private readonly IPeopleService PeopleService;
        private readonly IAccountConfigService AccountConfigService;

        /// <summary>
        ///     Construct a new <c>RelationshipsController</c> with the given service
        ///     implementations.
        /// </summary>
        public RelationshipsController(
            IAuditService auditService,
            IAccountConfigService accountConfigService,
            IContactService contactService,
            IMasterListService masterListService,
            IMeetingService meetingService,
            IPackageService packageService,
            IUserService userService,
            IPeopleService peopleService,
            IHandoverService handoverService,
            RelationshipsCore relationshipsCore,
            MeetingsCore meetingsCore
            )
            : base(
                auditService,
                masterListService,
                packageService,
                userService
                )
        {
            RelationshipsCore = relationshipsCore;


            MeetingsCore = meetingsCore;

            ContactService = contactService;
            MeetingService = meetingService;
            HandoverService = handoverService;
            PeopleService = peopleService;
            AccountConfigService = accountConfigService;
        }

        #endregion

        #region ACTIONS

        #region Index

        /// <summary>
        ///     Render the Relationships module landing page.
        /// </summary>
        [HttpGet]
        public ActionResult Index(
            int pageNo = 1,
            IEnumerable<string> selectedCompanies = null,
            bool showMeetings = true,
            int activePageNo = 1,
            int retiredPageNo = 1,
            int introPageNo = 1,
            int managementPageNo = 1
            )
        {
            var packageId = GetCurrentPackageId().Value;
            var package = PackageService.GetByPackageId(packageId);

            selectedCompanies = ContactService.GetCompanies(packageId).ToList();

            var viewModel = RelationshipsCore.GetRelationshipsViewModel(
                pageNo,
                selectedCompanies,
                activePageNo,
                retiredPageNo,
                introPageNo,
                managementPageNo,
                package
                );

            AppSession.Current.MeetingFormReturnUrl = Request.RawUrl;
            AppSession.Current.ContactFormReturnUrl = Request.RawUrl;
            AppSession.Current.QuickAddRelationshipFormReturnUrl = Request.RawUrl;

            // Update last viewed date for relationships when a user views their own relationships
            var currentUser = GetCurrentUser();
            if (currentUser.ID == package.UserOwnerId)
            {
                package.RelationshipsLastViewed = DateTime.Now;
                PackageService.Update(package);
            }
            viewModel.ID = packageId;
            var cookie = Request.Cookies.Get("relationships-index");
            if (cookie != null)
            {
                cookie.Value = "contacts";
            }
            else
            {
                cookie = new HttpCookie("relationships-index", "contacts");
            }
            Request.Cookies.Set(cookie);

            return View("~/Areas/Home/Views/Relationships/Index.cshtml", viewModel);
        }

        #endregion

        #region Create

        /// <summary>
        ///     Render the relationship form in create mode.
        /// </summary>
        [HttpGet]
        public ActionResult Create()
        {
            // Check for hiding/showing default company

            var package = PackageService.GetByPackageId(GetCurrentPackageId().Value);
            var contactViewModel = RelationshipsCore.GetNewContactViewModel(package, GetCurrentUser());
            var a = AccountConfigService.Get().UserInternalContacts;
            var b = MasterListService.GetDefaultCompanyId().Value.ToString();

            // If users can add internal contacts the add the master companies to the view model
            if (!AccountConfigService.Get().UserInternalContacts && MasterListService.GetDefaultCompanyId().HasValue)
                contactViewModel.Person.Companies = MasterListService.GetAll(MasterList.ListType.Companies).OrderBy(c => c.Name).Select(c =>
                    new SelectListItem { Text = c.Name, Value = c.ID.ToString() }
                    );

            ViewData["HideSideNav"] = true;
            return View("ContactForm", contactViewModel);
        }

        /// <summary>
        ///     Handle relationship form submission.
        /// </summary>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ContactViewModel contactViewModel, HttpPostedFileBase profileImage)
        {
            // Get current package
            var package = PackageService.GetByPackageId(GetCurrentPackageId().Value);

            ValidateEmploymentStatus(ModelState, contactViewModel);

            if (ContactService.EmailExistsWidthCore(contactViewModel.Person.Email, package.ID))
            {
				if (contactViewModel.Person.Email != null)
				{
					ModelState.AddModelError("Person.Email", @"Contact with this Email already added");
				}
            }


            if (!ModelState.IsValid)
            {
                RelationshipsCore.PopulateContactViewModel(contactViewModel, package);

                if (!AccountConfigService.Get().UserInternalContacts && MasterListService.GetDefaultCompanyId().HasValue)
                    contactViewModel.Person.Companies = MasterListService.GetAll(MasterList.ListType.Companies).OrderBy(c => c.Name).Select(c =>
                    new SelectListItem { Text = c.Name, Value = c.ID.ToString() }
                    );

                ViewData["HideSideNav"] = true;
                return View("ContactForm", contactViewModel);
            }

            try
            {
                var contact = RelationshipsCore.CreateContact(contactViewModel, package, profileImage);

                var returnUrl = Url.Action("Edit", new { id = contact.ID });
                AppSession.Current.VoiceMessageManagerReturnUrl = returnUrl;
                AppSession.Current.AttachmentManagerReturnUrl = returnUrl;

                return GetContactSuccessResult(contact, "Contact created successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error creating contact.");
            }

            return RedirectToAction("Index");
        }

        #endregion

        #region Edit

        /// <summary>
        ///     Render the relationship form in edit mode.
        /// </summary>
        /// <param name="id">The ID of the Contact.</param>
        [Authorise("Contact")]
        [HttpGet]
        public ActionResult Edit(Guid id, string urlAction)
        {
            var contact = ContactService.GetById(id);
            var package = PackageService.GetByPackageId(GetCurrentPackageId().Value);
            var contactViewModel = RelationshipsCore.GetContactViewModel(contact, package, GetCurrentUser());

            SetModeratedViewModelFields(contact, contactViewModel, package);
            contactViewModel.ShowLineManagerSection = true;
            contactViewModel.ShowHrManagerSection = true;

            AuditService.RecordActivity<Contact>(AuditLog.EventType.Viewed, contact.ID);

            AppSession.Current.AttachmentManagerReturnUrl = Request.RawUrl;
            AppSession.Current.VoiceMessageManagerReturnUrl = Request.RawUrl;

            contactViewModel.Person.CompanyId = contact.CompanyId;
            contactViewModel.Person.OtherCompany = contact.OtherCompany;
            contactViewModel.Person.OtherCompanyName = contact.OtherCompanyName;

            ViewData["HideSideNav"] = true;

            ViewBag.urlAction = urlAction;

            contactViewModel.IsMyTeamReferer = Request.UrlReferrer.ToString().Contains("/Home/People");
            
            return View("~/Views/Shared/ContactForm.cshtml", contactViewModel);
        }

        /// <summary>
        ///     Handle edit relationship form submission.
        /// </summary>
        [Authorise("Contact")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ContactViewModel contactViewModel, HttpPostedFileBase profileImage)
        {
            var contact = ContactService.GetById(contactViewModel.ID);
            var package = PackageService.GetByPackageId(GetCurrentPackageId().Value);

            if (!contactViewModel.Managed)
                contactViewModel.EmploymentStatus = null;

            //fix AC-194
            if (!contactViewModel.Person.CompanyId.HasValue)
            {
                contactViewModel.CompanyGuid = "014f775c-8b77-48f3-8f3e-3b539405e8af";
                contactViewModel.Person.CompanyId = Guid.Parse("014f775c-8b77-48f3-8f3e-3b539405e8af");
                contactViewModel.Person.OtherCompany = true;
                ModelState.Remove("Person.CompanyId");
            }

            ValidateEmploymentStatus(ModelState, contactViewModel);

            if (ContactService.EmailExistsWidthCore(contactViewModel.Person.Email, package.ID) && contact.Person.Email != contactViewModel.Person.Email)
            {
                ModelState.AddModelError("Person.Email", @"Contact with this Email already in use");
            }

            if (!ModelState.IsValid)
            {
                RelationshipsCore.PopulateContactViewModel(contactViewModel, package, contactViewModel.ID);

                SetModeratedViewModelFields(contact, contactViewModel, package);
                contactViewModel.ShowLineManagerSection = true;
                contactViewModel.ShowHrManagerSection = true;

                contactViewModel.Managed = contact.Managed;
                contactViewModel.EmploymentStatus = contact.EmploymentStatus;
                contactViewModel.FutureTraining = contact.FutureTraining;
                contactViewModel.ManagementNotes = contact.ManagementNotes;

                contactViewModel.NotesSubjectivity = contact.CountSubjectivityRelationship(MasterListService);
                contactViewModel.NotesWordCount = contact.CountWordsRelationship();

                ViewData["HideSideNav"] = true;
                if (contactViewModel.IsMyTeamReferer)
                {
                    return RedirectToAction("Index", "People");
                }
                return RedirectToAction("Index", "Relationships");
            }

            try
            {
                UpdateModerated(contact, contactViewModel, package);

                // Delete intro meeting and set intro required back to no
                if (Request.Form["Redirect"] == "DeleteIntro")
                {
                    var introMeeting = contact.Meetings.FirstOrDefault(
                        m => m.MeetingTypeId == Guid.Parse("e72d15ef-bd12-49ab-b15b-269edf6a891b") &&
                        m.ParentContactId == contact.ID);

                    if (introMeeting != null)
                    {
                        MeetingService.Delete(introMeeting);
                        contactViewModel.IntroRequired = false;
                    }
                }

                RelationshipsCore.UpdateContact(contact, contactViewModel, package, GetCurrentUser(), profileImage);
				AddAlert(AlertViewModel.AlertType.Success, "Contact updated successfully.");

                if (contactViewModel.IsMyTeamReferer)
                {
                    return RedirectToAction("Index", "People");
                }

                return RedirectToAction("Index", "Relationships");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error updating contact.");
            }

            if (contactViewModel.IsMyTeamReferer)
            {
                return RedirectToAction("Index", "People");
            }

            //fix AC-186
            return RedirectToAction("Index", "Relationships");
        }

        #endregion

        #region Retire/Resume

        /// <summary>
        /// Retire the relationship with the given ID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="returnToUrl"></param>
        /// <returns></returns>
        [Authorise("Contact")]
        [HttpGet]
        public ActionResult RetireRelationship(Guid id, bool returnToUrl = false)
        {
            try
            {
                ContactService.Retire(id);
                AddAlert(AlertViewModel.AlertType.Success, "Contact retired successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Something went wrong, please try again.");
            }

            if (returnToUrl)
            {
                return Redirect(AppSession.Current.ContactFormReturnUrl);
            }

            return RedirectToAction("Edit", new { id });
        }

        /// <summary>
        /// Resume the relationship with the given ID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="returnToUrl"></param>
        /// <returns></returns>
        [Authorise("Contact")]
        [HttpGet]
        public ActionResult ResumeRelationship(Guid id, bool returnToUrl = false)
        {
            try
            {
                ContactService.Restore(id);
                AddAlert(AlertViewModel.AlertType.Success, "Contact resumed successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Something went wrong, please try again.");
            }

            if (returnToUrl)
            {
                return Redirect(AppSession.Current.ContactFormReturnUrl);
            }

            return RedirectToAction("Edit", new { id });
        }

        #endregion

        #region CreateMeeting

        /// <summary>
        ///     Render the meeting form in create mode.
        /// </summary>
        /// <param name="id">The contact ID.</param>
        [HttpGet]
        public ActionResult CreateMeeting()
        {
            var meetingViewModel = new MeetingViewModel { PackageId = GetCurrentPackageId().Value };
            PopulateMeetingViewModel(meetingViewModel);
            meetingViewModel.Start = DateTime.Now;
            return View("MeetingForm", meetingViewModel);
        }

        /// <summary>
        /// Render meeting form with package ID set
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult CreateMeetingForPackage(Guid id)
        {
            var meetingViewModel = new MeetingViewModel { PackageId = id };
            PopulateMeetingViewModel(meetingViewModel);
            meetingViewModel.Start = DateTime.Now;
            return View("MeetingForm", meetingViewModel);
        }

        /// <summary>
        /// Render intro meeting from for current package
        /// </summary>
        /// <returns></returns>
        public ActionResult CreateIntroMeeting()
        {
            var meetingViewModel = new MeetingViewModel { PackageId = GetCurrentPackageId().Value };
            PopulateMeetingViewModel(meetingViewModel);
            meetingViewModel.MeetingTypeId = new Guid("E72D15EF-BD12-49AB-B15B-269EDF6A891B"); // Slap me for this
            meetingViewModel.Start = null;

            meetingViewModel.TargetWeeksList = GetTargetWeekListItems(null);

            return View("IntroMeetingForm", meetingViewModel);
        }

        private static IEnumerable<SelectListItem> GetTargetWeekListItems(int? currentValue)
        {
            var earliestStartWeek = 1;

            currentValue = currentValue ?? earliestStartWeek;

            var weekDueListItems = new List<SelectListItem>();
            for (var i = earliestStartWeek; i <= Core.TaskCore.MaxWeekDue; i++)
            {
                weekDueListItems.Add(new SelectListItem()
                {
                    Text = string.Format("Week {0}", i),
                    Value = i.ToString(),
                    Selected = i == currentValue
                });
            }

            return weekDueListItems;
        }

        /// <summary>
        ///     Handle new meeting submission.
        /// </summary>
        [Authorise("MeetingCreate")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateMeeting(MeetingViewModel meetingViewModel)
        {
            if (!ModelState.IsValid || meetingViewModel.Start.HasValue && !IsMeetingDateValid(meetingViewModel.Start.Value, PackageService.GetByPackageId(meetingViewModel.PackageId)))
            {
                if (meetingViewModel.Start.HasValue && !IsMeetingDateValid(meetingViewModel.Start.Value,
                    PackageService.GetByPackageId(meetingViewModel.PackageId)))
                {
                    ModelState.AddModelError("Start Date",
                        "The meeting start date must be within the employee's employment period");
                }

                PopulateMeetingViewModel(meetingViewModel);
                return View("MeetingForm", meetingViewModel);
            }

            try
            {
                var meeting = MeetingsCore.CreateMeeting(meetingViewModel);

                var returnUrl = Url.Action("EditMeeting", new { id = meeting.ID });
                AppSession.Current.VoiceMessageManagerReturnUrl = returnUrl;
                AppSession.Current.AttachmentManagerReturnUrl = returnUrl;

                return GetMeetingSuccessResult(meeting.ID, "Meeting added successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error adding meeting.");
            }

            return RedirectToAction("Index");
        }

        #endregion

        #region EditMeeting

        /// <summary>
        ///     Render the meeting form in edit mode.
        /// </summary>
        /// <param name="id">The meeting ID.</param>
        [Authorise("Meeting")]
        [HttpGet]
        public ActionResult EditMeeting(Guid id)
        {
            var meeting = MeetingService.GetByMeetingId(id);

            var meetingViewModel = Mapper.Map<MeetingViewModel>(meeting);
            meetingViewModel.PackageId = PackageService.GetByMeetingId(id).ID;
            PopulateMeetingViewModel(meetingViewModel);
            meetingViewModel.ReadOnly = RoleAuthHelper.IsReadOnly("Meeting_Edit_Get");

            AppSession.Current.VoiceMessageManagerReturnUrl = Request.RawUrl;
            AppSession.Current.AttachmentManagerReturnUrl = Request.RawUrl;

            AuditService.RecordActivity<Meeting>(AuditLog.EventType.Viewed, meeting.ID);

            if (meetingViewModel.MeetingTypeId == new Guid("E72D15EF-BD12-49AB-B15B-269EDF6A891B")) // Slap me for this
            {
                meetingViewModel.TargetWeeksList = GetTargetWeekListItems(meetingViewModel.TargetWeek);
                return View("IntroMeetingForm", meetingViewModel);
            }
            return View("MeetingForm", meetingViewModel);
        }

        /// <summary>
        ///     Handle edited meeting submission.
        /// </summary>
        [Authorise("Meeting")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditMeeting(MeetingViewModel meetingViewModel)
        {
            if (!ModelState.IsValid)
            {
                PopulateMeetingViewModel(meetingViewModel);
                return View("MeetingForm", meetingViewModel);
            }

            try
            {
                var meeting = MeetingsCore.UpdateMeeting(meetingViewModel);
                return GetMeetingSuccessResult(meeting.ID, "Meeting updated successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error updating meeting.");
            }

            return RedirectToAction("Index");
        }

        #endregion

        #region UpdateChart

        /// <summary>
        ///     Update the chart coordinates on the given contacts.
        /// </summary>
        [HttpPost]
        public void UpdateChart(IEnumerable<ContactChartViewModel> contacts)
        {
            if (contacts == null)
                return;

            contacts = contacts.ToList();
            if (contacts.Any())
            {
                foreach (var contactViewModel in contacts)
                {
                    var contact = ContactService.GetById(contactViewModel.ID);
                    contact.ChartX = contactViewModel.ChartX;
                    contact.ChartY = contactViewModel.ChartY;
                    contact.RelationshipImportance = contactViewModel.RelationshipImportance;
                    ContactService.Update(contact);
                }
            }
        }

        #endregion

        [HttpGet]
        public ActionResult QuickAddRelationship()
        {
            var viewModel = new QuickAddRelationshipViewModel();
            PopulateSelectRelationshipViewModel(viewModel);

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult QuickAddRelationship(QuickAddRelationshipViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                PopulateSelectRelationshipViewModel(viewModel);
                return View(viewModel);
            }
            Guid newContactId = Guid.Empty;
            try
            {
                var currentPackageId = GetCurrentPackageId();
                var contact = ContactService.GetById(viewModel.ContactId.Value);

                if (contact.IsFramework && currentPackageId.HasValue)
                {
                    var newContact = contact.DeepClone();

                    newContact.PackageId = currentPackageId.Value;
                    newContact.RelationshipImportance = Contact.Importance.Level1;

                    newContact = ContactService.Add(newContact);
                    newContactId = newContact.ID;
                }

                AddAlert(AlertViewModel.AlertType.Success, SaveSuccessMessage);
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, SaveErrorMessage);
            }

            AppSession.Current.ContactFormReturnUrl = AppSession.Current.QuickAddRelationshipFormReturnUrl;

            return RedirectToAction("Edit", new { id = newContactId });
        }

        public ActionResult DownloadContacts(Guid id)
        {
            var package = PackageService.GetByPackageId(id);
            var contactList = ContactService.GetByPackageId(package.ID);
            var contacts = new List<ContactViewModel>();
            foreach (var contact in contactList)
            {
                var contactViewModel = RelationshipsCore.GetContactViewModel(contact, package, GetCurrentUser());
                contactViewModel.Person.CompanyId = contact.CompanyId;
                contactViewModel.Person.OtherCompany = contact.OtherCompany;
                contactViewModel.Person.OtherCompanyName = contact.OtherCompanyName;
                contacts.Add(contactViewModel);
            }

            var stringData = ContactsExport.ExportForOutlook(contacts);

            var stream = new MemoryStream(Encoding.UTF8.GetBytes(stringData ?? string.Empty));

            string fileName = string.Format("{0} Contacts.csv", package.DisplayName);

            return File(stream, "text/csv", fileName);
        }

        #region RelationshipsStats

        /// <summary>
        ///     Render relationships stats widget.
        /// </summary>
        [ChildActionOnly]
        public ActionResult RelationshipsStats()
        {
            var packageId = GetCurrentPackageId();

            if (packageId.HasValue)
            {
                var viewModel = RelationshipsCore.GetRelationshipsStatsViewModel(packageId.Value);
                return View("RelationshipsStats", viewModel);
            }
            return new EmptyResult();
        }

        #endregion

        #endregion

        #region HELPERS

        /// <summary>
        /// Populate meeting view model with dropdown data
        /// </summary>
        /// <param name="meetingViewModel"></param>
        private void PopulateMeetingViewModel(MeetingViewModel meetingViewModel)
        {
            meetingViewModel.Contacts = ContactService.GetByPackageId(meetingViewModel.PackageId).Select(c =>
                new SelectListItem
                {
                    Text = c.Name,
                    Value = c.ID.ToString()
                });

            meetingViewModel.MeetingTypeItems = MeetingService.GetMeetingTypes().OrderBy(x => x.Order).Select(m =>
                new SelectListItem
                {
                    Text = m.Title,
                    Value = m.ID.ToString()
                });

            meetingViewModel.DurationMinItems = new List<SelectListItem>
            {
                new SelectListItem {Text = "30 mins", Value = "30"},
                new SelectListItem {Text = "1 hour", Value = "60"},
                new SelectListItem {Text = "2 hours", Value = "120"},
                new SelectListItem {Text = "3 hours", Value = "180"},
                new SelectListItem {Text = "4 hours", Value = "240"}
            };
        }

        /// <summary>
        /// Get an approprite action result for a successful submission of the contact form
        /// </summary>
        /// <param name="contact"></param>
        /// <param name="successMessage"></param>
        /// <returns></returns>
        private ActionResult GetContactSuccessResult(Contact contact, string successMessage)
        {
            try
            {
                var companyName = MasterListService.GetByMasterListId(contact.CompanyId.Value).Name;

                AppCookies.SelectedCompanies = AppCookies.SelectedCompanies.Union(new[] { companyName }).ToList();
            }
            catch (Exception ex)
            {
                LogException(ex);
            }

            var redirectTo = Request.Form["Redirect"];
            Guid Id = Guid.Empty;

            if (redirectTo != null && (redirectTo.StartsWith("EditTask") || redirectTo.StartsWith("EditMeeting")))
            {
                var temp = redirectTo.Split('/');
                redirectTo = temp[0];
                Id = new Guid(temp[1]);
            }

            // Redirect to a specific action depending on the submit button pressed
            switch (redirectTo)
            {
                case "VoiceMessages":
                    return RedirectToAction("Index", "VoiceMessage",
                        new { area = "", id = contact.ID, type = ItemType.Contact });
                case "Attachments":
                    return RedirectToAction("Index", "Attachments",
                        new { area = "", id = contact.ID, type = ItemType.Contact });
                case "AddAnother":
                    return RedirectToAction("Create");
                case "Tasks":
                    AppSession.Current.TaskFormReturnUrl = String.Format("{0}#leaver", Request.RawUrl);
                    return RedirectToAction("Index", "TaskManager", new { area = "", id = contact.ID, type = ItemType.Contact });
                case "AddTask":
                    AppSession.Current.TaskFormReturnUrl = String.Format("{0}#leaver", Request.RawUrl);
                    return RedirectToAction("CreateTask", "Task", new { area = "", id = contact.ID, parentType = TaskViewModel.ParentType.Contact });
                case "EditTask":
                    AppSession.Current.TaskFormReturnUrl = String.Format("{0}#leaver", Request.RawUrl);
                    return RedirectToAction("EditTask", "Task", new { area = "", id = Id });
                case "EditMeeting":
                    AppSession.Current.MeetingFormReturnUrl = String.Format("{0}#leaver", Request.RawUrl);
                    return RedirectToAction("EditMeeting", "Relationships", new { id = Id });
                case "DeleteIntro":
                    return Redirect(Url.Action("Edit", new {id = contact.ID}) + "#leaver");
            }

            AddAlert(AlertViewModel.AlertType.Success, successMessage);
            return Redirect(AppSession.Current.ContactFormReturnUrl);
        }

        /// <summary>
        /// Get an approprite action result for a successful submission of the meeting form
        /// </summary>
        private ActionResult GetMeetingSuccessResult(Guid meetingId, string successMessage)
        {
            // Redirect to a specific action depending on the submit button pressed
            switch (Request.Form["Redirect"])
            {
                case "VoiceMessages":
                    return RedirectToAction("Index", "VoiceMessage",
                        new { area = "", id = meetingId, type = ItemType.Meeting });
                case "Attachments":
                    return RedirectToAction("Index", "Attachments",
                        new { area = "", id = meetingId, type = ItemType.Meeting });
            }

            AddAlert(AlertViewModel.AlertType.Success, successMessage);
            return Redirect(AppSession.Current.MeetingFormReturnUrl);
        }

        private void PopulateSelectRelationshipViewModel(QuickAddRelationshipViewModel viewModel)
        {
            var currentPackageId = GetCurrentPackageId();
            if (currentPackageId.HasValue)
            {
                var packageContactSources = ContactService.GetByPackageId(currentPackageId.Value).Select(c => c.SourceId).ToList();
                viewModel.Contacts = ContactService.GetAllFramework()
                    .AsQueryable()
                    .Include(c => c.Company)
                    .Where(c => !packageContactSources.Contains(c.ID) && !string.IsNullOrEmpty(c.Position) && !string.IsNullOrEmpty(c.Title) && c.Company != null)
                    .ToList()
                    .Select(c => new SelectListItem()
                    {
                        Text = (c.Company != null ? c.Company.Name + " - " : "") + c.Name,
                        Value = c.ID.ToString()
                    })
                    .OrderBy(c => c.Text)
                    .ToList();
            }
        }

        private static void ValidateEmploymentStatus(ModelStateDictionary modelState, ContactViewModel model)
        {
            modelState["RelationshipImportance"].Value = null;

            if (model.Managed && string.IsNullOrWhiteSpace(model.EmploymentStatus))
            {
                modelState.AddModelError("EmploymentStatus", "The Employment Type * field is required.");
            }

            //fix AC-194
            if (!model.Person.OtherCompany)
            {
                model.Person.OtherCompanyName = null;
            }

            if (model.RelationshipImportance == null)
            {
                modelState.AddModelError("RelationshipImportance", @"Please select a relationship importance level");
            }
        }

        private bool IsMeetingDateValid(DateTime date, Package package)
        {
            bool inFuture = date > SystemTime.Now;
            if (HandoverService.IsOnboarding(package.ID))
            {
                return date >= package.StartDate && inFuture;
            }
            else if (HandoverService.IsLeaving(package.ID))
            {
                return date <= package.Handover.LeavingPeriod.LastDay && inFuture;
            }
            else
            {
                return date >= package.StartDate && inFuture;
            }
        }

        #endregion
    }
}