﻿using System.Web.Mvc;
using MomentumPlus.Activate.Services;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Core;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Areas.Home.Controllers
{
    /// <summary>
    /// Controller for the Critical module.
    /// </summary>
    public class CriticalController : TopicGroupsController
    {
        #region IOC

        /// <summary>
        /// Construct a new <c>CriticalController</c> with the given service
        /// implementations.
        /// </summary>
        public CriticalController(
            IAuditService auditService,
            IHandoverService handoverService,
            IMasterListService masterListService,
            IModuleService moduleService,
            IPackageService packageService,
            ISystemService systemService,
            ITaskService taskService,
            ITopicGroupService topicGroupService,
            ITopicService topicService,
            ITopicQuestionService topicQuestionService,
            IUserService userService,
            IWorkplaceService workplaceService,
            IProLinkService proLink,
            IFlaggingService flaggingService,
            IPositionService positionService,
            TopicCore topicCore
        )
            : base(
                auditService,
                handoverService,
                masterListService,
                moduleService,
                packageService,
                systemService,
                taskService,
                topicGroupService,
                topicService,
                topicQuestionService,
                userService,
                workplaceService,
                proLink,
                flaggingService,
                positionService,
                topicCore)
        {
        }

        #endregion

        #region ACTIONS

        /// <summary>
        /// Render the Critical module landing page.
        /// </summary>
        [HttpGet]
        public ActionResult Index()
        {
            return GetTopicGroupsView(Module.ModuleType.Critical, "F6D961ED-EF35-451D-A235-5E9B2DD90321");
        }

        #endregion
    }
}
