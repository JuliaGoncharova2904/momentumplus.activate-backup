﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MomentumPlus.Activate.Services;
using MomentumPlus.Activate.Web.Controllers;
using MomentumPlus.Activate.Web.Core.Home;
using MomentumPlus.Activate.Services.Interfaces;

namespace MomentumPlus.Activate.Web.Areas.Home.Controllers
{
    public class ToDoItemController : BaseController
    {

        private IToDoItemService ToDoItemService;

        public ToDoItemController(
            IToDoItemService toDoItemService,
            IAuditService auditService,
            IMasterListService masterListService,
            IPackageService packageService,
            IUserService userService
        )
            : base(
                  auditService,
                  masterListService,
                  packageService,
                  userService
              )
        {
            ToDoItemService = toDoItemService;
        }

        [HttpPost]
        public bool Dismiss(Guid id)
        {
            ToDoItemService.Dismiss(id);
            return true;
        }
    }
}