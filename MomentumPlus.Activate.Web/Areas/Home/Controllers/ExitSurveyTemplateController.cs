﻿using MomentumPlus.Activate.Services;
using MomentumPlus.Activate.Web.Controllers;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MomentumPlus.Core.DataSource;

namespace MomentumPlus.Activate.Web.Areas.Home.Controllers
{
    public class ExitSurveyTemplateController : BaseController
    {
        public ActionResult Index()
        {
            ExitSurveyService exitSurveyService = new ExitSurveyService(new MomentumContext());

            IEnumerable<ExitSurveyTemplateViewModel> model =
                exitSurveyService.GetAllExitSurveyTemplate().OrderBy(t => t.Name).Select(t => new ExitSurveyTemplateViewModel
                {
                    Id = t.ID,
                    Name = t.Name
                });

            return View("~/Views/ExitSurvey/Template/Index.cshtml", model);
        }

        [HttpGet]
        public ActionResult CreateExitSurveyTemplate()
        {
            ExitSurveyService exitSurveyService = new ExitSurveyService(new MomentumContext());

            CreateExitSurveyTemplateViewModel model = new CreateExitSurveyTemplateViewModel
            {
                Packages = PackageService.GetAll().OrderBy(p => p.Name).Select(p => new SelectListItem
                {
                    Text = p.Name,
                    Value = p.ID.ToString()
                }).ToList(),
                ExitSurveys = exitSurveyService.GetAll().Select(e => new SelectListItem
                {
                    Text = e.Name,
                    Value = e.ID.ToString()
                }).ToList()
            };

            return View("~/Views/ExitSurvey/Template/CreateTemplate.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateExitSurveyTemplate(CreateExitSurveyTemplateViewModel model)
        {
            ModelState.Clear();
            List<Package> packages = new List<Package>();
            foreach (var packageId in model.PackagesId)
            {
                Package package = PackageService.GetByPackageId(packageId);
                packages.Add(package);
            }

            ExitSurveyTemplate exitSurveyTemplate = new ExitSurveyTemplate()
            {
                ID = Guid.NewGuid(),
                Name = model.Name,
                ExitSurveyId = model.ExitSurveysId,

            };

            MomentumContext context = new MomentumContext();
            context.ExitSurveyTemplate.Add(exitSurveyTemplate);
            context.SaveChanges();

            foreach (var package in packages)
            {
                package.ExitSurveyTemplateId = exitSurveyTemplate.ID;

                PackageService.Update(package);
            }
            context.SaveChanges();

            return RedirectToAction("Index", "ExitSurveyTemplate");
        }

        [HttpGet]
        public ActionResult EditExitSurveyTemplate(Guid exitSurveyId)
        {
            ExitSurveyService exitSurveyService = new ExitSurveyService(new MomentumContext());

            ExitSurveyTemplate exitSurveyTemplate = exitSurveyService.GetExitSurveyTemplateById(exitSurveyId);

            EditExitSurveyTemplateViewModel model = new EditExitSurveyTemplateViewModel
            {
                PackagesId = exitSurveyTemplate.Packages.Select(p => p.ID).ToList(),
                ExitSurveysId = exitSurveyTemplate.ExitSurveyId,
                Name = exitSurveyTemplate.Name,
                Packages = PackageService.GetAll().OrderBy(p => p.Name).Select(p => new SelectListItem
                {
                    Value = p.ID.ToString(),
                    Text = p.Name
                }).ToList(),
                ExitSurveys = exitSurveyService.GetAll().Select(e => new SelectListItem
                {
                    Text = e.Name,
                    Value = e.ID.ToString()
                }).ToList(),
                TemplateId = exitSurveyTemplate.ID
            };

            return View("~/Views/ExitSurvey/Template/EditTemplate.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditExitSurveyTemplate(EditExitSurveyTemplateViewModel model)
        {
            ModelState.Clear();
            ExitSurveyService exitSurveyService = new ExitSurveyService(new MomentumContext());

            ExitSurveyTemplate exitSurveyTemplate = exitSurveyService.GetExitSurveyTemplateById(model.TemplateId);

            exitSurveyTemplate.ExitSurveyId = model.ExitSurveysId;
            exitSurveyTemplate.Name = model.Name;

            exitSurveyService.UpdateExitSurveyTemplate(exitSurveyTemplate);
            
            return RedirectToAction("Index", "ExitSurveyTemplate");
        }
    }
}