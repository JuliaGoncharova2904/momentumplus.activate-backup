﻿using System.Linq;
using System.Web.Mvc;
using MomentumPlus.Activate.Web.Areas.Home.Models;
using MomentumPlus.Activate.Web.Controllers;
using MomentumPlus.Activate.Services.Interfaces;
using System;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Activate.Web.Core;
using MomentumPlus.Activate.Web.Helpers;
using AutoMapper;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Models.Requests;
using System.Text;
using MomentumPlus.Activate.Services;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.Requests;
using MomentumPlus.Core.Models.Moderation;

namespace MomentumPlus.Activate.Web.Areas.Home.Controllers
{
    /// <summary>
    /// Controller for the Projects module.
    /// </summary>
    public class ProjectsController : BaseController
    {
        private ProjectCore ProjectCore;

        #region IOC

        private IModerationService ModerationService;
        private IProjectService ProjectService;
        private IRequestService RequestService;
        private IImageService ImageService;
        private IContactService ContactService;
        private ITaskService TaskService;
        private IMeetingService MeetingService;
        private IHandoverService HandoverService;

        public ProjectsController(
            IAuditService auditService,
            IContactService contactService,
            IImageService imageService,
            IMasterListService masterListService,
            IModerationService moderationService,
            IPackageService packageService,
            IProjectService projectService,
            IRequestService requestService,
            IUserService userService,
            ITaskService taskService,
            IMeetingService meetingService,
            IHandoverService handoverService
            )
            : base(
                auditService,
                masterListService,
                packageService,
                userService
                )
        {
            ProjectCore = new ProjectCore(
                contactService,
                imageService,
                masterListService,
                packageService,
                projectService,
                requestService,
                moderationService
                );

            ModerationService = moderationService;
            ProjectService = projectService;
            RequestService = requestService;
            ImageService = imageService;
            ContactService = contactService;
            TaskService = taskService;
            MeetingService = meetingService;
            HandoverService = handoverService;
        }

        #endregion

        #region ACTIONS

        #region Index

        /// <summary>
        /// Render the projets page for the logged in user.
        /// </summary>
        public ActionResult Index(
            ProjectInstance.SortColumn orderBy = ProjectInstance.SortColumn.Priority,
            bool sortAscending = false,
            int? projectStatus = null,
            int projectsPerPage = 5,
            int pageNo = 1,
            bool retiredProjects = false
        )
        {
            var viewModel = ProjectCore.GetProjectsViewModel(
                orderBy,
                sortAscending,
                projectStatus,
                projectsPerPage,
                pageNo,
                GetCurrentPackageId().Value,
                retiredProjects
            );
                        
            AppSession.Current.QuickAddProjectFormReturnUrl = Request.RawUrl;
            AppSession.Current.ProjectInstanceFormReturnUrl = Request.RawUrl;
            AppSession.Current.ProjectTransferFormReturnUrl = Request.RawUrl;
            AppSession.Current.CancelRequestReturnUrl = Request.RawUrl;
            AppSession.Current.ProjectInstanceRoleChangeFormReturnUrl = Request.RawUrl;
            AppSession.Current.VoiceMessageManagerReturnUrl = Request.RawUrl;
            AppSession.Current.AttachmentManagerReturnUrl = Request.RawUrl;

            // Update last viewed date for projects when a user views their own projects
            var currentUser = GetCurrentUser();
            var package = GetCurrentPackage();
            if (currentUser.ID == package.UserOwnerId)
            {
                package.ProjectsLastViewed = DateTime.Now;
                PackageService.Update(package);
            }

            return View(viewModel);
        }

        #endregion

        #region QuickAddProject

        /// <summary>
        /// Render the quick add project form.
        /// </summary>
        [HttpGet]
        public ActionResult QuickAddProject()
        {
            var viewModel = ProjectCore.GetQuickAddProjectViewModel(GetCurrentPackageId().Value);
            viewModel.SelectExisting = true;
            return View(viewModel);
        }

        /// <summary>
        /// Handle quick add project submission.
        /// </summary>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult QuickAddProject(QuickAddProjectViewModel viewModel)
        {
            if (viewModel.SelectExisting && !viewModel.ExistingProjectId.HasValue)
            {
                ModelState.AddModelError("ExistingProjectId", "Please select a project.");
            }
            else if (!viewModel.SelectExisting && string.IsNullOrEmpty(viewModel.ProjectName))
            {
                ModelState.AddModelError("ProjectName", "Please enter a project name.");
            }

            if (!ModelState.IsValid)
            {
                var packageId = GetCurrentPackageId();
                ProjectCore.PopulateQuickAddProjectViewModel(viewModel, packageId.Value);
                return View(viewModel);
            }

            try
            {
                var packageId = GetCurrentPackageId().Value;
                
                var projectInstance = new ProjectInstance()
                {
                    PackageId = packageId,
                    ProjectId = viewModel.SelectExisting ? viewModel.ExistingProjectId : null,
                    Priority = ProjectInstance.ProjectInstancePriority.Low
                };

                ProjectService.CreateProjectInstance(projectInstance, viewModel.ProjectName,
                    viewModel.ProjectId);

                AuditService.RecordActivity<ProjectInstance>(AuditLog.EventType.Modified, projectInstance.ID);
                
                AddAlert(AlertViewModel.AlertType.Success, SaveSuccessMessage);
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, SaveErrorMessage);
            }

            return RedirectToAction("Index");
        }

        #endregion

        #region EditProjectRequest

        /// <summary>
        /// Render the edit project request form.
        /// </summary>
        [Authorise("Request")]
        [HttpGet]
        public ActionResult EditProjectRequest(Guid id)
        {
            var request = RequestService.GetRequest<NewProjectRequest>(id);
            var viewModel = ProjectCore.GetQuickAddProjectViewModel(GetCurrentPackageId().Value, request.ProjectInstance.ID);
            viewModel.Declined = request.IsDeclined();
            return View("QuickAddProject", viewModel);
        }

        /// <summary>
        /// Handle edit project request submission.
        /// </summary>
        [Authorise("Request")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditProjectRequest(QuickAddProjectViewModel viewModel)
        {
            if (viewModel.SelectExisting && !viewModel.ExistingProjectId.HasValue)
            {
                ModelState.AddModelError("ExistingProjectId", "Please select a project.");
            }
            else if (!viewModel.SelectExisting && string.IsNullOrEmpty(viewModel.ProjectName))
            {
                ModelState.AddModelError("ProjectName", "Please enter a project name.");
            }

            var packageId = GetCurrentPackageId();

            if (!ModelState.IsValid)
            {
                ProjectCore.PopulateQuickAddProjectViewModel(viewModel, packageId.Value);
                return View("QuickAddProject", viewModel);
            }

            try
            {
                ProjectCore.UpdateProjectInstance(viewModel);
                AddAlert(AlertViewModel.AlertType.Success, SaveSuccessMessage);
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, SaveErrorMessage);
            }

            return RedirectToAction("Index");
        }

        #endregion

        #region DeleteRequest

        /// <summary>
        /// Delete a project card still awaiting approval.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorise("ProjectInstance")]
        public ActionResult DeleteRequest(Guid id)
        {
            try
            {
                ProjectService.DeleteInstance(id);
                AddAlert(AlertViewModel.AlertType.Success, SaveSuccessMessage);
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, SaveErrorMessage);
            }

            return Redirect(AppSession.Current.QuickAddProjectFormReturnUrl);
        }

        #endregion

        #region EditProjectCard

        /// <summary>
        /// Render edit form for a project card
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorise("ProjectInstance")]
        [HttpGet]
        public ActionResult EditProjectCard(Guid id)
        {
            var projectInstance = ProjectService.GetProjectInstanceById(id);

            var viewModel = Mapper.Map<ProjectInstanceViewModel>(projectInstance);
            if (projectInstance.Project == null)
            {
                viewModel.Project = ProjectCore.GetDummyProjectViewModel(projectInstance.NewProjectRequest.SuggestedName, projectInstance.NewProjectRequest.SuggetestedProjectId);
            }
            else
            {
                ProjectCore.PopulateProjectViewModel(viewModel.Project);
            }

            viewModel.Project.ReadOnly = true;
            viewModel.Roles = viewModel.Project.Roles;

            SetModeratedViewModelFields(projectInstance, viewModel, projectInstance.Package);
            viewModel.ShowLineManagerSection = true;
            viewModel.ShowHrManagerSection = true;

            // Set return URLs for managers
            AppSession.Current.VoiceMessageManagerReturnUrl = Request.RawUrl;
            AppSession.Current.AttachmentManagerReturnUrl = Request.RawUrl;
            AppSession.Current.TaskManagerReturnUrl = Request.RawUrl;
            AppSession.Current.TaskFormReturnUrl = Request.RawUrl;
            AppSession.Current.PackageTransferFormReturnUrl = Request.RawUrl;
            AppSession.Current.MeetingFormReturnUrl = Request.RawUrl;

            AuditService.RecordActivity<ProjectInstance>(AuditLog.EventType.Viewed, projectInstance.ID);

            return View("ProjectInstanceForm", viewModel);
        }

        /// <summary>
        /// Handle project card form submission
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [Authorise("ProjectInstance")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditProjectCard(ProjectInstanceViewModel viewModel)
        {
            var projectInstance = ProjectService.GetProjectInstanceById(viewModel.ID);

            // Remove validation errors for Project fields
            foreach (var key in ModelState.Keys.Where(k => k.StartsWith("Project.")).ToList())
            {
                ModelState.Remove(key);
            }

            if (HandoverService.IsLeaving(PackageService.GetByProjectInstanceId(viewModel.ID).ID))
                CheckTasksAndMeetings(viewModel);

            if (!ModelState.IsValid)
            {
                SetModeratedViewModelFields(projectInstance, viewModel, projectInstance.Package);
                viewModel.ShowLineManagerSection = true;
                viewModel.ShowHrManagerSection = true;

                ProjectCore.PopulateProjectViewModel(viewModel.Project);
                viewModel.Tasks = TaskService.GetAllByProjectInstanceId(viewModel.ID);
                viewModel.Project.ReadOnly = true;
                viewModel.Roles = viewModel.Project.Roles;
                return View("ProjectInstanceForm", viewModel);
            }

            try
            {
                // Update the fields on the database entity from the view model
                UpdateModerated(projectInstance, viewModel, projectInstance.Package);

                projectInstance.Notes = viewModel.Notes;
                projectInstance.ReadyForReview = viewModel.ReadyForReview;
                projectInstance.Priority = viewModel.Priority;

                var packageId = GetCurrentPackageId();
                var unapprovedProjectInstanceRoleChangeRequests
                    = RequestService.GetRequests<ProjectInstanceRoleChangeRequest>(packageId.Value);
                var roleChangeRequest = unapprovedProjectInstanceRoleChangeRequests.FirstOrDefault(
                    r => r.ProjectInstanceId == projectInstance.ID);

                // If role ID is project manager create a request to change the role
                if (viewModel.RoleId == new Guid("ABCBF948-0715-4791-AEE7-9EC2F9CEE0DC") && projectInstance.ProjectId.HasValue)
                {
                    if (roleChangeRequest == null &&
                        projectInstance.RoleId != viewModel.RoleId)
                    {
                        var projectInstanceRoleChangeRequest = new ProjectInstanceRoleChangeRequest
                        {
                            PackageId = projectInstance.PackageId,
                            ProjectInstanceId = projectInstance.ID,
                            OldRoleId = projectInstance.RoleId,
                            NewRoleId = viewModel.RoleId
                        };

                        ModerationService.AddModerated(projectInstanceRoleChangeRequest);
                    }
                }
                else
                {
                    var contact = ContactService.GetById(projectInstance.Package.UserOwnerId);
                    if (contact != null && projectInstance.Project != null)
                    {
                        projectInstance.Project.ProjectManagerId = contact.ID;
                    }

                    if (roleChangeRequest != null)
                    {
                        RequestService.DeleteRequest(roleChangeRequest);
                    }

                    projectInstance.RoleId = viewModel.RoleId;
                }

                ProjectService.Update(projectInstance);

                AuditService.RecordActivity<ProjectInstance>(AuditLog.EventType.Modified, projectInstance.ID);

                return GetProjectInstanceSucessResult(projectInstance.ID, SaveSuccessMessage);
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, SaveErrorMessage);
            }

            return Redirect(AppSession.Current.ProjectInstanceFormReturnUrl);
        }
        
        #endregion

        #region TransferProjectCard

        /// <summary>
        /// Render form to transfer a project card from one package to another
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorise("ProjectInstance")]
        [HttpGet]
        public ActionResult TransferProjectCard(Guid id)
        {
            var projectInstance = ProjectService.GetProjectInstanceById(id);

            if (projectInstance.Project == null)
            {
                AddAlert(AlertViewModel.AlertType.Danger, "An unapproved project cannot be transferred. Please contact admin to approve this project and try again.");
                return RedirectToAction("EditProjectCard", new { id = id} );
            }
            else
            {
                var viewModel = new ProjectInstanceTransferRequestViewModel
                {
                    ProjectInstance = projectInstance,
                    ProjectInstanceId = id
                };

                viewModel.Packages = PackageService.GetAll().Where(p => p.ID != projectInstance.PackageId && p.ProjectsEnabled).OrderBy(p => p.DisplayName).Select(p =>
                    new SelectListItem { Text = p.DisplayName, Value = p.ID.ToString() }
                );

                return View("ProjectTransferForm", viewModel);
            }
        }

        /// <summary>
        /// Handle project card transfer form submission
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [Authorise("ProjectInstance")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult TransferProjectCard(ProjectInstanceTransferRequestViewModel viewModel)
        {
            var projectInstance = ProjectService.GetProjectInstanceById(viewModel.ProjectInstanceId.Value);

            if (!ModelState.IsValid)
            {
                viewModel.ProjectInstance = projectInstance;

                viewModel.Packages = PackageService.GetAll().Where(p => p.ID != projectInstance.PackageId && p.ProjectsEnabled).OrderBy(p => p.DisplayName).Select(p =>
                    new SelectListItem { Text = p.DisplayName, Value = p.ID.ToString() }
                );

                return View("ProjectTransferForm", viewModel);
            }

            try
            {
                var currentUser = GetCurrentUser();

                // Auto-approve transfers when the current user is admin/manager
                if (currentUser.Role.RoleParsed == Role.RoleEnum.iHandoverAdmin ||
                    currentUser.Role.RoleParsed == Role.RoleEnum.Administrator ||
                    currentUser.ID == projectInstance.Package.UserHRManagerId ||
                    currentUser.ID == projectInstance.Package.UserLineManagerId)
                {
                    projectInstance.PackageId = viewModel.DestinationId;
                    ProjectService.Update(projectInstance);
                }
                else
                {
                    var transferRequest = new ProjectInstanceTransferRequest
                    {
                        PackageId = projectInstance.PackageId,
                        ProjectInstanceId = projectInstance.ID,
                        TransferSourceId = projectInstance.Package.ID,
                        DestinationId = viewModel.DestinationId
                    };

                    ModerationService.AddModerated(transferRequest);
                }

                AddAlert(AlertViewModel.AlertType.Success, SaveSuccessMessage);
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, SaveErrorMessage);
            }

            return Redirect(AppSession.Current.ProjectTransferFormReturnUrl);
        }

        #endregion

        #region RetireProjectCard

        /// <summary>
        /// Retire the project card with the given ID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="returnToUrl"></param>
        /// <returns></returns>
        [Authorise("ProjectInstance")]
        [HttpGet]
        public ActionResult RetireProjectCard(Guid id, bool returnToUrl = false)
        {
            try
            {
                ProjectService.RetireInstance(id);
                AddAlert(AlertViewModel.AlertType.Success, "Project Card retired successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Something went wrong, please try again.");
            }

            if (returnToUrl)
            {
                return Redirect(AppSession.Current.ContactFormReturnUrl);
            }

            return RedirectToAction("EditProjectCard", new { id = id });
        }

        #endregion

        #region ResumeProjectCard

        /// <summary>
        /// Resume the project card with the given ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorise("ProjectInstance")]
        [HttpGet]
        public ActionResult ResumeProjectCard(Guid id)
        {
            try
            {
                ProjectService.RestoreInstance(id);
                AddAlert(AlertViewModel.AlertType.Success, "Project Card resumed successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Error resuming Project Card.");
            }

            return RedirectToAction("EditProjectCard", new { id = id });
        }

        #endregion

        #region EditTransferRequest

        /// <summary>
        /// Render transfer request edit form
        /// </summary>
        /// <param name="id">The ID of the transfer request</param>
        /// <returns></returns>
        [Authorise("Request")]
        [HttpGet]
        public ActionResult EditTransferRequest(Guid id)
        {
            var transferRequest = ModerationService.GetById<ProjectInstanceTransferRequest>(id);
            var viewModel = Mapper.Map<ProjectInstanceTransferRequestViewModel>(transferRequest);

            viewModel.Packages = PackageService.GetAll().Where(p => p.ID != transferRequest.SourceId && p.ProjectsEnabled).OrderBy(p => p.DisplayName).Select(p =>
                new SelectListItem { Text = p.DisplayName, Value = p.ID.ToString() }
            );

            return View("ProjectTransferForm", viewModel);
        }

        /// <summary>
        /// Handle edit transfer request form submission
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [Authorise("Request")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTransferRequest(ProjectInstanceTransferRequestViewModel viewModel)
        {
            var projectInstance = ProjectService.GetProjectInstanceById(viewModel.ProjectInstanceId.Value);

            if (!ModelState.IsValid)
            {
                viewModel.ProjectInstance = projectInstance;

                viewModel.Packages = PackageService.GetAll().Where(p => p.ID != projectInstance.PackageId && p.ProjectsEnabled).OrderBy(p => p.DisplayName).Select(p =>
                    new SelectListItem { Text = p.DisplayName, Value = p.ID.ToString() }
                );

                return View("ProjectTransferForm", viewModel);
            }

            try
            {
                var transferRequest = ModerationService.GetById<ProjectInstanceTransferRequest>(viewModel.ID);
                transferRequest.DestinationId = viewModel.DestinationId;
                ModerationService.UpdateModerated(transferRequest);
                AddAlert(AlertViewModel.AlertType.Success, SaveSuccessMessage);
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, SaveErrorMessage);
            }

            return Redirect(AppSession.Current.ProjectTransferFormReturnUrl);
        }

        #endregion

        #region EditRoleChangeRequest

        /// <summary>
        /// Render role change request edit form
        /// </summary>
        /// <param name="id">The ID of the request</param>
        /// <returns></returns>
        [Authorise("Request")]
        [HttpGet]
        public ActionResult EditRoleChangeRequest(Guid id)
        {
            var projectInstanceRoleChangeRequest
                = ModerationService.GetById<ProjectInstanceRoleChangeRequest>(id);

            var viewModel = Mapper.Map<ProjectInstanceRoleChangeRequestViewModel>(projectInstanceRoleChangeRequest);

            return View("ProjectInstanceRoleChangeRequestForm", viewModel);
        }

        /// <summary>
        /// Handle submission of role change request edit form
        /// </summary>
        /// <returns></returns>
        [Authorise("Request")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditRoleChangeRequest(ProjectInstanceRoleChangeRequestViewModel viewModel)
        {
            return Redirect(AppSession.Current.ProjectInstanceRoleChangeFormReturnUrl);
        }

        #endregion

        #region ProjectsStats

        /// <summary>
        /// Render project stats view for the current package
        /// </summary>
        /// <returns></returns>
        [ChildActionOnly]
        public ActionResult ProjectsStats()
        {
            Guid? packageId = GetCurrentPackageId();

            if(packageId.HasValue)
            {
                int projectsCount = ProjectService.GetProjectInstances(packageId.Value, null, 0, 0, null, false).Count();

                var model = new ProjectsStatsViewModel()
                {
                    ProjectsCount = projectsCount
                };

                return View(model);
            }

            return new EmptyResult();
        }

        #endregion

        /// <summary>
        /// Update the priority of a given project instance
        /// </summary>
        /// <param name="id"></param>
        /// <param name="priorityValue"></param>
        /// <returns></returns>
        public bool UpdatePriority(Guid id, int priorityValue)
        {
            var projectInstance = ProjectService.GetProjectInstanceById(id);
            projectInstance.Priority = (ProjectInstance.ProjectInstancePriority)priorityValue;
            try
            {
                ProjectService.Update(projectInstance);
                return true;
            }
            catch (Exception ex)
            {
                LogException(ex);
                return false;
            }
        }

        #endregion

        #region HELPERS

        /// <summary>
        /// Get an appropriate action result for successful submission of the project instance form
        /// </summary>
        /// <param name="projectInstanceId"></param>
        /// <param name="successMessage"></param>
        /// <returns></returns>
        private ActionResult GetProjectInstanceSucessResult(Guid projectInstanceId, string successMessage)
        {
            var redirectTo = Request.Form["Redirect"];
            Guid Id = Guid.Empty;

            if (redirectTo != null && (redirectTo.StartsWith("EditTask") || redirectTo.StartsWith("EditMeeting")))
            {
                var temp = redirectTo.Split('/');
                redirectTo = temp[0];
                Id = new Guid(temp[1]);
            }
            // Redirect to a specific action depending on the submit button pressed
            switch (redirectTo)
            {
                case "VoiceMessages":
                    return RedirectToAction("Index", "VoiceMessage", new { area = "", id = projectInstanceId, type = ItemType.ProjectInstance });
                case "Attachments":
                    return RedirectToAction("Index", "Attachments", new { area = "", id = projectInstanceId, type = ItemType.ProjectInstance });
                case "Tasks":
                    return RedirectToAction("Index", "TaskManager", new { area = "", id = projectInstanceId, type = ItemType.ProjectInstance });
                case "AddTask":
                    return RedirectToAction("CreateTask", "Task", new { area = "", id = projectInstanceId, parentType = TaskViewModel.ParentType.Project });
                case "EditTask":
                    return RedirectToAction("EditTask", "Task", new { area = "", id = Id });
                case "AddMeeting":
                    return RedirectToAction("CreateProjectIntroMeeting", "Meetings", new { area = "Home", id = projectInstanceId});
                case "EditMeeting":
                    return RedirectToAction("EditProjectIntroMeeting", "Meetings", new { area = "Home", id = Id });
                case "None":
                    return RedirectToAction("EditProjectCard", new { id = projectInstanceId });
            }

            AddAlert(AlertViewModel.AlertType.Success, successMessage);
            return Redirect(AppSession.Current.ProjectInstanceFormReturnUrl);
        }

        /// <summary>
        /// Check if a project instance has a meeting and tasks and show a warning if there are none or
        /// there are tasks which are not approved.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        private bool CheckTasksAndMeetings(ProjectInstanceViewModel viewModel)
        {
            var tasks = TaskService.GetAllByProjectInstanceId(viewModel.ID);
            var meeting = MeetingService.GetByProjectInstanceId(viewModel.ID);
            var bValid = true;
            var warning = new StringBuilder();

            warning.AppendLine("The project was approved with the following warnings:");

            if ((viewModel.LineManagerDecision.HasValue && viewModel.LineManagerDecision == ModerationDecision.Approved) ||
                (viewModel.HrManagerDecision.HasValue && viewModel.HrManagerDecision == ModerationDecision.Approved))
            {
                if (tasks != null)
                {
                    if (!tasks.Any())
                    {
                        bValid = false;
                        warning.AppendLine(" - No tasks are set");

                    }
                    else
                    {
                        var task = tasks.FirstOrDefault(t => !t.IsApproved());
                        if (task != null)
                        {
                            bValid = false;
                            warning.AppendLine(" - There are tasks which are not approved");
                        }
                    }
                }

                if (meeting == null)
                {
                    bValid = false;
                    warning.AppendLine(" - No meeting is set");
                }

                if (!bValid)           
                    AddAlert(AlertViewModel.AlertType.Warning, warning.ToString());
            }

            return bValid;
        }

        #endregion
    }
}
