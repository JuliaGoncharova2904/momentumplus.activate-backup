﻿using System;
using System.Linq;
using System.Web.Mvc;
using MomentumPlus.Activate.Web.Controllers;
using MomentumPlus.Activate.Web.Models;
using System.Collections.Generic;
using MomentumPlus.Activate.Services;
using MomentumPlus.Core.Models;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Core.DataSource;

namespace MomentumPlus.Activate.Web.Areas.Home.Controllers
{

    public class ExitSurveyController : BaseController
    {
        public ActionResult Index()
        {
            ExitSurveyService exitService = new ExitSurveyService(new MomentumContext());

            IEnumerable<ExitSurvey> exitSurveys = exitService.GetAll();

            var model = exitSurveys.OrderBy(e => e.Name).Select(e => new ExitSurveyViewModel
            {
                ExitSurveyName = e.Name,
                ExitSurveyId = e.ID
            });

            return View("~/Views/ExitSurvey/Survey/ExitSurveyList.cshtml", model);
        }

        public ActionResult CompleteStatus(Guid sectionId, Guid userId)
        {
            var service = new ExitSurveyService(new MomentumContext());
            var section = service.GetExitSurveySectionById(sectionId);
            var totalCount = section.Questions.Count();
            int answeredCount = 0;
            foreach (var item in section.Questions)
            {
                if (item.ExitSurveyAnswers.Any(a => a.UserId == userId))
                {
                    answeredCount++;
                }
            }
            string CompleteStatus = answeredCount + "/" + totalCount;
            return PartialView("~/Views/ExitSurvey/Shared/_AnsweredCount.cshtml", CompleteStatus);
        }

        public ActionResult MySurvey()
        {
            ExitSurveyService exitSurveyService = new ExitSurveyService(new MomentumContext());
            Package package = GetCurrentPackage();
            ExitSurvey exitSurvey = exitSurveyService.GetExitSurveyByPackageId(package.ID);

            if (exitSurvey != null)
            {

                var currentUser = GetCurrentUser();
                MyExitSurveyViewModel model = new MyExitSurveyViewModel
                {
                    UserId = package.UserOwnerId,
                    ExitSurveyId = exitSurvey.ID,
                    HasAccess = (currentUser.Role.RoleName == RoleAuthHelper.LineManager && package.SubmitExitSurvey == MomentumPlus.Core.Models.Activate.SubmitExitSurvey.HRManagerSubmit)
                                || (currentUser.Role.RoleName == RoleAuthHelper.HRManager && package.SubmitExitSurvey == MomentumPlus.Core.Models.Activate.SubmitExitSurvey.UserSubmit)
                                || (currentUser.Role.RoleName == RoleAuthHelper.HRManager && package.SubmitExitSurvey == MomentumPlus.Core.Models.Activate.SubmitExitSurvey.HRManagerSubmit)
                                || currentUser.ID == package.UserOwnerId,
                    ReadMode = currentUser.Role.RoleName == RoleAuthHelper.Administrator || currentUser.Role.RoleName == RoleAuthHelper.iHandoverAdmin || currentUser.Role.RoleName == RoleAuthHelper.Admin_General
                };

                return View("~/Views/ExitSurvey/Survey/MySurvey.cshtml", model);
            }

            return Redirect(AppSession.Current.PackageExamineReturnUrl);
        }

        [HttpGet]
        public ActionResult CreateSurvey()
        {
            return View("~/Views/ExitSurvey/Survey/CreateExitSurvey.cshtml", new ExitSurveyViewModel
            {
                ExitSurveyDescription = "Please take a moment to complete the Exit Survey. Your individual responses are completely confidential and will only be shared with your nominated HR representative.The information provided by completion of the survey helps us better understand our workforce and deliver an environment that brings out the very best in people."
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateSurvey(ExitSurveyViewModel model)
        {
            ModelState.Clear();

            ExitSurveyService exitSurveyService = new ExitSurveyService(new MomentumContext());

            IEnumerable<ExitSurveyQuestion> exitSurveyQuestions = exitSurveyService.GetAllQuestion();

            model.AllQuestions = exitSurveyQuestions.OrderBy(e => e.Name).Select(q => new SelectListItem { Text = q.Name, Value = q.ID.ToString() }).ToList();

            if (model.ExitSurveyId == null || model.ExitSurveyId == Guid.Empty)
            {
                ExitSurvey exitSurvey = new ExitSurvey();

                exitSurvey.Name = model.ExitSurveyName;
                exitSurvey.ID = Guid.NewGuid();
                exitSurvey.Description = model.ExitSurveyDescription;
                exitSurveyService.AddExitSurvey(exitSurvey);

                model.ExitSurveyId = exitSurvey.ID;
            }
            else
            {
                ExitSurvey exitSurvey = exitSurveyService.GetSurveyByID(model.ExitSurveyId);

                exitSurvey.Name = model.ExitSurveyName;
                exitSurveyService.UpdateExitSurvey(exitSurvey);

                if (model.Questions != null && model.Questions.Any())
                {
                    ExitSurveySection newSection = new ExitSurveySection
                    {
                        Questions =
                            model.Questions.Select(q => exitSurveyService.GetQuestionById(q)).ToList(),
                        ID = Guid.NewGuid(),
                        Name = model.SectionName,
                        Number = exitSurvey.Sections.Any() ? exitSurvey.Sections.Max(s => s.Number) + 1 : 1,
                        ExitSurveyId = exitSurvey.ID
                    };
                    List<ExitSurveyQuestion> questions = model.Questions.Select(q => exitSurveyService.GetQuestionById(q)).OrderBy(q => q.Name).ToList();

                    exitSurveyService.AddSection(newSection);
                    var i = 0;
                    foreach (var question in questions)
                    {
                        if (question.PositionJSON != null)
                        {
                            var positions = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PositionQuestionViewModel>>(question
                                .PositionJSON);
                            if (positions.Any(p => p.SectionId == newSection.ID))
                            {
                                var position = positions.Where(p => p.SectionId == newSection.ID && p.QuestionId == question.ID).FirstOrDefault();
                                positions.Remove(position);
                                positions.Add(new PositionQuestionViewModel
                                {
                                    SectionId = position.SectionId,
                                    QuestionId = position.QuestionId,
                                    Position = i
                                });
                                question.PositionJSON = Newtonsoft.Json.JsonConvert.SerializeObject(positions);
                                exitSurveyService.UpdateQuestion(question);
                            }
                            else
                            {
                                positions.Add(new PositionQuestionViewModel
                                {
                                    SectionId = newSection.ID,
                                    QuestionId = question.ID,
                                    Position = i
                                });
                                question.PositionJSON = Newtonsoft.Json.JsonConvert.SerializeObject(positions);
                                exitSurveyService.UpdateQuestion(question);
                            }
                        }
                        else
                        {
                            List<PositionQuestionViewModel> positions = new List<PositionQuestionViewModel>
                            {
                                new PositionQuestionViewModel
                                {
                                    SectionId = newSection.ID,
                                    QuestionId = question.ID,
                                    Position = i
                                }
                            };
                            question.PositionJSON = Newtonsoft.Json.JsonConvert.SerializeObject(positions);
                            exitSurveyService.UpdateQuestion(question);
                        }
                        i++;
                    }
                }
                model.Questions = new List<Guid>();
                model.ExitSurveyId = exitSurvey.ID;
                model.SectionName = "";
                model.Sections = exitSurveyService.GetSectionBySurveyId(model.ExitSurveyId).OrderBy(s => s.Number).Select(s => new ExitSurveySectionViewModel
                {
                    SectionId = s.ID,
                    AllQuestions = exitSurveyQuestions.Select(q => new SelectListItem { Text = q.Name, Value = q.ID.ToString() }),
                    Questions = s.Questions.Select(q => q.ID),
                    Number = s.Number,
                    Name = s.Name,
                    QuestionsPosition = s.Questions.Any()
                                ? s.Questions.Select(q => new ExitSurveyQuestionPositionViewModel
                                {
                                    QuestionId = q.ID,
                                    SectionId = s.ID,
                                    QuestionName = q.Name,
                                    Position = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PositionQuestionViewModel>>(q.PositionJSON).Any(p => p.SectionId == s.ID && p.QuestionId == q.ID)
                                                    ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<PositionQuestionViewModel>>(q.PositionJSON).Where(p => p.SectionId == s.ID && p.QuestionId == q.ID).FirstOrDefault().Position
                                                    : 0
                                }).OrderBy(q => q.Position).ToList()
                                : new List<ExitSurveyQuestionPositionViewModel>()
                }).ToList();
            }

            if (model.IsSave)
            {
                return RedirectToAction("Index", "ExitSurvey");
            }
            else
            {
                return View("~/Views/ExitSurvey/Survey/CreateExitSurvey.cshtml", model);
            }
        }

        [HttpGet]
        public ActionResult EditSurvey(Guid exitSurveyId)
        {
            ExitSurveyService exitSurveyService = new ExitSurveyService(new MomentumContext());

            ExitSurvey exitSurvey = exitSurveyService.GetSurveyByID(exitSurveyId);
            IEnumerable<ExitSurveyQuestion> exitSurveyQuestions = exitSurveyService.GetAllQuestion();

            ExitSurveyViewModel model = new ExitSurveyViewModel
            {
                ExitSurveyId = exitSurvey.ID,
                ExitSurveyName = exitSurvey.Name,
                ExitSurveyDescription = exitSurvey.Description != null
                                    ? exitSurvey.Description
                                    : "Please take a moment to complete the Exit Survey. Your individual responses are completely confidential and will only be shared with your nominated HR representative.The information provided by completion of the survey helps us better understand our workforce and deliver an environment that brings out the very best in people."
            };

            List<ExitSurveySection> sections = exitSurveyService.GetSectionBySurveyId(exitSurveyId).OrderBy(s => s.Number).ToList();

            List<ExitSurveySectionViewModel> sectionList = new List<ExitSurveySectionViewModel>();
            foreach (var section in sections)
            {
                ExitSurveySectionViewModel sectionItem = new ExitSurveySectionViewModel
                {
                    SectionId = section.ID,
                    Name = section.Name,
                    AllQuestions = exitSurveyQuestions.OrderBy(e => e.Name).Select(q => new SelectListItem { Text = q.Name, Value = q.ID.ToString() }),
                    Questions = section.Questions.Select(q => q.ID),
                    Number = section.Number,
                };

                List<ExitSurveyQuestionPositionViewModel> questionsPositionList = new List<ExitSurveyQuestionPositionViewModel>();
                int i = 0;
                foreach (var question in section.Questions)
                {
                    ExitSurveyQuestionPositionViewModel questionPositionItem = new ExitSurveyQuestionPositionViewModel
                    {
                        QuestionId = question.ID,
                        SectionId = section.ID,
                        QuestionName = question.Name,
                    };
                    if (question.PositionJSON != null)
                    {
                        var positions = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PositionQuestionViewModel>>(question.PositionJSON);
                        var position = positions.Where(p => p.SectionId == section.ID && p.QuestionId == question.ID).FirstOrDefault();
                        questionPositionItem.Position = position != null ? position.Position : i;
                    }
                    else
                    {
                        questionPositionItem.Position = i;
                    }
                    questionsPositionList.Add(questionPositionItem);
                    i++;
                }
                sectionItem.QuestionsPosition = questionsPositionList.OrderBy(p => p.Position).ToList();
                sectionList.Add(sectionItem);
            }

            model.Sections = sectionList;
            return View("~/Views/ExitSurvey/Survey/EditExitSurvey.cshtml", model);
        }

        [HttpPost]
        public ActionResult EditSurvey(ExitSurveyViewModel model)
        {
            ExitSurveyService exitSurveyService = new ExitSurveyService(new MomentumContext());

            IEnumerable<ExitSurveyQuestion> exitSurveyQuestions = exitSurveyService.GetAllQuestion();
            ExitSurvey exitSurvey = exitSurveyService.GetSurveyByID(model.ExitSurveyId);

            exitSurvey.Name = model.ExitSurveyName;
            exitSurvey.Description = model.ExitSurveyDescription;
            exitSurveyService.UpdateExitSurvey(exitSurvey);

            if (model.Questions != null && model.Questions.Any())
            {
                ExitSurveySection newSection = new ExitSurveySection
                {
                    Questions =
                        model.Questions.Select(q => exitSurveyService.GetQuestionById(q)).ToList(),
                    ID = Guid.NewGuid(),
                    Name = model.SectionName,
                    Number = exitSurvey.Sections.Any() ? exitSurvey.Sections.Max(s => s.Number) + 1 : 1,
                    ExitSurveyId = exitSurvey.ID
                };
                List<ExitSurveyQuestion> questions = model.Questions.Select(q => exitSurveyService.GetQuestionById(q)).OrderBy(q => q.Name).ToList();
                exitSurveyService.AddSection(newSection);
                var i = 0;
                foreach (var question in questions)
                {
                    if (question.PositionJSON != null)
                    {
                        var positions = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PositionQuestionViewModel>>(question
                            .PositionJSON);
                        if (positions.Any(p => p.SectionId == newSection.ID))
                        {
                            var position = positions.Where(p => p.SectionId == newSection.ID).FirstOrDefault();
                            positions.Remove(position);
                            positions.Add(new PositionQuestionViewModel
                            {
                                SectionId = position.SectionId,
                                QuestionId = position.QuestionId,
                                Position = i
                            });
                            question.PositionJSON = Newtonsoft.Json.JsonConvert.SerializeObject(positions);
                            exitSurveyService.UpdateQuestion(question);
                        }
                        else
                        {
                            positions.Add(new PositionQuestionViewModel
                            {
                                SectionId = newSection.ID,
                                QuestionId = question.ID,
                                Position = i
                            });
                            question.PositionJSON = Newtonsoft.Json.JsonConvert.SerializeObject(positions);
                            exitSurveyService.UpdateQuestion(question);
                        }
                    }
                    else
                    {
                        List<PositionQuestionViewModel> positions = new List<PositionQuestionViewModel>
                        {
                            new PositionQuestionViewModel
                            {
                                SectionId = newSection.ID,
                                QuestionId = question.ID,
                                Position = i
                            }
                        };
                        question.PositionJSON = Newtonsoft.Json.JsonConvert.SerializeObject(positions);
                        exitSurveyService.UpdateQuestion(question);
                    }
                    i++;
                }
            }
            model.Questions = new List<Guid>();
            model.ExitSurveyId = exitSurvey.ID;
            model.SectionName = "";
            model.Sections = exitSurveyService.GetSectionBySurveyId(model.ExitSurveyId).OrderBy(s => s.Number).Select(s => new ExitSurveySectionViewModel
            {
                SectionId = s.ID,
                AllQuestions = exitSurveyQuestions.OrderBy(e => e.Name).Select(q => new SelectListItem { Text = q.Name, Value = q.ID.ToString() }),
                Questions = s.Questions.Select(q => q.ID),
                Number = s.Number,
                Name = s.Name,
                QuestionsPosition = s.Questions.Select(q => new ExitSurveyQuestionPositionViewModel
                {
                    QuestionId = q.ID,
                    SectionId = s.ID,
                    QuestionName = q.Name,
                    Position = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PositionQuestionViewModel>>(q.PositionJSON).Any(p => p.SectionId == s.ID && p.QuestionId == q.ID)
                        ? Newtonsoft.Json.JsonConvert.DeserializeObject<List<PositionQuestionViewModel>>(q.PositionJSON).Where(p => p.SectionId == s.ID && p.QuestionId == q.ID).FirstOrDefault().Position
                        : 0
                }).OrderBy(q => q.Position).ToList()
            }).ToList();
            model.AllQuestions = exitSurveyQuestions.OrderBy(e => e.Name)
                .Select(q => new SelectListItem { Text = q.Name, Value = q.ID.ToString() }).ToList();
            if (model.IsSave)
            {
                return RedirectToAction("Index", "ExitSurvey");
            }
            else
            {
                return View("~/Views/ExitSurvey/Survey/EditExitSurvey.cshtml", model);
            }
        }

        public ActionResult SavePosition(string resultJSON)
        {
            List<PositionQuestionViewModel> positionsList = Newtonsoft.Json.JsonConvert
                .DeserializeObject<List<PositionQuestionViewModel>>(resultJSON);
            ExitSurveyService exitSurveyService = new ExitSurveyService(new MomentumContext());

            foreach (var position in positionsList)
            {
                ExitSurveyQuestion question = exitSurveyService.GetQuestionById(position.QuestionId);

                if (question.PositionJSON != null)
                {
                    List<PositionQuestionViewModel> positions =
                        Newtonsoft.Json.JsonConvert
                            .DeserializeObject<List<PositionQuestionViewModel>>(question.PositionJSON);
                    if (positions.Any(p => p.SectionId == position.SectionId))
                    {
                        var positionItem = positions.Where(p => p.SectionId == position.SectionId).FirstOrDefault();
                        positions.Remove(positionItem);
                        positions.Add(new PositionQuestionViewModel
                        {
                            SectionId = position.SectionId,
                            QuestionId = position.QuestionId,
                            Position = position.Position
                        });
                        question.PositionJSON = Newtonsoft.Json.JsonConvert.SerializeObject(positions);
                        exitSurveyService.UpdateQuestion(question);
                    }
                    else
                    {
                        positions.Add(new PositionQuestionViewModel
                        {
                            SectionId = position.SectionId,
                            QuestionId = position.QuestionId,
                            Position = position.Position
                        });
                        question.PositionJSON = Newtonsoft.Json.JsonConvert.SerializeObject(positions);
                        exitSurveyService.UpdateQuestion(question);
                    }
                }
                else
                {
                    List<PositionQuestionViewModel> positions = new List<PositionQuestionViewModel>
                    {
                        new PositionQuestionViewModel
                        {
                            SectionId = position.SectionId,
                            QuestionId = position.QuestionId,
                            Position = position.Position
                        }
                    };
                    question.PositionJSON = Newtonsoft.Json.JsonConvert.SerializeObject(positions);
                    exitSurveyService.UpdateQuestion(question);
                }
            }
            return Content("ok");
        }

        [HttpGet]
        public ActionResult ViewSurvey(Guid exitSurveyId, Guid userOwnerId)
        {
            ExitSurveyService exitSurveyService = new ExitSurveyService(new MomentumContext());
            ExitSurvey exitSurvey = exitSurveyService.GetSurveyByID(exitSurveyId);
            IEnumerable<ExitSurveySection> exitSurveySectios = exitSurveyService.GetSectionBySurveyId(exitSurveyId).OrderBy(s => s.Number).Where(s => s.ID != null);

            ExitSurveyForViewViewModel model = new ExitSurveyForViewViewModel
            {
                Description = exitSurvey.Description
            };
            List<ExitSurveySectionForViewViewModel> sections = new List<ExitSurveySectionForViewViewModel>();

            int totalQuestion = 0;
            int answeredQuestion = 0;
            foreach (var section in exitSurveySectios)
            {
                ExitSurveySectionForViewViewModel sectionViewModel = new ExitSurveySectionForViewViewModel
                {
                    SectionId = section.ID,
                    Name = section.Name,
                    Number = section.Number,
                    QuestionList = GetQuestionListForSection(section.ID, userOwnerId).OrderBy(q => q.Position).ToList()
                };
                sections.Add(sectionViewModel);
                totalQuestion += section.Questions.Count;

                foreach (var item in section.Questions)
                {
                    if (item.ExitSurveyAnswers.Any(a => a.UserId == userOwnerId && a.UserAnswerJSON != null))
                    {
                        answeredQuestion++;
                    }
                }
            }
            var currentUser = GetCurrentUser();

            model.Sections = sections;
            model.UserId = userOwnerId;
            model.IsOwnerSurvey = currentUser.ID == userOwnerId;
            model.IsHRManagerMode = currentUser.Role.RoleName == RoleAuthHelper.HRManager;
            model.IsLineManagerMode = currentUser.Role.RoleName == RoleAuthHelper.LineManager;
            model.SubmitType = PackageService.GetByUserOwnerId(userOwnerId).Where(p => p.ExitSurveyTemplateId.HasValue && p.ExitSurveyTemplate.ExitSurveyId == exitSurveyId).FirstOrDefault().SubmitExitSurvey.ToString();
            model.ExitSurveyId = exitSurvey.ID;
            model.CanSubmit = totalQuestion == answeredQuestion;
            model.ReadMode = currentUser.Role.RoleName == RoleAuthHelper.Administrator ||
                             currentUser.Role.RoleName == RoleAuthHelper.iHandoverAdmin ||
                             currentUser.Role.RoleName == RoleAuthHelper.Admin_General;
            return View("~/Views/ExitSurvey/Survey/ViewExitSurvey.cshtml", model);
        }

        [HttpPost]
        public ActionResult SaveAnswers(Guid userId, string answers, Guid exitSurveyId)
        {
            if (answers != null)
            {
                SaveExitSurvey(userId, answers);
            }
            ExitSurveyService exitSurveyService = new ExitSurveyService(new MomentumContext());

            IEnumerable<ExitSurveySection> exitSurveySectios = exitSurveyService.GetSectionBySurveyId(exitSurveyId).OrderBy(s => s.Number).Where(s => s.ID != null);
            ExitSurveySectionTemplate model = new ExitSurveySectionTemplate();

            List<ExitSurveySectionForViewViewModel> sections = new List<ExitSurveySectionForViewViewModel>();

            foreach (var section in exitSurveySectios)
            {
                ExitSurveySectionForViewViewModel sectionViewModel = new ExitSurveySectionForViewViewModel
                {
                    SectionId = section.ID,
                    Name = section.Name,
                    Number = section.Number,
                    QuestionList = GetQuestionListForSection(section.ID, userId).OrderBy(q => q.Position).ToList()
                };
                sections.Add(sectionViewModel);
            }
            var currentUser = GetCurrentUser();
            model.UserId = userId;
            model.IsOwnerSurvey = currentUser.ID == userId;
            model.IsHRManagerMode = currentUser.Role.RoleName == RoleAuthHelper.HRManager;
            model.IsLineManagerMode = currentUser.Role.RoleName == RoleAuthHelper.LineManager;
            model.Sections = sections;

            return PartialView("~/Views/ExitSurvey/Survey/_ExitSurveySectionTemplate.cshtml", model);
        }

        private void SaveExitSurvey(Guid userId, string answers)
        {
            ExitSurveyService exitSurveyService = new ExitSurveyService(new MomentumContext());

            List<AnswerResult> result = Newtonsoft.Json.JsonConvert.DeserializeObject<List<AnswerResult>>(answers);

            foreach (var answer in result)
            {
                ExitSurveySection section = exitSurveyService.GetExitSurveySectionById(answer.SectionId);
                ExitSurveyQuestion question = section.Questions.FirstOrDefault(q => q.ID == answer.QuestionId);

                ExitSurveyAnswer exitSurveyAnswer = question.ExitSurveyAnswers.Where(ea => ea.UserId == userId).FirstOrDefault();

                if (exitSurveyAnswer != null)
                {
                    exitSurveyAnswer.UserAnswerJSON = answer.Answer;
                    exitSurveyAnswer.UserNotes = answer.UserNotes;
                    exitSurveyAnswer.IsHRManagerApproved = answer.IsHRApproved;
                    exitSurveyAnswer.IsHRManagerApprovedUserNotes = answer.IsUsersNotesApproved;
                    exitSurveyAnswer.UserNotes = answer.UserNotes;
                    exitSurveyAnswer.HRManagerNotes = answer.HRComments;
                    exitSurveyService.UpdateExitSurveyAnswer(exitSurveyAnswer);
                }
                else
                {
                    ExitSurveyAnswer answerEntity = new ExitSurveyAnswer
                    {
                        ID = Guid.NewGuid(),
                        UserId = userId,
                        UserAnswerJSON = answer.Answer,
                        UserNotes = answer.UserNotes,
                        IsHRManagerApproved = answer.IsHRApproved,
                        IsHRManagerApprovedUserNotes = answer.IsUsersNotesApproved,
                        HRManagerNotes = answer.HRComments
                    };

                    question.ExitSurveyAnswers.Add(answerEntity);
                    exitSurveyService.UpdateQuestion(question);
                }
            }
        }

        [HttpPost]
        public ActionResult SubmitExitSurvey(Guid exitSurveyId, Guid userId, int submitType, string answers = "")
        {
            Package package = PackageService.GetByUserOwnerId(userId).Where(p => p.ExitSurveyTemplateId.HasValue && p.ExitSurveyTemplate.ExitSurveyId == exitSurveyId).FirstOrDefault();

            if (answers != null)
            {
                SaveExitSurvey(userId, answers);
            }

            if (package != null)
            {
                switch (submitType)
                {
                    case 0:
                        package.SubmitExitSurvey = MomentumPlus.Core.Models.Activate.SubmitExitSurvey.NotSubmit;
                        break;
                    case 1:
                        package.SubmitExitSurvey = MomentumPlus.Core.Models.Activate.SubmitExitSurvey.UserSubmit;
                        break;
                    case 2:
                        package.SubmitExitSurvey = MomentumPlus.Core.Models.Activate.SubmitExitSurvey.HRManagerSubmit;
                        break;
                    default:
                        package.SubmitExitSurvey = MomentumPlus.Core.Models.Activate.SubmitExitSurvey.NotSubmit;
                        break;
                }

                PackageService.Update(package);
            }
            return Content("ok");
        }

        public ActionResult ViewSurveyReport(Guid exitSurveyId)
        {
            Package package = GetCurrentPackage();
            Guid userId = package.UserOwnerId;
            var surveyService = new ExitSurveyService(new MomentumContext());
            ExitSurvey exitSurvey = surveyService.GetSurveyByID(exitSurveyId);
            IEnumerable<ExitSurveySection> exitSurveySectios = surveyService.GetSectionBySurveyId(exitSurveyId).OrderBy(s => s.Number).Where(s => s.ID != null);
            ExitSurveyForViewViewModel model = new ExitSurveyForViewViewModel
            {
                Description = exitSurvey.Description
            };
            List<ExitSurveySectionForViewViewModel> sections = new List<ExitSurveySectionForViewViewModel>();
            foreach (var section in exitSurveySectios)
            {
                ExitSurveySectionForViewViewModel sectionViewModel = new ExitSurveySectionForViewViewModel
                {
                    SectionId = section.ID,
                    Number = section.Number,
                    Name = section.Name,
                    QuestionList = GetQuestionListForSection(section.ID, userId).OrderBy(q => q.Position).ToList()
                };

                sections.Add(sectionViewModel);
            }
            model.Sections = sections;
            model.UserId = userId;
            model.ExitSurveyId = exitSurvey.ID;

            return PartialView("~/Views/ExitSurvey/Survey/ViewExitSurveyReport.cshtml", model);
        }

        private List<ExitSurveyQuestionForViewViewModel> GetQuestionListForSection(Guid sectionId, Guid userId)
        {
            var surveyService = new ExitSurveyService(new MomentumContext());
            var questions = surveyService.GetQuestionBySection(sectionId).OrderBy(q => q.Name).ToList();
            ExitSurveySectionReport sectionViewModel = new ExitSurveySectionReport
            {
                SectionId = sectionId
            };
            List<ExitSurveyQuestionForViewViewModel> listQuestionViewModel = new List<ExitSurveyQuestionForViewViewModel>();

            int i = 0;
            foreach (var question in questions)
            {
                ExitSurveyQuestionForViewViewModel questionViewModel = new ExitSurveyQuestionForViewViewModel();

                if (question.PositionJSON != null)
                {
                    List<PositionQuestionViewModel> positions = Newtonsoft.Json.JsonConvert
                        .DeserializeObject<List<PositionQuestionViewModel>>(question.PositionJSON);
                    var position = positions.Where(p => p.SectionId == sectionId && p.QuestionId == question.ID).FirstOrDefault();
                    questionViewModel.Position = position != null ? position.Position : i;
                }
                else
                {
                    questionViewModel.Position = i;
                }
                i++;
                ExitSurveyAnswer answerObject = question.ExitSurveyAnswers.FirstOrDefault(a => a.UserId == userId);
                switch (question.Type)
                {
                    case QuestionTypes.Multi:
                        questionViewModel.QuestionType = QuestionTypeEnum.Multi;
                        break;
                    case QuestionTypes.Radio:
                        questionViewModel.QuestionType = QuestionTypeEnum.Radio;
                        break;
                    case QuestionTypes.Text:
                        questionViewModel.QuestionType = QuestionTypeEnum.Text;
                        break;
                    case QuestionTypes.Rating:
                        questionViewModel.QuestionType = QuestionTypeEnum.Rating;
                        break;
                }
                if (question.DefaulfAnswer != null && question.DefaulfAnswer != "null" && answerObject == null)
                {
                    List<string> defaultAnswer = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(question.DefaulfAnswer);
                    List<Answer> answerForUser = new List<Answer>();
                    foreach (var answer in defaultAnswer)
                    {
                        answerForUser.Add(
                            new Answer
                            {
                                Name = answer,
                                Type = questionViewModel.QuestionType,
                                Value = false
                            });
                    }
                    questionViewModel.UserNotes = string.Empty;
                    questionViewModel.HRManagerNotes = string.Empty;
                    questionViewModel.IsHRCommentApproved = false;
                    questionViewModel.IsUsersNotesApproved = false;
                    questionViewModel.AnswerObject = answerForUser;
                }
                else if (question.DefaulfAnswer != null && question.DefaulfAnswer != "null" && answerObject != null && answerObject.UserAnswerJSON != null)
                {
                    List<Answer> userAnswer =
                        Newtonsoft.Json.JsonConvert.DeserializeObject<List<Answer>>(answerObject.UserAnswerJSON);
                    foreach (var answer in userAnswer)
                    {
                        answer.Type = questionViewModel.QuestionType;
                    }
                    questionViewModel.UserNotes = answerObject.UserNotes;
                    questionViewModel.HRManagerNotes = answerObject.HRManagerNotes;
                    questionViewModel.IsHRCommentApproved = answerObject.IsHRManagerApproved.HasValue ? answerObject.IsHRManagerApproved.Value : false;
                    questionViewModel.IsUsersNotesApproved = answerObject.IsHRManagerApprovedUserNotes.HasValue ? answerObject.IsHRManagerApprovedUserNotes.Value : false;
                    questionViewModel.AnswerObject = userAnswer;
                }
                else if (question.DefaulfAnswer == "null")
                {
                    List<Answer> answers = new List<Answer>();

                    if (answerObject != null)
                    {
                        answers =
                            Newtonsoft.Json.JsonConvert.DeserializeObject<List<Answer>>(answerObject.UserAnswerJSON);
                        questionViewModel.UserNotes = answerObject.UserNotes;
                        questionViewModel.HRManagerNotes = answerObject.HRManagerNotes;
                        questionViewModel.IsHRCommentApproved = answerObject.IsHRManagerApproved.HasValue ? answerObject.IsHRManagerApproved.Value : false;
                        questionViewModel.IsUsersNotesApproved = answerObject.IsHRManagerApprovedUserNotes.HasValue ? answerObject.IsHRManagerApprovedUserNotes.Value : false;
                    }
                    else
                    {
                        answers.Add(new Answer
                        {
                            Type = questionViewModel.QuestionType,
                            Name = question.Name,
                            TextAnswer = "",
                            Value = true
                        });
                        questionViewModel.UserNotes = string.Empty;
                        questionViewModel.HRManagerNotes = string.Empty;
                        questionViewModel.IsHRCommentApproved = false;
                        questionViewModel.IsUsersNotesApproved = false;
                    }
                    questionViewModel.AnswerObject = answers;
                }
                questionViewModel.Name = question.Name;
                questionViewModel.QuestionId = question.ID;
                questionViewModel.SectionId = sectionId;
                listQuestionViewModel.Add(questionViewModel);
            }
            sectionViewModel.Questions = listQuestionViewModel;
            ViewBag.sectionId = sectionId;
            return listQuestionViewModel;
        }
    }
}