﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MomentumPlus.Activate.Web.Controllers;
using MomentumPlus.Activate.Web.Areas.Home.Models;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Models.People;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Activate.Web.Helpers;
using AutoMapper;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Activate.Web.Models.Requests;
using MomentumPlus.Activate.Web.Core;
using MomentumPlus.Activate.Services;
using MomentumPlus.Activate.Web.Core.Home;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.People;
using MomentumPlus.Core.Models.Requests;

namespace MomentumPlus.Activate.Web.Areas.Home.Controllers
{
    public class PeopleController : BaseController
    {
        private PersonCore PersonCore;
        private RelationshipsCore RelationshipsCore;

        #region IOC

        private IContactService ContactService;
        private IPeopleService PeopleService;
        private IRequestService RequestService;
        private IProjectService ProjectService;

        public PeopleController(
            IAuditService auditService,
            IContactService contactService,
            IMasterListService masterListService,
            IPackageService packageService,
            IPeopleService peopleService,
            IRequestService requestService,
            IUserService userService,
            IProjectService projectService,
            PersonCore personCore
        )
            : base(
                auditService,
                masterListService,
                packageService,
                userService
            )
        {
            PersonCore = personCore;

            ContactService = contactService;
            PeopleService = peopleService;
            RequestService = requestService;
            ProjectService = projectService;
        }

        #endregion

        #region ACTIONS

        #region Index

        /// <summary>
        /// Render people index view
        /// </summary>
        /// <param name="searchTerm">Search query used to filter people</param>
        /// <param name="orderBy">Column to sort by</param>
        /// <param name="sortAscending">If true will sort ascending</param>
        /// <param name="pageNo">Number of the page to show</param>
        /// <param name="retired">If true will show only retired people</param>
        /// <returns></returns>
        //[NoBackFilterAttribute]
        public ActionResult Index(
            string searchTerm = "",
            Person.SortColumn orderBy = Person.SortColumn.FirstName,
            bool sortAscending = true,
            int pageNo = 1,
            bool retired = false
        )
        {
            var pageSize = 6;
            var package = GetCurrentPackage();
            var userPackageIds = PackageService.GetAllPackagesByUser(package.UserOwner).Select(p => p.ID);

            // Update last viewed date for people when a user views their own people
            var currentUser = GetCurrentUser();

            if (currentUser.ID == package.UserOwnerId)
            {
                package.PeopleLastViewed = DateTime.Now;
                PackageService.Update(package);
                SetCurrentPackage(package);
                AppSession.Current.CurrentUser = currentUser;
                AppSession.Current.ViewAsUser = currentUser;
            }


            var viewModel = new PeopleViewModel
            {
                SearchTerm = searchTerm,
                OrderBy = orderBy,
                SortAscending = sortAscending,
                PageNo = pageNo,
                PageSize = pageSize
            };

            // Perform search and update the view model
            var staffContacts = ContactService.SearchPeople(
                package.ID,
                searchTerm,
                pageNo,
                pageSize,
                orderBy,
                sortAscending,
                retired
            );

            List<Contact> contacts = new List<Contact>();
            foreach (var contact in staffContacts)
            {
                if (!userPackageIds.Any(i => i == contact.PackageId))
                {
                    contacts.Add(contact);
                }
            }
            viewModel.StaffContacts = contacts;

            foreach (var staff in viewModel.StaffContacts)
            {
                // Update indirect relationship description as appropriate for indirect relationships
                if (staff.IndirectRelationshipContactId.HasValue)
                    staff.IndirectRelationshipDescription = String.Format("Indirect via {0}",
                        ContactService.GetById(staff.IndirectRelationshipContactId.Value).FullName);
            }

            viewModel.TotalCount = contacts.Count; //ContactService.CountPeopleContacts(package.ID, searchTerm, retired);

            viewModel.DefaultCompanyId = MasterListService.GetDefaultCompanyId();

            AppSession.Current.StaffFormReturnUrl = Request.RawUrl;
            AppSession.Current.PackageFormReturnUrl = Request.RawUrl;
            AppSession.Current.PackageExamineReturnUrl = Request.RawUrl;

            return View("~/Areas/Home/Views/People/Index.cshtml", viewModel);
        }

        #endregion

        #region QuickAddPerson

        /// <summary>
        /// Render person quick add form
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult QuickAddPerson()
        {
            if (HttpContext.Request.UrlReferrer != null)
            {
                AppSession.Current.CancelRequestReturnUrl =
                    HttpContext.Request.UrlReferrer.AbsolutePath;
            }

            var viewModel = new QuickAddPersonViewModel();
            PopulateSelectPersonViewModel(viewModel);
            return View(viewModel);
        }

        /// <summary>
        /// Handle person quick add form submission
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult QuickAddPerson(QuickAddPersonViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                PopulateSelectPersonViewModel(viewModel);
                return View(viewModel);
            }
            Guid newPersonId = Guid.Empty;
            try
            {
                var currentPackage = GetCurrentPackage();
                var contact = ContactService.GetById(viewModel.ContactId.Value);

                if (contact.PackageId == currentPackage.ID)
                {
                    // Contact is from current package - set the management properties
                    contact.Managed = true;
                    contact.EmploymentStatus = viewModel.EmploymentStatus;
                    ContactService.Update(contact);
                    newPersonId = contact.ID;
                }
                else
                {
                    // Contact is from another package - clone and set package and management properties
                    var newContact = contact.DeepClone();
                    newContact.Package = null;
                    newContact.PackageId = currentPackage.ID;
                    newContact.Managed = true;
                    newContact.EmploymentStatus = viewModel.EmploymentStatus;

                    ContactService.Add(newContact);
                    newPersonId = newContact.ID;
                }

                AddAlert(AlertViewModel.AlertType.Success, SaveSuccessMessage);

                return RedirectToAction("Index", "People", new { area = "Home" });
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, SaveErrorMessage);
            }

            AppSession.Current.StaffFormReturnUrl = AppSession.Current.QuickAddPersonFormReturnUrl;

            return RedirectToAction("Edit", "Relationships", new { id = newPersonId });
        }

        #endregion

        #region CreatePerson

        /// <summary>
        /// Render create person form
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult CreatePerson()
        {
            var viewModel = new StaffViewModel();

            PopulateStaffViewModel(viewModel);

            return View("StaffForm", viewModel);
        }

        /// <summary>
        /// Handle create person form submission
        /// </summary>
        /// <param name="viewModel"></param>
        /// <param name="profileImage"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreatePerson(StaffViewModel viewModel, HttpPostedFileBase profileImage)
        {
            ValidateCompany(viewModel, ModelState);

            if (!ModelState.IsValid)
            {
                PopulateStaffViewModel(viewModel);
                return View("StaffForm", viewModel);
            }

            try
            {
                var staff = new Contact();
                UpdateStaffFields(staff, viewModel, profileImage);

                var packageId = GetCurrentPackageId();
                var contact = new Contact { PackageId = packageId, /*Person = staff*/ };
                ContactService.Add(contact);

                AuditService.RecordActivity<Contact>(AuditLog.EventType.Created, contact.ID);
                AuditService.RecordActivity<Staff>(AuditLog.EventType.Created, staff.ID);

                return GetStaffSuccessResult(staff.ID, contact.ID, SaveSuccessMessage);
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, SaveErrorMessage);
            }

            return Redirect(AppSession.Current.StaffFormReturnUrl);
        }

        #endregion

        #region EditPerson

        /// <summary>
        /// Render edit person form
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorise("Staff")]
        [HttpGet]
        public ActionResult EditPerson(Guid id)
        {
            var staff = PeopleService.GetStaff(id);
            var viewModel = Mapper.Map<StaffViewModel>(staff);
            PopulateStaffViewModel(viewModel, staff);

            AppSession.Current.ContactFormReturnUrl = Request.RawUrl;
            AppSession.Current.AttachmentManagerReturnUrl = Request.RawUrl;
            AppSession.Current.VoiceMessageManagerReturnUrl = Request.RawUrl;
            AppSession.Current.PackageExamineReturnUrl = Request.RawUrl;

            var package = PackageService.GetByStaffId(staff.ID);
            if (package != null)
            {
                // Set moderation properties on view model
                SetModeratedViewModelFields(staff, viewModel, package);
                viewModel.ShowLineManagerSection = true;
                viewModel.ShowHrManagerSection = true;
            }
            viewModel.DisablePersonFields = true;
            AuditService.RecordActivity<Staff>(AuditLog.EventType.Viewed, staff.ID);

            return View("StaffForm", viewModel);
        }

        /// <summary>
        /// Handle edit person form submission
        /// </summary>
        /// <param name="viewModel"></param>
        /// <param name="profileImage"></param>
        /// <returns></returns>
        [Authorise("Staff")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditPerson(StaffViewModel viewModel, HttpPostedFileBase profileImage)
        {

            var staff = PeopleService.GetStaff(viewModel.ID);
            var package = PackageService.GetByStaffId(staff.ID);

            ValidateCompany(viewModel, ModelState);

            // If model state is invalid re-populate model and re-render view
            if (!ModelState.IsValid)
            {
                PopulateStaffViewModel(viewModel, staff);
                if (package != null)
                {
                    // Set moderation properties on view model
                    SetModeratedViewModelFields(staff, viewModel, package);
                    viewModel.ShowLineManagerSection = true;
                    viewModel.ShowHrManagerSection = true;
                }

                return View("StaffForm", viewModel);
            }

            try
            {
                // Update properties on the database entity
                UpdateStaffFields(staff, viewModel, profileImage);
                UpdateModerated(staff, viewModel, package);

                AuditService.RecordActivity<Staff>(AuditLog.EventType.Modified, staff.ID);

                var contact = ContactService.GetById(staff.ID);
                return GetStaffSuccessResult(staff.ID, contact.ID, SaveSuccessMessage);
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, SaveErrorMessage);
            }

            return Redirect(AppSession.Current.StaffFormReturnUrl);
        }

        /// <summary>
        /// Validate the company passed on the given view model and update the model state with any
        /// error messages.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="modelState"></param>
        private void ValidateCompany(StaffViewModel model, ModelStateDictionary modelState)
        {
            if (model.OtherCompany && string.IsNullOrWhiteSpace(model.OtherCompanyName))
            {
                modelState.AddModelError("OtherCompanyName", "The Company Name * field is required.");
            }
            else if (!model.OtherCompany && model.CompanyId == null && !modelState["CompanyId"].Errors.Any())
            {
                modelState.AddModelError("CompanyId", "The Company * field is required.");
            }
            if (!model.OtherCompany)
            {
                model.OtherCompanyName = null;
            }
        }

        #endregion

        #region Retire/Resume

        /// <summary>
        /// Retire the person with the given ID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="returnToUrl"></param>
        /// <param name="retireRelationship"></param>
        /// <returns></returns>
        [Authorise("Staff")]
        [HttpGet]
        public ActionResult RetirePerson(Guid id, bool returnToUrl = false, bool retireRelationship = false)
        {
            try
            {
                var staff = PeopleService.GetStaff(id);
                ContactService.Retire(staff.ID);
                AddAlert(AlertViewModel.AlertType.Success, "Contact retired successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Something went wrong, please try again.");
            }

            if (returnToUrl)
            {
                return Redirect(AppSession.Current.ContactFormReturnUrl);
            }

            return RedirectToAction("EditPerson", new { id = id });
        }

        /// <summary>
        /// Resume a person with the given ID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="returnToUrl"></param>
        /// <returns></returns>
        [Authorise("Staff")]
        [HttpGet]
        public ActionResult ResumePerson(Guid id, bool returnToUrl = false)
        {
            try
            {
                var staff = PeopleService.GetStaff(id);
                ContactService.Restore(staff.ID);
                AddAlert(AlertViewModel.AlertType.Success, "Contact resumed successfully.");
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, "Something went wrong, please try again.");
            }

            if (returnToUrl)
            {
                return Redirect(AppSession.Current.ContactFormReturnUrl);
            }

            return RedirectToAction("EditPerson", new { id = id });
        }

        #endregion

        #region TransferPerson

        /// <summary>
        /// Transfer a person from one package to another
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult TransferPerson(StaffTransferRequestViewModel viewModel)
        {
            var contact = ContactService.GetById(viewModel.ContactId.Value);

            if (!ModelState.IsValid)
            {
                viewModel.Contact = contact;
                viewModel.Package = GetCurrentPackage();

                viewModel.Packages = PackageService.GetAll().Where(p => p.ID != contact.PackageId && p.PeopleManagementEnabled).OrderBy(p => p.DisplayName).Select(p =>
                    new SelectListItem { Text = p.DisplayName, Value = p.ID.ToString() }
                );

                return View("StaffTransferForm", viewModel);
            }

            try
            {
                var currentPackage = GetCurrentPackage();

                var currentUser = GetCurrentUser();
                // Auto-approve transfers when the current user is admin/manager
                if (currentUser.Role.RoleParsed == Role.RoleEnum.iHandoverAdmin ||
                    currentUser.Role.RoleParsed == Role.RoleEnum.Administrator ||
                    currentUser.ID == contact.Package.UserHRManagerId ||
                    currentUser.ID == contact.Package.UserLineManagerId)
                {
                    var DestinationContact = contact.DeepClone();
                    DestinationContact.Package = null;
                    DestinationContact.PackageId = viewModel.DestinationId;

                    ContactService.Add(DestinationContact);
                }
                else
                {
                    var transferRequest = new StaffTransferRequest
                    {
                        PackageId = currentPackage.ID,
                        TransferSourceId = currentPackage.ID,
                        DestinationId = viewModel.DestinationId,
                        ContactId = contact.ID
                    };

                    RequestService.AddRequest(transferRequest);
                }

                AddAlert(AlertViewModel.AlertType.Success, SaveSuccessMessage);
            }
            catch (Exception ex)
            {
                LogException(ex);
                AddAlert(AlertViewModel.AlertType.Danger, SaveErrorMessage);
            }

            return Redirect(AppSession.Current.StaffTransferFormReturnUrl);
        }

        #endregion

        #region GetUserDetails

        /// <summary>
        /// Get basic details for user with the given ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetUserDetails(Guid id)
        {
            var user = UserService.GetByUserId(id);

            return Json(new
            {
                Title = user.Title,
                FirstName = user.FirstName,
                LastName = user.LastName,
                ImageId = user.ImageId,
                CompanyId = user.CompanyId,
                Position = user.Position,
                Email = user.Username,
                Phone = user.Phone,
                SearchTags = user.SearchTags
            }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region PeopleStats

        /// <summary>
        /// Render people stats view for the current package
        /// </summary>
        /// <returns></returns>
        [ChildActionOnly]
        public ActionResult PeopleStats()
        {
            Guid? packageId = GetCurrentPackageId();

            if (packageId.HasValue)
            {
                int peopleCount = ContactService.GetStaffContacts(packageId.Value).Count();

                var model = new PeopleStatsViewModel()
                {
                    PeopleCount = peopleCount
                };

                return View(model);
            }

            return new EmptyResult();
        }

        #endregion

        #endregion

        #region HELPERS

        /// <summary>
        /// Populate the quick add person view model with contacts from the current package
        /// </summary>
        /// <param name="viewModel"></param>
        private void PopulateSelectPersonViewModel(QuickAddPersonViewModel viewModel)
        {
            var currentPackageId = GetCurrentPackageId();

            // Contacts in current package
            viewModel.Contacts = ContactService.GetByPackageId(currentPackageId.Value)
                .Where(c => c.Company != null && !string.IsNullOrEmpty(c.Title) && !string.IsNullOrEmpty(c.Position))
                .Select(c =>
                    new SelectListItem
                    {
                        Text = (c.Company != null ? c.Company.Name + " - " : "") + c.Name,
                        Value = c.ID.ToString()
                    }
                )
                .OrderBy(i => i.Text)
                .ToList();
        }

        /// <summary>
        /// Populate a staff view model with the properties required for rendering the form
        /// </summary>
        /// <param name="viewModel"></param>
        /// <param name="staff"></param>
        private void PopulateStaffViewModel(StaffViewModel viewModel, Contact staff = null)
        {
            PersonCore.PopulatePersonViewModel(viewModel);

            // Create select list with all users
            viewModel.Users = UserService.GetAll().OrderBy(u => u.LastName).Select(u =>
                new SelectListItem { Text = u.LastName + ", " + u.FirstName, Value = u.ID.ToString() }
            );

            // Show "Go To Package" button only for authorised roles
            viewModel.ShowGoToPackage = AppSession.Current.CurrentUser.Role != null &&
                (AppSession.Current.CurrentUser.Role.RoleParsed == Role.RoleEnum.LineManager ||
                 AppSession.Current.CurrentUser.Role.RoleParsed == Role.RoleEnum.HRManager ||
                 AppSession.Current.CurrentUser.Role.RoleParsed == Role.RoleEnum.Administrator ||
                 AppSession.Current.CurrentUser.Role.RoleParsed == Role.RoleEnum.iHandoverAgent ||
                 AppSession.Current.CurrentUser.Role.RoleParsed == Role.RoleEnum.iHandoverAdmin);

            // Set staff and user properties
            if (staff != null)
            {
                viewModel.NotesSubjectivity = staff.CountSubjectivity(MasterListService);
                viewModel.NotesWordCount = staff.CountWords();
                viewModel.ReadyForReview = staff.ReadyForReview;

                if (staff.User != null)
                {
                    viewModel.Title = staff.User.Title;
                    viewModel.FirstName = staff.User.FirstName;
                    viewModel.LastName = staff.User.LastName;
                    viewModel.ImageId = staff.User.ImageId;
                    viewModel.CompanyId = staff.User.CompanyId;
                    viewModel.Position = staff.User.Position;
                    viewModel.Email = staff.User.Email;
                    viewModel.Phone = staff.User.Phone;
                    viewModel.DisablePersonFields = true;
                }
            }
            else if (viewModel.UserId.HasValue)
            {
                // Populate user properties based on user ID on view model
                var user = UserService.GetByUserId(viewModel.UserId.Value);
                viewModel.Title = user.Title;
                viewModel.FirstName = user.FirstName;
                viewModel.LastName = user.LastName;
                viewModel.ImageId = user.ImageId;
                viewModel.CompanyId = user.CompanyId;
                viewModel.Position = user.Position;
                viewModel.Email = user.Email;
                viewModel.Phone = user.Phone;
                viewModel.DisablePersonFields = true;
            }
        }

        /// <summary>
        /// Update the fields on the staff entity from the corresponding properties on the view model
        /// </summary>
        /// <param name="staff"></param>
        /// <param name="viewModel"></param>
        /// <param name="profileImage"></param>
        private void UpdateStaffFields(Contact staff, StaffViewModel viewModel, HttpPostedFileBase profileImage)
        {
            PersonCore.UpdatePersonFields(staff.User, viewModel, profileImage);

            staff.ID = viewModel.UserId.Value;
            staff.SearchTags = viewModel.SearchTags;
            staff.EmploymentStatus = viewModel.EmploymentStatus;
            staff.FutureTraining = viewModel.FutureTraining;
            staff.ManagementNotes = viewModel.ManagementNotes;

            staff.ReadyForReview = viewModel.ReadyForReview;
        }

        /// <summary>
        /// Get an appropriate action result for a successful submission of the person form
        /// </summary>
        /// <param name="staffId"></param>
        /// <param name="contactId"></param>
        /// <param name="successMessage"></param>
        /// <returns></returns>
        private ActionResult GetStaffSuccessResult(Guid staffId, Guid contactId, string successMessage)
        {
            // Redirect to a specific action depending on the submit button pressed
            switch (Request.Form["Redirect"])
            {
                case "VoiceMessages":
                    return RedirectToAction("Index", "VoiceMessage", new { area = "", id = staffId, type = ItemType.Staff });
                case "Attachments":
                    return RedirectToAction("Index", "Attachments", new { area = "", id = staffId, type = ItemType.Staff });
                case "Relationship":
                    return RedirectToAction("Edit", "Relationships", new { id = contactId });
            }

            AddAlert(AlertViewModel.AlertType.Success, successMessage);
            return Redirect(AppSession.Current.StaffFormReturnUrl);
        }

        #endregion
    }
}
