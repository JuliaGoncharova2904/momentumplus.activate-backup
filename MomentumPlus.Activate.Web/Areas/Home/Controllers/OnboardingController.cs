﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MomentumPlus.Activate.Services;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Areas.Home.Models;
using MomentumPlus.Activate.Web.Controllers;
using MomentumPlus.Activate.Web.Core;
using MomentumPlus.Activate.Web.Core.Home;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models.Home;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Areas.Home.Controllers
{
    /// <summary>
    /// Controller for the Onboarding module.
    /// </summary>
    public class OnboardingController : TaskController
    {
        #region CONSTANTS

        /// <summary>
        /// Number of tasks to display in the Onboarding Tasks list.
        /// </summary>
        private const int TasksPageSize = int.MaxValue;

        /// <summary>
        /// Number of topics to display in the Onboarding Tasks list.
        /// </summary>
        private const int TopicsPageSize = int.MaxValue;

        /// <summary>
        /// Number of meetings to display in the Onboarding Meetings list.
        /// </summary>
        private const int MeetingsPageSize = int.MaxValue;

        #endregion

        #region IOC

        private IHandoverService HandoverService;

        private OnboardingCore Onboarding;

        public OnboardingController(
            IAuditService auditService,
            IHandoverService handoverService,
            IMasterListService masterListService,
            IMeetingService meetingService,
            IPackageService packageService,
            ITaskService taskService,
            ITopicService topicService,
            ITopicGroupService topicGroupService,
            ITopicQuestionService topicQuestionService,
            IUserService userService,
            IWorkplaceService workplaceService,
            IProLinkService proLink,
            IProjectService projectService,
            IContactService contactService,
            OnboardingCore onboardingCore,
            TaskCore taskCore,
            IPositionService positionService,
            TopicCore topicCore
        )
            : base(
                  auditService,
                  handoverService,
                  masterListService,
                  packageService,
                  taskService,
                  topicService,
                  topicGroupService,
                  topicQuestionService,
                  userService,
                  workplaceService,
                  proLink,
                  projectService,
                  contactService,
                  taskCore,
                  positionService,
                  topicCore
              )
        {
            Onboarding = onboardingCore;

            HandoverService = handoverService;
        }

        #endregion

        #region ACTIONS

        [HttpGet]
        public ActionResult Index()
        {
            return RedirectToAction("Tasks");
        }

        /// <summary>
        /// Render the Tasks view.
        /// </summary>
        [HttpGet]
        public ActionResult Tasks(
            int weekNo = 1,
            int pageNo = 1,
            OnboardingTaskItemViewModel.SortColumn sortColumn = OnboardingTaskItemViewModel.SortColumn.NameTask,
            bool sortAscending = true,
            Guid? ancestor = null
        )
        {
            var currentPackage = GetCurrentPackage();
            var ancestorPackageId = currentPackage.AncestorPackageId;

            OnboardingTasksViewModel onboardingTasks = new OnboardingTasksViewModel { };

            if (ancestorPackageId.HasValue)
            {
                // Get the onboarding tasks from the ancestor package
                onboardingTasks = Onboarding.GetTasks(ancestorPackageId.Value, pageNo, TasksPageSize, sortColumn, sortAscending);
            }

            // Get the total number of weeks to show on the view from the onboarding period
            var onboardingPeriod = HandoverService.GetOnboardingPeriodForPackage(currentPackage.ID);
            var duration = onboardingPeriod.Duration.Value;
            onboardingTasks.TotalWeeks = duration;

            // Get the expanded weeks from cookies, used to show/hide certain weeks in the view
            var expandedWeeksCookie = Request.Cookies[String.Format("{0}-expandedWeeks", currentPackage.ID)];
            onboardingTasks.ExpandedWeeks = new List<int>();

            if (expandedWeeksCookie != null)
            {
                if (!string.IsNullOrWhiteSpace(expandedWeeksCookie.Value))
                {
                    onboardingTasks.ExpandedWeeks.AddRange(expandedWeeksCookie.Value.Split(',').Select(x => int.Parse(x)));
                }
            }
            else
            {
                onboardingTasks.ExpandedWeeks.Add(0);
                Response.SetCookie(new HttpCookie(String.Format("{0}-expandedWeeks", currentPackage.ID), string.Join(",", onboardingTasks.ExpandedWeeks)));
            }

            AppSession.Current.TaskFormReturnUrl = Request.RawUrl;
            AppSession.Current.AttachmentManagerReturnUrl = Request.RawUrl;
            AppSession.Current.VoiceMessageManagerReturnUrl = Request.RawUrl;

            onboardingTasks.PackageId = currentPackage.ID;

            return View(onboardingTasks);
        }

        /// <summary>
        /// Render the Topics view.
        /// </summary>
        [HttpGet]
        public ActionResult Topics(
            int criticalPageNo = 1,
            int pageSize = 8,
            OnboardingTopicItemViewModel.SortColumn criticalSortColumn = OnboardingTopicItemViewModel.SortColumn.NameTopic,
            bool criticalSortAscending = true,
            int experiencesPageNo = 1,
            OnboardingTopicItemViewModel.SortColumn experiencesSortColumn = OnboardingTopicItemViewModel.SortColumn.NameTopic,
            bool experiencesSortAscending = true
        )
        {
            var currentPackage = GetCurrentPackage();
            var ancestorPackageId = GetAncestorPackageId();
            OnboardingTopicsViewModel onboardingTopics = new OnboardingTopicsViewModel { };

            if (ancestorPackageId.HasValue)
            {
                // Get the onboarding topics from the ancestor package
                onboardingTopics = Onboarding.GetTopicsByModule(ancestorPackageId.Value, criticalPageNo, pageSize, criticalSortColumn, criticalSortAscending, experiencesPageNo, experiencesSortColumn, experiencesSortAscending);
            }
            else
            {
                onboardingTopics = new OnboardingTopicsViewModel { };
            }

            // Get the expanded weeks from cookies, used to show/hide certain weeks in the view
            var expandedTopicsCookie = Request.Cookies[String.Format("{0}-expandedTopics", currentPackage.ID)];
            onboardingTopics.ExpandedTopics = new List<int>();

            if (expandedTopicsCookie != null)
            {
                if (!string.IsNullOrWhiteSpace(expandedTopicsCookie.Value))
                {
                    onboardingTopics.ExpandedTopics.AddRange(expandedTopicsCookie.Value.Split(',').Select(x => int.Parse(x)));
                }
            }
            else
            {
                onboardingTopics.ExpandedTopics.Add(0);
                Response.SetCookie(new HttpCookie(String.Format("{0}-expandedTopics", currentPackage.ID), string.Join(",", onboardingTopics.ExpandedTopics)));
            }

            onboardingTopics.PackageId = currentPackage.ID;

            AppSession.Current.TopicFormReturnUrl = Request.RawUrl;
            AppSession.Current.AttachmentManagerReturnUrl = Request.RawUrl;
            AppSession.Current.VoiceMessageManagerReturnUrl = Request.RawUrl;

            return View(onboardingTopics);
        }

        /// <summary>
        /// Render the Meetings view.
        /// </summary>
        [HttpGet]
        public ActionResult Meetings(
            int pageNo = 1,
            Meeting.SortColumn sortColumn = Meeting.SortColumn.Start,
            bool sortAscending = true
        )
        {
            Guid? sessionPackageId = GetCurrentPackageId();

            RelationshipsViewModel onboardingMeetings = Onboarding.GetMeetings(
                sessionPackageId.Value, pageNo, MeetingsPageSize, sortColumn, sortAscending);

            RelationshipsMeetingsViewModel meetingsViewModel = new RelationshipsMeetingsViewModel
            {
                Meetings = onboardingMeetings,
                SortAscending = sortAscending
            };

            AppSession.Current.MeetingFormReturnUrl = Request.RawUrl;

            return View(meetingsViewModel);
        }

        #endregion

        #region HELPERS

        /// <summary>
        /// Get the ID of the ancestor of the current package
        /// </summary>
        /// <returns></returns>
        private Guid? GetAncestorPackageId()
        {
            var sessionPackageId = GetCurrentPackageId();
            var package = PackageService.GetByPackageId(sessionPackageId.Value);
            return package.AncestorPackageId;
        }

        private IEnumerable<SelectListItem> GetAncestorSelect()
        {
            var sessionPackageId = GetCurrentPackageId();
            var package = PackageService.GetByPackageId(sessionPackageId.Value);
            return PackageService.GetAncestorPackages(package).Select(p =>
                new SelectListItem
                {
                    Text = package.DisplayName,
                    Value = package.ID.ToString()
                });
        }

        private string GetParentName(Task task)
        {
            if (task.ParentTopic != null)
            {
                return task.ParentTopic.ParentTopicGroup.Title;
            }
            if (task.ProjectInstace != null)
            {
                return task.ProjectInstace.Project.Name;
            }
            if (task.Contact != null)
            {
                return task.Contact.Name;
            }

            return "-";
        }

        private string GetTaskModuleName(Task task)
        {
            if (task.ParentTopic != null)
            {
                return task.ParentTopic.ParentTopicGroup.ParentModule.Type.ToString();
            }

            if (task.ProjectInstace != null)
            {
                return "Projects";
            }

            if (task.Contact != null)
            {
                return "Relationships";
            }

            return "-";
        }

        #endregion
    }
}
