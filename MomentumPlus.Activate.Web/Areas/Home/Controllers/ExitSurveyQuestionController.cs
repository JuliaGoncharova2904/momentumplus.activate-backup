﻿using System;
using System.Web.Mvc;
using MomentumPlus.Activate.Web.Controllers;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Core.Models;
using MomentumPlus.Activate.Services;
using System.Net;
using MomentumPlus.Core.DataSource;

namespace MomentumPlus.Activate.Web.Areas.Home.Controllers
{
    [Authorise(Roles = RoleAuthHelper.ABSOLUTE_GENERAL_ACCESS)]
    public class ExitSurveyQuestionController : BaseController
    {
        public ActionResult Index()
        {
            ExitSurveyService exitSurveyService = new ExitSurveyService(new MomentumContext());
            IEnumerable<ExitSurveyQuestion> exitSurveyQuestions = exitSurveyService.GetAllQuestion();

            List<ExitSurveyQuestionViewModel> model =
                exitSurveyQuestions.OrderBy(q => q.Name).Select(q => new ExitSurveyQuestionViewModel
                {
                    Name = q.Name,
                    Type = q.Type.ToString(),
                    QuestionId = q.ID
                }).ToList();

            return View("~/Views/ExitSurvey/Question/QuestionList.cshtml", model);
        }


        [HttpGet]
        public ActionResult CreateQuestion()
        {
            AppSession.Current.ExitSurveyQuestionFromReturnUrl = Request.RawUrl;
            return View("~/Views/ExitSurvey/Question/CreateQuestionForm.cshtml");
        }

        [HttpPost]
        public ActionResult CreateQuestion(string Name, int TypeQuestion, List<string> Answers)
        {
            ExitSurveyQuestion questionModel = new ExitSurveyQuestion();
            var MContext = new ExitSurveyService(new MomentumContext());
            questionModel.DefaulfAnswer = Newtonsoft.Json.JsonConvert.SerializeObject(Answers);
            questionModel.Name = Name;
            switch (TypeQuestion)
            {
                case 0:
                    questionModel.Type = QuestionTypes.Radio;
                    break;
                case 1:
                    questionModel.Type = QuestionTypes.Multi;
                    break;
                case 2:
                    questionModel.Type = QuestionTypes.Text;
                    break;
                case 3:
                    questionModel.Type = QuestionTypes.Rating;
                    List<string> ans = new List<string> { "1", "2", "3", "4", "5"};
                    questionModel.DefaulfAnswer = Newtonsoft.Json.JsonConvert.SerializeObject(ans);
                    break;
                default:
                    questionModel.Type = QuestionTypes.Radio;
                    break;
            }
            questionModel.ID = Guid.NewGuid();
            MContext.AddQuestion(questionModel);

            return RedirectToAction("Index", "ExitSurveyQuestion");
        }

        [HttpGet]
        public ActionResult ViewQuestion(Guid questionId)
        {
            ExitSurveyService exitSurveyService = new ExitSurveyService(new MomentumContext());

            ExitSurveyQuestion exitSurveyQuestion = exitSurveyService.GetQuestionById(questionId);
            if (exitSurveyQuestion != null)
            {
                ExitSurveyQuestionForViewViewModel model = new ExitSurveyQuestionForViewViewModel();
                if (exitSurveyQuestion.DefaulfAnswer != null)
                {
                    List<string> answerList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(exitSurveyQuestion.DefaulfAnswer);
                    model.Answers = answerList;
                    model.QuestionId = exitSurveyQuestion.ID;
                    switch (exitSurveyQuestion.Type)
                    {
                        case QuestionTypes.Multi:
                            model.QuestionType = QuestionTypeEnum.Multi;
                            break;
                        case QuestionTypes.Radio:
                            model.QuestionType = QuestionTypeEnum.Radio;
                            break;
                        case QuestionTypes.Text:
                            model.QuestionType = QuestionTypeEnum.Text;
                            break;
                        case QuestionTypes.Rating:
                            model.QuestionType = QuestionTypeEnum.Rating;
                            break;
                    }
                    model.Name = exitSurveyQuestion.Name;
                }
                else
                {
                    model.QuestionId = exitSurveyQuestion.ID;
                    model.Name = exitSurveyQuestion.Name;
                }

                return View("~/Views/ExitSurvey/Question/ViewQuestionForm.cshtml", model);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

        }

        public ActionResult GetAnswerView(int count)
        {
            return PartialView("~/Views/ExitSurvey/Question/AnswerTemplate.cshtml", count);
        }
    }
}