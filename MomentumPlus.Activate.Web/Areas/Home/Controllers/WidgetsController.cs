﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Areas.Home.Models;
using MomentumPlus.Activate.Web.Controllers;
using MomentumPlus.Activate.Web.Core;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.People;

namespace MomentumPlus.Activate.Web.Areas.Home.Controllers
{
    public class WidgetsController : BaseController
    {
        private readonly IAttachmentService AttachmentService;
        private readonly IContactService ContactService;
        private readonly IDocumentLibraryService DocumentLibraryService;
        private readonly ProjectCore ProjectCore;
        private readonly IProjectService ProjectService;

        public WidgetsController(
            IAttachmentService attachmentService,
            IAuditService auditService,
            IContactService contactService,
            IDocumentLibraryService documentLibraryService,
            IImageService imageService,
            IMasterListService masterListService,
            IModerationService moderationService,
            IPackageService packageService,
            IProjectService projectService,
            IRequestService requestService,
            IUserService userService,
            ProjectCore projectCore)
            : base(
                auditService,
                masterListService,
                packageService,
                userService
                )
        {
            AttachmentService = attachmentService;
            ContactService = contactService;
            DocumentLibraryService = documentLibraryService;
            ProjectService = projectService;

            ProjectCore = projectCore;
        }

        [ChildActionOnly]
        public ActionResult MyAttachments(WidgetViewModel viewModel)
        {
            var attachments = AttachmentService.GetRecentByPackage(viewModel.PackageId);
            var documents = DocumentLibraryService.GetRecentByPackageId(viewModel.PackageId);

            viewModel.Attachments = attachments.Concat(documents).OrderByDescending(m => m.Created).Take(9);

            ViewData["CurrentPackage"] = GetCurrentPackage();

            return View(viewModel);
        }

        [ChildActionOnly]
        public ActionResult MyTeam(WidgetViewModel viewModel)
        {
            viewModel.StaffContacts = ContactService.GetTeamContacts(viewModel.PackageId, Person.SortColumn.FirstName);
            viewModel.DefaultCompanyId = MasterListService.GetDefaultCompanyId();
            viewModel.ShowGoToPackage = AppSession.Current.CurrentUser.Role != null &&
                                        (AppSession.Current.CurrentUser.Role.RoleParsed == Role.RoleEnum.LineManager ||
                                         AppSession.Current.CurrentUser.Role.RoleParsed == Role.RoleEnum.HRManager ||
                                         AppSession.Current.CurrentUser.Role.RoleParsed == Role.RoleEnum.Administrator ||
                                         AppSession.Current.CurrentUser.Role.RoleParsed == Role.RoleEnum.iHandoverAgent ||
                                         AppSession.Current.CurrentUser.Role.RoleParsed == Role.RoleEnum.iHandoverAdmin);
            return View(viewModel);
        }

        [ChildActionOnly]
        public ActionResult MyProjects(WidgetViewModel viewModel)
        {
            var projectInstances = ProjectService.GetProjectInstances(viewModel.PackageId, null,
                ProjectInstance.SortColumn.Name, true, approved: null);
            viewModel.ProjectInstances = Mapper.Map<IEnumerable<ProjectInstance>,
                IEnumerable<ProjectInstanceViewModel>>(projectInstances);

            foreach (var project in viewModel.ProjectInstances)
            {
                if (project.ProjectId == null && project.NewProjectRequest != null)
                {
                    project.Project = ProjectCore.GetDummyProjectViewModel(project.NewProjectRequest.SuggestedName,
                        project.NewProjectRequest.SuggetestedProjectId);
                }

                if (project.Package.AncestorPackageId.HasValue && project.ProjectId.HasValue)
                {
                    var projectInstance = ProjectService.GetProjectInstanceByPackageAndProjectId(project.Package.AncestorPackageId.Value, project.ProjectId.Value);
                    if (projectInstance != null)
                        project.FirstTask = projectInstance.Tasks.OrderBy(t => t.Fixed).Any() && projectInstance != null ?  projectInstance.Tasks.OrderBy(t => t.Fixed).FirstOrDefault() : null;
                }
            }

            return View(viewModel);
        }

        /// <summary>
        /// Update the widget at the given position to the given type.
        /// </summary>
        /// <param name="packageId"></param>
        /// <param name="widgetNumber"></param>
        /// <param name="widgetType"></param>
        /// <returns></returns>
        public bool UpdateWidget(Guid packageId, int widgetPosition, Package.Widget widgetType)
        {
            try
            {
                var package = PackageService.GetByPackageId(packageId);

                switch (widgetPosition)
                {
                    case 1:
                        package.Widget1 = widgetType;
                        break;
                    case 2:
                        package.Widget2 = widgetType;
                        break;
                    case 3:
                        package.Widget3 = widgetType;
                        break;
                }

                PackageService.Update(package);
                return true;
            }
            catch (Exception ex)
            {
                LogException(ex);
                return false;
            }
        }

        /// <summary>
        ///     Remove the widget at the given position on the given package.
        /// </summary>
        /// <param name="packageId"></param>
        /// <param name="widgetPosition"></param>
        /// <returns></returns>
        public bool RemoveWidget(Guid packageId, int widgetPosition)
        {
            try
            {
                var package = PackageService.GetByPackageId(packageId);

                switch (widgetPosition)
                {
                    case 1:
                        package.Widget1 = null;
                        break;
                    case 2:
                        package.Widget2 = null;
                        break;
                    case 3:
                        package.Widget3 = null;
                        break;
                }

                PackageService.Update(package);
                return true;
            }
            catch (Exception ex)
            {
                LogException(ex);
                return false;
            }
        }
    }
}