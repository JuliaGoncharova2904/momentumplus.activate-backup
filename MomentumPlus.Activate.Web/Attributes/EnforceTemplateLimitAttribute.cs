﻿using System.Web.Mvc;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Activate.Web.Properties;

namespace MomentumPlus.Activate.Web.Attributes
{
    /// <summary>
    /// Attribute when applied to a method will check the number of templates against
    /// the template limit set by iHandover. If the template count has reached its limit
    /// the action will be cancelled and an error message shown.
    /// </summary>
    public class EnforceTemplateLimitAttribute : ActionFilterAttribute, IActionFilter
    {
        public ITemplateService TemplateService { get; set; }

        void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext)
        {
            // Get upper template limit from account config
            var templateLimit = AccountConfig.Settings.TemplateLimit;

            // If template limit not set use value from app config
            if (!templateLimit.HasValue)
            {
                templateLimit = Settings.Default.DefaultTemplateLimit;
            }

            // Get total number of existing live templates
            var templateCount = TemplateService.Count();

            // If the upper limit has been reached then add an alert and redirect to the previous page
            if (templateCount >= templateLimit)
            {
                filterContext.Controller.TempData["Alert"] = new AlertViewModel
                {
                    Type = AlertViewModel.AlertType.Warning,
                    Message = string.Format("Your licence only allows for up to {0} templates.", templateLimit)
                };

                filterContext.Result = new RedirectResult(AppSession.Current.TemplateFormReturnUrl);
            }
        }
    }
}