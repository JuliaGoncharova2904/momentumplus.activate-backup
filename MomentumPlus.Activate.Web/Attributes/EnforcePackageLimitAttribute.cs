﻿using System.Web.Mvc;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Activate.Web.Properties;

namespace MomentumPlus.Activate.Web.Attributes
{
    /// <summary>
    /// Attribute when applied to a method will check the number of packages against
    /// the package limit set by iHandover. If the package count has reached its limit
    /// the action will be cancelled and an error message shown.
    /// </summary>
    public class EnforcePackageLimitAttribute : ActionFilterAttribute, IActionFilter
    {
        public IPackageService PackageService { get; set; }

        void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext)
        {
            // Get upper package limit from account config
            var packageLimit = AccountConfig.Settings.PackageLimit;

            // If package limit not set use value from app config
            if (!packageLimit.HasValue)
            {
                packageLimit = Settings.Default.DefaultPackageLimit;
            }

            // Get total number of existing live packages
            var packageCount = PackageService.Count();

            // If the upper limit has been reached then add an alert and redirect to the previous page
            if (packageCount >= packageLimit)
            {
                filterContext.Controller.TempData["Alert"] = new AlertViewModel
                {
                    Type = AlertViewModel.AlertType.Warning,
                    Message = string.Format("Your licence only allows for up to {0} packages.", packageLimit)
                };

                filterContext.Result = new RedirectResult(AppSession.Current.PackageFormReturnUrl);
            }
        }
    }
}