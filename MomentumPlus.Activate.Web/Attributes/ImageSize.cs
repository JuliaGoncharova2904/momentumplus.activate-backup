﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.IO;

namespace MomentumPlus.Activate.Web.Attributes
{
    /// <summary>
    /// Attribute to validate the size of a submitted image
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ImageSize : ValidationAttribute
    {

        private readonly int width;
        private readonly int height;

        public ImageSize(int width, int height, string errorMessage = "Image must be {0}x{1}px") : base(errorMessage)
        {
            this.width = width;
            this.height = height;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null)
                return null;

            if (value is HttpPostedFileBase)
            {
                try
                {
                    // Read uploaded file into Stream, convert to Image and check dimensions
                    Stream inputStream = (value as HttpPostedFileBase).InputStream;
                    Image img = Image.FromStream(inputStream);
                    inputStream.Seek(0, SeekOrigin.Begin);
                    if (img.Width == width && img.Height == height)
                    {
                        return null;
                    }
                }
                catch {}

            }

            return new ValidationResult(String.Format(ErrorMessageString, width, height));
        }

    }
}