﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MomentumPlus.Activate.Web.Attributes
{
    /// <summary>
    /// Attribute to validate the size of an uploaded file
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class MaxFileSizeAttribute : ValidationAttribute
    {
        private readonly int sizeMB;
        private readonly int sizeB;

        public MaxFileSizeAttribute(int sizeMB, string errorMessage = "Maximum allowed file size is {0}MB") : base(errorMessage)
        {
            this.sizeMB = sizeMB;
            sizeB = sizeMB * 1048576;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null || (value is HttpPostedFileBase && (value as HttpPostedFileBase).ContentLength <= sizeB))
            {
                return null;
            }

            return new ValidationResult(String.Format(ErrorMessageString, sizeMB));
        }

    }
}