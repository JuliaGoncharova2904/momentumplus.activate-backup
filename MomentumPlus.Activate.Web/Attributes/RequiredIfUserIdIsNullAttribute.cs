﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Foolproof;
using MomentumPlus.Activate.Web.Models.People;

namespace MomentumPlus.Activate.Web.Attributes
{
    /// <summary>
    /// Attribute to validate a property on a staff view model that must be
    /// given when the UserId property is null.
    /// </summary>
    public class RequiredIfUserIdIsNullAttribute : ModelAwareValidationAttribute
    {
        static RequiredIfUserIdIsNullAttribute()
        {
            Register.Attribute(typeof(RequiredIfUserIdIsNullAttribute));
        }

        public override bool IsValid(object value, object container)
        {
            if (value == null)
            {
                if (container is StaffViewModel)
                {
                    var model = container as StaffViewModel;
                    return model.UserId.HasValue;
                }

                return false;
            }

            return true;
        }
    }
}