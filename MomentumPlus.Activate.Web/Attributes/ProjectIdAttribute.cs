﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using MomentumPlus.Activate.Web.Helpers;

namespace MomentumPlus.Activate.Web.Attributes
{
    /// <summary>
    /// Attribute to validate a project ID against a regex set in the admin section of the application
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class ProjectIdAttribute : ValidationAttribute
    {
        private const string DefaultErrorMessage = "Invalid project ID.";

        public ProjectIdAttribute()
            : base(DefaultErrorMessage)
        {
        }

        public override bool IsValid(object value)
        {
            if (value != null && !string.IsNullOrEmpty(AccountConfig.Settings.ProjectIdFormat))
            {
                var regex = new Regex(AccountConfig.Settings.ProjectIdFormat);
                return regex.IsMatch(value.ToString());
            }

            return true;
        }
    }
}