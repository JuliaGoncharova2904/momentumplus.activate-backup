﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using MomentumPlus.Activate.Services;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Core.Models.People;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.Requests;

namespace MomentumPlus.Activate.Web.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public class AuthoriseAttribute : AuthorizeAttribute
    {
        private string IdParam;
        private string ItemType;

        public IUserService UserService { get; set; }
        public ITaskService TaskService { get; set; }
        public ITopicService TopicService { get; set; }
        public IContactService ContactService { get; set; }
        public IMeetingService MeetingService { get; set; }
        public IHandoverService HandoverService { get; set; }
        public IPackageService PackageService { get; set; }
        public IPeopleService PeopleService { get; set; }
        public IProjectService ProjectService { get; set; }
        public IRequestService RequestService { get; set; }
        public ITopicGroupService TopicGroupService { get; set; }

        private IEnumerable<string> AuthorisedRoles
        {
            get
            {
                return from piece in Roles.Split(',')
                       let trimmed = piece.Trim()
                       where !string.IsNullOrEmpty(trimmed)
                       select trimmed;
            }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public AuthoriseAttribute(string itemType = "", string idParam = "id")
        {
            IdParam = idParam;
            ItemType = itemType;
        }

        /// <summary>
        /// Custom authorisation checks.
        /// </summary>
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext == null)
            {
                throw new ArgumentNullException("httpContext");
            }

            // Current user
            IPrincipal user = httpContext.User;

            // Ensure the user is authenticated
            if (!user.Identity.IsAuthenticated)
            {
                return false;
            }

            //if (!ActiveSessions.IsValid(HttpContext.Current.User.Identity.Name, HttpContext.Current.Session.SessionID))
            //{
            //    AppSession.Current.CurrentUser = null;

            //    WebSecurity.Logout();
            //    HttpContext.Current.Session.Clear();

            //    return false;
            //}

            var currentUser = AppSession.Current.CurrentUser;

            if (currentUser == null)
            {
                currentUser = AppSession.Current.CurrentUser = UserService.GetByUsername(HttpContext.Current.User.Identity.Name);
            }

            // If any roles are specified check that the user is in one of them
            if (AuthorisedRoles.Count() > 0 && !AuthorisedRoles.Any(user.IsInRole))
            {
                return false;
            }

            // split ABSOLUTE_GENERAL_ACCESS into , deliminated then check
            // Admin has access to everything
            foreach (var my_Role in RoleAuthHelper.ABSOLUTE_GENERAL_ACCESS.Split(',').ToList())
            {
                if (user.IsInRole(my_Role))
                {
                    return true;
                }
            }

            // Check that the user has permission to view/edit the particular item
            if (!string.IsNullOrEmpty(ItemType))
            {
                if (httpContext.Request.HttpMethod == "GET")
                {
                    var itemId = httpContext.Request.RequestContext.RouteData.Values[IdParam];

                    if (itemId != null)
                    {
                        var id = new Guid(itemId.ToString());
                        return Authorised(id, ItemType, currentUser);
                    }
                }
                else if (httpContext.Request.HttpMethod == "POST")
                {
                    if (ItemType != "MeetingCreate")
                    {
                        // Get the item GUID from the URL
                        var url = httpContext.Request.RawUrl;
                        var itemId = url.Substring(url.LastIndexOf('/') + 1, 36);
                        return Authorised(new Guid(itemId), ItemType, currentUser);
                    }
                    else
                    {
                        // Meeting creation is a special case - contact ID
                        // is not passed in the URL.
                        return Authorised(new Guid(), ItemType, currentUser);
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Determine if the current user is authorised to view certain types of entities
        /// </summary>
        /// <param name="id"></param>
        /// <param name="itemType"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        private bool Authorised(Guid id, string itemType, User currentUser)
        {
            var httpContext = HttpContext.Current;

            switch (itemType)
            {
                case "Task":
                    var task = TaskService.GetByTaskId(id);
                    var tProject = ProjectService.GetByTaskId(id);
                    var tContact = ContactService.GetByTaskId(id);
                    return (task != null && task.IsOwner(currentUser, PackageService, true)) || tProject != null && tProject.IsOwner(currentUser, PackageService, true) || tContact != null && tContact.IsOwner(currentUser, PackageService, true) || (currentUser.Role.RoleParsed == Role.RoleEnum.Administrator ||
                        currentUser.Role.RoleParsed == Role.RoleEnum.HRManager ||
                        currentUser.Role.RoleParsed == Role.RoleEnum.LineManager ||
                        currentUser.Role.RoleParsed == Role.RoleEnum.iHandoverAdmin ||
                        currentUser.Role.RoleParsed == Role.RoleEnum.iHandoverAgent);
                case "Topic":
                    var topic = TopicService.GetByTopicId(id);
                    return topic != null && topic.IsOwner(currentUser, PackageService, true);
                case "TopicGroup":
                    var topicGroup = TopicGroupService.GetByTopicGroupId(id);
                    return topicGroup != null && topicGroup.IsOwner(currentUser, PackageService, true);
                case "Contact":
                    var contact = ContactService.GetById(id);
                    return contact != null && contact.IsOwner(currentUser, PackageService, true);
                case "Meeting":
                    var meeting = MeetingService.GetByMeetingId(id);
                    var mContact = ContactService.GetByMeetingId(id);
                    var mProject = ProjectService.GetByMeetingId(id);

                    return meeting != null && (mContact.IsOwner(currentUser, PackageService, true) || mProject.IsOwner(currentUser, PackageService, true));
                case "MeetingCreate":
                    var contactId = httpContext.Request.Form["ParentContactId"];

                    if (!string.IsNullOrEmpty(contactId))
                    {
                        var meetingContact = ContactService.GetById(new Guid(contactId));
                        var meetingProject = ProjectService.GetProjectInstanceById(id);
                        return (meetingContact != null && meetingContact.IsOwner(currentUser, PackageService, true)) || (meetingProject != null && meetingProject.IsOwner(currentUser, PackageService, true));
                    }

                    break;
                case "Package":
                    var package = PackageService.GetByPackageId(id);
                    return package != null && package.IsOwner(currentUser, PackageService, true);
                case "Handover":
                    var handoverPackage = PackageService.GetByPackageId(id);
                    return handoverPackage != null && handoverPackage.IsOwner(currentUser, PackageService, true);
                case "Position":
                    return currentUser.Role.RoleParsed == Role.RoleEnum.Administrator ||
                        currentUser.Role.RoleParsed == Role.RoleEnum.HRManager ||
                        currentUser.Role.RoleParsed == Role.RoleEnum.LineManager ||
                        currentUser.Role.RoleParsed == Role.RoleEnum.iHandoverAdmin ||
                        currentUser.Role.RoleParsed == Role.RoleEnum.iHandoverAgent;
                case "ProjectInstance":
                    var projectInstance = ProjectService.GetProjectInstanceById(id);
                    return projectInstance != null && projectInstance.IsOwner(currentUser, PackageService, true);
                case "Request":
                    var request = RequestService.GetRequest<Request>(id);
                    return request != null && request.IsOwner(currentUser, PackageService, true);
                case "Staff":
                    var staff = ContactService.GetById(id);
                    return staff != null && staff.IsOwner(currentUser, PackageService, true);
                case "Attachment":
                case "VoiceMessage":
                    if (httpContext.Request.HttpMethod == "GET")
                    {
                        // Item type in query string
                        var parentItemType = httpContext.Request.QueryString["type"];

                        if (parentItemType != null)
                        {
                            return Authorised(id, parentItemType, currentUser);
                        }
                    }
                    else if (httpContext.Request.HttpMethod == "POST")
                    {
                        // Item type in form data
                        var parentItemType = httpContext.Request.Form["ItemType"];

                        if (!string.IsNullOrEmpty(parentItemType))
                        {
                            return Authorised(id, parentItemType, currentUser);
                        }
                    }

                    break;
                case "AttachmentEdit":
                case "VoiceMessageEdit":
                    if (httpContext.Request.HttpMethod == "GET")
                    {
                        // Item type and ID in query string
                        var parentItemType = httpContext.Request.QueryString["type"];
                        var parentItemId = httpContext.Request.QueryString["itemId"];

                        if (parentItemType != null && parentItemId != null)
                        {
                            return Authorised(new Guid(parentItemId), parentItemType, currentUser);
                        }
                    }
                    else if (httpContext.Request.HttpMethod == "POST")
                    {
                        // Item type and ID in form data
                        var parentItemType = httpContext.Request.Form["ItemType"];
                        var parentItemId = httpContext.Request.Form["ItemId"];

                        if (!string.IsNullOrEmpty(parentItemType) && !string.IsNullOrEmpty(parentItemId))
                        {
                            return Authorised(new Guid(parentItemId), parentItemType, currentUser);
                        }
                    }

                    break;
            }

            return true;
        }
    }
}