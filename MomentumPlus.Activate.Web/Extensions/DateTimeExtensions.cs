﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MomentumPlus.Activate.Web.Extensions
{
    public static class DateTimeExtensions
    {
        /// <summary>
        /// Example '5th Aug 2015'
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static string ToStringShortFormat(this DateTime val)
        {
            string day = AddOrdinal(val.Day);
            string month = val.ToString("MMM");
            string year = val.Year.ToString();
            string formated =
                string.Format("{0} {1} {2}", day, month, year);
            return formated;
        }

        private static string ConvertDay(int day)
        {
            string suffix = string.Empty;

            switch (day)
            {
                case 1:
                case 21:
                case 31:
                    suffix = "st";
                    break;
                case 2:
                case 22:
                    suffix = "nd";
                    break;
                case 3:
                case 23:
                    suffix = "rd";
                    break;
                default:
                    suffix = "th";
                    break;
            }
            return string.Format("{0}{1}", day, suffix);
        }

        public static string AddOrdinal(int num)
        {
            if (num <= 0) return num.ToString();
            switch (num % 100)
            {
                case 11:
                case 12:
                case 13:
                    return num + "th";
            }
            switch (num % 10)
            {
                case 1:
                    return num + "st";
                case 2:
                    return num + "nd";
                case 3:
                    return num + "rd";
                default:
                    return num + "th";
            }
        }
    }
}