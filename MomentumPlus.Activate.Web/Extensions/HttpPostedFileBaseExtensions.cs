﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using MomentumPlus.Activate.Web.Helpers;

namespace MomentumPlus.Activate.Web.Extensions
{
    public static class HttpPostedFileBaseExtensions
    {
        /// <summary>
        /// Check if the uploaded file is valid based on the allowed file extensions
        /// set in the account config.
        /// </summary>
        public static bool IsValid(this HttpPostedFileBase httpPostedFileBase)
        {
            var allowedExtensions = new List<string>();

            if (AccountConfig.Settings.FileExtensions != null)
            {
                allowedExtensions = AccountConfig.Settings.FileExtensions.Split(',').Select(s => s.Trim()).ToList();
            }

            var extension = Path.GetExtension(httpPostedFileBase.FileName);
            return allowedExtensions.Contains(extension);
        }
    }
}