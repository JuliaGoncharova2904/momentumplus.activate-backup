﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web.Routing;

namespace MomentumPlus.Activate.Web.Extensions
{
    public static class NameValueCollectionExtensions
    {
        /// <summary>
        /// Convert a <c>NameValueCollection</c> object to a <c>RouteValueDictionary</c>.
        /// </summary>
        /// <param name="nameValueCollection">
        /// The <c>NameValueCollection</c> object.
        /// </param>
        /// <param name="exclude">
        /// An optional array of keys to exclude from the resulting object.
        /// </param>
        /// <returns>
        /// A <c>RouteValueDictionary</c> object containing all the key-value pairs from
        /// the <c>NameValueCollection</c> except those specifically excluded.
        /// </returns>
        public static RouteValueDictionary ToRouteValueDictionary(
            this NameValueCollection nameValueCollection,
            string[] exclude = null)
        {
            if (nameValueCollection == null || !nameValueCollection.HasKeys())
            {
                return new RouteValueDictionary();
            }

            exclude = exclude ?? new string[] { };

            var routeValues = new RouteValueDictionary();

            foreach (string key in nameValueCollection.AllKeys.Where(k => !exclude.Contains(k)))
            {
                routeValues.Add(key, nameValueCollection[key]);
            }

            return routeValues;
        }

        /// <summary>
        /// Adds a dictionary to a <c>NameValueCollection</c>, replacing existing values,
        /// and converts to a <c>RouteValueDictionary</c>.
        /// </summary>
        /// <param name="nameValueCollection">The <c>NameValueCollection</c> object.</param>
        /// <param name="values">The value collection.</param>
        /// <returns>
        /// A <c>RouteValueDictionary</c> object containing all the key-value pairs from
        /// the <c>NameValueCollection</c> updated with the given dictionary.
        /// </returns>
        public static RouteValueDictionary AddOrUpdate(
            this NameValueCollection nameValueCollection,
            IDictionary<string, object> dict)
        {
            var routeValueDictionary = nameValueCollection.ToRouteValueDictionary();

            foreach (var key in dict.Keys)
            {
                if (routeValueDictionary.ContainsKey(key))
                {
                    routeValueDictionary[key] = dict[key];
                }
                else
                {
                    routeValueDictionary.Add(key, dict[key]);
                }
            }

            return routeValueDictionary;
        }
    }
}