﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;

namespace MomentumPlus.Activate.Web.Extensions
{
    /// <summary>
    /// Extensions for <c>Enum</c> objects and types.
    /// </summary>
    public static class EnumExtensions
    {
        /// <summary>
        /// Get the value of the description attribute set on an enumeration.
        /// </summary>
        /// <param name="val">The enumeration value.</param>
        /// <returns>The description string.</returns>
        public static string GetDescription(this Enum val)
        {
            try
            {
                var attributes = (DescriptionAttribute[])val
                    .GetType()
                    .GetField(val.ToString())
                    .GetCustomAttributes(typeof(DescriptionAttribute), false);
                return attributes.Length > 0 ? attributes[0].Description : val.ToString();
            }
            catch
            {
                return val.ToString();
            }
        }

        /// <summary>
        /// Generate a list of <c>SelectListItem</c> objects from an enumeration object.
        /// </summary>
        /// <typeparam name="TEnum">The enumeration type.</typeparam>
        /// <param name="enumObj">The enumeration object.</param>
        /// <returns>An <c>IEnumerable</c> of <c>SelectListItem</c> objects.</returns>
        public static IEnumerable<SelectListItem> ToSelectList<TEnum>(this TEnum enumObj)
        {
            var values = from TEnum e in Enum.GetValues(typeof(TEnum))
                         select new SelectListItem
                         {
                             Text = (e as Enum).GetDescription(),
                             Value = e.ToString(),
                         };

            return values;
        }
    }
}