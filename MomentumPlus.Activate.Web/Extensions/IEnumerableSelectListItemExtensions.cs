﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace MomentumPlus.Activate.Web.Extensions
{
    public static class IEnumerableSelectListItemExtensions
    {
        public static string ToJsonArray(this IEnumerable<SelectListItem> enumerable)
        {
            var strings = enumerable.Select(s => s.Text);

            var serializer = new JavaScriptSerializer();
            return serializer.Serialize(strings);
        }
    }
}