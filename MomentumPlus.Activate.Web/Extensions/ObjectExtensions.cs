﻿using System;
using System.Data.Entity.Design.PluralizationServices;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Routing;

namespace MomentumPlus.Activate.Web.Extensions
{
    /// <summary>
    /// Extension methods for all objects.
    /// </summary>
    public static class ObjectExtensions
    {
        /// <summary>
        /// Add an attribute to an object containing HTML element attributes based on some condition.
        /// </summary>
        /// <param name="htmlAttributes">The HTML attributes object.</param>
        /// <param name="attribute">The name of the attribute.</param>
        /// <param name="value">The value of the attribute.</param>
        /// <param name="condition">A boolean determining whether or not the attribute will be added.</param>
        /// <returns>A <c>RouteValueDictionary</c> object containing the updated HTML attributes.</returns>
        public static RouteValueDictionary WithAttrIf(this object htmlAttributes, string attribute, object value, bool condition)
        {
            var attributes = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);

            if (condition)
            {
                attributes[attribute] = value;
            }

            return attributes;
        }

        public static RouteValueDictionary WithAttrIf(this RouteValueDictionary attributes, string attribute, object value, bool condition)
        {
            if (condition)
            {
                attributes[attribute] = value;
            }

            return attributes;
        }

        // Deep clone
        public static T DeepClone<T>(this T a)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, a);
                stream.Position = 0;
                return (T)formatter.Deserialize(stream);
            }
        }

        // Singularises the string
        public static String Singularise(this String s)
        {
            return PluralizationService.CreateService(CultureInfo.CurrentCulture).Singularize(s);
        }

        // Pluralises the string
        public static String Pluralise(this String s)
        {
            return PluralizationService.CreateService(CultureInfo.CurrentCulture).Pluralize(s);
        }

    }
}