﻿using System;
using System.ComponentModel;

namespace MomentumPlus.Activate.Web.Extensions
{
    public static class StringExtensions
    {
        public static T? ToNullable<T>(this string s) where T : struct
        {
            T? result = new Nullable<T>();
            try
            {
                if (!string.IsNullOrEmpty(s) && s.Trim().Length > 0)
                {
                    TypeConverter conv = TypeDescriptor.GetConverter(typeof(T));
                    result = (T)conv.ConvertFrom(s);
                }
            }
            catch { }
            return result;
        }
    }
}