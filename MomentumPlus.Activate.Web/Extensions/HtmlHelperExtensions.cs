﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using MomentumPlus.Activate.Services.Helpers;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;
using AccountConfig = MomentumPlus.Activate.Web.Helpers.AccountConfig;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.People;
using System.Web.Mvc.Ajax;
using MomentumPlus.Activate.Services.Extensions;

namespace MomentumPlus.Activate.Web.Extensions
{
    /// <summary>
    /// <c>HtmlHelper</c> extension methods for use in views across the application.
    /// </summary>
    public static class HtmlHelperExtensions
    {
        /// <summary>
        /// Create an HTML element based on a given condition.
        /// </summary>
        /// <param name="htmlHelper">The <c>HtmlHelper</c> object.</param>
        /// <param name="tagName">The HTML element name.</param>
        /// <param name="cssClass">The CSS class to add to the element.</param>
        /// <param name="condition">
        /// If <c>true</c> the specified tag will be created and returned.
        /// If <c>false</c> then the alternative tag will be returned.
        /// If no alternative tag is specified then null is returned.
        /// </param>
        /// <param name="altTag">Optional tag to show when the condition is false.</param>
        /// <param name="altCssClass">CSS class to add to the alternative tag.</param>
        /// <returns>An HTML-encoded string or <c>null</c> depending on the value of the condition.</returns>
        public static MvcHtmlString Element(
            this HtmlHelper htmlHelper,
            string tagName,
            string cssClass,
            bool condition,
            string altTag = null,
            string altCssClass = "")
        {
            if (condition)
            {
                var tagBuilder = new TagBuilder(tagName);
                tagBuilder.AddCssClass(cssClass);
                return MvcHtmlString.Create(tagBuilder.ToString());
            }
            else if (altTag != null)
            {
                var tagBuilder = new TagBuilder(altTag);
                tagBuilder.AddCssClass(altCssClass);
                return MvcHtmlString.Create(tagBuilder.ToString());
            }

            return null;
        }

        /// <summary>
        /// Get the number of objects in a collection. If the collection is <c>null</c>
        /// then a count of zero is returned.
        /// </summary>
        /// <typeparam name="T">The type of the objects in the collection.</typeparam>
        /// <param name="htmlHelper">The <c>HtmlHelper</c> object.</param>
        /// <param name="collection">The collection.</param>
        /// <returns>The number of items in the collection.</returns>
        public static int Count<T>(this HtmlHelper htmlHelper, ICollection<T> collection)
        {
            return collection != null ? collection.Count : 0;
        }

        /// <summary>
        /// Create a label for the given package status.
        /// </summary>
        /// <param name="htmlHelper">The <c>HtmlHelper</c> object.</param>
        /// <param name="status">The package status.</param>
        /// <returns>An HTML-encoded string containing the label.</returns>
        public static MvcHtmlString PackageStatusLabel(this HtmlHelper htmlHelper, Guid id)
        {
            var handoverService = DependencyResolver.Current.GetService<IHandoverService>();
            var packageService = DependencyResolver.Current.GetService<IPackageService>();

            var tagBuilder = new TagBuilder("span");
            var label = String.Empty;
            var status = "";

            var package = packageService.GetByPackageId(id);

            if (handoverService.IsOnboarding(id))
            {
                label = "primary";
                status = "Onboarding";
            }
            else if (handoverService.IsLeaving(id))
            {
                label = "leaving";
                status = "Leaving";
            }

            //if (packageService.IsOngoing(id))
            //{
            //    label = "default";
            //    status = "Ongoing";
            //}

            //else if (handoverService.HasLeft(id) || (package != null && package.Deleted.HasValue))
            //{
            //    label = "retired";
            //    status = "Retired";
            //} else
            else if (package != null && package.Deleted.HasValue)
            {
                label = "retired";
                status = "Retired";
            }
            else if (handoverService.HasLeft(id))
            {
                label = "finalise";
                status = "Finalise";
            }
            else
            {
                label = "default";
                status = "Ongoing";
            }

            tagBuilder.AddCssClass("label label-" + label);
            tagBuilder.InnerHtml = status;

            return MvcHtmlString.Create(tagBuilder.ToString());
        }

        /// <summary>
        /// Create a label for the given task priority.
        /// </summary>
        /// <param name="htmlHelper">The <c>HtmlHelper</c> object.</param>
        /// <param name="status">The task priority.</param>
        /// <returns>An HTML-encoded string containing the label.</returns>
        public static MvcHtmlString TaskPriorityLabel(this HtmlHelper htmlHelper, Task.TaskPriority priority)
        {
            var tagBuilder = new TagBuilder("span");

            var label = String.Empty;
            switch (priority)
            {
                case Task.TaskPriority.Low:
                    label = "info"; // or "success"
                    break;
                case Task.TaskPriority.Medium:
                    label = "warning";
                    break;
                case Task.TaskPriority.High:
                    label = "danger";
                    break;
            }

            tagBuilder.AddCssClass("label label-" + label);
            tagBuilder.InnerHtml = priority.ToString();
            return MvcHtmlString.Create(tagBuilder.ToString());
        }


        /// <summary>
        /// Create a label for the topic.
        /// </summary>
        /// <param name="htmlHelper">The <c>HtmlHelper</c> object.</param>
        /// <param name="createdDate">The topic created date.</param>
        /// <returns>An HTML-encoded string containing the label.</returns>
        public static MvcHtmlString TopicNewLabel(this HtmlHelper htmlHelper, DateTime? createdDate)
        {
            var tagBuilder = new TagBuilder("span");

            var label = String.Empty;

            if (createdDate != null && createdDate > SystemTime.Now.AddDays(-2))
            {
                tagBuilder.AddCssClass("label label-new-topic");
                tagBuilder.InnerHtml = "New";
            }


            return MvcHtmlString.Create(tagBuilder.ToString());
        }




        public static MvcHtmlString ProjectPriorityLabel(this HtmlHelper htmlHelper, ProjectInstance.ProjectInstancePriority priority)
        {
            var tagBuilder = new TagBuilder("span");

            var label = String.Empty;
            switch (priority)
            {
                case ProjectInstance.ProjectInstancePriority.Low:
                    label = "info"; // or "success"
                    break;
                case ProjectInstance.ProjectInstancePriority.Medium:
                    label = "warning";
                    break;
                case ProjectInstance.ProjectInstancePriority.High:
                    label = "danger";
                    break;
            }

            tagBuilder.AddCssClass("label label-" + label);
            tagBuilder.InnerHtml = priority.ToString();
            return MvcHtmlString.Create(tagBuilder.ToString());
        }

        /// <summary>
        /// Create a colour coded last viewed date.
        /// </summary>
        /// <param name="htmlHelper">The <c>HtmlHelper</c> object.</param>
        /// <param name="date">The last viewed date.</param>
        /// <param name="type">The item type.</param>
        /// <returns>An HTML-encoded string containing the element.</returns>
        public static MvcHtmlString LastViewedDate(this HtmlHelper htmlHelper, DateTime? date, ItemType? type = null)
        {
            var tagBuilder = new TagBuilder("span");

            if (date.HasValue)
            {
                var days = (DateTime.Now - date.Value).TotalDays;

                var highAlertDays = 180;
                var mediumAlertDays = 90;

                if (type == ItemType.Task)
                {
                    highAlertDays = AccountConfig.Settings.TaskAlertDaysHigh;
                    mediumAlertDays = AccountConfig.Settings.TaskAlertDaysMedium;
                }
                else if (type == ItemType.Topic)
                {
                    highAlertDays = AccountConfig.Settings.TopicAlertDaysHigh;
                    mediumAlertDays = AccountConfig.Settings.TopicAlertDaysMedium;
                }

                if (days >= highAlertDays)
                {
                    tagBuilder.AddCssClass("bg-danger");
                }
                else if (days >= mediumAlertDays)
                {
                    tagBuilder.AddCssClass("bg-warning");
                }

                tagBuilder.InnerHtml = date.Value.ToShortDateString();
            }
            else
            {
                tagBuilder.AddCssClass("glyphicon glyphicon-minus");
            }

            return MvcHtmlString.Create(tagBuilder.ToString());
        }

        /// <summary>
        /// Truncate a string.
        /// </summary>
        /// <param name="html">The <c>HtmlHelper</c> object.</param>
        /// <param name="value">The string to truncate.</param>
        /// <param name="count">The maximum length of the output string (excluding ellipsis).</param>
        /// <returns>The truncated string.</returns>
        public static string Truncate(this HtmlHelper html, string value, int count)
        {
            if (string.IsNullOrEmpty(value))
            {
                return string.Empty;
            }

            if (value.Length > count)
            {
                value = value.Substring(0, count - 1) + "...";
            }

            return value;
        }

        /// <summary>
        /// Convert a nullable <c>DateTime</c> to short date string or placeholder
        /// value when the object is null.
        /// </summary>
        /// <param name="html">The <c>HtmlHelper</c> object.</param>
        /// <param name="dateTime">Nullable <c>DateTime</c> object.</param>
        /// <returns>A short date string or placeholder when the date is null.</returns>
        public static string ShortDate(this HtmlHelper html, DateTime? dateTime)
        {
            if (dateTime.HasValue)
            {
                return dateTime.Value.ToShortDateString();
            }
            else
            {
                return "--";
            }
        }

        /// <summary>
        /// Convert a file size in bytes to a human-readable format.
        /// </summary>
        /// <param name="html">The <c>HtmlHelper</c> object.</param>
        /// <param name="bytes">The file size in bytes.</param>
        /// <returns>A human-readable file size string.</returns>
        public static string ReadableFileSize(this HtmlHelper html, long bytes)
        {
            string sign = (bytes < 0 ? "-" : "");
            double readable = (bytes < 0 ? -bytes : bytes);
            string suffix;

            if (bytes >= 0x1000000000000000) // Exabyte
            {
                suffix = "EB";
                readable = (double)(bytes >> 50);
            }
            else if (bytes >= 0x4000000000000) // Petabyte
            {
                suffix = "PB";
                readable = (double)(bytes >> 40);
            }
            else if (bytes >= 0x10000000000) // Terabyte
            {
                suffix = "TB";
                readable = (double)(bytes >> 30);
            }
            else if (bytes >= 0x40000000) // Gigabyte
            {
                suffix = "GB";
                readable = (double)(bytes >> 20);
            }
            else if (bytes >= 0x100000) // Megabyte
            {
                suffix = "MB";
                readable = (double)(bytes >> 10);
            }
            else if (bytes >= 0x400) // Kilobyte
            {
                suffix = "KB";
                readable = (double)bytes;
            }
            else
            {
                return bytes.ToString(sign + "0 B"); // Byte
            }

            readable = readable / 1024;

            return sign + readable.ToString("0.### ") + suffix;
        }

        public static MvcHtmlString TruncatedLink(this HtmlHelper htmlHelper, string url)
        {
            var tagBuilder = new TagBuilder("a");
            tagBuilder.Attributes.Add("href", url);
            tagBuilder.Attributes.Add("target", "_blank");
            tagBuilder.InnerHtml = htmlHelper.Truncate(url, 30);
            return MvcHtmlString.Create(tagBuilder.ToString());
        }

        public static MvcHtmlString UnorderedList(this HtmlHelper htmlHelper, IEnumerable<string> items)
        {
            var ul = new TagBuilder("ul");

            foreach (var item in items)
            {
                var li = new TagBuilder("li");
                li.InnerHtml = item;
                ul.InnerHtml += li;
            }

            return MvcHtmlString.Create(ul.ToString());
        }

        public static MvcHtmlString TruncateWithTooltip(this HtmlHelper htmlHelper, string text, int maxLength,
            string placement = "top")
        {
            if (string.IsNullOrEmpty(text))
            {
                return MvcHtmlString.Create("");
            }

            var span = new TagBuilder("span");
            span.InnerHtml = htmlHelper.Truncate(text, maxLength);

            if (text.Length > maxLength)
            {
                span.Attributes.Add("data-toggle", "tooltip");
                span.Attributes.Add("data-placement", placement);
                span.Attributes.Add("title", text);
            }

            return MvcHtmlString.Create(span.ToString());
        }

        public static MvcHtmlString PackagePermissionsLabel(this HtmlHelper htmlHelper, bool hr, bool lm)
        {
            var strong = new TagBuilder("span");
            strong.AddCssClass("label label-default");
            strong.Attributes.Add("data-toggle", "tooltip");
            strong.Attributes.Add("data-placement", "top");

            if (hr && lm)
            {
                strong.InnerHtml = "HR/LM";
                strong.Attributes.Add("title", "HR and Line Manager");
            }
            else if (hr)
            {
                strong.InnerHtml = "HR";
                strong.Attributes.Add("title", "HR Manager");
            }
            else if (lm)
            {
                strong.InnerHtml = "LM";
                strong.Attributes.Add("title", "Line Manager");
            }
            else
            {
                return MvcHtmlString.Create("");
            }

            return MvcHtmlString.Create(strong.ToString());
        }

        public static MvcHtmlString PackagePermissionsLabel(this HtmlHelper htmlHelper, Package package, User user)
        {
            return PackagePermissionsLabel(htmlHelper, package.UserHRManagerId == user.ID, package.UserLineManagerId == user.ID);
        }

        public static MvcHtmlString PackagePermissionsTableHeader(this HtmlHelper htmlHelper)
        {
            if (AppSession.Current.ViewAsUser.Role.RoleParsed == Role.RoleEnum.HRManager ||
                AppSession.Current.ViewAsUser.Role.RoleParsed == Role.RoleEnum.LineManager)
            {
                return MvcHtmlString.Create("<th class=\"text-center\" style=\"width: 5%;\"></th>");
            }
            else
            {
                return MvcHtmlString.Create("<th style=\"width: 0%;\"></th>");
            }
        }

        public static bool TimeManipulationToolEnabled(this HtmlHelper htmlHelper)
        {
            return Properties.Settings.Default.TimeManipulationToolEnabled;
        }

        public static MvcHtmlString RawActionLink(this AjaxHelper ajaxHelper, string linkText, string actionName, string controllerName, object routeValues, AjaxOptions ajaxOptions, object htmlAttributes)
        {
            var repID = Guid.NewGuid().ToString();
            var lnk = ajaxHelper.ActionLink(repID, actionName, controllerName, routeValues, ajaxOptions, htmlAttributes);
            return MvcHtmlString.Create(lnk.ToString().Replace(repID, linkText));
        }

        /// <summary>
        /// Get description of enum
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="htmlHelper">The <c>HtmlHelper</c> object.</param>
        /// <param name="enumVal">Enum value</param>
        /// <param name="template">Template string</param>
        /// <returns>An HTML-encoded string.</returns>
        public static MvcHtmlString EnumDescription<TEnum>(this HtmlHelper htmlHelper, TEnum enumVal, string template = "{0}") where TEnum : struct, IConvertible
        {
            return new MvcHtmlString(string.Format(template, enumVal.GetEnumDescription<TEnum>()));
        }
    }
}