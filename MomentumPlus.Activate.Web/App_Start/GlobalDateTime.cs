﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using MomentumPlus.Activate.Web.Properties;

namespace MomentumPlus.Activate.Web.App_Start
{
    public class GlobalDateTime
    {
        public static void SetDateTimeFormat()
        {
            var culture = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            culture.DateTimeFormat.ShortDatePattern = Settings.Default.ShortDateFormat;
            Thread.CurrentThread.CurrentCulture = culture;
        }
    }
}