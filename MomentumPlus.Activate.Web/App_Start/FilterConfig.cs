﻿using System;
using System.Diagnostics;
using System.Web.Mvc;
using MomentumPlus.Activate.Services;
using MomentumPlus.Activate.Web.Filters; 
using NLog;

namespace MomentumPlus.Activate.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new XFrameOptionsFilter());
            filters.Add(new AuthorizeAttribute());

            FilterProviders.Providers.Add(new PerformanceTestFilterProvider());
        }
    }

    public class XFrameOptionsFilter : ActionFilterAttribute
    {
        private bool AllowFrames;

        public XFrameOptionsFilter(bool allowFrames = false)
        {
            AllowFrames =
                allowFrames;
        }

        public override void OnResultExecuting(System.Web.Mvc.ResultExecutingContext filterContext)
        {
            if (!AllowFrames)
            {
                filterContext.HttpContext.Response.AddHeader("x-frame-options", "DENY");
            }
        }
    }

   

  


}