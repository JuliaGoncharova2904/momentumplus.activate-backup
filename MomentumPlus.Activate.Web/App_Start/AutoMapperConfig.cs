﻿using AutoMapper;
using MomentumPlus.Activate.Web.Areas.Framework.Models;
using MomentumPlus.Activate.Web.Areas.Home.Models;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Activate.Web.Models.Moderation;
using MomentumPlus.Activate.Web.Models.Requests;
using MomentumPlus.Activate.Web.Models.People;
using MomentumPlus.Activate.Web.Extensions;
using MomentumPlus.Activate.Web.Models.Handover;
using AccountConfig = MomentumPlus.Core.Models.AccountConfig;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.Handover;
using MomentumPlus.Core.Models.Moderation;
using MomentumPlus.Core.Models.People;
using MomentumPlus.Core.Models.Requests;
using MomentumPlus.Core.Models.ProLink;
using System.Collections.Generic;
using System.Web.Mvc;
using System;
using MvcPaging;

namespace MomentumPlus.Activate.Web
{
    public static class AutoMapperConfig
    {

        /// <summary>
        /// Create the maps between the domain models from the
        /// <c>MomentumPlus.Activate.Entities</c> project and
        /// the view models in <c>MomentumPlus.Activate.Web</c>.
        /// </summary>
        public static void CreateMaps()
        {
            Mapper.CreateMap<BaseModel, BaseViewModel>()
                .Include<AccountConfig, AccountConfigViewModel>()
                .Include<Attachment, AttachmentViewModel>()
                .Include<BaseApproval, BaseApprovalViewModel>()
                .Include<DocumentLibrary, DocumentLibraryFormViewModel>()
                .Include<File, FileViewModel>()
                .Include<Handover, HandoverViewModel>()
                .Include<Meeting, MeetingViewModel>()
                .Include<Moderated, ModeratedViewModel>()
                .Include<Module, ModuleViewModel>()
                .Include<Package, PackageViewModel>()
                .Include<Period, PeriodViewModel>()
                .Include<Person, PersonViewModel>()
                .Include<Position, PositionViewModel>()
                .Include<Project, ProjectViewModel>()
                .Include<ProjectRole, ProjectRoleViewModel>()
                .Include<ProjectStatus, ProjectStatusViewModel>()
                .Include<Role, RoleViewModel>()
                .Include<Template, TemplateViewModel>()
                .Include<TopicAnswer, TopicAnswerViewModel>()
                .Include<TopicField, TopicFieldViewModel>()
                .Include<TopicGroup, TopicGroupViewModel>()
                .Include<TopicQuestion, TopicQuestionViewModel>()
                .Include<User, UserViewModel>()
                .Include<VoiceMessage, VoiceMessageViewModel>()
                .Include<VoiceMessage, VoiceMessageByPhoneViewModel>()
                .Include<VoiceMessage, VoiceMessageByPCViewModel>()
                .Include<Workplace, WorkplaceViewModel>()
                .IgnoreAllUnmapped();

            Mapper.CreateMap<BaseViewModel, BaseModel>()
                .Include<AttachmentViewModel, Attachment>()
                .Include<BaseApprovalViewModel, BaseApproval>()
                .Include<DocumentLibraryFormViewModel, DocumentLibrary>()
                .Include<FileViewModel, File>()
                .Include<MeetingsViewModel, Meeting>()
                .Include<HandoverViewModel, Handover>()
                .Include<MeetingViewModel, Meeting>()
                .Include<ModuleViewModel, Module>()
                .Include<PackageViewModel, Package>()
                .Include<PeriodViewModel, Period>()
                .Include<PositionViewModel, Position>()
                .Include<ProjectViewModel, Project>()
                .Include<ProjectRoleViewModel, ProjectRole>()
                .Include<ProjectStatusViewModel, ProjectStatus>()
                .Include<RoleViewModel, Role>()
                .Include<TemplateViewModel, Template>()
                .Include<TopicAnswerViewModel, TopicAnswer>()
                .Include<TopicFieldViewModel, TopicField>()
                .Include<TopicGroupViewModel, TopicGroup>()
                .Include<TopicQuestionViewModel, TopicQuestion>()
                .Include<UserViewModel, User>()
                .Include<VoiceMessageViewModel, VoiceMessage>()
                .Include<VoiceMessageByPhoneViewModel, VoiceMessage>()
                .Include<VoiceMessageByPCViewModel, VoiceMessage>()
                .Include<WorkplaceViewModel, Workplace>()
                .ForMember(m => m.ID, o => o.Ignore())
                .ForMember(m => m.Created, o => o.Ignore())
                .ForMember(m => m.Modified, o => o.Ignore())
                .IgnoreAllUnmapped();

            Mapper.CreateMap<BaseApproval, BaseApprovalViewModel>()
                .IgnoreAllUnmapped();

            Mapper.CreateMap<BaseApprovalViewModel, BaseApproval>()
                .IgnoreAllUnmapped();

            Mapper.CreateMap<AccountConfig, AccountConfigViewModel>();

            Mapper.CreateMap<Attachment, AttachmentViewModel>().IgnoreAllUnmapped();
            Mapper.CreateMap<AttachmentViewModel, Attachment>().IgnoreAllUnmapped();

            Mapper.CreateMap<Contact, ContactViewModel>();
            Mapper.CreateMap<ContactViewModel, Contact>()
                .IgnoreAllUnmapped();

            Mapper.CreateMap<Contact, ContactChartViewModel>()
                .ForMember(c => c.CompanyId, o => o.MapFrom(c => c.CompanyId));

            Mapper.CreateMap<DocumentLibrary, DocumentLibraryFormViewModel>();

            Mapper.CreateMap<File, FileViewModel>().Include<Image, ImageViewModel>();
            Mapper.CreateMap<FileViewModel, File>().Include<ImageViewModel, Image>();

            Mapper.CreateMap<Handover, HandoverViewModel>()
                .ForMember(m => m.LeavingPeriodViewModel, o => o.MapFrom(m => m.LeavingPeriod))
                .ForMember(m => m.OnboardingPeriodViewModel, o => o.MapFrom(m => m.OnboardingPeriod));
            Mapper.CreateMap<HandoverViewModel, Handover>()
                .ForMember(m => m.LeavingPeriod, o => o.MapFrom(m => m.LeavingPeriodViewModel))
                .ForMember(m => m.OnboardingPeriod, o => o.MapFrom(m => m.OnboardingPeriodViewModel));

            Mapper.CreateMap<LeavingPeriod, LeavingPeriodViewModel>();
            Mapper.CreateMap<LeavingPeriodViewModel, LeavingPeriod>();

            Mapper.CreateMap<Meeting, MeetingViewModel>().IgnoreAllUnmapped();
            Mapper.CreateMap<MeetingViewModel, Meeting>();

            Mapper.CreateMap<Moderated, ModeratedViewModel>()
                .Include<Request, RequestViewModel>()
                .Include<Contact, ContactViewModel>()
                .Include<Task, TaskViewModel>()
                .Include<Topic, TopicViewModel>();

            Mapper.CreateMap<Module, ModuleViewModel>().IgnoreAllUnmapped();
            Mapper.CreateMap<ModuleViewModel, Module>().IgnoreAllUnmapped();

            Mapper.CreateMap<OnboardingPeriod, OnboardingPeriodViewModel>();
            Mapper.CreateMap<OnboardingPeriodViewModel, OnboardingPeriod>();

            Mapper.CreateMap<Package, PackageViewModel>()
                .ForMember(m => m.UserOwners, o => o.Ignore())
                .ForMember(m => m.Positions, o => o.Ignore())
                .ForMember(m => m.Workplaces, o => o.Ignore())
                .ForMember(m => m.UserLineManagers, o => o.Ignore())
                .ForMember(m => m.UserHRManagers, o => o.Ignore());
            Mapper.CreateMap<PackageViewModel, Package>()
                .ForMember(m => m.ID, o => o.Ignore())
                .ForMember(m => m.Created, o => o.Ignore())
                .ForMember(m => m.Modified, o => o.Ignore())
                .ForMember(m => m.Modules, o => o.Ignore())
                .ForMember(m => m.Name, o => o.Ignore())
                .ForMember(m => m.UserOwner, o => o.Ignore())
                .ForMember(m => m.Position, o => o.Ignore())
                .ForMember(m => m.Workplace, o => o.Ignore())
                .ForMember(m => m.UserLineManager, o => o.Ignore())
                .ForMember(m => m.UserHRManager, o => o.Ignore());

            Mapper.CreateMap<Period, PeriodViewModel>()
                .Include<LeavingPeriod, LeavingPeriodViewModel>()
                .Include<OnboardingPeriod, OnboardingPeriodViewModel>();

            Mapper.CreateMap<PeriodViewModel, Period>()
                .Include<LeavingPeriodViewModel, LeavingPeriod>()
                .Include<OnboardingPeriodViewModel, OnboardingPeriod>();

            Mapper.CreateMap<Person, PersonViewModel>();

            Mapper.CreateMap<Position, PositionViewModel>().IgnoreAllUnmapped();
            Mapper.CreateMap<PositionViewModel, Position>()
                .ForMember(m => m.ID, o => o.Ignore())
                .ForMember(m => m.Created, o => o.Ignore())
                .ForMember(m => m.Modified, o => o.Ignore());

            Mapper.CreateMap<Project, ProjectViewModel>().
                ForMember(p => p.Status, o => o.MapFrom(p => p.Status))
                .ForMember(p => p.ProjectManager, o => o.MapFrom(p => p.ProjectManager));

            Mapper.CreateMap<ProjectInstance, ProjectInstanceViewModel>()
                .ForMember(p => p.Project, o => o.MapFrom(p => p.Project))
                .ForMember(p => p.Role, o => o.MapFrom(p => p.Role));

            Mapper.CreateMap<ProjectInstanceRoleChangeRequest, ProjectInstanceRoleChangeRequestViewModel>()
                .ForMember(p => p.ProjectInstance, o => o.MapFrom(p => p.ProjectInstance))
                .ForMember(p => p.OldRole, o => o.MapFrom(p => p.OldRole))
                .ForMember(p => p.NewRole, o => o.MapFrom(p => p.NewRole));

            Mapper.CreateMap<ProjectInstanceTransferRequest, ProjectInstanceTransferRequestViewModel>();

            Mapper.CreateMap<ProjectRole, ProjectRoleViewModel>();

            Mapper.CreateMap<ProjectStatus, ProjectStatusViewModel>();

            Mapper.CreateMap<Request, RequestViewModel>()
                .Include<ProjectInstanceRoleChangeRequest, ProjectInstanceRoleChangeRequestViewModel>()
                .Include<TransferRequest, TransferRequestViewModel>()
                .ForMember(r => r.Package, o => o.MapFrom(r => r.Package));

            Mapper.CreateMap<Role, RoleViewModel>();
            Mapper.CreateMap<RoleViewModel, Role>();

            Mapper.CreateMap<Contact, StaffViewModel>();

            Mapper.CreateMap<StaffTransferRequest, StaffTransferRequestViewModel>()
                .ForMember(r => r.Package, o => o.MapFrom(r => r.Package));;

            Mapper.CreateMap<Task, TaskViewModel>().IgnoreAllUnmapped();
            Mapper.CreateMap<TaskViewModel, Task>().IgnoreAllUnmapped();

            Mapper.CreateMap<Template, TemplateViewModel>();
            Mapper.CreateMap<TemplateViewModel, Template>();

            Mapper.CreateMap<Topic, TopicViewModel>().IgnoreAllUnmapped();
            Mapper.CreateMap<TopicViewModel, Topic>()
                .ForMember(m => m.Attachments, o => o.Ignore())
                .ForMember(m => m.Fields, o => o.Ignore())
                .ForMember(m => m.Tasks, o => o.Ignore())
                .ForMember(m => m.VoiceMessages, o => o.Ignore())
                .IgnoreAllUnmapped();

            Mapper.CreateMap<TopicAnswer, TopicAnswerViewModel>();
            Mapper.CreateMap<TopicAnswerViewModel, TopicAnswer>();

            Mapper.CreateMap<TopicField, TopicFieldViewModel>();
            Mapper.CreateMap<TopicFieldViewModel, TopicField>();

            Mapper.CreateMap<TopicGroup, TopicGroupViewModel>().IgnoreAllUnmapped();
            Mapper.CreateMap<TopicGroupViewModel, TopicGroup>().IgnoreAllUnmapped();

            Mapper.CreateMap<TopicQuestion, TopicQuestionViewModel>();
            Mapper.CreateMap<TopicQuestionViewModel, TopicQuestion>();

            Mapper.CreateMap<TransferRequest, TransferRequestViewModel>()
                .Include<ProjectInstanceTransferRequest, ProjectInstanceTransferRequestViewModel>()
                .Include<StaffTransferRequest, StaffTransferRequestViewModel>();

            Mapper.CreateMap<User, UserViewModel>()
                .ForMember(m => m.Email, o => o.MapFrom(m => m.Username))
                .ForMember(m => m.Roles, o => o.Ignore());
            Mapper.CreateMap<UserViewModel, User>()
                .ForMember(m => m.Username, o => o.Ignore())
                .ForMember(m => m.Created, o => o.Ignore())
                .ForMember(m => m.Modified, o => o.Ignore())
                .ForMember(m => m.Password, o => o.Ignore());

            Mapper.CreateMap<VoiceMessage, VoiceMessageViewModel>()
                .ForMember(m => m.FileId, o => o.Ignore());
            Mapper.CreateMap<VoiceMessageViewModel, VoiceMessage>()
                .ForMember(m => m.CoreService_Guid, o => o.Ignore())
                .ForMember(m => m.CoreService_Pin, o => o.Ignore())
                .ForMember(m => m.AudioData, o => o.Ignore());

            Mapper.CreateMap<VoiceMessage, VoiceMessageByPhoneViewModel>();
            Mapper.CreateMap<VoiceMessageByPhoneViewModel, VoiceMessage>();

            Mapper.CreateMap<VoiceMessage, VoiceMessageByPCViewModel>();
            Mapper.CreateMap<VoiceMessageByPCViewModel, VoiceMessage>();

            Mapper.CreateMap<Workplace, WorkplaceViewModel>();
            Mapper.CreateMap<WorkplaceViewModel, Workplace>()
                .ForMember(m => m.ID, o => o.Ignore())
                .ForMember(m => m.Created, o => o.Ignore())
                .ForMember(m => m.Modified, o => o.Ignore());

            Mapper.CreateMap<MasterList, MasterListViewModel>();
            Mapper.CreateMap<MasterListViewModel, MasterList>()
                .ForMember(m => m.ID, o => o.Ignore())
                .ForMember(m => m.Created, o => o.Ignore())
                .ForMember(m => m.Modified, o => o.Ignore())
                .ForMember(m => m.Name, o => o.MapFrom(y => y.Name.Trim()));

            Mapper.CreateMap<TemplateItem, TemplateItemViewModel>().IgnoreAllUnmapped();

            Mapper.CreateMap<Function, FunctionViewModel>().IgnoreAllUnmapped();
            Mapper.CreateMap<FunctionViewModel, Function>().IgnoreAllUnmapped();

            Mapper.CreateMap<ProcessGroup, ProcessGroupViewModel>().IgnoreAllUnmapped();
            Mapper.CreateMap<ProcessGroupViewModel, ProcessGroup>().IgnoreAllUnmapped();

            Mapper.CreateMap<Topic, ProLinkTopicViewModel>().IgnoreAllUnmapped();

            Mapper.CreateMap<LeaverPriorities, LeaverPrioritiesViewModel>();
            Mapper.CreateMap<LeaverPrioritiesViewModel, LeaverPriorities>();

            Mapper.CreateMap<OnboarderPriorities, OnboarderPrioritiesViewModel>(); 
            Mapper.CreateMap<OnboarderPrioritiesViewModel, OnboarderPriorities>();

            Mapper.CreateMap<Staff, ModeratedStaff>();
            Mapper.CreateMap<ModeratedStaff, Staff>();

            Mapper.CreateMap<Package, SelectListItem>()
                .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.ID.ToString()));

        }
    }
}