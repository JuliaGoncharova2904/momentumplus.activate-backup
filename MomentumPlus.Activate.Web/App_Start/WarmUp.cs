﻿using MomentumPlus.Activate.Services;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models;
using NLog;

namespace MomentumPlus.Activate.Web.App_Start
{
    public class WarmUp
    {
        private static Logger Logger = LogManager.GetCurrentClassLogger();

        public static void Start()
        {
            Logger.Info("Warming up");

            using (MomentumContext context = new MomentumContext())
            {
                IModuleService moduleService = new ModuleService(context);

                var moduleCritical = moduleService.GetFrameworkModel(Module.ModuleType.Critical);
                var moduleCriticalClone = moduleService.Clone(moduleCritical.ID);

                var moduleExperiences = moduleService.GetFrameworkModel(Module.ModuleType.Experiences);
                var moduleExperienceClone = moduleService.Clone(moduleExperiences.ID);
            }

            Logger.Info("Warm up compelete");
        }

    }
}