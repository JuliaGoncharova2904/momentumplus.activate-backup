﻿using System.Web.Mvc;
using System.Web.Routing;

namespace MomentumPlus.Activate.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            // Image handler
            routes.MapRoute(
                name: "ImageHandler",
                url: "image/{id}",
                defaults: new { controller = "Image", action = "Get" },
                namespaces: new[] { "MomentumPlus.Activate.Web.Controllers" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "MomentumPlus.Activate.Web.Controllers" }
            );
        }
    }
}