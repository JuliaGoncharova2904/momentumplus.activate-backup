﻿using System.Web;
using System.Web.Http;
using LightInject;
using MomentumPlus.Activate.Services;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Core;
using MomentumPlus.Activate.Web.Core.Framework;
using MomentumPlus.Activate.Web.Core.Home;
using MomentumPlus.Activate.Web.Core.Management;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models.People;

namespace MomentumPlus.Activate.Web
{
    public class LightInjectConfig
    {
        /// <summary>
        /// Register service implementations with service container for dependency injection
        /// </summary>
        public static void RegisterServices()
        {
            var container = new ServiceContainer();

            container.Register<MomentumContext, MomentumContext>(new PerScopeLifetime());
            container.Register<IRepositoriesUnitOfWork, RepositoriesUnitOfWork>(new PerScopeLifetime());

            container.Register<IUserService, UserService>(new PerScopeLifetime());

            container.Register<IAccountConfigService, AccountConfigService>(new PerScopeLifetime());
            container.Register<IAttachmentService, AttachmentService>(new PerScopeLifetime());
            container.Register<IContactService, ContactService>(new PerScopeLifetime());
            container.Register<IFileService, FileService>(new PerScopeLifetime());
            container.Register<IKeywordService, KeywordService>(new PerScopeLifetime());
            container.Register<IMasterListService, MasterListService>(new PerScopeLifetime());
            container.Register<IMasterListService, MasterListService>(new PerScopeLifetime());
            container.Register<IModuleService, ModuleService>(new PerScopeLifetime());
            container.Register<IRequestService, RequestService>(new PerScopeLifetime());
            container.Register<IRoleService, RoleService>(new PerScopeLifetime());
            container.Register<ISMSMessagesService, SMSMessageService>(new PerScopeLifetime());
            container.Register<ISystemService, SystemService>(new PerScopeLifetime());
            container.Register<ITemplateService, TemplateService>(new PerScopeLifetime());
            container.Register<ITemplateItemService, TemplateItemService>(new PerScopeLifetime());
            container.Register<ITopicGroupService, TopicGroupService>(new PerScopeLifetime());
            container.Register<ITopicService, TopicService>(new PerScopeLifetime());
            container.Register<ITopicQuestionService, TopicQuestionService>(new PerScopeLifetime());
            container.Register<IHandoverService, HandoverService>(new PerScopeLifetime());
            container.Register<IVoiceMessageService, VoiceMessageService>(new PerScopeLifetime());
            container.Register<IWorkplaceService, WorkplaceService>(new PerScopeLifetime());
            container.Register<IProjectService, ProjectService>(new PerScopeLifetime());
            container.Register<IModerationService, ModerationService>(new PerScopeLifetime());
            container.Register<IRequestService, RequestService>(new PerScopeLifetime());
            container.Register<IImageService, ImageService>(new PerScopeLifetime());
            container.Register<IPeopleService, PeopleService>(new PerScopeLifetime());
            container.Register<IProLinkService, ProLinkService>(new PerScopeLifetime());
            container.Register<IDocumentLibraryService, DocumentLibraryService>(new PerScopeLifetime());
            container.Register<IPositionService, PositionService>(new PerScopeLifetime());
            container.Register<IToDoItemService, ToDoItemService>(new PerScopeLifetime());
            container.Register<IFlaggingService, FlaggingService>(new PerScopeLifetime());

            // Inject Core Classes
            container.Register<ModuleCore>(new PerScopeLifetime());

            container.Register<MeetingsCore>(new PerScopeLifetime());
            container.Register<OnboardingCore>(new PerScopeLifetime());
            container.Register<RelationshipsCore>(new PerScopeLifetime());
            container.Register<SummaryCore>(new PerScopeLifetime());
            container.Register<TasksCore>(new PerScopeLifetime());

            container.Register<InHandoverCore>(new PerScopeLifetime());
            container.Register<InUseCore>(new PerScopeLifetime());

            container.Register<AttachmentCore>(new PerScopeLifetime());
            container.Register<DocumentLibraryCore>(new PerScopeLifetime());      
            container.Register<EmployeeCore>(new PerScopeLifetime());
            container.Register<PackageCore>(new PerScopeLifetime());
            container.Register<PersonCore>(new PerScopeLifetime());          
            container.Register<ProjectCore>(new PerScopeLifetime());
            container.Register<TaskCore>(new PerScopeLifetime());
            container.Register<TopicCore>(new PerScopeLifetime());
            container.Register<WorkplaceCore>(new PerScopeLifetime());
            container.Register<PositionCore>(new PerScopeLifetime());

            container.Register<IAuditService>(
                factory => new AuditService(factory.GetInstance<MomentumContext>(),
                    factory.GetInstance<IMeetingService>(),
                    factory.GetInstance<IContactService>(),
                    GetLoggedInUser(container)),
                new PerScopeLifetime());
            container.Register<IMeetingService>(
                factory => new MeetingService(factory.GetInstance<MomentumContext>(), GetLoggedInUser(container)),
                new PerScopeLifetime());
            container.Register<IPackageService>(
                factory => new PackageService(factory.GetInstance<MomentumContext>(), 
                    factory.GetInstance<IMeetingService>(),
                    GetLoggedInUser(container)),
                new PerScopeLifetime());
            container.Register<ITaskService>(
                factory => new TaskService(factory.GetInstance<MomentumContext>(), GetLoggedInUser(container)),
                new PerScopeLifetime());

            // Enable dependency injection for all MVC and Web API controllers
            container.RegisterControllers();
            container.EnableMvc();
            container.RegisterApiControllers();
            container.EnablePerWebRequestScope();
            container.EnableWebApi(GlobalConfiguration.Configuration);
        }

        /// <summary>
        /// Get the currently logged in user
        /// </summary>
        /// <param name="container">
        /// A service container with a registered <c>IUserService</c> instance
        /// </param>
        /// <returns>A <c>User</c> object</returns>
        private static User GetLoggedInUser(ServiceContainer container)
        {
            if (AppSession.Current.CurrentUser != null)
                return AppSession.Current.CurrentUser;

            var userService = container.GetInstance<IUserService>();

            AppSession.Current.CurrentUser =
                userService.GetByUsername(HttpContext.Current.User.Identity.Name);

            return AppSession.Current.CurrentUser;
        }
    }
}