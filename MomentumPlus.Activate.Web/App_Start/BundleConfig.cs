﻿using System.Web;
using System.Web.Optimization;

namespace MomentumPlus.Activate.Web
{
    public class BundleConfig
    {
        /// <summary>
        /// Register the minified CSS and JavaScript bundles used on the
        /// front end of the application.
        /// </summary>
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/bundles/style.css")
                .Include("~/Content/common/bootstrap/css/bootstrap.css", new CssRewriteUrlTransform())
                .Include("~/Content/common/bootstrap-combobox/css/bootstrap-combobox.css")
                .Include("~/Content/common/bootstrap-datepicker/css/datepicker3.css")
                .Include("~/Content/common/bootstrap-select/dist/css/bootstrap-select.min.css")
                .Include("~/Content/common/bootstrap-tokenfield/bootstrap-tokenfield.css")
                .Include("~/Content/common/d3-tip/styles.css")
                .Include("~/Content/common/ms-Dropdown/css/msdropdown/dd.css", new CssRewriteUrlTransform())
                .Include("~/Content/common/typeaheadjs.css")
                .Include("~/Content/css/overrides.css")
                .Include("~/Content/css/custom.css", new CssRewriteUrlTransform())
                .Include("~/Content/css/my-package.css")
                .Include("~/Content/css/framework.css")
                .Include("~/Content/css/print.css")
                .Include("~/Content/css/menu.css")
                .Include("~/Content/css/loading-animation.css")
                .Include("~/bower_components/bxslider-4/dist/jquery.bxslider.css")
                .Include("~/bower_components/fontawesome/css/font-awesome.css")
            );

            bundles.Add(new ScriptBundle("~/bundles/script.js")
                .Include("~/bower_components/jquery/dist/jquery.js")
                .Include("~/Content/common/jquery-debounce/jquery.ba-throttle-debounce.js")
                //.Include("~/Content/common/jquery-3.1.1.js")
                .Include("~/Content/lib/jquery-validation/dist/jquery.validate.min.js")
                .Include("~/Content/lib/jquery-validation-unobtrusive/jquery.validate.unobtrusive.min.js")
                .Include("~/Content/lib/jquery-ajax-unobtrusive/jquery.unobtrusive-ajax.min.js")
                .Include("~/Content/common/jquery.ba-outside-events.js")
                .Include("~/Content/common/jquery.unobtrusive-ajax.js")
                .Include("~/bower_components/bxslider-4/dist/jquery.bxslider.js")
                .Include("~/Content/common/jquery.cookie.js")
                .Include("~/Content/common/bootstrap/js/bootstrap.js")
                .Include("~/Content/common/bootstrap-combobox/js/bootstrap-combobox.js")
                .Include("~/Content/common/bootstrap-datepicker/js/bootstrap-datepicker.js")
                .Include("~/Content/common/bootstrap-tokenfield/bootstrap-tokenfield.js")
                .Include("~/Content/common/snap.js")
                .Include("~/Content/common/typeahead.bundle.js")
                .Include("~/Content/common/angular/angular.js")
                .Include("~/Content/common/ui-bootstrap-tpls-0.11.0.js")
                .Include("~/Content/common/spin.js")
                .Include("~/Content/common/truncate.js")
                //.Include("~/Content/common/truncate.js")
                .Include("~/Content/js/activate.js")
                .Include("~/Content/js/people.js")
                .Include("~/Content/js/projects.js")
                .Include("~/Content/js/widgets.js")
                .Include("~/Content/js/search.js")
                .Include("~/Content/js/offscreen.js")
                .Include("~/Content/common/elastic.js")
                .Include("~/Content/common/ms-Dropdown/js/msdropdown/jquery.dd.js")
                .Include("~/Content/common/bootstrap-select/dist/js/bootstrap-select.min.js")
            );

            bundles.Add(new ScriptBundle("~/bundles/attachments.js")
                .Include("~/Content/js/attachments.js")
            );

            bundles.Add(new ScriptBundle("~/bundles/cloning.js")
                .Include("~/Content/js/cloning.js")
            );

            bundles.Add(new ScriptBundle("~/bundles/package-form.js")
                .Include("~/Content/js/package-form.js")
            );

            bundles.Add(new ScriptBundle("~/bundles/package-transfer.js")
                .Include("~/Content/js/package-transfer.js")
            );

            bundles.Add(new ScriptBundle("~/bundles/relationships.js")
                .Include("~/Content/common/d3.js")
                .Include("~/Content/common/d3-tip/index.js")
                .Include("~/Content/js/relationships.js")
            );

            bundles.Add(new ScriptBundle("~/bundles/template.js")
                .Include("~/Content/js/template.js")
            );

            bundles.Add(new ScriptBundle("~/bundles/topics.js")
                .Include("~/Content/js/topics.js")
            );

            bundles.Add(new ScriptBundle("~/bundles/voice-messages.js")
                .Include("~/Content/js/voice-messages.js")
            );

            bundles.Add(new ScriptBundle("~/bundles/people.js")
                .Include("~/Content/js/people.js")
            );

            bundles.Add(new ScriptBundle("~/bundles/pro-link.js")
                .Include("~/Content/js/pro-link.js")
            );

            bundles.Add(new ScriptBundle("~/bundles/tasks.js")
                .Include("~/Content/js/tasks.js")
            );

            bundles.Add(new ScriptBundle("~/bundles/to-do-list.js")
                .Include("~/Content/js/to-do-list.js")
            );

            bundles.Add(new ScriptBundle("~/bundles/task-priority-reorder.js")
                .Include("~/Content/common/jquery-ui.min.js")
                .Include("~/Content/js/task-priority-reorder.js")
            );

            bundles.Add(new ScriptBundle("~/bundles/meeting-quick-edit.js")
                .Include("~/Content/js/meeting-quick-edit.js")
            );

            bundles.Add(new ScriptBundle("~/bundles/timeline.js")
                .Include("~/Content/js/moment.js")
                .Include("~/Content/js/moment-range.js")
                .Include("~/Content/js/angular-sanitize.js")
                .Include("~/Content/js/angular-filter.js")
                .Include("~/Content/js/angular-horizontal-timeline.js")
                .Include("~/Content/js/timeline.js")
            );

            bundles.Add(new ScriptBundle("~/bundles/handover-form.js")
                .Include("~/Content/js/moment.js")
                .Include("~/Content/js/handover-form.js")
            );

            BundleTable.EnableOptimizations = false;
        }
    }
}