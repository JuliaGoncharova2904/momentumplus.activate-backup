﻿How to Migrate:

If you change the Entities:

1) Open Package Manager Console [Tools –> Library Package Manager –> Package Manager Console]
2) In the "Default Project" dropdown select "MomentumPlus.Activate.Data"
3) in the console type "Add-Migration MyMigrationName" where MyMigrationName is a short description of your changes (date is applied automatically)
4) This command wipes the database: in the console type "Update-Database" 
5) Run your code

Commands:
Add-Migration MyMigrationName
Update-Database


To turn off migrations 
1) Open Package Manager Console [Tools –> Library Package Manager –> Package Manager Console]
2) In the "Default Project" dropdown select "MomentumPlus.Activate.Data"
3) in the console type  "Disable-Migrations"

Commands:
Disable-Migrations


Backing up your database before adding a migration
1) Open MS SQL Server MAnagement Studio
2) 





Settings to setup a new database: