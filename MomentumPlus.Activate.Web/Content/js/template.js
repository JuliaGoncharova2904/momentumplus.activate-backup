﻿angular.module('momentumPlus.activate')

/**
* Controller for the template managera and template transfer views.
*/
.controller('TemplateCtrl', ['$http', '$scope', '$window', function ($http, $scope, $window)
{

    var systems = 0;
    var businessProcesses = 1;

    $scope.alerts = [];

    /**
    * Remove the alert at the given index.
    */
    $scope.closeAlert = function (index)
    {
        $scope.alerts.splice(index, 1);
    };

    /**
    * Reloads the current page with the given template ID.
    */
    var reloadPage = function ()
    {
        $window.location.href = '?selectedTemplate=' + $scope.selectedTemplate;
    };

    /**
    * Get the template items that have been enabled - critical and experiences.
    */
    var getEnabledItems = function ()
    {
        var templateItemsCritical = $scope.criticalTopicGroups.concat($scope.criticalTopics, $scope.criticalTasks);
        var templateItemsExperiences = $scope.experiencesTopicGroups.concat($scope.experiencesTopics, $scope.experiencesTasks);

        // Filter only enabled
        templateItemsCritical = templateItemsCritical.filter(function (item) { return item.Enabled; });
        templateItemsExperiences = templateItemsExperiences.filter(function (item) { return item.Enabled; });

        return {
            templateItemsCritical: templateItemsCritical,
            templateItemsExperiences: templateItemsExperiences
        };
    };

    /**
    * Reload the current page, passing the newly selected template ID.
    */
    $scope.changeTemplate = function ()
    {
        reloadPage();
        $scope.selectedTemplate = $scope.currentTemplate;
    };

    /**
    * Save template changes.
    */
    $scope.saveChanges = function ()
    {
        var enabledItems = getEnabledItems();
        var processesChildrenFound = false;
        var systemsChildrenFound = false;

        enabledItems.templateItemsCritical.forEach(function (item)
        {
            if (item.ParentTopicType != null && item.ParentTopicType == businessProcesses)
            {
                processesChildrenFound = true;
            } else if (item.ParentTopicType != null && item.ParentTopicType == systems)
            {
                systemsChildrenFound = true;
            }
        });

        if (processesChildrenFound && systemsChildrenFound)
        {
            $scope.saving = true;
        } else
        {
            $scope.alerts = [{
                type: 'danger',
                msg: 'A template must include at least one topic from Business Processes topic group and one topic from Systems topic group.'
            }];
            $scope.saving = false;
            return;
        }

        var updatePackages = false;
        var newItemsCritical = enabledItems.templateItemsCritical.filter(function (i) { return i.IsNew; }).length;
        var newItemsExperiences = enabledItems.templateItemsExperiences.filter(function (i) { return i.IsNew; }).length;

        if (newItemsCritical || newItemsExperiences)
        {
            var totalNewItems = newItemsCritical + newItemsExperiences;
            updatePackages = confirm(
                'Click OK to push ' +
                totalNewItems +
                ' new item' + (totalNewItems > 1 ? 's' : '') +
                ' to existing packages, or Cancel to save without updating packages.'
            );
        }

        /**
        * Get a lightweight template item for sending to the server.
        */
        var getBasicItem = function (item)
        {
            console.log(item);

            return {
                I: item.SourceReferenceId,
                P: item.SourceParentReferenceId,
                T: item.Type,
                N: item.IsNew
            };
        };

        /**
        * Set all IsNew flags on the given items to false.
        */
        var resetIsNew = function (items)
        {
            items.forEach(function (item)
            {
                item.IsNew = false;
            });
        };

        //console.log(enabledItems.templateItemsCritical);


        var lightTemplateItemsCritical = enabledItems.templateItemsCritical.map(getBasicItem);
        var lightTemplateItemsExperiences = enabledItems.templateItemsExperiences.map(getBasicItem);

        var enabledModules = {
            E: $scope.enabledModules.experiences,
            R: $scope.enabledModules.relationships,
            Pr: $scope.enabledModules.projects,
            Pe: $scope.enabledModules.people
        };

        var postData = {
            templateId: $scope.selectedTemplate,
            lightTemplateItemsCritical: lightTemplateItemsCritical,
            lightTemplateItemsExperiences: lightTemplateItemsExperiences,
            enabledModules: enabledModules,
            updatePackages: updatePackages
        }

        //console.log(postData);


        $http.post('/Framework/Template/UpdateTemplateItems', postData)
            .success(function (data, status, headers, config)
            {
                var message = 'Template updated successfully.';

                if (updatePackages)
                {
                    message = 'Template and packages updated successfully.';
                }

                $scope.alerts = [{
                    type: 'success',
                    msg: message
                }];

                $scope.saving = false;

                resetIsNew(enabledItems.templateItemsCritical);
                resetIsNew(enabledItems.templateItemsExperiences);

                $scope.$broadcast('TEMPLATE_SAVED');
            })
            .error(function (data, status, headers, config)
            {
                $scope.alerts = [{
                    type: 'danger',
                    msg: 'Something went wrong, please try again.'
                }];

                $scope.saving = false;
            });
    };

    /**
    * Clear template changes - simply reload the page
    */
    $scope.clearChanges = function ()
    {
        reloadPage();
    };

    /**
    * Transfer items from one template to another.
    */
    $scope.transferTemplateItems = function ()
    {
        $scope.saving = true;

        var enabledItems = getEnabledItems();

        var getSourceReferenceId = function (item)
        {
            return item.SourceReferenceId;
        };

        var sourceReferenceIdsCritical = enabledItems.templateItemsCritical.map(getSourceReferenceId);
        var sourceReferenceIdsExperiences = enabledItems.templateItemsExperiences.map(getSourceReferenceId);

        var postData = {
            templateIdSource: $scope.selectedTemplate,
            templateIdDest: $scope.destinationTemplate,
            sourceReferenceIdsCritical: sourceReferenceIdsCritical,
            sourceReferenceIdsExperiences: sourceReferenceIdsExperiences
        }

        $http.post('/Framework/Template/TransferTemplateItems', postData)
            .success(function (data, status, headers, config)
            {
                $scope.alerts = [{
                    type: 'success',
                    msg: 'Items transferred successfully.'
                }];

                $scope.saving = false;
            })
            .error(function (data, status, headers, config)
            {
                $scope.alerts = [{
                    type: 'danger',
                    msg: 'Something went wrong, please try again.'
                }];

                $scope.saving = false;
            });
    };

    $scope.getCoreTasks = function ()
    {
        var coreTasks;

        if ($scope.criticalTasks !== null)
        {
            coreTasks = $scope.criticalTasks.concat($scope.experiencesTasks).filter(function (task)
            {
                return task.IsCore;
            });
        }

        return coreTasks;
    };

}])

/**
* Directive for the template item selector.
*/
.directive('templateItemSelector', [function ()
{
    return {
        restrict: 'E',
        templateUrl: 'template-item-selector.html',
        scope: {
            topicGroups: '=',
            topics: '=',
            tasks: '=',
            coreTasks: '='
        },
        controller: ['$scope', '$window', function ($scope, $window)
        {
            var businessProcesses = 1;

            // Pagination settings
            $scope.pageSize = 10;

            $scope.activeTopicGroup = null;

            $scope.selectedTopicGroupTopics = [];
            $scope.topicPageNo = 1;
            $scope.topicPage = [];
            $scope.totalTopicPages = 1;

            $scope.selectedTopicTasks = [];
            $scope.taskPageNo = 1;
            $scope.taskPage = [];
            $scope.totalTaskPages = 1;

            $scope.selectedFunctions = [];

            /**
            * Get the source reference IDs of the enabled template items before modification.
            */
            var getEnabledSourceReferenceIds = function (items)
            {
                if (!items)
                {
                    return [];
                }

                var enabledItems = items.filter(function (item)
                {
                    return item.Enabled;
                });

                // Get the source reference IDs of the enabled items
                var enabledSourceReferenceIds = enabledItems.map(function (item)
                {
                    return item.SourceReferenceId;
                });

                return enabledSourceReferenceIds;
            };

            /**
            * Find the item in the given array with the given source reference ID.
            */
            var getItemBySourceReferenceId = function (items, sourceReferenceId)
            {
                return items.filter(function (item)
                {
                    return item.SourceReferenceId === sourceReferenceId;
                })[0];
            };

            /**
            * Filter a list of items based on their source parent reference ID.
            */
            var getItemsBySourceParentReferenceId = function (items, sourceParentReferenceId)
            {
                return items.filter(function (item)
                {
                    return item.SourceParentReferenceId === sourceParentReferenceId;
                });
            };

            /**
            * Select the topic group with the given ID.
            */
            $scope.selectTopicGroup = function (topicGroup)
            {
            	// Deselect all groups
                angular.forEach($scope.topicGroups, function (topicGroup)
                {
                    topicGroup.isActive = false;
                });

                // Select this group
                topicGroup.isActive = true;

                $scope.activeTopicGroup = topicGroup;

                // Get all topics in group and page accordingly
                $scope.selectedTopicGroupTopics = getItemsBySourceParentReferenceId($scope.topics, topicGroup.SourceReferenceId);

                var checkedElements = 0;
                angular.forEach($scope.selectedTopicGroupTopics,function (checkboxValue, checkboxKey)
                {
                	if (checkboxValue.Enabled)
                	{
                		checkedElements++;
                	}
                });

                $("input[ng-model=allTopics]").prop('checked', ($scope.selectedTopicGroupTopics.length == checkedElements));

                $scope.selectedTopicGroupTopics.sort(function (t1, t2)
                {
                    return t1.Name < t2.Name ? -1 : t1.Name > t2.Name;
                });

                // Filter by Pro-Link Function
                if ($scope.$parent.$parent.proLinkFunctions && $scope.isProcessesTopicGroup(topicGroup))
                {
                    if ($scope.selectedFunctions.length)
                    {
                        $scope.selectedTopicGroupTopics = $scope.selectedTopicGroupTopics.filter(function (val)
                        {
                            return $scope.selectedFunctions.indexOf(String(val.FunctionReferenceId || '')) > -1;
                        });
                    }
                    else
                    {
                        $scope.selectedTopicGroupTopics = [];
                    }
                }

                $scope.topicPageNo = 1;
                $scope.topicPage = $scope.selectedTopicGroupTopics.slice(0, $scope.pageSize);
                $scope.totalTopicPages = Math.ceil($scope.selectedTopicGroupTopics.length / $scope.pageSize);
                // Reset task page
                $scope.taskPage = null;
            };

            /**
            * Select the topic with the given ID.
            */
            $scope.selectTopic = function (topic)
            {
                // Deselect all topics
                angular.forEach($scope.topics, function (topic)
                {
                    topic.isActive = false;
                });

                // Select this topic
                topic.isActive = true;


                // Get all tasks in topic and page accordingly
                $scope.selectedTopicTasks = getItemsBySourceParentReferenceId($scope.tasks, topic.SourceReferenceId);
                $scope.taskPageNo = 1;
                $scope.taskPage = $scope.selectedTopicTasks.slice(0, $scope.pageSize);
                $scope.totalTaskPages = Math.ceil($scope.selectedTopicTasks.length / $scope.pageSize);

                var checkedElements = 0;
                angular.forEach($scope.selectedTopicTasks, function (checkboxValue, checkboxKey) {
                	if (checkboxValue.Enabled) {
                		checkedElements++;
                	}
                });

                $("input[ng-model=allTasks]").prop('checked', ($scope.selectedTopicTasks.length == checkedElements));

            };

            /**
            * Enable or disable all topic groups.
            */
            $scope.toggleAllTopicGroups = function (enabled)
            {
                angular.forEach($scope.topicGroups, function (topicGroup)
                {
                    $scope.toggleTopicGroup(topicGroup, enabled);
                });
            };

            /**
            * Enable or disable all topics in the selected topic group.
            */
            $("[ng-model=allTopics]").on("click", function ()
            {
            	var enabled = $(this).prop('checked');
            	angular.forEach($scope.selectedTopicGroupTopics, function (topic)
            	{
            		$scope.toggleTopic(topic, enabled);
            	});
            });
			//does not working from first time
            /*$scope.toggleAllTopics = function (enabled)
            {
                angular.forEach($scope.selectedTopicGroupTopics, function (topic)
                {
                    $scope.toggleTopic(topic, enabled);
                });
            };*/

            /**
            * Enable or disable all tasks in the selected topic.
            */
            $("[ng-model=allTasks]").on("click", function ()
            {
            	var enabled = $(this).prop('checked');
            	angular.forEach($scope.selectedTopicTasks, function (task)
            	{
            		task.Enabled = enabled;
            	});
            });
        	//does not working from first time
            /*$scope.toggleAllTasks = function (enabled)
            {
                angular.forEach($scope.selectedTopicTasks, function (task)
                {
                    task.Enabled = enabled;
                });
            };*/

            /**
            * Toggle a topic group in the template along with
            * all child topics.
            */
            $scope.toggleTopicGroup = function (topicGroup, enabled)
            {
                if (angular.isDefined(enabled))
                {
                    topicGroup.Enabled = enabled;
                }

                if (!topicGroup.Enabled)
                {
                    var topics = getItemsBySourceParentReferenceId($scope.topics, topicGroup.SourceReferenceId);

                    angular.forEach(topics, function (topic)
                    {
                        $scope.toggleTopic(topic, topicGroup.Enabled);
                    });
                } else
                {
                    $scope.selectTopicGroup(topicGroup);

                    if (originalTopicGroups.indexOf(topicGroup.SourceReferenceId) === -1)
                    {
                        topicGroup.IsNew = true;
                    }
                }
            };

            /**
            * Enable a topic in the template along with all child tasks.
            */
            $scope.toggleTopic = function (topic, enabled)
            {
                if (angular.isDefined(enabled))
                {
                    topic.Enabled = enabled;
                }

                if (!topic.Enabled)
                {
                    var tasks = getItemsBySourceParentReferenceId($scope.tasks, topic.SourceReferenceId);

                    angular.forEach(tasks, function (task)
                    {
                        task.Enabled = topic.Enabled;
                    });
                } else
                {
                    $scope.selectTopic(topic);

                    // Enable parent topic group
                    var topicGroup = getItemBySourceReferenceId($scope.topicGroups, topic.SourceParentReferenceId);
                    topicGroup.Enabled = true;

                    if (originalTopics.indexOf(topic.SourceReferenceId) === -1)
                    {
                        topic.IsNew = true;
                    }
                }
            };

            $scope.toggleTask = function (task)
            {
                if (task.Enabled)
                {
                    // Enable parent topic and group
                    var topic = getItemBySourceReferenceId($scope.topics, task.SourceParentReferenceId);
                    topic.Enabled = true;
                    var topicGroup = getItemBySourceReferenceId($scope.topicGroups, topic.SourceParentReferenceId);
                    topicGroup.Enabled = true;

                    if (originalTasks.indexOf(task.SourceReferenceId) === -1)
                    {
                        task.IsNew = true;
                    }
                }
            };

            /**
             * Check whether or not a task belongs to a ProLink topic
             */
            $scope.isTaskProLink = function (topics, sourceParentReferenceId)
            {
                var topic = getItemBySourceReferenceId(topics, sourceParentReferenceId);
                if (topic.FunctionReferenceId != null)
                {
                    return true;
                } else
                {
                    return false;
                }
            };

            $scope.isProcessesTopicGroup = function (topicGroup)
            {
                if (!topicGroup)
                    return false;

                var topics = getItemsBySourceParentReferenceId($scope.topics, topicGroup.SourceReferenceId);
                return topics && topics.length && topics[0].ParentTopicType === businessProcesses;
            };

            $scope.filterTopicsByFunction = function ()
            {
                $scope.selectTopicGroup($scope.activeTopicGroup);
            };

            if ($scope.coreTasks)
            {
                $scope.selectedTopicTasks = $scope.coreTasks;
                $scope.pageSize = $scope.selectedTopicTasks.length;
            }

            // Get the original template items before modification
            var originalTopicGroups = getEnabledSourceReferenceIds($scope.topicGroups);
            var originalTopics = getEnabledSourceReferenceIds($scope.topics);
            var originalTasks;

            if ($scope.coreTasks)
            {
                originalTasks = getEnabledSourceReferenceIds($scope.coreTasks);
            }
            else
            {
                originalTasks = getEnabledSourceReferenceIds($scope.tasks);
            }

            /**
            * Watch for changes in topic page number and update shown topics.
            */
            $scope.$watch('topicPageNo', function ()
            {
                var begin = ($scope.topicPageNo - 1) * $scope.pageSize;
                var end = begin + $scope.pageSize;
                $scope.topicPage = $scope.selectedTopicGroupTopics.slice(begin, end);
            });

            /**
            * Watch for changes in task page number and update shown tasks.
            */
            $scope.$watch('taskPageNo', function ()
            {
                var begin = ($scope.taskPageNo - 1) * $scope.pageSize;
                var end = begin + $scope.pageSize;
                $scope.taskPage = $scope.selectedTopicTasks.slice(begin, end);
            });

            /**
            * When a template is saved ensure that the original template items array
            * contains any newly selected items.
            */
            $scope.$on('TEMPLATE_SAVED', function ()
            {
                originalTopicGroups = getEnabledSourceReferenceIds($scope.topicGroups);
                originalTopics = getEnabledSourceReferenceIds($scope.topics);

                if ($scope.coreTasks)
                {
                    originalTasks = getEnabledSourceReferenceIds($scope.coreTasks);
                }
                else
                {
                    originalTasks = getEnabledSourceReferenceIds($scope.tasks);
                }
            });
        }]
    };
}]);
