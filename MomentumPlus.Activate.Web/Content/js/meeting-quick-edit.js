﻿/**
* Augment the ACTIVATE JavaScript module with the methods required for
* the meeting quick edit functionality.
*/
var ACTIVATE = (function (module)
{

    /**
     * Initialise quick meeeting edit.
     */
    module.initialiseMeetingQuickEdit = function ()
    {
        $("body").on("click", ".meetingEdit", function (e)
        {
            e.preventDefault();

            //Close pop-up if click to edit meeting
            $(".close").trigger("click");

            var id = $(this).closest('.meeting').data('id');
            $.ajax({
                method: "GET",
                url: '/Home/Meetings/GetMeeting',
                data: { id: id },
                success: function (data)
                {
                    $('#meeting-quick-edit-modal').remove();
                    $('#labels').after(data);
                    $('#meeting-quick-edit-modal').modal('show');
                    module.initialiseComponents();
                },
                error: function (e)
                {
                    alert('There was an error getting the required data. Please report this problem.');
                }
            });
        });
        //Close pop-up if click outside

        $(document).mouseup(function (e)
        {
            var container = $(".timeline-view");

            if (!container.is(e.target)
                && container.has(e.target).length === 0)
            {
                $(".timeline-view").children(".close").trigger("click");
            }
        });

    };

    module.initialiseMeetingQuickEditSubmit = function ()
    {
        $("body").on("submit", "form.popup", function (event)
        {
            event.preventDefault();
            var form = $(this);
            var date = $("#Start").val();
            var hours = $("#StartHours").val();
            var mins = $("#StartMins").val();
            var day = moment(date).date();
            var month = moment(date).month() + 1;
            var year = moment(date).year();

            if (hours != "" && mins != "")
            {
                hours = hours > 9 ? "" + hours : "0" + hours;
                mins = mins > 9 ? "" + mins : "0" + mins;
                month = month > 9 ? "" + month : "0" + month;
                day = day > 9 ? "" + day : "0" + day;
                var timezone = moment().format().substr(moment().format().length - 5);
                var eventId = $("#ID").val();
                var timelineId = $(".meeting[data-id='" + eventId + "'").closest('.timeline-container').attr('id');

                var mDate = moment(day + '-' + month + '-' + year, "DD-MM-YYYY");

                $.ajax({
                    method: 'POST',
                    url: '/Home/Meetings/UpdateMeeting',
                    data: form.serialize(),
                    success: function (data)
                    {
                        location.reload();
                    },
                    error: function (e)
                    {
                        alert('There was an error updating the meeting. Please report this problem.');
                    }
                });
                $(form).closest('.modal').modal('hide');
                id = null;
            } else
            {
                alert('Please select a valid time.');
            }
        });
    };

    module.initialiseOffscreen = function ()
    {
        function resize()
        {
            $('.timeline-view').offscreen({
                rightClass: 'right-edge',
                leftClass: 'left-edge',
                topClass: 'top-edge',
                bottomClass: 'bottom-edge',
                smartResize: true
            });
        };

        $(window).resize(function ()
        {
            resize();
        });

        $(function ()
        {
            resize();
        });
    };

    $(function ()
    {
        module.initialiseMeetingQuickEdit();
        module.initialiseMeetingQuickEditSubmit();
        module.initialiseOffscreen();
    });

    return module;

})(ACTIVATE || {});