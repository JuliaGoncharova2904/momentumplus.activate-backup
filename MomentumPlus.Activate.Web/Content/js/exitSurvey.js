﻿$(document).ready(function () {

    $(".type-answer").selectpicker({
        style: 'select-class',
        dropupAuto: false
    });

    $("#typeAnswer").change(function () {
        var _this = this;
        $(".answer").remove();
        $("#countAnswer").val("");

        if (($(_this).val() === "0") || ($(_this).val() === "1")) {
            $(".counter-block").attr("style", "");
        } else {
            $(".counter-block").attr("style", "display: none;");
        }
    });

    $("#countAnswer").change(function () {
        var count = $(this).val();

        if ((($("#typeAnswer").val() === "0") || ($("#typeAnswer").val() === "1")) && (count !== "0")) {
            var answers = $(".answer");

            var answer = '<input type="text" placeholder="Type your answer" class="form-control answer" name="Answers">';
            if (answers.length > 0 && count > answers.length) {
                for (var i = 0; i < (count - answers.length); i++) {
                    $(".answer").last().after(answer);
                }
            } else if (answers.length > 0 && count < answers.length) {
                for (var i = 0; i < (answers.length - count) ; i++) {
                    $($(".answer").last()).remove();
                }
            } else {
                var newContent = "";
                for (var i = 0; i < count; i++) {
                    newContent += answer;
                }
                $(".answer-block").append(newContent);
            }
        }
    });

    $("#Name").change(function () {
        if ($("#Name").val() !== "") {
            $("#saveSurvey").removeAttr("disabled");
            $("#addSection").removeAttr("disabled");
        }
        else {
            $("#saveSurvey").attr("disabled", "");
            $("#addSection").attr("disabled", "");
        }
    });

    $("#close-survey").click(function () {
        window.location.replace("/Home/ExitSurveyQuestion");
    });

});