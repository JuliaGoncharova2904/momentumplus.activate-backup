﻿/**
* Augment the ACTIVATE JavaScript module with the methods required for
* the projects module.
*/
var ACTIVATE = (function (module) {

    /**
    * Initialise confirmation dialog on quick add project form.
    */
    module.initialiseProjectConfirmation = function () {
        $('#project-confirmation').click(function (e) {
            var message;

            if ($('#SelectExisting').is(':checked')) {
                var selectedOption = $('#ExistingProjectId option:selected');

                if (!selectedOption.val()) {
                    alert('Please select a project.');
                    return false;
                }

                message = 'Are you sure you would like to add the Project Card \'' + selectedOption.text() + '\'?'
            } else {
                var projectName = $('#ProjectName').val();

                if (!projectName) {
                    alert('Please enter a project name.');
                    return false;
                }

                var projectId = $('#ProjectId').val();
                var project = projectName + ' - ' + projectId;
                message = 'Are you sure you would like to submit the Project with the name \'' + project + '\' for approval?';
            }

            if (!confirm(message)) {
                var returnUrl = $(this).data('return-url');

                if (returnUrl) {
                    window.location = returnUrl;
                }

                return false;
            }
        });
    };

    /**
    * Initialise confirmation dialog on project transfer form.
    */
    module.initialiseProjectTransferConfirmation = function () {
        $('#project-transfer-confirmation').click(function (e) {
            var selectedOption = $('#DestinationId option:selected');

            if (!selectedOption.val()) {
                alert('Please select a target package.');
                return false;
            }

            var projectName = $('#ProjectName').text();
            var message = 'Are you sure you would like to transfer the Project Card \'' + projectName + '\' to the Package \'' + selectedOption.text() + '\'?'

            if (!confirm(message)) {
                var returnUrl = $(this).data('return-url');

                if (returnUrl) {
                    window.location = returnUrl;
                }

                return false;
            }
        });
    };

    module.initialiseProjectCardPriority = function() {
        $('.project-priority').click(function (e) {
            e.stopPropagation();
            e.preventDefault();
        });

        $('.project-priority').change(function (e) {
            var priority = this.value;
            var div = $(this).closest(".project-card");
            var id = div.data('id');
            $.ajax({
                method: 'POST', url: '/Home/Projects/UpdatePriority', data: { id: id, priorityValue: priority },
                success: function(success) {
                    div.removeClass("project-priority-High");
                    div.removeClass("project-priority-Medium");
                    div.removeClass("project-priority-Low");
                    switch(priority) {
                        case "1":
                            div.addClass("project-priority-Low");
                            break;
                        case "2":
                            div.addClass("project-priority-Medium");
                            break;
                        case "3":
                            div.addClass("project-priority-High");
                            break;
                    }
                },
                error: function () {
                    alert('There was an error updating the project. Please report this problem.');
                }
            });
        });
    };

    module.ProjectBudgetValidation = function () {
        $('#Project_Budget').keyup(function () {
            if ($('#Project_Budget').val() > 999999999) {
                var input = $('#Project_Budget').val().toString();
                input = input.substring(0, 9);
                $('#Project_Budget').val(parseInt(input));
            }
        });
    };

    $(function () {
        module.initialiseProjectConfirmation();
        module.initialiseProjectTransferConfirmation();
        module.initialiseProjectCardPriority();
        module.ProjectBudgetValidation();
    });

    return module;

})(ACTIVATE || {});
