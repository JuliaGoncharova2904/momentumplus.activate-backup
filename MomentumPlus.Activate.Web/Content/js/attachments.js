﻿/**
* Augment the ACTIVATE JavaScript module with the methods required for
* the attachment form.
*/
var ACTIVATE = (function (module) {

    /**
    * Initialise clear button which clears the current attachment.
    */
    module.initialiseClearButton = function () {
        $('#clear-file').click(function (e) {
            e.preventDefault();

            if (confirm('Are you sure you want to replace the current file?')) {
                var attachmentType = $(this).data('attachment-type');

                $(this).closest('.form-group').hide();
                $('#FilePath').val(null);
                $('#FileId').val(null);
                $('#file-controls').removeClass('hidden');

                if (attachmentType === 'upload') {
                    // No action required
                    $('#file-location-radio').prop('checked', true).change();
                } else if (attachmentType === 'location') {
                    // Show location input control
                    $('#file-location-radio').prop('checked', true).change();
                }
            }
        });

        $(".radio-inline").click(function() {
            if ($("#file-location").hasClass("hidden")) {
                $("#ChooseFileUpload").val(true);
            } else {
                $("#ChooseFileUpload").val(false);
            }
        });
    };

    $(function () {
        module.initialiseClearButton();
    });

    return module;

})(ACTIVATE || {});
