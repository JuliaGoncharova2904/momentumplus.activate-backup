/**
 * Angular JS horizontal timeline
 *
 * (c) eowo
 * http://github.com/eowo/angularjs-horizontal-timeline
 *
 * Version: v0.0.1
 *
 * Licensed under the MIT license
 */
var iCount = 0;
var template =
    '<section id="{{id}}" class="panel panel-primary timeline-container {{status | lowercase}}">' +
        '<div class="panel-heading clearfix no-redirect row vertical-align">' +
                        '<div class="col-lg-7 col-md-7 col-sm-7 col-xs-6 no-padding vertical-align">' +
        //'<a href="{{download}}/{{id}}" class="download" target="_blank" title="Download ICS file"><span class="glyphicon glyphicon-calendar"></span></a>' +
        //'<span title="" data-toggle="popover" data-placement="top" data-original-title="This timeline shows meetings which occur during the set time period."><span class="glyphicon glyphicon-info-sign"></span></span>' +
        '<span class="timeline-title"><span>Timeline</span></span>' +
        '<span title="" data-toggle="popover" data-placement="top" data-original-title="This timeline shows meetings which occur during the set time period."><span class="tooltip-icon"></span></span>' +
        '<span style="padding-left: 10px; font-size: 14px; font-weight: bold;" id="timelineOwner" owner-name="{{ ownerName }}">{{ ownerName }}</span>' +
                        '</div>' +
              '<div class="col-lg-5 col-md-5 col-sm-5 col-xs-6 no-padding" style="padding-top:2px">' +
        '<a style="padding-left: 5px;padding-right: 5px;" ng-if="status == \'Onboarder\'" href="{{report}}/{{id}}?ancestor=True" class="pull-right" title="View handover report" ng-show="report" target="_blank"><span class="search-icon"></span></a>' +
        '<a style="padding-left: 5px;" ng-if="status != \'Onboarder\'" href="{{report}}/{{id}}" class="pull-right" title="View handover report" ng-show="report" target="_blank"><span class="search-icon"></span></a>' +
        '<a href="{{download}}/{{id}}" style="padding-left: 5px;padding-right: 5px;" class="download pull-right" target="_blank" title="Download ICS file"><span class="download-icon"></span></a>' +
        '<a href="{{createMeeting}}/{{id}}"  style="padding-right: 5px;" class="pull-right" title="Add a meeting"><span class="add-meeting-icon"></span></a>' +
                '</div>' +
'</div>' +
'<div class="panel-body timeline text-center" title="">' +
'<div id="labels" class="{{status}}"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-left"><label>{{startDate | date: \'dd-MMM-yyyy\'}}</label></div><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right"><label>{{endDate | date: \'dd-MMM-yyyy\'}}</label></div>' +
'<div class="progress timeline-progress" title=""><div class="clearfix" style="height:10px;background-color:#eee;position:absolute;left:0;border-top-left-radius:3px;border-top-right-radius:3px;width:100%;"></div>' +
'	<span title="" class="progress-{{status}}" ng-style="{width:progress_percent+\'%\'}"></span>' +
'	<div class="timeline-events">' +
'		<li id="{{event.ID}}" class="timeline-event" ng-repeat="(key, value) in events.events track by $index"' +
'			ng-click="showNewEvent(key)"' +
'			title="{{key}}" event-date="{{key | date: \'yyyy-MM-dd\'}}"' +
'			timeline-event-marker><span class="marker"></span>' +
'			<div title="" class="timeline-view" ng-if="selectedEvent[key]" tabindex="-1">' +
'               <button type="button" class="close" aria-label="Close" ng-click="hideEvent(key)"><span aria-hidden="true">&times;</span></button>' +
'               <div ng-repeat="event in value | filter:{ Start: eventDate }" class="timeline-object">' +
'                   <span><strong>{{event.Name}}</strong><br>{{event.Start | date: \'dd-MMM-yyyy\'}} at {{event.Start | date: \'HH:mm\'}}</span><br/>' +
'                   <div class="links meeting" id={{event.ID}} data-id={{event.ID}}>' +
'                       <a href="/Home/Meetings/DownloadVCalender/{{event.ID}}" target="_blank" title="Download ICS file"><span class="glyphicon glyphicon-calendar"></span></a><span></span>' +
'                       <a href="" ng-click="selectedEvent[key]" title="View meeting" class="meetingEdit" data-toggle="modal" data-target="#meeting-quick-edit-modal"><span class="glyphicon glyphicon-search" ></span></a><span></span>' +
'                   </div>' +
'              </div>' +
'           </div>' +
'		</li>' +
'	</div>' +
'	<ul title="" class="timeline-bg text-left">' +
'		<li class="timeline-month" ng-repeat="month in months" index=$index months=months status=status start-date=startDate end-date=endDate' +
'			timeline-month>' +
'			<ul>' +
'				<li class="timeline-day" ng-repeat="day in month.days" timeline-date months=months month=month date=day status=status start-date=startDate end-date=endDate>' +
'					<span title="{{day + \'/\' + month.date.substr(5,2) + \'/\' + month.date.substr(0,4)}}"><i title="{{day + \'/\' + month.date.substr(5,2) + \'/\' + month.date.substr(0,4)}}"></i>{{day}}</span>' +
'				</li>' +
'			</ul><span class="no-cursor">{{month.name}}</span></li>' +
'	</ul>' +
'</div>' +
'</div>' + '</section>';

angular.module('angular-horizontal-timeline', ['ngSanitize'])
    .filter('unsafe', function ($sce) {
        return function (val) {
            return $sce.trustAsHtml(val);
        };
    })
    .directive('horizontalTimeline', function () {
        function controller($scope) {
            $scope.selectedEvent = [];
            $scope.months = [];
            $scope.events = events[iCount];
            iCount++;

            $scope.showNewEvent = function (index)
            {
                for (var event in $scope.events.events) {
                    if (event != index) {
                        $scope.selectedEvent[event] = false;
                    } else {
                        $scope.selectedEvent[event] = true;
                    }
                }
                $(window).trigger('resize');
            };

            $scope.hideEvent = function (index)
            {
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.selectedEvent[index] = false;
                    });
                });
            };

            $scope.getPosition = function (date) {
                date = moment(date);
                if (date < moment($scope.startDate).date(1)) {
                    return -1;
                } else if (date > moment($scope.endDate).date(31)) {
                    return 101;
                } else {
                    var diff = date.diff(moment($scope.startDate).date(1), 'months');
                    if (diff === -0) {
                        diff = 0;
                    }
                    if ($scope.status === "Ongoing") {
                        var curWeekWidth = 100 / $scope.months[diff].days.length;
                        var monthsWidth = 100 / $scope.months.length;
                        var ixOfWeek = Math.ceil(date.format('D') / 7) - 1;
                        var curDOfMPercent = (date.format('D') - $scope.months[diff].days[ixOfWeek]) * 14.28;

                        return ((monthsWidth * diff) + (((ixOfWeek * curWeekWidth) + (curDOfMPercent / 100 * curWeekWidth)) / 100 * monthsWidth));
                    } else {
                        var length = moment($scope.endDate).diff(moment($scope.startDate), "days");
                        var current = date.diff(moment($scope.startDate), "days");
                        return (current / length) * 100;
                    }
                }
            };

            if ($scope.status === 'Ongoing') {

                var range = moment().range(moment($scope.startDate).startOf('month'), moment($scope.endDate).endOf('month'));
                range.by('months', function (month) {
                    $scope.months.push({
                        'date': month.format('YYYY-MM'),
                        'name': month.format('MMM'),
                        'days': []
                    });

                    var dayrange = moment($scope.startDate).range(month.startOf('month').format('YYYY-MM-DD'), month.endOf('month').format('YYYY-MM-DD'));
                    dayrange.by('weeks', function (week) {
                        $scope.months[$scope.months.length - 1].days.push(week.format('DD'));
                    });
                });

            } else {

                var date = moment($scope.startDate);
                var found;
                while (date <= moment($scope.endDate)) {
                    found = false;
                    for (var i = 0; i < $scope.months.length; i++) {
                        if ($scope.months[i].date == date.format('YYYY-MM')) {
                            $scope.months[i].days.push(date.format('DD'));
                            var found = true;
                            break;
                        }
                    }
                    if (!found) {
                        $scope.months.push({
                            'date': date.format('YYYY-MM'),
                            'name': date.format('MMMM'),
                            'days': [],
                            'daysInMonth': ''
                        });
                        // Set days in month
                        if (moment($scope.startDate).format('MM') == date.format('MM')) {
                            $scope.months[$scope.months.length - 1].daysInMonth = moment($scope.startDate).endOf('month').diff(moment($scope.startDate), 'days')+1;
                            if ($scope.months[$scope.months.length - 1].daysInMonth == 0) {
                                $scope.months[$scope.months.length - 1].daysInMonth = 1;
                            }
                        } else if (moment($scope.endDate).format('MM') == date.format('MM')) {
                            $scope.months[$scope.months.length - 1].daysInMonth = moment($scope.endDate).diff(moment($scope.endDate).startOf('month'), 'days')+1;
                            if ($scope.months[$scope.months.length - 1].daysInMonth == 0) {
                                $scope.months[$scope.months.length - 1].daysInMonth = 1;
                            }
                        } else {
                            var save = moment(date);
                            $scope.months[$scope.months.length - 1].daysInMonth = save.endOf('month').format("DD");
                        }
                        $scope.months[$scope.months.length - 1].days.push(date.format('DD'));
                    }
                    date.add(1, 'weeks');
                }
            }

            $scope.progress_percent = $scope.getPosition(moment($scope.currentDate).format('YYYY-MM-DD'));
            if ($scope.progress_percent > 100) {
                $scope.progress_percent = 100;
            }
        }

            return {
                restrict: 'AEC',
                controller: controller,
                scope: {
                    id: '=',
                    ancestorId: '=',
                    title: '=',
                    startDate: '@',
                    currentDate: '@',
                    endDate: '@',
                    events: '=',
                    report: '=',
                    viewMeeting: '=',
                    createMeeting: '=',
                    download: '=',
                    status: '=',
                    ownerName: '=',
                    control: '='
        },
            template: template
        };
    })
    .directive("timelineMonth", function () {
        function link(scope, element, attr) {
            var monthWidth;
            if (scope.status === "Ongoing") {
                monthWidth = 100 / scope.months.length;
            } else {
                var days = (moment(scope.endDate).diff(moment(scope.startDate), "days")) + 1;
                monthWidth = (100 / days) * scope.months[scope.index].daysInMonth;
            }
            if (monthWidth > 100) { monthWidth = 100; }
            element.css({ 'width': monthWidth + "%" });
        }

        return {
            restrict: "A",
            link: link,
            scope: {
                index: "=",
                months: "=",
                status: "=",
                startDate: "=",
                endDate: "="
            }
        };
    })
    .directive("timelineDate", function () {
        function link(scope, element, attr) {
            var dateWidth = "";
            if (scope.status == "Ongoing") {
                dateWidth = Math.floor(scope.date / 7) * (100 / scope.month.days.length);
            } else {
                // Check for first month
                if (moment(scope.startDate).format("MM") === scope.month.date.substr(5) && moment(scope.endDate).format("MM") === scope.month.date.substr(5) && moment(scope.startDate).format("YYYY") === moment(scope.endDate).format("YYYY")) {
                    var days = scope.date - scope.startDate.substr(8);
                    var monthDuration = scope.endDate.substr(8) - scope.startDate.substr(8);
                    dateWidth = ((100 / monthDuration) * days);
                } else if (moment(scope.startDate).format("MM") == scope.month.date.substr(5)) {
                    if (scope.date == scope.startDate.substr(8)) {
                        dateWidth = 0;
                    } else {
                        var days = scope.date - scope.startDate.substr(8);
                        dateWidth = ((100 / scope.month.daysInMonth) * days);
                    }
                } else {
                    // Normal month
                    var day = "";
                    if (scope.date == 1) {
                        day = 0;
                    } else {
                        day = scope.date;
                    }
                    dateWidth = (100 / scope.month.daysInMonth) * day;
                }
            }
            if (dateWidth <= 100) {
                element.css({
                    'left': dateWidth + "%"
                });
            }
            else {
                element.css({
                    'display': "none"
                });
            }
        }
        return {
            restrict: "A",
            link: link,
            scope: {
                date: "=",
                status: "=",
                month: "=",
                months: "=",
                startDate: "=",
                endDate: "="
            }
        };
    })

.directive("timelineEventMarker", function () {
    function link(scope, element, attr) {

        scope.eventDate = moment(attr.eventDate, "DD-MMM-YYYY").format("YYYY-MM-DD");
        scope.popUpDate = moment(attr.eventDate, "DD-MMM-YYYY").format("DD-MMM-YYYY");

        function setPosition() {
            var pos = scope.getPosition(scope.eventDate);
            if (pos < 0 || pos > 100) {
                element.css({ 'display': "none" });
            } else {
                element.css({
                    'left': pos + "%"
                });
            }
        }

        scope.$watch("event", function () {
            scope.eventDate = scope.eventDate;
            setPosition();
        }, true);
    }
    return {
        restrict: "A",
        link: link,
        scope: false
    };
});
