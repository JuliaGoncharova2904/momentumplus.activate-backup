﻿/**
* Augment the ACTIVATE JavaScript module with the methods required for
* the people module.
*/
var ACTIVATE = (function (module) {

    /**
    * Initialise confirmation dialog on quick add person form.
    */
    module.initialisePersonConfirmation = function () {
        $('#person-confirmation').click(function (e) {
            var selectedOption = $('#ContactId option:selected');

            if (!selectedOption.val()) {
                alert('Please select a person.');
                return false;
            }

            var message = 'Click OK to add \'' + selectedOption.text() + '\' or Cancel to return to the previous page.'

            if (!confirm(message)) {
                var returnUrl = $(this).data('return-url');

                if (returnUrl) {
                    window.location = returnUrl;
                }

                return false;
            }
        });
    };

    /**
    * Initialise person retire button.
    */
    module.initialisePersonRetireButton = function () {
        $('#person-retire').click(function (e) {
            if (confirm('Are you sure you would like to retire this item?')) {
                if (confirm('Would you like this item retired from the Relationships Module?')) {
                    var url = $(this).attr('href');
                    window.location = url + '?retireRelationship=true';
                    return false;
                } else {
                    return true;
                }
            } else {
                return false;
            }
        });
    };

    /**
    * Initialise M+ user dropdown control.
    */
    module.initialiseMomentumUserSelector = function () {
        var currentUser = null;
        $('#UserId').change(function (e) {
            var id = $(this).val();

            if (id) {
                $.get('/Home/People/GetUserDetails', { id: id }, function (details) {
                    currentUser = details;
                    for (var property in details) {
                        if (details.hasOwnProperty(property)) {
                            var selector = '#' + property;
                            $(selector).val(details[property]);
                            $(selector).prop('readonly', true);

                            if ($(selector).is('select')) {
                                $(selector).prop('disabled', true);
                            }

                            if ($(selector).hasClass('combobox')) {
                                $(selector).combobox('disable');
                                $(selector).combobox('refresh');
                            }
                        }
                    }
                });
            } else if (currentUser) {
                for (var property in currentUser) {
                    if (currentUser.hasOwnProperty(property)) {
                        var selector = '#' + property;
                        $(selector).prop('readonly', false);

                        if ($(selector).is('select')) {
                            $(selector).prop('disabled', false);
                        }

                        if ($(selector).hasClass('combobox')) {
                            $(selector).combobox('enable');
                            $(selector).combobox('refresh');
                        }
                    }
                }
            }
        });
    };

    /**
    * Initialise "Go to Package" button.
    */
    module.initialiseGoToPackage = function () {
        $('#go-to-package').click(function (e) {
            var userId = $('#UserId').val();

            if (userId) {
                var count = $('#UserId option').length;
                var index = $('#UserId')[0].selectedIndex;
                var itemsPerPage = 8;
                var page = Math.floor(index / itemsPerPage);

                if (!(index % itemsPerPage === 0)) {
                    page++;
                }

                var url = '/Packages/Employees/Index/' + userId + '?PageNo=' + page;
                window.location = url;
            } else {
                alert('No user selected.');
            }

            return false;
        });
    };

    /**
    * Initialise profile image validation.
    */
    module.initialiseImageUploadValidation = function () {
        $('input#ImageId').change(function () {
            var filename = $(this).val();
            var extension = filename.substring(filename.lastIndexOf('.') + 1).toLowerCase();

            switch (extension) {
                case 'jpg':
                case 'png':
                    var size = $(this)[0].files[0].size;

                    if (size > 1000000) {
                        $(this).val(null);
                        alert('File too large. Please select an image that is 1MB or smaller.');
                    }

                    break;
                default:
                    $(this).val(null);
                    alert('Invalid file type. Please upload only JPG or PNG images.');
                    break;
            }
        });
    };

    $(function () {
        module.initialisePersonConfirmation();
        module.initialisePersonRetireButton();
        module.initialiseMomentumUserSelector();
        module.initialiseGoToPackage();
        module.initialiseImageUploadValidation();
    });

    return module;

})(ACTIVATE || {});
