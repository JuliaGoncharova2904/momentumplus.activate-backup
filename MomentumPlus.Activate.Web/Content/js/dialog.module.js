﻿; (function (window) {
    "use strict";
    //===================================================================
    // config: {
    //      url: (dialog url),
    //      acceptFiles: false,
    //      data: ( post data )
    // }
    //======================= Form Dialog ===============================
    function FormDialog(config) {
        this.config = config;
        this.openHandler = null;
        this.submitHandler = null;
        this.closeHandler = null;
        this.errorHandler = null;
    }
    FormDialog.prototype.setOpenEvent = function (eventHandler) {
        this.openHandler = eventHandler;
    };
    FormDialog.prototype.setSubmitEvent = function (eventHandler) {
        this.submitHandler = eventHandler;
    };
    FormDialog.prototype.setCloseEvent = function (eventHandler) {
        this.closeHandler = eventHandler;
    };
    FormDialog.prototype.setErrorEvent = function (eventHandler) {
        this.errorHandler = eventHandler;
    };
    FormDialog.prototype.open = function () {

        var _this = this;

        $('#loading-animation').show();

        if (_this.openHandler) {
            _this.openHandler();
        }

        $.ajax({
                url: _this.config.url,
                method: "GET",
                data: _this.config.data
            })
            .done(function (data) {
                var formDialog = new _Dialog(_this.config.dialogId, _this.config.acceptFiles);
                formDialog.buildDialog(data);
                formDialog.submitHandler = _this.submitHandler;
                formDialog.closeHandler = _this.closeHandler;
                formDialog.open();
            })
            .fail(function () {
                //var errorDialog = Activate.ErrorDialog();
                //errorDialog.setCloseEvent(function () {
                //    _this.errorHandler && _this.errorHandler();
                //    _this.closeHandler && _this.closeHandler();
                //});
                //errorDialog.open();
            })
            .always(function () {
                $('#loading-animation').hide();
            });
    };
    //===================================================================
    function _Dialog(dialogId, acceptFiles) {
        this.dialogId = dialogId;
        this.acceptFiles = acceptFiles;
        this.conteiner = null;
        this.dialog = null;
        this.form = null;
        //--- events ----
        this.submitHandler = null;
        this.closeHandler = null;
    }
    _Dialog.prototype.open = function () {
        var _this = this;

        $(this.dialog).modal('show');

        setTimeout(function () {
            //autosize.update($(".simple-textarea", _this.dialog));
        }, 150);
    };
    _Dialog.prototype.buildDialog = function (dialogHtml) {
        this.conteiner = document.createElement("div");
        this.conteiner.innerHTML = dialogHtml;
        document.body.appendChild(this.conteiner);

        if (this.dialogId) {
            this.dialog = $('#' + this.dialogId, this.conteiner)[0];
        }
        else {
            this.dialog = $(this.conteiner).children().first();
            this.dialogId = this.dialog.attr("id");
        }

        var _this = this;
        $(this.dialog).on('hidden.bs.modal', function () {
            $(_this.conteiner).remove();
            if (_this.closeHandler) {
                _this.closeHandler();
            }
        });

        var forms = $("form", this.conteiner);
        if (forms.length > 0) {
            this.form = forms[0];
            forms.submit(this.submitForm.bind(this));
        }

        this.runScriptsForElement(this.conteiner);
        this.initChangeDataEvents();
        //Activate.InitInputFields(this.dialog);
    };
    _Dialog.prototype.submitForm = function (event) {
        var _this = this;
        //$.validator.unobtrusive.parse(_this.form);
        if (_this.form.length > 0) {
            $.ajax({
                url: _this.form.action,
                type: _this.form.method,
                data: _this.acceptFiles ? new FormData(_this.form) : $(_this.form).serialize(),
                processData: _this.acceptFiles ? false : undefined,
                contentType: _this.acceptFiles ? false : undefined,
                success: function (result) {
                    if (result === "ok") {
                        if (_this.submitHandler) {
                            _this.submitHandler();
                        }
                        $(_this.dialog).modal("hide");
                    } else {
                        var htmlObject = document.createElement("div");
                        htmlObject.innerHTML = result;
                        var forms = $("form", htmlObject);
                        if (forms.length > 0) {
                            _this.form = forms[0];
                            $(_this.dialog).html(_this.form);
                            $(_this.form).submit(_this.submitForm.bind(_this));
                            _this.runScriptsForElement(htmlObject);
                            _this.initChangeDataEvents();
                            //Activate.InitInputFields(_this.dialog);
                        }
                        else {
                            _this.showErrorDialog();
                        }
                    }
                },
                error: function () {
                    _this.showErrorDialog();
                }
            });
        }
        event.preventDefault();
    };
    _Dialog.prototype.showErrorDialog = function () {
        console.log("error");
    };
    _Dialog.prototype.runScriptsForElement = function (element) {
        $('script', element).each(function () {
            if ($(this).attr("src") !== undefined) {
                $.ajax({
                    url: $(this).attr("src"),
                    method: "GET"
                });
            }
            else {
                eval($(this).text());
            }
        });
    };
    _Dialog.prototype.initChangeDataEvents = function () {
        var _this = this;

        var handler = function () {
            //------- Confirm close event --------
            $(".btn-close", _this.dialog).removeAttr("data-dismiss");
            $(".btn-close", _this.dialog).bind("click", _this.closeEvent.bind(_this));
            //------- Active save button ---------
            $("button[type=submit]", _this.dialog).addClass("active");
            //------ Unbind change events --------
            $('select', _this.dialog).unbind("change", handler);
            $('.simple-input', _this.dialog).unbind("input", handler);
            //$('.simple-input', _this.dialog).unbind("change", handler);
            $('.simple-textarea', _this.dialog).unbind("input", handler);
            $("button[type=submit]", _this.dialog).unbind("activate", handler);
            //------------------------------------
        };

        $("select", this.dialog).bind("change", handler);
        $(".simple-input", this.dialog).bind("input", handler);
        //$(".simple-input", this.dialog).bind("change", handler);
        $(".simple-textarea", this.dialog).bind("input", handler);
        $("button[type=submit]", this.dialog).bind("activate", handler);
    };
    _Dialog.prototype.closeEvent = function () {
        console.log("close");
    };
    //===================================================================
    window.Activate = window.Activate || {};
    window.Activate.FormDialog = function (config) {
        return new FormDialog(config);
    };
    //===================================================================
})(window);