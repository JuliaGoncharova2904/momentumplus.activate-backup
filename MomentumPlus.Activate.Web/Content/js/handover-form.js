﻿/**
* Augment the ACTIVATE JavaScript module with the methods required for
* the handover form.
*/
var ACTIVATE = (function (module) {

    module.initialiseHandoverFormUpdate = function () {

        function update(elem) {
            if ($(elem).val() !== '') {
                var id = $(elem).val();
                $.ajax({
                    method: 'GET', url: '/Package/GetDetails', data: { id: id }
                })
                .done(function (success) {
                    $('#newProfileName').html(success.name);
                    $('#newProfileEmail').html(success.email);
                    $('#newProfileView').attr('href', '/PackageSelector/ExaminePackage/' + id);
                    $('#newProfileView').show(); 
                    $('#starterViewPriorities').attr('href', '/Handover/OnboarderPriorities/' + id);
                    $('#starterViewTimeline').attr('href', '/PackageSelector/ExaminePackage/' + id);
                })
                .fail(function () {
                    alert('There was an error retrieving package information. Please report this problem.');
                });
            } else {
                $('#newProfileName').html('');
                $('#newProfileEmail').html('');
                $('#newProfileView').attr('href', '#');
                $('#newProfileView').hide();
                $('#starterViewTimeline').attr('href', '#');
            }
        }

        // check at least one accordion panel is open and hide save buttons if not
        $(".handover-accordion").click(function () {
            if ($(this).children(".glyphicon").hasClass("glyphicon-chevron-up")) {
                $(this).children(".glyphicon").removeClass("glyphicon-chevron-up").addClass("glyphicon-chevron-down");
                $(this).removeClass("col-sm-10").addClass("col-sm-12");
                $(this).siblings(".handover-buttons").addClass("hide");
                if ($(".panel-collapse.in").length === 1) {
                    $(".panel-footer").hide();
                }
            } else {
                $(this).children(".glyphicon").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-up");
                $(this).removeClass("col-sm-12").addClass("col-sm-10");
                $(this).siblings(".handover-buttons").removeClass("hide");
                $(".panel-footer").show();
            }
        });

        $('#OnboardingPeriodViewModel_PackageId').change(function () {
            update(this);
        });

        update($('#OnboardingPeriodViewModel_PackageId'));
    };

    /**
    * Initilaise leaving last day/onboarding start point alert.
    */
    module.initialiseDateAlert = function () {
        $('#LeavingPeriodViewModel_LastDay, #OnboardingPeriodViewModel_StartPoint').change(function () {
            var leaverLastDay = moment($('#LeavingPeriodViewModel_LastDay').val());
            var onboarderStartPoint = moment($('#OnboardingPeriodViewModel_StartPoint').val());

            if (leaverLastDay && onboarderStartPoint && onboarderStartPoint.isBefore(leaverLastDay)) {
                alert('Onboarder start-point is before leaver last day. Click OK to acknowledge.');
            }
        });
    };

    $(function () {
        module.initialiseHandoverFormUpdate();

        if ($("#LeavingNoticePeriod").val() == '') {
            $("#LeavingNoticePeriod").val('OneMonth');
        }
    });

    return module;

})(ACTIVATE || {});