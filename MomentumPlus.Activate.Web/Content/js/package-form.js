﻿/**
* Augment the ACTIVATE JavaScript module with the methods required for
* the package form.
*/
var ACTIVATE = (function (module) {

    function disableSelectPicker(element) {
        element.val(null);
        element.prop('disabled', true);
        element.selectpicker('refresh');
    }

    function enableSelectPicker(element) {
        element.prop('disabled', false);
        element.selectpicker('refresh');
    }

    /**
    * Initialise positions dropdown.
    */
    module.initialisePositionDropdown = function () {
        /**
        * Reload page with the selected position on change.
        */
        $('#PositionId').change(function () {
            var positionId = $(this).val();

            var contactIds = $('#ContactIds');
            var projectIds = $('#ProjectIds');

            if (positionId) {
                $.get('/Package/GetEnabledModules', { positionId: positionId }, function (enabledModules) {
                    if (enabledModules.Projects) {
                        enableSelectPicker(projectIds);
                        projectIds.attr("title", "");
                        projectIds.selectpicker('refresh');
                    } else {
                        disableSelectPicker(projectIds);
                        projectIds.attr("title", "Projects Module is not enabled for this position");
                        projectIds.selectpicker('refresh');
                    }
                });
            } else {
                disableSelectPicker(projectIds);
                projectIds.attr("title", "Projects Module is not enabled for this position");
                projectIds.selectpicker('refresh');
            }
        }).change();
    };

    $(function () {
        module.initialisePositionDropdown();
    });

    return module;

})(ACTIVATE || {});
