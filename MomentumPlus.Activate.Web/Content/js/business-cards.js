﻿angular.module('momentumPlus.activate', ['truncate'])
    .controller('businessCardCtrl', ['$scope', function ($scope)
    {

        function reInitCombobox(selector)
        {
            //Workaround
            setTimeout(function ()
            {
                var combobox = $(selector);

                combobox.removeData('combobox').siblings('.combobox-container').remove();
                combobox.combobox();

            }, 1);
        }

        $scope.changeCompany = function ()
        {
            var option = $('#company-dropdown').find('.combobox').val();

            $scope.company = option;
        };
        $scope.changeRelationshipImportance = function ()
        {
            var option = $('#RelationshipImportance :selected').text();
            $scope.relationshipImportanceDesc = option;
        };
        $scope.changeIndirect = function ()
        {
            var option = $('#isIndirect').find('.combobox').val();
            $scope.indirect = option;
        };
        $scope.otherCompanyChange = function (val)
        {
            if (!$scope.otherCompany)
            {
                $scope.companyId = val;
        	}
        	else
            {
                $scope.otherCompanyName = '';
                $scope.companyId = '';
                val = '';
            }
            $('.combobox-container.combobox-selected input[type="hidden"]').val(val);
            $('#CompanyGuid').val(val);
            $('#companyId').val(val);
            $scope.CompanyGuid = val;
            reInitCombobox('select.company-dropdown.combobox');
        };
        $scope.companyChange = function (companyId, otherCompanyId)
        {
            if (companyId === otherCompanyId)
            {
                $scope.otherCompany = true;
            } else
            {
                $scope.companyId = companyId;
            }

            $scope.CompanyGuid = companyId;

            $('.combobox-container.combobox-selected input[type="hidden"]').val(companyId);
            $('#CompanyGuid').val($scope.CompanyGuid);
            $('#companyId').val($scope.CompanyGuid);

            reInitCombobox('select.company-dropdown.combobox');
        }

    }])
    .directive('businessCard', [function ()
    {
        return {
            restrict: 'E',
            templateUrl: 'business-card.html',
            scope: {
                id: '=',
                firstName: '=',
                lastName: '=',
                managed: '=',
                imageId: '=',
                position: '=',
                otherCompany: '=',
                otherCompanyName: '=',
                company: '=',
                companyId: '=',
                telephone: '=',
                email: '=',
                management: '=',
                introRequired: '=',
                CompanyGuid: '='
            }
        };
    }])
    .directive('peopleInfo', [function ()
    {
        return {
            restrict: 'E',
            templateUrl: 'people-info.html',
            scope: {
                id: '=',
                firstName: '=',
                roleName: '=',
                lastName: '=',
                managed: '=',
                packageStatus: '=',
                contactPackageId: '=',
                imageId: '=',
                position: '=',
                otherCompany: '=',
                otherCompanyName: '=',
                company: '=',
                companyId: '=',
                telephone: '=',
                email: '=',
                management: '=',
                introRequired: '=',
                CompanyGuid: '='
            }
        };
    }])
    .directive('businessCardEditor', [function ()
    {
        return {
            restrict: 'E',
            templateUrl: 'business-card-editor.html',
            scope: {
                id: '=',
                titleOptions: '=',
                personTitle: '=',
                firstName: '=',
                lastName: '=',
                managed: '=',
                imageId: '=',
                position: '=',
                otherCompanyId: '=',
                otherCompany: '=',
                otherCompanyName: '=',
                company: '=',
                companyId: '=',
                companyOptions: '=',
                telephone: '=',
                email: '=',
                management: '=',
                introRequired: '=',
                CompanyGuid: '='
            }
        };
    }])
;