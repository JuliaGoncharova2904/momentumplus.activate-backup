﻿/**
* Augment the ACTIVATE JavaScript module with the methods required for
* the summary page widgets.
*/
var ACTIVATE = (function (module) {

    /**
     * Initialise the search 
     */
    module.initialiseSearch = function () {
        $(".form-control-feedback").click(function (e) {
            $(this).siblings(".form-control").focus();
        });
    };

    $(function () {
        module.initialiseSearch();
    });

    return module;

})(ACTIVATE || {});
