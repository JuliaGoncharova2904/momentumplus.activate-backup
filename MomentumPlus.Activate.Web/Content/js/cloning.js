﻿/**
* Augment the ACTIVATE JavaScript module with the methods required for
* the cloning form.
*/
var ACTIVATE = (function (module) {

    /**
    * Initialise the topic cloning form.
    */
    module.initialiseCloningForm = function (topicId) {
        var forms = $('#cloning-form').find('form');
        var sourceForm = $(forms.get(0));
        var destinationForm = $(forms.get(1));

        /**
        * Copy individual fields.
        */
        $('.form-group').each(function () {
            var control = $(this).find('.clone-object');
            var id = control.attr('id');
            var val = control.val();

            var copyButton = $(this).find('.copy-one');
            var cloneCollection = copyButton.data('collection');

            var sourceTopic = copyButton.data('source');
            var count = parseInt(copyButton.data('count'), 10);
            var target = copyButton.data('target');

            var targetElement = destinationForm.find(target);
            var targetCount = parseInt(targetElement.text(), 10);
            var newCount = targetCount + count;

            copyButton.click(function () {
                // Simple value copy
                if (!cloneCollection) {
                    // The ID of the destination element will be the ID of the
                    // cloned element without the "Clone" prefix.
                    destinationForm.find('#' + id.substring(5)).val(val);
                    return;
                }

                // Collection copy (voice messages, attachments, tasks)
                var destinationInput = destinationForm
                    .find('[name=' + cloneCollection + ']');
                var destinationInputCount = destinationForm
                    .find('[name=' + cloneCollection + 'Count]');

                targetElement.text(newCount);

                if (destinationInput.length) {
                    var currentVal = destinationInput.val();
                    destinationInput.val(sourceTopic);
                    destinationInputCount.val(newCount);
                } else {
                    destinationForm.append($('<input>', {
                        'type': 'hidden',
                        'name': cloneCollection,
                        'val': sourceTopic
                    }));

                    destinationForm.append($('<input>', {
                        'type': 'hidden',
                        'val': newCount,
                        'name': cloneCollection + 'Count'
                    }));
                }
            });
        });

        /**
        * Copy all fields.
        */
        $('#copy-all').click(function () {
            $('.form-group').each(function () {
                var copyButton = $(this).find('.copy-one');
                copyButton.click();
            });
        });

        /**
        * Close cloning form.
        */
        $('.clone-close').click(function () {
            destinationForm.append('<input type="hidden" name="Redirect" value="None">');
            destinationForm.submit();
            return false;
        });
    };

    return module;

})(ACTIVATE || {});
