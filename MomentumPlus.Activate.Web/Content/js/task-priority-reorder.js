﻿/**
* Augment the ACTIVATE JavaScript module with the methods required for
* the to do list functionality.
*/
var ACTIVATE = (function (module) {

    /**
     * Initialise drag and drop functionality for the task prioritisation.
     */

    module.initialiseDragDrop = function () {
        // Adapted from: http://jsfiddle.net/yf47u/
        $tables = $(".panel-body table");

        var id, startWeek, newWeek = -1;

        $("tbody.connectedSortable")
            .sortable({
                connectWith: ".connectedSortable",
                items: "> tr:not(.dummy)",
                appendTo: $tables,
                helper: "clone",
                zIndex: 999990,
                start: function (event, ui) {
                    $tables.addClass("dragging");
                    startWeek = $(this).closest("table").data("week");
                    id = $(ui.item).attr('id');
                },
                stop: function () { $tables.removeClass("dragging"); }
            })
            .disableSelection();

        var $tab_items = $(".panel").droppable({
            accept: ".connectedSortable tr",
            hoverClass: "ui-state-hover",

            drop: function (event, ui) {
                //newWeek = $(id).closest('table').data('week');
                newWeek = $('.ui-sortable-placeholder').closest('table').data('week');
                if (newWeek === undefined) {
                     newWeek = $(this).find('table').data('week');
                }

                id = $(ui.draggable).attr('id');

                if (startWeek != newWeek) {
                    $.ajax({
                        method: "POST", url: '/Task/Update', data: { id: id, newWeek: newWeek },          
                        error: function () {
                            alert('There was an error updating the task. Please report this problem.');
                        }
                    });
                }
                return false;
            }
        });

        // Adapted from: http://jsfiddle.net/bgrins/tzYbU/
        var fixHelperModified = function (e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function (index) {
                $(this).width($originals.eq(index).width());
            });
            return $helper;
        };

        $(".table tbody").sortable({
            helper: fixHelperModified
        }).disableSelection();
    };

    $(function () {
        $(".connectedSortable tr").css("cursor", "move");
        $(".ui-sortable-helper").css("cursor", "move");
        module.initialiseDragDrop();
    });

    return module;

})(ACTIVATE || {});