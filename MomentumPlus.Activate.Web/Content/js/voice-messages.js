﻿/**
* Augment the ACTIVATE JavaScript module with the methods required for
* the voice message form.
*/
var ACTIVATE = (function (module) {

    /**
    * Initialise clear button which clears the current message.
    */
    module.initialiseClearButton = function () {
        $('#clear-message').click(function (e) {
            e.preventDefault();

            if (confirm('Are you sure you want to replace the current message?')) {
                $(this).closest('.form-group').hide();
                $('#upload-control').removeClass('hidden');
            }
        });
    };

    $(function () {
        module.initialiseClearButton();
    });

    return module;

})(ACTIVATE || {});
