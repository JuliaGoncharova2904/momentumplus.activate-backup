﻿/*global d3*/
/**
* Augment the ACTIVATE JavaScript module with the methods required for
* the Relationships module.
*/

var ACTIVATE = (function (module) {

    var slider;
    var slideMappings = {};

    /**
     * Show the contact with the given ID in the slider control
     * and highlight the node on the graph.
     * @param {} contactId 
     * @returns {} 
     */
    function showContact(contactId) {
        if (slider.goToSlide) {
            slider.goToSlide(slideMappings[contactId]);
            highlightNode(contactId);
        }
    }

    /**
     * Highlight the graph node corresponding to the given contact ID
     * @param {} contactId 
     * @returns {} 
     */
    function highlightNode(contactId) {
        // Remove current highlight
        var currentSelection = d3.select('#selected-node');
        currentSelection.remove();

        var selectedNode = d3.select('[data-contact-id="' + contactId + '"]');
        selectedNode.append('image')
            .attr('x', -7.5)
            .attr('y', -4)
            .attr('width', 15)
            .attr('height', 15)
            .attr('xlink:href', '/Content/css/images/momentum_icon.png')
            .attr('id', 'selected-node')
            .attr('r', 3);
    }

    /**
    * Initialise the relationships graph.
    *
    * Args:
    *     rootNode: ID of the root node (required).
    *     nodes: An object containing the graph nodes keyed by node ID (required).
    *     options: A JavaScript object specifying graph parameters (optional).
    *
    * Options:
    *     size: The length of one side of the graph bounding box in pixels
    *         (default: 450px).
    *     colours: A list of colour codes to be used for colouring the 5 rings
    *         of the graph. For example, 'rgb(77, 189, 234)'.
    *     graphContainer: Selector for the element within which the graph will
    *         be drawn (default: '#graph-container').
    *     linkClass: Class to give to the lines connecting nodes on the graph
    *         (default: 'link').
    *     indirectLinkClass: Class to give to the lines connecting direct and
    *         indirect contacts (default: 'indirect').
    *     nodeClass: Class to give to the nodes on the graph
    *         (default: 'node').
    *     nodeImageIndividual: Path of the image to display at each node for
    *         an individual (default: '/Content/css/images/user.png').
    *     nodeImageGroup: Path of the image to display at each node for
    *         a group (default: '/Content/css/images/group.png').
    *     nodeImageSize: The length of one side of the node images (default: 20px).
    *     static: If set to true dragging and editing will be disabled.
    */
    module.initialiseRelationshipsGraph = function (rootNode, defaultCompanyId, nodes, options) {
        var settings,
            size,
            IMPORTANT_NODES_BREAK_POINT = 10,
            TOTAL_NODES_BREAK_POINT = 20,
            importantNodesCount = 0,
            nodesCount = 0,
            circles,
            ringWidth,
            svg,
            tip,
            links = [],
            link,
            originalPosition = {},
            dragging = false,
            force,
            nodeDrag,
            nodeSelection,
            linkToggleCookieId = 'a2bccbcf-c2f4-4b08-8148-85221a634af1',
            allNodes = $.extend(true, {}, nodes),
            companiesCookieId = 'DD7A021A-E091-4CA9-AC83-7EEE109A3E24',
            companyCheckboxes = $('#company-filter .company-checkbox'),
            allCompaniesCheckbox = $('#all-companies'),
            cookieCompanies = getCookieCompanies(),
            allCompanies = [],
            selectedCompanies = [];

        companyCheckboxes.each(function () {
            var company = $(this).val();
            allCompanies.push(company);

            // Ensure new companies are selected by default
            if (cookieCompanies && !cookieCompanies.hasOwnProperty(company)) {
                cookieCompanies[company] = true;
            }
        });

        if (!cookieCompanies) {
            cookieCompanies = {};

            // Select all companies by default when cookie does not exist
            for (var i = 0; i < allCompanies.length; i++) {
                cookieCompanies[allCompanies[i]] = true;
            }
        }

        $.cookie(companiesCookieId, JSON.stringify(cookieCompanies));

        companyCheckboxes.each(function () {
            var company = $(this).val();

            if (cookieCompanies[company]) {
                $(this).prop('checked', true);
                selectedCompanies.push(company);
            }
        });

        var companiesArray = $.map(JSON.parse($.cookie(companiesCookieId)), function (el) { return el; });

        $(companiesArray).each(function (index, value) {
            updateAllCompaniesCheckbox();
            updateCookieCompanies(index, value);

            if (value) {
                cookieCompanies[index] = value;
                selectedCompanies.push(index);
            }
        });

        updateAllCompaniesCheckbox();
        //updateGraph(false, true);

        /**
         * Create relationships graph.
         * @returns {} 
         */
        function createGraph() {
            // Create settings object from given options, using defaults where an
            // option is not specified.
            options = options || {};
            settings = {
                size: options.size || 450,
                colours: options.colours || [
                    'rgb(0, 153, 130)',
                    'rgb(51, 173, 155)',
                    'rgb(102, 194, 180)',
                    'rgb(153, 214, 205)',
                    'rgb(255, 255, 255)'
                ],
                graphContainer: options.graphContainer || '#graph-container',
                linkClass: options.linkClass || 'link',
                indirectLinkClass: options.linkClass || 'indirect',
                nodeClass: options.nodeClass || 'node',
                nodeImageIndividual: options.nodeImageIndividual || '/Content/css/images/user.png',
                nodeImageIndividualExternal: options.nodeImageIndividualExternal || '/Content/css/images/user-white.png',
                nodeImageIndividualNode: options.nodeImageIndividualNode || '/Content/css/images/node-blue.png',
                nodeImageIndividualExternalNode: options.nodeImageIndividualExternalNode || '/Content/css/images/node-white.png',
                nodeImageGroup: options.nodeImageGroup || '/Content/css/images/group.png',
                nodeImageGroupExternal: options.nodeImageGroupExternal || '/Content/css/images/group-white.png',
                nodeImageSize: options.nodeImageSize || 20,
                static: options.static
            };

            for (var i in nodes) {
                if (nodes.hasOwnProperty(i)) {
                    if (nodes[i].RelationshipImportance > 3) {
                        importantNodesCount++;
                    }
                    nodesCount++;
                }
            }

            // Length of one side of the graph bounding box
            size = settings.size;

            // The circles that make up the rings of the graph
            circles = [
                { radius: 1 / 2 * size, colour: settings.colours[0] },
                { radius: 2 / 5 * size, colour: settings.colours[1] },
                { radius: 3 / 10 * size, colour: settings.colours[2] },
                { radius: 1 / 5 * size, colour: settings.colours[3] },
                { radius: 1 / 10 * size, colour: settings.colours[4] }
            ];

            // Width of one ring
            ringWidth = size / (circles.length * 2);

            // Create root SVG element
            svg = d3.select(settings.graphContainer).append('svg')
                .attr('width', size)
                .attr('height', size);

            // Contact tooltip
            tip = d3.tip().attr('class', 'd3-tip').html(function (d) {
                var items = ['<strong>' + d.Name + '</strong>', d.Position, d.GroupName, d.ContactDetails];
                items = items.filter(function (item) {
                    return item;
                }); // Filter out null/empty strings
                return items.join('<br>');
            });

            svg.call(tip);

            // Create circle elements
            svg.selectAll('circle')
                .data(circles)
                .enter().append('circle')
                .attr('cx', size / 2)
                .attr('cy', size / 2)
                .attr('r', function (circle) {
                    return circle.radius;
                })
                .attr('fill', function (circle) {
                    return circle.colour;
                });

            // Importance labels
            svg.selectAll('text')
                .data([5, 4, 3, 2, 1])
                .enter().append('text')
                .attr('x', size / 2 - 5)
                .attr('y', function (d) {
                    return size / 2 + calculateDistanceByImportance(d) + 10;
                })
                .text(Object)
                .attr('font-size', function (d) {
                    return (1.5 - (5 - d) * 0.1) + 'em';
                })
                .attr('font-weight', 'bold')
                .attr('fill', 'rgb(131, 101, 168)')
                .attr('style', 'text-shadow: 0 -4px 3px rgba(255, 255, 255, 0.3), 0 3px 4px rgba(0, 0, 0, 0.2);');

            // Create the links from the given nodes
            for (var id in nodes) {
                if (id !== rootNode && nodes.hasOwnProperty(id)) {
                    // Direct link
                    links.push({
                        source: nodes[rootNode],
                        target: nodes[id]
                    });

                    // In direct link
                    if (nodes[id].IndirectRelationshipContactId) {
                        var source = nodes[nodes[id].IndirectRelationshipContactId];

                        if (source) {
                            links.push({
                                source: source,
                                target: nodes[id]
                            });
                        }
                    }
                }
            }

            // Create force-directed graph layout
            force = d3.layout.force()
                .nodes(d3.values(nodes))
                .links(links)
                .size([size, size])
                .linkDistance(function (link) {
                    if (link.source.ID === rootNode) {
                        // Direct link
                        return calculateDistanceByImportance(link.target.RelationshipImportance);
                    }
                    else {
                        // Indirect link
                        var importanceDiff = Math.abs(nodes[link.source.ID].RelationshipImportance - link.target.RelationshipImportance);
                        return ringWidth * (importanceDiff + 1);
                    }
                })
                .charge(-60)
                .chargeDistance(ringWidth / 2)
                .on('tick', tick)
                .on('end', updateChart)
                .start();

            // Create the links
            link = svg.selectAll('.' + settings.linkClass)
                .data(force.links())
                .enter().append('line')
                .attr('class', 'invisible');

            /**
            * Node drag behaviour.
            */
            nodeDrag = d3.behavior.drag()
                .on('dragstart', function (d, i) {
                    force.stop();
                    dragging = true;
                    originalPosition.x = d.x;
                    originalPosition.y = d.y;
                    tip.hide();
                })
                .on('drag', function (d, i) {
                    // Prevent dragging outside of circle
                    if (distanceFromRoot(d3.event.x, d3.event.y) <= (size / 2) - (ringWidth / 4)) {
                        d.px += d3.event.dx;
                        d.py += d3.event.dy;
                        d.x += d3.event.dx;
                        d.y += d3.event.dy;
                        tick();
                    }

                    if (distanceFromRoot(d3.event.x, d3.event.y) > ((size / 2) - (ringWidth / 4)) - 25) {
                        d.fixed = true;
                        tick();
                        dragging = false;
                        updateChart();
                    }
                })
                .on('dragend', function (d, i) {
                    // If node is dropped outside of circle reset its position
                    if (distanceFromRoot(d.x, d.y) >= (size / 2) - (ringWidth / 4)) {
                        d.x = originalPosition.x;
                        d.y = originalPosition.y;
                    }

                    d.fixed = true;
                    tick();
                    force.resume();
                    dragging = false;
                    updateChart();
                });

            // Create the nodes
            nodeSelection = svg.selectAll('.' + settings.nodeClass)
                .data(force.nodes())
                .enter().append('g')
                .attr('class', settings.nodeClass)
                .attr('data-contact-id', function (node) {
                    return node.ID;
                })
                .on('click', nodeClick);


            if (!settings.static) {
                nodeSelection.call(nodeDrag);
            }

            // Add node icons
            nodeSelection.append('image')
                .attr('xlink:href', function (node) {
                    if (node.ID === rootNode) {
                        return importantNodesCount > IMPORTANT_NODES_BREAK_POINT || nodesCount > TOTAL_NODES_BREAK_POINT ? settings.nodeImageIndividualNode : settings.nodeImageIndividual;
                    }

                    switch (node.RelationshipType) {
                        case 0:
                            if (node.CompanyId === defaultCompanyId) {
                                return importantNodesCount > IMPORTANT_NODES_BREAK_POINT || nodesCount > TOTAL_NODES_BREAK_POINT ? settings.nodeImageIndividualNode : settings.nodeImageIndividual;
                            }
                            else {
                                return importantNodesCount > IMPORTANT_NODES_BREAK_POINT || nodesCount > TOTAL_NODES_BREAK_POINT ? settings.nodeImageIndividualExternalNode : settings.nodeImageIndividualExternal;
                            }
                        case 1:
                            if (node.CompanyId === defaultCompanyId) {
                                return importantNodesCount > IMPORTANT_NODES_BREAK_POINT || nodesCount > TOTAL_NODES_BREAK_POINT ? settings.nodeImageIndividualNode : settings.nodeImageIndividual;
                            }
                            else {
                                return importantNodesCount > IMPORTANT_NODES_BREAK_POINT || nodesCount > TOTAL_NODES_BREAK_POINT ? settings.nodeImageIndividualExternalNode : settings.nodeImageIndividualExternal;
                            }
                    }
                })
                .attr('x', -settings.nodeImageSize / 2)
                .attr('y', -settings.nodeImageSize / 2)
                .attr('width', settings.nodeImageSize)
                .attr('height', settings.nodeImageSize);

            setNodeText(nodeSelection);

            // Tooltip events
            nodeSelection
                .on('mouseover', function (node) {
                    if (!dragging) {
                        tip.show(node);
                    }
                })
                .on('mouseout', tip.hide);

            setNodeText(nodeSelection);

            if (!settings.static) {
                svg.append('text')
                    .attr('x', settings.size - 80)
                    .attr('y', 25)
                    .text('+ Add New')
                    .attr('style', 'cursor: pointer;')
                    .on('click', function () {
                        window.location = '/Home/Relationships/QuickAddRelationship';
                    });
            }

            if ($.cookie("link-toggle") === "true") {
                $('#link-toggle').prop('checked', true);
            } else {
                $('#link-toggle').prop('checked', false);
            }

            $('#link-toggle').change(function () {
                if ($(this).prop('checked')) {
                    $('#link-toggle').prop('checked', true);
                    link.attr('class', getLinkClass);
                    $.cookie(linkToggleCookieId, true);
                    $.cookie("link-toggle", true);
                }
                else {
                    $('#link-toggle').prop('checked', false);
                    link.attr('class', 'invisible');
                    $.removeCookie(linkToggleCookieId);
                    $.removeCookie("link-toggle");
                }
            });

            //Fix select card after edited - AC 39

            var savingContactCardId = $.cookie('savingContactCardId');

            if (typeof savingContactCardId !== 'undefined' || savingContactCardId) {
                var contact = { ID: savingContactCardId };

                $(document).ready(function () {
                    showContact(contact.ID);
                    $.removeCookie('savingContactCardId');
                });
            }

            var companiesArray = $.map(JSON.parse($.cookie(companiesCookieId)), function (el) { return el; });

            $(companiesArray).each(function (index, value) {
                updateAllCompaniesCheckbox();
                updateCookieCompanies(index, value);

                if (value) {
                    cookieCompanies[index] = value;
                    selectedCompanies.push(index);
                }
            });
        }

        /**
        * Node click event handler.
        */
        function nodeClick(d) {
            // Prevent when dragging, or when the
            // root node is clicked.
            if (d3.event.defaultPrevented || d.ID === rootNode) {
                return;
            }

            var node = d3.select('[data-contact-id="' + d.ID + '"]');
            var marker = node.select('#selected-node');

            if (marker.empty()) {
                // Show contact in card slider and highlight
                showContact(d.ID);
            }
            else {
                $.cookie('savingContactCardId', d.ID);

                var secondParams = $.cookie("relationships-index") === "management" ? "?urlAction=viewManagement" : "";
                window.location = '/Home/Relationships/Edit/' + d.ID + secondParams;
            }
        }

        /**
        * Get the CSS class for a link.
        */
        function getLinkClass(link) {
            var cssClass = settings.linkClass;

            if (link.source.ID !== rootNode) {
                cssClass += ' ' + settings.indirectLinkClass;
            }

            if (link.source.ID === rootNode && link.target.IndirectRelationshipContactId) {
                cssClass += ' invisible';
            }

            return cssClass;
        }

        /**
        * Asynchronously update the chart coordinates in the database.
        */
        function updateChart() {
            var contactViewModels = [];

            // Assign chart x/y values and calculate importance for each contact
            for (var id in nodes) {
                if (id !== rootNode && nodes.hasOwnProperty(id)) {
                    nodes[id].ChartX = nodes[id].x;
                    nodes[id].ChartY = nodes[id].y;
                    nodes[id].RelationshipImportance = calculateImportance(id);
                    contactViewModels.push(nodes[id]);
                }
            }

            d3.xhr('/Home/Relationships/UpdateChart')
                .header('Content-Type', 'application/json')
                .post(JSON.stringify({ contacts: contactViewModels }));
        }

        /**
            * Gets the transform attribute for the node label - if the text falls
            * outside of the graph area then it it moved to the opposite side of
            * the node.
            */
        function getTransform(node) {
            var bBox = this.getBBox();

            if (node.x + bBox.width < size - 20) {
                return 'translate(0, 0)';
            }
            else {
                return 'translate(' + (-bBox.width - settings.nodeImageSize) + ', 0)';
            }
        }

        /**
        * Label the given node selection with their name/company name.
        */
        function setNodeText(nodes) {
            nodes.selectAll('g').remove();

            // Wrapper around the node label
            var textWrapper = nodes.append('g').attr('class', 'initials');

            textWrapper.append('text')
                .text(function (d) {
                    return d.Initials;
                })
                .attr('x', 10)
                .attr('dy', '.35em');

            textWrapper.attr('transform', getTransform);
        }

        function tick() {
            if (rootNode !== null) {
                // Centre the root node
                nodes[rootNode].x = nodes[rootNode].y = size / 2;

                if (!dragging) {
                    // Set positions of other nodes
                    for (var id in nodes) {
                        if (id !== rootNode && nodes.hasOwnProperty(id)) {
                            if (nodes[id].ChartX && nodes[id].ChartY) {
                                nodes[id].x = nodes[id].ChartX;
                                nodes[id].y = nodes[id].ChartY;
                            }
                        }
                    }
                }

                // Set the node coordinates
                nodeSelection
                    .attr('transform', function (node) {
                        return 'translate(' + node.x + ',' + node.y + ')';
                    });

                // Set the link endpoints
                link
                    .attr('x1', function (link) {
                        return link.source.x;
                    })
                    .attr('y1', function (link) {
                        return link.source.y;
                    })
                    .attr('x2', function (link) {
                        return link.target.x;
                    })
                    .attr('y2', function (link) {
                        return link.target.y;
                    });

                // Set node text
                //setNodeText(nodeSelection);
                nodeSelection.selectAll('.initials').attr('transform', getTransform);
            }
        }

        /**
        * Calculate the distance of the given coordinates from
        * the center (root node).
        */
        function distanceFromRoot(x, y) {
            var rootNodeX = nodes[rootNode].x;
            var rootNodeY = nodes[rootNode].y;
            return Math.sqrt(Math.pow(rootNodeX - x, 2) + Math.pow(rootNodeY - y, 2));
        }

        /**
        * Calculate the importance of a given contact.
        */
        function calculateImportance(nodeId) {
            var distance = distanceFromRoot(nodes[nodeId].x, nodes[nodeId].y);
            var importance = (size / 2 - distance + ringWidth / 2 + settings.nodeImageSize / 2) / ringWidth;
            return Math.min(Math.floor(importance), 5);
        }

        /**
        * Calculate the distance from the center based on importance.
        */
        function calculateDistanceByImportance(importance) {
            return (size / 2) - (ringWidth * importance - ringWidth / 2);
        }

        function updateGraph(managedOnly, report) {
            // Reset everything
            settings = null;
            size = null;
            importantNodesCount = 0;
            nodesCount = 0;
            circles = null;
            ringWidth = null;
            svg = null;
            tip = null;
            links = [],
            link = null;
            originalPosition = {};
            dragging = false;
            force = null;
            nodeDrag = null;
            nodeSelection = null;

            $('#graph-container').empty();

            nodes = {};

            var selectedCompanies = [];
            var checked = companyCheckboxes.filter(':checked');

            checked.each(function () {
                selectedCompanies.push($(this).val());
            });

            if ($.cookie("relationships-index") === "viewManagement") {
                if (typeof options != 'undefined') {
                    options = {};
                    options.static = false;
                }
                managedOnly = true;
            }

            // Filter by company name

            var blueNodesCount = 0;
            var whiteNodesCount = 0;

            for (var i in allNodes) {
                if (allNodes.hasOwnProperty(i)) {
                    if (i === rootNode ||
                            (
                                ($.inArray(allNodes[i].Company, selectedCompanies) !== -1 || $.inArray(allNodes[i].OtherCompanyName, selectedCompanies) !== -1)
                                && (!managedOnly || allNodes[i].Managed)
                            ) || report || (typeof options != "undefined" && options.static)
                        ) {
                        nodes[i] = allNodes[i];
                    }
                }
            }

            for (var k in nodes) {
                if (nodes.hasOwnProperty(k)) {
                    switch (nodes[k].RelationshipType) {
                        case 0:
                            if (nodes[k].CompanyId === defaultCompanyId) {
                                blueNodesCount++;
                            }
                            else {
                                whiteNodesCount++;
                            }
                            break;
                        case 1:
                            if (nodes[k].CompanyId === defaultCompanyId) {
                                blueNodesCount++;
                            }
                            else {
                                whiteNodesCount++;
                            }
                            break;
                    }
                }
            }

            $("#allBlueNodes").text(blueNodesCount);
            $("#allNodesCount").text(whiteNodesCount - 1);

            if ($.cookie("relationships-index") === "viewManagement") {
                if (typeof options != 'undefined') {
                    options.static = false;
                }
                managedOnly = false;
            }

            createGraph();

            if ($("#link-toggle").prop('checked')) {
                $.cookie("link-toggle", true);
                link.attr('class', getLinkClass);
            }
            else {
                $.cookie("link-toggle", false);
                link.attr('class', 'invisible');
            }

            if ($.cookie("link-toggle") === "true") {
                $("#link-toggle").prop('checked', true);
            } else {
                $("#link-toggle").prop('checked', false);
            }
        }

        /**
        * Get the companies object stored in the companies cookie.
        * @returns {} 
        */
        function getCookieCompanies() {
            var companiesCookie = $.cookie(companiesCookieId);
            var companies = null;
            if (companiesCookie) {
                companies = JSON.parse(companiesCookie);
            }
            return companies;
        }

        /**
        * Add a company to the companies cookie.
        * @param {} company 
        * @param {} checked 
        * @returns {} 
        */
        function updateCookieCompanies(company, checked) {
            var cookieCompanies = getCookieCompanies();
            cookieCompanies[company] = checked;
            $.cookie(companiesCookieId, JSON.stringify(cookieCompanies));
        }

        /**
        * Update the "select all" checkbox based on the number
        * of company checkboxes that are checked.
        * @param {} checked 
        * @returns {} 
        */
        function updateAllCompaniesCheckbox() {
            var checked = companyCheckboxes.filter(':checked');

            if (checked.length < companyCheckboxes.length) {
                allCompaniesCheckbox.prop('checked', false);
            }
            else {
                allCompaniesCheckbox.prop('checked', true);
            }
        }

        companyCheckboxes.change(function () {
            selectedCompanies.push($(this).val());
            updateAllCompaniesCheckbox();
            updateGraph();
            updateCookieCompanies($(this).val(), $(this).prop('checked'));
        });

        allCompaniesCheckbox.change(function () {
            if ($(this).prop('checked')) {
                companyCheckboxes.each(function () {
                    $(this).prop('checked', true);
                    updateCookieCompanies($(this).val(), true);
                });
            }
            else {
                companyCheckboxes.each(function () {
                    $(this).prop('checked', false);
                    updateCookieCompanies($(this).val(), false);
                });
            }

            updateGraph();
        });

        $("#relationships-tabs a").each(function () {
            if ($($(this).parent()).hasClass("active") && $(this).attr('href') === '#management') {
                updateGraph(true);
            } else {
                updateGraph();
            }

            if ($($(this).parent()).hasClass("active") && ($(this).attr("href") === "#ongoing" || $(this).attr("href") === "#businesscard" || $(this).attr("href") === "#leaver")) {
                updateGraph(true);
            }
        });

        $('#relationships-tabs a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
            var url = String(this);
            url = url.split('#')[1];
            $('#SelectedTab').val(url);
            $.cookie('relationships-index', url);

            if ($(this).attr("href") === "#ongoing" || $(this).attr("href") === "#businesscard" || $(this).attr("href") === "#leaver") {
                updateGraph(true);
            }

            if ($(this).attr('href') === '#management') {
                updateGraph(true);
            }
            else {
                updateGraph();
            }
        });

        $('#link-toggle').change(function () {
            if ($(this).prop('checked')) {
                $('#link-toggle').prop('checked', true);
                link.attr('class', getLinkClass);
                $.cookie(linkToggleCookieId, true);
                $.cookie("link-toggle", true);
            }
            else {
                $('#link-toggle').prop('checked', false);
                link.attr('class', 'invisible');
                $.removeCookie(linkToggleCookieId);
                $.removeCookie("link-toggle");
            }
        });
    };

    /**
     * Initialise business card slider.
     * @returns {} 
     */
    module.initialiseBusinessCardSlider = function () {
        // Find elements
        var businessCardContainer = $('.business-card-container');
        var businessCardSlider = businessCardContainer.find('.business-card-slider');
        var businessCardSlides = businessCardSlider.find('.business-card-slide');

        // Auto-select the node corresponding to the first card
        var firstContactId = businessCardSlides.first().data('contact-id');
        highlightNode(firstContactId);

        // Set up mappings from contact ID to slide number
        businessCardSlides.each(function (i) {
            var contactId = $(this).data('contact-id');
            slideMappings[contactId] = i;
        });

        // Init bxSlider
        slider = $(businessCardSlider).bxSlider({
            pager: false,
            controls: false,
            adaptiveHeight: true,
            wrapperClass: '',
            slideSelector: '.business-card-slide',
            onSlideAfter: function (slideElement) {
                var contactId = slideElement.data('contact-id');
                highlightNode(contactId);
            },
            onSliderLoad: function () {
                businessCardSlider.css('visibility', 'visible');
            }
        });

        /**
         * Previous arrow click handler.
         */
        businessCardContainer.find('.prev-arrow').click(function (e) {
            e.preventDefault();
            slider.goToPrevSlide();
        });

        /**
         * Next arrow click handler.
         */
        businessCardContainer.find('.next-arrow').click(function (e) {
            e.preventDefault();
            slider.goToNextSlide();
        });

        /**
         * Fix - save select contact on card click
         */

        $('.business-card-slide').on('click', function () {
            var contactId = $(this).data('contact-id');
            $.cookie('savingContactCardId', contactId);
        });

    };

    module.initialiseSelectAll = function () {
        $('#BttcAll').change(function () {
            if ($(this).is(':checked')) {
                $(this).closest('.form-group').find('input[type=checkbox]').prop('checked', $(this).prop('checked'));
            }
        });
        // untick Select All checkbox
        $('#BttcAll').closest('.form-group').find('input[type=checkbox]').change(function () {
            if (!$(this).is(':checked')) {
                $('#BttcAll').prop('checked', false);
            }
        });
    };

    /**
    * Initialise confirmation dialog on quick add person form.
    */
    module.initialisePersonConfirmation = function () {
        $('#relationship-confirmation').click(function (e) {
            var selectedOption = $('#ContactId option:selected');

            if (!selectedOption.val()) {
                alert('Please select a relationship.');
                return false;
            }

            var message = 'Click OK to add \'' + selectedOption.text() + '\' or Cancel to return to the previous page.';

            if (!confirm(message)) {
                var returnUrl = $(this).data('return-url');

                if (returnUrl) {
                    window.location = returnUrl;
                }

                return false;
            }
        });
    };

    module.initialiseNewCompany = function () {

        function showOtherCompanyName() {
            $('#new-company').collapse('toggle');
            $('#Person_CompanyId').attr('disabled', 'disabled');
            $('#company-dropdown').find('.combobox').val('Other').attr('disabled', 'disabled');
            $('.input-group-addon').css('pointer-events', 'none');

            $('#Person_CompanyId option').attr('selected', false);
            $('#Person_CompanyId option:contains("Other")').attr('selected', 'selected');
            $('input[name=\'Person.CompanyId\']').val($('#Person_CompanyId').find(':selected').val());
        }

        function hideOtherCompanyName() {
            $('#new-company').collapse('toggle');
            $('#Person_CompanyId').removeAttr('disabled');
            $('#company-dropdown').find('.combobox').val('').removeAttr('disabled');
            $('#company-dropdown').find('.combobox-container').removeClass('combobox-selected');
            $('.input-group-addon').css('pointer-events', 'auto');
            $('#Person_CompanyId option').attr('selected', false);
            $('input[name=\'Person.CompanyId\']').val('');
        }

        $('#Person_OtherCompany').click(function () {
            if ($(this).is(':checked')) {
                showOtherCompanyName();
            }
            else {
                hideOtherCompanyName();
            }
            $('#Person_CompanyId').combobox();
        });

        $('input[name=\'Person.CompanyId\']').change(function () {
            if ($('#company-dropdown').find('.combobox').val() === 'Other') {
                $('#Person_OtherCompany').prop('checked', true);
                showOtherCompanyName();
            }
            else {
                $('#Person_OtherCompany').prop('checked', false);
            }
        });

        $(function () {
            if ($('#Person_OtherCompany').is(':checked')) {
                showOtherCompanyName();
                $('input[name=\'Person.CompanyId\']').val($('#Person_CompanyId').find(':selected').val());
            }
        });
    };

    module.initialiseManagement = function () {
        function management() {
            if ($('#Managed').is(':checked')) {
                $('#EmploymentStatus').removeAttr('disabled');
                $('#FutureTraining').removeAttr('readonly');
                $('#ManagementNotes').removeAttr('readonly');
            }
            else {
                $('#EmploymentStatus').attr('disabled', true);
                $('#FutureTraining').attr('readonly', true);
                $('#ManagementNotes').attr('readonly', true);
            }
        }

        $(function () {
            management();
        });

        $('#Managed').on('change', function (e) {
            management();
        });
    };

    module.initialiseSubmitAnchors = function () {

        $('a.submit').click(function () {

            var name = $(this).data('name');
            var value = $(this).data('value');
            $(this).closest('form').append($('<input>', { type: 'hidden', name: name, value: value })).submit();

            return false;
        });
    };

    $(function () {
        module.initialiseSelectAll();
        module.initialisePersonConfirmation();
        module.initialiseNewCompany();
        module.initialiseManagement();
        module.initialiseSubmitAnchors();
        module.initialiseBusinessCardSlider();

        ignore_onbeforeunload = true;

        $('.relationship-start .date input').change(function () {
            $('#relationship-length').val(moment.duration(moment().diff(moment($(this).val(), 'DD-MMM-YYYY'))).humanize());
        }).change();

        // Javascript to enable link to tab
        var url = document.location.toString();
        if (url.match('#')) {
            $('.nav-tabs a[href=#' + url.split('#')[1] + ']').tab('show');

            setTimeout(function () {
                window.scrollTo(0, 0);
            }, 1);
        }

        if ($("#SelectedTab").val() != null) {
            $('.nav-tabs a[href=#' + $("#SelectedTab").val() + ']').tab('show');
        }

        // Change hash for page-reload
        $('.nav-tabs a').on('shown.bs.tab',
            function (e) {
                window.location.hash = e.target.hash;
                window.scrollTo(0, 0);
            });

        $('.relationship-start .date input').change(function () {
            var val = $(this).val();
            var result = 'Relationship length';
            if (val) {
                result = moment.duration(moment().diff(moment(val, 'DD-MMM-YYYY'))).humanize();
            }
            $('#relationship-length').val(result);
        }).change();

        moment.locale('en', {
            relativeTime: {
                s: 'seconds',
                m: '1 minute',
                mm: '%d minutes',
                h: '1 hour',
                hh: '%d hours',
                d: '1 day',
                dd: '%d days',
                M: '1 month',
                MM: '%d months',
                y: '1 year',
                yy: '%d years'
            }
        });
    });

    return module;

})(ACTIVATE || {});