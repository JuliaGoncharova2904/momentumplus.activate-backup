﻿/**
* Augment the ACTIVATE JavaScript module with the methods required for
* the summary page widgets.
*/
var ACTIVATE = (function (module)
{

    var packageId;
    var widgetPosition;

    /**
     * Initialise modal window for selecting new widgets.
     */
    module.initialiseWidgetModal = function ()
    {
        /**
         * Store the values of the package ID and widget position
         * when the modal is brought up.
         */
        $('#widget-modal').on('show.bs.modal', function (event)
        {
            packageId = $(event.relatedTarget).data('package-id');
            widgetPosition = $(event.relatedTarget).data('widget-number');

        });

        /**
         * Update the widget on the server.
         */
        $('#update-widget').click(function ()
        {
            var widgetType = $('#WidgetType').val();

            $.get('/Home/Widgets/UpdateWidget', {
                packageId: packageId,
                widgetPosition: widgetPosition,
                widgetType: widgetType
            }, function (success)
            {
                if (success)
                {
                    document.location.reload(true);
                } else
                {
                    alert('There was an error adding the widget. Please report this problem.');
                }
            });
        });
    };

    /**
     * Initialise remove widget controls.
     */
    module.initialiseRemoveWidgetControls = function ()
    {
        $('.remove-widget').click(function ()
        {
            if (confirm('Are you sure you want to remove this widget?'))
            {
                var packageId = $(this).data('package-id');
                var widgetPosition = $(this).data('widget-position');

                $.get('/Home/Widgets/RemoveWidget', {
                    packageId: packageId,
                    widgetPosition: widgetPosition
                }, function (success)
                {
                    if (success)
                    {
                        document.location.reload(true);
                    } else
                    {
                        alert('There was an error removing the widget. Please report this problem.');
                    }
                });
            }
        });
    };

    /**
     * Initiailise widget slider controls.
     */
    module.initialiseWidgetSliders = function ()
    {

        $('.widget-container').each(function ()
        {
            var slider;
            var widgetContainer = $(this);
            var widgetBody = widgetContainer.find('.widget-body');
            var widgetSlider = widgetContainer.find('.widget-slider');
            var currentSlide = widgetContainer.find('.current-slide');
            var attachmentsCount = widgetContainer.find('.attachments-count');
            var voicemessagesCount = widgetContainer.find('.voicemessages-count');
            var attachmentsLink = widgetContainer.find('.attachments-link');
            var voicemessagesLink = widgetContainer.find('.voicemessages-link');
            /**
             * Update the current slide number on the UI.
             */
            function updateCurrentSlide()
            {
                var currentSlideNumber = slider.getCurrentSlide() + 1;
                currentSlide.text(currentSlideNumber);
                var type = widgetBody.find('.panel-default').data('type');
                module.updateWidgetCookie(type, currentSlideNumber);
            }

            /**
             * Update attachment/voice message counts on the UI.
             */
            function updateCounts(slideElement)
            {

                var attachmentsCountData = slideElement.data('attachments-count');
                attachmentsCount.text(attachmentsCountData);
                var voicemessagesCountData = slideElement.data('voicemessages-count');
                voicemessagesCount.text(voicemessagesCountData);

                var attachmentsUrl = slideElement.data('attachments-url');
                attachmentsLink.attr('href', attachmentsUrl);
                var voicemessagesUrl = slideElement.data('voicemessages-url');
                voicemessagesLink.attr('href', voicemessagesUrl);
            }

            //Fixes update saving slider -AC 52

            var slideNo = parseInt(widgetContainer.find('.current-slide').text()) - 1;
            var savingSlider = widgetContainer.find('.widget-slide').eq(slideNo);

            updateCounts(savingSlider);

            if (widgetSlider.length)
            {
                slider = widgetSlider.bxSlider({
                    startSlide: slideNo,
                    pager: false,
                    controls: false,
                    wrapperClass: '',
                    onSlideBefore: updateCounts,
                    onSlideAfter: updateCounts,
                    adaptiveHeight: true,
                    onSliderLoad: function ()
                    {
                        widgetBody.css('visibility', 'visible');
                    }
                });
            } else
            {
                widgetBody.css('visibility', 'visible');
            }

            widgetContainer.find('.prev-arrow').click(function (e)
            {
                e.preventDefault();
                slider.goToPrevSlide();
                updateCurrentSlide();
            });

            widgetContainer.find('.next-arrow').click(function (e)
            {
                e.preventDefault();
                slider.goToNextSlide();
                updateCurrentSlide();
            });

            widgetContainer.find('.first-arrow').click(function (e)
            {
                e.preventDefault();
                slider.goToSlide(0);
                updateCurrentSlide();
            });

            widgetContainer.find('.last-arrow').click(function (e)
            {
                e.preventDefault();
                slider.goToSlide(slider.getSlideCount() - 1);
                updateCurrentSlide();
            });
        });
    };

    module.updateWidgetCookie = function (widget, index)
    {
        $.cookie.raw = true;
        widgetCookie = index;
        $.cookie(widget, widgetCookie, { path: '/' });
    };

    $(function ()
    {
        module.initialiseWidgetModal();
        module.initialiseRemoveWidgetControls();
        module.initialiseWidgetSliders();
    });

    return module;

})(ACTIVATE || {});
