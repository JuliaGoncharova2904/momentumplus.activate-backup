﻿/**
* Augment the ACTIVATE JavaScript module with the methods required for
* the to do list functionality.
*/
var ACTIVATE = (function (module) {

    /**
     * Initialise dismiss to do list item.
     */

    module.initialiseDismissToDoItem = function () {
        var pageCount = $("#toDoList tr").length;

        $('.dismiss-todo').click(function () {
            var that = $(this);
            var toDoId = $(this).data('id');
            
            $.ajax({
                method: "POST", url: '/Home/ToDoItem/Dismiss', data: { id: toDoId },
                success: function () {
                    that.closest('tr').fadeOut(250, 'linear');
                    toDoNum = toDoNum - 1;
                    pageCount = pageCount - 1;
                    
                    if (pageCount == 0 && pageNo > 1) {
                        var link = window.location.href;
                        link = link.replace("toDoPageNo=" + pageNo, "toDoPageNo=" + (pageNo - 1));
                        window.location.href = link;
                    }
                    if (toDoNum % pageSize == 0 && pageNo <= Math.floor(toDoNum / pageSize)) {
                        window.location.reload();
                    }
                },
                error: function () {
                    //alert('There was an error dismissing the item. Please report this problem.');
                }
            });
        });
    };

    $(function () {
        module.initialiseDismissToDoItem();
    });

    return module;

})(ACTIVATE || {});