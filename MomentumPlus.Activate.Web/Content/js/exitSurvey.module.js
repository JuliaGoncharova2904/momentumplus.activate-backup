﻿$(document).ready(function () {

    var panel = $("#exitSurveyInterview");
    var userId = $("#UserId", panel).val();
    var exitSurveyId = $("#ExitSurveyId", panel).val();
    var answerForm = {};

    function answerOnClick(e) {
        var dataId = $(e).attr("dataid");

        var config = dataId.split("_");
        var questionId = config[0];
        var sectionId = config[1];

        var answerInput = "";
        if ($(e)[0].tagName === "TEXTAREA" && $(e).hasClass("answer-text-class")) {
            answerInput = $('.answer-text-class[dataid="' + dataId + '"]');
        } else {
            answerInput = $('.btn[dataid="' + dataId + '"]');
        }

        var answerItem = [];

        var type = $(e).attr("datatype");
        var numericType = 0;

        switch (type) {
            case "Multi":
                numericType = 1;
                break;
            case "Radio":
                numericType = 0;
                break;
            case "Text":
                numericType = 2;
                break;
            case "Rating":
                numericType = 3;
                break;
        }

        for (var i = 0; i < answerInput.length; i++) {
            var item = answerInput[i];
            var answerJson = {
                Name: numericType === 2 ? $(item).attr("name") : $(item).attr("answer"),
                Type: numericType,
                Value: $(item).prop("checked"),
                TextAnswer: $(item)[0].tagName === "TEXTAREA" ? $(item).val() : null
            }

            answerItem.push(answerJson);
        }

        var answerObj = {
            Id: dataId,
            QuestionId: questionId,
            SectionId: sectionId,
            Answer: JSON.stringify(answerItem),
            UserNotes: $('.notes-box[dataid="' + dataId + '"]').val(),
            IsUsersNotesApproved: $('.user-approved[dataid="' + dataId + '"]').prop("checked"),
            IsHRApproved: $('.hr-approved[dataid="' + dataId + '"]').prop("checked"),
            HRComments: $('.hr-comment-box[dataid="' + dataId + '"]').val()
        };

        answerForm[dataId] = answerObj;
    }

    $(".answer-class, .answer-text-class", panel).on("click change", function () {
        var _this = this;
        answerOnClick(_this);
        save();
    });

    $(".checkbox-approved", panel).on("click", $.debounce(200, function () {
        var _this = this;

        var dataId = $(_this).attr("dataid");

        var ans = $('.answer-class[dataid="' + dataId + '"]');
        answerOnClick(ans);
        save();
    }));

    $(".hr-comment-box", panel).on("change", $.debounce(200, function () {
        var _this = this;

        var dataId = $(_this).attr("dataid");

        var ans = $('.answer-class[dataid="' + dataId + '"]');
        answerOnClick(ans);
        save();
    }));

    $(".notes-box", panel).on("change", $.debounce(200, function () {
        var _this = this;

        var dataId = $(_this).attr("dataid");

        var ans = $('.answer-class[dataid="' + dataId + '"]');
        answerOnClick(ans);
        save();
    }));

    var isOwnerSurvey = $("#IsOwnerSurvey").val() === "True";

    if (!isOwnerSurvey) {
        $("#saveExitSurveyAnswers").prop("disabled", true);
    }


    function save() {

        var sendResult = [];

        $.each(answerForm, function (index, value) {
            sendResult.push(value);
        });

        $.ajax({
            url: "/Home/ExitSurvey/SaveAnswers",
            method: "POST",
            data: {
                userId: userId,
                answers: JSON.stringify(sendResult),
                exitSurveyId: $("#ExitSurveyId").val()
            },
            success: function (result) {
                var htmlObject = document.createElement("div");
                htmlObject.innerHTML = result;
                if (htmlObject.length > 0) {
                    $("#newContent").html(htmlObject);

                    $('script', htmlObject).each(function () {
                        if ($(this).attr("src") !== undefined) {
                            $.ajax({
                                url: $(this).attr("src"),
                                method: "GET"
                            });
                        }
                        else {
                            eval($(this).text());
                        }
                    });
                }
            }
        });
    }

    $("#saveExitSurveyAnswers", panel).click(function () {

        var sendResult = [];

        $.each(answerForm, function (index, value) {
            sendResult.push(value);
        });

        $.ajax({
            url: "/Home/ExitSurvey/SaveAnswers",
            method: "POST",
            data: {
                userId: userId,
                answers: JSON.stringify(sendResult),
                exitSurveyId: $("#ExitSurveyId").val()
            },
            success: function (result) {
                var htmlObject = document.createElement("div");
                htmlObject.innerHTML = result;
                if (htmlObject.length > 0) {
                    $("#newContent").html(htmlObject);

                    $('script', htmlObject).each(function () {
                        if ($(this).attr("src") !== undefined) {
                            $.ajax({
                                url: $(this).attr("src"),
                                method: "GET"
                            });
                        }
                        else {
                            eval($(this).text());
                        }
                    });
                }

                //if (result === "ok") {
                //    $("#loading-animation").attr("style", "display: block;");
                //    window.location.replace("/Home/ExitSurvey/ViewSurvey/?exitSurveyId=" + exitSurveyId + "&userOwnerId=" + userId);
                //}
            }
        });
    });

    $(".submit-survey", panel).click(function () {
        var _this = this;
        var sendResult = [];

        $.each(answerForm, function (index, value) {
            sendResult.push(value);
        });

        $.ajax({
            url: "/Home/ExitSurvey/SubmitExitSurvey",
            method: "POST",
            data: {
                exitSurveyId: exitSurveyId,
                userId: userId,
                submitType: $(_this).attr("datatype"),
                answers: JSON.stringify(sendResult)
            },
            success: function (result) {
                if (result === "ok") {
                    $("#loading-animation").attr("style", "display: block;");
                    window.location.replace("/Home/ExitSurvey/ViewSurvey/?exitSurveyId=" + exitSurveyId + "&userOwnerId=" + userId);
                }
            }
        });
    });
});