﻿angular.module('momentumPlus.activate')

.controller('timelineActionCtrl', function ($scope) {
    var timelineAction = $("#timelineAction");
    var control = $(timelineAction).attr("control");

    if (control === "True") {
        $(timelineAction).attr("style", "display: block;");
    }
    
    $scope.message = 'Hide Timeline';
    $scope.state = 1;
        $scope.action = function () {
        if ($scope.state === 1) {
            $scope.message = 'Show Timeline';
            $scope.state = 0;
            $('#timelineCtrl').hide();
        } else {
            $scope.message = 'Hide Timeline';
            $scope.state = 1;
            $('#timelineCtrl').show();
        }
        
    }
})

.controller('timelineCtrl', function ($scope, $http) {
});

