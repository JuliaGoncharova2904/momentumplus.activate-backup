﻿/**
* Augment the ACTIVATE JavaScript module with the methods required for
* the topic forms.
*/
var ACTIVATE = (function (module) {

    /**
    * Initialise Location topic form.
    */
    module.initialiseLocationForm = function () {
        /**
        * Update address and map link fields when a workplace is
        * selected from the list of suggestions.
        */
        $('#LocationId').change(function () {
            var workplaceId = $(this).val();

            if (workplaceId) {
                $.getJSON('/Workplace/GetWorkplaceInfo/' + workplaceId, function (data) {
                    if (data && data.address || data.mapLink) {
                        $('#Address').val(data.address);
                        $('#MapLink').val(data.mapLink);
                    }
                });
            }
        });
    };

    /**
    * Initialise Project topic form.
    */
    module.initialiseProjectForm = function () {
        /**
        * Update sponser and manager fields when a project is
        * selected from the list of suggestions.
        */
        $('#ProjectId').change(function () {
            var projectId = $(this).val();

            if (projectId) {
                $.getJSON('/Framework/Projects/GetProjectInfo/' + projectId, function (data) {
                    if (data && data.projectSponsor) {
                        $('#ProjectSponsor').val(data.projectSponsor);
                    }
                });
            }
        });
    };

    /**
     * allow anchor tags to submit forms with additional values
     */
    module.initialiseSubmitAnchors = function () {

        $("a.submit").click(function () {

            var name = $(this).data('name');
            var value = $(this).data('value');
            $(this).closest('form').append($('<input>', { type: 'hidden', name: name, value: value })).submit();

            return false;
        });
    };

    $(function () {
        module.initialiseLocationForm();
        module.initialiseProjectForm();
        module.initialiseSubmitAnchors();
    });

    return module;

})(ACTIVATE || {});
