﻿/**
* Augment the ACTIVATE JavaScript module with the methods required for
* the people module.
*/
var ACTIVATE = (function (module) {

    module.initialiseNewCompany = function () {

        function showOtherCompanyName() {
            $('#new-company').collapse('toggle');
            $("#CompanyId").attr("disabled", "disabled");
            $('#company-dropdown').find('.combobox').val("Other").attr("disabled", "disabled");
            $('.input-group-addon').css("pointer-events", "none");

            $('#CompanyId option').attr('selected', false);
            $('#CompanyId option:contains("Other")').attr('selected', 'selected');
            $("input[name='CompanyId']").val($('#CompanyId').find(":selected").val());
        }

        function hideOtherCompanyName() {
            $('#new-company').collapse('toggle');
            $("#CompanyId").removeAttr("disabled");
            $('#company-dropdown').find('.combobox').val("").removeAttr("disabled");
            $('#company-dropdown').find('.combobox-container').removeClass('combobox-selected');
            $('.input-group-addon').css("pointer-events", "auto");
            $('#CompanyId option').attr('selected', false);
            $("input[name='CompanyId']").val("");
        }

        $('#OtherCompany').click(function () {
            if ($(this).is(":checked")) {
                showOtherCompanyName();
            } else {
                hideOtherCompanyName();
            }
            $('#CompanyId').combobox();
        });

        $("input[name='CompanyId']").change(function () {
            if ($('#company-dropdown').find('.combobox').val() === 'Other') {
                $("#OtherCompany").prop("checked", true);
                showOtherCompanyName();
            } else {
                $("#OtherCompany").prop("checked", false);
            }
        });

        $(function () {
            if ($('#OtherCompany').is(":checked")) {
                showOtherCompanyName();
                $("input[name='CompanyId']").val($('#CompanyId').find(":selected").val());
            }
        });
    };

    $(function () {
        module.initialiseNewCompany();
    });

    return module;

})(ACTIVATE || {});
