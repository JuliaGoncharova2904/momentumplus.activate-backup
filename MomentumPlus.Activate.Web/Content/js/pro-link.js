﻿angular.module('momentumPlus.activate')

/**
* Controller for the pro-link manager.
*/
.controller('ProLinkCtrl', ['$scope', '$window', '$http', function ($scope, $window, $http) {

    $scope.functions = functions;
    $scope.functionsPage = [];
    $scope.functionsPageNo = 1;

    $scope.activeFunction;
    $scope.activeProcessGroup;
    $scope.pageSize = 10;

    $scope.currentProcessGroups = [];
    $scope.currentProcessGroupsPage = [];
    $scope.processGroupPageNo = 1;

    $scope.currentTopics = [];
    $scope.topicsPage = [];
    $scope.topicPageNo = 1;

    $scope.alerts = [];

    $scope.saving = false;

    /**
    * Remove the alert at the given index.
    */
    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.functionSelect = function (func) {
        if ($scope.activeFunction)
            $.removeCookie('selectedProcessGroup');

        $scope.activeFunction = func;
        $scope.activeProcessGroup = null;
        $scope.currentProcessGroups = func.ProcessGroups;
        $scope.processGroupPageNo = 1;
        $scope.processGroupPageChange();
        $scope.topicsPage = [];

        var expireDate = new Date();
        expireDate.setHours(expireDate.getHours() + 1);

        $.cookie('selectedFunction', func.ID, { expires: expireDate });
    };


    $scope.processGroupSelect = function (procGroup) {
        $scope.activeProcessGroup = procGroup;
        $scope.currentTopics = procGroup.Topics;
        $scope.topicPageNo = 1;
        $scope.topicPageChange();

        var expireDate = new Date();
        expireDate.setHours(expireDate.getHours() + 1);

        $.cookie('selectedProcessGroup', procGroup.ID, { expires: expireDate });
    };

    $scope.functionsPageChange = function () {
        var pageStartIndex = ($scope.functionsPageNo - 1) * $scope.pageSize;
        var pageEndIndex = pageStartIndex + $scope.pageSize;
        $scope.functionsPage = $scope.functions.slice(pageStartIndex, pageEndIndex);

        $scope.currentProcessGroups = [];
        $scope.currentProcessGroupsPage = [];
        $scope.processGroupPageNo = 1;

        $scope.currentTopics = [];
        $scope.topicsPage = [];
        $scope.topicPageNo = 1;

        $scope.activeFunction = null;
        $scope.activeProcessGroup = null;
    };
    $scope.functionsPageChange();

    $scope.processGroupPageChange = function () {
        var pageStartIndex = ($scope.processGroupPageNo - 1) * $scope.pageSize;
        var pageEndIndex = pageStartIndex + $scope.pageSize;
        $scope.currentProcessGroupsPage = $scope.currentProcessGroups.slice(pageStartIndex, pageEndIndex);

        $scope.currentTopics = [];
        $scope.topicsPage = [];
        $scope.topicPageNo = 1;

        $scope.activeProcessGroup = null;
    };

    $scope.topicPageChange = function () {
        var pageStartIndex = ($scope.topicPageNo - 1) * $scope.pageSize;
        var pageEndIndex = pageStartIndex + $scope.pageSize;
        $scope.topicsPage = $scope.currentTopics.slice(pageStartIndex, pageEndIndex);
    };

    $scope.createProcessGroupClick = function ($event) {
        if (!$scope.activeFunction) {
            $event.preventDefault();
            alert("Please select a function.");
        }
    };

    $scope.createProcessGroupClick = function ($event) {
        if (!$scope.activeFunction) {
            $event.preventDefault();
            alert("Please select a process group.");
        }
    };

    $scope.saveChanges = function () {
        $scope.saving = true;

        $http.post('/Framework/ProLink/UpdateTopics', $scope.topics)
            .success(function (data, status, headers, config) {
                $scope.alerts = [{
                    type: 'success',
                    msg: 'Topics updated successfully.'
                }];

                $scope.saving = false;
            })
            .error(function (data, status, headers, config) {
                $scope.alerts = [{
                    type: 'danger',
                    msg: 'Something went wrong, please try again.'
                }];

                $scope.saving = false;
            });

        $.removeCookie('selectedProcessGroup');
        $.removeCookie('selectedFunction');
    };

    /**
    * Clear changes - simply reload the page
    */
    $scope.clearChanges = function () {
        $.removeCookie('selectedProcessGroup');
        $.removeCookie('selectedFunction');
        //reloadPage();
    };

    var reloadPage = function () {
        $window.location.href = $window.location.href;
    };


    // handle selections from cookies
    if ($.cookie('selectedFunction')) {
        var selectedFunctionID = $.cookie('selectedFunction');
        for (var i in $scope.functions) {
            if ($scope.functions[i].ID == selectedFunctionID) {
                $scope.functionSelect($scope.functions[i]);
            }
        }
    }

    if ($.cookie('selectedProcessGroup')) {
        var selectedProcessGroupID = $.cookie('selectedProcessGroup');
        for (var i in $scope.currentProcessGroups) {
            if ($scope.currentProcessGroups[i].ID == selectedProcessGroupID) {
                $scope.processGroupSelect($scope.currentProcessGroups[i]);
            }
        }
    }

} ]);