﻿/**
* Augment the ACTIVATE JavaScript module with the methods required for
* the package transfer form.
*/
var ACTIVATE = (function (module) {

    /**
    * Initialise package selector dropdown.
    */
    module.initialisePackageSelector = function () {
        /**
        * Update topic groups/topics dropdown when a package
        * is selected.
        */
        $('#TargetPackageId').change(function () {
            var targetPackageId = $(this).val();

            if (targetPackageId) {
                var url = module.updateQueryStringParameter(window.location.href,
                    'targetPackageId', targetPackageId);

                // Prevent unload alert
                window.onbeforeunload = null;

                window.location.href = url;
            }
        });
    };

    $(function () {
        module.initialisePackageSelector();
    });

    return module;

})(ACTIVATE || {});
