﻿/**
* Momentum+ Activate JavaScript module.
*/
var ACTIVATE = (function (module)
{
    /**
    * Utilitly method for adding or a updating a parameter on a
    * query string. If a null value is given then the parameter
    * is removed from the query string.
    */
    module.updateQueryStringParameter = function (uri, key, value)
    {
        var re = new RegExp('([?&])' + key + '=.*?(&|$)', 'i');
        var separator = uri.indexOf('?') !== -1 ? '&' : '?';

        if (uri.match(re))
        {
            if (value === null)
            {
                return uri.replace(re, '');
            } else
            {
                return uri.replace(re, '$1' + key + '=' + value + '$2');
            }
        }
        else
        {
            if (value === null)
            {
                return uri;
            } else
            {
                return uri + separator + key + '=' + value;
            }
        }
    };

    /**
    * Initialise Bootstrap JS components.
    */
    module.initialiseComponents = function ()
    {
        $('#accordion').on('shown.bs.collapse', function ()
        {
            var panel = $(this).find('.in');

            $('html, body').animate({
                scrollTop: panel.offset().top
            }, 500);
        });

        //Fix close dropdown when click outside
        $(document).ready(function ()
        {
            $('[data-toggle=tooltip]').tooltip();
            $('.combobox').combobox();
            $('.input-group.date').datepicker();
            $('.selectpicker').selectpicker({ noneSelectedText: '' });
            $('.ms-dropdown').msDropdown({ rowHeight: 30 });
            
            $(".dropdown-toggle").dropdown();
            $('body').on('click', function (e)
            {
                $('#master-package-selector').removeClass('open');
            });
        });
    };

    /**
    * Initialise popover components with close buttons in the title.
    */
    module.initialisePopovers = function ()
    {
        $('[data-toggle=popover]').popover({
            html: true,
            title: function ()
            {
                return $(this).data('title') + '<span class="close">&times;</span>';
            },
            container: 'body'
        }).on('shown.bs.popover', function (e)
        {
            $('[data-toggle=popover]').not(this).popover('hide');

            var popover = $(this);

            $('body').find('div.popover .close').on('click', function (e)
            {
                popover.popover('hide');
            });
            $('body').on('click', function (e)
            {
                $('[data-toggle="popover"]').each(function ()
                {
                    if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0)
                    {
                        $(this).popover('hide');
                    }
                });
            });
        });

    };

    /**
    * Initialise typeahead.js controls.
    */
    module.initialiseTypeaheads = function ()
    {
        /**
        * Create a function that matches substrings in a given
        * set of select list items.
        */
        var substringMatcher = function (items)
        {
            /**
            * Check for substring matches in the Text properties
            * of the items.
            */
            return function (query, callback)
            {
                var matches = [];
                var substrRegex = new RegExp(query, 'i');

                $.each(items, function (i, item)
                {
                    if (substrRegex.test(item.Text))
                    {
                        matches.push({ value: item.Text, id: item.Value });
                    }
                });

                callback(matches);
            };
        };

        $('.typeahead').each(function ()
        {
            var relatedElement = $($(this).data('relation'));
            var items = $(this).data('source');
            var source = substringMatcher(items);

            $(this).typeahead({}, {
                source: source
            }).on('typeahead:selected', function (event, suggestion)
            {
                // Set the value of the related input to the ID of the
                // chosen item and trigger change event for any further
                // handling.
                relatedElement.val(suggestion.id).change();
            }).change(function ()
            {
                var element = $(this);
                var value = element.val();

                // Check if the value entered matches exactly to one of the
                // suggestions (ignoring case). If so then the ID of the
                // related element is set to the ID of the suggestion.
                // Otherwise the related element value is cleared.
                source(value, function (suggestions)
                {
                    if (suggestions.length > 0)
                    {
                        for (var i = 0; i < suggestions.length; i += 1)
                        {
                            var suggestion = suggestions[i];

                            if (value.toLowerCase() === suggestion.value.toLowerCase())
                            {
                                relatedElement.val(suggestion.id);
                            } else
                            {
                                relatedElement.val(null);
                            }
                        }
                    } else
                    {
                        relatedElement.val(null);
                    }

                    relatedElement.change();
                });
            });
        });
    };

    /**
    * Initialise search tag tokenfield/typeahead components.
    */
    module.initialiseSearchTagTypeaheads = function ()
    {
        $('.search-tags').each(function ()
        {
            // Create a new suggestion engine wired up to the search tags
            // suggestion API method.
            var engine = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote: '/Framework/SearchTags/GetSuggestions?query=%QUERY'
            });

            engine.initialize();

            $(this).tokenfield({
                typeahead: [{ createTokensOnBlur: true }, { source: engine.ttAdapter() }]
            });
        });
    };

    /**
    * Initialise form controls that enable or disable (and show or hide)
    * other controls based on a selected value.
    */
    module.initialiseToggleStates = function ()
    {
        $('[data-toggle-state]').each(function ()
        {
            var enableVal = $(this).data('enable-val');
            var targetElementSelector = $(this).data('toggle-state');

            $(this).change(function ()
            {
                var val = $(this).val();

                if ($(this).is(':checked') || val === enableVal)
                {
                    $(targetElementSelector).prop('disabled', false);
                    $(targetElementSelector).closest('.form-group').removeClass('hidden');
                } else
                {
                    $(targetElementSelector).prop('disabled', true);
                    $(targetElementSelector).closest('.form-group').addClass('hidden');
                }
            });
        });
    };

    /**
    * Initialise the package filter dropdown.
    */
    module.initialisePackageFilter = function ()
    {
        $('#SelectedPackageStatus').change(function ()
        {
            var url = module.updateQueryStringParameter(window.location.href,
                'packageStatus', $(this).val());
            window.location.href = url;
        });
    };

    /**
    * Initialise "Go To" buttons for opening URLs input to fields
    * in a new window.
    */
    module.initialiseGoToButtons = function ()
    {
        $('[data-go-to]').each(function ()
        {
            var elementSelector = $(this).data('go-to');

            $(this).click(function ()
            {
                var elementValue = $(elementSelector).val();

                if (elementValue)
                {
                    // Find the beginning of the URL in the string
                    var addressStart = elementValue.search(/http(s)?:\/\//);
                    var url;

                    if (addressStart === -1)
                    {
                        // Prefix input value with protocol and use
                        // this as the URL.
                        url = elementValue.replace(/^/, 'http://');
                    } else if (addressStart > -1)
                    {
                        // Take from the beginning of the protocol to
                        // the end of the string as the URL.
                        url = elementValue.slice(addressStart);
                    }

                    if (url)
                    {
                        window.open(url, '_blank');
                    }
                }

                return false;
            });
        });
    };

    /**
    * Initialise "Get Directions" buttons.
    */
    module.initialiseGetDirectionsButtons = function ()
    {
        $('[data-get-directions]').each(function ()
        {
            var $this = $(this);
            var elementSelector = $this.data('get-directions');

            $(elementSelector).keyup(function ()
            {
                if (!$(this).val().length)
                {
                    $this.addClass('disabled');
                } else
                {
                    $this.removeClass('disabled');
                }
            }).keyup();

            $this.click(function ()
            {
                var elementValue = $(elementSelector).val();

                if (elementValue)
                {
                    // Replace line breaks
                    elementValue = elementValue.replace(/(\r\n|\r|\n)/g, '+');
                    var url = 'https://maps.google.com?daddr=' + elementValue;
                    window.open(url, '_blank');
                }

                return false;
            });
        });
    };

    /**
    * Initialise left-hand sliding menu.
    */
    module.initialiseSnapDrawer = function ()
    {
        var snapper = new Snap({
            element: document.getElementById('content'),
            disable: 'right',
            hyperextensible: false,
            maxPosition: 200,
            minPosition: -200,
            touchToDrag: false
        });

        /**
        * Add click event to menu toggle buttons.
        */
        $('#open-button, #close-button').click(function ()
        {
            if (snapper.state().state === 'left')
            {
                snapper.close();
                $('.drawers').css("position", "");
            } else
            {
                $('.left-drawer').css("display", "block");
                snapper.open('left');
                $('.drawers').css("position", "fixed");
            }
        });
    };

    /**
    * Initialise panel headings click events.
    */
    module.initialisePanelHeadings = function ()
    {
        $('.panel-heading:not(.no-redirect)').each(function ()
        {
            if ($(this).find('a').length > 0)
            {
                $(this).css('cursor', 'pointer');

                $(this).click(function (e)
                {
                    // Only redirect when the element clicked was the heading
                    // itself and not a child element.
                    var target = $(e.target);
                    if (target.is('.panel-heading'))
                    {
                        var anchor = $(this).find('a').first();
                        var url = anchor.attr('href');

                        if (url)
                        {
                            window.location = url;
                        } else
                        {
                            anchor.click();
                        }
                    }
                        // React to arrow clicks
                    else if (target.is('.glyphicon-triangle-right') || target.is('.glyphicon-triangle-bottom'))
                    {
                        var firstAnchor = target.closest('.panel-heading').find('a').first();
                        if (firstAnchor)
                        {
                            firstAnchor.click();
                        }
                    }
                });
            }
        });
    };

    /**
    * Initialise elements that should cause their containing
    * form to submit when changed.
    */
    module.initialiseOnChangeSubmits = function ()
    {
        $('.on-change-submit').change(function ()
        {
            $(this).closest('form').submit();
        });
    };

    /**
    * Initialise basic tokenfield inputs.
    */
    /**
    * Update an active topic groups cookie - if the given topic group is
    * active then it is removed from the cookie, otherwise it is added.
    */
    module.updateActiveTopicGroups = function (cookieName, topicGroupIndex)
    {
        $.cookie.raw = true;
        var activeTopicGroupsCookie = $.cookie(cookieName);
        var separator = ',';
        var activeTopicGroups;

        if (activeTopicGroupsCookie)
        {
            // Cookie exists - check for existence of topic group
            activeTopicGroups = activeTopicGroupsCookie.split(separator).map(Number);
            var indexOfTopicGroup = activeTopicGroups.indexOf(topicGroupIndex);

            if (indexOfTopicGroup === -1)
            {
                // Topic group inactive - add
                activeTopicGroups.push(topicGroupIndex);
            } else
            {
                // Topic group active - remove
                activeTopicGroups.splice(indexOfTopicGroup, 1);
            }
        } else
        {
            // Cookie does not exist - add sole topic group
            activeTopicGroups = [topicGroupIndex];
        }

        $.cookie(cookieName, activeTopicGroups.join(separator), { path: '/' });
    };

    /**
    * Initialise basic token inputs.
    */

    module.initialiseTokenfield = function ()
    {
        $('.tokenfield-input').tokenfield({ createTokensOnBlur: true });
    };

    /**
    * Initialise dropdowns used to filter displayed items by their active state.
    */
    module.initialiseActiveStateDropdowns = function ()
    {
        $('.active-state-dropdown').change(function ()
        {
            var retired = $(this).val();
            var queryParam = 'retired';  // Default
            var customQueryParam = $(this).data('param');

            if (customQueryParam)
            {
                queryParam = customQueryParam;
            }

            var url = module.updateQueryStringParameter(window.location.href,
                queryParam, retired);
            url = module.updateQueryStringParameter(url, 'pageNo', null);
            window.location.href = url;
        });
    };

    /**
    * Register event handler that alerts the user when they attempt to
    * leave a page after making changes to a form and not saving.
    */
    module.initialiseChangesMadeAlert = function ()
    {
        var changesMade = false;

        $('body').on('change', '.form-horizontal :input, template-item-selector, .pro-link-form :input', function ()
        {
            // Quick fix to prevent confirm message on package transfer form
            if (document.URL.indexOf("PackageTransfer") !== -1)
            {
                return;
            }

            changesMade = true;
        });

		//Fix AC 89
        $('#relationship-confirmation').click(function ()
        {
        	$(this).prop('disabled', true);
        	$(this).closest('form').trigger('submit');
        });


        $('body').on('click', '[type=submit],[type=button],[type=reset]', function ()
        {
        	changesMade = false;
        });

        //Fix AC 38
        $("#saveTemplateChanges").click(function () {
            changesMade = false;
            ignore_onbeforeunload = true;
        });



        var ignore_onbeforeunload = false;
        $('a[href^=mailto], .download').on('click', function ()
        {
            ignore_onbeforeunload = true;
        });
        if (window.location.href.indexOf("Relationship") > -1)
        {
            ignore_onbeforeunload = true;
        }

        window.onbeforeunload = function (e)
        {
            //debugger;
            if (!ignore_onbeforeunload)
            {
                if (changesMade)
                {
                    // If we haven't been passed the event get the window.event
                    e = e || window.event;

                    var message = 'Are you sure you want to navigate away from this page? Any changes you made will be lost if you do not save.';

                    // For IE6-8 and Firefox prior to version 4
                    if (e)
                    {
                        e.returnValue = message;
                    }
                    e.returnValue = 'Are you sure?';
                    // For Chrome, Safari, IE8+ and Opera 12+
                    return message;
                } else
                {
                    setTimeout(function ()
                    {
                        $('#loading-animation').show();
                    }, 1000);
                }
            }
            ignore_onbeforeunload = false;
        };


        $('html').keyup(function (e)
        {
            if (e.keyCode == 8)
            {
                window.onbeforeunload;
            }
        });
    };

    /**
    * Initialise elements that show or hide other elements
    * when they are checked/unchecked.
    */
    module.initialiseShowHideControls = function ()
    {
        $('[data-show-element],[data-hide-element]').each(function ()
        {
            var showElement = $(this).data('show-element');
            var hideElement = $(this).data('hide-element');

            $(this).change(function ()
            {
                if ($(this).is(':checked'))
                {
                    $(hideElement).addClass('hidden');
                    $(showElement).removeClass('hidden');
                } else
                {
                    $(hideElement).removeClass('hidden');
                    $(showElement).addClass('hidden');
                }
            });
        });
    };

    /**
    * Initialise links for non-anchor elements.
    */
    module.initialiseSoftLinks = function ()
    {
        $('[data-soft-link]').each(function ()
        {
            var url = $(this).data('soft-link');
            $(this).css('cursor', 'pointer');

            $(this).click(function (e)
            {
                if (!$(e.target).closest('a').length)
                {
                    window.location = url;
                }
            });
        });
    };

    /**
    * Initialise links that require confirmation before
    * proceeding.
    */
    module.initialiseConfirmLinks = function ()
    {
        $('[data-confirm-link]').each(function ()
        {
            var message = $(this).data('confirm-link');

            $(this).click(function ()
            {
                if (confirm(message))
                {
                    return true;
                } else
                {
                    var returnUrl = $(this).data('return-url');

                    if (returnUrl)
                    {
                        window.location = returnUrl;
                    }

                    return false;
                }
            });
        });
    };

    /**
    * Initialise elements that enable or disable other elements
    * when they are checked/unchecked.
    */
    module.initialiseEnableDisableControls = function ()
    {
        $('[data-enable]').each(function ()
        {
            var enableSelectors = $(this).data('enable');
            var disableSelectors = $(this).data('disable');

            $(this).change(function ()
            {
                if ($(this).is(':checked'))
                {
                    $(enableSelectors).attr('disabled', false);
                    $(disableSelectors).attr('disabled', true);
                } else
                {
                    $(enableSelectors).attr('disabled', true);
                    $(disableSelectors).attr('disabled', false);
                }
            });
        });
    };

    /**
    * Initialise all components.
    */
    $(function ()
    {
        module.initialiseTypeaheads();
        module.initialiseSearchTagTypeaheads();
        module.initialiseToggleStates();
        module.initialisePackageFilter();
        module.initialiseGoToButtons();
        module.initialiseGetDirectionsButtons();
        module.initialiseComponents();
        module.initialisePopovers();
        module.initialiseSnapDrawer();
        module.initialisePanelHeadings();
        module.initialiseOnChangeSubmits();
        module.initialiseTokenfield();
        module.initialiseActiveStateDropdowns();
        module.initialiseChangesMadeAlert();
        module.initialiseShowHideControls();
        module.initialiseSoftLinks();
        module.initialiseConfirmLinks();
        module.initialiseEnableDisableControls();


        //Fix AC-48
        //$('textarea').elastic();
        $('#container').elastic().trigger('blur');

        $('.collapse').on('hide.bs.collapse', function ()
        {
            var panel = $(this).closest('.panel');
            panel.removeClass('expanded').addClass('collapsed');

            var panelHeading = $('.panel-heading', panel);
            $('.glyphicon-triangle-bottom', panelHeading).removeClass('glyphicon-triangle-bottom').addClass('glyphicon-triangle-right');
        });

        $('.collapse').on('show.bs.collapse', function ()
        {
            var panel = $(this).closest('.panel');
            panel.removeClass('collapsed').addClass('expanded');

            var panelHeading = $('.panel-heading', panel);
            $('.glyphicon-triangle-right', panelHeading).removeClass('glyphicon-triangle-right').addClass('glyphicon-triangle-bottom');
        });

        // Automatically expand accordion section
        var hash = window.location.hash;

        if (hash)
        {
            var toggle = $('[data-target=' + hash + ']');

            if (toggle.length)
            {
                $(hash).collapse('show');
            }
        }

        if ($("#refresh").val() == 'yes')
        {
            var editUrl = $.cookie('editUrl');
            if (editUrl)
            {
                $.removeCookie('editUrl');
                window.location.replace(window.location.protocol + "//" + window.location.host + editUrl);
            } else
            {
                window.location.reload(true);
            }
        } else
        {
            $('#refresh').val('yes');
        }
    });

    return module;

}(ACTIVATE || {}));

/**
* Momentum+ Activate AngularJS module.
*/
angular.module('momentumPlus.activate', ['ui.bootstrap', 'angular-horizontal-timeline']);