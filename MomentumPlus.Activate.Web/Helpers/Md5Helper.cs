﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace MomentumPlus.Activate.Web.Helpers
{
    public class Md5Helper
    {
        public static string GetHash(byte[] byteArray)
        {
            string hash;

            using (SHA1CryptoServiceProvider sha1 = new SHA1CryptoServiceProvider())
            {
                hash = Convert.ToBase64String(sha1.ComputeHash(byteArray));
            }

            return hash;
        }

        public static string GetHash(string stringValue)
        {
            byte[] byteArray 
                = new UTF8Encoding().GetBytes(stringValue);

            return GetHash(byteArray);
        }
    }
}