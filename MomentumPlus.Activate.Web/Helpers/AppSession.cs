﻿using MomentumPlus.Core.Models.People;
using System;
using System.Web;

namespace MomentumPlus.Activate.Web.Helpers
{
    public class AppSession
    {
        #region Current package properties

        /// <summary>
        /// ID of the currently selected package.
        /// </summary>
        public Guid? CurrentPackageId { get; set; }

        /// <summary>
        /// ID of the package being examined.
        /// </summary>
        public Guid? ExaminePackageId { get; set; }

        #endregion

        #region Logged in user properties

        /// <summary>
        /// Current logged in User
        /// </summary>
        public User CurrentUser { get; set; }

        private User _viewAsUser;
        public User ViewAsUser
        {
            get { return _viewAsUser ?? CurrentUser; }
            set { _viewAsUser = value; }
        }

        public bool UserIsHRManager { get; set; }
        public bool CurrentPackageHasAncestor { get; set; }

        public bool ExperiencesEnabled { get; set; }
        public bool RelationshipsEnabled { get; set; }
        public bool ProjectsEnabled { get; set; }
        public bool PeopleManagementEnabled { get; set; }
        public bool ExitSurveyEnabled { get; set; }
        
        public Guid ExitSurveyId { get; set; }
        #endregion

        #region Return URLs

        private string defaultUrl = "/";

        private string _exitSurveyQuestionFromReturnUrl;
        public string ExitSurveyQuestionFromReturnUrl
        {
            get { return string.IsNullOrEmpty(_exitSurveyQuestionFromReturnUrl) ? defaultUrl : _exitSurveyQuestionFromReturnUrl; }
            set { _exitSurveyQuestionFromReturnUrl = value; }
        }

        private string _packageFormReturnUrl;
        public string PackageFormReturnUrl
        {
            get { return string.IsNullOrEmpty(_packageFormReturnUrl) ? defaultUrl : _packageFormReturnUrl; }
            set { _packageFormReturnUrl = value; }
        }

        private string _triggerFormReturnUrl;
        public string TriggerFormReturnUrl
        {
            get { return string.IsNullOrEmpty(_triggerFormReturnUrl) ? defaultUrl : _triggerFormReturnUrl; }
            set { _triggerFormReturnUrl = value; }
        }

        private string _employeeFormReturnUrl;
        public string EmployeeFormReturnUrl
        {
            get { return string.IsNullOrEmpty(_employeeFormReturnUrl) ? defaultUrl : _employeeFormReturnUrl; }
            set { _employeeFormReturnUrl = value; }
        }

        private string _changePasswordFormReturnUrl;
        public string ChangePasswordFormReturnUrl
        {
            get { return string.IsNullOrEmpty(_changePasswordFormReturnUrl) ? defaultUrl : _changePasswordFormReturnUrl; }
            set { _changePasswordFormReturnUrl = value; }
        }

        private string _positionFormReturnUrl;
        public string PositionFormReturnUrl
        {
            get { return string.IsNullOrEmpty(_positionFormReturnUrl) ? defaultUrl : _positionFormReturnUrl; }
            set { _positionFormReturnUrl = value; }
        }

        private string _workplaceFormReturnUrl;
        public string WorkplaceFormReturnUrl
        {
            get { return string.IsNullOrEmpty(_workplaceFormReturnUrl) ? defaultUrl : _workplaceFormReturnUrl; }
            set { _workplaceFormReturnUrl = value; }
        }

        private string _topicFormReturnUrl;
        public string TopicFormReturnUrl
        {
            get { return string.IsNullOrEmpty(_topicFormReturnUrl) ? defaultUrl : _topicFormReturnUrl; }
            set { _topicFormReturnUrl = value; }
        }

        private string _taskManagerReturnUrl;
        public string TaskManagerReturnUrl
        {
            get { return string.IsNullOrEmpty(_taskManagerReturnUrl) ? defaultUrl : _taskManagerReturnUrl; }
            set { _taskManagerReturnUrl = value; }
        }

        private string _taskFormReturnUrl;
        public string TaskFormReturnUrl
        {
            get { return string.IsNullOrEmpty(_taskFormReturnUrl) ? defaultUrl : _taskFormReturnUrl; }
            set { _taskFormReturnUrl = value; }
        }

        private string _documentLibraryReturnUrl;
        public string DocumentLibraryReturnUrl
        {
            get { return string.IsNullOrEmpty(_documentLibraryReturnUrl) ? defaultUrl : _documentLibraryReturnUrl; }
            set { _documentLibraryReturnUrl = value; }
        }

        private string _documentLibraryFormReturnUrl;
        public string DocumentLibraryFormReturnUrl
        {
            get { return string.IsNullOrEmpty(_documentLibraryFormReturnUrl) ? defaultUrl : _documentLibraryFormReturnUrl; }
            set { _documentLibraryFormReturnUrl = value; }
        }

        private string _attachmentManagerReturnUrl;
        public string AttachmentManagerReturnUrl
        {
            get { return string.IsNullOrEmpty(_attachmentManagerReturnUrl) ? defaultUrl : _attachmentManagerReturnUrl; }
            set { _attachmentManagerReturnUrl = value; }
        }

        private string _attachmentFormReturnUrl;
        public string AttachmentFormReturnUrl
        {
            get { return string.IsNullOrEmpty(_attachmentFormReturnUrl) ? defaultUrl : _attachmentFormReturnUrl; }
            set { _attachmentFormReturnUrl = value; }
        }

        private string _voiceMessageManagerReturnUrl;
        public string VoiceMessageManagerReturnUrl
        {
            get { return string.IsNullOrEmpty(_voiceMessageManagerReturnUrl) ? defaultUrl : _voiceMessageManagerReturnUrl; }
            set { _voiceMessageManagerReturnUrl = value; }
        }

        private string _voiceMessageFormReturnUrl;
        public string VoiceMessageFormReturnUrl
        {
            get { return string.IsNullOrEmpty(_voiceMessageFormReturnUrl) ? defaultUrl : _voiceMessageFormReturnUrl; }
            set { _voiceMessageFormReturnUrl = value; }
        }

        private string _topicGroupFormReturnUrl;
        public string TopicGroupFormReturnUrl
        {
            get { return string.IsNullOrEmpty(_topicGroupFormReturnUrl) ? defaultUrl : _topicGroupFormReturnUrl; }
            set { _topicGroupFormReturnUrl = value; }
        }

        private string _templateFormReturnUrl;
        public string TemplateFormReturnUrl
        {
            get { return string.IsNullOrEmpty(_templateFormReturnUrl) ? defaultUrl : _templateFormReturnUrl; }
            set { _templateFormReturnUrl = value; }
        }

        private string _masterListFormReturnUrl;
        public string MasterListFormReturnUrl
        {
            get { return string.IsNullOrEmpty(_masterListFormReturnUrl) ? defaultUrl : _masterListFormReturnUrl; }
            set { _masterListFormReturnUrl = value; }
        }

        private string _meetingFormReturnUrl;
        public string MeetingFormReturnUrl
        {
            get { return string.IsNullOrEmpty(_meetingFormReturnUrl) ? defaultUrl : _meetingFormReturnUrl; }
            set { _meetingFormReturnUrl = value; }
        }

        private string _contactFormReturnUrl;
        public string ContactFormReturnUrl
        {
            get { return string.IsNullOrEmpty(_contactFormReturnUrl) ? defaultUrl : _contactFormReturnUrl; }
            set { _contactFormReturnUrl = value; }
        }

        private string _packageExamineReturnUrl;
        public string PackageExamineReturnUrl
        {
            get { return string.IsNullOrEmpty(_packageExamineReturnUrl) ? defaultUrl : _packageExamineReturnUrl; }
            set { _packageExamineReturnUrl = value; }
        }

        private string _packageTransferFormReturnUrl;
        public string PackageTransferFormReturnUrl
        {
            get { return string.IsNullOrEmpty(_packageTransferFormReturnUrl) ? defaultUrl : _packageTransferFormReturnUrl; }
            set { _packageTransferFormReturnUrl = value; }
        }

        private string _documentReportReturnUrl;
        public string DocumentReportReturnUrl
        {
            get { return string.IsNullOrEmpty(_documentReportReturnUrl) ? defaultUrl : _documentReportReturnUrl; }
            set { _documentReportReturnUrl = value; }
        }

        private string _projectFormReturnUrl;
        public string ProjectFormReturnUrl
        {
            get { return string.IsNullOrEmpty(_projectFormReturnUrl) ? defaultUrl : _projectFormReturnUrl; }
            set { _projectFormReturnUrl = value; }
        }

        private string _quickAddProjectFormReturnUrl;
        public string QuickAddProjectFormReturnUrl
        {
            get { return string.IsNullOrEmpty(_quickAddProjectFormReturnUrl) ? defaultUrl : _quickAddProjectFormReturnUrl; }
            set { _quickAddProjectFormReturnUrl = value; }
        }

        private string _projectInstanceFormReturnUrl;
        public string ProjectInstanceFormReturnUrl
        {
            get { return string.IsNullOrEmpty(_projectInstanceFormReturnUrl) ? defaultUrl : _projectInstanceFormReturnUrl; }
            set { _projectInstanceFormReturnUrl = value; }
        }

        private string _projectTransferFormReturnUrl;
        public string ProjectTransferFormReturnUrl
        {
            get { return string.IsNullOrEmpty(_projectTransferFormReturnUrl) ? defaultUrl : _projectTransferFormReturnUrl; }
            set { _projectTransferFormReturnUrl = value; }
        }

        private string _cancelRequestReturnUrl;
        public string CancelRequestReturnUrl
        {
            get { return string.IsNullOrEmpty(_cancelRequestReturnUrl) ? defaultUrl : _cancelRequestReturnUrl; }
            set { _cancelRequestReturnUrl = value; }
        }

        private string _projectInstanceRoleChangeFormReturnUrl;
        public string ProjectInstanceRoleChangeFormReturnUrl
        {
            get { return string.IsNullOrEmpty(_projectInstanceRoleChangeFormReturnUrl) ? defaultUrl : _projectInstanceRoleChangeFormReturnUrl; }
            set { _projectInstanceRoleChangeFormReturnUrl = value; }
        }

        private string _staffFormReturnUrl;
        public string StaffFormReturnUrl
        {
            get { return string.IsNullOrEmpty(_staffFormReturnUrl) ? defaultUrl : _staffFormReturnUrl; }
            set { _staffFormReturnUrl = value; }
        }

        private string _quickAddPersonFormReturnUrl;
        public string QuickAddPersonFormReturnUrl
        {
            get { return string.IsNullOrEmpty(_quickAddPersonFormReturnUrl) ? defaultUrl : _quickAddPersonFormReturnUrl; }
            set { _quickAddPersonFormReturnUrl = value; }
        }

        private string _staffTransferFormReturnUrl;
        public string StaffTransferFormReturnUrl
        {
            get { return string.IsNullOrEmpty(_staffTransferFormReturnUrl) ? defaultUrl : _staffTransferFormReturnUrl; }
            set { _staffTransferFormReturnUrl = value; }
        }

        private string _functionFormReturnUrl;
        public string FunctionFormReturnUrl
        {
            get { return string.IsNullOrEmpty(_functionFormReturnUrl) ? defaultUrl : _functionFormReturnUrl; }
            set { _functionFormReturnUrl = value; }
        }

        private string _processGroupFormReturnUrl;
        public string ProcessGroupFormReturnUrl
        {
            get { return string.IsNullOrEmpty(_processGroupFormReturnUrl) ? defaultUrl : _processGroupFormReturnUrl; }
            set { _processGroupFormReturnUrl = value; }
        }

        private string _quickAddRelationshipFormReturnUrl;
        public string QuickAddRelationshipFormReturnUrl
        {
            get { return string.IsNullOrEmpty(_quickAddRelationshipFormReturnUrl) ? defaultUrl : _quickAddRelationshipFormReturnUrl; }
            set { _quickAddRelationshipFormReturnUrl = value; }
        }

        private string _handoverFormReturnUrl;
        public string HandoverFormReturnUrl
        {
            get { return string.IsNullOrEmpty(_handoverFormReturnUrl) ? defaultUrl : _handoverFormReturnUrl; }
            set { _handoverFormReturnUrl = value; }
        }

        private string _prioritiesToolsReturnUrl;
        public string PrioritiesToolsReturnUrl
        {
            get { return string.IsNullOrEmpty(_prioritiesToolsReturnUrl) ? defaultUrl : _prioritiesToolsReturnUrl; }
            set { _prioritiesToolsReturnUrl = value; }
        }

        #endregion

        #region FormViewModel

        public object FormViewModel { get; set; }

        #endregion

        #region Debug Tools

        public bool EnableTimeManipulationTool { get; set; }

        #endregion

        /// <summary>
        /// Get the current session.
        /// </summary>
        public static AppSession Current
        {
            get
            {
                AppSession session = (AppSession)HttpContext.Current.Session["MomentumPlusActivateSession"];
                if (session == null)
                {
                    session = new AppSession();
                    HttpContext.Current.Session["MomentumPlusActivateSession"] = session;
                }
                return session;
            }
        }
    }
}