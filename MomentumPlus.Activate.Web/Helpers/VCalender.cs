﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MomentumPlus.Activate.Web.Helpers
{
    public class VCalander
    {
        #region constructors

        public VCalander(DateTime startDate, DateTime endDate, string organiser,
            string location, string summary, string description, string category)
        {
            this.startDate = startDate;
            this.endDate = endDate;
            this.organiser = organiser;
            this.location = location;
            this.summary = summary;
            this.description = description;
            this.category = category;
        }

        #endregion

        #region implicit operators

        public static implicit operator VCalander(Meeting meeting)
        {
            VCalander vCalander = null;
            vCalander =
                new VCalander(
                    meeting.Start.Value,
                    meeting.End.Value,
                    meeting.ParentContact.Name,
                    meeting.Location,
                    meeting.Name,
                    meeting.Notes,
                    meeting.MeetingType.Title);


            return vCalander;
        }

        #endregion

        #region private properties

        private readonly string description;
        private readonly string location;
        private readonly string organiser;
        private readonly string summary;
        private DateTime endDate;
        private DateTime startDate;
        private readonly string category;

        #endregion

        #region private methods

        private string DateFormat
        {
            get { return "yyyyMMddTHHmmssZ"; }
        }

        private string GetMeetingBody()
        {
            var content =
                new StringBuilder();

            content.AppendLine("BEGIN:VEVENT");
            content.AppendLine("CATEGORIES:" + category);
            content.AppendLine("ORGANIZER:MAILTO:" + organiser);
            content.AppendLine("DTSTART:" + startDate.ToUniversalTime().ToString(DateFormat));
            content.AppendLine("DTEND:" + endDate.ToUniversalTime().ToString(DateFormat));
            content.AppendLine("LOCATION:" + location);
            content.AppendLine("SUMMARY:" + summary);
            content.AppendLine("DESCRIPTION:" + description);
            content.AppendLine("CLASS:PRIVATE");
            content.AppendLine("END:VEVENT");

            return content.ToString();
        }

        #endregion

        #region public methods

        public static string GetMeeting(DateTime startDate, DateTime endDate, string organsier, string location,
            string summary, string description, string category)
        {
            var vCalander =
                new VCalander(startDate, endDate, organsier, location, summary, description, category);

            var content =
                GetMeetings(new List<VCalander> {vCalander});

            return content;
        }

        public static string GetMeeting(VCalander vCalender)
        {
            var data =
                GetMeetings(new List<VCalander> {vCalender});

            return data;
        }

        public static string GetMeetings(List<VCalander> vCalenders)
        {
            var content =
                new StringBuilder();

            content.AppendLine("BEGIN:VCALENDAR");
            content.AppendLine("VERSION:1.0");

            foreach (var vCalander in vCalenders)
                content.Append(vCalander.GetMeetingBody());

            content.AppendLine("END:VCALENDAR");

            return content.ToString();
        }

        #endregion
    }
}