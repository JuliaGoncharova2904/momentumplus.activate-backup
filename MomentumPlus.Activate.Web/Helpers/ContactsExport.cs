﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using MomentumPlus.Activate.Web.Models;
using MvcPaging;

namespace MomentumPlus.Activate.Web.Helpers
{
    public class ContactsExport
    {
        private const string Header = "\"Title\",\"First Name\",\"Middle Name\",\"Last Name\",\"Suffix\",\"Company\",\"Department\",\"Job Title\",\"Business Street\",\"Business Street 2\",\"Business Street 3\",\"Business City\",\"Business State\",\"Business Postal Code\",\"Business Country/Region\",\"Home Street\",\"Home Street 2\",\"Home Street 3\",\"Home City\",\"Home State\",\"Home Postal Code\",\"Home Country/Region\",\"Other Street\",\"Other Street 2\",\"Other Street 3\",\"Other City\",\"Other State\",\"Other Postal Code\",\"Other Country/Region\",\"Assistant's Phone\",\"Business Fax\",\"Business Phone\",\"Business Phone 2\",\"Callback\",\"Car Phone\",\"Company Main Phone\",\"Home Fax\",\"Home Phone\",\"Home Phone 2\",\"ISDN\",\"Mobile Phone\",\"Other Fax\",\"Other Phone\",\"Pager\",\"Primary Phone\",\"Radio Phone\",\"TTY/TDD Phone\",\"Telex\",\"Account\",\"Anniversary\",\"Assistant's Name\",\"Billing Information\",\"Birthday\",\"Business Address PO Box\",\"Categories\",\"Children\",\"Directory Server\",\"E-mail Address\",\"E-mail Type\",\"E-mail Display Name\",\"E-mail 2 Address\",\"E-mail 2 Type\",\"E-mail 2 Display Name\",\"E-mail 3 Address\",\"E-mail 3 Type\",\"E-mail 3 Display Name\",\"Gender\",\"Government ID Number\",\"Hobby\",\"Home Address PO Box\",\"Initials\",\"Internet Free Busy\",\"Keywords\",\"Language\",\"Location\",\"Manager's Name\",\"Mileage\",\"Notes\",\"Office Location\",\"Organizational ID Number\",\"Other Address PO Box\",\"Priority\",\"Private\",\"Profession\",\"Referred By\",\"Sensitivity\",\"Spouse\",\"User 1\",\"User 2\",\"User 3\",\"User 4\",\"Web Page\"\r\n";

        public static string ExportForOutlook(IEnumerable<ContactViewModel> contacts)
        {
            var content = new StringBuilder();

            content.Append(Header);

            foreach (var contact in contacts)
            {
                content.Append(OutlookContactExport(contact));
            }
            
            return content.ToString();
        }

        public static string OutlookContactExport(ContactViewModel contact)
        {
            var company = contact.Person.OtherCompany ? contact.Person.OtherCompanyName : contact.Person.Companies.FirstOrDefault(c => c.Value == contact.Person.CompanyId.ToString()).Text;
            return String.Format("\"{0}\",\"{1}\",\"\",\"{2}\",\"\",\"{3}\",\"\",\"{4}\",,,,,,,,,,,,,,,,,,,,,,,,\"{5}\",,,,,,,,,\"{6}\",,,,,,,,\"\",\"0/0/00\",,,\"0/0/00\",,,,,\"{7}\",\"SMTP\",\"{1} {2} ({7})\",,,,,,,\"Unspecified\",,,,\"{1}.{2}.\",,\"\",\"\",\"\",,,\"\",,,,\"Normal\",\"False\",,,\"Normal\"\r\n", 
                contact.Person.Title, 
                contact.Person.FirstName,
                contact.Person.LastName,
                company,
                contact.Person.Position,
                contact.Person.Phone,
                contact.Person.Mobile,
                contact.Person.Email);
        }

    }
}