﻿using MvcSiteMapProvider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using MomentumPlus.Activate.Services.Interfaces;

namespace MomentumPlus.Activate.Web.Helpers
{
    public class MomentumSiteMapNodeVisibilityProvider : FilteredSiteMapNodeVisibilityProvider
    {

        public override bool IsVisible(ISiteMapNode node, IDictionary<string, object> sourceMetadata)
        {
            var handoverService = DependencyResolver.Current.GetService<IHandoverService>();
           
            bool result = base.IsVisible(node, sourceMetadata);
 
            IPrincipal user = null;

            var httpContext = HttpContext.Current;
            if (httpContext != null)
                user = httpContext.User;
            
            if (result)
            {
                if (node.Area == "Home")
                {
                    switch (node.Controller)
                    {
                        case "Experiences":
                            result = AppSession.Current.ExperiencesEnabled;
                            break;
                        case "Relationships":
                            result = AppSession.Current.RelationshipsEnabled;
                            break;
                        case "Projects":
                            result = AppSession.Current.ProjectsEnabled;
                            break;
                        case "My Team":
                            result = AppSession.Current.PeopleManagementEnabled;
                            break;
                        case "Onboarding":
                            result = AppSession.Current.CurrentPackageHasAncestor;
                            break;
                        case "ExitSurvey":
                            result = AppSession.Current.ExitSurveyEnabled;
                            break;
                    }
                }
                else if (node.Area == "Framework")
                {
                    switch (node.Controller)
                    {
                        case "Projects":
                            result = false;
                            break;
                        case "ProLink":
                            result = AccountConfig.Settings.ProLinkEnabled
                                     && user != null
                                     && (user.IsInRole(RoleAuthHelper.iHandoverAgent)
                                         || user.IsInRole(RoleAuthHelper.iHandoverAdmin));
                            break;
                    }
                }
                else if (node.Controller == "DocumentLibrary")
                {
                    result = user != null
                             && (user.IsInRole(RoleAuthHelper.Administrator)
                                 || user.IsInRole(RoleAuthHelper.iHandoverAdmin)
                                 || user.IsInRole(RoleAuthHelper.iHandoverAgent));
                }
                else if (node.Controller == "Reports")
                {
                    
                    bool isLeaver = false;

                    var currentPackageId = AppSession.Current.CurrentPackageId;
                    if (currentPackageId != null)
                        isLeaver = handoverService.IsLeaving(currentPackageId.Value);

                    switch (node.Action)
                    {
                        case "ProgressReports":
                            result = !isLeaver || (user != null && !user.IsInRole(RoleAuthHelper.User));
                            break;
                        case "RAGReports":
                            result = isLeaver || (user != null && !user.IsInRole(RoleAuthHelper.User));
                            break;
                    }
                } else if (node.Title.Equals("Exit Survey") && user.IsInRole(RoleAuthHelper.Administrator) || !user.IsInRole(RoleAuthHelper.iHandoverAdmin) || !user.IsInRole(RoleAuthHelper.HRManager))
                {
                    result = true;
                }
                if (node.Title.Equals("My Pack") && result &&
                    (
                        user.IsInRole(RoleAuthHelper.iHandoverAdmin) ||
                        user.IsInRole(RoleAuthHelper.Administrator) ||
                        user.IsInRole(RoleAuthHelper.LineManager))
                    )
                {
                    var userService = DependencyResolver.Current.GetService<IUserService>();
                    var packageService = DependencyResolver.Current.GetService<IPackageService>();
                    var oUser = userService.GetByUsername(user.Identity.Name);
                    var packages = packageService.GetByUserOwnerId(oUser.ID);
                    if (!packages.Where(p => !p.Deleted.HasValue).Any())
                    {
                        result = false;
                    }
                }
                
            }

            return result;
        }

    }
}