﻿namespace MomentumPlus.Activate.Web.Helpers
{
    public struct FileMimeTypes
    {
        public string FileType { get; set; }
        public string FileMimeType { get; set; }
    }
}