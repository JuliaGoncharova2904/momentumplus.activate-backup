﻿using System.Text;
using System.Web;
using System.Web.Mvc;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Helpers
{
    public class TaskFlagging
    {
        public static HtmlString GetTaskFlag(TaskViewModel task)
        {
            var html = new StringBuilder();
            var div = new TagBuilder("div");
            div.Attributes.Add("class", "pull-right");
            if (task.ParentTopic != null && task.ParentTopic.ProcessGroupId != null)
            {
                div.InnerHtml += ProLink().ToString();
            }
            if (task.IsCore)
            {
                div.InnerHtml += Core().ToString();
            }
            if (task.Deleted.HasValue)
            {
                div.InnerHtml += Retired().ToString();
            }
            html.Append(div);
            return new HtmlString(html.ToString());
        }

        public static HtmlString GetTaskFlag(Task task)
        {
            var html = new StringBuilder();
            var div = new TagBuilder("div");
            div.Attributes.Add("class", "pull-right");
            if (task.ParentTopic != null && task.ParentTopic.ProcessGroupId != null)
            {
                div.InnerHtml += ProLink().ToString();
            }
            if (task.IsCore)
            {
                div.InnerHtml += Core().ToString();
            }
            if (task.Deleted.HasValue)
            {
                div.InnerHtml += Retired().ToString();
            }
            html.Append(div);
            return new HtmlString(html.ToString());
        }

        public static HtmlString GetTopicProLinkFlag(Topic topic)
        {
            var html = new StringBuilder();
            var div = new TagBuilder("div");
            div.Attributes.Add("class", "pull-right");
            if (topic != null && topic.ProcessGroupId != null)
            {
                div.InnerHtml += ProLink().ToString();
            }
            html.Append(div);
            return new HtmlString(html.ToString());
        }

        public static HtmlString GetTaskProLinkFlag(Task task)
        {
            var html = new StringBuilder();
            var div = new TagBuilder("div");
            div.Attributes.Add("class", "pull-right");
            if (task.ParentTopic != null && task.ParentTopic.ProcessGroupId != null)
            {
                div.InnerHtml += ProLink().ToString();
            }
            html.Append(div);
            return new HtmlString(html.ToString());
        }

        private static TagBuilder ProLink()
        {
            var span = new TagBuilder("span");
            span.Attributes.Add("style", "clear:both");
            span.Attributes.Add("class", "label label-default pull-right badging");
            span.SetInnerText("Pro-Link");
            return span;
        }

        private static TagBuilder Core()
        {
            var span = new TagBuilder("span");
            span.Attributes.Add("style" ,"clear:both");
            span.Attributes.Add("class", "label label-green pull-right badging");
            span.SetInnerText("Core");
            return span;
        }

        private static TagBuilder Retired()
        {
            var span = new TagBuilder("span");
            span.Attributes.Add("style", "clear:both");
            span.Attributes.Add("class", "label label-danger pull-right badging");
            span.SetInnerText("Retired");
            return span;
        }
    }
}