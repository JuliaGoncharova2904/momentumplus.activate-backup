﻿using System;
using System.Net.Mail;
using System.Text;
using Nustache.Core;
using MomentumPlus.Activate.Web.Extensions;
using MomentumPlus.Core.Models.People;
using MomentumPlus.Core.Models.Handover;

namespace MomentumPlus.Activate.Web.Helpers
{
    public class EmailHelper
    {
        public static string AgentId = "5hP7xe+OVEk=";
        public static string AppId = "VZuQJVce9G0=";

        public static void SendPasswordResetMessage(User user, string url)
        {
            var body = RenderTemplate("PasswordReset", new { name = user.FirstName + " " + user.LastName, url = url });
            Send("Activate Password Reset", body, user.Email);
        }

        public static bool SendKITSLeaverMessage(Handover handover, string url)
        {
            var phone = "";

            if (handover.Package.UserOwner.Phone != null)
                phone = String.Format("or call on {0}", handover.Package.UserOwner.Phone.ToString());

            var body = RenderTemplate("KITSLeaver", new
            {
                name = handover.Package.UserOwner.FullName,
                package = handover.Package.DisplayName,
                url = String.Format("{0}/PackageSelector/ExaminePackage/{1}", url, handover.Package.ID),
                date = handover.LeavingPeriod.LastDay.Value.ToShortDateString(),
                leaving = handover.LeavingPeriod.NoticePeriod.GetDescription(),
                email = handover.Package.UserOwner.Username,
                phone = phone
            });

            try { 
                Send("Handover Support Requested for Leaver", body, AccountConfig.Settings.HandoverConsultant);
                return true;
            }
            catch (Exception ex) {
                return false;
            }
        }

        public static bool SendKITSOnboardMessage(Handover handover, string url)
        {
            var phone = "";

            if (handover.OnboardingPeriod.Package.UserOwner.Phone != null)
                phone = String.Format("or call on {0}", handover.OnboardingPeriod.Package.UserOwner.Phone.ToString());

            var body = RenderTemplate("KITSOnboarder", new
            {
                name = handover.OnboardingPeriod.Package.UserOwner.FullName,
                package = handover.OnboardingPeriod.Package.DisplayName,
                url = String.Format("{0}/PackageSelector/ExaminePackage/{1}", url, handover.Package.ID),
                date = handover.OnboardingPeriod.StartPoint.Value.ToShortDateString(),
                onboard = String.Format("{0} weeks", handover.OnboardingPeriod.Duration.ToString()),
                email = handover.OnboardingPeriod.Package.UserOwner.Username,
                phone = phone
            });

            try
            {
                Send("Handover Support Requested for Onboarder", body, AccountConfig.Settings.HandoverConsultant);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static void SendAppRecycleEmail(int Attempt, string URL, string extraMessage = "")
        {
            string EmailTitle = "Momentum Plus Recycle Report";


            StringBuilder body = new StringBuilder();

            if (Attempt == 0)
            {
                body.Append("Momentum Plus' Application pool is being recycled at " + DateTime.Now.ToString());
                body.Append("\n\nApplication url is: " + URL);
            }
            else if (Attempt == 1)
            {
                body.Append("Momentum Plus' Application pool has been recycled at " + DateTime.Now.ToString());
                body.Append("\n\nApplication url is: " + URL);
            }
            else
            {
                body.Append("Momentum Plus' Application pool recycling was requested, but failed at " + DateTime.Now.ToString() + "\n\nDETAIL: " + extraMessage);
                body.Append("\n\nApplication url is: " + URL);
            }
        }

        public static void Send(string subject, string body, string recipient)
        {
            try
            {
                // Smtp settings are stored in the web.config file
                using (SmtpClient client = new SmtpClient())
                {
                    MailMessage message = new MailMessage();
                    message.To.Add(new MailAddress(recipient));
                    message.Subject = subject;
                    message.Body = body;
                    message.IsBodyHtml = true;
                    message.Priority = MailPriority.Normal;

                    client.Send(message);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(
                    string.Format("Error sending email, {0}", ex.Message), ex);
            }
        }

        private static string RenderTemplate(string templateName, object data)
        {
            var template = AppDomain.CurrentDomain.BaseDirectory + "EmailTemplates\\" + templateName + ".mustache";
            return Render.FileToString(template, data);
        }
    }
}