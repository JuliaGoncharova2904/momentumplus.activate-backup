﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Models;
using System.Web.Mvc;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Helpers
{
    public class AttachmentHelper
    {
        private IAttachmentService AttachmentService;

        public AttachmentHelper()
        {
            AttachmentService = DependencyResolver.Current.GetService<IAttachmentService>();
        }

        public Attachment GetAttachment(Guid id)
        {
            return AttachmentService.GetByID(id);
        }

        public AttachmentViewModel GetAttachmentViewModel(Guid id)
        {
            var attachment = GetAttachment(id);
            return Mapper.Map<AttachmentViewModel>(attachment);
        }

        public static List<string> AllowedFileTypes
        {
            get
            {
                if (Helpers.AccountConfig.Settings.FileExtensions != null)
                {
                    return Helpers.AccountConfig.Settings.FileExtensions.Split(',').Select(s => s.Trim()).ToList();
                }
                else
                {
                    return new List<string>();
                }
            }
        }
    }
}