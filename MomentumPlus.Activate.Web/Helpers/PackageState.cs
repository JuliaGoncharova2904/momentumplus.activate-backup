﻿using System;
using System.Web.Mvc;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Helpers
{
    public class PackageState
    {
        public static string GetState(Guid id)
        {
            var packageService = DependencyResolver.Current.GetService<IPackageService>();
            var handoverService = DependencyResolver.Current.GetService<IHandoverService>();
            var package = packageService.GetByTaskId(id) ?? packageService.GetByProjectInstanceId(id) ?? packageService.GetByContactId(id) ?? packageService.GetByTopicId(id);

            if (handoverService != null && package != null)
            {
                if (handoverService.IsOnboarding(package.ID))
                {
                    return Package.Status.New.ToString();
                }
                else if (handoverService.IsLeaving(package.ID) || handoverService.HasLeft(package.ID))
                {
                    return Package.Status.Triggered.ToString();
                }
                else
                {
                    return Package.Status.InProgress.ToString();
                }
            }
            else
            {
                return Package.Status.InProgress.ToString();
            }
        }
    }
}