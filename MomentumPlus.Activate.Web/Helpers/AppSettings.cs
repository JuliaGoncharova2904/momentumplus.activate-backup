﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace MomentumPlus.Activate.Web.Helpers
{
    public class AppSettings
    {
        public static IEnumerable<string> AudioFileTypes
        {
            get { return ConfigurationManager.AppSettings["AudioFileTypes"].Split(','); }
        }

        public static IEnumerable<string> DocumentFileTypes
        {
            get { return ConfigurationManager.AppSettings["DocumentFileTypes"].Split(','); }
        }

        public static IEnumerable<string> ImageFileTypes
        {
            get { return ConfigurationManager.AppSettings["ImageFileTypes"].Split(','); }
        }

        public static IEnumerable<int> OnboardDurations
        {
            get { return ConfigurationManager.AppSettings["OnboardDurations"].Split(',').Select(int.Parse); }
        }

        public static String iHandoverConsultant
        {
            get { return ConfigurationManager.AppSettings["iHandoverConsultant"]; }
        }

        public static int CacheHelperTimeoutMins
        {
            get { return int.Parse(ConfigurationManager.AppSettings["CacheHelperTimeoutMins"]); }
        }
    }
}