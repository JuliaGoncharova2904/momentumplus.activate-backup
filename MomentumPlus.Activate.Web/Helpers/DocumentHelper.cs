﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace MomentumPlus.Activate.Web.Helpers
{
    public class DocumentHelper
    {
        /// <summary>
        /// Accepts a mimetype and returns the file extension. 
        /// for instance the string:
        /// 
        /// Accepts
        /// application/msword
        /// 
        /// Returns 
        /// .Doc
        /// </summary>
        /// <param name="fileMimeType"></param>
        /// <returns></returns>
        public static string GetFileType(string fileMimeType)
        {
            string fileType = null;
            var _myAcceptedFileTypes = CreateFileMimeTypes();

            foreach (var _myFileType in _myAcceptedFileTypes)
            {
                if (_myFileType.FileMimeType.Equals(fileMimeType))
                {
                    fileType = _myFileType.FileType;
                }
            }

            if (!string.IsNullOrEmpty(fileType))
            {
                return fileType;
            }
            else
            {
                return null;
            }
        }

        public static bool ValidateMimeType(HttpPostedFileBase theFile)
        {
            byte[] buffer = new byte[512];
            try
            {
                System.IO.MemoryStream memoryStream = new MemoryStream();
                theFile.InputStream.CopyTo(memoryStream);

                byte[] m_bytes = ReadToEnd(memoryStream);

                string hex = BitConverter.ToString(m_bytes).Substring(0, 5);


                //http://en.wikipedia.org/wiki/Magic_number_(programming)
                // magic nummnbers for comparing against. Example below is a windows .exe
                var comparison = String.Compare(hex, "4D-5A");

                if (comparison == 0)
                {
                    //this is an exe
                    return false;
                }

            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }
        private static byte[] ReadToEnd(System.IO.Stream stream)
        {
            long originalPosition = 0;

            if (stream.CanSeek)
            {
                originalPosition = stream.Position;
                stream.Position = 0;
            }
            try
            {
                byte[] readBuffer = new byte[4096];
                int totalBytesRead = 0;
                int bytesRead;

                while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0)
                {
                    totalBytesRead += bytesRead;
                    if (totalBytesRead == readBuffer.Length)
                    {
                        int nextByte = stream.ReadByte();
                        if (nextByte != -1)
                        {
                            byte[] temp = new byte[readBuffer.Length * 2];
                            Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                            Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
                            readBuffer = temp;
                            totalBytesRead++;
                        }
                    }
                }
                byte[] buffer = readBuffer;
                if (readBuffer.Length != totalBytesRead)
                {
                    buffer = new byte[totalBytesRead];
                    Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
                }
                return buffer;
            }
            finally
            {
                if (stream.CanSeek)
                {
                    stream.Position = originalPosition;
                }
            }
        }


        public static string GetFileMimeType(string fileType)
        {
            string fileMimeType = null;
            var _acceptedFileTypes = CreateFileMimeTypes();
            foreach (var _myFileType in _acceptedFileTypes)
            {
                if (_myFileType.FileType.Equals(fileType))
                {
                    fileMimeType = _myFileType.FileMimeType;
                }
            }
            if (!string.IsNullOrEmpty(fileMimeType))
            {
                return fileMimeType;
            }
            else
            {
                return null;
            }
        }


        public static bool CheckValidFileByExtension(List<string> ValidExtensions, string fileTypeExtension)
        {
            bool success = false;
            //var _myAcceptedFileTypes = 

            return success;
        }

        /// <summary>
        /// checks if an uploaded file is valid for saving on the server
        /// </summary>
        /// <param name="fileType"></param>
        /// <returns></returns>
        public static bool CheckValidFileByExtension(string fileTypeExtension)
        {
            bool success = false;
            var _myAcceptedFileTypes = CreateFileMimeTypes();
            foreach (var filetype in _myAcceptedFileTypes)
            {
                if (filetype.FileMimeType.Equals(fileTypeExtension))
                {
                    success = true;
                }
            }
            return success;
        }
        public static bool CheckValidFileByMimeType(string fileMimeType)
        {
            bool success = false;
            var _myAcceptedFileTypes = CreateFileMimeTypes();
            foreach (var filetype in _myAcceptedFileTypes)
            {
                if (filetype.FileType.Equals(fileMimeType))
                {
                    success = true;
                }
            }
            return success;
        }
        public static List<string> ListMyFiles()
        {

            List<string> temp = new List<string>();
            var me = Helpers.AccountConfig.Settings.FileExtensions;

            List<string> myStuff = me.Split(',').ToList<string>();
            foreach (var t in myStuff)
            {
                temp.Add(t);
            }

            return temp;

        }


        /// <summary>
        /// create file mime types
        /// 
        /// check them against this session's posisble account config's file types
        /// </summary>
        /// <returns></returns>
        public static bool IsMyAcceptedFileType(string fileExtens)
        {
            var success = false;
            var AllFileTypes = CreateFileMimeTypes();
            var MySessFileExtensions = DocumentHelper.ListMyFiles();

            int i = 0;
            foreach (var item in AllFileTypes)
            {
                if (MySessFileExtensions.Contains(item.FileMimeType))
                {
                    return true;
                }
            }
            return false;
        }

        public static List<FileMimeTypes> CreateFileMimeTypes()
        {
            var acceptedFileTypes = new List<FileMimeTypes>();

            var acceptedType = new FileMimeTypes();



            ///Accepted Audio
            acceptedType.FileMimeType = ".wav";
            acceptedType.FileType = "audio/wav";
            acceptedFileTypes.Add(acceptedType);

            acceptedType.FileMimeType = ".wav";
            acceptedType.FileType = "audio/x-wav";
            acceptedFileTypes.Add(acceptedType);

            acceptedType.FileMimeType = ".wma";
            acceptedType.FileType = "audio/x-ms-wma";
            acceptedFileTypes.Add(acceptedType);

            acceptedType.FileMimeType = ".mp3";
            acceptedType.FileType = "audio/mpeg3";
            acceptedFileTypes.Add(acceptedType);

            acceptedType.FileMimeType = ".mp3";
            acceptedType.FileType = "audio/mp3";
            acceptedFileTypes.Add(acceptedType);

            acceptedType.FileMimeType = ".mp3";
            acceptedType.FileType = "audio/mpeg";
            acceptedFileTypes.Add(acceptedType);


            ///Accepted Documents
            acceptedType.FileMimeType = ".doc";
            acceptedType.FileType = "application/msword";
            acceptedFileTypes.Add(acceptedType);

            acceptedType.FileMimeType = ".docx";
            acceptedType.FileType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
            acceptedFileTypes.Add(acceptedType);

            acceptedType.FileMimeType = ".pdf";
            acceptedType.FileType = "application/pdf";
            acceptedFileTypes.Add(acceptedType);

            acceptedType.FileMimeType = ".ppt";
            acceptedType.FileType = "application/mspowerpoint";
            acceptedFileTypes.Add(acceptedType);

            acceptedType.FileMimeType = ".pptx";
            acceptedType.FileType = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
            acceptedFileTypes.Add(acceptedType);

            acceptedType.FileMimeType = ".xls";
            acceptedType.FileType = "application/excel";
            acceptedFileTypes.Add(acceptedType);

            acceptedType.FileMimeType = ".xlsx";
            acceptedType.FileType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            acceptedFileTypes.Add(acceptedType);

            acceptedType.FileMimeType = ".xlt";
            acceptedType.FileType = "application/excel";
            acceptedFileTypes.Add(acceptedType);

            acceptedType.FileMimeType = ".xltx";
            acceptedType.FileType = "application/vnd.openxmlformats-officedocument.spreadsheetml.template";
            acceptedFileTypes.Add(acceptedType);


            //acceptedType.FileMimeType = ".3dm";
            //acceptedType.FileType = "x-world/x-3dmf";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".3dmf";
            //acceptedType.FileType = "x-world/x-3dmf";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".a";
            //acceptedType.FileType = "application/octet-stream";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".aab";
            //acceptedType.FileType = "application/x-authorware-bin";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".aam";
            //acceptedType.FileType = "application/x-authorware-map";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".aas";
            //acceptedType.FileType = "application/x-authorware-seg";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".abc";
            //acceptedType.FileType = "text/vnd.abc";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".acgi";
            //acceptedType.FileType = "text/html";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".afl";
            //acceptedType.FileType = "video/animaflex";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".ai";
            //acceptedType.FileType = "application/postscript";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".aif";
            //acceptedType.FileType = "audio/aiff";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".aif";
            //acceptedType.FileType = "audio/x-aiff";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".aifc";
            //acceptedType.FileType = "audio/aiff";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".aifc";
            //acceptedType.FileType = "audio/x-aiff";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".aiff";
            //acceptedType.FileType = "audio/aiff";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".aiff";
            //acceptedType.FileType = "audio/x-aiff";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".aim";
            //acceptedType.FileType = "application/x-aim";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".aip";
            //acceptedType.FileType = "text/x-audiosoft-intra";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".ani";
            //acceptedType.FileType = "application/x-navi-animation";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".aos";
            //acceptedType.FileType = "application/x-nokia-9000-communicator-add-on-software";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".aps";
            //acceptedType.FileType = "application/mime";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".arc";
            //acceptedType.FileType = "application/octet-stream";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".arj";
            //acceptedType.FileType = "application/arj";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".arj";
            //acceptedType.FileType = "application/octet-stream";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".art";
            //acceptedType.FileType = "image/x-jg";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".asf";
            //acceptedType.FileType = "video/x-ms-asf";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".asm";
            //acceptedType.FileType = "text/x-asm";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".asp";
            //acceptedType.FileType = "text/asp";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".asx";
            //acceptedType.FileType = "application/x-mplayer2";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".asx";
            //acceptedType.FileType = "video/x-ms-asf";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".asx";
            //acceptedType.FileType = "video/x-ms-asf-plugin";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".au";
            //acceptedType.FileType = "audio/basic";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".au";
            //acceptedType.FileType = "audio/x-au";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".avi";
            //acceptedType.FileType = "application/x-troff-msvideo";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".avi";
            //acceptedType.FileType = "video/avi";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".avi";
            //acceptedType.FileType = "video/msvideo";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".avi";
            //acceptedType.FileType = "video/x-msvideo";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".avs";
            //acceptedType.FileType = "video/avs-video";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".bcpio";
            //acceptedType.FileType = "application/x-bcpio";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".bin";
            //acceptedType.FileType = "application/mac-binary";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".bin";
            //acceptedType.FileType = "application/macbinary";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".bin";
            //acceptedType.FileType = "application/octet-stream";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".bin";
            //acceptedType.FileType = "application/x-binary";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".bin";
            //acceptedType.FileType = "application/x-macbinary";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".bm";
            //acceptedType.FileType = "image/bmp";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".bmp";
            //acceptedType.FileType = "image/bmp";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".bmp";
            //acceptedType.FileType = "image/x-windows-bmp";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".boo";
            //acceptedType.FileType = "application/book";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".book";
            //acceptedType.FileType = "application/book";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".boz";
            //acceptedType.FileType = "application/x-bzip2";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".bsh";
            //acceptedType.FileType = "application/x-bsh";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".bz";
            //acceptedType.FileType = "application/x-bzip";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".bz2";
            //acceptedType.FileType = "application/x-bzip2";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".c";
            //acceptedType.FileType = "text/plain";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".c";
            //acceptedType.FileType = "text/x-c";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".c++";
            //acceptedType.FileType = "text/plain";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".cat";
            //acceptedType.FileType = "application/vnd.ms-pki.seccat";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".cc";
            //acceptedType.FileType = "text/plain";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".cc";
            //acceptedType.FileType = "text/x-c";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".ccad";
            //acceptedType.FileType = "application/clariscad";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".cco";
            //acceptedType.FileType = "application/x-cocoa";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".cdf";
            //acceptedType.FileType = "application/cdf";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".cdf";
            //acceptedType.FileType = "application/x-cdf";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".cdf";
            //acceptedType.FileType = "application/x-netcdf";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".cer";
            //acceptedType.FileType = "application/pkix-cert";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".cer";
            //acceptedType.FileType = "application/x-x509-ca-cert";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".cha";
            //acceptedType.FileType = "application/x-chat";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".chat";
            //acceptedType.FileType = "application/x-chat";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".class";
            //acceptedType.FileType = "application/java";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".class";
            //acceptedType.FileType = "application/java-byte-code";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".class";
            //acceptedType.FileType = "application/x-java-class";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".com";
            //acceptedType.FileType = "application/octet-stream";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".com";
            //acceptedType.FileType = "text/plain";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".conf";
            //acceptedType.FileType = "text/plain";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".cpio";
            //acceptedType.FileType = "application/x-cpio";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".cpp";
            //acceptedType.FileType = "text/x-c";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".cpt";
            //acceptedType.FileType = "application/mac-compactpro";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".cpt";
            //acceptedType.FileType = "application/x-compactpro";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".cpt";
            //acceptedType.FileType = "application/x-cpt";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".crl";
            //acceptedType.FileType = "application/pkcs-crl";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".crl";
            //acceptedType.FileType = "application/pkix-crl";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".crt";
            //acceptedType.FileType = "application/pkix-cert";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".crt";
            //acceptedType.FileType = "application/x-x509-ca-cert";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".crt";
            //acceptedType.FileType = "application/x-x509-user-cert";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".csh";
            //acceptedType.FileType = "application/x-csh";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".csh";
            //acceptedType.FileType = "text/x-script.csh";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".css";
            //acceptedType.FileType = "application/x-pointplus";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".css";
            //acceptedType.FileType = "text/css";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".cxx";
            //acceptedType.FileType = "text/plain";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".dcr";
            //acceptedType.FileType = "application/x-director";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".deepv";
            //acceptedType.FileType = "application/x-deepv";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".def";
            //acceptedType.FileType = "text/plain";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".der";
            //acceptedType.FileType = "application/x-x509-ca-cert";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".dif";
            //acceptedType.FileType = "video/x-dv";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".dir";
            //acceptedType.FileType = "application/x-director";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".dl";
            //acceptedType.FileType = "video/dl";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".dl";
            //acceptedType.FileType = "video/x-dl";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".doc";
            //acceptedType.FileType = "application/msword";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".dot";
            //acceptedType.FileType = "application/msword";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".dp";
            //acceptedType.FileType = "application/commonground";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".drw";
            //acceptedType.FileType = "application/drafting";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".dump";
            //acceptedType.FileType = "application/octet-stream";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".dv";
            //acceptedType.FileType = "video/x-dv";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".dvi";
            //acceptedType.FileType = "application/x-dvi";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".dwf";
            //acceptedType.FileType = "drawing/x-dwf (old)";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".dwf";
            //acceptedType.FileType = "model/vnd.dwf";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".dwg";
            //acceptedType.FileType = "application/acad";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".dwg";
            //acceptedType.FileType = "image/vnd.dwg";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".dwg";
            //acceptedType.FileType = "image/x-dwg";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".dxf";
            //acceptedType.FileType = "application/dxf";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".dxf";
            //acceptedType.FileType = "image/vnd.dwg";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".dxf";
            //acceptedType.FileType = "image/x-dwg";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".dxr";
            //acceptedType.FileType = "application/x-director";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".el";
            //acceptedType.FileType = "text/x-script.elisp";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".elc";
            //acceptedType.FileType = "application/x-bytecode.elisp (compiled elisp)";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".elc";
            //acceptedType.FileType = "application/x-elc";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".env";
            //acceptedType.FileType = "application/x-envoy";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".eps";
            //acceptedType.FileType = "application/postscript";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".es";
            //acceptedType.FileType = "application/x-esrehber";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".etx";
            //acceptedType.FileType = "text/x-setext";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".evy";
            //acceptedType.FileType = "application/envoy";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".evy";
            //acceptedType.FileType = "application/x-envoy";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".exe";
            //acceptedType.FileType = "application/octet-stream";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".f";
            //acceptedType.FileType = "text/plain";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".f";
            //acceptedType.FileType = "text/x-fortran";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".f77";
            //acceptedType.FileType = "text/x-fortran";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".f90";
            //acceptedType.FileType = "text/plain";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".f90";
            //acceptedType.FileType = "text/x-fortran";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".fdf";
            //acceptedType.FileType = "application/vnd.fdf";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".fif";
            //acceptedType.FileType = "application/fractals";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".fif";
            //acceptedType.FileType = "image/fif";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".fli";
            //acceptedType.FileType = "video/fli";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".fli";
            //acceptedType.FileType = "video/x-fli";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".flo";
            //acceptedType.FileType = "image/florian";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".flx";
            //acceptedType.FileType = "text/vnd.fmi.flexstor";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".fmf";
            //acceptedType.FileType = "video/x-atomic3d-feature";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".for";
            //acceptedType.FileType = "text/plain";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".for";
            //acceptedType.FileType = "text/x-fortran";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".fpx";
            //acceptedType.FileType = "image/vnd.fpx";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".fpx";
            //acceptedType.FileType = "image/vnd.net-fpx";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".frl";
            //acceptedType.FileType = "application/freeloader";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".funk";
            //acceptedType.FileType = "audio/make";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".g";
            //acceptedType.FileType = "text/plain";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".g3";
            //acceptedType.FileType = "image/g3fax";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".gif";
            //acceptedType.FileType = "image/gif";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".gl";
            //acceptedType.FileType = "video/gl";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".gl";
            //acceptedType.FileType = "video/x-gl";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".gsd";
            //acceptedType.FileType = "audio/x-gsm";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".gsm";
            //acceptedType.FileType = "audio/x-gsm";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".gsp";
            //acceptedType.FileType = "application/x-gsp";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".gss";
            //acceptedType.FileType = "application/x-gss";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".gtar";
            //acceptedType.FileType = "application/x-gtar";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".gz";
            //acceptedType.FileType = "application/x-compressed";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".gz";
            //acceptedType.FileType = "application/x-gzip";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".gzip";
            //acceptedType.FileType = "application/x-gzip";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".gzip";
            //acceptedType.FileType = "multipart/x-gzip";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".h";
            //acceptedType.FileType = "text/plain";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".h";
            //acceptedType.FileType = "text/x-h";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".hdf";
            //acceptedType.FileType = "application/x-hdf";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".help";
            //acceptedType.FileType = "application/x-helpfile";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".hgl";
            //acceptedType.FileType = "application/vnd.hp-hpgl";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".hh";
            //acceptedType.FileType = "text/plain";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".hh";
            //acceptedType.FileType = "text/x-h";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".hlb";
            //acceptedType.FileType = "text/x-script";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".hlp";
            //acceptedType.FileType = "application/hlp";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".hlp";
            //acceptedType.FileType = "application/x-helpfile";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".hlp";
            //acceptedType.FileType = "application/x-winhelp";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".hpg";
            //acceptedType.FileType = "application/vnd.hp-hpgl";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".hpgl";
            //acceptedType.FileType = "application/vnd.hp-hpgl";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".hqx";
            //acceptedType.FileType = "application/binhex";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".hqx";
            //acceptedType.FileType = "application/binhex4";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".hqx";
            //acceptedType.FileType = "application/mac-binhex";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".hqx";
            //acceptedType.FileType = "application/mac-binhex40";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".hqx";
            //acceptedType.FileType = "application/x-binhex40";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".hqx";
            //acceptedType.FileType = "application/x-mac-binhex40";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".hta";
            //acceptedType.FileType = "application/hta";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".htc";
            //acceptedType.FileType = "text/x-component";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".htm";
            //acceptedType.FileType = "text/html";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".html";
            //acceptedType.FileType = "text/html";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".htmls";
            //acceptedType.FileType = "text/html";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".htt";
            //acceptedType.FileType = "text/webviewhtml";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".htx";
            //acceptedType.FileType = "text/html";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".ice";
            //acceptedType.FileType = "x-conference/x-cooltalk";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".ico";
            //acceptedType.FileType = "image/x-icon";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".idc";
            //acceptedType.FileType = "text/plain";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".ief";
            //acceptedType.FileType = "image/ief";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".iefs";
            //acceptedType.FileType = "image/ief";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".iges";
            //acceptedType.FileType = "application/iges";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".iges";
            //acceptedType.FileType = "model/iges";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".igs";
            //acceptedType.FileType = "application/iges";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".igs";
            //acceptedType.FileType = "model/iges";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".ima";
            //acceptedType.FileType = "application/x-ima";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".imap";
            //acceptedType.FileType = "application/x-httpd-imap";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".inf";
            //acceptedType.FileType = "application/inf";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".ins";
            //acceptedType.FileType = "application/x-internett-signup";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".ip";
            //acceptedType.FileType = "application/x-ip2";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".isu";
            //acceptedType.FileType = "video/x-isvideo";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".it";
            //acceptedType.FileType = "audio/it";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".iv";
            //acceptedType.FileType = "application/x-inventor";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".ivr";
            //acceptedType.FileType = "i-world/i-vrml";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".ivy";
            //acceptedType.FileType = "application/x-livescreen";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".jam";
            //acceptedType.FileType = "audio/x-jam";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".jav";
            //acceptedType.FileType = "text/plain";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".jav";
            //acceptedType.FileType = "text/x-java-source";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".java";
            //acceptedType.FileType = "text/plain";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".java";
            //acceptedType.FileType = "text/x-java-source";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".jcm";
            //acceptedType.FileType = "application/x-java-commerce";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".jfif";
            //acceptedType.FileType = "image/jpeg";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".jfif";
            //acceptedType.FileType = "image/pjpeg";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".jfif-tbnl";
            //acceptedType.FileType = "image/jpeg";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".jpe";
            //acceptedType.FileType = "image/jpeg";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".jpe";
            //acceptedType.FileType = "image/pjpeg";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".jpeg";
            //acceptedType.FileType = "image/jpeg";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".jpeg";
            //acceptedType.FileType = "image/pjpeg";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".jpg";
            //acceptedType.FileType = "image/jpeg";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".jpg";
            //acceptedType.FileType = "image/pjpeg";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".jps";
            //acceptedType.FileType = "image/x-jps";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".js";
            //acceptedType.FileType = "application/x-javascript";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".jut";
            //acceptedType.FileType = "image/jutvision";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".kar";
            //acceptedType.FileType = "audio/midi";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".kar";
            //acceptedType.FileType = "music/x-karaoke";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".ksh";
            //acceptedType.FileType = "application/x-ksh";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".ksh";
            //acceptedType.FileType = "text/x-script.ksh";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".la";
            //acceptedType.FileType = "audio/nspaudio";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".la";
            //acceptedType.FileType = "audio/x-nspaudio";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".lam";
            //acceptedType.FileType = "audio/x-liveaudio";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".latex";
            //acceptedType.FileType = "application/x-latex";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".lha";
            //acceptedType.FileType = "application/lha";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".lha";
            //acceptedType.FileType = "application/octet-stream";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".lha";
            //acceptedType.FileType = "application/x-lha";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".lhx";
            //acceptedType.FileType = "application/octet-stream";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".list";
            //acceptedType.FileType = "text/plain";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".lma";
            //acceptedType.FileType = "audio/nspaudio";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".lma";
            //acceptedType.FileType = "audio/x-nspaudio";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".log";
            //acceptedType.FileType = "text/plain";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".lsp";
            //acceptedType.FileType = "application/x-lisp";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".lsp";
            //acceptedType.FileType = "text/x-script.lisp";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".lst";
            //acceptedType.FileType = "text/plain";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".lsx";
            //acceptedType.FileType = "text/x-la-asf";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".ltx";
            //acceptedType.FileType = "application/x-latex";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".lzh";
            //acceptedType.FileType = "application/octet-stream";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".lzh";
            //acceptedType.FileType = "application/x-lzh";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".lzx";
            //acceptedType.FileType = "application/lzx";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".lzx";
            //acceptedType.FileType = "application/octet-stream";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".lzx";
            //acceptedType.FileType = "application/x-lzx";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".m";
            //acceptedType.FileType = "text/plain";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".m";
            //acceptedType.FileType = "text/x-m";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".m1v";
            //acceptedType.FileType = "video/mpeg";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".m2a";
            //acceptedType.FileType = "audio/mpeg";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".m2v";
            //acceptedType.FileType = "video/mpeg";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".m3u";
            //acceptedType.FileType = "audio/x-mpequrl";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".man";
            //acceptedType.FileType = "application/x-troff-man";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".map";
            //acceptedType.FileType = "application/x-navimap";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mar";
            //acceptedType.FileType = "text/plain";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mbd";
            //acceptedType.FileType = "application/mbedlet";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mc$";
            //acceptedType.FileType = "application/x-magic-cap-package-1.0";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mcd";
            //acceptedType.FileType = "application/mcad";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mcd";
            //acceptedType.FileType = "application/x-mathcad";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mcf";
            //acceptedType.FileType = "image/vasa";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mcf";
            //acceptedType.FileType = "text/mcf";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mcp";
            //acceptedType.FileType = "application/netmc";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".me";
            //acceptedType.FileType = "application/x-troff-me";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mht";
            //acceptedType.FileType = "message/rfc822";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mhtml";
            //acceptedType.FileType = "message/rfc822";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mid";
            //acceptedType.FileType = "application/x-midi";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mid";
            //acceptedType.FileType = "audio/midi";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mid";
            //acceptedType.FileType = "audio/x-mid";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mid";
            //acceptedType.FileType = "audio/x-midi";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mid";
            //acceptedType.FileType = "music/crescendo";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mid";
            //acceptedType.FileType = "x-music/x-midi";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".midi";
            //acceptedType.FileType = "application/x-midi";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".midi";
            //acceptedType.FileType = "audio/midi";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".midi";
            //acceptedType.FileType = "audio/x-mid";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".midi";
            //acceptedType.FileType = "audio/x-midi";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".midi";
            //acceptedType.FileType = "music/crescendo";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".midi";
            //acceptedType.FileType = "x-music/x-midi";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mif";
            //acceptedType.FileType = "application/x-frame";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mif";
            //acceptedType.FileType = "application/x-mif";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mime";
            //acceptedType.FileType = "message/rfc822";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mime";
            //acceptedType.FileType = "www/mime";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mjf";
            //acceptedType.FileType = "audio/x-vnd.audioexplosion.mjuicemediafile";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mjpg";
            //acceptedType.FileType = "video/x-motion-jpeg";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mm";
            //acceptedType.FileType = "application/base64";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mm";
            //acceptedType.FileType = "application/x-meme";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mme";
            //acceptedType.FileType = "application/base64";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mod";
            //acceptedType.FileType = "audio/mod";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mod";
            //acceptedType.FileType = "audio/x-mod";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".moov";
            //acceptedType.FileType = "video/quicktime";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mov";
            //acceptedType.FileType = "video/quicktime";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".movie";
            //acceptedType.FileType = "video/x-sgi-movie";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mp2";
            //acceptedType.FileType = "audio/mpeg";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mp2";
            //acceptedType.FileType = "audio/x-mpeg";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mp2";
            //acceptedType.FileType = "video/mpeg";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mp2";
            //acceptedType.FileType = "video/x-mpeg";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mp2";
            //acceptedType.FileType = "video/x-mpeq2a";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mp3";
            //acceptedType.FileType = "audio/mpeg3";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mp3";
            //acceptedType.FileType = "audio/x-mpeg-3";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mp3";
            //acceptedType.FileType = "video/mpeg";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mp3";
            //acceptedType.FileType = "video/x-mpeg";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mpa";
            //acceptedType.FileType = "audio/mpeg";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mpa";
            //acceptedType.FileType = "video/mpeg";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mpc";
            //acceptedType.FileType = "application/x-project";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mpe";
            //acceptedType.FileType = "video/mpeg";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mpeg";
            //acceptedType.FileType = "video/mpeg";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mpg";
            //acceptedType.FileType = "audio/mpeg";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mpg";
            //acceptedType.FileType = "video/mpeg";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mpga";
            //acceptedType.FileType = "audio/mpeg";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mpp";
            //acceptedType.FileType = "application/vnd.ms-project";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mpt";
            //acceptedType.FileType = "application/x-project";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mpv";
            //acceptedType.FileType = "application/x-project";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mpx";
            //acceptedType.FileType = "application/x-project";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mrc";
            //acceptedType.FileType = "application/marc";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".ms";
            //acceptedType.FileType = "application/x-troff-ms";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mv";
            //acceptedType.FileType = "video/x-sgi-movie";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".my";
            //acceptedType.FileType = "audio/make";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".mzz";
            //acceptedType.FileType = "application/x-vnd.audioexplosion.mzz";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".nap";
            //acceptedType.FileType = "image/naplps";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".naplps";
            //acceptedType.FileType = "image/naplps";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".nc";
            //acceptedType.FileType = "application/x-netcdf";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".ncm";
            //acceptedType.FileType = "application/vnd.nokia.configuration-message";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".nif";
            //acceptedType.FileType = "image/x-niff";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".niff";
            //acceptedType.FileType = "image/x-niff";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".nix";
            //acceptedType.FileType = "application/x-mix-transfer";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".nsc";
            //acceptedType.FileType = "application/x-conference";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".nvd";
            //acceptedType.FileType = "application/x-navidoc";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".o";
            //acceptedType.FileType = "application/octet-stream";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".oda";
            //acceptedType.FileType = "application/oda";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".omc";
            //acceptedType.FileType = "application/x-omc";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".omcd";
            //acceptedType.FileType = "application/x-omcdatamaker";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".omcr";
            //acceptedType.FileType = "application/x-omcregerator";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".p";
            //acceptedType.FileType = "text/x-pascal";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".p10";
            //acceptedType.FileType = "application/pkcs10";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".p10";
            //acceptedType.FileType = "application/x-pkcs10";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".p12";
            //acceptedType.FileType = "application/pkcs-12";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".p12";
            //acceptedType.FileType = "application/x-pkcs12";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".p7a";
            //acceptedType.FileType = "application/x-pkcs7-signature";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".p7c";
            //acceptedType.FileType = "application/pkcs7-mime";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".p7c";
            //acceptedType.FileType = "application/x-pkcs7-mime";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".p7m";
            //acceptedType.FileType = "application/pkcs7-mime";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".p7m";
            //acceptedType.FileType = "application/x-pkcs7-mime";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".p7r";
            //acceptedType.FileType = "application/x-pkcs7-certreqresp";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".p7s";
            //acceptedType.FileType = "application/pkcs7-signature";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".part";
            //acceptedType.FileType = "application/pro_eng";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".pas";
            //acceptedType.FileType = "text/pascal";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".pbm";
            //acceptedType.FileType = "image/x-portable-bitmap";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".pcl";
            //acceptedType.FileType = "application/vnd.hp-pcl";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".pcl";
            //acceptedType.FileType = "application/x-pcl";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".pct";
            //acceptedType.FileType = "image/x-pict";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".pcx";
            //acceptedType.FileType = "image/x-pcx";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".pdb";
            //acceptedType.FileType = "chemical/x-pdb";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".pdf";
            //acceptedType.FileType = "application/pdf";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".pfunk";
            //acceptedType.FileType = "audio/make";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".pfunk";
            //acceptedType.FileType = "audio/make.my.funk";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".pgm";
            //acceptedType.FileType = "image/x-portable-graymap";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".pgm";
            //acceptedType.FileType = "image/x-portable-greymap";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".pic";
            //acceptedType.FileType = "image/pict";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".pict";
            //acceptedType.FileType = "image/pict";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".pkg";
            //acceptedType.FileType = "application/x-newton-compatible-pkg";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".pko";
            //acceptedType.FileType = "application/vnd.ms-pki.pko";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".pl";
            //acceptedType.FileType = "text/plain";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".pl";
            //acceptedType.FileType = "text/x-script.perl";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".plx";
            //acceptedType.FileType = "application/x-pixclscript";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".pm";
            //acceptedType.FileType = "image/x-xpixmap";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".pm";
            //acceptedType.FileType = "text/x-script.perl-module";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".pm4";
            //acceptedType.FileType = "application/x-pagemaker";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".pm5";
            //acceptedType.FileType = "application/x-pagemaker";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".png";
            //acceptedType.FileType = "image/png";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".pnm";
            //acceptedType.FileType = "application/x-portable-anymap";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".pnm";
            //acceptedType.FileType = "image/x-portable-anymap";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".pot";
            //acceptedType.FileType = "application/mspowerpoint";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".pot";
            //acceptedType.FileType = "application/vnd.ms-powerpoint";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".pov";
            //acceptedType.FileType = "model/x-pov";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".ppa";
            //acceptedType.FileType = "application/vnd.ms-powerpoint";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".ppm";
            //acceptedType.FileType = "image/x-portable-pixmap";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".pps";
            //acceptedType.FileType = "application/mspowerpoint";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".pps";
            //acceptedType.FileType = "application/vnd.ms-powerpoint";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".ppt";
            //acceptedType.FileType = "application/mspowerpoint";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".ppt";
            //acceptedType.FileType = "application/powerpoint";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".ppt";
            //acceptedType.FileType = "application/vnd.ms-powerpoint";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".ppt";
            //acceptedType.FileType = "application/x-mspowerpoint";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".ppz";
            //acceptedType.FileType = "application/mspowerpoint";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".pre";
            //acceptedType.FileType = "application/x-freelance";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".prt";
            //acceptedType.FileType = "application/pro_eng";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".ps";
            //acceptedType.FileType = "application/postscript";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".psd";
            //acceptedType.FileType = "application/octet-stream";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".pvu";
            //acceptedType.FileType = "paleovu/x-pv";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".pwz";
            //acceptedType.FileType = "application/vnd.ms-powerpoint";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".py";
            //acceptedType.FileType = "text/x-script.phyton";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".pyc";
            //acceptedType.FileType = "applicaiton/x-bytecode.python";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".qcp";
            //acceptedType.FileType = "audio/vnd.qcelp";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".qd3";
            //acceptedType.FileType = "x-world/x-3dmf";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".qd3d";
            //acceptedType.FileType = "x-world/x-3dmf";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".qif";
            //acceptedType.FileType = "image/x-quicktime";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".qt";
            //acceptedType.FileType = "video/quicktime";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".qtc";
            //acceptedType.FileType = "video/x-qtc";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".qti";
            //acceptedType.FileType = "image/x-quicktime";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".qtif";
            //acceptedType.FileType = "image/x-quicktime";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".ra";
            //acceptedType.FileType = "audio/x-pn-realaudio";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".ra";
            //acceptedType.FileType = "audio/x-pn-realaudio-plugin";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".ra";
            //acceptedType.FileType = "audio/x-realaudio";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".ram";
            //acceptedType.FileType = "audio/x-pn-realaudio";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".ras";
            //acceptedType.FileType = "application/x-cmu-raster";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".ras";
            //acceptedType.FileType = "image/cmu-raster";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".ras";
            //acceptedType.FileType = "image/x-cmu-raster";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".rast";
            //acceptedType.FileType = "image/cmu-raster";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".rexx";
            //acceptedType.FileType = "text/x-script.rexx";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".rf";
            //acceptedType.FileType = "image/vnd.rn-realflash";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".rgb";
            //acceptedType.FileType = "image/x-rgb";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".rm";
            //acceptedType.FileType = "application/vnd.rn-realmedia";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".rm";
            //acceptedType.FileType = "audio/x-pn-realaudio";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".rmi";
            //acceptedType.FileType = "audio/mid";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".rmm";
            //acceptedType.FileType = "audio/x-pn-realaudio";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".rmp";
            //acceptedType.FileType = "audio/x-pn-realaudio";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".rmp";
            //acceptedType.FileType = "audio/x-pn-realaudio-plugin";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".rng";
            //acceptedType.FileType = "application/ringing-tones";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".rng";
            //acceptedType.FileType = "application/vnd.nokia.ringing-tone";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".rnx";
            //acceptedType.FileType = "application/vnd.rn-realplayer";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".roff";
            //acceptedType.FileType = "application/x-troff";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".rp";
            //acceptedType.FileType = "image/vnd.rn-realpix";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".rpm";
            //acceptedType.FileType = "audio/x-pn-realaudio-plugin";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".rt";
            //acceptedType.FileType = "text/richtext";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".rt";
            //acceptedType.FileType = "text/vnd.rn-realtext";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".rtf";
            //acceptedType.FileType = "application/rtf";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".rtf";
            //acceptedType.FileType = "application/x-rtf";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".rtf";
            //acceptedType.FileType = "text/richtext";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".rtx";
            //acceptedType.FileType = "application/rtf";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".rtx";
            //acceptedType.FileType = "text/richtext";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".rv";
            //acceptedType.FileType = "video/vnd.rn-realvideo";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".s";
            //acceptedType.FileType = "text/x-asm";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".s3m";
            //acceptedType.FileType = "audio/s3m";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".saveme";
            //acceptedType.FileType = "application/octet-stream";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".sbk";
            //acceptedType.FileType = "application/x-tbook";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".scm";
            //acceptedType.FileType = "application/x-lotusscreencam";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".scm";
            //acceptedType.FileType = "text/x-script.guile";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".scm";
            //acceptedType.FileType = "text/x-script.scheme";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".scm";
            //acceptedType.FileType = "video/x-scm";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".sdml";
            //acceptedType.FileType = "text/plain";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".sdp";
            //acceptedType.FileType = "application/sdp";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".sdp";
            //acceptedType.FileType = "application/x-sdp";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".sdr";
            //acceptedType.FileType = "application/sounder";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".sea";
            //acceptedType.FileType = "application/sea";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".sea";
            //acceptedType.FileType = "application/x-sea";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".set";
            //acceptedType.FileType = "application/set";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".sgm";
            //acceptedType.FileType = "text/sgml";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".sgm";
            //acceptedType.FileType = "text/x-sgml";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".sgml";
            //acceptedType.FileType = "text/sgml";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".sgml";
            //acceptedType.FileType = "text/x-sgml";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".sh";
            //acceptedType.FileType = "application/x-bsh";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".sh";
            //acceptedType.FileType = "application/x-sh";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".sh";
            //acceptedType.FileType = "application/x-shar";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".sh";
            //acceptedType.FileType = "text/x-script.sh";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".shar";
            //acceptedType.FileType = "application/x-bsh";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".shar";
            //acceptedType.FileType = "application/x-shar";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".shtml";
            //acceptedType.FileType = "text/html";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".shtml";
            //acceptedType.FileType = "text/x-server-parsed-html";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".sid";
            //acceptedType.FileType = "audio/x-psid";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".sit";
            //acceptedType.FileType = "application/x-sit";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".sit";
            //acceptedType.FileType = "application/x-stuffit";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".skd";
            //acceptedType.FileType = "application/x-koan";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".skm";
            //acceptedType.FileType = "application/x-koan";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".skp";
            //acceptedType.FileType = "application/x-koan";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".skt";
            //acceptedType.FileType = "application/x-koan";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".sl";
            //acceptedType.FileType = "application/x-seelogo";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".smi";
            //acceptedType.FileType = "application/smil";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".smil";
            //acceptedType.FileType = "application/smil";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".snd";
            //acceptedType.FileType = "audio/basic";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".snd";
            //acceptedType.FileType = "audio/x-adpcm";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".sol";
            //acceptedType.FileType = "application/solids";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".spc";
            //acceptedType.FileType = "application/x-pkcs7-certificates";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".spc";
            //acceptedType.FileType = "text/x-speech";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".spl";
            //acceptedType.FileType = "application/futuresplash";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".spr";
            //acceptedType.FileType = "application/x-sprite";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".sprite";
            //acceptedType.FileType = "application/x-sprite";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".src";
            //acceptedType.FileType = "application/x-wais-source";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".ssi";
            //acceptedType.FileType = "text/x-server-parsed-html";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".ssm";
            //acceptedType.FileType = "application/streamingmedia";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".sst";
            //acceptedType.FileType = "application/vnd.ms-pki.certstore";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".step";
            //acceptedType.FileType = "application/step";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".stl";
            //acceptedType.FileType = "application/sla";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".stl";
            //acceptedType.FileType = "application/vnd.ms-pki.stl";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".stl";
            //acceptedType.FileType = "application/x-navistyle";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".stp";
            //acceptedType.FileType = "application/step";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".sv4cpio";
            //acceptedType.FileType = "application/x-sv4cpio";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".sv4crc";
            //acceptedType.FileType = "application/x-sv4crc";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".svf";
            //acceptedType.FileType = "image/vnd.dwg";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".svf";
            //acceptedType.FileType = "image/x-dwg";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".svr";
            //acceptedType.FileType = "application/x-world";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".svr";
            //acceptedType.FileType = "x-world/x-svr";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".swf";
            //acceptedType.FileType = "application/x-shockwave-flash";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".t";
            //acceptedType.FileType = "application/x-troff";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".talk";
            //acceptedType.FileType = "text/x-speech";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".tar";
            //acceptedType.FileType = "application/x-tar";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".tbk";
            //acceptedType.FileType = "application/toolbook";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".tbk";
            //acceptedType.FileType = "application/x-tbook";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".tcl";
            //acceptedType.FileType = "application/x-tcl";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".tcl";
            //acceptedType.FileType = "text/x-script.tcl";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".tcsh";
            //acceptedType.FileType = "text/x-script.tcsh";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".tex";
            //acceptedType.FileType = "application/x-tex";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".texi";
            //acceptedType.FileType = "application/x-texinfo";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".texinfo";
            //acceptedType.FileType = "application/x-texinfo";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".text";
            //acceptedType.FileType = "application/plain";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".text";
            //acceptedType.FileType = "text/plain";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".tgz";
            //acceptedType.FileType = "application/gnutar";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".tgz";
            //acceptedType.FileType = "application/x-compressed";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".tif";
            //acceptedType.FileType = "image/tiff";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".tif";
            //acceptedType.FileType = "image/x-tiff";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".tiff";
            //acceptedType.FileType = "image/tiff";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".tiff";
            //acceptedType.FileType = "image/x-tiff";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".tr";
            //acceptedType.FileType = "application/x-troff";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".tsi";
            //acceptedType.FileType = "audio/tsp-audio";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".tsp";
            //acceptedType.FileType = "application/dsptype";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".tsp";
            //acceptedType.FileType = "audio/tsplayer";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".tsv";
            //acceptedType.FileType = "text/tab-separated-values";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".turbot";
            //acceptedType.FileType = "image/florian";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".txt";
            //acceptedType.FileType = "text/plain";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".uil";
            //acceptedType.FileType = "text/x-uil";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".uni";
            //acceptedType.FileType = "text/uri-list";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".unis";
            //acceptedType.FileType = "text/uri-list";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".unv";
            //acceptedType.FileType = "application/i-deas";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".uri";
            //acceptedType.FileType = "text/uri-list";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".uris";
            //acceptedType.FileType = "text/uri-list";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".ustar";
            //acceptedType.FileType = "application/x-ustar";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".ustar";
            //acceptedType.FileType = "multipart/x-ustar";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".uu";
            //acceptedType.FileType = "application/octet-stream";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".uu";
            //acceptedType.FileType = "text/x-uuencode";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".uue";
            //acceptedType.FileType = "text/x-uuencode";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".vcd";
            //acceptedType.FileType = "application/x-cdlink";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".vcs";
            //acceptedType.FileType = "text/x-vcalendar";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".vda";
            //acceptedType.FileType = "application/vda";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".vdo";
            //acceptedType.FileType = "video/vdo";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".vew";
            //acceptedType.FileType = "application/groupwise";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".viv";
            //acceptedType.FileType = "video/vivo";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".viv";
            //acceptedType.FileType = "video/vnd.vivo";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".vivo";
            //acceptedType.FileType = "video/vivo";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".vivo";
            //acceptedType.FileType = "video/vnd.vivo";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".vmd";
            //acceptedType.FileType = "application/vocaltec-media-desc";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".vmf";
            //acceptedType.FileType = "application/vocaltec-media-file";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".voc";
            //acceptedType.FileType = "audio/voc";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".voc";
            //acceptedType.FileType = "audio/x-voc";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".vos";
            //acceptedType.FileType = "video/vosaic";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".vox";
            //acceptedType.FileType = "audio/voxware";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".vqe";
            //acceptedType.FileType = "audio/x-twinvq-plugin";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".vqf";
            //acceptedType.FileType = "audio/x-twinvq";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".vql";
            //acceptedType.FileType = "audio/x-twinvq-plugin";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".vrml";
            //acceptedType.FileType = "application/x-vrml";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".vrml";
            //acceptedType.FileType = "model/vrml";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".vrml";
            //acceptedType.FileType = "x-world/x-vrml";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".vrt";
            //acceptedType.FileType = "x-world/x-vrt";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".vsd";
            //acceptedType.FileType = "application/x-visio";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".vst";
            //acceptedType.FileType = "application/x-visio";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".vsw";
            //acceptedType.FileType = "application/x-visio";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".w60";
            //acceptedType.FileType = "application/wordperfect6.0";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".w61";
            //acceptedType.FileType = "application/wordperfect6.1";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".w6w";
            //acceptedType.FileType = "application/msword";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".wav";
            //acceptedType.FileType = "audio/wav";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".wav";
            //acceptedType.FileType = "audio/x-wav";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".wb1";
            //acceptedType.FileType = "application/x-qpro";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".wbmp";
            //acceptedType.FileType = "image/vnd.wap.wbmp";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".web";
            //acceptedType.FileType = "application/vnd.xara";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".wiz";
            //acceptedType.FileType = "application/msword";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".wk1";
            //acceptedType.FileType = "application/x-123";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".wmf";
            //acceptedType.FileType = "windows/metafile";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".wml";
            //acceptedType.FileType = "text/vnd.wap.wml";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".wmlc";
            //acceptedType.FileType = "application/vnd.wap.wmlc";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".wmls";
            //acceptedType.FileType = "text/vnd.wap.wmlscript";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".wmlsc";
            //acceptedType.FileType = "application/vnd.wap.wmlscriptc";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".word";
            //acceptedType.FileType = "application/msword";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".wp";
            //acceptedType.FileType = "application/wordperfect";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".wp5";
            //acceptedType.FileType = "application/wordperfect";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".wp5";
            //acceptedType.FileType = "application/wordperfect6.0";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".wp6";
            //acceptedType.FileType = "application/wordperfect";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".wpd";
            //acceptedType.FileType = "application/wordperfect";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".wpd";
            //acceptedType.FileType = "application/x-wpwin";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".wq1";
            //acceptedType.FileType = "application/x-lotus";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".wri";
            //acceptedType.FileType = "application/mswrite";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".wri";
            //acceptedType.FileType = "application/x-wri";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".wrl";
            //acceptedType.FileType = "application/x-world";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".wrl";
            //acceptedType.FileType = "model/vrml";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".wrl";
            //acceptedType.FileType = "x-world/x-vrml";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".wrz";
            //acceptedType.FileType = "model/vrml";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".wrz";
            //acceptedType.FileType = "x-world/x-vrml";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".wsc";
            //acceptedType.FileType = "text/scriplet";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".wsrc";
            //acceptedType.FileType = "application/x-wais-source";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".wtk";
            //acceptedType.FileType = "application/x-wintalk";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xbm";
            //acceptedType.FileType = "image/x-xbitmap";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xbm";
            //acceptedType.FileType = "image/x-xbm";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xbm";
            //acceptedType.FileType = "image/xbm";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xdr";
            //acceptedType.FileType = "video/x-amt-demorun";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xgz";
            //acceptedType.FileType = "xgl/drawing";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xif";
            //acceptedType.FileType = "image/vnd.xiff";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xl";
            //acceptedType.FileType = "application/excel";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xla";
            //acceptedType.FileType = "application/excel";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xla";
            //acceptedType.FileType = "application/x-excel";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xla";
            //acceptedType.FileType = "application/x-msexcel";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xlb";
            //acceptedType.FileType = "application/excel";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xlb";
            //acceptedType.FileType = "application/vnd.ms-excel";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xlb";
            //acceptedType.FileType = "application/x-excel";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xlc";
            //acceptedType.FileType = "application/excel";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xlc";
            //acceptedType.FileType = "application/vnd.ms-excel";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xlc";
            //acceptedType.FileType = "application/x-excel";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xld";
            //acceptedType.FileType = "application/excel";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xld";
            //acceptedType.FileType = "application/x-excel";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xlk";
            //acceptedType.FileType = "application/excel";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xlk";
            //acceptedType.FileType = "application/x-excel";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xll";
            //acceptedType.FileType = "application/excel";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xll";
            //acceptedType.FileType = "application/vnd.ms-excel";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xll";
            //acceptedType.FileType = "application/x-excel";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xlm";
            //acceptedType.FileType = "application/excel";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xlm";
            //acceptedType.FileType = "application/vnd.ms-excel";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xlm";
            //acceptedType.FileType = "application/x-excel";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xls";
            //acceptedType.FileType = "application/excel";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xls";
            //acceptedType.FileType = "application/vnd.ms-excel";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xls";
            //acceptedType.FileType = "application/x-excel";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xls";
            //acceptedType.FileType = "application/x-msexcel";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xlt";
            //acceptedType.FileType = "application/excel";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xlt";
            //acceptedType.FileType = "application/x-excel";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xlv";
            //acceptedType.FileType = "application/excel";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xlv";
            //acceptedType.FileType = "application/x-excel";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xlw";
            //acceptedType.FileType = "application/excel";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xlw";
            //acceptedType.FileType = "application/vnd.ms-excel";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xlw";
            //acceptedType.FileType = "application/x-excel";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xlw";
            //acceptedType.FileType = "application/x-msexcel";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xm";
            //acceptedType.FileType = "audio/xm";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xml";
            //acceptedType.FileType = "application/xml";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xml";
            //acceptedType.FileType = "text/xml";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xmz";
            //acceptedType.FileType = "xgl/movie";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xpix";
            //acceptedType.FileType = "application/x-vnd.ls-xpix";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xpm";
            //acceptedType.FileType = "image/x-xpixmap";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xpm";
            //acceptedType.FileType = "image/xpm";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".x-png";
            //acceptedType.FileType = "image/png";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xsr";
            //acceptedType.FileType = "video/x-amt-showrun";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xwd";
            //acceptedType.FileType = "image/x-xwd";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xwd";
            //acceptedType.FileType = "image/x-xwindowdump";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".xyz";
            //acceptedType.FileType = "chemical/x-pdb";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".z";
            //acceptedType.FileType = "application/x-compress";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".z";
            //acceptedType.FileType = "application/x-compressed";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".zip";
            //acceptedType.FileType = "application/x-compressed";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".zip";
            //acceptedType.FileType = "application/x-zip-compressed";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".zip";
            //acceptedType.FileType = "application/zip";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".zip";
            //acceptedType.FileType = "multipart/x-zip";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".zoo";
            //acceptedType.FileType = "application/octet-stream";
            //acceptedFileTypes.Add(acceptedType);

            //acceptedType.FileMimeType = ".zsh";
            //acceptedType.FileType = "text/x-script.zsh";
            //acceptedFileTypes.Add(acceptedType);

            return acceptedFileTypes;
        }






    }
}