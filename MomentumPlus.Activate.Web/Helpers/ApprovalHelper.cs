﻿//using System;
//using System.Linq;
//using MomentumPlus.Activate.Entities.Models;
//using MomentumPlus.Activate.Web.Models;
//using MomentumPlus.Activate.Entities.Models.Moderation;
//using System.Collections.Generic;
//using MomentumPlus.Activate.Entities.Models.People;

//namespace MomentumPlus.Activate.Web.Helpers
//{
//    public class ApprovalHelper
//    {
//        #region BaseApproval (Obsolete)

//        /// <summary>
//        /// Set the approval properties on a <c>BaseApprovalViewModel</c>
//        /// for the given <c>Package</c> and <c>User</c>.
//        /// </summary>
//        [Obsolete]
//        public static void SetApproval(
//            BaseApprovalViewModel baseApprovalViewModel,
//            Package package,
//            User user
//        )
//        {
//            if (package != null)
//            {
//                baseApprovalViewModel.UserIsLineManager = package.UserLineManagerId == user.ID;
//                baseApprovalViewModel.UserIsHRManager = package.UserHRManagerId == user.ID;

//                if (package.Trigger != null &&
//                    package.Trigger.Moderations.Any( m => m.Decision == ModerationDecision.Approved ))
//                {
//                    baseApprovalViewModel.DisableReadyForReview = true;
//                }
//            }
//            else
//            {
//                // No package so assume a framework item
//                baseApprovalViewModel.FrameworkItem = true;
//            }
//        }

//        /// <summary>
//        /// Set the approval properties on a <c>BaseApproval</c> object given a
//        /// <c>BaseApprovalViewModel</c>, <c>Package</c>, and <c>User</c>.
//        /// </summary>
//        [Obsolete]
//        public static void UpdateApproval(
//            BaseApproval baseApproval,
//            BaseApprovalViewModel baseApprovalViewModel,
//            Package package,
//            User user
//        )
//        {
//            if (package != null)
//            {
//                if (user.ID == package.UserLineManagerId)
//                {
//                    baseApproval.ApprovedLineManager = baseApprovalViewModel.IsApprovedLineManager ? (DateTime?)DateTime.Now : null;
//                    baseApproval.DeclinedNotesLineManager = baseApprovalViewModel.DeclinedNotesLineManager;
//                }

//                if (user.ID == package.UserHRManagerId)
//                {
//                    baseApproval.ApprovedHRManager = baseApprovalViewModel.IsApprovedHRManager ? (DateTime?)DateTime.Now : null;
//                    baseApproval.DeclinedNotesHRManager = baseApprovalViewModel.DeclinedNotesHRManager;
//                }

//                baseApproval.ReadyForReview = baseApprovalViewModel.ReadyForReview;
//            }
//        }

//        #endregion
//    }
//}