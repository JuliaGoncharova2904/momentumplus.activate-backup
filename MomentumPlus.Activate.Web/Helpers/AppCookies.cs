﻿using System.Collections.Generic;
using System.Web;
using System.Web.Helpers;

namespace MomentumPlus.Activate.Web.Helpers
{
    public class AppCookies
    {
        private static string SelectedCompaniesId = "af4ba75e-ab47-43aa-89e3-e33a29766186";
        private const string CompaniesId = "DD7A021A-E091-4CA9-AC83-7EEE109A3E24";

        public static IEnumerable<string> SelectedCompanies
        {
            get
            {
                var cookie = HttpContext.Current.Request.Cookies[SelectedCompaniesId];

                if (cookie != null && !string.IsNullOrWhiteSpace(cookie.Value))
                {
                    return Json.Decode<IEnumerable<string>>(cookie.Value);
                }
                else
                {
                    return null;
                }
            }
            set
            {
                if (value == null)
                {
                    value = new List<string>();
                }

                var cookie = new HttpCookie(SelectedCompaniesId, Json.Encode(value));
                HttpContext.Current.Response.Cookies.Add(cookie);
            }
        }

        public static IDictionary<string, bool> Companies
        {
            get
            {
                var cookie = HttpContext.Current.Request.Cookies[CompaniesId];

                if (cookie != null && !string.IsNullOrWhiteSpace(cookie.Value))
                {
                    return Json.Decode<IDictionary<string, bool>>(cookie.Value);
                }
                else
                {
                    return new Dictionary<string, bool>();
                }
            }
            set
            {
                if (value == null)
                {
                    value = new Dictionary<string, bool>();
                }

                var cookie = new HttpCookie(CompaniesId, Json.Encode(value));
                HttpContext.Current.Response.Cookies.Add(cookie);
            }
        }
    }
}