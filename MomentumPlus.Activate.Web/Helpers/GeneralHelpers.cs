﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MomentumPlus.Activate.Web.Helpers
{
    public class GeneralHelpers
    {
        public static string GetUrlId(string url)
        {
            if (url != null)
            {
                var items = url.Split('/');
                return items.LastOrDefault() ?? "";
            }
            else
            {
                return "";
            }
        }
    }
}