﻿using System;
using System.Web;

namespace MomentumPlus.Activate.Web.Helpers
{
    /// <summary>
    /// Helper class to keep track of active user sessions.
    /// </summary>
    public class ActiveSessions
    {
        /// <summary>
        /// Prefix that will be added to usernames to form
        /// the cache keys.
        /// </summary>
        private static string CacheKeyPrefix = "Session_";

        /// <summary>
        /// Add a new active session to the cache for a given username.
        /// The previous session will be removed.
        /// </summary>
        public static void Add(string username)
        {
            var cachedSessionId = Remove(username);

            HttpContext.Current.Cache.Add(
                CacheKeyPrefix + username,
                HttpContext.Current.Session.SessionID,
                null,
                System.Web.Caching.Cache.NoAbsoluteExpiration,
                TimeSpan.FromMinutes(HttpContext.Current.Session.Timeout),
                System.Web.Caching.CacheItemPriority.Normal,
                null
            );
        }

        /// <summary>
        /// Check if a given session ID is valid for a given username.
        /// </summary>
        public static bool IsValid(string username, string sessionId)
        {
            var cachedSessionId = HttpContext.Current.Cache.Get(CacheKeyPrefix + username);
            return (string)cachedSessionId == sessionId;
        }

        /// <summary>
        /// Remove an active session from the cache.
        /// </summary>
        public static object Remove(string username)
        {
            return HttpContext.Current.Cache.Remove(CacheKeyPrefix + username);
        }
    }
}