﻿using System.Runtime.CompilerServices;

namespace MomentumPlus.Activate.Web.Helpers
{
    /// <summary>
    /// This list is alphabetical
    /// 
    /// Where there's ambiguity (eg, Employees section has many controllers)
    /// it's noted which of the controllers in the comments above the item 
    /// 
    /// </summary>
    public class RoleAuthHelper
    {
        public const string Administrator = "Administrator";
        public const string HRManager = "HR Manager";
        public const string LineManager = "Line Manager";
        public const string User = "User";
        public const string iHandoverAgent = "iHandover Agent";
        public const string Executive = "Executive";
        public const string iHandoverAdmin = "iHandover Admin";

        // THESE USERS HAVE ABSOLUET ACCESS TO EVERYTHING IN THE APPLICATION!
        public const string ABSOLUTE_GENERAL_ACCESS = Administrator + ", " + iHandoverAgent + ", " + iHandoverAdmin;


        //Assets
        public const string Assets_General = ABSOLUTE_GENERAL_ACCESS + ", " + Executive;

        //Admin
        public const string Admin_General = ABSOLUTE_GENERAL_ACCESS + ", " + Executive;

        //Approvals
        public const string Approvals_General = ABSOLUTE_GENERAL_ACCESS + ", " + HRManager + ", " + LineManager;

        //Companies
        public const string Companies_General = ABSOLUTE_GENERAL_ACCESS;

        //Document Library
        public const string DocLib_General = ABSOLUTE_GENERAL_ACCESS + ", " + Executive + ", " + HRManager + ", " + LineManager + ", " + User;

        //Employee (unique from Employees!)
        public const string Employee_General = ABSOLUTE_GENERAL_ACCESS + ", " + Executive;
        public const string Employee_View = ABSOLUTE_GENERAL_ACCESS + ", " + Executive;
        public const string Employee_Create = ABSOLUTE_GENERAL_ACCESS + ", " + HRManager;
        public const string Employee_Edit = ABSOLUTE_GENERAL_ACCESS + ", " + HRManager + ", " + Executive;

        //Employees for HumanResources Area (unique from Employee!)
        public const string Employees_HumanResources_General = ABSOLUTE_GENERAL_ACCESS + ", " + HRManager + ", " + Executive;

        //Employees for Packages area
        public const string Employees_Packages_General = ABSOLUTE_GENERAL_ACCESS + ", " + HRManager + ", " + LineManager + ", " + Executive;

        //Expert Users
        public const string ExpertUsers_General = ABSOLUTE_GENERAL_ACCESS + ", " + Executive;

        //Framework
        public const string Framework_General = ABSOLUTE_GENERAL_ACCESS + ", " + Executive;

        //Dashboard
        public const string Dashboard = ABSOLUTE_GENERAL_ACCESS + ", " + HRManager + ", " + LineManager + ", " + Executive;

        //Handovers
        public const string Handover_Controller = ABSOLUTE_GENERAL_ACCESS + ", " + HRManager + ", " + LineManager + ", " + Executive;
        public const string Handover_Controller_Accept = User;

        //Timeline
        public const string Timeline_Controller = ABSOLUTE_GENERAL_ACCESS + ", " + HRManager + ", " + LineManager + ", " + Executive;

        //Human Resources
        public const string HumanResources_General = ABSOLUTE_GENERAL_ACCESS + ", " + HRManager + ", " + Executive;

        //In Handover
        public const string InHandover_General = ABSOLUTE_GENERAL_ACCESS + ", " + HRManager + ", " + LineManager + ", " + Executive;

        //In Use
        public const string InUse_General = ABSOLUTE_GENERAL_ACCESS + ", " + HRManager + ", " + LineManager + ", " + Executive;

        //Keywords
        public const string Keywords_General = ABSOLUTE_GENERAL_ACCESS + ", " + Executive;

        //Management
        public const string Management_General = ABSOLUTE_GENERAL_ACCESS + ", " + HRManager + ", " + LineManager + ", " + Executive;

        //Master Lists
        public const string MasterLists_General = ABSOLUTE_GENERAL_ACCESS + ", " + HRManager + ", " + Executive;
        public const string MasterLists_Controller_General = ABSOLUTE_GENERAL_ACCESS + ", " + Executive;

        //My People
        public const string MyPeople_General = ABSOLUTE_GENERAL_ACCESS + ", " + HRManager + ", " + LineManager + ", " + Executive;

        //Organisational Contacts
        public const string OrganisationalContacts_General = ABSOLUTE_GENERAL_ACCESS + ", " + Executive;

        //Package (unique from Packages!)
        public const string Package_Controller_General = ABSOLUTE_GENERAL_ACCESS + ", " + HRManager + ", " + LineManager + ", " + Executive;
        public const string Package_Controller_Create = ABSOLUTE_GENERAL_ACCESS + ", " + HRManager + ", " + Executive;

        //Package Edit
        public const string Package_Controller_Edit = ABSOLUTE_GENERAL_ACCESS + ", " + HRManager + ", " + LineManager;
        public const string Package_Controller_Edit_GET = ABSOLUTE_GENERAL_ACCESS + ", " + HRManager + ", " + LineManager;

        //Package -> Trigger
        public const string Package_Controller_Trigger_Create = ABSOLUTE_GENERAL_ACCESS + ", " + HRManager + ", " + Executive;

        //Packages (Distinct from Package!)
        public const string Packages_Controller_General = ABSOLUTE_GENERAL_ACCESS + ", " + HRManager + ", " + LineManager + ", " + Executive;

        // Package Transfer
        public const string PackageTransfer_Controller_General = ABSOLUTE_GENERAL_ACCESS + ", " + HRManager + ", " + LineManager;

        //Positions
        public const string Positions_General = ABSOLUTE_GENERAL_ACCESS + ", " + HRManager + ", " + Executive;

        //positions controller
        public const string Position_Controller_Create = ABSOLUTE_GENERAL_ACCESS + ", " + HRManager + ", " + Executive;

        //Positions for Packages
        public const string Positions_Packages_General = ABSOLUTE_GENERAL_ACCESS + ", " + HRManager + ", " + LineManager + ", " + Executive;

        //Projects
        public const string Projects_General = ABSOLUTE_GENERAL_ACCESS + ", " + Executive;
        public const string Projects_GetProjectInfo = ABSOLUTE_GENERAL_ACCESS + ", " + HRManager + ", " + LineManager + ", " + User + ", " + Executive;

        //Relationship
        public const string Relationship_Edit = ABSOLUTE_GENERAL_ACCESS + ", " + ", " + Executive;
        public const string Relationship_CreateMeeting = ABSOLUTE_GENERAL_ACCESS + ", " + ", " + Executive;
        public const string Relationship_EditMeeting = ABSOLUTE_GENERAL_ACCESS + ", " + ", " + Executive;

        //Reports - Subjectivity
        public const string Reports_SubjectivityReports = ABSOLUTE_GENERAL_ACCESS + ", " + HRManager + ", " + LineManager + ", " + Executive;

        //Search Tags
        public const string SearchTags_General = ABSOLUTE_GENERAL_ACCESS + ", " + Executive;
        public const string SearchTags_GetSuggestions = ABSOLUTE_GENERAL_ACCESS + ", " + HRManager + ", " + LineManager + ", " + User + ", " + Executive;

        //Settings
        public const string Settings_General = ABSOLUTE_GENERAL_ACCESS + ", " + iHandoverAgent + ", " + Executive;

        //Systems
        public const string Systems_General = ABSOLUTE_GENERAL_ACCESS + ", " + Executive;

        //Template
        public const string Template_General = ABSOLUTE_GENERAL_ACCESS + ", " + Executive;

        //Task -- Task also authorises "Task" in it's method
        public const string Task_Edit_Get = null;

        //Topic

        //Topic General also Authorises "Topic"
        public const string Topic_General = null;

        public const string Topic_Create_Post = null;
        public const string Topic_Create_Get = null;
        public const string Topic_Edit_Post = null;
        public const string Topic_Edit_Get = null;

        //Users
        public const string Users_General = ABSOLUTE_GENERAL_ACCESS + ", " + iHandoverAgent + ", " + Executive;

        //Websites
        public const string Websites_General = ABSOLUTE_GENERAL_ACCESS + ", " + Executive;

        //Workplace (Distinct from Workplaces)
        public const string Workplace_Controller_Create = ABSOLUTE_GENERAL_ACCESS + ", " + HRManager + ", " + Executive;
        public const string Workplace_Controller_GetInfo = ABSOLUTE_GENERAL_ACCESS + ", " + HRManager + ", " + LineManager + ", " + User + ", " + Executive;

        //Workplaces
        public const string Workplaces_General = ABSOLUTE_GENERAL_ACCESS + ", " + HRManager + ", " + Executive;

        //Workplaces for packages
        public const string Workplaces_Packages_General = ABSOLUTE_GENERAL_ACCESS + ", " + HRManager + ", " + LineManager + ", " + Executive;

        //Onboarder Tasklist Priority
        public const string Onboarder_Tasklist = ABSOLUTE_GENERAL_ACCESS + ", " + HRManager + ", " + LineManager;

        [MethodImpl(MethodImplOptions.NoInlining)]
        public static bool IsReadOnly(string callerDetails)
        {
            var myRole = AppSession.Current.CurrentUser.Role.RoleName;
            switch (myRole)
            {
                case Administrator:
                    return false;
                case HRManager:
                    return false;
                case LineManager:
                    switch (callerDetails)
                    {
                        case "Package_Edit_Get":
                            return true;
                        default:
                            return false;
                    }
                case User:
                    return false;
                case iHandoverAgent:
                    return false;
                case Executive:
                    //has readonly access to everything
                    //case/switch left in because it's a list of all the implimented ones 
                    //so far
                    return true;
                    switch (callerDetails)
                    {
                        case "Admin_Settings_Index_Get":
                            return true;
                        case "Admin_Users_Settings_Index_Get":
                            return true;
                        case "Employee_Edit_Get":
                            return true;
                        case "MasterList_Edit_Get":
                            return true;
                        case "Meeting_Edit_Get":
                            return true;
                        case "Package_Edit_Get":
                            return true;
                        case "Position_Edit_Get":
                            return true;
                        case "Relationship_Edit_Get":
                            return true;
                        case "Relationships_View_Index":
                            return true;
                        case "Task_Edit_Get":
                            return true;
                        case "Topic_Edit_Get":
                            return true;
                        case "Topic_View_Index":
                            return true;
                        case "TopicGroup_Edit_Get":
                            return true;
                        case "Workplace_Edit_Get":
                            return true;
                        default:
                            return true;
                    }
                case iHandoverAdmin:
                    return false;
                default:
                    return true;
            }
        }
    }
}