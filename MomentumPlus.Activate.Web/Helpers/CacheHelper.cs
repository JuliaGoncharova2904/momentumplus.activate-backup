﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Security;
using System.Web;
using System.Web.Caching;

namespace MomentumPlus.Activate.Web.Helpers
{
    public class CacheHelper
    {
        public static T GetCached<T>(string key, Func<T> initializer)
        {
            return GetCached(key, initializer, Cache.NoSlidingExpiration, DateTime.Now.AddMinutes(AppSettings.CacheHelperTimeoutMins));
        }

        public static T GetCached<T>(string key, Func<T> initializer, TimeSpan slidingExpiration, DateTime absoluteExpiration)
        {
            var httpContext = HttpContext.Current;

            if (httpContext != null)
            {
                var obj = httpContext.Cache[key];
                if (obj == null)
                {
                    // cache expired, create new instance
                    obj = initializer();

                    // Burn to cache
                    httpContext.Cache.Add(key, obj, null, absoluteExpiration, slidingExpiration, CacheItemPriority.Default, null);
                }
                return (T)obj;
            }
            else
            {
                // return new indtance, no cache
                return initializer();
            }
        }
    }
}