﻿Web Project Structure
=====================
The following sections describe the key folders, files and namespaces that exist within the Web project and their function. The Web project is an ASP.NET MVC application.

App_Data
--------
Default MVC folder - currently unused.

App_Start and Global.asax
-------------------------
The App_Start folder contains a number of classes used to initialise the application. The Global.asax file calls the methods within these classes when the Application_Start event is fired. The configuration classes are as follows:
* AutoMapperConfig - creates the maps between the domain models from the Entities project and the view models in the Web project.
* BundleConfig - defines the CSS and JavaScript bundles required by the front end of the application.
* DatabaseConfig - initialises the database.
* GlobalDateTime - sets the default date/time format across the application.
* LightInjectConfig - registers all service implementations with the inversion of control container (LightInject).
* WarmUp - warms up certain expensive methods in the application.
* FilterConfig, RouteConfig, WebApiConfig - MVC default configuration classes for registering routes and filters.

Areas
-----
Areas provide a way to separate a large MVC Web application into smaller functional groupings. Each area within the application corresponds to an item in the left-hand navigation on the front end, and contains it's own models, views, and controllers. Inside each area is a registration class that is executed on app start to register the area.

Attributes
----------
Contains various attributes and action filters that can be applied to controllers or models to implement a certain piece of functionality.

bower_components
----------------
Contains front-end packages that have been installed using the Bower package manager.

Content
-------
The Content folder contains static files such as style sheets (CSS files), images, and scripts (JS files).  It is organised as follows:
* common - third-party reusable components such as jQuery and Bootstrap.
* css - application-specific styles and associated images.
* img - application-specific image files.
* js - application-specific scripts.

Controllers
-----------
The Controllers namespace contains the ASP.NET MVC Controller classes that are common to many areas of the application. There is an abstract base Controller that all Controllers in the application inherit from in order to get access to certain shared functionality.

Core
----
The Core layer is responsible for the business logic associated with generating the Views. Controllers will call Core methods in order to retrieve the view models they need. Core communicates with the Services layer to retrieve and query the data.

DataAnnotations
---------------
DataAnnotations contains custom validation attributes that can be applied to view models. This custom validation will be enforced on forms that use the view model.

Documentation
-------------
Documentation for specific functionality in the application:
* CSV importer
* Live Chat Setup (deprecated)

EmailTemplates
--------------
This folder contains Mustache templates for the different types of email messages sent out by the application.

Extensions
----------
Extension methods for various types in referenced assemblies.

Faqs
----
XML representations of the application FAQs, used to generate the FAQ pages.

Filters
-------
Action filters used specifically for performance testing.

Helpers
-------
Helper classes/methods.

Hubs
----
SignalR hubs - currently only one hub for the voice messages page.

Models
------
The Models namespace houses the view models of the application. These are often mapped from domain models in the Entities project and augmented with extra data that is needed to render the Views. As with Controllers there is an abstract base model that the other models can inherit from to get access to common variables.

Scripts
-------
The Scripts folder contains the jQuery and SignalR JavaScript files installed by NuGet with SignalR.

Service References
------------------
Contains references to the following:
* Intelligent Mobile email service (deprecated)
* Intelligent Mobile SMS gateway

Views
-----
The Views folder contains the MVC Razor views that make up the front end. The folders within Views are usually named after a controller and contain the views rendered by that controller. The exception is the Shared folder which contains views common to many controllers. Within the Shared folder there are:
* DisplayTemplates - templates for rendering the HTML for a specific model.
* EditorTemplates - templates for rendering the HTML form elements for editing a specific model.

Web References
--------------
Contains a reference to the Twilio core service voice app (see Twilio docs).

_connections.config
-------------------
Contains an example connections.config - should be copied, renamed and the appropriate connection string added

bower.json
----------
Bower package manager dependencies.

Mvc.sitemap
-----------
Contains the configuration for the MVC sitemap package that is used to generate the navigation in the application.

NLog.config
-----------
Configuration for the NLog package that is used for logging throughout the application.

packages.config
---------------
NuGet packages installed in the project.

robots.txt
----------
Robots file to prevent the Momentum+ site appearing in search results.

Startup.cs
----------
Initialises SignalR

Web.config
----------
Main settings and configuration file for the ASP.NET MVC application.
