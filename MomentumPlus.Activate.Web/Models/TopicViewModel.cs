﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MomentumPlus.Activate.Web.Models.Moderation;

namespace MomentumPlus.Activate.Web.Models
{
    public class TopicViewModel : ReviewableViewModel
    {
        #region DOMAIN

        [Required]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        [Display(Name = "Handover Topic *")]
        public string Name { get; set; }

        [Display(Name = "Employee Copy Request?")]
        public bool EmployeeCopy { get; set; }

        public Guid? SourceTemplateId { get; set; }

        #endregion

        #region VIEW

        public Guid TopicGroupId { get; set; }

        public IEnumerable<SelectListItem> HandoverTopics { get; set; }

        public bool HasPrevTopic { get; set; }

        public TopicViewModel PrevTopic { get; set; }

        public bool IsPrevTopic { get; set; }

        public string LeaverName { get; set; }

        public int CurrentPrevTopic { get; set; }

        public int TotalPrevTopics { get; set; }

        public Guid? CloneVoiceMessages { get; set; }

        public Guid? CloneAttachments { get; set; }

        public Guid? CloneTasks { get; set; }

        public bool HasOpenTasks { get; set; }

        public string ModuleName { get; set; }
        public string TopicGroupName { get; set; }

        public int Subjectivity { get; set; }
        public string ParentPackageName { get; set; }

        public bool FrameworkItem { get; set; }

        public Guid? ProcessGroupId { get; set; }

        public bool IsLeavingPackageState { get; set; }

        #endregion
    }
}
