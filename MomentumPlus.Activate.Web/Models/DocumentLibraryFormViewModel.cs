﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using MomentumPlus.Activate.Web.Models.People;

namespace MomentumPlus.Activate.Web.Models
{
    public class DocumentLibraryFormViewModel : BaseViewModel
    {
        [Required]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        [Display(Name = "Name *")]
        public string Name { get; set; }

        public string Query { get; set; }
        public Guid? PositionID { get; set; }

        [Display(Name = "Positions *")]
        [Required]
        public List<Guid> PositionIDs { get; set; }
        public IEnumerable<SelectListItem> PositionSet { get; set; }

        public virtual IEnumerable<PositionViewModel> Positions { get; set; }

        public virtual AttachmentViewModel Attachment { get; set; }

        public int TasksCount { get; set; }
        public int TopicsCount { get; set; }
        public int ContactsCount { get; set; }
        public int MeetingsCount { get; set; }
        public int TriggersCount { get; set; }
        public int ProjectInstancesCount { get; set; }
        public int StaffsCount { get; set; }

        public virtual IEnumerable<TaskViewModel> Tasks { get; set; }
        public virtual IEnumerable<TopicViewModel> Topics { get; set; }
        public virtual IEnumerable<ContactViewModel> Contacts { get; set; }
        public virtual IEnumerable<MeetingViewModel> Meetings { get; set; }
        //public virtual IEnumerable<TriggerViewModel> Triggers { get; set; }
        public virtual IEnumerable<ProjectInstanceViewModel> ProjectInstances { get; set; }
        public virtual IEnumerable<StaffReportViewModel> Staffs { get; set; }

        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }

        public Guid? ItemId { get; set; }
        public ItemType? ItemType { get; set; }

        public bool Retired { get; set; }

        public IEnumerable<string> FileTypes { get; set; }
        public HttpPostedFileBase PostedFile { get; set; }

        [Display(Name = "File Location")]
        public string FilePath { get; set; }
    }
}
