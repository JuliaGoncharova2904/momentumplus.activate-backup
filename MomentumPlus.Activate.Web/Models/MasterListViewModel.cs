﻿
using MomentumPlus.Core.Models;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Activate.Web.Models
{
    public class MasterListViewModel : BaseViewModel
    {
        #region DOMAIN

        [Required]
        [StringLength(50, MinimumLength = 3)]
        [Display(Name = "Name *")]
        public string Name { get; set; }

        public MasterList.ListType Type { get; set; }

        public string ClientReference { get; set; }

        #endregion

        #region VIEW

        public string SingularListType { get; set; }

        public string AdditionalField1 { get; set; }
        public string AdditionalField2 { get; set; }
        public bool AdditionalField3 { get; set; } // Default Company

        #endregion
    }
}
