﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MomentumPlus.Activate.Web.Models.People;

namespace MomentumPlus.Activate.Web.Models
{
    public class RoleViewModel : BaseViewModel
    {
        #region DOMAIN

        [Required]
        [Display(Name = "Role *")]
        public virtual string RoleName { get; set; }

        public virtual string Description { get; set; }

        public virtual ICollection<UserViewModel> Users { get; set; }

        #endregion
    }

}
