﻿using System;
using System.Collections.Generic;
using MomentumPlus.Activate.Web.Models.Moderation;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Models
{
    public class TasksViewModel : ReviewableViewModel
    {
        public string TopicName { get; set; }
        public IEnumerable<Task> Tasks { get; set; }
        public Guid ItemID { get; set; }
        public ItemType ItemType { get; set; }
        public DateTime Start { get; set; }
    }
}
