﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MomentumPlus.Activate.Web.Models.Handover
{
    public class OnboarderPrioritiesViewModel : PrioritiesViewModel
    {

        public int CriticalDue { get; set; }

        public int RelationshipsDue { get; set; }

        public int ExperiencesDue { get; set; }

        public int ProjectsDue { get; set; }

        public int PeopleDue { get; set; }

        public bool IsLocked { get; set; }
    }
}