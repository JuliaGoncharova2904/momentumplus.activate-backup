﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Activate.Web.Models.Handover
{
    public class PeriodViewModel : BaseViewModel
    {
        [Display(Name = @"Start-Point")]
        public DateTime? StartPoint { get; set; }

        [Display(Name = @"Mid-Point")]
        public DateTime? MidPoint { get; set; }

        [Display(Name = @"Last Day")]
        public DateTime? LastDay { get; set; }

        [Display(Name = @"Handover Support Requested")]
        public bool HandoverSupportRequested { get; set; }

        public DateTime? Locked { get; set; }

        public bool IsLocked { get; set; }

        public bool IsPrioritySaved { get; set; }
    }
}