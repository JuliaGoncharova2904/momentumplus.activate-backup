﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MomentumPlus.Activate.Web.Models.Handover
{
    public class PrioritiesViewModel
    {
        public Guid HandoverId { get; set; }

        public bool ExperiancesEnabled { get; set; }

        public bool ProjectsEnabled { get; set; }

        public bool PeopleEnabled { get; set; }

        public IEnumerable<SelectListItem> PeriodOptions { get; set; }

        public bool PackageExperiencesDisabled { get; set; }

        public bool PackageProjectsDisabled { get; set; }

        public bool PackagePeopleDisabled { get; set; }
    }


}