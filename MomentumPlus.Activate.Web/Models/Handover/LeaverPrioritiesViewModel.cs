﻿using MomentumPlus.Core.Models.Handover;
using System.Collections;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MomentumPlus.Activate.Web.Models.Handover
{
    public class LeaverPrioritiesViewModel : PrioritiesViewModel
    {
        public DuePeriod CriticalDue { get; set; }

        public DuePeriod RelationshipsDue { get; set; }

        public DuePeriod ExperiencesDue { get; set; }

        public DuePeriod ProjectsDue { get; set; }

        public DuePeriod PeopleDue { get; set; }

        public DuePeriod CoreTasksDue { get; set; }

        public string UserName { get; set; }

        public bool IsLocked { get; set; }

    }
}