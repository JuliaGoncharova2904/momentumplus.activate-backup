﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Activate.Web.Models.Handover
{
    public class HandoverViewModel : BaseViewModel, IValidatableObject
    {
        /// <summary>
        /// The different types of submit buttons on the handover form.
        /// </summary>
        public enum Action
        {
            Save,
            LockLeaving,
            UnlockLeaving,
            LockOnboarding,
            UnlockOnboarding,
            LeaverPriorities,
            OnboarderPriorities
        }

        /// <summary>
        /// The action taken by the user (i.e., the submit button pressed).
        /// </summary>
        public Action SubmitAction { get; set; }

        public LeavingPeriodViewModel LeavingPeriodViewModel { get; set; }
        public OnboardingPeriodViewModel OnboardingPeriodViewModel { get; set; }

        /// <summary>
        /// Validate the handover view model.
        /// </summary>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var results = new List<ValidationResult>();

            switch (SubmitAction)
            {
                case Action.Save:
                    break;
                case Action.LockLeaving:
                    if (!LeavingPeriodViewModel.LastDay.HasValue)
                    {
                        results.Add(new ValidationResult("No last day selected for leaver.",
                            new[] {"LeavingPeriodViewModel.LastDay"}));
                    }
                    else if (LeavingPeriodViewModel.LastDay.Value.Date < DateTime.Now.Date)
                    {
                        results.Add(new ValidationResult("Last day cannot be a date in the past.",
                            new[] { "LeavingPeriodViewModel.LastDay" }));
                    }

                    if (!LeavingPeriodViewModel.IsPrioritySaved)
                    {
                        results.Add(new ValidationResult("Leaver priorities not saved.",
                            new[] { "LeavingPeriodViewModel.IsPrioritySaved" }));
                    }

                    break;
                case Action.UnlockLeaving:
                    if (OnboardingPeriodViewModel.IsLocked)
                    {
                        results.Add(
                            new ValidationResult("Cannot unlock leaving period when onboarding period is locked."));
                    }

                    break;
                case Action.LockOnboarding:
                    if (!LeavingPeriodViewModel.IsLocked)
                    {
                        results.Add(
                            new ValidationResult("Cannot lock onboarding period before leaving period."));
                    }

                    //if (!OnboardingPeriodViewModel.HandoverReportFinalised)
                    //{
                    //    results.Add(new ValidationResult("Handover report not finalised.",
                    //        new[] { "OnboardingPeriodViewModel.HandoverReportFinalised" }));
                    //}

                    //if (!OnboardingPeriodViewModel.NewProfileCreated)
                    //{
                    //    results.Add(new ValidationResult("New profile not created.",
                    //        new[] {"OnboardingPeriodViewModel.NewProfileCreated"}));
                    //}

                    if (!OnboardingPeriodViewModel.PackageId.HasValue)
                    {
                        results.Add(new ValidationResult("New profile not selected.",
                            new[] {"OnboardingPeriodViewModel.PackageId"}));
                    }

                    if (!OnboardingPeriodViewModel.Duration.HasValue)
                    {
                        results.Add(new ValidationResult("Onboard duration not selected.",
                            new[] {"OnboardingPeriodViewModel.Duration"}));
                    }

                    if (!OnboardingPeriodViewModel.StartPoint.HasValue)
                    {
                        results.Add(new ValidationResult("Onboard start-point not selected.",
                            new[] {"OnboardingPeriodViewModel.StartPoint"}));
                    }

                    if (!OnboardingPeriodViewModel.HandoverTasksFinalised)
                    {
                        results.Add(new ValidationResult("Tasks to hand over not finalised.",
                            new[] {"OnboardingPeriodViewModel.HandoverTasksFinalised"}));
                    }

                    if (!OnboardingPeriodViewModel.IsPrioritySaved)
                    {
                        results.Add(new ValidationResult("Onboarder priorities not saved.",
                            new[] { "OnboardingPeriodViewModel.IsPrioritySaved" }));
                    }

                    break;
                case Action.UnlockOnboarding:
                    break;
            }

            return results;
        }
    }
}