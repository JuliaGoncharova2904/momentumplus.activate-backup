﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Activate.Web.Models.Handover
{
    public class OnboardingPeriodViewModel : PeriodViewModel
    {
        //[Display(Name = @"Finalise Handover Report")]
        //public bool HandoverReportFinalised { get; set; }

        //[Display(Name = @"New Profile Created")]
        //public bool NewProfileCreated { get; set; }

        public int ItemsApproved { get; set; }
        public int ItemsTotal { get; set; }
        public int ItemsRejected { get; set; }

        [Display(Name = @"Profile")]
        public Guid? PackageId { get; set; }

        public IEnumerable<SelectListItem> Packages { get; set; }

        [Display(Name = @"Onboard Duration")]
        public int? Duration { get; set; }

        public IEnumerable<SelectListItem> Durations { get; set; }

        [Display(Name = @"Finalise Handover Report")]
        public bool HandoverTasksFinalised { get; set; }

        [Display(Name = @"View previous package content")]
        public bool ViewPreviousPackageContent { get; set; }

        [Display(Name = @"Carry Over Relationships")]
        public bool CarryOverRelationships { get; set; }

        [Display(Name = @"Carry Over Projects")]
        public bool CarryOverProjects { get; set; }

        public Guid LeaverPackageId { get; set; }

        public PackageViewModel Package { get; set; }
    }
}