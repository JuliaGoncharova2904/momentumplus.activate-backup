﻿using MomentumPlus.Core.Models.Handover;
using System;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Activate.Web.Models.Handover
{
    public class LeavingPeriodViewModel : PeriodViewModel
    {
        public Guid? PackageId { get; set; }

        [Display(Name = @"Profile")]
        public string PackageName { get; set; }

        [Display(Name = @"Notice Period")]
        public LeavingPeriod.Notice NoticePeriod { get; set; }

        [Display(Name = @"Exit Survey and Interview")]
        public bool ExitSurvey { get; set; }
    }
}