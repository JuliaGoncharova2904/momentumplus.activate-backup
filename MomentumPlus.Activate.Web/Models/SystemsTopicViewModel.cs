﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Activate.Web.Models
{
    public class SystemsTopicViewModel : TopicViewModel
    {
        public Guid? SystemId { get; set; }
        public string SystemIdLabel { get; set; }

        [Display(Name = "Frequency Of Use")]
        public Topic.SystemFrequencyOfUse FrequencyOfUse { get; set; }
        public string FrequencyOfUseLabel { get; set; }

        [StringLength(50, ErrorMessage = "{0} cannot be more than {1} characters long.")]
        [Display(Name = "Expert User")]
        public string ExpertUser { get; set; }
        public string ExpertUserLabel { get; set; }
        public Guid? ExpertUserId { get; set; }

        [Display(Name = "Rating")]
        public Topic.SystemImportanceRating ImportanceRating { get; set; }
        public string ImportanceRatingLabel { get; set; }

        [StringLength(2000, ErrorMessage = "{0} cannot be more than {1} characters long.")]
        [Display(Name = "Website")]
        public string WebsiteAddress { get; set; }
        public string WebsiteAddressLabel { get; set; }
        public Guid? WebsiteAddressId { get; set; }

        public IEnumerable<SelectListItem> Systems { get; set; }
        public IEnumerable<SelectListItem> ExpertUsers { get; set; }
        public IEnumerable<SelectListItem> WebsiteAddresses { get; set; }
    }
}
