﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using MomentumPlus.Activate.Web.Models.Moderation;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.Requests;

namespace MomentumPlus.Activate.Web.Models
{
    public class ProjectInstanceViewModel : ReviewableViewModel
    {
        #region DOMAIN

        [ForeignKey("PackageId")]
        public Guid PackageId { get; set; }
        public virtual Package Package { get; set; }

        public Guid? ProjectId { get; set; }
        public virtual ProjectViewModel Project { get; set; }

        public ProjectInstance.ProjectInstancePriority Priority { get; set; }
        
        [Required]
        [Display(Name = "My Role *")]
        public Guid RoleId { get; set; }
        public ProjectRoleViewModel Role { get; set; }
        public IEnumerable<SelectListItem> Roles { get; set; }

        public NewProjectRequest NewProjectRequest { get; set; }

        //[Required]
        [Display(Name = "Handover Notes")]
        public string Notes { get; set; }

        [Display(Name = @"Ready For Review?")]
        public bool ReadyForReview { get; set; }

        #endregion

        #region VIEW

        public bool AwaitingProjectManagerApproval { get; set; }

        public Task FirstTask { get; set; }

        #endregion
    }
}