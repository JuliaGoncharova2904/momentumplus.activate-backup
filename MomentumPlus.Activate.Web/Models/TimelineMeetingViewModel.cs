﻿using MomentumPlus.Activate.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MomentumPlus.Activate.Web.Models
{
    public class TimelineMeetingViewModel
    {
        public Guid ID { get; set; }
        public DateTime Start { get; set; }
        public String Name { get; set; }
        public String Location { get; set; }
        
    }
}