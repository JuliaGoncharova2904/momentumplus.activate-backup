﻿using System;
using System.Collections.Generic;
using MomentumPlus.Activate.Web.Areas.Home.Models;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.People;

namespace MomentumPlus.Activate.Web.Models
{
    public class ThingsToDoReportViewModel
    {
        // General
        public bool IsPdf { get; set; }
        public bool IsAncestor { get; set; }
        public Package Package { get; set; }

        // Introduction
        public User Pickup { get; set; }

        // Meetings
        public IEnumerable<Meeting> Meetings { get; set; }

        // Relationships
        public IDictionary<Guid, ContactChartViewModel> GraphNodes { get; set; }
        public Guid? DefaultCompanyId { get; set; }

        // Critical
        public IEnumerable<TopicGroup> CriticalTopicGroups { get; set; }

        // Experiences
        public IEnumerable<TopicGroup> ExperiencesTopicGroups { get; set; }

        // Onboarding
        public IEnumerable<Task> Tasks { get; set; }

        // Onboarder Review Date - Critical Module
        public DateTime? OnboarderReviewDateCritical { get; set; }

        // Onboarder Review Date - Experiences Module
        public DateTime? OnboarderReviewDateExperiences { get; set; }

        public DateTime? OnboarderReviewDateProjects { get; set; }

        public DateTime? OnboarderReviewDatePeople { get; set; }

        public bool IsLeavingPeriodLocked { get; set; }
        public bool IsOnboardingPeriodLocked { get; set; }
    }
}