﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Security;

namespace MomentumPlus.Activate.Web.Models
{
    /// <summary>
    /// Base view model that all other view models
    /// in the application are to inherit from.
    /// </summary>
    public abstract class BaseViewModel
    {
        #region DOMAIN

        public Guid ID { get; set; }

        public Guid? SourceId { get; set; }

        public DateTime Created { get; set; }

        public DateTime? Modified { get; set; }

        public DateTime? UserLastViewed { get; set; }

        public DateTime? Deleted { get; set; }

        [Display(Name = "Notes")]
        [StringLength(2000, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Notes { get; set; }

        public virtual int VoiceMessagesCount { get; set; }
        public virtual int AttachmentsCount { get; set; }
        public virtual int DocumentLibrariesCount { get; set; }
        public virtual int TasksCount { get; set; }
        public virtual int MeetingsCount { get; set; }

        public MembershipUser currentUser { get; set; }

        [Display(Name = "Search Tags")]
        [StringLength(500, MinimumLength = 2, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string SearchTags { get; set; }

        public IEnumerable<Task> Tasks { get; set; }

        public IEnumerable<MeetingViewModel> Meetings { get; set; } 

        #endregion

        #region VIEW

        public bool ReadOnly { get; set; }

        public bool CreateMode
        {
            get
            {
                return this.ID == default(Guid);
            }
        }

        public int NotesWordCount { get; set; }

        public int NotesSubjectivity { get; set; }

        public string UserOwnerName { get; set; }

        #endregion
    }
}
