﻿using System;

namespace MomentumPlus.Activate.Web.Models.Home
{
    public class InUseOnboarderItemViewModel
    {
        public enum SortColumn
        {
            NameOnboarder, NameExit, LastUpdated, ApprovedLineManager, ApprovedHRManager, ApprovedUser
        }

        public string NameOnboarder { get; set; }
        public string NameExit { get; set; }
        public int Subjectivity { get; set; }
        public DateTime? LastUpdated { get; set; }
        public bool ApprovedLineManager { get; set; }
        public bool ApprovedHRManager { get; set; }

        public bool CurrentUserIsHr { get; set; }
        public bool CurrentUserIsLm { get; set; }

        public Guid PackageId { get; set; }
        public bool HasTrigger { get; set; }
    }
}