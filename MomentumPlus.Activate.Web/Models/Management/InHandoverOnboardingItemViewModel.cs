﻿using System;

namespace MomentumPlus.Activate.Web.Models.Home
{
    public class InHandoverOnboardingItemViewModel
    {
        public enum SortColumn
        {
            NameNewStart, Started
        }

        public string NameNewStart { get; set; }

        public DateTime Started { get; set; }

        public bool CurrentUserIsHr { get; set; }
        public bool CurrentUserIsLm { get; set; }

        public Guid PackageId { get; set; }
    }
}