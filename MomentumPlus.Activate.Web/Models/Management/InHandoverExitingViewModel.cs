﻿using System.Collections.Generic;

namespace MomentumPlus.Activate.Web.Models.Home
{
    public class InHandoverExitingViewModel
    {
        public IEnumerable<InHandoverExitingItemViewModel> Items { get; set; }
        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public bool SortAscending { get; set; }
    }
}