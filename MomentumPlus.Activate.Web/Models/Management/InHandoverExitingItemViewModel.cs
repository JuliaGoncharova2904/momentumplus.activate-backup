﻿using System;

namespace MomentumPlus.Activate.Web.Models.Home
{
    public class InHandoverExitingItemViewModel
    {
        public enum SortColumn
        {
            NameOnboarder, Finish, DaysToGo, StatusCompletePercent, NameExit, AverageContent, Subjectivity
        }

        public string NameOnboarder { get; set; }
        public DateTime? Finish { get; set; }
        public string DaysToGo { get; set; }
        public int StatusCompletePercent { get; set; }
        public int NumberOfModules { get; set; }
        public int NumberOfModulesComplete { get; set; }
        public int AverageContent { get; set; }
        public int Subjectivity { get; set; }
        public string NameExit { get; set; }

        public bool CurrentUserIsHr { get; set; }
        public bool CurrentUserIsLm { get; set; }

        public Guid PackageId { get; set; }
    }
}