﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Activate.Web.Models
{
    public class TopicFieldViewModel : BaseViewModel
    {
        public virtual TopicQuestionViewModel Question { get; set; }

        public Guid TopicQuestionId { get; set; }

        public virtual ICollection<TopicAnswerViewModel> Answers { get; set; }
    }
}
