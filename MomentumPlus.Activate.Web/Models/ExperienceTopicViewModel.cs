﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Activate.Web.Models
{
    public class ExperienceTopicViewModel : TopicViewModel
    {
        [Display(Name = "When")]
        public DateTime? When { get; set; }
        public string WhenLabel { get; set; }

        [StringLength(50, ErrorMessage = "{0} cannot be more than {1} characters long.")]
        [Display(Name = "Contact")]
        public string KeyContact { get; set; }
        public string KeyContactLabel { get; set; }
        public Guid? KeyContactId { get; set; }

        public IEnumerable<SelectListItem> KeyContacts { get; set; }
    }
}