﻿using System.Collections.Generic;

namespace MomentumPlus.Activate.Web.Models
{
    public class DocumentReportViewModel : BaseViewModel
    {
        public enum SortColumn
        {
            FirstName, LastName, Position, Workplace
        }

        public IEnumerable<DocumentLibraryFormViewModel> Documents { get; set; }
    }
}
