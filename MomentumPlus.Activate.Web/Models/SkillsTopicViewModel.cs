﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Activate.Web.Models
{
    public class SkillsTopicViewModel : TopicViewModel
    {
        [Display(Name = "Training Frequency")]
        public Topic.TrainingFrequency TrainingFrequency { get; set; }
        public string TrainingFrequencyLabel { get; set; }

        [StringLength(50, ErrorMessage = "{0} cannot be more than {1} characters long.")]
        [Display(Name = "Contact")]
        public string Contact { get; set; }
        public string ContactLabel { get; set; }
        public Guid? ContactId { get; set; }

        [Display(Name = "Perceived Standard of Training")]
        public Topic.TrainingStandard TrainingStandard { get; set; }
        public string TrainingStandardLabel { get; set; }

        [Display(Name = "Training Required")]
        public bool TrainingRequired { get; set; }
        public string TrainingRequiredLabel { get; set; }

        public IEnumerable<SelectListItem> Contacts { get; set; }
    }
}
