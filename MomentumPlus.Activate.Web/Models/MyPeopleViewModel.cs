﻿using MomentumPlus.Activate.Web.Models.Home;

namespace MomentumPlus.Activate.Web.Models
{
    public class MyPeopleViewModel
    {
        public TriggersViewModel Unapproved { get; set; }
        public AgedViewModel Aged { get; set; }
    }
}