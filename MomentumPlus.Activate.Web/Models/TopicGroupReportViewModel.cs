﻿using MomentumPlus.Core.Models;
using System;

namespace MomentumPlus.Activate.Web.Models
{
    public class TopicGroupReportViewModel : BaseViewModel
    {
        #region DOMAIN

        public virtual TopicGroup.TopicType TopicGroupType { get; set; }
        public string TopicGroupName { get; set; }
        public string TopicGroupTitle { get; set; }
        public string TopicName { get; set; }
        public bool TopicHasAudio { get; set; }
        public bool TopicHasAttachments { get; set; }
        public int TopicAttachmentsCount { get; set; }
        public int TopicAudioCount { get; set; }
        public Guid TopicId { get; set; }
        public string TopicNotes { get; set; }

        #endregion
    }
}
