﻿using MomentumPlus.Core.Models.Requests;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MomentumPlus.Activate.Web.Models
{
    public class ApprovalsViewModel
    {
        public int ItemsForApproval { get; set; }

        public Guid? PackageId { get; set; }
        public IEnumerable<SelectListItem> Packages { get; set; }

        public IEnumerable<NewProjectRequest> NewProjectRequests { get; set; }
        public IEnumerable<ProjectInstanceTransferRequest> ProjectInstanceTransferRequests { get; set; }
        public IEnumerable<ProjectInstanceRoleChangeRequest> ProjectInstanceRoleChangeRequests { get; set; }
        public IEnumerable<StaffTransferRequest> StaffTransferRequests { get; set; }
    }
}