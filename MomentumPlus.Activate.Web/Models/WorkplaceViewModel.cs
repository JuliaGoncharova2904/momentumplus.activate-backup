﻿using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Activate.Web.Models
{
    public class WorkplaceViewModel : BaseViewModel
    {
        [Required]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        [Display(Name = "Name *")]
        public string Name { get; set; }

        [StringLength(750)]
        public string Address { get; set; }

        [Display(Name = "Map Link")]
        [StringLength(2000)]
        public string MapLink { get; set; }
    }
}
