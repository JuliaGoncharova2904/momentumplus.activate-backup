﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web;
using MomentumPlus.Activate.Web.DataAnnotations;

namespace MomentumPlus.Activate.Web.Models
{
    public class VoiceMessageViewModel : BaseViewModel
    {
        [Required]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        [Display(Name = "Name *")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Length *")]
        public int DurationSeconds { get; set; }

        [Required]
        [Display(Name = "File ID *")]
        public Guid FileId { get; set; }

        [Required]
        [Display(Name = "Tags *")]
        public string Tags { get; set; }

        [Display(Name = "Selected File *")]
        [FileTypes("mp3,wav,wma,m4a,ogg")]
        public HttpPostedFileBase PostedFile { get; set; }

        public string CoreService_Pin { get; set; }
        public string TwilioToken { get; set; }

        public Guid ItemId { get; set; }
        public ItemType ItemType { get; set; }

        public bool HasExistingData { get; set; }

        public bool RequiresNotification { get; set; }

        public int CreationMethod { get; set; }
    }
}
