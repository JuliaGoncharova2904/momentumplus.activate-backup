﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MomentumPlus.Activate.Web.Models
{
    public class TopicConfirmationViewModel
    {
        public Guid TopicId { get; set; }

        public string OldName { get; set; }

        [Display(Name = "New Name")]
        public string NewName { get; set; }

        public Guid TopicGroupId { get; set; }

        public string Action { get; set; }

    }
}