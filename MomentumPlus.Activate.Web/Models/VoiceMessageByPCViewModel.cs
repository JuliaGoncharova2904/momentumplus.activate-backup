﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Activate.Web.Models
{
    public class VoiceMessageByPCViewModel : BaseViewModel
    {
        [Required]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        [Display(Name = "Name *")]
        public string Name { get; set; }

        public Guid ItemId { get; set; }
        public ItemType ItemType { get; set; }

        public string accountSid { get; set; }
        public string authToken { get; set; }
        public string AppSid { get; set; }

        public string pinNumber { get; set; }

        public IEnumerable<VoiceMessageViewModel> VoiceMessages { get; set; }


        public string TwilioToken { get; set; }
    }
}