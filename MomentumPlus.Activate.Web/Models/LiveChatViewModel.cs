﻿using System.Collections.Generic;

namespace MomentumPlus.Activate.Web.Models
{
    public class LiveChatViewModel
    {
        public string ChatService { get; set; }
        public IEnumerable<string> ChatRooms { get; set; }
        public string ServerName { get; set; }
        public string Nickname { get; set; }
    }
}