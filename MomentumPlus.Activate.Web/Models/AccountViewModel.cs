﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Models
{
    public class AccountConfigViewModel : BaseViewModel
    {

        public IEnumerable<SelectListItem> EarliestStartWeekListItems { get; set; }

        public IEnumerable<SelectListItem> FiscalYearStartMonthListItems { get; set; }

        public string Action { get; set; }

        #region Twilio
            
        [Display(Name = "Twilio Account")]
        public string TwilioAccount { get; set; }

        [Display(Name = "Twilio Endpoint")]
        public string TwilioEndpoint { get; set; }

        [Display(Name = "Twilio Phone Number")]
        public string TwilioPhoneNumber { get; set; }

        [Display(Name = "SMS Gateway")]
        public string SmsGateway { get; set; }

        [Display(Name = "SMS Per User Per Day")]
        public string NumberSMSPerUserPerDay { get; set; }

        [Display(Name = "Alternative SMS Gateway")]
        public string AlternativeSmsGateway { get; set; }

        [Display(Name = "SMS On")]
        public bool SmsOn { get; set; }

        [Display(Name = "Alternative SMS On")]
        public bool AlternativeSmsOn { get; set; }

        #endregion

        #region Client Settings

        [Display(Name = "Tenant Name (friendly name of the organisation)")]
        public string TenantFriendlyName { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "{0} must be greater than or equal to {1}.")]
        [Display(Name = "Days Until Medium Task Alert")]
        public int TaskAlertDaysMedium { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "{0} must be greater than or equal to {1}.")]
        [Display(Name = "Days Until High Task Alert")]
        public int TaskAlertDaysHigh { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "{0} must be greater than or equal to {1}.")]
        [Display(Name = "Days Until Medium Topic Alert")]
        public int TopicAlertDaysMedium { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "{0} must be greater than or equal to {1}.")]
        [Display(Name = "Days Until High Topic Alert")]
        public int TopicAlertDaysHigh { get; set; }

        [Display(Name = "Allowed File Types for Attachments")]
        public string FileExtensions { get; set; }

        [Display(Name = "Target package size (Volume)")]
        public int TargetVolume { get; set; }
        [Display(Name = "Target package average wordcount")]
        public int TargetAverageWordcount { get; set; }
        [Display(Name = "Target package attachments")]
        public int TargetAttachments { get; set; }

        [Display(Name = "Project ID Format")]
        public string ProjectIdFormat { get; set; }

        [Display(Name = "Company Logo")]
        //[FileExtensions(Extensions = "png,jpeg,jpg", ErrorMessage = "Allowed file extensions are .png, .jpeg and .jpg")]
        //[ImageSize(400, 150)]
        public HttpPostedFileBase UploadedLogo { get; set; }

        public Image Logo { get; set; }

        public bool RemoveLogo { get; set; }
        
        [Display(Name = "Handover Consultant")]
        [EmailAddress]
        [Required]
        public string HandoverConsultant { get; set; }

        [Display(Name = @"Can Users Add Internal Contacts?")]
        [Required]
        public bool UserInternalContacts { get; set; }

        [Range(1, 3, ErrorMessage = "Earliest start week must be between {0} and {1}.")]
        [Display(Name = "Earliest Start Week")]
        public int EarliestStartWeek { get; set; }

        [Range(1, 12)]
        [Display(Name = "Fiscal Year Begins")]
        public int FiscalYearStartMonth { get; set; }

        [Display(Name = "Technical Support")]
        public string TechnicalSupport { get; set; }
        

        #endregion

        #region Version Details

        public int TemplatesCount { get; set; }
        public int PackagesCount { get; set; }

        [Display(Name = "Purchase Date")]
        public DateTime? PurchaseDate { get; set; }

        [Display(Name = "Licence End Date")]
        public DateTime? LicenceEndDate { get; set; }

        [Display(Name = "Process Support Tier")]
        public AccountConfig.SupportTier ProcessSupportTier { get; set; }

        [Display(Name = "Maximum Templates")]
        public int? TemplateLimit { get; set; }

        [Display(Name = "Maximum Packs")]
        public int? PackageLimit { get; set; }

        [Display(Name = "iHandover Account Manager")]
        public string AccountManager { get; set; }

        public string ProjectVersion { get; set; }

        [Display(Name = "Enable Pro-Link Mode")]
        public bool ProLinkEnabled { get; set; }

        #endregion

        #region Tooltips

        //tooltips
        [Display(Name = "Summary To Do Menu")]
        public string SummaryToDoMenu { get; set; }
        [Display(Name = "Summary Progess Menu")]
        public string SummaryProgessMenu { get; set; }

        [Display(Name = "Essentials Business Process Menu")]
        public string CriticalBusinessProcessMenu { get; set; }
        [Display(Name = "Essentials Systems Menu")]
        public string CriticalSystemsMenu { get; set; }
        [Display(Name = "Essentials Skills Training Menu")]
        public string CriticalSkillsTrainingMenu { get; set; }
        [Display(Name = "Essentials Tools Equipment Menu")]
        public string CriticalToolsEquipMenu { get; set; }
        [Display(Name = "Essentials Projects Menu")]
        public string CriticalProjectsMenu { get; set; }
        [Display(Name = "Essentials Strategic Aspects Menu")]
        public string CriticalStrategicAspectsMenu { get; set; }
        [Display(Name = "Essentials Regulatory Compliance Menu")]
        public string CriticalRegulatoryComplianceMenu { get; set; }
        [Display(Name = "Essentials Anything Else Menu")]
        public string CriticalAnythingElseMenu { get; set; }

        [Display(Name = "Relationship Add New Menu")]
        public string RelationshipAddNewMenu { get; set; }
        [Display(Name = "Relationship Intro Schedule Menu")]
        public string RelationshipIntroScheduleMenu { get; set; }

        [Display(Name = "Achievements Three Best Things Header")]
        public string ExperiencesThreeBestThingsHeader { get; set; }
        [Display(Name = "Achievements Not So Good Exp Menu")]
        public string ExperiencesNotSoGoodexpMenu { get; set; }
        [Display(Name = "Achievements Team Acheivements Menu")]
        public string ExperiencesTeamAcheivementsMenu { get; set; }
        [Display(Name = "Achievements Other Things Menu")]
        public string ExperiencesOtherThingsMenu { get; set; }
        [Display(Name = "Achievements Achievements Made Menu")]
        public string ExperiencesAchievementsMadeMenu { get; set; }
        [Display(Name = "Achievements Need to Change Menu")]
        public string ExperiencesNeedtoChangeMenu { get; set; }

        [Display(Name = "Topic Title Header")]
        public string TopicTitleHeader { get; set; }
        [Display(Name = "Topic Item Type Topic Menu")]
        public string TopicItemTypeTopicMenu { get; set; }
        [Display(Name = "Topic Notes Menu")]
        public string TopicNotesMenu { get; set; }
        [Display(Name = "Topic Voice Messages Menu")]
        public string TopicVoiceMessagesMenu { get; set; }
        [Display(Name = "Topic Attachments Menu")]
        public string TopicAttachmentsMenu { get; set; }
        [Display(Name = "Topic Process Interaction Menu")]
        public string TopicProcessInteractionMenu { get; set; }
        [Display(Name = "Topic Inhouse Trainer Menu")]
        public string TopicInhouseTrainerMenu { get; set; }
        [Display(Name = "Topic Fit For Purpose Menu")]
        public string TopicFitForPurposeMenu { get; set; }
        [Display(Name = "Topic Linked Project Menu")]
        public string TopicLinkedProjectMenu { get; set; }
        [Display(Name = "Topic Search Tags Menu")]
        public string TopicSearchTagsMenu { get; set; }
        [Display(Name = "Topic Tasks Menu")]
        public string TopicTasksMenu { get; set; }
        [Display(Name = "Topic Employee Copy Request Menu")]
        public string TopicEmployeeCopyRequestMenu { get; set; }
        [Display(Name = "Topic Ready for Review Menu")]
        public string TopicReadyforReviewMenu { get; set; }
        [Display(Name = "Topic Line Manager Approval Menu")]
        public string TopicLineManagerApprovalMenu { get; set; }
        [Display(Name = "Topic HR Manager Approval Menu")]
        public string TopicHRManagerApprovalMenu { get; set; }

        //topic system form
        [Display(Name = "TopicSystemFrequencyOfUse")]
        public string TopicSystemFrequencyOfUse { get; set; }
        [Display(Name = "TopicSystemExpertUser")]
        public string TopicSystemExpertUser { get; set; }
        [Display(Name = "TopicSystemImportanceRating")]
        public string TopicSystemImportanceRating { get; set; }
        [Display(Name = "TopicSystemWebsiteAddress")]
        public string TopicSystemWebsiteAddress { get; set; }

        //topic future training form
        [Display(Name = "TopicFutureTrainingTrainingFrequency")]
        public string TopicFutureTrainingTrainingFrequency { get; set; }
        [Display(Name = "TopicFutureTrainingInHouseTrainer")]
        public string TopicFutureTrainingInHouseTrainer { get; set; }
        [Display(Name = "TopicTrainingRequired")]
        public string TopicTrainingRequired { get; set; }

        //topic equipment form
        [Display(Name = "TopicEquipmentFrequencyOfUse")]
        public string TopicEquipmentFrequencyOfUse { get; set; }
        [Display(Name = "TopicEquipmentConditionOfEquipment")]
        public string TopicEquipmentConditionOfEquipment { get; set; }
        [Display(Name = "TopicEquipmentAchievementContact")]
        public string TopicEquipmentExperiencedContact { get; set; }

        //topic project
        [Display(Name = "TopicProjectProjecctID")]
        public string TopicProjectProjecctID { get; set; }
        [Display(Name = "TopicProjectProjectSponsor")]
        public string TopicProjectProjectSponsor { get; set; }
        [Display(Name = "TopicProjectProjectManager")]
        public string TopicProjectProjectManager { get; set; }
        [Display(Name = "TopicProjectBudgetSpend")]
        public string TopicProjectBudgetSpend { get; set; }
        [Display(Name = "TopicProjectProjectStatus")]
        public string TopicProjectProjectStatus { get; set; }
        [Display(Name = "TopicProjectPerceivedSuccess")]
        public string TopicProjectPerceivedSuccess { get; set; }
        [Display(Name = "TopicProjectProjectEndDate")]
        public string TopicProjectProjectEndDate { get; set; }

        //topic strategic aspects
        [Display(Name = "TopicStrategicAspectsReviewFrequency")]
        public string TopicStrategicAspectsReviewFrequency { get; set; }
        [Display(Name = "TopicStrategicAspectsQuality")]
        public string TopicStrategicAspectsQuality { get; set; }

        //topic RegulatoryAndCompliance
        [Display(Name = "TopicRegCompDocumentName")]
        public string TopicRegCompDocumentName { get; set; }
        [Display(Name = "TopicRegCompDocumentLocation")]
        public string TopicRegCompDocumentLocation { get; set; }
        [Display(Name = "TopicRegCompPrimaryContact")]
        public string TopicRegCompPrimaryContact { get; set; }
        [Display(Name = "TopicRegCompWebsiteAddress")]
        public string TopicRegCompWebsiteAddress { get; set; }

        //topic location and environment
        [Display(Name = "TopicLocEnvLocation")]
        public string TopicLocEnvLocation { get; set; }
        [Display(Name = "TopicLocEnvAddress")]
        public string TopicLocEnvAddress { get; set; }
        [Display(Name = "TopicLocEnvMapLink")]
        public string TopicLocEnvMapLink { get; set; }

        //topic  experiences
        [Display(Name = "TopicAchievementsWhat")]
        public string TopicExperiencesWhat { get; set; }
        [Display(Name = "TopicAchievementsWhen")]
        public string TopicExperiencesWhen { get; set; }
        [Display(Name = "TopicAchievementsKeyContact")]
        public string TopicExperiencesKeyContact { get; set; }


        [Display(Name = "Tasks Core Tasks Core TasksMenuHeader")]
        public string TasksCoreTasksCoreTasksMenuHeader { get; set; }
        [Display(Name = "Tasks Core Tasks Additional Tasks Menu")]
        public string TasksCoreTasksAdditionalTasksMenu { get; set; }
        [Display(Name = "Tasks Other Tasks Essentials Tasks Menu")]
        public string TasksOtherTasksCriticalTasksMenu { get; set; }
        [Display(Name = "Tasks Other Tasks Project Tasks Menu")]
        public string TasksOtherTasksProjectTasksMenu { get; set; }
        [Display(Name = "Tasks Other Tasks Relationship Tasks Menu")]
        public string TasksOtherTasksRelationshipTasksMenu { get; set; }
        [Display(Name = "Tasks Other Tasks Achievement Tasks Menu")]
        public string TasksOtherTasksExperienceTasksMenu { get; set; }

        [Display(Name = "Create Task Form Menu")]
        public string CreateTaskFormMenu { get; set; }
        [Display(Name = "Create Task From Topic")]
        public string CreateTaskFromTopic { get; set; }
        [Display(Name = "Create Task From Name")]
        public string CreateTaskFromName { get; set; }
        [Display(Name = "Create Task From Handover")]
        public string CreateTaskFromHandover { get; set; }
        [Display(Name = "Create Task From Notes")]
        public string CreateTaskFromNotes { get; set; }
        [Display(Name = "Create Task From Voice Messages")]
        public string CreateTaskFromVoiceMessages { get; set; }
        [Display(Name = "Create Task From Attachments")]
        public string CreateTaskFromAttachments { get; set; }
        [Display(Name = "Create Task From Week Due")]
        public string CreateTaskFromWeekDue { get; set; }
        [Display(Name = "Create Task From Fixed Date/Time")]
        public string CreateTaskFromFixedDateTime { get; set; }
        [Display(Name = "Create Task From Priority")]
        public string CreateTaskFromPriority { get; set; }
        [Display(Name = "Create Task From Search Tags")]
        public string CreateTaskFromSearchTags { get; set; }
        [Display(Name = "Create Task From Ready For Review")]
        public string CreateTaskFromReadyForReview { get; set; }
        [Display(Name = "Create Task From LineManager Approval")]
        public string CreateTaskFromLineManagerApproval { get; set; }
        [Display(Name = "Create Task From HRManager Approval")]
        public string CreateTaskFromHRManagerApproval { get; set; }

        //Triggers
        [Display(Name = "Handover Header")]
        public string TriggerHeader { get; set; }
        [Display(Name = "Handover Leaver Menu")]
        public string TriggerLeaverMenu { get; set; }
        [Display(Name = "Handover Set Menu")]
        public string TriggerTriggerMenu { get; set; }
        [Display(Name = "Handover Finish Date Menu")]
        public string TriggerFinishDateMenu { get; set; }
        [Display(Name = "Handover Pick Up Menu")]
        public string TriggerPickUpMenu { get; set; }
        [Display(Name = "Handover Duration Menu")]
        public string TriggerDurationMenu { get; set; }
        [Display(Name = "Handover Notes Menu")]
        public string TriggerNotesMenu { get; set; }
        [Display(Name = "Handover Key Contact Menu")]
        public string TriggerKeyContactMenu { get; set; }

        [Display(Name = "Audio Manager Link Button")]
        public string AudioManagerLinkButton { get; set; }
        [Display(Name = "Voice Message Manager Link Button")]
        public string VoiceMessageManagerLinkButton { get; set; }

        [Display(Name = "Audio Manager Title Header")]
        public string AudioManagerTitleHeader { get; set; }
        [Display(Name = "Audio Manager Title Right Header")]
        public string AudioManagerTitleRightHeader { get; set; }
        [Display(Name = "Audio Manager Audio Player Header")]
        public string AudioManagerAudioPlayerHeader { get; set; }
        [Display(Name = "Audio Manager Record from Device Button")]
        public string AudioManagerRecordfromPCButton { get; set; }
        [Display(Name = "Audio Manager Upload Button")]
        public string AudioManagerUploadButton { get; set; }
        [Display(Name = "Audio Manager Use Phone Button")]
        public string AudioManagerUsePhoneButton { get; set; }

        [Display(Name = "File Uploader Name Menu")]
        public string FileUploaderNameMenu { get; set; }
        [Display(Name = "File Uploader Search Tags Menu")]
        public string FileUploaderSearchTagsMenu { get; set; }
        [Display(Name = "File Uploader Select File Menu")]
        public string FileUploaderSelectFileMenu { get; set; }

        [Display(Name = "Attachment Manager Title Menu")]
        public string AttachmentManagerTitleMenu { get; set; }
        [Display(Name = "Attachment Manager Title Right Menu")]
        public string AttachmentManagerTitleRightMenu { get; set; }
        [Display(Name = "Attachment Manager Upload Menu")]
        public string AttachmentManagerUploadMenu { get; set; }

        //my people
        [Display(Name = "People Live Packs Header")]
        public string MyPeopleLivePackagesHeader { get; set; }
        [Display(Name = "People Aged Summary Header")]
        public string MyPeopleAgedSummaryHeader { get; set; }

        //profile
        [Display(Name = "Profile Header")]
        public string ProfileHeader { get; set; }
        [Display(Name = "Profile Surname Menu")]
        public string ProfileSurnameMenu { get; set; }
        [Display(Name = "Profile First Name Menu")]
        public string ProfileFirstNameMenu { get; set; }
        [Display(Name = "Profile DirectDial Menu")]
        public string ProfileDirectDialMenu { get; set; }
        [Display(Name = "Profile Mobile Menu")]
        public string ProfileMobileMenu { get; set; }
        [Display(Name = "Profile LoginEmail Menu")]
        public string ProfileLoginEmailMenu { get; set; }
        [Display(Name = "Profile Password Menu")]
        public string ProfilePasswordMenu { get; set; }
        [Display(Name = "Profile Notes Menu")]
        public string ProfileNotesMenu { get; set; }

        //MI Dashboard
        [Display(Name = "IM Dashboard In Use Header")]
        public string IMDashboardInUseHeader { get; set; }
        [Display(Name = "In Handover Ongoing Header")]
        public string InHandoverExitingHeader { get; set; }
        [Display(Name = "In Handover Leaving and Onboarding Header")]
        public string InHandoverOnboardingHeader { get; set; }

        // Framework - MasterLists
        [Display(Name = "Master Lists Systems Header")]
        public string MasterListsSystemsHeader { get; set; }
        [Display(Name = "Master Lists Expert Users Header")]
        public string MasterListsExpertUsersHeader { get; set; }
        [Display(Name = "Master Lists Organisation Contacts Header")]
        public string MasterListsOrganisationContactsHeader { get; set; }
        [Display(Name = "Master Lists Projects Header")]
        public string MasterListsProjectsHeader { get; set; }
        [Display(Name = "Master Lists Websites Header")]
        public string MasterListsWebsitesHeader { get; set; }
        [Display(Name = "Master Lists Assets Header")]
        public string MasterListsAssetsHeader { get; set; }
        [Display(Name = "Master Lists Companies Header")]
        public string MasterListsCompaniesHeader { get; set; }
        [Display(Name = "Master Lists Search Tags Header")]
        public string MasterListsSearchTagsHeader { get; set; }
        [Display(Name = "Master Lists Subjectivity Header")]
        public string MasterListsSubjectivityHeader { get; set; }

        // Framework - Modules 
        [Display(Name = "Modules Topic Group Header")]
        public string ModulesTopicGroupHeader { get; set; }
        [Display(Name = "Modules Topics Header")]
        public string ModulesTopicsHeader { get; set; }
        [Display(Name = "Modules Tasks Header")]
        public string ModulesTasksHeader { get; set; }
        [Display(Name = "Modules Relationship Diagram Header")]
        public string ModulesRelationshipDiagramHeader { get; set; }
        [Display(Name = "Modules Core Tasks Header")]
        public string ModulesCoreTasksHeader { get; set; }

        //Framework Template
        [Display(Name = "Template Topic Group Header")]
        public string TemplateTopicGroupHeader { get; set; }
        [Display(Name = "Template Topics Header")]
        public string TemplateTopicsHeader { get; set; }
        [Display(Name = "Template Tasks Header")]
        public string TemplateTasksHeader { get; set; }
        [Display(Name = "Template Relationship Diagram Header")]
        public string TemplateRelationshipDiagramHeader { get; set; }

        //Import Data
        [Display(Name = "Import Import Data Header")]
        public string ImportImportDataHeader { get; set; }

        //Packs
        [Display(Name = "Packs Workplaces Header")]
        public string PackagesWorkplacesHeader { get; set; }
        [Display(Name = "Packs Packs Header")]
        public string PackagesPackagesHeader { get; set; }
        [Display(Name = "Packs Positions Header")]
        public string PackagesPositionsHeader { get; set; }
        [Display(Name = "Packs Employees Header")]
        public string PackagesEmployeesHeader { get; set; }

        //Document Lib
        [Display(Name = "Document Library Documents Header")]
        public string DocumentLibraryDocumentsHeader { get; set; }

        //Reports
        [Display(Name = "Reports Main Report Header")]
        public string ReportsMainReportHeader { get; set; }
        [Display(Name = "Reports Additional Report Header")]
        public string ReportsAdditionalReportHeader { get; set; }

        // Project Card
        [Display(Name = "Edit Project Card - Header")]
        public string ProjectCardFormHeader { get; set; }
        [Display(Name = "Edit Project Card - My Role")]
        public string ProjectCardFormMyRole { get; set; }
        [Display(Name = "Edit Project Card - Handover Notes")]
        public string ProjectCardFormHandoverNotes { get; set; }
        [Display(Name = "Edit Project Card - Priority")]
        public string ProjectCardFormPriority { get; set; }

        // Project Form
        [Display(Name = "Project Form - Header")]
        public string ProjectFormHeader { get; set; }
        [Display(Name = "Project Form - Project Name")]
        public string ProjectFormName { get; set; }
        [Display(Name = "Project Form - Project ID")]
        public string ProjectFormProjectId { get; set; }
        [Display(Name = "Project Form - Start Date")]
        public string ProjectFormStart { get; set; }
        [Display(Name = "Project Form - End Date")]
        public string ProjectFormEnd { get; set; }
        [Display(Name = "Project Form - Project Manager")]
        public string ProjectFormProjectManager { get; set; }
        [Display(Name = "Project Form - Image")]
        public string ProjectFormImage { get; set; }
        [Display(Name = "Project Form - Status")]
        public string ProjectFormStatus { get; set; }
        [Display(Name = "Project Form - Budget")]
        public string ProjectFormBudget { get; set; }
        [Display(Name = "Project Form - Project Space/Folder Location")]
        public string ProjectFormProjectSpaceLocation { get; set; }
        [Display(Name = "Project Form - Description")]
        public string ProjectFormDescription { get; set; }
        [Display(Name = "Project Form - My Role")]
        public string ProjectFormMyRole { get; set; }
        [Display(Name = "Project Form - Handover Notes")]
        public string ProjectFormHandoverNotes { get; set; }

        // Project Transfer Form
        [Display(Name = "Project Transfer Form - Source Package")]
        public string ProjectTransferFormSourcePackage { get; set; }
        [Display(Name = "Project Transfer Form - Source Project")]
        public string ProjectTransferFormSourceProject { get; set; }
        [Display(Name = "Project Transfer Form - Target Package")]
        public string ProjectTransferFormTargetPackage { get; set; }

        // Moderation Form
        [Display(Name = "Approval Form - Line Manager")]
        public string ModerationFormLineManagerDecision { get; set; }
        [Display(Name = "Approval Form - HR Manager")]
        public string ModerationFormHrManagerDecision { get; set; }

        // Person Form
        [Display(Name = "Person Form - Title")]
        public string PersonFormTitle { get; set; }
        [Display(Name = "Person Form - First Name")]
        public string PersonFormFirstName { get; set; }
        [Display(Name = "Person Form - Last Name")]
        public string PersonFormLastName { get; set; }
        [Display(Name = "Person Form - Profile Image")]
        public string PersonFormImage { get; set; }
        [Display(Name = "Person Form - Company")]
        public string PersonFormCompany { get; set; }
        [Display(Name = "Person Form - Position")]
        public string PersonFormPosition { get; set; }
        [Display(Name = "Person Form - Email")]
        public string PersonFormEmail { get; set; }
        [Display(Name = "Person Form - Phone")]
        public string PersonFormPhone { get; set; }
        [Display(Name = "Person Form - Direct/Indirect")]
        public string PersonFormDirect { get; set; }
        [Display(Name = "Person Form - Importance")]
        public string PersonFormImportance { get; set; }
        [Display(Name = "Person Form - Notes")]
        public string PersonFormNote { get; set; }

        // Staff Form
        [Display(Name = "Person Form - Header")]
        public string StaffFormHeader { get; set; }
        [Display(Name = "Person Form - Momentum+ User")]
        public string StaffFormUser { get; set; }
        [Display(Name = "Person Form - Search Tags")]
        public string StaffFormSearchTags { get; set; }
        [Display(Name = "Person Form - Employment Status")]
        public string StaffFormEmploymentStatus { get; set; }
        [Display(Name = "Person Form - Future Training")]
        public string StaffFormFutureTraining { get; set; }
        [Display(Name = "Person Form - Management Notes")]
        public string StaffFormManagementNotes { get; set; }

        #endregion

        #region Live Chat

        [Display(Name = "Chat Service")]
        public string ChatService { get; set; }

        [Display(Name = "Chat Rooms")]
        public string ChatRooms { get; set; }

        #endregion
    }
}