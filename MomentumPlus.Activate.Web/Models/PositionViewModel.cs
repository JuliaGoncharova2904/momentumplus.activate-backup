﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Activate.Web.Models
{
    public class PositionViewModel : BaseViewModel
    {
        #region DOMAIN

        [Required]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        [Display(Name = "Name *")]
        public string Name { get; set; }

        public virtual ICollection<AttachmentViewModel> Attachments { get; set; }

        #endregion

        #region VIEW

        public IEnumerable<SelectListItem> Templates { get; set; }
        [Required]
        [Display(Name = "Template *")]
        public Guid? TemplateId { get; set; }

        public IEnumerable<SelectListItem> Workplaces { get; set; }
        [Display(Name = "Workplace")]
        public Guid? DefaultWorkplaceId { get; set; } // Domain

        #endregion
    }
}
