﻿using MomentumPlus.Core.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Activate.Web.Models
{
    public class TopicGroupViewModel : BaseViewModel
    {
        #region DOMAIN

        [StringLength(75, MinimumLength = 3, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Title { get; set; }
        [StringLength(75, MinimumLength = 3, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Name { get; set; }
        public virtual TopicGroup.Category TopicGroupCategory { get; set; }
        public virtual int TopicsCount { get; set; }

        public virtual TopicGroup.TopicType TopicGroupType { get; set; }

        public Guid? SourceTemplateId { get; set; }

        #endregion
    }
}
