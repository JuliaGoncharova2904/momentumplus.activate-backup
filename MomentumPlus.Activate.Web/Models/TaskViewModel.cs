﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MomentumPlus.Activate.Web.Models.Moderation;
using System.ComponentModel.DataAnnotations.Schema;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Models
{
    public class TaskViewModel : ReviewableViewModel
    {
        #region DOMAIN

        public enum ParentType
        {
            Topic, Project, Contact
        }

        public int Ordinal { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        [Display(Name = "Name *")]
        public string Name { get; set; }

        
        [StringLength(2000, ErrorMessage = "{0} cannot be more than {1} characters long.")]
        public string Handover { get; set; }

        //[Range(0, int.MaxValue, ErrorMessage = "{0} must be greater than or equal to {1}.")]
        [Display(Name = "Week Due *")]
        public int WeekDue { get; set; }

        [Display(Name = "Fixed Date/Time")]
        public DateTime? Fixed { get; set; }

        public Task.TaskPriority Priority { get; set; }

        public bool Complete { get; set; }

        public DateTime? CompleteDate { get; set; }

        public Guid? SourceTemplateId { get; set; }

        public bool IsCore { get; set; }

        #endregion

        #region VIEW

        public Guid? TopicId { get; set; }
        public Guid? ProjectInstanceId { get; set; }
        public Guid? ContactId { get; set; }

        public string TopicName { get; set; }

        public int Subjectivity { get; set; }
        public string ParentPackageName { get; set; }

        public string UserOwnerName { get; set; }

        public bool FrameworkItem { get; set; }

        public IEnumerable<SelectListItem> WeekDueListItems { get; set; }

        public virtual ICollection<VoiceMessage> VoiceMessages { get; set; }
        public virtual ICollection<Attachment> Attachments { get; set; }
        public virtual ICollection<DocumentLibrary> DocumentLibraries { get; set; }

        [ForeignKey("ParentTopicId")]
        public virtual Topic ParentTopic { get; set; }
        public virtual Guid? ParentTopicId { get; set; }

        public bool OnboarderMode { get; set; }

        public bool IsLeavingPackageState { get; set; }


        #endregion
    }
}
