﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Activate.Web.Models.People
{
    public class UserViewModel : PersonViewModel
    {
        #region DOMAIN

        [Display(Name = "Password *")]
        [RegularExpression(
            @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,}$",
            ErrorMessage = @"Your password must be 8 characters and contain a mixture of upper and lower-case letters, numbers and special characters such as !, #, @ or ?"
        )]
        public String Password { get; set; }
        [DataType(DataType.MultilineText)]
        public String Comment { get; set; }
        public DateTime? LastSMSMessage { get; set; }
        public int SMSMessagesToday { get; set; }
        public bool IsLoggedIn { get; set; }
        public string LastSessionID { get; set; }

        public virtual Position DefaultPosition { get; set; }
        [Display(Name = "Position *")]
        public Guid? DefaultPositionId { get; set; }

        public virtual Role Role { get; set; }
        [Required]
        [Display(Name = "User Type *")]
        public Guid? RoleId { get; set; }

        public Boolean IsApproved { get; set; }
        public int PasswordFailuresSinceLastSuccess { get; set; }
        public DateTime? LastPasswordFailureDate { get; set; }
        public DateTime? LastActivityDate { get; set; }
        public DateTime? LastLockoutDate { get; set; }
        public DateTime? LastLoginDate { get; set; }
        public String ConfirmationToken { get; set; }
        public DateTime? CreateDate { get; set; }
        public Boolean IsLockedOut { get; set; }
        public DateTime? LastPasswordChangedDate { get; set; }
        public String PasswordVerificationToken { get; set; }
        public DateTime? PasswordVerificationTokenExpirationDate { get; set; }

        [Display(Name = "Show Tooltips")]
        public bool DisplayTooltips { get; set; }

        public Guid? PasswordResetGuid { get; set; }

        [Display(Name = "Enable Time Manipulation Tool for Session")]
        public bool EnableTimeManipulationTool { get; set; }

        public bool IsAdminEdit { get; set; }

        #endregion

        #region VIEW

        public string UserLineManagerName { get; set; }
        public string UserAdministratorName { get; set; }

        [System.Web.Mvc.Compare("Password", ErrorMessage = "The passwords do not match.")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password *")]
        public virtual String PasswordConfirm { get; set; }

        public IEnumerable<SelectListItem> Roles { get; set; }
        public IEnumerable<SelectListItem> Positions { get; set; }

        #endregion
    }
}