﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.Moderation;

namespace MomentumPlus.Activate.Web.Models.People
{
    public class StaffViewModel : PersonViewModel
    {
        #region DOMAIN

        [ForeignKey("PackageId")]
        public Guid PackageId { get; set; }
        public virtual Package Package { get; set; }

        [Display(Name = "Momentum+ User?")]
        public Guid? UserId { get; set; }
        public IEnumerable<SelectListItem> Users { get; set; }

        [Required]
        [Display(Name = "Employment Status *")]
        public string EmploymentStatus { get; set; }

        //[Required]
        [Display(Name = "Future Training")]
        [StringLength(500, MinimumLength = 2, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string FutureTraining { get; set; }

        //[Required]
        [Display(Name = "Management Notes")]
        [StringLength(500, MinimumLength = 2, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string ManagementNotes { get; set; }

        #endregion

        #region VIEW

        public bool ShowGoToPackage { get; set; }

        #endregion

        public bool UserIsClientAdmin { get; set; }
        public bool UserIsHrManager { get; set; }
        public bool UserIsLineManager { get; set; }

        public ModerationDecision? LineManagerDecision { get; set; }
        public string LineManagerNotes { get; set; }

        public ModerationDecision? HrManagerDecision { get; set; }

        public string HrManagerNotes { get; set; }
        public bool ShowLineManagerSection { get; set; }
        public bool ShowHrManagerSection { get; set; }

        public bool ReadyForReview { get; set; }
    }
}