﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using MomentumPlus.Activate.Web.Attributes;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Models.People
{
    public class PersonViewModel : BaseViewModel
    {
        #region DOMAIN

        [StringLength(10)]
        public string Title { get; set; }

        [Required]
        [Display(Name = "First Name *")]
        [StringLength(35, MinimumLength = 2, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string FirstName { get; set; }

        [Display(Name = "Surname")]
        [StringLength(35, MinimumLength = 2, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string LastName { get; set; }

        [Display(Name = "Profile Image")]
        public Guid? ImageId { get; set; }

        public string FullName { get; set; }

        [Display(Name = "Address 1")]
        [StringLength(50, MinimumLength = 1, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Address1 { get; set; }

        [Display(Name = "Address 2")]
        [StringLength(50, MinimumLength = 1, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Address2 { get; set; }

        [StringLength(50, MinimumLength = 1, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string City { get; set; }

        [StringLength(50, MinimumLength = 1, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Country { get; set; }

        [Display(Name = "Pcode/Zip")]
        [StringLength(8, MinimumLength = 1, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string PostcodeOrZip { get; set; }

        [EmailAddress]
        [StringLength(256, MinimumLength = 5, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Email { get; set; }

        [Display(Name = "Telephone")]
        [StringLength(15, MinimumLength = 2, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Phone { get; set; }

        public string Mobile { get; set; }

        //fix AC-194
        [Required(ErrorMessage = "The Company * field is required.")]
        [Display(Name = @"Company *")]
        public Guid? CompanyId { get; set; }
        public IEnumerable<SelectListItem> Companies { get; set; }
        public bool OtherCompany { get; set; }
        [Display(Name = @"Company Name *")]
        public string OtherCompanyName { get; set; }
        [ForeignKey("CompanyId")]
        public virtual MasterList Company { get; set; }


        [RequiredIfUserIdIsNull(ErrorMessage = "The Position * field is required.")]
        [Display(Name = "Position *")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Position { get; set; }

        #endregion

        #region VIEW

        public bool UserMode { get; set; }

        public bool DisablePersonFields { get; set; }

        public bool HidePosition { get; set; }

        #endregion
    }
}