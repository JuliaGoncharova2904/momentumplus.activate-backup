﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace MomentumPlus.Activate.Web.Models
{
    public class MeetingViewModel : BaseViewModel
    {
        #region DOMAIN

        [Required]
        [StringLength(100, MinimumLength = 2)]
        [Display(Name = "Meeting Title *")]
        public string Name { get; set; }
        [StringLength(50, MinimumLength = 2)]
        public string Location { get; set; }

        [Display(Name = "Target Week")]
        public int? TargetWeek { get; set; }

        [Display(Name = "Start Date/Time")]
        public DateTime? Start { get; set; }

        [Display(Name = "Duration (Mins)")]
        [Range(1, int.MaxValue, ErrorMessage = "The value must be greater than 0")]
        public int DurationMins { get; set; }

        [ScriptIgnore]
        public virtual ContactViewModel ParentContact { get; set; }
        
        [ScriptIgnore]
        [Required]
        [Display(Name = "Person *")]
        public virtual Guid ParentContactId { get; set; }

        [ScriptIgnore]
        [Required]
        [Display(Name = "Meeting Type *")]
        public virtual Guid MeetingTypeId { get; set; }
        [ScriptIgnore]
        public virtual MeetingType MeetingType { get; set; }

        [ScriptIgnore]
        public string UserOwnerName { get; set; }

        [ForeignKey("PackageId")]
        public virtual Guid PackageId { get; set; }
        public virtual Package Package { get; set; }

        public Guid? ProjectInstanceId { get; set; }

        #endregion

        #region VIEW

        public IEnumerable<SelectListItem> Contacts { get; set; }

        public IEnumerable<SelectListItem> MeetingTypeItems { get; set; }

        public IEnumerable<SelectListItem> DurationMinItems { get; set; }

        public IEnumerable<SelectListItem> TargetWeeksList { get; set; } 

        #endregion

    }
}
