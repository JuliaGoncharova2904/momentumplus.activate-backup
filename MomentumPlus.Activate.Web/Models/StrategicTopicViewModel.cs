﻿using MomentumPlus.Core.Models;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Activate.Web.Models
{
    public class StrategicTopicViewModel : TopicViewModel
    {
        [StringLength(50, ErrorMessage = "{0} cannot be more than {1} characters long.")]
        [Display(Name = "Note Title")]
        public string NoteTitle { get; set; }
        public string NoteTitleLabel { get; set; }

        [Display(Name = "Review Frequency")]
        public Topic.StategyReviewFrequency ReviewFrequency { get; set; }
        public string ReviewFrequencyLabel { get; set; }

        public Topic.StategyQuality Quality { get; set; }
        public string QualityLabel { get; set; }
    }
}
