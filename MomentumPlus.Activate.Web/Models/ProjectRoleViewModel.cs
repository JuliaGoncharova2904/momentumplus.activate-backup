﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MomentumPlus.Activate.Web.Models
{
    public class ProjectRoleViewModel : BaseViewModel
    {
        public string Title { get; set; }
    }
}