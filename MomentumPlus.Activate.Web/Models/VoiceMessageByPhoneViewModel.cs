﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Activate.Web.Models
{
    public class VoiceMessageByPhoneViewModel : BaseViewModel
    {
        [Required]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        [Display(Name = "Name *")]
        public string Name { get; set; }

        public Guid ItemId { get; set; }
        public ItemType ItemType { get; set; }

        public string CoreService_Pin { get; set; }
    }
}