﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MomentumPlus.Activate.Web.Areas.Home.Models;
using MomentumPlus.Activate.Web.Models.People;
using MomentumPlus.Activate.Web.Models.Moderation;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Models
{
    public class ContactViewModel : ReviewableViewModel
    {

        public ContactViewModel()
        {
            CompanyGuid = "";
        }


        #region DOMAIN

        public bool IsMyTeamReferer { get; set; }
        public bool IsFramework { get; set; }

        public string CompanyGuid { get; set; }


        public PersonViewModel Person { get; set; }
        public Guid? PersonId { get; set; }

        [Display(Name = "Direct/Indirect")]
        public Guid? IndirectRelationshipContactId { get; set; }
        public string IndirectRelationship { get; set; }

        [Display(Name = "Importance")]
        public Contact.Importance RelationshipImportance { get; set; }

        [Display(Name = "Intro Required")]
        public bool IntroRequired { get; set; }

        // [Display(Name = "Length of Relation")]
        //public int LengthOfRelationship { get; set; }
        [Display(Name = "Relationship Start Date")]
        public DateTime? StartDate { get; set; }

        [Display(Name = "Relationship Strength")]
        public virtual Contact.Strength StrengthOfRelationship { get; set; }

        [Display(Name = "Improve Reason")]
        public string ImproveReason { get; set; }

        [Display(Name = "Employee Copy Request?")]
        public bool EmployeeCopyRequest { get; set; }

        public int? ChartX { get; set; }
        public int? ChartY { get; set; }

        public virtual int MeetingsCount { get; set; }

        public bool Managed { get; set; }
        public bool IsLeaver { get; set; }

        [Display(Name = @"Future Training")]
        public string FutureTraining { get; set; }
        [Display(Name = @"Management Notes")]
        public string ManagementNotes { get; set; }

        [Display(Name = "Employment Type *")]
        public string EmploymentStatus { get; set; }

        public string SelectedTab { get; set; }

        #region Best Time To Contact

        public bool BttcMon { get; set; }
        public bool BttcTue { get; set; }
        public bool BttcWed { get; set; }
        public bool BttcThu { get; set; }
        public bool BttcFri { get; set; }
        public bool BttcSat { get; set; }
        public bool BttcSun { get; set; }
        public bool BttcAm { get; set; }
        public bool BttcPm { get; set; }

        public bool BttcTst { get; set; }

        #endregion

        #endregion

        #region VIEW

        public IEnumerable<SelectListItem> Contacts { get; set; }

        public Guid PackageOwnerId { get; set; }
        public IDictionary<Guid, ContactChartViewModel> GraphNodes { get; set; }

        public IEnumerable<string> Companies { get; set; }

        public Guid? DefaultCompanyId { get; set; }

        public bool ShowManagingTab { get; set; }

        //targets
        public int TargetVolume { get; set; }
        public int TargetAverageWordCount { get; set; }
        public int TargetAttachments { get; set; }
        public string TargetCompletionDate { get; set; }

        //actuals
        public int ActualVolume { get; set; }
        public int ActualTotalWordCount { get; set; }
        public int ActualAverageWordCount { get; set; }
        public int ActualAttachments { get; set; }
        public int ActualApproved { get; set; }

        //percentages
        public int PercentageVolume { get; set; }
        public int PercentageWordCount { get; set; }
        public int PercentageAttachments { get; set; }
        public int PercentageApproved { get; set; }

        //rag status
        public string RagWordCount { get; set; }
        public string RagAttachments { get; set; }
        public string RagVolume { get; set; }
        public string RagApproved { get; set; }

        #endregion


    }
}