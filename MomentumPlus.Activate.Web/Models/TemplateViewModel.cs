﻿using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Activate.Web.Models
{
    public class TemplateViewModel : BaseViewModel
    {
        [Required]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        [Display(Name = "Display Name *")]
        public string Name { get; set; } // aka Display Name

        [Display(Name = "Duration")]
        public int DurationDays { get; set; }
    }
}
