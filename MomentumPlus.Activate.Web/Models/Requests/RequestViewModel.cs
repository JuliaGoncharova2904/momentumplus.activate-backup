﻿using MomentumPlus.Activate.Web.Models.Moderation;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Models.Requests
{
    public class RequestViewModel : ModeratedViewModel
    {
        public Package Package { get; set; }
    }
}