﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Activate.Web.Models.Requests
{
    public class TransferRequestViewModel : RequestViewModel
    {
        public Guid? TransferSourceId { get; set; }

        [Required]
        [Display(Name = "Target Package *")]
        public Guid? DestinationId { get; set; }
        public IEnumerable<SelectListItem> Packages { get; set; }
    }
}