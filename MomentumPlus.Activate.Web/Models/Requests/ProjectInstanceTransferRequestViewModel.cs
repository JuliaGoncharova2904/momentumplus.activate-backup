﻿using MomentumPlus.Core.Models;
using System;

namespace MomentumPlus.Activate.Web.Models.Requests
{
    public class ProjectInstanceTransferRequestViewModel : TransferRequestViewModel
    {
        public Guid? ProjectInstanceId { get; set; }

        public virtual ProjectInstance ProjectInstance { get; set; }
    }
}