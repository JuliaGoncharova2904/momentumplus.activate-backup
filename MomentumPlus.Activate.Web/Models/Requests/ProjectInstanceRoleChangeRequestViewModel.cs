﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MomentumPlus.Activate.Web.Models.Requests
{
    public class ProjectInstanceRoleChangeRequestViewModel : RequestViewModel
    {
        public Guid? ProjectInstanceId { get; set; }

        public virtual ProjectInstanceViewModel ProjectInstance { get; set; }

        public Guid? NewRoleId { get; set; }

        public virtual ProjectRoleViewModel NewRole { get; set; }

        public Guid? OldRoleId { get; set; }

        public virtual ProjectRoleViewModel OldRole { get; set; }
    }
}