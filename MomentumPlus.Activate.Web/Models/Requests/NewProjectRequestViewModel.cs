﻿using MomentumPlus.Core.Models.Moderation;
using System;

namespace MomentumPlus.Activate.Web.Models.Requests
{
    public class NewProjectRequestViewModel
    {

        public Guid Id { get; set; }

        public ModerationDecision? Decision { get; set; }

        public ProjectViewModel Project { get; set; }

    }
}