﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Activate.Web.Models.Requests
{
    public class StaffTransferRequestViewModel : TransferRequestViewModel
    {
        public Guid? ContactId { get; set; }

        public virtual Contact Contact { get; set; }

        [Display(Name = "Relationship")]
        public Guid? DestinationContactId { get; set; }

        public virtual Contact DestinationContact { get; set; }
        
        public IEnumerable<SelectListItem> Contacts { get; set; }
    }
}