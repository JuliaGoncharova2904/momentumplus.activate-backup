﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Activate.Web.Models
{
    public class ChangePasswordViewModel
    {
        public Guid UserId { get; set; }
        [Required]
        [Display(Name = @"Current Password *")]
        public string CurrentPassword { get; set; }
        [Required]
        [Display(Name = @"New Password *")]
        [RegularExpression(
            @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,}$",
            ErrorMessage = @"Your password must be 8 characters and contain a mixture of upper and lower-case letters, numbers and special characters such as !, #, @ or ?"
        )]
        public string NewPassword { get; set; }

        [Required]
        [System.Web.Mvc.Compare("NewPassword", ErrorMessage = @"The passwords do not match.")]
        [Display(Name = @"Confirm New Password *")]
        public string ConfirmNewPassword { get; set; }
    }
}