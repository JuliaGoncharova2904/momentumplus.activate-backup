﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MomentumPlus.Activate.Web.Models.Moderation;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Models
{
    public class TransferProjectCardViewModel : ModeratedViewModel
    {
        public Guid? ProjectTransferRequestId { get; set; }

        public Guid ProjectInstanceId { get; set; }
        public ProjectInstance ProjectInstance { get; set; }

        [Display(Name = "Target Package")]
        [Required]
        public Guid? TargetPackageId { get; set; }
        public IEnumerable<SelectListItem> Packages { get; set; }
    }
}