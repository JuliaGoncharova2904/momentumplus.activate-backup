﻿using MomentumPlus.Core.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Activate.Web.Models
{
    public class BaseApprovalViewModel : BaseViewModel
    {
        #region DOMAIN

        public DateTime? ApprovedLineManager { get; set; }
        public BaseApproval.DeclinedReason DeclinedNotesLineManager { get; set; }

        public DateTime? ApprovedHRManager { get; set; }
        public BaseApproval.DeclinedReason DeclinedNotesHRManager { get; set; }

        public bool ReadyForReview { get; set; }
        public bool IsApproved { get; set; }

        #endregion

        #region VIEW

        public bool UserIsLineManager { get; set; }
        public bool UserIsHRManager { get; set; }

        public string UserOwnerName { get; set; }

        private bool? isApprovedLineManager;

        [Display(Name = "Line Mgr Approval")]
        public bool IsApprovedLineManager
        {
            get
            {
                if (isApprovedLineManager.HasValue)
                {
                    return isApprovedLineManager.Value;
                }

                return this.ApprovedLineManager.HasValue;
            }
            set
            {
                isApprovedLineManager = value;
            }
        }

        private bool? isApprovedHRManager;

        [Display(Name = "HR Mgr Approval")]
        public bool IsApprovedHRManager
        {
            get
            {
                if (isApprovedHRManager.HasValue)
                {
                    return isApprovedHRManager.Value;
                }

                return this.ApprovedHRManager.HasValue;
            }
            set
            {
                isApprovedHRManager = value;
            }
        }

        public bool HideReadyForReview { get; set; }

        public bool DisableReadyForReview { get; set; }

        public bool FrameworkItem { get; set; }

        #endregion
    }
}
