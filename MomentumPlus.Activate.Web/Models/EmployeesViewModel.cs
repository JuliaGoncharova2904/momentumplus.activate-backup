﻿using System;
using System.Collections.Generic;
using MomentumPlus.Activate.Web.Models.People;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Models
{
    public class EmployeesViewModel
    {
        public IEnumerable<UserViewModel> Users { get; set; }
        public IEnumerable<PackageViewModel> Packages { get; set; }
        public Guid? SelectedUserId { get; set; }
        public UserViewModel SelectedUser { get; set; }

        public string Query { get; set; }
        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public bool SortAscending { get; set; }

        public Package.Status? SelectedPackageStatus { get; set; }

        public bool IsAuthorisedToAdd { get; set; }

        public bool ReadOnly { get; set; }

        public bool Retired { get; set; }
    }
}