﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Activate.Web.Models
{
    public class PackageTransferViewModel
    {
        public Guid? TopicId { get; set; }
        public Guid? TaskId { get; set; }
        public object SourceDetails { get; set; }
        [Required]
        [Display(Name = "Target Package")]
        public Guid? TargetPackageId { get; set; }
        [Display(Name = "Target Topic Group")]
        public Guid? TargetParentTopicGroupId { get; set; }
        [Display(Name = "Target Topic")]
        public Guid? TargetParentTopicId { get; set; }

        public IEnumerable<SelectListItem> Packages { get; set; }
        public IEnumerable<SelectListItem> TopicGroups { get; set; }
        public IEnumerable<SelectListItem> Topics { get; set; }
    }
}