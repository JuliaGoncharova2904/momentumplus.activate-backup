﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Foolproof;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Attributes;

namespace MomentumPlus.Activate.Web.Models
{
    public class ProjectViewModel : BaseViewModel
    {
        public virtual TemplateViewModel SourceTemplate { get; set; }
        public Guid? SourceTemplateId { get; set; }

        public bool IsFrameworkDefault { get; set; }

        [Required]
        [Display(Name = "Name *")]
        [StringLength(30, MinimumLength = 3)]
        public string Name { get; set; }

        [Display(Name = "Project ID")]
        [ProjectId]
        public string ProjectId { get; set; }

        [Required]
        [Display(Name = "Start Date *")]
        public DateTime? Start { get; set; }

        [Display(Name = "End Date")]
        [GreaterThan("Start", PassOnNull = true)]
        public DateTime? End { get; set; }

        [Required]
        [Display(Name = "Project Manager *")]
        public Guid? ProjectManagerId { get; set; }
        public ContactViewModel ProjectManager { get; set; }
        public IEnumerable<SelectListItem> Contacts { get; set; }

        [Display(Name = "Project Image / Logo")]
        public Guid? ImageId { get; set; }
        public IEnumerable<SelectListItem> Images { get; set; }

        [Required]
        [Display(Name = "Status *")]
        public Guid? StatusId { get; set; }
        public ProjectStatusViewModel Status { get; set; }
        public IEnumerable<SelectListItem> Statuses { get; set; }

        [Display(Name = "Budget (£)")]
        [Range(0, 999999999, ErrorMessage = "The field Budget (£) must be less than £1,000,000,000")]
        public double? Budget { get; set; }

        [Display(Name = "Project Space / Folder Location")]
        public string ProjectSpaceLocation { get; set; }

        [Required]
        [Display(Name = "Brief Description / History *")]
        public string Description { get; set; }


        [Display(Name = "My Role")]
        public Guid? RoleId { get; set; }
        public ProjectRoleViewModel Role { get; set; }
        public IEnumerable<SelectListItem> Roles { get; set; }


        [Display(Name = "Handover Notes")]
        public string Notes { get; set; }

        public bool IsLeavingPackageState { get; set; }
    }
}