﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MomentumPlus.Activate.Web.Models
{
    public class ProjectReportViewModel : BaseViewModel
    {
        //targets
        public int TargetVolume { get; set; }
        public int TargetAverageWordCount { get; set; }
        public int TargetAttachments { get; set; }
        public string TargetCompletionDate { get; set; }


        //actuals
        public int ActualVolume { get; set; }
        public int ActualTotalWordCount { get; set; }
        public int ActualAverageWordCount { get; set; }
        public int ActualAttachments { get; set; }
        public int ActualApproved { get; set; }

        //percentages
        public int PercentageVolume { get; set; }
        public int PercentageWordCount { get; set; }
        public int PercentageAttachments { get; set; }
        public int PercentageApproved { get; set; }

        //rag status
        public string RagWordCount { get; set; }
        public string RagAttachments { get; set; }
        public string RagVolume { get; set; }
        public string RagApproved { get; set; }
    }
}