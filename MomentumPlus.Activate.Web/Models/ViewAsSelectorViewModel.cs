﻿using MomentumPlus.Core.Models.People;
using System.Collections.Generic;

namespace MomentumPlus.Activate.Web.Models
{
    public class ViewAsSelectorViewModel
    {
        public string SelectedUser { get; set; }
        public IEnumerable<User> Users { get; set; }
        public bool IsAdmin { get; set; }
    }
}