﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Activate.Web.Models
{
    public class TopicQuestionViewModel : BaseViewModel
    {
        [Required]
        [Display(Name = "Label *")]
        public string Label { get; set; }

        [Required]
        [Display(Name = "Reference *")]
        public Guid Reference { get; set; }
    }
}
