﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Activate.Web.Models
{
    public class LocationTopicViewModel : TopicViewModel
    {
        [Required]
        [StringLength(50, ErrorMessage = "{0} cannot be more than {1} characters long.")]
        [Display(Name = "Location *")]
        public string Location { get; set; }
        public string LocationLabel { get; set; }
        public Guid? LocationId { get; set; }

        [StringLength(2000, ErrorMessage = "{0} cannot be more than {1} characters long.")]
        public string Address { get; set; }
        public string AddressLabel { get; set; }

        [StringLength(2000, ErrorMessage = "{0} cannot be more than {1} characters long.")]
        [Display(Name = "Map Link")]
        public string MapLink { get; set; }
        public string MapLinkLabel { get; set; }

        public IEnumerable<SelectListItem> Locations { get; set; }
    }
}
