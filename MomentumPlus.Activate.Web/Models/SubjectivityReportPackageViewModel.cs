﻿using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.People;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Activate.Web.Models
{
    public class SubjectivityReportPackageViewModel : BaseViewModel
    {
        public IEnumerable<Topic> Topics { get; set; }
        public IEnumerable<Task> Tasks { get; set; }

        #region DOMAIN

        public string Name { get; set; }

        [Required]
        [Display(Name = "Package Status")]
        public Package.Status PackageStatus { get; set; }

        [Display(Name = "Package Ancestor")]
        public PackageViewModel Ancestor { get; set; }

        public virtual Position Position { get; set; }

        public virtual Workplace Workplace { get; set; }

        public virtual User UserOwner { get; set; }

        public virtual User UserLineManager { get; set; }

        public virtual User UserHRManager { get; set; }

        //public TriggerViewModel Trigger { get; set; }

        [Display(Name = "Start Date")]
        public DateTime? StartDate { get; set; }

        public virtual ICollection<Contact> Contacts { get; set; }

        #endregion
    }
}
