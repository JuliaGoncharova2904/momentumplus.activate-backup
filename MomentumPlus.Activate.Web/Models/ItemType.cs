﻿namespace MomentumPlus.Activate.Web.Models
{
    public enum ItemType
    {
        Contact,
        Meeting,
        Position,
        Task,
        Topic,
        Attachment,
        VoiceMessage,
        MasterList,
        Document,
        Project,
        ProjectInstance,
        Staff,
        Relationship
    }
}