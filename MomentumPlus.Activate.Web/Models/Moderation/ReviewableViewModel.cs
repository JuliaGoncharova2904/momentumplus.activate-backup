﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MomentumPlus.Activate.Web.Models.Moderation
{
    public class ReviewableViewModel : ModeratedViewModel
    {
        #region DOMAIN

        public bool ReadyForReview { get; set; }

        #endregion

        #region VIEW

        public bool DisableReadyForReview { get; set; }

        #endregion
    }
}