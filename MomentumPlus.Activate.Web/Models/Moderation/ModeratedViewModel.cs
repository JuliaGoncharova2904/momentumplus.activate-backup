﻿using MomentumPlus.Core.Models.Moderation;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Activate.Web.Models.Moderation
{
    public class ModeratedViewModel : BaseViewModel
    {
        #region VIEW

        [Display(Name = "Line Mgr Approval")]
        public ModerationDecision? LineManagerDecision { get; set; }
        public string LineManagerNotes { get; set; }
        public bool UserIsLineManager { get; set; }
        public bool ShowLineManagerSection { get; set; }

        [Display(Name = "HR Mgr Approval")]
        public ModerationDecision? HrManagerDecision { get; set; }
        public string HrManagerNotes { get; set; }
        public bool UserIsHrManager { get; set; }
        public bool ShowHrManagerSection { get; set; }

        public bool UserIsClientAdmin { get; set; }

        public bool ApprovalMode { get; set; }

        #endregion
    }
}