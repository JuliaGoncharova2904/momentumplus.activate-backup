﻿using System.Collections.Generic;
using MomentumPlus.Activate.Web.Models.Home;

namespace MomentumPlus.Activate.Web.Models
{
    public class TriggersViewModel
    {
        public IEnumerable<TriggerItemViewModel> Triggers { get; set; }
        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }

        public TriggerItemViewModel.SortColumn Sort { get; set; }
        public bool SortAscending { get; set; }
    }
}
