﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Activate.Web.Models
{
    public class ComplianceTopicViewModel : TopicViewModel
    {
        [Required]
        [StringLength(50, ErrorMessage = "{0} cannot be more than {1} characters long.")]
        [Display(Name = "Name *")]
        public string DocumentName { get; set; }
        public string DocumentNameLabel { get; set; }

        [Required]
        [StringLength(750, ErrorMessage = "{0} cannot be more than {1} characters long.")]
        [Display(Name = "Notes *")]
        public string RegulationAndComplianceNotes { get; set; }
        public string RegulationAndComplianceNotesLabel { get; set; }

        [StringLength(750, ErrorMessage = "{0} cannot be more than {1} characters long.")]
        [Display(Name = "Location")]
        public string DocumentLocation { get; set; }
        public string DocumentLocationLabel { get; set; }
        public Guid? DocumentLocationId { get; set; }

        [StringLength(50, ErrorMessage = "{0} cannot be more than {1} characters long.")]
        [Display(Name = "Contact")]
        public string PrimaryContact { get; set; }
        public string PrimaryContactLabel { get; set; }
        public Guid? PrimaryContactId { get; set; }

        [StringLength(2000, ErrorMessage = "{0} cannot be more than {1} characters long.")]
        [Display(Name = "Website")]
        public string ReferenceWebsite { get; set; }
        public string ReferenceWebsiteLabel { get; set; }
        public Guid? ReferenceWebsiteId { get; set; }

        [Display(Name = "Training Required")]
        public bool TrainingRequired { get; set; }
        public string TrainingRequiredLabel { get; set; }

        public IEnumerable<SelectListItem> DocumentLocations { get; set; }
        public IEnumerable<SelectListItem> PrimaryContacts { get; set; }
        public IEnumerable<SelectListItem> ReferenceWebsites { get; set; }
    }
}