﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Activate.Web.Models
{
    public class VoiceMessagesViewModel
    {
        public IEnumerable<VoiceMessageViewModel> VoiceMessages { get; set; }
        public Guid ItemId { get; set; }
        public ItemType ItemType { get; set; }
        public string ItemName { get; set; }
        public string VoiceMessagePhoneNumber { get; set; }
        public bool OnboarderView { get; set; }
    }
}