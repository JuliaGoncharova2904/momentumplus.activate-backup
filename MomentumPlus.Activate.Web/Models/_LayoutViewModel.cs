﻿using System.Web.Security;

namespace MomentumPlus.Activate.Web.Models
{
    public class _LayoutViewModel
    {
        public MembershipUser currentUser { get; set; }
    }
}