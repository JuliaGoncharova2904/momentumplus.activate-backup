﻿using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Activate.Web.Models
{
    public class FileViewModel : BaseViewModel
    {
        [Required]
        [Display(Name = "Data *")]
        public byte[] BinaryData { get; set; }
    }
}
