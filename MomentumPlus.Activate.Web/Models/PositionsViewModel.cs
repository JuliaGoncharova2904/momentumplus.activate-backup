﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Activate.Web.Models
{
    public class PositionsViewModel
    {
        public IEnumerable<PositionViewModel> Positions { get; set; }
        public IEnumerable<PackageViewModel> Packages { get; set; }
        public Guid? SelectedPositionId { get; set; }

        public string Query { get; set; }
        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public bool SortAscending { get; set; }

        public Package.Status? SelectedPackageStatus { get; set; }

        public bool IsAuthorisedToAdd { get; set; }

        public bool Retired { get; set; }
    }
}