﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Activate.Web.Models
{
    public class ProcessTopicViewModel : TopicViewModel
    {
        [StringLength(750, ErrorMessage = "{0} cannot be more than {1} characters long.")]
        [Display(Name = "Notes")]
        public string WorkaroundNotes { get; set; }
        public string WorkaroundNotesLabel { get; set; }

        [Display(Name = "Process Interaction")]
        public Topic.ProcessInteraction ProcessInteraction { get; set; }
        public string ProcessInteractionLabel { get; set; }

        [StringLength(50, ErrorMessage = "{0} cannot be more than {1} characters long.")]
        public string Contact { get; set; }
        public string ContactLabel { get; set; }
        public Guid? ContactId { get; set; }

        [Display(Name = "Fit For Purpose")]
        public Topic.ProcessFitForPurpose FitForPurpose { get; set; }
        public string FitForPurposeLabel { get; set; }

        [StringLength(750, ErrorMessage = "{0} cannot be more than {1} characters long.")]
        [Display(Name = "Reason")]
        public string FitForPurposeReason { get; set; }
        public string FitForPurposeReasonLabel { get; set; }

        public bool LinkedProject { get; set; }

        [StringLength(50, ErrorMessage = "{0} cannot be more than {1} characters long.")]
        [Display(Name = "Project Reference")]
        public string ProjectReference { get; set; }
        public string ProjectReferenceLabel { get; set; }
        public Guid? ProjectReferenceId { get; set; }

        public IEnumerable<SelectListItem> Contacts { get; set; }
        public IEnumerable<SelectListItem> ProjectReferences { get; set; }

        public bool IsProLinkCore { get; set; }
    }
}
