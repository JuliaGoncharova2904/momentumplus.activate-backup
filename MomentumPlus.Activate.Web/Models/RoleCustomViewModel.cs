﻿namespace MomentumPlus.Activate.Web.Models
{
    public class RoleCustomViewModel
    {
        public enum CustomType
        {
            Css,
            Javascript,
            Html
        }

        public CustomType Type { get; set; }
        public string Content { get; set; }
    }
}