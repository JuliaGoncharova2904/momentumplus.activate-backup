﻿using System.Collections.Generic;
using MomentumPlus.Activate.Web.Models.People;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Models
{
    public class AdditionalReportViewModel : BaseViewModel
    {
        public IEnumerable<Package> Packages { get; set; }

        public Package Package { get; set; }

        public bool IsAncestor { get; set; }
        public bool IsPdf { get; set; }
        public bool IsApproved { get; set; }

        public int HandoverCountdownDays { get; set; }
        public int HandoverCountdownWeeks { get; set; }
        public int HandoverCountDownDaysTotal { get; set; }

        public int TargetVolume { get; set; }
        public int TargetAverageWordCount { get; set; }
        public int TargetAttachments { get; set; }
        public string TargetCompletionDate { get; set; }

        public UserViewModel Pickup { get; set; }
        public UserViewModel Leaver { get; set; }

        public virtual ICollection<ModuleViewModel> Modules { get; set; }
        public virtual ICollection<ContactViewModel> Contacts { get; set; }

        public ProjectReportViewModel Projects { get; set; }
        public StaffReportViewModel People { get; set; } 

        public virtual ContactViewModel ContactReport { get; set; }
    }
}