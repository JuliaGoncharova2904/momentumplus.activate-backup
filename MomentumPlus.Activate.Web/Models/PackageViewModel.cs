﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MomentumPlus.Activate.Web.Models.Handover;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.People;

namespace MomentumPlus.Activate.Web.Models
{
    public class PackageViewModel : BaseViewModel
    {
        #region DOMAIN

        public string Name { get; set; }

        [Required]
        [Display(Name = "Pack Status")]
        public Package.Status PackageStatus { get; set; }

        [Display(Name = "Pack Ancestor")]
        public PackageViewModel Ancestor { get; set; }

        public virtual Position Position { get; set; }

        public virtual Workplace Workplace { get; set; }

        public virtual User UserOwner { get; set; }

        public virtual User UserLineManager { get; set; }

        public virtual User UserHRManager { get; set; }

        //public TriggerViewModel Trigger { get; set; }

        public HandoverViewModel Handover { get; set; }

        [Display(Name = "Start Date")]
        public DateTime? StartDate { get; set; }

        public virtual ICollection<Contact> Contacts { get; set; }

        #endregion

        #region VIEW

        public IEnumerable<SelectListItem> Packs { get; set; }
        public bool Editing { get; set; }
        public bool ContactsEnabled { get; set; }
        public bool ProjectsEnabled { get; set; }

        [Required]
        [Display(Name = "Pack Owner *")]
        public Guid? UserOwnerId { get; set; }
        public IEnumerable<SelectListItem> UserOwners { get; set; }

        [Required]
        [Display(Name = "Position *")]
        public Guid? PositionId { get; set; }
        public IEnumerable<SelectListItem> Positions { get; set; }

        [Required]
        [Display(Name = "Workplace *")]
        public Guid? WorkplaceId { get; set; }
        public IEnumerable<SelectListItem> Workplaces { get; set; }

        [Required]
        [Display(Name = "Line Manager *")]
        public Guid? UserLineManagerId { get; set; }
        public IEnumerable<SelectListItem> UserLineManagers { get; set; }

        [Required]
        [Display(Name = "HR Manager *")]
        public Guid? UserHRManagerId { get; set; }
        public IEnumerable<SelectListItem> UserHRManagers { get; set; }

        [Display(Name = "Relationships")]
        public IList<Guid> ContactIds { get; set; }
        public IEnumerable<SelectListItem> FrameworkContacts { get; set; }

        [Display(Name = "Projects")]
        public IList<Guid> ProjectIds { get; set; }
        public IEnumerable<SelectListItem> FrameworkProjects { get; set; }

        public Guid? AncestorPackageId { get; set; }

        #endregion

        #region subjectivity

        public int SubjectivityCount { get; set; }
        public int TotalWordCount { get; set; }
        public int SubjectivityVsWordCount { get; set; }
        public int CountTopicsTasksWithSubjectivity { get; set; }
        public int TotalTopicsTasks { get; set; }
        public int SubjectivityTopicsTasks { get; set; }

        #endregion
    }
}
