﻿using System.Collections.Generic;

namespace MomentumPlus.Activate.Web.Models
{
    public class PackageSelectorViewModel
    {
        public IEnumerable<PackageViewModel> Packages { get; set; }
        public string SelectedPackage { get; set; }
        public bool ExamineMode { get; set; }
    }
}