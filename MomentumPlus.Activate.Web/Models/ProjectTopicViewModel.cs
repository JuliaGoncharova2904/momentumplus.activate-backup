﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Activate.Web.Models
{
    public class ProjectTopicViewModel : TopicViewModel
    {
        public Guid? ProjectId { get; set; }
        public string ProjectIdLabel { get; set; }

        [StringLength(50, ErrorMessage = "{0} cannot be more than {1} characters long.")]
        [Display(Name = "Project Sponsor")]
        public string ProjectSponsor { get; set; }
        public string ProjectSponsorLabel { get; set; }

        [StringLength(50, ErrorMessage = "{0} cannot be more than {1} characters long.")]
        [Display(Name = "Project Manager")]
        public string ProjectManager { get; set; }
        public string ProjectManagerLabel { get; set; }

        [Display(Name = "Budgeted Spend")]
        public Topic.BudgetedSpend BudgetedSpend { get; set; }
        public string BudgetedSpendLabel { get; set; }

        public Topic.ProjectStatus Status { get; set; }
        public string StatusLabel { get; set; }

        public Topic.ProjectSuccess Success { get; set; }
        public string SuccessLabel { get; set; }

        [Display(Name = "Project End Date")]
        public DateTime? ProjectEndDate { get; set; }
        public string ProjectEndDateLabel { get; set; }

        public IEnumerable<SelectListItem> Projects { get; set; }
    }
}
