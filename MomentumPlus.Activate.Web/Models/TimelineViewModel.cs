﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace MomentumPlus.Activate.Web.Models
{
    public class TimelineViewModel
    {
        public Guid ID { get; set; }
        public string TimelineOwner { get; set; }
        public Guid? AncestorId { get; set; }
        public String Title { get; set; }
        public String Report { get; set; }
        public String Download { get; set; }
        public String ViewMeeting { get; set; }
        public String CreateMeeting { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Dictionary<string, IEnumerable<TimelineMeetingViewModel>> Events { get; set; }
        public String Status { get; set; }
    }
}