﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace MomentumPlus.Activate.Web.Models
{
    public class AttachmentViewModel : BaseViewModel
    {
        [Required]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        [Display(Name = "Name *")]
        public string Name { get; set; }

        [Display(Name = "File Name")]
        public string FileName { get; set; }

        [Display(Name = "File Location *")]
        public string FilePath { get; set; }

        [Display(Name = "File ID *")]
        public Guid? FileId { get; set; }

        public virtual int DataBinaryDataLength { get; set; }

        public bool InLibrary { get; set; }
        public bool ChooseFileUpload { get; set; }

        public Guid? LibraryId { get; set; }

        [Display(Name = "File *")]
        public HttpPostedFileBase PostedFile { get; set; }
        public List<string> FileTypes { get; set; }

        public Guid ItemId { get; set; }
        public ItemType ItemType { get; set; }

    }
}
