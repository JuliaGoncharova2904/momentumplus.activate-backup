﻿//using System;
//using System.Collections.Generic;
//using System.ComponentModel.DataAnnotations;
//using System.Web.Mvc;
//using MomentumPlus.Activate.Web.Models.Moderation;

//namespace MomentumPlus.Activate.Web.Models
//{
//    public class TriggerViewModel : ReviewableViewModel
//    {
//        [Display(Name = "Finish Date")]
//        public DateTime? FinishDate { get; set; }

//        [Display(Name = "Pick-up")]
//        public Guid? PickupPackageId { get; set; }

//        public PackageViewModel PickupPackage { get; set; }

//        [Display(Name = "Pick-up")]
//        public string NextEmployee
//        {
//            get
//            {
//                if (PickupPackage != null)
//                {
//                    return PickupPackage.UserOwner.LastName + " " + PickupPackage.UserOwner.FirstName;
//                }
//                else
//                {
//                    return " - ";
//                }
//            }
//        }

//        [Display(Name = "Exit Duration")]
//        public int DurationDays { get; set; }

//        [Display(Name = "Carry Over")]
//        public bool CarryOver { get; set; }

//        [Display(Name = "Key Contact")]
//        public string KeyContact { get; set; }

//        public bool Approved { get; set; }

//        public Guid LeaverPackageId { get; set; }
//        public string LeaverName { get; set; }

//        public IEnumerable<SelectListItem> Packages { get; set; }
//        public IEnumerable<SelectListItem> KeyContacts { get; set; }
//    }
//}
