﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace MomentumPlus.Activate.Web.Models
{
    public class ModuleViewModel : BaseViewModel
    {
        public enum ModuleType
        {
            [Description("Essentials")]
            Critical,
            [Description("Achievements")]
            Experiences,
            None
        }

        public ModuleType Type { get; set; }

        public virtual TemplateViewModel Template { get; set; }
        public Guid? TemplateId { get; set; }

        public virtual ICollection<TopicGroupViewModel> TopicGroups { get; set; }

        //targets
        public int TargetVolume { get; set; }
        public int TargetAverageWordCount { get; set; }
        public int TargetAttachments { get; set; }
        public string TargetCompletionDate { get; set; }


        //actuals
        public int ActualVolume { get; set; }
        public int ActualTotalWordCount { get; set; }
        public int ActualAverageWordCount { get; set; }
        public int ActualAttachments { get; set; }
        public int ActualApproved { get; set; }

        //percentages
        public int PercentageVolume { get; set; }
        public int PercentageWordCount { get; set; }
        public int PercentageAttachments { get; set; }
        public int PercentageApproved { get; set; }

        //rag status
        public string RagWordCount { get; set; }
        public string RagAttachments { get; set; }
        public string RagVolume { get; set; }
        public string RagApproved { get; set; }
    }
}
