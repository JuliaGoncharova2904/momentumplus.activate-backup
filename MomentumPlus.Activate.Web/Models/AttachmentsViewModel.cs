﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Activate.Web.Models
{
    public class AttachmentsViewModel
    {
        public IEnumerable<AttachmentViewModel> Attachments { get; set; }
        public IEnumerable<DocumentLibraryFormViewModel> Documents { get; set; }
        public Guid ItemId { get; set; }
        public ItemType ItemType { get; set; }
        public string ItemName { get; set; }
        public bool OnboarderView { get; set; }
    }
}