﻿using MomentumPlus.Core.Models;
using System.Collections.Generic;

namespace MomentumPlus.Activate.Web.Models
{
    public class MasterListsViewModel : BaseViewModel
    {
        public MasterList.ListType Type { get; set; }
        public string SingularListType { get; set; }
        public IEnumerable<MasterListViewModel> List { get; set; }
        public string Query { get; set; }
        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public bool SortAscending { get; set; }
    }
}
