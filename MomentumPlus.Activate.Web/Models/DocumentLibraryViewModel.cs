﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MomentumPlus.Activate.Web.Models
{
    public class DocumentLibraryViewModel
    {
        public string Query { get; set; }
        public Guid? PositionId { get; set; }
        public List<SelectListItem> Positions { get; set; }
        public IEnumerable<AttachmentViewModel> Attachments { get; set; }

        public IEnumerable<DocumentLibraryFormViewModel> DocumentLibraries { get; set; }

        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }

        public Guid? ItemId { get; set; }
        public ItemType? ItemType { get; set; }

        public bool Retired { get; set; }

        public bool DisablePositionFilter { get; set; }
    }
}