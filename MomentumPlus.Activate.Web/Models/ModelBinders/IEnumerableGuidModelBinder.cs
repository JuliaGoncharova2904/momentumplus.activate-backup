﻿using System;
using System.Linq;
using System.Web.Mvc;

namespace MomentumPlus.Activate.Web.ModelBinders
{
    public class IEnumerableGuidModelBinder : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext,
            ModelBindingContext modelBindingContext)
        {
            var key = modelBindingContext.ModelName;
            var valueProviderResult = modelBindingContext.ValueProvider.GetValue(key);

            if (valueProviderResult == null ||
                string.IsNullOrEmpty(valueProviderResult.AttemptedValue))
            {
                return null;
            }

            modelBindingContext.ModelState.SetModelValue(key, valueProviderResult);

            var value = ((string[])valueProviderResult.RawValue).FirstOrDefault();

            if (value != null)
            {
                return value.Split(',').Select(v => new Guid(v.Trim())).ToList();
            }
            else
            {
                return null;
            }
        }
    }
}
