﻿using System;
using System.Web.Mvc;

namespace MomentumPlus.Activate.Web.ModelBinders
{
    public class TopicViewModelBinder : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext,
            ModelBindingContext modelBindingContext)
        {
            var typeName = modelBindingContext.ValueProvider.GetValue("ModelType");
            var type = Type.GetType(typeName.AttemptedValue, true);

            var model = Activator.CreateInstance(type);

            modelBindingContext.ModelMetadata =
                ModelMetadataProviders.Current.GetMetadataForType(null, type);
            modelBindingContext.ModelMetadata.Model = model;

            return base.BindModel(controllerContext, modelBindingContext);
        }
    }
}