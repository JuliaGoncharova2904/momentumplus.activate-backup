﻿using System;
using System.Globalization;
using System.Web.Mvc;
using MomentumPlus.Activate.Web.Properties;

namespace MomentumPlus.Activate.Web.ModelBinders
{
    public class DateTimeModelBinder : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext,
            ModelBindingContext modelBindingContext)
        {
            var key = modelBindingContext.ModelName;
            var valueProviderResult = modelBindingContext.ValueProvider.GetValue(key);

            if (valueProviderResult == null ||
                string.IsNullOrEmpty(valueProviderResult.AttemptedValue))
            {
                return null;
            }

            modelBindingContext.ModelState.SetModelValue(key, valueProviderResult);

            var values = (string[])valueProviderResult.RawValue;

            var format = Settings.Default.ShortDateFormat;
            DateTime date = new DateTime();

            try
            {
                date = DateTime.ParseExact(values[0], format, CultureInfo.InvariantCulture);

                if (values.Length == 3)
                {
                    double hours;
                    double minutes;
                    if (double.TryParse(values[1], out hours))
                    {
                        date = date.AddHours(hours);
                    }
                    else
                    {
                        date = date.AddHours(0);
                    }
                    if (double.TryParse(values[2], out minutes))
                    {
                        date = date.AddMinutes(minutes);
                    }
                    else
                    {
                        date = date.AddMinutes(0);
                    }
                }

                return date;
            }
            catch
            {
            }

            return null;
        }
    }
}