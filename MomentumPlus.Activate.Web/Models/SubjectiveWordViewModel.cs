﻿using System.Collections.Generic;
using MomentumPlus.Activate.Web.Models.People;

namespace MomentumPlus.Activate.Web.Models
{
    public class SubjectiveWordViewModel
    {
        public string Keyword { get; set; }
        public int UseCount { get; set; }
        public int NumberOfUsers { get; set; }
        public List<TopicViewModel> Topics { get; set; }
        public List<TaskViewModel> Tasks { get; set; }
        public List<StaffViewModel> People { get; set; }
        public List<ProjectInstanceViewModel> Projects { get; set; }
        public List<UserViewModel> Users { get; set; }
    }
}