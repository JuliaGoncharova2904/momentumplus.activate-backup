﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MomentumPlus.Activate.Web.Models
{
    public class AnswerResult
    {
        public Guid SectionId { get; set; }
        public Guid QuestionId { get; set; }
        public string Answer { get; set; }
        public string UserNotes { get; set; }
        public bool IsUsersNotesApproved { get; set; }
        public string HRComments { get; set; }
        public bool IsHRApproved { get; set; }
    }
}