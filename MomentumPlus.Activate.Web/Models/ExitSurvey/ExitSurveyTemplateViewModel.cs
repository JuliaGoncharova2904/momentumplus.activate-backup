﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MomentumPlus.Activate.Web.Models;

namespace MomentumPlus.Activate.Web.Models
{
    public class ExitSurveyTemplateViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}