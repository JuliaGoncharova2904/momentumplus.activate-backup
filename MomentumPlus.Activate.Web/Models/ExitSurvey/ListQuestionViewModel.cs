﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MomentumPlus.Activate.Web.Models;

namespace MomentumPlus.Activate.Web.Models
{
    public class ListQuestionViewModel : BaseViewModel
    {

        public int? Number { get; set; }
        public List<Guid> QuestionIds { get; set; }
        
        public List<SelectListItem> SetQuestion { get; set; }
    }
}