﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MomentumPlus.Activate.Web.Models
{
    public class EditQuestionViewModel
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public List<Answer> Answer { get; set; }
    }
}