﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MomentumPlus.Activate.Web.Models
{
    public class QuestionViewModel : BaseViewModel
    {
        public string Value { get; set; }

        public string Text { get; set; }
    }
}