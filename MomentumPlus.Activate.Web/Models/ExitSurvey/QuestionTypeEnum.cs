﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace MomentumPlus.Activate.Web.Models
{
    public enum QuestionTypeEnum
    {
        [Description("One answer")]
        Radio = 0,
        [Description("Many answer")]
        Multi = 1,
        [Description("Text answer")]
        Text = 2,
        [Description("Rating answer")]
        Rating = 3
    }
}