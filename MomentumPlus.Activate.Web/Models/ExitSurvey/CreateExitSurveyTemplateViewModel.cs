﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MomentumPlus.Activate.Web.Models;

namespace MomentumPlus.Activate.Web.Models
{
    public class CreateExitSurveyTemplateViewModel
    {
        public string Name { get; set; }
        public List<SelectListItem> Packages { get; set; }
        public List<Guid> PackagesId { get; set; } 
        public List<SelectListItem> ExitSurveys { get; set; }
        public Guid ExitSurveysId { get; set; }
    }
}