﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MomentumPlus.Activate.Web.Models
{
    public class ViewSurveyViewModel : BaseViewModel
    {
        public string Name { get; set; }

        public string PackageName { get; set; }

        public List<ExitSurveySectionViewModel> Section { get; set; }

    }
}