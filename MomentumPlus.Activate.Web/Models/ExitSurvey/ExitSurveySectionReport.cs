﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MomentumPlus.Activate.Web.Models;
using MvcPaging;

namespace MomentumPlus.Activate.Web.Models
{
    public class ExitSurveySectionReport
    {
        public Guid SectionId { get; set; }
        public int Number { get; set; }
        public IEnumerable<Guid> QuestionId { get; set; }
        public List<ExitSurveyQuestionForViewViewModel> Questions { get; set; }
        public ListQuestionViewModel QuestionList { get; set; }
        
    }
}