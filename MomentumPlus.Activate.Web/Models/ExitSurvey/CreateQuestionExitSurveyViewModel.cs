﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Models
{
    public class CreateQuestionExitSurveyViewModel
    {
        public string Name { get; set; }
        public string TypeQuestion { get; set; }
        public IEnumerable<string> Answers { get; set; }
    }
}