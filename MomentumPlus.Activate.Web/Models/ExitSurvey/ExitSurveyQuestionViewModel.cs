﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MomentumPlus.Activate.Web.Models;

namespace MomentumPlus.Activate.Web.Models
{
    public class ExitSurveyQuestionViewModel
    {
        public Guid QuestionId { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
    }
}