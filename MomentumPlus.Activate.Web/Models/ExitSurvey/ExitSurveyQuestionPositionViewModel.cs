﻿using System;

namespace MomentumPlus.Activate.Web.Models
{
    public class ExitSurveyQuestionPositionViewModel
    {
        public Guid SectionId { get; set; }
        public Guid QuestionId { get; set; }
        public string QuestionName { get; set; }
        public int Position { get; set; }
    }
}