﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Activate.Web.Models
{
    public class ExitSurveyQuestionForViewViewModel
    {
        public Guid QuestionId { get; set; }
        public string Name { get; set; }
        public string DefaultAnswer { get; set; }
        public QuestionTypeEnum QuestionType { get; set; }
        public List<string> Answers { get; set; }
        public List<Answer> AnswerObject { get; set; }
        public Guid SectionId { get; set; }
        public bool ReadOnlyAnswer { get; set; }
        public string TextAnswer { get; set; }
        public string UserNotes { get; set; }
        public string HRManagerNotes { get; set; }
        public bool IsUsersNotesApproved { get; set; }
        public bool IsHRCommentApproved { get; set; }
        public int Position { get; set; }
    }
}