﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Activate.Web.Models
{
    public class ExitSurveySectionForViewViewModel
    {
        public Guid SectionId { get; set; }
        public int Number { get; set; }
        public string Name { get; set; }
        public List<ExitSurveyQuestionForViewViewModel> QuestionList { get; set; }
    }
}