﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MomentumPlus.Activate.Web.Models
{
    public class PositionQuestionViewModel
    {
        public Guid SectionId { get; set; }
        public Guid QuestionId { get; set; }
        public int Position { get; set; }
    }
}