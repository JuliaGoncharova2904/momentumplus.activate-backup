﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Activate.Web.Models
{
    public class ExitSurveySectionTemplate
    {
        public bool IsOwnerSurvey { get; set; }
        public bool IsHRManagerMode { get; set; }
        public bool IsLineManagerMode { get; set; }
        public string SubmitType { get; set; }
        public bool CanSubmit { get; set; }
        public bool ReadMode { get; set; }
        public Guid UserId { get; set; }
        public IEnumerable<ExitSurveySectionForViewViewModel> Sections { get; set; }
    }
}