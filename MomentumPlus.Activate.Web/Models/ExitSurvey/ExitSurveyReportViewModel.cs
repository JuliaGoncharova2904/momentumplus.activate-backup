﻿using System;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.People;

namespace MomentumPlus.Activate.Web.Models
{
    public class ExitSurveyReportViewModel
    {
        public bool IsPdf { get; set; }
        public Package Package { get; set; }
        public User Pickup { get; set; }
        public Guid ExitSurveyId { get; set; }
    }
}