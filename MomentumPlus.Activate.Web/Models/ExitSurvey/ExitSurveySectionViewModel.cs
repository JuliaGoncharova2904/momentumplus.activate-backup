﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MomentumPlus.Activate.Web.Models
{
    public class ExitSurveySectionViewModel
    {
        public int Number { get; set; }
        public string Name { get; set; }
        public IEnumerable<Guid> Questions { get; set; }
        public IEnumerable<SelectListItem> AllQuestions { get; set; }
        public Guid? SectionId { get; set; }
        public List<ExitSurveyQuestionPositionViewModel> QuestionsPosition { get; set; }
    }
}