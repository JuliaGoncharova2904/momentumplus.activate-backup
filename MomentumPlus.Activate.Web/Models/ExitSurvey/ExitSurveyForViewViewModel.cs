﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Activate.Web.Models
{
    public class ExitSurveyForViewViewModel
    {
        public Guid ExitSurveyId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string PackageName { get; set; }
        public Guid UserId { get; set; }
        public IEnumerable<ExitSurveySectionForViewViewModel> Sections { get; set; } 
        public bool IsOwnerSurvey { get; set; }
        public bool IsHRManagerMode { get; set; }
        public bool IsLineManagerMode { get; set; }
        public string SubmitType { get; set; }
        public bool CanSubmit { get; set; }
        public bool ReadMode { get; set; }
    }
}