﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.WebPages.Html;

namespace MomentumPlus.Activate.Web.Models
{
    public class ExistSections : BaseViewModel
    {
        public int Number { get; set; }
        public List<Guid> QuestionIds { get; set; }
        public List<SelectListItem> SetQuestion { get; set; }
    }
}