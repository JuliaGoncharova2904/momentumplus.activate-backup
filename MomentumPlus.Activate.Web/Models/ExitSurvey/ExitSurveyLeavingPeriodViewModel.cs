﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Activate.Web.Models
{
    public class ExitSurveyLeavingPeriodViewModel
	{
        public Guid PackageId { get; set; }
        public IEnumerable<SelectListItem> ExitSurveys { get; set; }

	    [Required(ErrorMessage = "Exit Survey is required")]
        public Guid ExitSurveyId { get; set; }
        public bool IsExists { get; set; }
        public string UrlReferrer { get; set; }
    }
}