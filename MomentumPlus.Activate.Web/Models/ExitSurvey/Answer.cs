﻿namespace MomentumPlus.Activate.Web.Models
{
    public class Answer
    {
        public string Name { get; set; }

        public QuestionTypeEnum Type { get; set; }

        public bool Value { get; set; }

        public string TextAnswer { get; set; }
    }
}