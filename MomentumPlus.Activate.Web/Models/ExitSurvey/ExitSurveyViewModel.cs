﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MomentumPlus.Activate.Web.Models
{
    public class ExitSurveyViewModel
    {
        public Guid ExitSurveyId { get; set; }
        public string ExitSurveyName { get; set; }
        public string ExitSurveyDescription { get; set; }
        public bool IsSave { get; set; }
        public List<ExitSurveySectionViewModel> Sections { get; set; }
        public List<Guid> Questions { get; set; }
        public List<SelectListItem> AllQuestions { get; set; }
        public string SectionName { get; set; }
    }
}