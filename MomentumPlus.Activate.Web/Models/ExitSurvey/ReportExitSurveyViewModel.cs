﻿using System;

namespace MomentumPlus.Activate.Web.Models
{
    public class ReportExitSurveyViewModel
    {
        public Guid UserOwnerId { get; set; }
        public Guid? UserImageId { get; set; }
        public bool HasAccess { get; set; }
        public bool ReadMode { get; set; }
        public Guid PackageId { get; set; }
        public string UserOwnerName { get; set; }
        public string LineManagerName { get; set; }
        public string LastDateOfWork { get; set; }
        public string ExitSurveyName { get; set; }
        public string Status { get; set; }
        public string CounterStatus { get; set; }
        public string ExitSurveyId { get; set; }
    }
}