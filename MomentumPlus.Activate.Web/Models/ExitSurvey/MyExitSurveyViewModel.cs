﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MomentumPlus.Activate.Web.Models
{
    public class MyExitSurveyViewModel
    {
        public Guid ExitSurveyId { get; set; }
        public Guid UserId { get; set; }
        public bool ReadMode { get; set; }
        public bool HasAccess { get; set; }
    }
}