﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MomentumPlus.Activate.Web.Models;

namespace MomentumPlus.Activate.Web.Models
{
    public class SurveyEditViewModel
    {
        public Guid ID { get; set; }

        public string Name { get; set; }
        public List<ExistSections> Sertions { get; set; }

        public ListQuestionViewModel ListQuestion { get; set; }

        public IEnumerable<Guid> SectionId { get; set; }

        public IEnumerable<SelectListItem> Pack { get; set; }

        public Guid PackageID { get; set; }
    }
}