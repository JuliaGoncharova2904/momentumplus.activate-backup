﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Activate.Web.Models
{
    public class ToolsTopicViewModel : TopicViewModel
    {
        public Guid? AssetId { get; set; }
        public string AssetIdLabel { get; set; }

        public Topic.EquipmentFrequencyOfUse FrequencyOfUse { get; set; }
        public string FrequencyOfUseLabel { get; set; }

        public Topic.EquipmentCondition Condition { get; set; }
        public string ConditionLabel { get; set; }

        [StringLength(50, ErrorMessage = "{0} cannot be more than {1} characters long.")]
        public string Contact { get; set; }
        public string ContactLabel { get; set; }
        public Guid? ContactId { get; set; }

        public IEnumerable<SelectListItem> Assets { get; set; }
        public IEnumerable<SelectListItem> Contacts { get; set; }
    }
}
