﻿using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Activate.Web.Models
{
    public class ForgottenPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email Address *")]
        public string Email { get; set; }
    }
}