﻿using System;
using System.Collections.Generic;
using MomentumPlus.Activate.Web.Areas.Home.Models;
using MomentumPlus.Activate.Web.Models.Home;
using MomentumPlus.Activate.Web.Models.People;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Models
{
    public class ReportViewModel : BaseViewModel
    {
        public enum SortColumn
        {
            FirstName, LastName, Position, Workplace
        }

        public IEnumerable<Package> Packages { get; set; }

        public Guid PackageId { get; set; }
        public bool IsAncestor { get; set; }

        public bool IsPdf { get; set; }

        public UserViewModel Pickup { get; set; }
        public UserViewModel Leaver { get; set; }

        public RelationshipsViewModel Meetings { get; set; }
        public MeetingsViewModel FirstMeeting { get; set; }
        public IEnumerable<TopicGroupReportViewModel> CriticalTopics { get; set; }
        public IEnumerable<TopicGroupReportViewModel> ExperiencesTopics { get; set; }
        public IEnumerable<OnboardingTaskItemViewModel> OnboardingTasks { get; set; }

        public Guid PackageOwnerId { get; set; }
        public IDictionary<Guid, ContactChartViewModel> GraphNodes { get; set; }
        public Guid? DefaultCompanyId { get; set; }

        public bool SortAscending { get; set; }
    }
}