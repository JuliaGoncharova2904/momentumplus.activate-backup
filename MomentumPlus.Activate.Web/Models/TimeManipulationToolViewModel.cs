﻿using System;

namespace MomentumPlus.Activate.Web.Models
{
    public class TimeManipulationToolViewModel
    {
        public DateTime? DateTime { get; set; }
    }
}