﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Activate.Web.Models.Home
{
    public class OnboardingTopicsViewModel
    {
        public IEnumerable<OnboardingTopicItemViewModel> CriticalItems { get; set; }        
        public int CriticalPageNo { get; set; }
        public int CriticalTotalCount { get; set; }
        public bool CriticalSortAscending { get; set; }
        public IEnumerable<OnboardingTopicItemViewModel> ExperienceItems { get; set; }
        public int ExperiencesPageNo { get; set; }        
        public int ExperiencesTotalCount { get; set; }
        public bool ExperiencesSortAscending { get; set; }
        public int PageSize { get; set; }
        public List<int> ExpandedTopics { get; set; }
        public Guid PackageId { get; set; }
    }
}