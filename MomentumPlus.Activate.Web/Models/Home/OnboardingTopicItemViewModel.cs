﻿using System;

namespace MomentumPlus.Activate.Web.Models.Home
{
    public class OnboardingTopicItemViewModel : BaseViewModel
    {
        public enum SortColumn
        {
            NameGroup, NameTopic, HasAudio, HasFiles
        }

        public Guid TopicId { get; set; }
        public string NameGroup { get; set; }
        public string NameTopic { get; set; }
        public bool HasAudio { get; set; }
        public bool HasFiles { get; set; }
    }
}