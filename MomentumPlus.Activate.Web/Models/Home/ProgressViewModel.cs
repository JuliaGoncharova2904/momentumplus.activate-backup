﻿namespace MomentumPlus.Activate.Web.Models.Home
{
    public class ProgressViewModel
    {
        public int Ready { get; set; }
        public int Approved { get; set; }
    }
}
