﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MomentumPlus.Activate.Web.Models.Home
{
    public class TasksProjectsViewModel
    {
        public IEnumerable<TasksProjectsItemViewModel> Items { get; set; }
        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }
    }
}