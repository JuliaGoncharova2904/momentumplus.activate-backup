﻿using System.Collections.Generic;
using System.Web.Mvc;
using System;

namespace MomentumPlus.Activate.Web.Models.Home
{
    public class OnboardingTasksViewModel
    {
        public IEnumerable<OnboardingTaskItemViewModel> Items { get; set; }
        public int TotalWeeks { get; set; }
        public bool SortAscending { get; set; }
        public List<int> ExpandedWeeks { get; set; }
        public Guid PackageId { get; set; }

        public IEnumerable<SelectListItem> Ancestors { get; set; }
    }
}