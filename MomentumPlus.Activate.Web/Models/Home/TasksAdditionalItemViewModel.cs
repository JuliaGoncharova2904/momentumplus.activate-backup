﻿using MomentumPlus.Core.Models;
using System;

namespace MomentumPlus.Activate.Web.Models.Home
{
    public class TasksAdditionalItemViewModel : BaseViewModel
    {
        public enum SortColumn
        {
            NameTopic, NameTask, LastUpdated, Priority, Approved, HasAudio, HasFiles, WordCount
        }

        public string NameTopic { get; set; }
        public string NameTask { get; set; }
        public int WordCount { get; set; }
        public DateTime? LastUpdated { get; set; }
        public Task.TaskPriority Priority { get; set; }
        public bool Approved { get; set; }
        public bool HasAudio { get; set; }
        public bool HasFiles { get; set; }
    }
}