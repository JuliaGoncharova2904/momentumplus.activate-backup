﻿using System;

namespace MomentumPlus.Activate.Web.Models.Home
{
    public class ToDoItemViewModel
    {
        public enum SortColumn
        {
            NameTopic, NameModuleType, NameTopicGroup, LastViewed, Modified
        }

        public string NameTopic { get; set; }
        public string NameModuleType { get; set; }
        public string NameTopicGroup { get; set; }
        public Guid TopicId { get; set; }
        public DateTime? LastViewed { get; set; }
        public DateTime? Modified { get; set; }
    }
}