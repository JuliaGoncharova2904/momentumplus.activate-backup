﻿using MomentumPlus.Core.Models;
using System;

namespace MomentumPlus.Activate.Web.Models.Home
{
    public class OnboardingTaskItemViewModel : BaseViewModel
    {
        public enum SortColumn
        {
            NameTopic, NameTask, NameTopicGroup, NameModule, Priority, Complete, HasAudio, HasFiles
        }

        public string NameTopic { get; set; }
        public string NameTask { get; set; }
        public Guid TaskId { get; set; }
        public string NameTopicGroup { get; set; }
        public string NameModule { get; set; }
        public Task.TaskPriority Priority { get; set; }
        public bool Complete { get; set; }
        public DateTime? CompleteDate { get; set; }
        public bool HasAudio { get; set; }
        public bool HasFiles { get; set; }
        public int WeekDue { get; set; }

    }
}