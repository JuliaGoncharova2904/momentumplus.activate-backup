﻿using System;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Models.Home
{
    public class TasksRelationshipsItemViewModel : BaseViewModel
    {
        public enum SortColumn
        {
            NameContact, NameTask, LastUpdated, Priority, Approved, HasAudio, HasFiles, WordCount, Flag
        }

        public string NameContact { get; set; }
        public string NameTask { get; set; }
        public int WordCount { get; set; }
        public DateTime? LastUpdated { get; set; }
        public Task.TaskPriority Priority { get; set; }
        public bool Approved { get; set; }
        public bool HasAudio { get; set; }
        public bool HasFiles { get; set; }
        public PackageFlag Flag { get; set; }
    }
}