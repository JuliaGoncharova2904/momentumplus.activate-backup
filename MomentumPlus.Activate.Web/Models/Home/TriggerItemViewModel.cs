﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace MomentumPlus.Activate.Web.Models.Home
{
    public class TriggerItemViewModel
    {
        public enum SortColumn
        {
            Position, NameOwner, LastViewed
        }

        public Guid PackageId { get; set; }
        public Guid? HandoverId { get; set; }
        public string Position { get; set; }
        public string NameOwner { get; set; }
        public DateTime? LastViewed { get; set; }

        public bool CurrentUserIsHr { get; set; }
        public bool CurrentUserIsLm { get; set; }

    }
}