﻿using MomentumPlus.Core.Models;
using System.Collections.Generic;

namespace MomentumPlus.Activate.Web.Models.Home
{
    public class ToDoViewModel
    {
        public IEnumerable<ToDoItem> Items { get; set; }
        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }
    }
}
