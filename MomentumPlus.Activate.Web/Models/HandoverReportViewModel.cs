﻿using System;
using System.Collections.Generic;
using MomentumPlus.Activate.Web.Areas.Home.Models;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.People;

namespace MomentumPlus.Activate.Web.Models
{
    public class HandoverReportViewModel
    {
        // General
        public bool IsPdf { get; set; }
        public bool IsAncestor { get; set; }
        public Package Package { get; set; }

        public bool ShowMeetings { get; set; }
        public bool ShowCritical { get; set; }
        public bool ShowExperiences { get; set; }
        public bool ShowTasks { get; set; }
        public bool ShowPeople { get; set; }
        public bool ShowProjects { get; set; } 

        // Introduction
        public User Pickup { get; set; }

        // Meetings
        public IEnumerable<Meeting> Meetings { get; set; }

        // Relationships
        public IDictionary<Guid, ContactChartViewModel> GraphNodes { get; set; }
        public Guid? DefaultCompanyId { get; set; }

        // Critical
        public IEnumerable<TopicGroup> CriticalTopicGroups { get; set; }

        // Experiences
        public IEnumerable<TopicGroup> ExperiencesTopicGroups { get; set; }

        // Onboarding
        public IEnumerable<Task> Tasks { get; set; }

        // People
        public IEnumerable<Contact> StaffContacts { get; set; }
 
        // Projects
        public IEnumerable<ProjectInstance> ProjectInstances { get; set; } 

        // Onboarder Review Date - Critical Module
        public DateTime? OnboarderReviewDateCritical { get; set; }

        // Onboarder Review Date - Experiences Module
        public DateTime? OnboarderReviewDateExperiences { get; set; }

        // List of Topics and their last viewed date
        public List<AuditLog> OnboarderLastViewedTopicsCritical { get; set; }
        
        // List of Topics and their last viewed date
        public List<AuditLog> OnboarderLastViewedTopicsExperiences { get; set; }

        public bool IsLeavingPeriodLocked { get; set; }
        public bool IsOnboardingPeriodLocked { get; set; }

        public bool IsOnboarder { get; set; }
    }
}