﻿namespace MomentumPlus.Activate.Web.Models
{
    public class TooltipViewModel
    {
        public string Message { get; set; }
    }
}