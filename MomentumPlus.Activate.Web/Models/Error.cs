﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MomentumPlus.Activate.Web.Models
{
    public class Error
    {

        public string Title { get; set; }
        public HtmlString Message { get; set; }
    }
}