﻿namespace MomentumPlus.Activate.Web.Models
{
    public class AlertViewModel
    {
        public enum AlertType
        {
            Success,
            Info,
            Warning,
            Danger
        }

        public AlertType Type { get; set; }
        public string Message { get; set; }
    }
}