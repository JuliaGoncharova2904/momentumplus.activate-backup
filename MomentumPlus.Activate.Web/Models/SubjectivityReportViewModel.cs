﻿using System.Collections.Generic;
using MomentumPlus.Activate.Web.Models.People;

namespace MomentumPlus.Activate.Web.Models
{
    public class SubjectivityReportViewModel : BaseViewModel
    {
        public bool IsPdf { get; set; }
        public IEnumerable<PackageViewModel> Packages { get; set; }
        public IEnumerable<TopicViewModel> Topics { get; set; }
        public IEnumerable<TaskViewModel> Tasks { get; set; }
        public IEnumerable<StaffViewModel> People { get; set; }
        public IEnumerable<ProjectInstanceViewModel> Projects { get; set; }
        public List<SubjectiveWordViewModel> SubjectiveWords { get; set; }
    }
}
