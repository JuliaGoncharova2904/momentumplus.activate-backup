﻿using System;

namespace MomentumPlus.Activate.Web.Models
{
    public class TopicAnswerViewModel : BaseViewModel
    {
        public Guid? Value { get; set; }

        public string Text { get; set; }
    }
}
