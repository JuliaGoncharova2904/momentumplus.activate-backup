﻿using System.Web.Mvc;

namespace MomentumPlus.Activate.Web.Filters
{
    public class PerformanceTestFilterProvider : IFilterProvider
    {
        public System.Collections.Generic.IEnumerable<Filter> GetFilters(ControllerContext controllerContext, ActionDescriptor actionDescriptor)
        {
            return new[] { new Filter(new PerformanceTestFilter(), FilterScope.Global, 0) };
        }
    }
}