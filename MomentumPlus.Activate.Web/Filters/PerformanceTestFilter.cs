﻿using System.Diagnostics;
using System.Web.Mvc;
using NLog;

namespace MomentumPlus.Activate.Web.Filters
{
    public class PerformanceTestFilter : IActionFilter
    {
        private readonly Logger _logger = LogManager.GetLogger("PerformanceMonitoring");
        private readonly Stopwatch _stopWatch = new Stopwatch();

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            _stopWatch.Restart();
        }

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            _stopWatch.Stop();

            var controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            var actionName = filterContext.ActionDescriptor.ActionName;
            var url = GetUrl(filterContext);

            var message =
                string.Format("Controller:{1}; Action:{2}; Url:{3}; Milliseconds: {0};",
                    _stopWatch.ElapsedMilliseconds,
                    controllerName,
                    actionName,
                    url);

            _logger.Debug(message);
        }

        private static string GetUrl(ActionExecutedContext filterContext)
        {
            var url = string.Empty;

            var urlHelper = new UrlHelper(filterContext.RequestContext);
            var uri = urlHelper.RequestContext.HttpContext.Request.Url;
            if (uri != null)
                url = uri.ToString();
            return url;
        }
    }
}