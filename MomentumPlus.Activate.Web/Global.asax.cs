﻿using System;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using MomentumPlus.Activate.Web.ModelBinders;
using MomentumPlus.Activate.Web.Models;
using System.Collections.Generic;
using MomentumPlus.Activate.Services;
using MomentumPlus.Activate.Web.Helpers;
using System.Web.Helpers;
using System.Net;
using System.Configuration;
using NLog;
using MomentumPlus.Activate.Web.App_Start;
using MomentumPlus.Core.DataSource;

namespace MomentumPlus.Activate.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        private static Logger Logger = LogManager.GetCurrentClassLogger();

        #region Application Events

        protected void Application_Start()
        {
            Logger.Info("Application Starting");

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AutoMapperConfig.CreateMaps();
            DatabaseConfig.InitialiseDatabase();
            LightInjectConfig.RegisterServices();
            GlobalDateTime.SetDateTimeFormat();
            AntiForgeryConfig.SuppressIdentityHeuristicChecks = true;

            #if (!DEBUG)
                WarmUp.Start();
            #endif

            AddModelBinders();
            InitialiseAccountConfig();

            Logger.Info("Application Start has completed");
        }

        protected void Application_End()
        {
            var url = ConfigurationManager.AppSettings["KeepAliveUrl"];

            try
            {
                Logger.Info("Application keep alive begin request: " + url);

                WebRequest webRequest = HttpWebRequest.Create(url);
                webRequest.Method = "HEAD";
                webRequest.GetResponse();

                Logger.Info("Application keep alive success.");
            }
            catch (Exception ex)
            {
                Logger.Error("Application keep alive failure at " + DateTime.Now.ToString() + ": " + ex.Message);
            }
        }

        protected void Application_BeginRequest()
        {
            GlobalDateTime.SetDateTimeFormat();
        }

        protected void Application_EndRequest()
        {
            
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var app = (HttpApplication)sender;
            var context = app.Context;

            Exception ex = context.Server.GetLastError();
        }

        private bool IsLocalRequest(HttpRequest httpRequest)
        {
            return httpRequest.IsLocal;
        }

        #endregion

        #region Model Binders

        /// <summary>
        /// Register the custom model binders used by the application.
        /// </summary>
        private void AddModelBinders()
        {
            System.Web.Mvc.ModelBinders.Binders.Add(typeof(DateTime), new DateTimeModelBinder());
            System.Web.Mvc.ModelBinders.Binders.Add(typeof(DateTime?), new DateTimeModelBinder());
            System.Web.Mvc.ModelBinders.Binders.Add(typeof(ICollection<String>), new ICollectionStringModelBinder());
            System.Web.Mvc.ModelBinders.Binders.Add(typeof(IEnumerable<String>), new ICollectionStringModelBinder());
            System.Web.Mvc.ModelBinders.Binders.Add(typeof(IEnumerable<Guid>), new IEnumerableGuidModelBinder());
            System.Web.Mvc.ModelBinders.Binders.Add(typeof(TopicViewModel), new TopicViewModelBinder());
        }

        #endregion

        #region Account Config

        /// <summary>
        /// Load account configuration from database into an object that
        /// will be around for the lifetime of the application.
        /// </summary>
        private void InitialiseAccountConfig()
        {
            // Scope does not exist at this point, therefore can't use DI
            using (var context = new MomentumContext())
            {
                AccountConfig.Settings = new AccountConfigService(context).Get();
            }
        }

        #endregion
    }
}
