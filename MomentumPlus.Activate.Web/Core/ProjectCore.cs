﻿using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Models;
using System.Web.Mvc;
using MomentumPlus.Activate.Web.Areas.Home.Models;
using AutoMapper;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.Requests;

namespace MomentumPlus.Activate.Web.Core
{
    public class ProjectCore : BaseCore
    {
        #region Dependency Injection

        private IContactService ContactService;
        private IImageService ImageService;
        private IPackageService PackageService;
        private IProjectService ProjectService;
        private IRequestService RequestService;
        private IModerationService ModerationService;

        /// <summary>
        /// Construct a new <c>ProjectCore</c> instance with
        /// the given service implementations.
        /// </summary>
        /// <param name="masterListService"></param>
        /// <param name="projectService"></param>
        public ProjectCore(
            IContactService contactService,
            IImageService imageService,
            IMasterListService masterListService,
            IPackageService packageService,
            IProjectService projectService,
            IRequestService requestService,
            IModerationService moderationService
        )
            : base(masterListService)
        {
            ContactService = contactService;
            ImageService = imageService;
            PackageService = packageService;
            ProjectService = projectService;
            RequestService = requestService;
            ModerationService = moderationService;
        }

        #endregion

        #region Create

        /// <summary>
        /// Create a new Project entity from the given view model.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public Project CreateProject(ProjectViewModel viewModel)
        {
            var project = new Project();
            UpdateProjectFields(project, viewModel);
            ProjectService.Add(project);
            return project;
        }

        /// <summary>
        /// Create a new project instance in the given package from the
        /// given view model.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <param name="packageId"></param>
        /// <returns></returns>
        public ProjectInstance CreateProjectInstance(QuickAddProjectViewModel viewModel, Guid packageId)
        {
            ProjectInstance projectInstance = new ProjectInstance { PackageId = packageId };
            UpdateProjectInstanceFields(projectInstance, viewModel);
            ProjectService.Add(projectInstance);
            return projectInstance;
        }

        #endregion

        #region Read

        public ProjectsViewModel GetProjectsViewModel(
            ProjectInstance.SortColumn orderBy,
            bool sortAscending,
            int? projectStatus,
            int projectsPerPage,
            int pageNo,
            Guid packageId,
            bool retiredProjects
        )
        {
            var viewModel = new ProjectsViewModel
            {
                OrderBy = orderBy,
                SortAscending = sortAscending,
                ProjectStatus = projectStatus,
                ProjectsPerPage = projectsPerPage,
                PageNo = pageNo
            };

            viewModel.Statuses = GetProjectStatusesSelectList();

            var projectInstances = ProjectService.GetProjectInstances(
                packageId,
                projectStatus,
                pageNo,
                projectsPerPage,
                orderBy,
                sortAscending,
                retiredProjects,
                null
            );
            
            viewModel.ProjectInstances = Mapper.Map<IEnumerable<ProjectInstance>,
                IEnumerable<ProjectInstanceViewModel>>(projectInstances);

            foreach (var project in viewModel.ProjectInstances) 
            {
                if(project.ProjectId == null && project.NewProjectRequest != null)
                {
                    project.Project = GetDummyProjectViewModel(project.NewProjectRequest.SuggestedName, project.NewProjectRequest.SuggetestedProjectId);
                }

                // Add first task from leaver package to viewmodel
                if (project.Package.AncestorPackageId.HasValue && project.ProjectId.HasValue)
                {
                    var projectInstance = ProjectService.GetProjectInstanceByPackageAndProjectId(project.Package.AncestorPackageId.Value, project.ProjectId.Value);
                    if (projectInstance != null)
                        project.FirstTask = projectInstance.Tasks.OrderBy(t => t.Fixed).Any() ? projectInstance.Tasks.OrderBy(t => t.Fixed).FirstOrDefault() : null;
                }
            }

            viewModel.TotalCount = ProjectService.CountProjectIntances(
                packageId,
                projectStatus,
                retiredProjects,
                null
            );

            //var unapprovedCount = ProjectService.CountProjectIntances(packageId, null, approved: false);

            /*var unapprovedProjectInstances = ProjectService.GetProjectInstances(
                packageId,
                null,
                1,
                unapprovedCount,
                ProjectInstance.SortColumn.Created,
                true,
                approved: false
            );*/

            viewModel.NewProjectRequests = ModerationService.GetAllNotApproved<NewProjectRequest>(packageId);

            viewModel.UnapprovedProjectTransferRequests
                = RequestService.GetRequests<ProjectInstanceTransferRequest>(packageId);

            viewModel.UnapprovedProjectInstanceRoleChangeRequests
                = RequestService.GetRequests<ProjectInstanceRoleChangeRequest>(packageId);

            viewModel.UnapprovedCount = viewModel.NewProjectRequests.Count()
                + viewModel.UnapprovedProjectTransferRequests.Count()
                + viewModel.UnapprovedProjectInstanceRoleChangeRequests.Count();

            foreach (var projectInstanceViewModel in viewModel.ProjectInstances)
            {
                projectInstanceViewModel.AwaitingProjectManagerApproval =
                    viewModel.UnapprovedProjectInstanceRoleChangeRequests.Any(
                        r => r.ProjectInstanceId == projectInstanceViewModel.ID
                    );
            }

            return viewModel;
        }

        public QuickAddProjectViewModel GetQuickAddProjectViewModel(Guid packageId, Guid? projectInstanceId = null)
        {
            var viewModel = new QuickAddProjectViewModel { ProjectInstanceId = projectInstanceId };
            PopulateQuickAddProjectViewModel(viewModel, packageId);

            if (projectInstanceId.HasValue)
            {
                var projectInstance = ProjectService.GetProjectInstanceById(projectInstanceId.Value);
                if (projectInstance.NewProjectRequest != null)
                {
                    viewModel.ProjectName = projectInstance.NewProjectRequest.SuggestedName;
                    viewModel.ProjectId = projectInstance.NewProjectRequest.SuggetestedProjectId;
                }

            }

            return viewModel;
        }

        public TransferProjectCardViewModel GetTransferProjectCardViewModel(Guid id)
        {
            var projectInstance = ProjectService.GetProjectInstanceById(id);

            var viewModel = new TransferProjectCardViewModel
            {
                ProjectInstance = projectInstance,
                ProjectInstanceId = projectInstance.ID
            };

            PopulateTransferProjectCardViewModel(viewModel);

            return viewModel;
        }

        #endregion

        #region Update

        public Project UpdateProject(ProjectViewModel viewModel)
        {
            var project = ProjectService.GetProjectById(viewModel.ID);
            UpdateProjectFields(project, viewModel);
            ProjectService.Update(project);
            return project;
        }

        public ProjectInstance UpdateProjectInstance(QuickAddProjectViewModel viewModel)
        {
            var projectInstance = ProjectService.GetProjectInstanceById(viewModel.ProjectInstanceId.Value);
            UpdateProjectInstanceFields(projectInstance, viewModel);
            ProjectService.Update(projectInstance);
            return projectInstance;
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Populate a quick add project view model with the data required
        /// for rendering the quick add project form.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <param name="packageId"></param>
        public void PopulateQuickAddProjectViewModel(QuickAddProjectViewModel viewModel, Guid packageId)
        {
            var package = PackageService.GetByPackageId(packageId);
            var projectIds = package.Projects.Select(p => p.ProjectId);

            var activeProjects = ProjectService.GetProjects();
            var retiredProjects = ProjectService.GetProjects(true);

            var allProjects = activeProjects.ToList();
            allProjects.AddRange(retiredProjects);

            viewModel.ExistingProjects = allProjects.Where(p => !projectIds.Contains(p.ID))
                .Select(
                    p => new SelectListItem
                    {
                        Text = string.Join(" - ", p.Name, p.Status.Title) + (p.Deleted.HasValue ? " (Retired)" : " (Active)"),
                        Value = p.ID.ToString()
                    }
                )
                .OrderBy(p => p.Text);
        }

        /// <summary>
        /// Populate a project view model with the data required
        /// for rendering the project form.
        /// </summary>
        /// <param name="viewModel"></param>
        public void PopulateProjectViewModel(ProjectViewModel viewModel)
        {

           var package = PackageService.GetByProjectInstanceId(viewModel.ID);

            if (package != null)
            {
                viewModel.IsLeavingPackageState = package.PackageStatus == Package.Status.Triggered;
            }
            else
            {
                viewModel.IsLeavingPackageState = false;
            }


            viewModel.Contacts = ContactService.GetAllFramework().ToList().Select(c =>
                new SelectListItem { Text = c.FullName, Value = c.ID.ToString() }
            );

            viewModel.Images = ImageService.GetDefaultImages().Select(i =>
                new SelectListItem { Text = i.Title, Value = i.ID.ToString(), Selected = i.ID == viewModel.ImageId }
            );

            viewModel.Statuses = ProjectService.GetProjectStatuses().Select(s =>
                new SelectListItem { Text = s.Title, Value = s.ID.ToString() }
            );

            viewModel.Roles = ProjectService.GetProjectRoles().Select(r =>
                new SelectListItem {Text = r.Title, Value = r.ID.ToString()}
                );
            
        }

        public void PopulateTransferProjectCardViewModel(TransferProjectCardViewModel viewModel)
        {
            viewModel.Packages = PackageService.GetAll().Select(p =>
                new SelectListItem { Text = p.DisplayName, Value = p.ID.ToString() }
            );
        }

        /// <summary>
        /// Update the fields in the given project entity with the corresponding
        /// fields in the view model.
        /// </summary>
        /// <param name="project"></param>
        /// <param name="viewModel"></param>
        public void UpdateProjectFields(Project project, ProjectViewModel viewModel)
        {
            project.Name = viewModel.Name;
            project.ProjectId = viewModel.ProjectId;
            project.Start = viewModel.Start.Value;
            project.End = viewModel.End;
            project.ProjectManagerId = viewModel.ProjectManagerId;
            project.ImageId = viewModel.ImageId;
            project.StatusId = viewModel.StatusId;
            project.Budget = viewModel.Budget;
            project.ProjectSpaceLocation = viewModel.ProjectSpaceLocation;
            project.Description = viewModel.Description;
            project.Notes = viewModel.Notes;
            project.RoleId = viewModel.RoleId;
        }

        public void UpdateProjectInstanceFields(ProjectInstance projectInstance, QuickAddProjectViewModel viewModel)
        {
            if (viewModel.SelectExisting)
            {
                projectInstance.ProjectId = viewModel.ExistingProjectId;
                projectInstance.NewProjectRequest = null;
            }
            else
            {
                projectInstance.NewProjectRequest.SuggestedName = viewModel.ProjectName;
                projectInstance.NewProjectRequest.SuggetestedProjectId = viewModel.ProjectId;
            }
        }

        public IEnumerable<SelectListItem> GetProjectStatusesSelectList()
        {
            var statuses = ProjectService.GetProjectStatuses().Select(s =>
                new SelectListItem { Text = s.Title, Value = s.ReferenceId.ToString() }
            ).ToList();

            statuses.Insert(0, new SelectListItem { Text = "All", Value = string.Empty });

            return statuses;
        }

        public ProjectViewModel GetDummyProjectViewModel(string suggestedName, string suggestedId)
        {
            return new ProjectViewModel()
            {
                Name = suggestedName,
                ProjectId = suggestedId,
                Start = null,
                End = null,
                ImageId = ImageService.GetDefaultImages().FirstOrDefault().ID,
                Images = new List<SelectListItem> { new SelectListItem { Text = "TBA", Value = ImageService.GetDefaultImages().FirstOrDefault().ID.ToString() } },
                Contacts = new List<SelectListItem> { new SelectListItem { Text = "TBA" } },
                Statuses = new List<SelectListItem> { new SelectListItem { Text = "TBA" } },
                Roles = ProjectService.GetProjectRoles().Select(r =>
                    new SelectListItem { Text = r.Title, Value = r.ID.ToString() }
                ),
                Description = "TBA",
                Status = new ProjectStatusViewModel() { Title = "TBA" }
            };
        }

        public Project GetDummyProject(string suggestedName, string suggestedId)
        {
            return new Project()
            {
                Name = suggestedName,
                ProjectId = suggestedId,
                Description = "TBA"             
            };
        }

        #endregion
    }
}