﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using CsvHelper;
using CsvHelper.Configuration;
using MomentumPlus.Activate.Services;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Core
{
    public class TopicImporter : BaseImporter, IImporter, IDisposable
    {
        private List<Module> modules;

        #region CSV object

        public class TopicRow : IEquatable<TopicRow>
        {
            [Required]
            public string ModuleType { get; set; }
            [Required]
            [StringLength(75, MinimumLength = 3)]
            public string TopicGroupName { get; set; }
            [StringLength(50, MinimumLength = 3)]
            public string Name { get; set; }

            public override int GetHashCode()
            {
                return
                    string.Format("{0}{1}{2}",
                    ModuleType.ToLowerRemoveSpaces(),
                    TopicGroupName.ToLowerRemoveSpaces(),
                    Name.ToLowerRemoveSpaces()).GetHashCode();
            }

            public bool Equals(TopicRow other)
            {
                bool equal = false;

                if (this.ModuleType.ToLowerRemoveSpaces() == other.ModuleType.ToLowerRemoveSpaces() &&
                    this.TopicGroupName.ToLowerRemoveSpaces() == other.TopicGroupName.ToLowerRemoveSpaces() &&
                    this.Name.ToLowerRemoveSpaces() == other.Name.ToLowerRemoveSpaces())
                    equal = true;

                return equal;
            }
        }

        #endregion

        #region CSV object mapping

        public sealed class TopicRowMap : CsvClassMap<TopicRow>
        {
            public TopicRowMap()
            {
                Map(m => m.ModuleType).Name("ModuleType");
                Map(m => m.TopicGroupName).Name("TopicGroupName");
                Map(m => m.Name).Name("Name");
            }
        }

        #endregion

        #region Public methods

        public List<string> Import(Stream csv)
        {
            IEnumerable<TopicRow> rows =
                new List<TopicRow>();

            using (StreamReader streamReader = new StreamReader(csv))
            {
                CsvReader csvReader =
                    new CsvReader(streamReader);

                csvReader.Configuration.RegisterClassMap<TopicRowMap>();
                csvReader.Configuration.Delimiter = delimiter;
                csvReader.Configuration.IgnoreReadingExceptions = true;

                csvReader.Configuration.ReadingExceptionCallback = (ex, row) =>
                {
                    ProcessReadError(ex, row, ref errors);
                };

                rows =
                    csvReader.GetRecords<TopicRow>().ToList();
            }

            if (errors.Count == 0)
                errors.AddRange(ValidateDomainIntegrity((List<TopicRow>)rows));

            if (errors.Count == 0)
                errors.AddRange(ValidateDuplicates((List<TopicRow>)rows));

            if (errors.Count == 0)
            {
                modules =
                     unitOfWork.ModuleRepository.Find(m => m.IsFrameworkDefault && m.Deleted == null).ToList();

                // Get framework modules
                int rowIndex = 0;
                foreach (TopicRow row in rows)
                {
                    // Does module exist?
                    var module = modules.Find(
                        m => m.Type.ToString().ToLowerRemoveSpaces()
                            == row.ModuleType.ToLowerRemoveSpaces());

                    if (module != null)
                    {
                        var topicGroup = (from tg in module.TopicGroups
                                          where tg.Deleted == null &&
                                          tg.Name.ToLowerRemoveSpaces() == row.TopicGroupName.ToLowerRemoveSpaces()
                                          select tg).FirstOrDefault();

                        if (topicGroup != null)
                        {
                            bool exists =
                                topicGroup.Topics.Any
                                (t => t.Deleted == null &&
                                    t.Name.ToLower() == row.Name.ToLower());

                            if (!exists)
                            {
                                Topic topic = new Topic
                                {
                                    Name = row.Name
                                }; // add additional fields here

                                topicGroup.Topics.Add(topic);
                                unitOfWork.TopicGroupRepository.Update(topicGroup);
                            }
                            else
                            {
                                errors.Add(
                                        string.Format("Row {0}: Topic Name '{1}' already exists in the database",
                                        rowIndex + 1, row.Name));
                            }
                        }
                        else
                        {
                            errors.Add(
                              string.Format("Row {0}: Topic Group Name '{1}' does not exist in the database",
                              rowIndex + 1, row.TopicGroupName.ToString()));
                        }
                    }
                    else
                    {
                        errors.Add(
                           string.Format("Row {0}: Module Type '{1}' does not exist in the database",
                           rowIndex + 1, row.ModuleType.ToString()));
                    }

                    rowIndex++;
                }
            }

            if (errors.Count == 0)
                unitOfWork.Save();

            return errors;
        }

        #endregion

        public void Dispose()
        {
            modules = null;
            unitOfWork = null;

            GC.Collect();
        }
    }
}