﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using MomentumPlus.Core.DataSource;

namespace MomentumPlus.Activate.Web.Core
{
    public abstract class BaseImporter
    {
        protected RepositoriesUnitOfWork unitOfWork;
        protected const string delimiter = ",";

        protected List<string> errors = new List<string>();

        public BaseImporter()
        {
            unitOfWork = new RepositoriesUnitOfWork();
        }

        protected void ProcessReadError(Exception ex, CsvHelper.ICsvReader row, ref List<string> errors)
        {
            string error = null;

            if (ex is CsvHelper.CsvMissingFieldException)
                error = ex.Message.Replace("Fields ", "Field ").Replace(" do ", " does ");
            else
                error = string.Format("Row {0}: {1}", row.Parser.Row, ex.Message);

            if (!errors.Contains(error))
                errors.Add(error);
        }

        protected List<string> ValidateDuplicates<T>(List<T> objectsToValidate) where T : class
        {
            List<string> errorsDuplicates =
                new List<string>();

            HashSet<T> hashSet
                = new HashSet<T>();

            for (int i = 0; i < objectsToValidate.Count; i++)
            {
                if (!hashSet.Add(objectsToValidate[i]))
                {
                    errorsDuplicates.Add(
                        string.Format("Row {0}: Duplicate line in CSV", i + 1));
                }
            }

            if (errorsDuplicates.Any())
                errorsDuplicates.Add("");

            return errorsDuplicates;
        }

        protected List<string> ValidateDomainIntegrity<T>(List<T> objectsToValidate) where T : class
        {
            List<string> errorMessages =
                new List<string>();

            int rowIndex = 0;
            foreach (object objectToValidate in objectsToValidate)
            {
                IEnumerable<ValidationResult> errors =
                    ValidateDomainIntegrity(objectToValidate);

                StringBuilder objectErrorMessage =
                         new StringBuilder();

                if (errors.Count() > 0)
                {
                    foreach (ValidationResult result in errors)
                    {
                        errorMessages.Add(string.Format("Row {0}: {1}", rowIndex + 1, result));
                    }
                }

                rowIndex++;
            }

            return errorMessages;
        }

        public IEnumerable<ValidationResult> ValidateDomainIntegrity(Object objectToValidate)
        {
            var context =
                new ValidationContext(objectToValidate, null, null);

            var results =
                new List<ValidationResult>();

            Validator.TryValidateObject(objectToValidate, context, results, true);

            return results;
        }
    }
}