﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Models;
using NLog;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.People;

namespace MomentumPlus.Activate.Web.Core
{
    public class PackageCore
    {
        private readonly Logger _logger = LogManager.GetLogger("PerformanceMonitoring");
        private readonly Stopwatch _stopWatch = new Stopwatch();

        #region Dependency Injection

        private IAuditService AuditService;
        private IContactService ContactService;
        private IPackageService PackageService;
        private IPositionService PositionService;
        private IProjectService ProjectService;
        private ITemplateService TemplateService;
        private IUserService UserService;
        private IWorkplaceService WorkplaceService;

        public PackageCore(
            IAuditService auditService,
            IContactService contactService,
            IPackageService packageService,
            IPositionService positionService,
            IProjectService projectService,
            ITemplateService templateService,
            IUserService userService,
            IWorkplaceService workplaceService
        )
        {
            AuditService = auditService;
            ContactService = contactService;
            PackageService = packageService;
            PositionService = positionService;
            ProjectService = projectService;
            TemplateService = templateService;
            UserService = userService;
            WorkplaceService = workplaceService;
        }

        #endregion

        #region Create

        /// <summary>
        /// Create a new package entity based on the given view model.
        /// </summary>
        public Package CreatePackage(PackageViewModel viewModel)
        {
 
            
            var user = UserService.GetByUserId(viewModel.UserOwnerId.Value);
            _logger.Debug("Creating Package - UserService.GetByUserId");
       
            var position = PositionService.GetByPositionId(viewModel.PositionId.Value);
            _logger.Debug("Creating Package - PositionService.GetByPositionId");

            var workplace = WorkplaceService.GetByWorkplaceId(viewModel.WorkplaceId.Value);
            _logger.Debug("Creating Package - UserService.GetByUserId");

            var newPackage = PackageService.NewPackage();
            newPackage.Name = user.FirstName + " " + user.LastName + " - " + position.Name + " - " + workplace.Name;
            _logger.Debug("Creating Package - PackageService.NewPackage");

            UpdatePackageFields(newPackage, viewModel);
            _logger.Debug("Creating Package - UpdatePackageFields");

            newPackage = PackageService.Add(newPackage, position.TemplateId.Value, viewModel.ProjectIds, viewModel.ContactIds);
            _logger.Debug("Creating Package - PackageService.Add");

            PackageService.CreateMidAndEndReviewMeetings(newPackage, Helpers.AccountConfig.Settings.FiscalYearStartMonth);
            _logger.Debug("Creating Package - PackageService.CreateMidAndEndReviewMeetings");

            AuditService.RecordActivity<Package>(AuditLog.EventType.Created, newPackage.ID);
            _logger.Debug("Creating Package - AuditService.RecordActivity");

          

            return newPackage;
        }

  

        #endregion

        #region Update

        /// <summary>
        /// Update an existing package given its view model.
        /// </summary>
        public Package UpdatePackage(PackageViewModel viewModel)
        {
            var package = PackageService.GetByPackageId(viewModel.ID);

            UpdatePackageFields(package, viewModel);

            var user = UserService.GetByUserId(viewModel.UserOwnerId.Value);
            var position = PositionService.GetByPositionId(viewModel.PositionId.Value);
            var workplace = WorkplaceService.GetByWorkplaceId(viewModel.WorkplaceId.Value);

            package.Name = string.Format(
                "{0} {1} - {2} - {3}",
                user.FirstName,
                user.LastName,
                position.Name,
                workplace.Name
            );

            PackageService.Update(package);

            AuditService.RecordActivity<Package>(AuditLog.EventType.Modified, package.ID);

            return package;
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Populate a package view model with the data required for rendering the package form.
        /// </summary>
        public PackageViewModel PopulatePackageViewModel(
            Guid? selectedUserId = null,
            Guid? selectedWorkplaceId = null,
            Guid? selectedPositionId = null,
            PackageViewModel viewModel = null
        )
        {
            var packageStatus = Package.Status.New;
            var templates = TemplateService.GetAll();
            var users = UserService.GetAllIncludingRetired().OrderBy(u => u.FullName);
            var positions = PositionService.GetAll();
            var workplaces = WorkplaceService.GetAll();

            // Allow HR managers to be chosen as line managers
            var lineManagers = users.Where(u => u.Role != null && (u.Role.RoleParsed == Role.RoleEnum.LineManager ||
                u.Role.RoleParsed == Role.RoleEnum.HRManager || u.Role.RoleParsed == Role.RoleEnum.iHandoverAdmin || u.Role.RoleParsed == Role.RoleEnum.Administrator));
            var hrManagers = users.Where(u => u.Role != null && u.Role.RoleParsed == Role.RoleEnum.HRManager);

            // Create a new view model
            var packageViewModel = new PackageViewModel();
            if (viewModel != null)
            {
                packageViewModel = viewModel;
                packageViewModel.Editing = true;
            }
            else
            {
                // Set the package status
                packageViewModel.PackageStatus = packageStatus;

                // Set the package owner to the currently selected user
                if (selectedUserId.HasValue)
                {
                    Guid userId = selectedUserId.Value;
                    packageViewModel.UserOwnerId = userId;

                    try
                    {
                        User user = UserService.GetByUserId(userId);
                        Guid? positionId = user.DefaultPositionId;

                        if (positionId.HasValue)
                        {
                            packageViewModel.PositionId = positionId.Value;
                            selectedPositionId = positionId.Value;

                            try
                            {
                                Position position = PositionService.GetByPositionId(positionId.Value);
                                Guid? workplaceId = position.DefaultWorkplaceId;

                                if (workplaceId.HasValue) { packageViewModel.WorkplaceId = workplaceId.Value; }
                            }
                            catch { }
                        }
                    }
                    catch { }
                }

                // Set the package workplace to the currently selected workplace
                if (selectedWorkplaceId.HasValue) { packageViewModel.WorkplaceId = selectedWorkplaceId.Value; }

                // Set the package position to the currently selected position
                if (selectedPositionId.HasValue && !packageViewModel.Editing)
                {
                    packageViewModel.PositionId = selectedPositionId.Value;

                    // Enable relationships/projects dropdowns when enabled in the template
                    var position = PositionService.GetByPositionId(selectedPositionId.Value);
                    packageViewModel.ContactsEnabled = true;
                    packageViewModel.ProjectsEnabled = position.Template.ProjectsEnabled;
                }
            }

            // Generate values for Package Owner dropdown
            packageViewModel.UserOwners = new List<SelectListItem>
            (
                users.Select(u => new SelectListItem
                {
                    Text = u.FirstName + " " + u.LastName,
                    Value = u.ID.ToString()
                })
            );

            // Generate values for Position dropdown
            packageViewModel.Positions = new List<SelectListItem>
            (
                positions.Select(p => new SelectListItem
                {
                    Text = p.Name,
                    Value = p.ID.ToString()
                })
            );

            // Generate values for Workplace dropdown
            packageViewModel.Workplaces = new List<SelectListItem>
            (
                workplaces.Select(w => new SelectListItem
                {
                    Text = w.Name,
                    Value = w.ID.ToString()
                })
            );

            // Generate values for Line Manager dropdown
            packageViewModel.UserLineManagers = new List<SelectListItem>
            (
                lineManagers.Select(lm => new SelectListItem
                {
                    Text = lm.FirstName + " " + lm.LastName,
                    Value = lm.ID.ToString()
                })
            );

            // Generate values for HR Manager dropdown
            packageViewModel.UserHRManagers = new List<SelectListItem>
            (
                hrManagers.Select(hrm => new SelectListItem
                {
                    Text = hrm.FirstName + " " + hrm.LastName,
                    Value = hrm.ID.ToString()
                })
            );

            // Relationships dropdown
            packageViewModel.FrameworkContacts = GetFrameworkContactsSelectList();

            // Projects dropdown
            packageViewModel.FrameworkProjects = GetFrameworkProjectsSelectList();

            return packageViewModel;
        }

        /// <summary>
        /// Update a given package with the data from the view model.
        /// </summary>
        public void UpdatePackageFields(Package package, PackageViewModel viewModel)
        {
            package.UserOwnerId = viewModel.UserOwnerId.Value;
            package.PositionId = viewModel.PositionId.Value;
            package.WorkplaceId = viewModel.WorkplaceId.Value;
            package.UserLineManagerId = viewModel.UserLineManagerId.Value;
            package.UserHRManagerId = viewModel.UserHRManagerId.Value;
            package.StartDate = viewModel.StartDate;
        }

        public IEnumerable<SelectListItem> GetFrameworkContactsSelectList()
        {
            var relationships = ContactService.GetAllFramework().ToList();

            return new List<SelectListItem>
            (
                relationships.Select(r => new SelectListItem
                {
                    Text = r.FullName,
                    Value = r.ID.ToString()
                })
                .OrderBy(i => i.Text)
            );
        }

        public IEnumerable<SelectListItem> GetFrameworkProjectsSelectList()
        {
            var projects = ProjectService.GetProjects().ToList();

            return new List<SelectListItem>
            (
                projects.Select(p => new SelectListItem
                {
                    Text = p.Name,
                    Value = p.ID.ToString()
                })
                .OrderBy(i => i.Text)
            );
        }

        #endregion
    }
}