﻿using System;
using System.Linq;
using System.Web;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Models.People;
using System.IO;
using System.Web.Mvc;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.People;

namespace MomentumPlus.Activate.Web.Core
{
    public class PersonCore : BaseCore
    {
        #region Dependency Injection

        protected IPeopleService PeopleService;

        public PersonCore(
            IMasterListService masterListService,
            IPeopleService peopleService
        )
            : base(masterListService)
        {
            PeopleService = peopleService;
        }

        #endregion

        #region Helpers

        public void PopulatePersonViewModel(PersonViewModel viewModel)
        {
            viewModel.Companies =
                MasterListService.GetAll(MasterList.ListType.Companies).OrderBy(c => c.Name).Select(c =>
                    new SelectListItem { Text = c.Name, Value = c.ID.ToString() }
                    );

        }

        public void UpdatePersonFields(User person, PersonViewModel viewModel, HttpPostedFileBase profileImage)
        {
            person.Title = viewModel.Title;
            person.FirstName = viewModel.FirstName;
            person.LastName = viewModel.LastName;
            person.CompanyId = viewModel.CompanyId;
            if (!viewModel.CompanyId.HasValue || viewModel.CompanyId == Guid.Empty)
            {
                person.CompanyId = new Guid("{014F775C-8B77-48F3-8F3E-3B539405E8AF}");
            }
            person.OtherCompany = viewModel.OtherCompany;
            person.OtherCompanyName = viewModel.OtherCompanyName;
            person.Position = viewModel.Position;
            person.Email = viewModel.Email;
            person.Phone = viewModel.Phone;
            person.Mobile = viewModel.Mobile;

            if (profileImage != null)
            {
                MemoryStream memoryStream = new System.IO.MemoryStream();
                profileImage.InputStream.CopyTo(memoryStream);

                person.Image = person.Image ?? new Image();
                person.Image.BinaryData = memoryStream.ToArray();
                person.Image.FileType = Path.GetExtension(profileImage.FileName);
                person.Image.ContentType = profileImage.ContentType;
            }
        }

        public void UpdatePersonFields(Contact person, PersonViewModel viewModel, HttpPostedFileBase profileImage)
        {
            person.Title = viewModel.Title;
            person.FirstName = viewModel.FirstName;
            person.LastName = viewModel.LastName;
            person.CompanyId = viewModel.CompanyId;
            if (!viewModel.CompanyId.HasValue || viewModel.CompanyId == Guid.Empty)
            {
                person.CompanyId = new Guid("{014F775C-8B77-48F3-8F3E-3B539405E8AF}");
            }
            person.OtherCompany = viewModel.OtherCompany;
            person.OtherCompanyName = viewModel.OtherCompanyName;
            person.Position = viewModel.Position;
            person.Email = viewModel.Email;
            person.Phone = viewModel.Phone;
            person.Mobile = viewModel.Mobile;

            if (profileImage != null)
            {
                MemoryStream memoryStream = new System.IO.MemoryStream();
                profileImage.InputStream.CopyTo(memoryStream);

                person.Image = person.Image ?? new Image();
                person.Image.BinaryData = memoryStream.ToArray();
                person.Image.FileType = Path.GetExtension(profileImage.FileName);
                person.Image.ContentType = profileImage.ContentType;
            }
        }

        #endregion
    }
}