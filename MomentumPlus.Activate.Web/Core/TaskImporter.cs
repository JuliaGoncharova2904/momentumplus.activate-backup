﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using CsvHelper;
using CsvHelper.Configuration;
using MomentumPlus.Activate.Services;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Core
{
    public class TaskImporter : BaseImporter, IImporter, IDisposable
    {
        private List<Module> modules;

        #region CSV object

        public class TaskRow : IEquatable<TaskRow>
        {
            [Required]
            [StringLength(50, MinimumLength = 3)]
            public string ModuleType { get; set; }
            [Required]
            [StringLength(75, MinimumLength = 3)]
            public string TopicGroupName { get; set; }
            [StringLength(50, MinimumLength = 3)]
            public string TopicName { get; set; }
            [StringLength(50, MinimumLength = 3)]
            public string Name { get; set; }

            public override int GetHashCode()
            {
                return
                    string.Format("{0}{1}{2}{3}",
                    ModuleType.ToLowerRemoveSpaces(),
                    TopicGroupName.ToLowerRemoveSpaces(),
                    TopicName.ToLowerRemoveSpaces(),
                    Name.ToLowerRemoveSpaces()).GetHashCode();
            }

            public bool Equals(TaskRow other)
            {
                bool equal = false;

                if (this.ModuleType.ToLowerRemoveSpaces() == other.ModuleType.ToLowerRemoveSpaces() &&
                    this.TopicGroupName.ToLowerRemoveSpaces() == other.TopicGroupName.ToLowerRemoveSpaces() &&
                    this.TopicName.ToLowerRemoveSpaces() == other.TopicName.ToLowerRemoveSpaces() &&
                    this.Name.ToLowerRemoveSpaces() == other.Name.ToLowerRemoveSpaces())
                    equal = true;

                return equal;
            }
        }

        #endregion

        #region CSV object mapping

        public sealed class TaskRowMap : CsvClassMap<TaskRow>
        {
            public TaskRowMap()
            {
                Map(m => m.ModuleType).Name("ModuleType");
                Map(m => m.TopicGroupName).Name("TopicGroupName");
                Map(m => m.TopicName).Name("TopicName");
                Map(m => m.Name).Name("Name");
            }
        }

        #endregion

        #region Public methods

        public List<string> Import(Stream csv)
        {
            IEnumerable<TaskRow> rows =
                new List<TaskRow>();

            using (StreamReader streamReader = new StreamReader(csv))
            {
                CsvReader csvReader =
                    new CsvReader(streamReader);

                csvReader.Configuration.RegisterClassMap<TaskRowMap>();
                csvReader.Configuration.Delimiter = delimiter;
                csvReader.Configuration.IgnoreReadingExceptions = true;

                csvReader.Configuration.ReadingExceptionCallback = (ex, row) =>
                    {
                        ProcessReadError(ex, row, ref errors);
                    };

                rows =
                    csvReader.GetRecords<TaskRow>().ToList();
            }

            if (errors.Count == 0)
                errors.AddRange(ValidateDomainIntegrity((List<TaskRow>)rows));

            if (errors.Count == 0)
                errors.AddRange(ValidateDuplicates((List<TaskRow>)rows));

            if (errors.Count == 0)
            {
                modules =
                     unitOfWork.ModuleRepository.Find(m => m.IsFrameworkDefault && m.Deleted == null).ToList();

                // Get framework modules
                int rowIndex = 0;
                foreach (TaskRow row in rows)
                {
                    // Does module exist?
                    var module = modules.Find(
                        m => m.Type.ToString().ToLowerRemoveSpaces()
                            == row.ModuleType.ToString().ToLowerRemoveSpaces());

                    if (module != null)
                    {
                        var topicGroup = (from tg in module.TopicGroups
                                          where tg.Deleted == null &&
                                          tg.Name.ToLowerRemoveSpaces() == row.TopicGroupName.ToLowerRemoveSpaces()
                                          select tg).FirstOrDefault();

                        if (topicGroup != null)
                        {
                            var topic = (from t in topicGroup.Topics
                                         where t.Deleted == null &&
                                         t.Name.ToLowerRemoveSpaces() == row.TopicName.ToLowerRemoveSpaces()
                                         select t).FirstOrDefault();

                            if (topic != null)
                            {
                                // does task already exist?
                                bool exists = topic.Tasks.Any(t => t.Deleted == null && t.Name.ToLower() == row.Name.ToLower());

                                if (!exists)
                                {
                                    Task task = new Task
                                    {
                                        Name = row.Name
                                    }; // add additional fields here

                                    topic.Tasks.Add(task);
                                    unitOfWork.TopicRepository.Update(topic);
                                }
                                else
                                {
                                    errors.Add(
                                        string.Format("Row {0}: Task with Name '{1}' already exists in the database",
                                        rowIndex + 1, row.Name));
                                }
                            }
                            else
                            {
                                errors.Add(
                                    string.Format("Row {0}: Topic Name '{1}' does not exist in the database",
                                    rowIndex + 1, row.TopicName));
                            }
                        }
                        else
                        {
                            errors.Add(
                              string.Format("Row {0}: Topic Group Name '{1}' does not exist in the database",
                              rowIndex + 1, row.TopicGroupName.ToString()));
                        }
                    }
                    else
                    {
                        errors.Add(
                           string.Format("Row {0}: Module Type '{1}' does not exist in the database",
                           rowIndex + 1, row.ModuleType.ToString()));
                    }

                    rowIndex++;
                }
            }

            if (errors.Count == 0)
                unitOfWork.Save();



            return errors;
        }

        #endregion

        public void Dispose()
        {
            modules = null;
            unitOfWork = null;

            GC.Collect();
        }
    }
}