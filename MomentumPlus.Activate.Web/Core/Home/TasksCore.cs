﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using MomentumPlus.Activate.Services;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Models.Home;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Core.Home
{
    public class TasksCore : BaseCore
    {
        private IPackageService packageService;
        private readonly ITaskService TaskService;
        private IFlaggingService FlaggingService;
        private ITemplateService TemplateService;

        public TasksCore(
            IMasterListService masterListService,
            IPackageService packageService,
            ITaskService taskService,
            IFlaggingService flagginService,
            ITemplateService templateService
        )
            : base(masterListService)
        {
            this.packageService = packageService;
            TaskService = taskService;
            FlaggingService = flagginService;
            TemplateService = templateService;
        }

        // 5.7 Core & Other Tasks
        public TasksCoreViewModel GetCore(Guid packageId, Module.ModuleType moduleType, int pageNo, int pageSize, TasksCoreItemViewModel.SortColumn sortColumn, bool sortAscending)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            var package =
                packageService.GetByPackageId(packageId);

            IQueryable<TasksCoreItemViewModel> taskItems = TaskService.GetCoreTasksByModuleType(packageId, moduleType).Select(t =>
                                                            new TasksCoreItemViewModel
                                                            {
                                                                ID = t.ID,
                                                                Approved = t.IsApproved(),
                                                                HasAudio = t.HasVoiceMessages(),
                                                                HasFiles = t.HasAttachments(),
                                                                LastUpdated = t.UserLastViewed,
                                                                NameTask = t.Name,
                                                                NameTopic = t.ParentTopic.Name,
                                                                WordCount = t.CountWords(),
                                                                Priority = (Task.TaskPriority)Enum.Parse(typeof(Task.TaskPriority), t.Priority.ToString()),
                                                                Flag = FlaggingService.GetTaskFlag(t, packageId)
                                                            }).AsQueryable();


            int totalCount =
                taskItems.Count();

            SortAndPage<TasksCoreItemViewModel>(pageNo, pageSize, sortColumn.ToString(), sortAscending, ref taskItems);

            TasksCoreViewModel viewModel =
                new TasksCoreViewModel
                {
                    Items = taskItems,
                    PageNo = pageNo,
                    PageSize = pageSize,
                    TotalCount = totalCount
                };

            stopwatch.Stop();
            WriteCorePerfomanceLog("GetCore",
                stopwatch.Elapsed.Milliseconds);

            return viewModel;
        }

        // 5.7 Additional Core Tasks
        public TasksCoreViewModel GetAdditional(Guid packageId, int pageNo, int pageSize, TasksAdditionalItemViewModel.SortColumn sortColumn, bool sortAscending)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            var package =
                packageService.GetByPackageId(packageId);

            IQueryable<TasksCoreItemViewModel> taskItems = (from m in package.Modules
                                                            where m.Type == Module.ModuleType.Critical
                                                            from tg in m.TopicGroups
                                                            where tg.TopicGroupCategory == TopicGroup.Category.Core
                                                            from to in tg.Topics
                                                            from ta in to.Tasks
                                                            where !ta.SourceId.HasValue
                                                            select new TasksCoreItemViewModel
                                                            {
                                                                ID = ta.ID,
                                                                Approved = ta.IsApproved(),
                                                                HasAudio = ta.HasVoiceMessages(),
                                                                HasFiles = ta.HasAttachments(),
                                                                LastUpdated = ta.UserLastViewed,
                                                                NameTask = ta.Name,
                                                                NameTopic = to.Name,
                                                                WordCount = ta.CountWords(),
                                                                Priority = (Task.TaskPriority)Enum.Parse(typeof(Task.TaskPriority), ta.Priority.ToString()),
                                                                Flag = FlaggingService.GetTaskFlag(ta, packageId)
                                                            }).AsQueryable();


            int totalCount =
                taskItems.Count();

            SortAndPage<TasksCoreItemViewModel>(pageNo, pageSize, sortColumn.ToString(), sortAscending, ref taskItems);

            TasksCoreViewModel viewModel =
                new TasksCoreViewModel
                {
                    Items = taskItems,
                    PageNo = pageNo,
                    PageSize = pageSize,
                    TotalCount = totalCount
                };

            stopwatch.Stop();
            WriteCorePerfomanceLog("GetAdditional",
                stopwatch.Elapsed.Milliseconds);

            return viewModel;
        }

        public TasksCriticalViewModel GetCritical(Guid packageId, bool core, int pageNo, int pageSize, TasksCriticalItemViewModel.SortColumn sortColumn, bool sortAscending)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            var package =
                packageService.GetByPackageId(packageId);



            IQueryable<TasksCriticalItemViewModel> taskItemsIq;


            if (core)
            {
                var templateItemsTasks =
                    TemplateService.GetTemplateItems(package.Position.TemplateId.Value, Module.ModuleType.Critical)
                        .Where(t => t.Type == TemplateItem.TemplateItemType.Task && t.Enabled && t.IsCore);

                List<TasksCriticalItemViewModel> taskItems = (from templateItemsTask in templateItemsTasks
                                                              select
                                                                  TaskService.GetAllByPackageId(package.ID)
                                                                      .FirstOrDefault(tg => tg.Name.Equals(templateItemsTask.Name)) into task
                                                              where task != null
                                                              select new TasksCriticalItemViewModel
                                                              {
                                                                  ID = task.ID,
                                                                  Approved = task.IsApproved(),
                                                                  HasAudio = task.HasVoiceMessages(),
                                                                  HasFiles = task.HasAttachments(),
                                                                  LastUpdated = task.UserLastViewed,
                                                                  NameTask =
                                                                      Helpers.AccountConfig.Settings.ProLinkEnabled && task.ParentTopic.ProcessGroupId.HasValue
                                                                          ? String.Format("{0} - {1} - {2}", task.ParentTopic.ProcessGroup.Function.Name,
                                                                              task.ParentTopic.ProcessGroup.Name, task.Name)
                                                                          : task.Name,
                                                                  NameTopic = task.ParentTopic.Name,
                                                                  WordCount = task.CountWords(),
                                                                  Priority = (Task.TaskPriority)Enum.Parse(typeof(Task.TaskPriority), task.Priority.ToString()),
                                                                  Flag = FlaggingService.GetTaskFlag(task, packageId)
                                                              }).ToList();


                taskItemsIq = taskItems.AsQueryable();
            }
            else
            {
                taskItemsIq = (from m in package.Modules
                               where m.Type == Module.ModuleType.Critical
                               from tg in m.TopicGroups
                               from to in tg.Topics
                               from ta in to.Tasks
                               where ta.IsCore == core
                               select new TasksCriticalItemViewModel
                               {
                                   ID = ta.ID,
                                   Approved = ta.IsApproved(),
                                   HasAudio = ta.HasVoiceMessages(),
                                   HasFiles = ta.HasAttachments(),
                                   LastUpdated = ta.UserLastViewed,
                                   NameTask = Helpers.AccountConfig.Settings.ProLinkEnabled && ta.ParentTopic.ProcessGroupId.HasValue
                                       ? String.Format("{0} - {1} - {2}", ta.ParentTopic.ProcessGroup.Function.Name, ta.ParentTopic.ProcessGroup.Name, ta.Name)
                                       : ta.Name,
                                   NameTopic = to.Name,
                                   WordCount = ta.CountWords(),
                                   Priority = (Task.TaskPriority)Enum.Parse(typeof(Task.TaskPriority), ta.Priority.ToString()),
                                   Flag = FlaggingService.GetTaskFlag(ta, packageId)
                               }).AsQueryable();
            }


            int totalCount =
                taskItemsIq.Count();

            SortAndPage<TasksCriticalItemViewModel>(pageNo, pageSize, sortColumn.ToString(), sortAscending, ref taskItemsIq);

            TasksCriticalViewModel viewModel =
                new TasksCriticalViewModel
                {
                    Items = taskItemsIq,
                    PageNo = pageNo,
                    PageSize = pageSize,
                    TotalCount = totalCount
                };

            stopwatch.Stop();
            WriteCorePerfomanceLog("GetCritical",
                stopwatch.Elapsed.Milliseconds);

            return viewModel;
        }

        public TasksExperiencesViewModel GetExperience(Guid packageId, bool core, int pageNo, int pageSize, TasksExperiencesItemViewModel.SortColumn sortColumn, bool sortAscending)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            var package =
                packageService.GetByPackageId(packageId);



            IQueryable<TasksExperiencesItemViewModel> taskItemsIq;


            if (core)
            {
                var templateItemsTasks =
                    TemplateService.GetTemplateItems(package.Position.TemplateId.Value, Module.ModuleType.Experiences)
                        .Where(t => t.Type == TemplateItem.TemplateItemType.Task && t.Enabled);

                List<TasksExperiencesItemViewModel> taskItems = (from templateItemsTask in templateItemsTasks
                                                                 select
                                                                     TaskService.GetAllByPackageId(package.ID)
                                                                         .FirstOrDefault(tg => tg.Name.Equals(templateItemsTask.Name)) into task
                                                                 where task != null
                                                                 select new TasksExperiencesItemViewModel
                                                                 {
                                                                     ID = task.ID,
                                                                     Approved = task.IsApproved(),
                                                                     HasAudio = task.HasVoiceMessages(),
                                                                     HasFiles = task.HasAttachments(),
                                                                     LastUpdated = task.UserLastViewed,
                                                                     NameTask = task.Name,
                                                                     NameTopic = task.ParentTopic.Name,
                                                                     WordCount = task.CountWords(),
                                                                     Priority = (Task.TaskPriority)Enum.Parse(typeof(Task.TaskPriority), task.Priority.ToString()),
                                                                     Flag = FlaggingService.GetTaskFlag(task, packageId)
                                                                 }).ToList();


                taskItemsIq = taskItems.AsQueryable();
            }
            else
            {
                taskItemsIq = (from m in package.Modules
                               where m.Type == Module.ModuleType.Experiences
                               from tg in m.TopicGroups
                               from to in tg.Topics
                               from ta in to.Tasks
                               where ta.IsCore == core
                               select new TasksExperiencesItemViewModel
                               {
                                   ID = ta.ID,
                                   Approved = to.IsApproved(),
                                   HasAudio = ta.HasVoiceMessages(),
                                   HasFiles = ta.HasAttachments(),
                                   LastUpdated = ta.UserLastViewed,
                                   NameTask = ta.Name,
                                   NameTopic = to.Name,
                                   WordCount = ta.CountWords(),
                                   Priority = (Task.TaskPriority)Enum.Parse(typeof(Task.TaskPriority), ta.Priority.ToString()),
                                   Flag = FlaggingService.GetTaskFlag(ta, packageId)
                               }).AsQueryable();
            }




            int totalCount = taskItemsIq.Count();

            SortAndPage<TasksExperiencesItemViewModel>(pageNo, pageSize, sortColumn.ToString(), sortAscending, ref taskItemsIq);

            TasksExperiencesViewModel viewModel =
                new TasksExperiencesViewModel
                {
                    Items = taskItemsIq,
                    PageNo = pageNo,
                    PageSize = pageSize,
                    TotalCount = totalCount
                };

            stopwatch.Stop();
            WriteCorePerfomanceLog("GetExperience",
                stopwatch.Elapsed.Milliseconds);

            return viewModel;
        }

        public TasksProjectsViewModel GetProjects(Guid packageId, bool core, int pageNo, int pageSize, TasksProjectsItemViewModel.SortColumn sortColumn, bool sortAscending)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            var package = packageService.GetByPackageId(packageId);


            IQueryable<TasksProjectsItemViewModel> taskItemsIq;


            if (core)
            {
                var templateItemsTasks =
                    TemplateService.GetTemplateItems(package.Position.TemplateId.Value, Module.ModuleType.Project)
                        .Where(t => t.Type == TemplateItem.TemplateItemType.Task && t.Enabled);

                List<TasksProjectsItemViewModel> taskItems = (from templateItemsTask in templateItemsTasks
                                                                 select
                                                                     TaskService.GetAllByPackageId(package.ID)
                                                                         .FirstOrDefault(tg => tg.Name.Equals(templateItemsTask.Name)) into task
                                                                 where task != null
                                                                 select new TasksProjectsItemViewModel
                                                                 {
                                                                     ID = task.ID,
                                                                     Approved = task.IsApproved(),
                                                                     HasAudio = task.HasVoiceMessages(),
                                                                     HasFiles = task.HasAttachments(),
                                                                     LastUpdated = task.UserLastViewed,
                                                                     NameTask = task.Name,
                                                                     NameProject = task.ProjectInstace.Project.Name,
                                                                     WordCount = task.CountWords(),
                                                                     Priority = (Task.TaskPriority)Enum.Parse(typeof(Task.TaskPriority), task.Priority.ToString()),
                                                                     Flag = FlaggingService.GetTaskFlag(task, packageId)
                                                                 }).ToList();


                taskItemsIq = taskItems.AsQueryable();
            }
            else
            {
                taskItemsIq = TaskService.GetAllByPackageId(packageId)
                    .AsQueryable()
                    .Include(t => t.ProjectInstace.Project)
                    .Where(t => t.ProjectInstanceId != null)
                    .Where(t => t.ParentTopic != null)
                    .ToList().Select(p => new TasksProjectsItemViewModel
                {
                    ID = p.ID,
                    Approved = p.IsApproved(),
                    HasAudio = p.HasVoiceMessages(),
                    HasFiles = p.HasAttachments(),
                    LastUpdated = p.UserLastViewed,
                    NameTask = p.Name,
                    NameProject = p.ProjectInstace.Project.Name,
                    WordCount = p.CountWords(),
                    Priority = (Task.TaskPriority)Enum.Parse(typeof(Task.TaskPriority), p.Priority.ToString()),
                    Flag = FlaggingService.GetTaskFlag(p, packageId)
                }).AsQueryable();
            }



            var totalCount = taskItemsIq.Count();

            SortAndPage<TasksProjectsItemViewModel>(pageNo, pageSize, sortColumn.ToString(), sortAscending, ref taskItemsIq);

            var viewModel = new TasksProjectsViewModel
            {
                Items = taskItemsIq,
                PageNo = pageNo,
                PageSize = pageSize,
                TotalCount = totalCount
            };

            stopwatch.Stop();
            WriteCorePerfomanceLog("GetProjects", stopwatch.Elapsed.Milliseconds);

            return viewModel;
        }

        public TasksRelationshipsViewModel GetRelationships(Guid packageId, TopicGroup.Category category, int pageNo, int pageSize, TasksRelationshipsItemViewModel.SortColumn sortColumn, bool sortAscending)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            var package =
                packageService.GetByPackageId(packageId);

            IQueryable<TasksRelationshipsItemViewModel> tasks = (from mo in package.Modules
                                                                 from tg in mo.TopicGroups
                                                                 where tg.TopicGroupCategory == category
                                                                 from c in package.Contacts
                                                                 from me in c.Tasks
                                                                 select new TasksRelationshipsItemViewModel
                                                                 {
                                                                     ID = me.ID,
                                                                     NameTask = me.Name,
                                                                     NameContact = c.FullName,
                                                                     WordCount = me.CountWords(),
                                                                     LastUpdated = me.UserLastViewed,
                                                                     Priority = (Task.TaskPriority)Enum.Parse(typeof(Task.TaskPriority), me.Priority.ToString()),
                                                                     Approved = me.IsApproved(),
                                                                     HasAudio = me.HasVoiceMessages(),
                                                                     HasFiles = me.HasAttachments(),
                                                                     Flag = FlaggingService.GetFlag(me)
                                                                 }).MyDistinct(t => t.ID).AsQueryable();

            int totalCount = tasks.Count();

            SortAndPage<TasksRelationshipsItemViewModel>(pageNo, pageSize, sortColumn.ToString(), sortAscending, ref tasks);

            TasksRelationshipsViewModel viewModel =
                new TasksRelationshipsViewModel
                {
                    Items = tasks,
                    PageNo = pageNo,
                    PageSize = pageSize,
                    TotalCount = totalCount
                };

            stopwatch.Stop();
            WriteCorePerfomanceLog("GetRelationships",
                stopwatch.Elapsed.Milliseconds);

            return viewModel;
        }
    }
}