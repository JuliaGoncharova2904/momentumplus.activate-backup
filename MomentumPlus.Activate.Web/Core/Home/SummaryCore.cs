﻿using System;
using System.Diagnostics;
using System.Linq;
using MomentumPlus.Activate.Services;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Activate.Web.Models.Home;
using System.Collections.Generic;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Core.Home
{
    public class SummaryCore : BaseCore
    {
        private IPackageService packageService;

        public SummaryCore(
            IMasterListService masterListService,
            IPackageService packageService
        )
            : base(masterListService)
        {
            this.packageService = packageService;
        }

        /// <summary>
        /// Get Package by Package Id
        /// Filter where Topics are set to 'Ready for Review'
        /// </summary>
        /// <param name="packageId"></param>
        /// <param name="pageNo"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortAscending"></param>
        /// <returns></returns>
        public ToDoViewModel GetToDo(Guid packageId, int pageNo, int pageSize)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            Package package = packageService.GetByPackageId(packageId);

            IEnumerable<ToDoItem> toDoItems = packageService.GetToDoItems(packageId, pageNo, pageSize);
            int totalCount = toDoItems.Count();

            ToDoViewModel toDoViewModel =
                new ToDoViewModel
                {
                    Items = toDoItems,
                    TotalCount = totalCount,
                    PageNo = pageNo,
                    PageSize = pageSize
                };

            stopwatch.Stop();
            WriteCorePerfomanceLog("GetToDo",
                stopwatch.Elapsed.Milliseconds);

            return toDoViewModel;
        }

        /// <summary>
        /// Get Package by Package Id
        /// Counts based on Task 'Ready for Review'
        /// </summary>
        /// <param name="packageId"></param>
        /// <returns></returns>
        public ProgressViewModel GetProgress(Guid packageId)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            Package package =
                packageService.GetByPackageId(packageId);

            var topics = from m in package.Modules
                         from tg in m.TopicGroups
                         from to in tg.Topics
                         select to;

            var frameworkTopics = from to in topics
                                  where to.SourceId.HasValue
                                  select to;

            int countFrameworkTopics = frameworkTopics.Count();
            int countFrameworkTopicsComplete = (from ft in frameworkTopics
                                                where ft.ReadyForReview
                                                select ft).Count();

            int overallPercentageComplete = countFrameworkTopics > 1 ?
                (100 * countFrameworkTopicsComplete / countFrameworkTopics) :
                0;

            var tasks = from to in topics
                        from ta in to.Tasks
                        select ta;

            int countTasksOverall = tasks.Count();
            int countTasksComplete = (from t in tasks
                                      where t.ReadyForReview == true
                                      select t).Count();

            int tasksPercentageComplete = countTasksOverall > 1 ?
                (100 * countTasksComplete / countTasksOverall) :
                0;

            ProgressViewModel viewModel =
                new ProgressViewModel
                {
                    Approved = overallPercentageComplete,
                    Ready = tasksPercentageComplete
                };

            stopwatch.Stop();
            WriteCorePerfomanceLog("GetProgress",
                stopwatch.Elapsed.Milliseconds);

            return viewModel;
        }

        /// <summary>
        /// Get all packages where user is Owner, Line or HR manager
        /// Filter where Owner is not null, has a Trigger and is not Approved
        /// </summary>
        /// <param name="pageNo"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortAscending"></param>
        /// <returns></returns>
        public TriggersViewModel GetTriggers(int pageNo, int pageSize, TriggerItemViewModel.SortColumn sortColumn, bool sortAscending)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            //populate only Outdated(Finalise) package AC=156

            //var packagesOnboarding = packageService.GetByPackageStatus(Package.Status.New, false).ToList();
            //var packagesLeaving = packageService.GetByPackageStatus(Package.Status.Triggered, false).ToList();
            //var packages = packagesOnboarding.Union(packagesLeaving);

            var packages = packageService.GetAllFinalisePackage();

            IQueryable<TriggerItemViewModel> triggerItems = (from p in packages
                                                             where
                                                                 p.UserOwner != null
                                                             select new TriggerItemViewModel
                                                             {
                                                                 Position = p.Position != null ? p.Position.Name : null,
                                                                 NameOwner = GetName(p.UserOwner),
                                                                 LastViewed = p.UserLastViewed,
                                                                 PackageId = p.ID,
                                                                 HandoverId = p.Handover.ID, //== Package.Status.Triggered ? p.ID : p.AncestorPackageId.HasValue ? (Guid?)packageService.GetByPackageId(p.AncestorPackageId.Value).ID : null,
                                                                 CurrentUserIsHr = p.UserHRManager.ID == AppSession.Current.CurrentUser.ID,
                                                                 CurrentUserIsLm = p.UserLineManager.ID == AppSession.Current.CurrentUser.ID
                                                             }).AsQueryable().OrderBy(p => p.NameOwner);

            int totalCount =
                triggerItems.Count();

            SortAndPage<TriggerItemViewModel>(pageNo, pageSize, sortColumn.ToString(), sortAscending, ref triggerItems);

            TriggersViewModel triggerViewModel
                = new TriggersViewModel
                {
                    PageNo = pageNo,
                    PageSize = pageSize,
                    TotalCount = totalCount,
                    Triggers = triggerItems
                };

            stopwatch.Stop();
            WriteCorePerfomanceLog("GetTriggers",
                stopwatch.Elapsed.Milliseconds);

            return triggerViewModel;
        }


        /// <summary>
        /// Get all Contact where user is Owner, Line or HR manager
        /// Filter where Owner is not null, has a Trigger and is not Approved
        /// </summary>
        /// <param name="pageNo"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortAscending"></param>
        /// <returns></returns>
        public IQueryable<Contact> GetTriggers()
        {

            var packagesOnboarding = packageService.GetByPackageStatus(Package.Status.New, false).ToList();
            var packagesLeaving = packageService.GetByPackageStatus(Package.Status.Triggered, false).ToList();
            var packages = packagesOnboarding.Union(packagesLeaving);

            IQueryable<Contact> triggerItems = (from p in packages
                                                where
                                                    p.UserOwner != null
                                                select new Contact
                                                {
                                                    Position = p.Position != null ? p.Position.Name : null,

                                                    FullName = GetName(p.UserOwner),
                                                    FirstName = GetName(p.UserOwner),
                                                    PackageId = p.ID,
                                                }).AsQueryable().OrderBy(p => p.Name);


            return triggerItems;
        }

        /// <summary>
        /// Get all Topics where user is Owner, Line or HR manager
        /// Filter where Topic is not ready for review
        /// </summary>
        /// <param name="pageNo"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortAscending"></param>
        /// <returns></returns>
        public AgedViewModel GetAged(int pageNo, int pageSize, AgedItemViewModel.SortColumn sortColumn, bool sortAscending)
        {

            //populate Onboarding, Ongoing, Leaving package AC-156
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            var packagesOnboarding = packageService.GetByPackageStatus(Package.Status.New, false).ToList();
            //var packagesLeaving = packageService.GetByPackageStatus(Package.Status.Triggered, false).ToList();
            var packagesOngoing = packageService.GetByPackageStatus(Package.Status.InProgress, false).ToList();
            var packagesLeaving = packageService.GetByPackageStatus(Package.Status.Triggered, false).Where(p => p.Handover.LeavingPeriod.Locked.HasValue && p.Handover.LeavingPeriod.LastDay.HasValue && p.Handover.LeavingPeriod.LastDay.Value > System.DateTime.Now);
            var packages = packagesOngoing.Union(packagesOnboarding).Union(packagesLeaving);
            //var packages = packagesOnboarding.Union(packagesLeaving);
            

            IQueryable<AgedItemViewModel> agedViewItems = (from p in packages
                                                           from m in p.Modules
                                                           where m.Type == Module.ModuleType.Critical
                                                           from tg in m.TopicGroups
                                                           from t in tg.Topics
                                                           select new AgedItemViewModel
                                                           {
                                                               Position = p.Position != null ? p.Position.Name : null,
                                                               NameOwner = GetName(p.UserOwner),
                                                               LastViewed = p.UserLastViewed,
                                                               PackageId = p.ID,
                                                               CurrentUserIsHr = p.UserHRManager.ID == AppSession.Current.CurrentUser.ID,
                                                               CurrentUserIsLm = p.UserLineManager.ID == AppSession.Current.CurrentUser.ID
                                                           }).MyDistinct(p => p.PackageId).AsQueryable().OrderBy(p => p.NameOwner);

            int totalCount = agedViewItems.Count();

            SortAndPage<AgedItemViewModel>(pageNo, pageSize, sortColumn.ToString(), sortAscending, ref agedViewItems);

            AgedViewModel agedView =
                new AgedViewModel
                {
                    Items = agedViewItems.ToList(),
                    PageNo = pageNo,
                    PageSize = pageSize,
                    TotalCount = totalCount
                };

            stopwatch.Stop();
            WriteCorePerfomanceLog("GetAged", stopwatch.Elapsed.Milliseconds);

            return agedView;
        }
    }
}
