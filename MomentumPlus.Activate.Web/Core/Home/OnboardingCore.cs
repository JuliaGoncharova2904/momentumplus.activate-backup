﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using MomentumPlus.Activate.Services;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Areas.Home.Models;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Activate.Web.Models.Home;
using MomentumPlus.Activate.Web.Models.People;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.People;

namespace MomentumPlus.Activate.Web.Core.Home
{
    public class OnboardingCore : BaseCore
    {
        private readonly IPackageService packageService;
        private readonly IMeetingService meetingService;
        private readonly ITaskService taskService;

        public OnboardingCore(
            IPackageService packageService,
            IMasterListService masterListService,
            IMeetingService meetingService,
            ITaskService taskService
        )
            : base(masterListService)
        {
            this.packageService = packageService;
            this.meetingService = meetingService;
            this.taskService = taskService;
        }

        public UserViewModel GetLeaver(Guid packageId)
        {
            Package package = packageService.GetByPackageId(packageId);
            Package ancestorPackage = packageService.GetByPackageId(package.AncestorPackageId.Value);
            User ancestorOwner = ancestorPackage.UserOwner;

            return Mapper.Map<User, UserViewModel>(ancestorOwner);
        }

        public UserViewModel GetPickup(Guid packageId)
        {
            Package package = packageService.GetByPackageId(packageId);
            User owner = package.UserOwner;

            var returnable = Mapper.Map<User, UserViewModel>(owner);
            returnable.UserLineManagerName = package.UserLineManager.FirstName + " " + package.UserLineManager.LastName;
            return returnable;
        }

        public UserViewModel GetPackageOwner(Guid packageId)
        {
            Package package = packageService.GetByPackageId(packageId);
            User owner = package.UserOwner;

            var returnable = Mapper.Map<User, UserViewModel>(owner);

            returnable.UserLineManagerName = package.UserLineManager.FirstName + " " + package.UserLineManager.LastName;

            return returnable;
        }

        // 5.9.5 & 5.9.7
        public IEnumerable<TopicGroupReportViewModel> GetTopicGroups(Guid packageId, Module.ModuleType moduleType)
        {
            var package =
                packageService.GetByPackageId(packageId);

            var items = (from m in package.Modules
                         where m.Type == moduleType
                         from tg in m.TopicGroups
                         from to in tg.Topics
                         select new TopicGroupReportViewModel
                         {
                             TopicGroupType = tg.TopicGroupType,
                             TopicGroupTitle = tg.Title,
                             TopicGroupName = tg.Name,
                             TopicName = to.Name,
                             TopicId = to.ID,
                             TopicHasAudio = to.HasVoiceMessages(),
                             TopicHasAttachments = to.HasAttachments(),
                             TopicAttachmentsCount = to.Attachments.Count() + to.DocumentLibraries.Count(),
                             VoiceMessagesCount = to.VoiceMessages.Count(),
                             TopicNotes = to.Notes.Truncate(50)
                         }).MyDistinct(t => t.TopicId);

            return items;
        }

        // 5.9.6
        public IEnumerable<OnboardingTaskItemViewModel> GetTasks(Guid packageId)
        {
            var package =
              packageService.GetByPackageId(packageId);

            var items = (from m in package.Modules
                         from tg in m.TopicGroups
                         from to in tg.Topics
                         from ta in to.Tasks
                         select new OnboardingTaskItemViewModel
                         {
                             NameTopic = to.Name,
                             NameTask = ta.Name,
                             TaskId = ta.ID,
                             NameTopicGroup = tg.Name,
                             NameModule = m.Type.ToString(),
                             Priority = ta.Priority,
                             Complete = ta.Complete,
                             HasAudio = ta.HasVoiceMessages(),
                             HasFiles = ta.HasAttachments(),
                             AttachmentsCount = to.Attachments.Count() + to.DocumentLibraries.Count(),
                             VoiceMessagesCount = to.VoiceMessages.Count(),
                         }).MyDistinct(t => t.ID);

            return items;
        }

        // 5.3 Onboarding, Meetings Tab
        public RelationshipsViewModel GetMeetings(Guid packageId, int pageNo, int pageSize, Meeting.SortColumn sortColumn, bool sortAscending)
        {
            var package =
                packageService.GetByPackageId(packageId);


            var items = (from c in package.Contacts
                         from m in c.Meetings
                         select m).AsQueryable();

            int count = items.Count();

            SortAndPage<Meeting>(pageNo, pageSize, sortColumn.ToString(), sortAscending, ref items);

            RelationshipsViewModel viewModel = new RelationshipsViewModel
            {
                Meetings = items,
                Count = count,
                PageNo = pageNo,
                PageSize = pageSize
            };

            return viewModel;
        }

        public RelationshipsViewModel GetMeetings(Guid packageId)
        {
            IEnumerable<Meeting> items = meetingService.GetByPackageId(packageId);

            RelationshipsViewModel viewModel = new RelationshipsViewModel
            {
                Meetings = items,
                Count = items.Count(),
            };

            return viewModel;
        }

        // 5.3 Onboarding, Tasks tab
        public OnboardingTasksViewModel GetTasks(Guid packageId, int pageNo, int pageSize, OnboardingTaskItemViewModel.SortColumn sortColumn, bool sortAscending)
        {
            var items = taskService.GetAllByPackageId(packageId, true).Select(t => new OnboardingTaskItemViewModel
            {
                TaskId = t.ID,
                NameTopic = t.ParentTopic != null ? t.ParentTopic.Name : "-",
                NameTask = t.Name,
                NameTopicGroup = GetParentName(t),
                NameModule = GetTaskModuleName(t),
                Complete = t.Complete,
                CompleteDate = t.CompleteDate,
                HasAudio = t.HasVoiceMessages(),
                HasFiles = t.HasAttachments(),
                Priority = t.Priority,
                WeekDue = t.WeekDue
            }).AsQueryable();

            int count = items.Count();

            SortAndPage(pageNo, pageSize, sortColumn.ToString(), sortAscending, ref items);

            OnboardingTasksViewModel viewModel = new OnboardingTasksViewModel
            {
                Items = items,
                SortAscending = sortAscending
            };

            return viewModel;
        }

        // should be Topic
        private string GetParentName(Task task)
        {
            if (task.ParentTopic != null)
            {
                return task.ParentTopic.ParentTopicGroup.Title;
            }
            if (task.ProjectInstace != null)
            {
                return task.ProjectInstace.Project.Name;
            }
            if (task.Contact != null)
            {
                return task.Contact.Name;
            }

            return "-";
        }

        private string GetTaskModuleName(Task task)
        {
            if (task.ParentTopic != null)
            {
                return task.ParentTopic.ParentTopicGroup.ParentModule.Type.ToString();
            }

            if (task.ProjectInstace != null)
            {
                return "Projects";
            }

            if (task.Contact != null)
            {
                return "Relationships";
            }

            return "-";
        }

        // 5.3.1 Topics tab, by module
        public OnboardingTopicsViewModel GetTopicsByModule(Guid packageId, int criticalPageNo, int pageSize, OnboardingTopicItemViewModel.SortColumn criticalSortColumn, bool criticalSortAscending, int experiencesPageNo, OnboardingTopicItemViewModel.SortColumn experiencesSortColumn, bool experiencesSortAscending)
        {
            var package = packageService.GetByPackageId(packageId);

            var criticalItems = (from m in package.Modules
                                 where m.Type == Module.ModuleType.Critical
                                 from tg in m.TopicGroups
                                 from to in tg.Topics
                                 where !to.Deleted.HasValue
                                 where to.IsApproved()
                                 select new OnboardingTopicItemViewModel
                                 {
                                     TopicId = to.ID,
                                     NameTopic = to.Name,
                                     NameGroup = tg.Title,
                                     HasAudio = to.HasVoiceMessages(),
                                     HasFiles = to.HasAttachments(),
                                 }).AsQueryable();

            var experienceItems = (from m in package.Modules
                                   where m.Type == Module.ModuleType.Experiences
                                   from tg in m.TopicGroups
                                   from to in tg.Topics
                                   where !to.Deleted.HasValue
                                   where to.IsApproved()
                                   select new OnboardingTopicItemViewModel
                                   {
                                       TopicId = to.ID,
                                       NameTopic = to.Name,
                                       NameGroup = tg.Title,
                                       HasAudio = to.HasVoiceMessages(),
                                       HasFiles = to.HasAttachments(),
                                   }).AsQueryable();

            var criticalCount = criticalItems.Count();
            var experienceCount = experienceItems.Count();

            SortAndPage<OnboardingTopicItemViewModel>(criticalPageNo, pageSize, criticalSortColumn.ToString(), criticalSortAscending, ref criticalItems);
            SortAndPage<OnboardingTopicItemViewModel>(experiencesPageNo, pageSize, experiencesSortColumn.ToString(), experiencesSortAscending, ref experienceItems);

            OnboardingTopicsViewModel viewModel = new OnboardingTopicsViewModel
            {
                CriticalItems = criticalItems,
                CriticalPageNo = criticalPageNo,
                CriticalSortAscending = criticalSortAscending,
                CriticalTotalCount = criticalCount,
                ExperienceItems = experienceItems,
                ExperiencesPageNo = experiencesPageNo,
                ExperiencesSortAscending = experiencesSortAscending,
                ExperiencesTotalCount = experienceCount,
                PageSize = pageSize
            };

            return viewModel;
        }
    }
}
