﻿using System.Collections.Generic;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Core.Home
{
    public class MeetingsCore : BaseCore
    {

        #region Constructors

        private readonly IAuditService AuditService;
        private readonly IContactService ContactService;
        private readonly IMeetingService MeetingService;

        public MeetingsCore(IMasterListService masterListService, IMeetingService meetingService,
            IContactService contactService, IAuditService auditService)
            : base(masterListService)
        {
            ContactService = contactService;
            MeetingService = meetingService;
            AuditService = auditService;
        }

        #endregion

        #region Public Methods

        /// <summary>
        ///     Create a meeting from the given view model.
        /// </summary>
        public Meeting CreateMeeting(MeetingViewModel meetingViewModel)
        {
            var contact = ContactService.GetById(meetingViewModel.ParentContactId);

            var meeting = new Meeting();
            UpdateMeetingFields(meeting, meetingViewModel);

            if (contact.Meetings == null)
            {
                contact.Meetings = new List<Meeting>();
            }

            contact.Meetings.Add(meeting);
            ContactService.Update(contact);

            AuditService.RecordActivity<Meeting>(AuditLog.EventType.Created, meeting.ID);

            return meeting;
        }

        /// <summary>
        ///     Update an existing meeting given its view model.
        /// </summary>
        public Meeting UpdateMeeting(MeetingViewModel meetingViewModel)
        {
            var meeting = MeetingService.GetByMeetingId(meetingViewModel.ID);
            UpdateMeetingFields(meeting, meetingViewModel);

            MeetingService.Update(meeting);
            AuditService.RecordActivity<Meeting>(AuditLog.EventType.Modified, meeting.ID);

            return meeting;
        }

        /// <summary>
        ///     Update the properties of the given meeting with the corresponding
        ///     properties on the view model.
        /// </summary>
        private void UpdateMeetingFields(Meeting meeting, MeetingViewModel meetingViewModel)
        {
            meeting.Name = meetingViewModel.Name;
            meeting.Location = meetingViewModel.Location;
            meeting.TargetWeek = meetingViewModel.TargetWeek;
            meeting.Start = meetingViewModel.Start;
            meeting.ParentContactId = meetingViewModel.ParentContactId;
            meeting.MeetingTypeId = meetingViewModel.MeetingTypeId;
            meeting.ProjectInstanceId = meetingViewModel.ProjectInstanceId;
            if (meeting.Start.HasValue)
            {
                meeting.End = meetingViewModel.Start.Value.AddMinutes(meetingViewModel.DurationMins);
            }
        }

        #endregion
    }
}