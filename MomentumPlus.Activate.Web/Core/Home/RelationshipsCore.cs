﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using MomentumPlus.Activate.Services;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Areas.Home.Models;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;
using System.Web;
using MomentumPlus.Activate.Web.Models.People;
using System.IO;
using MomentumPlus.Activate.Web.Extensions;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.People;
using MomentumPlus.Core.Models.Moderation;

namespace MomentumPlus.Activate.Web.Core.Home
{
    public class RelationshipsCore : PersonCore
    {
        #region Constants

        /// <summary>
        /// Number of meetings to display on one page.
        /// </summary>
        private const int MeetingsPageSize = 5;

        /// <summary>
        /// Number of contacts to display on one page.
        /// </summary>
        private const int ContactsPageSize = 5;

        #endregion

        #region Dependency Injection

        private IAuditService AuditService;
        private IContactService ContactService;
        private IMeetingService MeetingService;
        private IPackageService PackageService;
        private IUserService UserService;
        private IHandoverService HandoverService;
        private IFlaggingService FlaggingService;

        public RelationshipsCore(
            IAuditService auditService,
            IContactService contactService,
            IMasterListService masterListService,
            IMeetingService meetingService,
            IPackageService packageService,
            IPeopleService peopleService,
            IUserService userService,
            IHandoverService handoverService,
            IFlaggingService flaggingService
        )
            : base(masterListService, peopleService)
        {
            AuditService = auditService;
            ContactService = contactService;
            MeetingService = meetingService;
            PackageService = packageService;
            UserService = userService;
            HandoverService = handoverService;
            FlaggingService = flaggingService;
        }

        #endregion

        #region Create

        public ContactViewModel GetNewContactViewModel()
        {
            var contactViewModel = new ContactViewModel();
            contactViewModel.Person = new Models.People.PersonViewModel();

            var defaultCompanyId = MasterListService.GetDefaultCompanyId();

            if (defaultCompanyId.HasValue && defaultCompanyId.Value != Guid.Empty)
            {
                var company = MasterListService.GetByMasterListId(defaultCompanyId.Value);
                contactViewModel.Person.CompanyId = company.ID;
                contactViewModel.DefaultCompanyId = company.ID;
            }
            //else
            //{
            //    contactViewModel.Person.CompanyId = Guid.Empty;
            //    contactViewModel.DefaultCompanyId = Guid.Empty;
            //}

            contactViewModel.RelationshipImportance = Contact.Importance.Level1;

            return contactViewModel;
        }

        /// <summary>
        /// Get a contact view model for a new contact for the given
        /// package and current user.
        /// </summary>
        public ContactViewModel GetNewContactViewModel(Package package, User currentUser)
        {
            var contactViewModel = GetNewContactViewModel();

            PopulateContactViewModel(contactViewModel, package);

            return contactViewModel;
        }

        /// <summary>
        /// Create a new contact from the given view model in the given package.
        /// If no package is specified then the contact is created in framework.
        /// </summary>
        public Contact CreateContact(ContactViewModel contactViewModel, Package package = null, HttpPostedFileBase profileImage = null)
        {
            // Create new contact and populate properties
            var contact = new Contact();

            UpdateContactFields(contact, contactViewModel, profileImage);

            if (package != null)
            {
                // Add contact to package and update audit log
                if (package.Contacts == null) { package.Contacts = new List<Contact>(); }
                package.Contacts.Add(contact);
                PackageService.Update(package);

                // Update audit log
                AuditService.RecordActivity<Contact>(AuditLog.EventType.Created, contact.ID);
            }
            else
            {
                // Todo, always add a copy to framework
                contact.IsFramework = true;
                ContactService.Add(contact);
            }

            // Add meeting stub for contact when an intro is required
            CreateIntroMeeting(contact);

            return contact;
        }

        #endregion

        #region Read

        /// <summary>
        /// Get the relationships view model for the given parameters.
        /// </summary>
        public RelationshipsViewModel GetRelationshipsViewModel(
            int pageNo,
            IEnumerable<string> selectedCompanies,
            int activePageNo,
            int retiredPageNo,
            int introPageNo,
            int managementPageNo,
            Package package
        )
        {
            var viewModel = new RelationshipsViewModel
            {
                SelectedCompanies = selectedCompanies ?? new List<string>(),
                DefaultCompanyId = MasterListService.GetDefaultCompanyId(),
                PackageOwnerId = package.UserOwner.ID,
                GraphNodes = GetGraphNodes(package, selectedCompanies),
                PageNo = pageNo,
                PageSize = MeetingsPageSize,
                ReadOnly = RoleAuthHelper.IsReadOnly("Relationships_View_Index"),
                ShowMeetings = true,
                ActivePageNo = activePageNo,
                RetiredPageNo = retiredPageNo,
                IntroPageNo = introPageNo,
                ManagementPageNo = managementPageNo,
                ContactsPageSize = ContactsPageSize
            };

            viewModel.ActiveContacts = ContactService.GetByPackageId(package.ID, activePageNo,
                ContactsPageSize, Person.SortColumn.FirstName, true);
            viewModel.ActiveCount = ContactService.CountByPackageId(package.ID);

            viewModel.RetiredContacts = ContactService.GetByPackageId(package.ID, retiredPageNo,
                ContactsPageSize, Person.SortColumn.FirstName, true, true);
            viewModel.RetiredCount = ContactService.CountByPackageId(package.ID, true);

            viewModel.Management = PeopleService.GetStaffsByPackageId(package.ID, managementPageNo,
               ContactsPageSize, Person.SortColumn.FirstName);
            viewModel.ManagementCount = PeopleService.CountStaffsByPackageId(package.ID, false);

            if (HandoverService.IsLeaverPeriodLocked(package.ID))
            {
                viewModel.Flags = new Dictionary<Contact, PackageFlag>();
                foreach (var contact in viewModel.ActiveContacts.Union(viewModel.Management).Union(viewModel.RetiredContacts))
                {
                    viewModel.Flags.Add(contact, FlaggingService.GetFlag(contact));
                }
            }

            // Get meetings, filter my Meeting Type "On-boarding - Introduction to Person"
            List<Guid> meetingTypesFilter = new List<Guid>() { Guid.Parse("E72D15EF-BD12-49AB-B15B-269EDF6A891B") };

            viewModel.Meetings = MeetingService.GetByPackageId(package.ID, introPageNo,
                ContactsPageSize, Meeting.SortColumn.ParentContactName, true, false, meetingTypesFilter);

            viewModel.IntroCount = MeetingService.CountByPackageId(package.ID, false, meetingTypesFilter);

            //viewModel.Management = ContactService.GetStaffContacts(package.ID).ToList();

            viewModel.Companies = ContactService.GetCompanies(package.ID);

            if (package.Handover != null && package.Handover.LeavingPeriod != null &&
                package.Handover.LeavingPeriod.Locked.HasValue)
                viewModel.IsLeaver = true;

            return viewModel;
        }

        /// <summary>
        /// Get the relationships stats view model for a given package.
        /// </summary>
        public RelationshipsStatsViewModel GetRelationshipsStatsViewModel(Guid packageId)
        {
            var companies = ContactService.GetCompanies(packageId);

            var defaultCompanyId = MasterListService.GetDefaultCompanyId();
            var defaultCompanyName = "";

            if (defaultCompanyId.HasValue)
            {
                var defaultCompany = MasterListService.GetByMasterListId(defaultCompanyId.Value);
                defaultCompanyName = defaultCompany.Name;
                companies = companies.Where(c => c != defaultCompanyName);
            }

            var viewModel = new RelationshipsStatsViewModel
            {
                PeopleInternal = PeopleCountInternal(packageId),
                PeopleExternal = PeopleCountExternal(packageId),
                Meetings = MeetingsCount(packageId),
                DefaultCompany = defaultCompanyName,
                OtherCompanies = string.Join(",", companies)
            };

            return viewModel;
        }

        /// <summary>
        /// Get the contact view model corresponding to the given
        /// contact in the given package.
        /// </summary>
        public ContactViewModel GetContactViewModel(Contact contact, Package package = null, User currentUser = null)
        {
            string funcDetails = "Relationship_Edit_Get";
            var contactViewModel = Mapper.Map<ContactViewModel>(contact);

            if (contact.Managed)
            {
                contactViewModel.Managed = contact.Managed;
                contactViewModel.EmploymentStatus = contact.EmploymentStatus;
                contactViewModel.FutureTraining = contact.FutureTraining;
                contactViewModel.ManagementNotes = contact.ManagementNotes;
            }

            if (package != null && currentUser != null)
            {
                PopulateContactViewModel(contactViewModel, package, contact.ID);
            }
            else
            {
                PopulateContactViewModel(contactViewModel);
            }

            contactViewModel.NotesSubjectivity = contact.CountSubjectivityRelationship(MasterListService);
            contactViewModel.NotesWordCount = contact.CountWordsRelationship();

            contactViewModel.StartDate = contact.StartDate;
            contactViewModel.Managed = contact.Managed;
            contactViewModel.IntroRequired = contact.IntroRequired;
            contactViewModel.IsFramework = contact.IsFramework;
            contactViewModel.EmploymentStatus = contact.EmploymentStatus;
            contactViewModel.FutureTraining = contact.FutureTraining;
            contactViewModel.ManagementNotes = contact.ManagementNotes;
            contactViewModel.RelationshipImportance = contact.RelationshipImportance;
            if (contactViewModel.RelationshipImportance == 0)
                contactViewModel.RelationshipImportance = Contact.Importance.Level1;


            // Set default company ids
            contactViewModel.DefaultCompanyId = MasterListService.GetDefaultCompanyId();
            if (contactViewModel.Person != null && !contactViewModel.Person.CompanyId.HasValue)
                contactViewModel.Person.CompanyId = contactViewModel.DefaultCompanyId;

            contactViewModel.ReadOnly = RoleAuthHelper.IsReadOnly(funcDetails);

            return contactViewModel;
        }

        #endregion

        #region Update

        /// <summary>
        /// Update a given contact in a package based on the submitted
        /// view model.
        /// </summary>
        public void UpdateContact(
            Contact contact,
            ContactViewModel contactViewModel,
            Package package = null,
            User currentUser = null,
            HttpPostedFileBase profileImage = null
        )
        {
            contact.CompanyId = contactViewModel.Person.CompanyId;
            contact.OtherCompany = contactViewModel.Person.OtherCompany;
            contact.OtherCompanyName = contactViewModel.Person.OtherCompanyName;

            UpdateContactFields(contact, contactViewModel, profileImage);

            // Add meeting stub for contact when an intro is required
            CreateIntroMeeting(contact);

            // Update contact in the database and record activity in audit log
            ContactService.Update(contact);
            AuditService.RecordActivity<Contact>(AuditLog.EventType.Modified, contact.ID);
        }


        #endregion

        #region Helpers

        /// <summary>
        /// Get nodes to display on a relationships map for a package.
        /// </summary>
        /// <param name="package">The Package the graph is being generated.</param>
        /// <param name="selectedCompanies"></param>
        /// <param name="b"></param>
        /// <returns>
        /// An <c>IDictionary</c> object whose keys are <c>Guid</c>s and values are
        /// <c>ContactViewModel</c> objects.
        /// </returns>
        public IDictionary<Guid, ContactChartViewModel> GetGraphNodes(Package package, IEnumerable<string> selectedCompanies, bool approved = false)
        {
            // Get the package owner and construct a ContactViewModel to use
            // at the centre of the graph.
            var packageOwner = UserService.GetByUserId(package.UserOwner.ID);
            var graphRoot = new ContactChartViewModel
            {
                ID = packageOwner.ID,
                Name = packageOwner.FirstName + " " + packageOwner.LastName,
                RootNode = true
            };

            var graphNodes = new Dictionary<Guid, ContactChartViewModel>
            {
                {
                    packageOwner.ID,
                    graphRoot
                }
            };

            // Get the contacts attached to the package which make up
            // the other nodes in the graph.
            IEnumerable<Contact> contacts = ContactService.GetByPackageId(package.ID, selectedCompanies);

            if (approved)
            {
                contacts = contacts.Where(Moderated.Approved<Contact>().Compile());
            }

            var contactViewModels = Mapper.Map<IEnumerable<Contact>,
                IEnumerable<ContactChartViewModel>>(contacts);

            foreach (var contactViewModel in contactViewModels)
            {
                if (contactViewModel.IndirectRelationshipContactId.HasValue)
                    contactViewModel.IndirectRelationship = String.Format("Indirect via {0}",
                        ContactService.GetById(contactViewModel.IndirectRelationshipContactId.Value).FullName);
                graphNodes.Add(contactViewModel.ID, contactViewModel);
            }

            return graphNodes;
        }

        /// <summary>
        /// Populate a contact view model with the data required for
        /// rendering the contact form.
        /// </summary>
        public void PopulateContactViewModel(
            ContactViewModel contactViewModel,
            Package package = null,
            Guid? excludeContact = null, bool isFramework = false
        )
        {
            contactViewModel.IsFramework = isFramework;

            PopulatePersonViewModel(contactViewModel.Person);

            IEnumerable<Contact> contacts;

            if (package != null)
            {
                contactViewModel.PackageOwnerId = package.UserOwner.ID;
                contactViewModel.GraphNodes = GetGraphNodes(package, ContactService.GetCompanies(package.ID));

                contacts = ContactService.GetByPackageId(package.ID);

                contactViewModel.ShowManagingTab = package.PeopleManagementEnabled;
            }
            else
            {
                contacts = ContactService.GetAllFramework();
            }

            contactViewModel.Contacts = contacts
                .Where(c => !c.IndirectRelationshipContactId.HasValue)
                .Where(c => c.ID != excludeContact)
                .ToList()
                .Select(c => new SelectListItem
                {
                    Text = "Indirect Via " + c.FullName,
                    Value = c.ID.ToString()
                });

            if (contactViewModel.IndirectRelationshipContactId.HasValue)
                contactViewModel.IndirectRelationship = string.Format("Indirect via {0}",
                    ContactService.GetById(contactViewModel.IndirectRelationshipContactId.Value).FullName);

            if (package != null && package.Handover != null && package.Handover.LeavingPeriod != null &&
                package.Handover.LeavingPeriod.Locked.HasValue)
                contactViewModel.IsLeaver = true;

            if (package != null)
                contactViewModel.Companies = ContactService.GetCompanies(package.ID);
        }

        public void UpdateReadyForReview(Contact contact, StaffViewModel person)
        {
            contact.ReadyForReview = person.ReadyForReview;
        }

        /// <summary>
        /// Update the properties of the given contact with the corresponding
        /// properties on the view model.
        /// </summary>
        private void UpdateContactFields(Contact contact, ContactViewModel contactViewModel, HttpPostedFileBase profileImage)
        {

            UpdatePersonFields(contact, contactViewModel.Person, profileImage);

            if (profileImage != null)
            {
                MemoryStream memoryStream = new System.IO.MemoryStream();
                profileImage.InputStream.Seek(0, SeekOrigin.Begin);
                profileImage.InputStream.CopyTo(memoryStream);

                contact.Image = contact.Image ?? new Image();
                contact.Image.BinaryData = memoryStream.ToArray();
                contact.Image.FileType = Path.GetExtension(profileImage.FileName);
                contact.Image.ContentType = profileImage.ContentType;
            }

            contact.RelationshipType = Contact.Type.Individual;
            contact.IndirectRelationshipContactId = contactViewModel.IndirectRelationshipContactId;

            if (contact.RelationshipImportance != contactViewModel.RelationshipImportance)
            {
                contact.ChartX = null;
                contact.ChartY = null;
            }

            contact.RelationshipImportance = contactViewModel.RelationshipImportance;
            contact.Notes = contactViewModel.Notes;
            contact.IntroRequired = contactViewModel.IntroRequired;
            contact.StartDate = contactViewModel.StartDate;
            //contact.LengthOfRelationship = contactViewModel.LengthOfRelationship;
            contact.StrengthOfRelationship = contactViewModel.StrengthOfRelationship;
            contact.ImproveReason = contactViewModel.ImproveReason;
            contact.SearchTags = contactViewModel.SearchTags;

            contact.CompanyId = contactViewModel.Person.CompanyId;
            contact.OtherCompany = contactViewModel.Person.OtherCompany;
            contact.OtherCompanyName = contactViewModel.Person.OtherCompanyName;

            contact.Managed = contactViewModel.Managed;
            contact.EmploymentStatus = contactViewModel.EmploymentStatus;
            contact.FutureTraining = contactViewModel.FutureTraining;
            contact.ManagementNotes = contactViewModel.ManagementNotes;

            contact.BttcMon = contactViewModel.BttcMon;
            contact.BttcTue = contactViewModel.BttcTue;
            contact.BttcWed = contactViewModel.BttcWed;
            contact.BttcThu = contactViewModel.BttcThu;
            contact.BttcFri = contactViewModel.BttcFri;
            contact.BttcSat = contactViewModel.BttcSat;
            contact.BttcSun = contactViewModel.BttcSun;
            contact.BttcAm = contactViewModel.BttcAm;
            contact.BttcPm = contactViewModel.BttcPm;

            contact.EmployeeCopyRequest = contactViewModel.EmployeeCopyRequest;
            contact.ReadyForReview = contactViewModel.ReadyForReview;
        }

        /// <summary>
        /// Get the number of internal contacts for a given package.
        /// </summary>
        /// <param name="packageId">The ID of the package.</param>
        /// <returns>The internal contact count.</returns>
        public int PeopleCountInternal(Guid packageId)
        {
            var contacts = ContactService.GetByPackageId(packageId);
            var defaultCompanyId = MasterListService.GetDefaultCompanyId();

            var internalContacts = from c in contacts
                                   where c.CompanyId == defaultCompanyId
                                   select c;

            return internalContacts.Count();
        }

        /// <summary>
        /// Get the number of external contacts for a given package.
        /// </summary>
        /// <param name="packageId">The ID of the package.</param>
        /// <returns>The external contact count.</returns>
        public int PeopleCountExternal(Guid packageId)
        {
            var contactsCount = ContactService.CountByPackageId(packageId);
            var internalContactsCount = PeopleCountInternal(packageId);
            return contactsCount - internalContactsCount;
        }

        /// <summary>
        /// Get the number of meetings for a given package.
        /// </summary>
        /// <param name="packageId">The ID of the package.</param>
        /// <returns>The meeting count.</returns>
        public int MeetingsCount(Guid packageId)
        {
            //var contacts = ContactService.GetByPackageId(packageId);

            //var meetings = contacts.SelectMany(c => c.Meetings);
            //return meetings.Count();

            List<Guid> meetingTypesFilter = new List<Guid>() { Guid.Parse("E72D15EF-BD12-49AB-B15B-269EDF6A891B") };

            var meetingsCount = MeetingService.CountByPackageId(packageId, false, meetingTypesFilter);

            return meetingsCount;

        }

        public void CreateIntroMeeting(Contact contact)
        {
            if (contact.IntroRequired && contact.Meetings.All(m => m.MeetingTypeId != new Guid("e72d15ef-bd12-49ab-b15b-269edf6a891b")))
            {
                contact = ContactService.GetById(contact.ID);

                if (contact.Meetings == null)
                {
                    contact.Meetings = new List<Meeting>();
                }

                contact.Meetings.Add(new Meeting { ParentContactId = contact.ID, Name = "Introduction", MeetingTypeId = new Guid("e72d15ef-bd12-49ab-b15b-269edf6a891b") });
                ContactService.Update(contact);

                AuditService.RecordActivity<Contact>(AuditLog.EventType.Modified, contact.ID);
            }
        }

        #endregion
    }
}