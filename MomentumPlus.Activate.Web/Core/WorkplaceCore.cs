﻿using System;
using System.Collections.Generic;
using AutoMapper;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.People;

namespace MomentumPlus.Activate.Web.Core
{
    public class WorkplaceCore
    {
        #region Constants

        /// <summary>
        /// Number of workplaces to display in the Workplace list.
        /// </summary>
        private const int PageSize = 8;

        #endregion

        #region Dependency Injection

        private IPackageService PackageService;
        private IWorkplaceService WorkplaceService;

        public WorkplaceCore(
            IPackageService packageService,
            IWorkplaceService workplaceService
        )
        {
            PackageService = packageService;
            WorkplaceService = workplaceService;
        }

        #endregion

        #region Create

        /// <summary>
        /// Create a new workplace entity based on the given view model.
        /// </summary>
        public Workplace CreateWorkplace(WorkplaceViewModel viewModel)
        {
            var newWorkplace = new Workplace();
            Mapper.Map<WorkplaceViewModel, Workplace>(viewModel, newWorkplace);
            WorkplaceService.Add(newWorkplace);
            return newWorkplace;
        }

        #endregion

        #region Read

        /// <summary>
        /// Get a workplaces view model for the given parameters.
        /// </summary>
        public WorkplacesViewModel GetWorkplacesViewModel(
            Guid? id,
            string Query,
            int PageNo,
            Workplace.SortColumn SortColumn,
            bool SortAscending,
            Package.Status? packageStatus,
            bool retired,
            User currentUser
        )
        {
            var workplaces = WorkplaceService.Search(Query, PageNo, PageSize, SortColumn, SortAscending);
            var workplacesCount = WorkplaceService.SearchCount(Query);

            var workplaceViewModel = Mapper.Map<IEnumerable<Workplace>, IEnumerable<WorkplaceViewModel>>(workplaces);
            var workplacesViewModel = new WorkplacesViewModel
            {
                Workplaces = workplaceViewModel,
                SortAscending = SortAscending,
                SelectedPackageStatus = packageStatus,
                Retired = retired,
                PageSize = PageSize,
                PageNo = PageNo,
                TotalCount = workplacesCount
            };

            if (id != null)
            {
                var packages = PackageService.GetByWorkplaceId(id.Value, packageStatus, retired);
                var packageViewModel = Mapper.Map<IEnumerable<Package>, IEnumerable<PackageViewModel>>(packages); 
                workplacesViewModel.Packages = packageViewModel;
                workplacesViewModel.SelectedWorkplaceId = id.Value;
            }
            else
            {
                IEnumerable<Package> packages = new List<Package>();
                var packageViewModel = Mapper.Map<IEnumerable<Package>, IEnumerable<PackageViewModel>>(packages);
                workplacesViewModel.Packages = packageViewModel;
            }

            if (currentUser.Role != null &&
               (currentUser.Role.RoleParsed == Role.RoleEnum.HRManager ||
                currentUser.Role.RoleParsed == Role.RoleEnum.Administrator ||
                currentUser.Role.RoleParsed == Role.RoleEnum.iHandoverAgent ||
                currentUser.Role.RoleParsed == Role.RoleEnum.iHandoverAdmin))
            {
                workplacesViewModel.IsAuthorisedToAdd = true;
            }

            return workplacesViewModel;
        }

        #endregion

        #region Update

        /// <summary>
        /// Update an existing workplace given its view model.
        /// </summary>
        public Workplace UpdateWorkplace(WorkplaceViewModel viewModel)
        {
            var workplace = WorkplaceService.GetByWorkplaceId(viewModel.ID);
            Mapper.Map<WorkplaceViewModel, Workplace>(viewModel, workplace);
            WorkplaceService.Update(workplace);
            return workplace;
        }

        #endregion
    }
}