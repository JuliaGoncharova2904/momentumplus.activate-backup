﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web.Mvc;
using AutoMapper;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Activate.Web.Models.People;
using System.Web;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.People;

namespace MomentumPlus.Activate.Web.Core
{
    public class EmployeeCore : PersonCore
    {
        #region Constants

        /// <summary>
        /// Number of employees to display in the Employee list.
        /// </summary>
        private const int PageSize = 8;

        #endregion

        #region Dependency Injection

        private IContactService ContactService;
        private IPackageService PackageService;
        private IPositionService PositionService;
        private IRoleService RoleService;
        private IUserService UserService;

        public EmployeeCore(
            IContactService contactService,
            IMasterListService masterListService,
            IPackageService packageService,
            IPeopleService peopleService,
            IPositionService positionService,
            IRoleService roleService,
            IUserService userService
        )
            : base(masterListService, peopleService)
        {
            ContactService = contactService;
            PackageService = packageService;
            PositionService = positionService;
            RoleService = roleService;
            UserService = userService;
        }

        #endregion

        #region Create

        /// <summary>
        /// Create a new employee from the given view model.
        /// </summary>
        public User CreateEmployee(UserViewModel viewModel, HttpPostedFileBase profileImage)
        {
            using (TransactionScope trans = new TransactionScope())
            {
                var newUser = new User();
                newUser.Contact = new Contact();
                newUser.Username = viewModel.Email;
                newUser.Password = viewModel.Password;
                newUser.RoleId = viewModel.RoleId;
                newUser.DefaultPositionId = viewModel.DefaultPositionId;
                newUser.Notes = viewModel.Notes;
                newUser.DisplayTooltips = viewModel.DisplayTooltips;
                newUser.Email = viewModel.Email;

                UpdatePersonFields(newUser, viewModel, profileImage);
                newUser = UserService.CreateUser(newUser);

                // Add Framework version if one with the same email address does not exist
                var contactsFramework = ContactService.GetAllFramework().ToList();

                if (contactsFramework.Count > 0)
                {
                    if (!contactsFramework.Where(c => c.User != null).Any(c =>
                        c.User.Username.Trim().ToLower() == newUser.Username.Trim().ToLower()))
                    {
                        newUser.Contact.IsFramework = true;

                        if (newUser.Image != null)
                            newUser.Image.ID = Guid.NewGuid();

                        ContactService.Add(newUser.Contact);
                    }
                }


                // Retrieve the new user to get their ID
                var user =
                    UserService.GetByUsername(newUser.Username);

                trans.Complete();

                return user;
            }
        }

        #endregion

        #region Read

        /// <summary>
        /// Get the employees view model for the given parameters.
        /// </summary>
        public EmployeesViewModel GetEmployeesViewModel(
            Guid? id,
            string Query,
            int PageNo,
            User.SortColumn SortColumn,
            bool SortAscending,
            Package.Status? packageStatus,
            bool retired
        )
        {
            var users = UserService.Search(Query, PageNo, PageSize, SortColumn, SortAscending);
            var usersCount = UserService.SearchCount(Query);
            var userViewModel = Mapper.Map<IEnumerable<User>, IEnumerable<UserViewModel>>(users);

            var employeesViewModel = new EmployeesViewModel
            {
                Users = userViewModel,
                SortAscending = SortAscending,
                SelectedPackageStatus = packageStatus,
                Retired = retired,
                PageSize = PageSize,
                PageNo = PageNo,
                TotalCount = usersCount
            };

            if (id != null)
            {
                var packages = PackageService.GetPackagesByUserId(id.Value, packageStatus, retired);
                var packageViewModel = Mapper.Map<IEnumerable<Package>, IEnumerable<PackageViewModel>>(packages);
                employeesViewModel.Packages = packageViewModel;
                employeesViewModel.SelectedUserId = id.Value;
            }
            else
            {
                IEnumerable<Package> packages = new List<Package>();

                var packageViewModel = Mapper.Map<IEnumerable<Package>, IEnumerable<PackageViewModel>>(packages);
                employeesViewModel.Packages = packageViewModel;
            }

            employeesViewModel.IsAuthorisedToAdd = AppSession.Current.CurrentUser.Role != null &&
                (AppSession.Current.CurrentUser.Role.RoleParsed == Role.RoleEnum.HRManager ||
                 AppSession.Current.CurrentUser.Role.RoleParsed == Role.RoleEnum.Administrator ||
                 AppSession.Current.CurrentUser.Role.RoleParsed == Role.RoleEnum.iHandoverAgent ||
                 AppSession.Current.CurrentUser.Role.RoleParsed == Role.RoleEnum.iHandoverAdmin);

            return employeesViewModel;
        }

        #endregion

        #region Update

        /// <summary>
        /// Update the a user based on a given view model.
        /// </summary>
        public void UpdateEmployee(User user, UserViewModel viewModel, HttpPostedFileBase profileImage)
        {
            user.Username = viewModel.Email;
            user.RoleId = viewModel.RoleId;
            user.DefaultPositionId = viewModel.DefaultPositionId;
            user.Notes = viewModel.Notes;
            user.DisplayTooltips = viewModel.DisplayTooltips;

            UpdatePersonFields(user, viewModel, profileImage);

            UserService.UpdateUser(user);

            // Update user stored in session with the new information
            if (AppSession.Current.CurrentUser.ID == user.ID)
            {
                AppSession.Current.CurrentUser = user;
            }
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Populate a given employee view model with the data required for
        /// rendering the employee form.
        /// </summary>
        public UserViewModel PopulateUserViewModel(UserViewModel viewModel, User currentUser)
        {
            PopulatePersonViewModel(viewModel);

            IEnumerable<Role> roles = RoleService.GetAll();

            // iHandover Agent/Administrator/Executive cannot create/edit iHandover Admins
            if (currentUser.Role.RoleParsed == Role.RoleEnum.iHandoverAgent ||
                currentUser.Role.RoleParsed == Role.RoleEnum.Administrator ||
                currentUser.Role.RoleParsed == Role.RoleEnum.Executive)
            {
                roles = roles.Where(r => r.RoleParsed != Role.RoleEnum.iHandoverAdmin);
            }

            // HR Manager can only create/edit Line Managers and Users
            if (currentUser.Role.RoleParsed == Role.RoleEnum.HRManager)
            {
                roles = roles.Where(r =>
                    r.RoleParsed == Role.RoleEnum.LineManager &&
                    r.RoleParsed == Role.RoleEnum.User
                );
            }

            // Line manager can only edit users
            if (currentUser.Role.RoleParsed == Role.RoleEnum.LineManager)
            {
                roles = roles.Where(r => r.RoleParsed == Role.RoleEnum.User);
            }

            // Get all the positions
            IEnumerable<Position> positions = PositionService.GetAll();

            // Generate values for Default Role dropdown
            viewModel.Roles = new List<SelectListItem>
            (
                roles.Select(r => new SelectListItem
                {
                    Text = r.RoleName,
                    Value = r.ID.ToString()
                })
            );

            // Generate values for Default Position dropdown
            viewModel.Positions = new List<SelectListItem>
            (
                positions.Select(p => new SelectListItem
                {
                    Text = p.Name,
                    Value = p.ID.ToString()
                })
            );

            return viewModel;
        }

        #endregion
    }
}