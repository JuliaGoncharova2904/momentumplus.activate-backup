﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web.Mvc;
using MomentumPlus.Activate.Services.Interfaces;
using NLog;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Comparers;
using MomentumPlus.Core.Models.People;

namespace MomentumPlus.Activate.Web.Core
{
    public class BaseCore
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region Dependency Injection

        protected IMasterListService MasterListService;

        public BaseCore(IMasterListService masterListService)
        {
            MasterListService = masterListService;
        }

        #endregion

        /// <summary>
        /// Generate a list of master list items for use in a select element.
        /// </summary>
        protected IEnumerable<SelectListItem> GetMasterListSelect(MasterList.ListType listType)
        {
            var selectListItems = new List<SelectListItem>();

            foreach (var masterList in MasterListService.GetAll(listType))
            {
                var text = masterList.Name;
                var value = masterList.ID.ToString();

                if (listType == MasterList.ListType.Projects && !string.IsNullOrEmpty(masterList.AdditionalField1))
                {
                    text = masterList.AdditionalField1 + " - " + masterList.Name;
                }
                else if (listType == MasterList.ListType.Websites)
                {
                    text += " - " + masterList.AdditionalField1;
                }

                selectListItems.Add(new SelectListItem { Text = text, Value = value });
            }

            return selectListItems;
        }

        public static void SortAndPage<T>(int pageNo, int pageSize, string sortColumn, bool sortAscending, ref IQueryable<T> objects)
        {
            if (objects != null && objects.Count() > 1)
            {
                var param = Expression.Parameter(typeof(T), "p");
                var prop = Expression.Property(param, sortColumn.ToString());
                var exp = Expression.Lambda(prop, param);
                string method = sortAscending ? "OrderBy" : "OrderByDescending";
                Type[] types = new Type[] { objects.ElementType, exp.Body.Type };
                var mce = Expression.Call(typeof(Queryable), method, types, objects.Expression, exp);
                objects = objects.Provider.CreateQuery<T>(mce);

                objects =
                    objects.Skip(pageSize * (pageNo - 1)).Take(pageSize);
            }
        }

        protected static void SortAndPage<T, TKey>(ref IEnumerable<T> objects, Func<T, TKey> sortObject, string sortColumnName, bool sortAscending, int pageNo, int pageSize)
        {
            objects = (sortAscending
                        ? objects.OrderBy(sortObject, new PropertyComparer<TKey>(sortColumnName))
                        : objects.OrderByDescending(sortObject, new PropertyComparer<TKey>(sortColumnName)))
                      .ToList();

            objects = objects.Skip(pageSize * (pageNo - 1)).Take(pageSize);
        }

        public static string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

            if (attributes != null &&
                attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }

        public static string GetName(User user)
        {
            return GetName(user, "Unassigned");
        }

        private static string GetName(User user, string returnIfNull)
        {
            string nameToReturn =
                returnIfNull;

            if (user != null)
                nameToReturn = string.Format("{0} {1}", user.FirstName, user.LastName);

            return nameToReturn;
        }

        public static string GetDaysToGo(DateTime? endDateTime)
        {
            return GetDaysToGo(endDateTime, " - ");
        }

        private static string GetDaysToGo(DateTime? endDateTime, string returnIfNull)
        {
            string daysToGo = returnIfNull;

            if (endDateTime.HasValue)
                daysToGo = endDateTime.Value.Subtract(DateTime.Now).Days.ToString();

            return daysToGo;
        }

        public static void WriteCorePerfomanceLog(string methodName, int elapsedMilliseconds)
        {
            if (elapsedMilliseconds > 100)
                logger.Warn("Expensive Core Query '{0}', {1}ms.", methodName, elapsedMilliseconds);
        }

    }

    public static class Extensions
    {
        public static IEnumerable<T> MyDistinct<T, V>(this IEnumerable<T> query,
                                                        Func<T, V> f)
        {
            return query.GroupBy(f).Select(x => x.First());
        }
    }


}