﻿using System;
using System.Collections.Generic;
using MomentumPlus.Activate.Services;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Models;
using System.Web.Mvc;
using MomentumPlus.Activate.Services.Helpers;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.People;
using MomentumPlus.Core.Models.Moderation;

namespace MomentumPlus.Activate.Web.Core
{
    public class TaskCore : BaseCore
    {

        public const int MaxWeekDue = 12;

        #region Dependency Injection

        private IAuditService AuditService;
        private IPackageService PackageService;
        private ITaskService TaskService;
        private ITopicService TopicService;
        private IProjectService ProjectService;
        private IContactService ContactService;

        /// <summary>
        /// Construct a new <c>TaskCore</c> instance
        /// with the given service implementations.
        /// </summary>
        public TaskCore(
            IAuditService auditService,
            IMasterListService masterListService,
            IPackageService packageService,
            ITaskService taskService,
            ITopicService topicService,
            IProjectService projectService,
            IContactService contactService
        )
            : base(masterListService)
        {
            AuditService = auditService;
            PackageService = packageService;
            TaskService = taskService;
            TopicService = topicService;
            ProjectService = projectService;
            ContactService = contactService;
        }

        #endregion

        #region Create

        /// <summary>
        /// Create a new task entity from the given view model.
        /// </summary>
        /// <param name="viewModel"></param>
        public Task CreateTask(Reviewable parent, TaskViewModel viewModel, bool proLink = false)
        {
            var task = new Task();
            UpdateTaskFields(task, viewModel);

            if (parent is Topic)
            {
                task.ParentTopicId = parent.ID;
                if (TopicService.GetByTopicId(parent.ID).ProcessGroupId.HasValue)
                {
                    task.IsProLink = proLink;
                }
            }
            else if (parent is Contact)
            {
                task.ContactId = parent.ID;
            }
            else
            {
                task.ProjectInstanceId = parent.ID;
            }

            TaskService.Add(task);

            AuditService.RecordActivity<Task>(AuditLog.EventType.Created, task.ID);
            AuditService.RecordActivity<Task>(AuditLog.EventType.Viewed, task.ID);

            return task;
        }

        #endregion

        #region Update

        /// <summary>
        /// Update the given task with the data passed in the view model.
        /// </summary>
        public void UpdateTask(Task task, TaskViewModel viewModel, User currentUser)
        {
            UpdateTaskFields(task, viewModel);

            TaskService.Update(task);

            AuditService.RecordActivity<Task>(AuditLog.EventType.Modified, task.ID);
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Populate a task view model with the data required
        /// to render the task form.
        /// </summary>
        public TaskViewModel PopulateTaskViewModel(
            TaskViewModel viewModel,
            User currentUser,
            Reviewable parent,
            Task task = null
        )
        {
            Guid? packageId = null;

            if (parent is Topic)
            {
                viewModel.TopicId = parent.ID;
                var topic = TopicService.GetByTopicId(parent.ID);
                viewModel.TopicName = topic.Name;

                var package = PackageService.GetByTopicId(parent.ID);
                packageId = package != null ? (Guid?)package.ID : null;
            }
            else if (parent is Contact)
            {
                viewModel.ContactId = parent.ID;
                var contact = ContactService.GetById(parent.ID);
                viewModel.TopicName = contact.FullName;
                packageId = parent.PackageId;
            }
            else
            {
                viewModel.ProjectInstanceId = parent.ID;
                var project = ProjectService.GetProjectInstanceById(parent.ID);
                viewModel.TopicName = project.Project.Name;
                packageId = parent.PackageId;
            }

            viewModel.FrameworkItem = !packageId.HasValue;

            if (task != null)
            {
                viewModel.NotesSubjectivity = task.CountSubjectivity(MasterListService);
                viewModel.NotesWordCount = task.CountWords();
                //viewModel.IsCore = task.IsCore;
            }

            viewModel.WeekDueListItems = GetWeekDueListItems(task != null ? (int?)task.WeekDue : null);


            if (packageId != null)
            {
                var package = PackageService.GetByPackageId((Guid) packageId);
                var leavingPeriod = package.Handover != null ? package.Handover.LeavingPeriod : null;
                viewModel.IsLeavingPackageState = leavingPeriod != null && leavingPeriod.Locked.HasValue && leavingPeriod.LastDay.Value >= SystemTime.Now;
                //viewModel.IsLeavingPackageState = PackageService.GetByPackageId((Guid)packageId).PackageStatus == Package.Status.Triggered;
            }
            else
            {
                viewModel.IsLeavingPackageState = false;
            }


            return viewModel;
        }


        /// <summary>
        /// Update the properties of the given task with the corresponding
        /// properties on the view model.
        /// </summary>
        public void UpdateTaskFields(Task task, TaskViewModel taskViewModel)
        {
            task.Name = taskViewModel.Name;
            task.Handover = taskViewModel.Handover;
            task.Notes = taskViewModel.Notes;
            task.WeekDue = taskViewModel.WeekDue;
            task.Fixed = taskViewModel.Fixed;
            task.Priority = taskViewModel.Priority;
            task.SearchTags = taskViewModel.SearchTags;
            task.Complete = taskViewModel.Complete;
            task.ReadyForReview = taskViewModel.ReadyForReview;

            if (taskViewModel.FrameworkItem)
            {
                task.IsCore = taskViewModel.IsCore;
            }
        }

        private static IEnumerable<SelectListItem> GetWeekDueListItems(int? currentValue)
        {
            var earliestStartWeek = Helpers.AccountConfig.Settings.EarliestStartWeek;

            currentValue = currentValue ?? earliestStartWeek;

            var weekDueListItems = new List<SelectListItem>();
            for (var i = earliestStartWeek; i <= MaxWeekDue; i++)
            {
                weekDueListItems.Add(new SelectListItem()
                {
                    Text = string.Format("Week {0}", i),
                    Value = i.ToString(),
                    Selected = i == currentValue
                });
            }

            return weekDueListItems;
        }

        #endregion
    }
}