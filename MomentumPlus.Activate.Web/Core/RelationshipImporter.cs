﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Net;
using CsvHelper;
using CsvHelper.Configuration;
using MomentumPlus.Activate.Services;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Core
{
    public class RelationshipImporter : BaseImporter, IImporter, IDisposable
    {
        public List<Contact> contacts;
        public List<MasterList> companies;

        public class ContactRow : IEquatable<ContactRow>
        {
            public enum EnumTitle
            {
                Mr,
                Mrs,
                Ms,
                Miss,
                Dr
            }

            //[Required]
            [StringLength(100)]
            public string Id { get; set; }
            //[Required(ErrorMessage = @"Title is required and should be either Mr, Mrs, Ms, Miss or Dr")]
            [EnumDataType(typeof(EnumTitle))]
            public string Title { get; set; }
            [Required]
            [Display(Name = @"First Name")]
            [StringLength(50)]
            public string FirstName { get; set; }
            [Required]
            [Display(Name = @"Last Name")]
            [StringLength(50)]
            public string LastName { get; set; }
            [Required]
            [StringLength(50)]
            public string Company { get; set; }
            [Required]
            [StringLength(50)]
            public string Position { get; set; }
            [Required]
            [StringLength(100)]
            [EmailAddress]
            public string Email { get; set; }
            [StringLength(20)]
            public string Telephone { get; set; }
            [StringLength(20)]
            public string Mobile { get; set; }
            [StringLength(50)]
            public string Image { get; set; }
            [StringLength(200)]
            public string ImageUrl { get; set; }

            public override int GetHashCode()
            {
                return
                    string.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}",
                    Id.ToLowerRemoveSpaces(),
                    Title.ToLowerRemoveSpaces(),
                    FirstName.ToLowerRemoveSpaces(),
                    LastName.ToLowerRemoveSpaces(),
                    Company.ToLowerRemoveSpaces(),
                    Position.ToLowerRemoveSpaces(),
                    Email.ToLowerRemoveSpaces(),
                    Telephone.ToLowerRemoveSpaces(),
                    Mobile.ToLowerRemoveSpaces(),
                    Image.ToLowerRemoveSpaces(),
                    ImageUrl.ToLowerRemoveSpaces()).GetHashCode();
            }

            public bool Equals(ContactRow other)
            {
                bool equal = this.Id.ToLowerRemoveSpaces() == other.Id.ToLowerRemoveSpaces() &&
                             this.Title.ToLowerRemoveSpaces() == other.Title.ToLowerRemoveSpaces() &&
                             this.FirstName.ToLowerRemoveSpaces() == other.FirstName.ToLowerRemoveSpaces() &&
                             this.LastName.ToLowerRemoveSpaces() == other.LastName.ToLowerRemoveSpaces() &&
                             this.Company.ToLowerRemoveSpaces() == other.Company.ToLowerRemoveSpaces() &&
                             this.Position.ToLowerRemoveSpaces() == other.Position.ToLowerRemoveSpaces() &&
                             this.Email.ToLowerRemoveSpaces() == other.Email.ToLowerRemoveSpaces() &&
                             this.Telephone.ToLowerRemoveSpaces() == other.Telephone.ToLowerRemoveSpaces() &&
                             this.Mobile.ToLowerRemoveSpaces() == other.Mobile.ToLowerRemoveSpaces() &&
                             this.Image.ToLowerRemoveSpaces() == other.Image.ToLowerRemoveSpaces() &&
                             this.ImageUrl.ToLowerRemoveSpaces() == other.ImageUrl.ToLowerRemoveSpaces();

                return equal;
            }
        }

        public sealed class ContactRowMap : CsvClassMap<ContactRow>
        {
            public ContactRowMap()
            {
                Map(m => m.Id).Name("ID");
                Map(m => m.Title).Name("Title");
                Map(m => m.FirstName).Name("First Name");
                Map(m => m.LastName).Name("Last Name");
                Map(m => m.Company).Name("Company");
                Map(m => m.Position).Name("Position");
                Map(m => m.Email).Name("Email");
                Map(m => m.Telephone).Name("Telephone");
                Map(m => m.Mobile).Name("Mobile");
                Map(m => m.Image).Name("Image Filename");
                Map(m => m.ImageUrl).Name("Image Url Root Prefix");
            }
        }

        public List<string> Import(Stream csv)
        {
            IEnumerable<ContactRow> rows = new List<ContactRow>();

            using (StreamReader streamReader = new StreamReader(csv))
            {
                CsvReader csvReader = new CsvReader(streamReader);

                csvReader.Configuration.RegisterClassMap<ContactRowMap>();
                csvReader.Configuration.Delimiter = delimiter;
                csvReader.Configuration.IgnoreReadingExceptions = true;
                csvReader.Configuration.TrimFields = true;

                csvReader.Configuration.ReadingExceptionCallback = (ex, row) =>
                {
                    ProcessReadError(ex, row, ref errors);
                };

                rows = csvReader.GetRecords<ContactRow>().ToList();
            }

            errors.AddRange(ValidateDuplicates((List<ContactRow>)rows));

            contacts = unitOfWork.ContactRepository.Find(m => m.Deleted == null).ToList();
            companies = unitOfWork.MasterListRepository.Find(m => m.Deleted == null && m.Type == (MasterList.ListType)7).ToList();
            companies.Where(c => c.Name.ToLower() != "other").ToList();

            var invalidChars = Path.GetInvalidPathChars();
            int rowIndex = 0;

            foreach (ContactRow row in rows)
            {
                // Does contact exist in DB?
                var contact = contacts.Find(c => String.Equals(c.Email, row.Email, StringComparison.CurrentCultureIgnoreCase));

                if (contact != null)
                {
                    errors.Add(string.Format("Row {0}: Email address '{1}' already exists in the database. Please specify a unique email address", rowIndex + 1, row.Email));
                }
                else
                {
                    // Does contact exist in .csv file?
                    if (rows.Count(r => String.Equals(r.Email, row.Email, StringComparison.CurrentCultureIgnoreCase)) > 1)
                    {
                        errors.Add(string.Format("Row {0}: Email address '{1}' exists multiple times in this file. Please specify a unique email address", rowIndex + 1, row.Email));
                    }

                    // Does contact exist in .csv file?
                    if (row.Id != null && rows.Count(r => String.Equals(r.Id, row.Id, StringComparison.CurrentCultureIgnoreCase)) > 1)
                    {
                        errors.Add(string.Format("Row {0}: ID '{1}' exists multiple times in this file. Please specify a unique ID", rowIndex + 1, row.Email));
                    }

                    // Validate against the model
                    IEnumerable<ValidationResult> errorMessage = ValidateDomainIntegrity(row);

                    if (errorMessage.Any())
                        foreach (ValidationResult result in errorMessage)
                        {
                            errors.Add(string.Format("Row {0}: {1}", rowIndex + 1, result));
                        }

                    // Custom validation checks
                    if (companies.All(c => c.Name.ToLower() != row.Company.ToLower()))
                        errors.Add(string.Format("Row {0}: The Company field must contain a company that exists in the database", rowIndex + 1));
                    if (row.Company.ToLower() == "other")
                        errors.Add(string.Format("Row {0}: The Company field must not be 'Other'", rowIndex + 1));

                    if (string.IsNullOrEmpty(row.Position))
                        errors.Add(string.Format("Row {0}: Position required", rowIndex + 1));

                    if (!string.IsNullOrEmpty(row.Image) && !row.Image.ToLower().Contains(".jpg"))
                        errors.Add(string.Format("Row {0}: The Image field must specify to a .jpg file", rowIndex + 1));

                    if (string.IsNullOrEmpty(row.Image) && row.Image.Any(character => invalidChars.Contains(character)))
                        errors.Add(string.Format("Row {0}: The Image field contains illegal characters", rowIndex + 1));

                    if (!string.IsNullOrEmpty(row.Image) && string.IsNullOrEmpty(row.ImageUrl))
                        errors.Add(string.Format("Row {0}: The Image Url field is required if an image filename is specified", rowIndex + 1));
                    if (string.IsNullOrEmpty(row.ImageUrl) && row.ImageUrl.Any(character => invalidChars.Contains(character)))
                        errors.Add(string.Format("Row {0}: The Image Url field contains illegal characters", rowIndex + 1));

                    if (!string.IsNullOrEmpty(row.Image) && !string.IsNullOrEmpty(row.ImageUrl))
                    {
                        try
                        {
                            var reader = new WebClient();
                            reader.Headers.Add("user-agent",
                                "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
                            var data = reader.OpenRead(string.Format("{0}{1}", row.ImageUrl, row.Image));

                            // Check if an image actually exists, we don't need to process the data yet, just check it is there
                            if (data == null)
                            {
                                errors.Add(
                                    string.Format("Row {0}: The image could not be found in the specified location",
                                        rowIndex + 1));
                            }
                        }
                        catch (Exception ex)
                        {
                            errors.Add(string.Format("Row {0}: The image could not be found in the specified location",
                                rowIndex + 1));
                        }
                    }
                }
                rowIndex++;
            }

            if (!rows.Any())
                errors.Add("The .csv file contained no data");

            if (!errors.Any())
            {
                rowIndex = 0;
                foreach (ContactRow row in rows)
                {
                    // Get the company
                    var company = companies.Find(c => c.Name.ToLower() == row.Company.ToLower());
                    //var company = companies.Find(c => c.Name == row.Company);
                    var image = new Image();

                    if (!string.IsNullOrEmpty(row.Image) && !string.IsNullOrEmpty(row.ImageUrl))
                    {
                        try
                        {
                            var reader = new WebClient();
                            reader.Headers.Add("user-agent",
                                "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
                            var data = reader.OpenRead(string.Format("{0}{1}", row.ImageUrl, row.Image));
                            if (data != null)
                            {
                                var streamReader = new MemoryStream();
                                data.CopyTo(streamReader);

                                image.BinaryData = streamReader.ToArray();
                                image.FileType = Path.GetExtension(row.Image);
                                image.ContentType = "image/jpeg"; // we're only allowing jpeg files
                            }
                        }
                        catch (WebException e)
                        {
                            errors.Add(string.Format(
                                "Row {0}: The image specified was found but could not be processed", rowIndex + 1));
                        }
                    }

                    Contact contact = new Contact
                    {
                        Title = row.Title,
                        FirstName = row.FirstName,
                        LastName = row.LastName,
                        Company = company,
                        Position = row.Position,
                        Email = row.Email,
                        Phone = row.Telephone,
                        Mobile = row.Mobile,
                        IsFramework = true
                    };

                    if (!string.IsNullOrEmpty(row.Image) && !string.IsNullOrEmpty(row.ImageUrl))
                        contact.Image = image;
                    
                    unitOfWork.ContactRepository.Add(contact);
                    rowIndex++;
                }
            }

            if (!errors.Any())
            {
                unitOfWork.Save();

                //ContactService contactService = new ContactService(unitOfWork.Context as MomentumContext);
            }

            return errors;
        }

        public void Dispose()
        {
            contacts = null;
            unitOfWork = null;

            GC.Collect();
        }
    }
}