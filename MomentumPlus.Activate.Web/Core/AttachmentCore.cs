﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using MomentumPlus.Activate.Services;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Core
{
    /// <summary>
    /// Core business logic related to attachments.
    /// </summary>
    public class AttachmentCore : BaseCore
    {
        #region Dependency Injection

        private IAttachmentService AttachmentService;
        private IContactService ContactService;
        private IDocumentLibraryService DocumentLibraryService;
        private IMeetingService MeetingService;
        private IPeopleService PeopleService;
        private IProjectService ProjectService;
        private ITaskService TaskService;
        private ITopicService TopicService;
        private IHandoverService HandoverService;
        private IPositionService PositionService;
        private IPackageService PackageService;

        /// <summary>
        /// Construct a new <c>AttachmentsCore</c> object with
        /// the given service implementations.
        /// </summary>
        public AttachmentCore(
            IAttachmentService attachmentService,
            IContactService contactService,
            IDocumentLibraryService documentLibraryService,
            IMasterListService masterListService,
            IMeetingService meetingService,
            IPackageService packageService,
            IPeopleService peopleService,
            IProjectService projectService,
            ITaskService taskService,
            ITopicService topicService,
            IHandoverService handoverService,
            IPositionService positionService
        )
            : base(masterListService)
        {
            AttachmentService = attachmentService;
            ContactService = contactService;
            DocumentLibraryService = documentLibraryService;
            MeetingService = meetingService;
            PackageService = packageService;
            PeopleService = peopleService;
            ProjectService = projectService;
            TaskService = taskService;
            TopicService = topicService;
            HandoverService = handoverService;
            PositionService = positionService;
        }

        #endregion

        #region Create

        /// <summary>
        /// Create a new attachment entity from the given view model and file.
        /// </summary>
        public void CreateAttachment(AttachmentViewModel attachmentViewModel, HttpPostedFileBase postedFile)
        {
            Attachment attachment = new Attachment
            {
                ID = Guid.NewGuid(),
                Name = attachmentViewModel.Name,
                SearchTags = attachmentViewModel.SearchTags,
            };

            if (postedFile != null)
            {
                // Copy the submitted file input stream to the memory stream
                System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
                postedFile.InputStream.CopyTo(memoryStream);

                // Create a file from the sumitted memory stream
                File file = new File()
                {
                    BinaryData = memoryStream.ToArray(),
                    FileType = System.IO.Path.GetExtension(postedFile.FileName)
                };

                attachment.FileId = file.ID;
                attachment.Data = file;
                attachment.FileName = postedFile.FileName;
            }
            else if (!string.IsNullOrEmpty(attachmentViewModel.FilePath))
            {
                attachment.FilePath = attachmentViewModel.FilePath;
            }

            switch (attachmentViewModel.ItemType)
            {
                case ItemType.Contact:
                    Contact contact = ContactService.GetById(attachmentViewModel.ItemId);
                    contact.Attachments.Add(attachment);
                    ContactService.Update(contact);
                    break;

                case ItemType.Document:
                    DocumentLibrary document = DocumentLibraryService.GetByID(attachmentViewModel.ItemId);
                    document.Attachment = attachment;
                    DocumentLibraryService.UpdateDocumentLibrary(document);
                    break;

                case ItemType.Meeting:
                    Meeting meeting = MeetingService.GetByMeetingId(attachmentViewModel.ItemId);
                    meeting.Attachments.Add(attachment);
                    MeetingService.Update(meeting);
                    break;

                case ItemType.Task:
                    Task task = TaskService.GetByTaskId(attachmentViewModel.ItemId);
                    task.Attachments.Add(attachment);
                    TaskService.Update(task);
                    break;

                case ItemType.Topic:
                    Topic topic = TopicService.GetByTopicId(attachmentViewModel.ItemId);
                    topic.Attachments.Add(attachment);
                    TopicService.Update(topic);
                    break;

                case ItemType.Position:
                    attachment.InLibrary = true;
                    Position position = PositionService.GetByPositionId(attachmentViewModel.ItemId);
                    position.Attachments.Add(attachment);
                    PositionService.Update(position);
                    break;

                case ItemType.Project:
                    Project project = ProjectService.GetProjectById(attachmentViewModel.ItemId);
                    project.Attachments.Add(attachment);
                    ProjectService.Update(project);
                    break;

                case ItemType.ProjectInstance:
                    ProjectInstance projectInstance = ProjectService.GetProjectInstanceById(attachmentViewModel.ItemId);
                    projectInstance.Attachments.Add(attachment);
                    ProjectService.Update(projectInstance);
                    break;

                case ItemType.Staff:
                    Contact staff = PeopleService.GetStaff(attachmentViewModel.ItemId);
                    staff.Attachments.Add(attachment);
                   // PeopleService.UpdatePerson<Staff>(staff);
                    break;
            }
        }

        #endregion

        #region Read

        /// <summary>
        /// Get the view model for an existing attachment with the given ID, parent, and type.
        /// </summary>
        public AttachmentViewModel GetAttachmentViewModel(Guid id, Guid itemId, ItemType type)
        {
            // Get attachment from the database
            Attachment attachment = AttachmentService.GetByID(id);

            // Populate a view model
            AttachmentViewModel viewModel = Mapper.Map<Attachment, AttachmentViewModel>(attachment);
            viewModel.ItemId = itemId;
            viewModel.ItemType = type;
            viewModel.FileTypes = AttachmentHelper.AllowedFileTypes;

            return viewModel;
        }

        /// <summary>
        /// Get the attachments view model for the item with the given ID and type.
        /// </summary>
        public AttachmentsViewModel GetAttachmentsViewModel(Guid id, ItemType type, Package currentPackage = null)
        {
            IEnumerable<Attachment> attachments = new List<Attachment>();
            IEnumerable<DocumentLibrary> documents = new List<DocumentLibrary>();
            string itemName = string.Empty;
            Package package = null;

            // Get the attachments and documents based on item type
            switch (type)
            {
                case ItemType.Contact:
                    var contact = ContactService.GetById(id);
                    attachments = contact.Attachments;
                    documents = contact.DocumentLibraries;
                    itemName = contact.FullName;
                    break;
                case ItemType.Document:
                    var document = DocumentLibraryService.GetByID(id);
                    var attachmentsList = new List<Attachment>();
                    if (document.Attachment != null)
                    {
                        attachmentsList.Add(document.Attachment);
                    }
                    attachments = attachmentsList;
                    itemName = document.Name;
                    break;
                case ItemType.Meeting:
                    var meeting = MeetingService.GetByMeetingId(id);
                    attachments = meeting.Attachments;
                    documents = meeting.DocumentLibraries;
                    itemName = meeting.Name;
                    break;
                case ItemType.Task:
                    var task = TaskService.GetByTaskId(id);
                    attachments = task.Attachments;
                    documents = task.DocumentLibraries;
                    itemName = task.Name;
                    package = PackageService.GetByTaskId(id);
                    break;
                case ItemType.Topic:
                    var topic = TopicService.GetByTopicId(id);
                    attachments = topic.Attachments;
                    documents = topic.DocumentLibraries;
                    itemName = topic.Name;
                    package = PackageService.GetByTopicId(id);
                    break;
                case ItemType.Position:
                    var position = PositionService.GetByPositionId(id);
                    attachments = position.Attachments;
                    documents = position.DocumentLibraries;
                    itemName = position.Name;
                    break;
                case ItemType.Project:
                    var project = ProjectService.GetProjectById(id);
                    attachments = project.Attachments;
                    documents = project.DocumentLibraries;
                    itemName = project.Name;
                    break;
                case ItemType.ProjectInstance:
                    var projectInstance = ProjectService.GetProjectInstanceById(id);
                    attachments = projectInstance.Attachments;
                    documents = projectInstance.DocumentLibraries;
                    itemName = projectInstance.Project != null ? projectInstance.Project.Name : projectInstance.NewProjectRequest.SuggestedName;
                    break;
                case ItemType.Staff:
                    var staff = PeopleService.GetStaff(id);
                    attachments = staff.Attachments;
                    documents = staff.DocumentLibraries;
                    //itemName = staff.FullName;
                    break;
            }

            // Map to view models
            var attachmentViewModels = Mapper.Map<IEnumerable<Attachment>,
                IEnumerable<AttachmentViewModel>>(attachments);
            var documentViewModels = Mapper.Map<IEnumerable<DocumentLibrary>,
                IEnumerable<DocumentLibraryFormViewModel>>(documents);

            // When true will hide edit links from onboarder
            var onboarderView = package != null && currentPackage != null && currentPackage.IsOnboarderFor(package);

            return new AttachmentsViewModel
            {
                Attachments = attachmentViewModels,
                Documents = documentViewModels,
                ItemId = id,
                ItemType = type,
                ItemName = itemName,
                OnboarderView = onboarderView
            };
        }

        #endregion

        #region Update

        /// <summary>
        /// Update an existing attachment given its view model and uploaded file.
        /// </summary>
        public void UpdateAttachment(
            Attachment attachment,
            AttachmentViewModel attachmentViewModel,
            HttpPostedFileBase postedFile
        )
        {
            // Update the attachment from the submitted data
            attachment.Name = attachmentViewModel.Name;
            attachment.SearchTags = attachmentViewModel.SearchTags;

            // Check if the view model holds the file
            if (postedFile != null)
            {
                // Copy the submitted file input stream to the memory stream
                System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
                postedFile.InputStream.CopyTo(memoryStream);

                // Create a file from the submitted memory stream
                File file = new File()
                {
                    BinaryData = memoryStream.ToArray(),
                    FileType = System.IO.Path.GetExtension(postedFile.FileName)
                };

                // Update the attachment
                attachment.FileId = file.ID;
                attachment.Data = file;
                attachment.FileName = postedFile.FileName;

                attachment.FilePath = null;
            }
            else if (!string.IsNullOrEmpty(attachmentViewModel.FilePath))
            {
                attachment.FilePath = attachmentViewModel.FilePath;

                attachment.FileId = null;
                attachment.Data = null;
                attachment.FileName = null;
            }

            switch (attachmentViewModel.ItemType)
            {
                case ItemType.Contact:
                    Contact contact = ContactService.GetById(attachmentViewModel.ItemId);
                    UpdateAttachment(contact.Attachments, attachment, postedFile);
                    ContactService.Update(contact);
                    break;

                case ItemType.Meeting:
                    Meeting meeting = MeetingService.GetByMeetingId(attachmentViewModel.ItemId);
                    UpdateAttachment(meeting.Attachments, attachment, postedFile);
                    MeetingService.Update(meeting);
                    break;

                case ItemType.Task:
                    Task task = TaskService.GetByTaskId(attachmentViewModel.ItemId);
                    UpdateAttachment(task.Attachments, attachment, postedFile);
                    TaskService.Update(task);
                    break;

                case ItemType.Topic:
                    Topic topic = TopicService.GetByTopicId(attachmentViewModel.ItemId);
                    UpdateAttachment(topic.Attachments, attachment, postedFile);
                    TopicService.Update(topic);
                    break;

                case ItemType.Position:
                    Position position = PositionService.GetByPositionId(attachmentViewModel.ItemId);
                    UpdateAttachment(position.Attachments, attachment, postedFile);
                    PositionService.Update(position);
                    break;

                case ItemType.Project:
                    Project project = ProjectService.GetProjectById(attachmentViewModel.ItemId);
                    UpdateAttachment(project.Attachments, attachment, postedFile);
                    ProjectService.Update(project);
                    break;

                case ItemType.ProjectInstance:
                    ProjectInstance projectInstance = ProjectService.GetProjectInstanceById(attachmentViewModel.ItemId);
                    UpdateAttachment(projectInstance.Attachments, attachment, postedFile);
                    ProjectService.Update(projectInstance);
                    break;

                case ItemType.Staff:
                    Contact staff = PeopleService.GetStaff(attachmentViewModel.ItemId);
                    UpdateAttachment(staff.Attachments, attachment, postedFile);
                    //PeopleService.UpdatePerson<Staff>(staff);
                    break;
            }
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Update an attachment within a collection.
        /// </summary>
        private void UpdateAttachment(
            IEnumerable<Attachment> attachments,
            Attachment newAttachment,
            HttpPostedFileBase file
        )
        {
            var attachment = attachments.FirstOrDefault(a => a.ID == newAttachment.ID);

            attachment.Name = newAttachment.Name;
            attachment.SearchTags = newAttachment.SearchTags;
            attachment.FilePath = newAttachment.FilePath;

            if (file != null)
            {
                attachment.FileId = newAttachment.FileId;
                attachment.Data = newAttachment.Data;
                attachment.FileName = newAttachment.FileName;
            }
            else
            {
                //attachment.FileId = null;
                //attachment.Data = null;
                //attachment.FileName = null;
            }
        }

        #endregion
    }
}