﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using MomentumPlus.Activate.Services;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Core
{
    /// <summary>
    /// Core business logic related to document library.
    /// </summary>
    public class DocumentLibraryCore : BaseCore
    {
        #region Constants

        private const int AttachmentCount = 10;

        #endregion

        #region Dependency Injection

        private IAuditService AuditService;
        private IContactService ContactService;
        private IDocumentLibraryService DocumentLibraryService;
        private IMeetingService MeetingService;
        private IPackageService PackageService;
        private IPeopleService PeopleService;
        private IPositionService PositionService;
        private IProjectService ProjectService;
        private ITaskService TaskService;
        private ITopicService TopicService;
        private IHandoverService HandoverService;

        public DocumentLibraryCore(
            IAuditService auditService,
            IContactService contactService,
            IDocumentLibraryService documentLibraryService,
            IMasterListService masterListService,
            IMeetingService meetingService,
            IPackageService packageService,
            IPeopleService peopleService,
            IPositionService positionService,
            IProjectService projectService,
            ITaskService taskService,
            ITopicService topicService,
            IHandoverService handoverService
        )
            : base(masterListService)
        {
            AuditService = auditService;
            ContactService = contactService;
            DocumentLibraryService = documentLibraryService;
            MeetingService = meetingService;
            PackageService = packageService;
            PeopleService = peopleService;
            PositionService = positionService;
            ProjectService = projectService;
            TaskService = taskService;
            TopicService = topicService;
            HandoverService = handoverService;
        }

        #endregion

        #region Create

        /// <summary>
        /// Create a new document entity from the give view model and file.
        /// </summary>
        public Guid CreateDocument(DocumentLibraryFormViewModel viewModel, HttpPostedFileBase postedFile)
        {
            // Create the document attachment
            Attachment attachment = new Attachment
            {
                ID = Guid.NewGuid(),
                Name = viewModel.Name,
                SearchTags = viewModel.SearchTags,
            };

            if (postedFile != null)
            {
                // Copy the submitted file input stream to the memory stream
                System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
                postedFile.InputStream.CopyTo(memoryStream);

                // Create a file from the sumitted memory stream
                File file = new File()
                {
                    BinaryData = memoryStream.ToArray(),
                    FileType = System.IO.Path.GetExtension(postedFile.FileName)
                };

                attachment.FileId = file.ID;
                attachment.Data = file;
                attachment.FileName = postedFile.FileName;
            }
            else if (!string.IsNullOrEmpty(viewModel.FilePath))
            {
                attachment.FilePath = viewModel.FilePath;
            }

            var document = new DocumentLibrary();
            document.Attachment = attachment;
            document.SearchTags = viewModel.SearchTags;
            document.Name = viewModel.Name;
            document.Positions = viewModel.PositionIDs.Select(id => PositionService.GetByPositionId(id)).ToList();

            DocumentLibraryService.Add(document);

            AuditService.RecordActivity<DocumentLibrary>(AuditLog.EventType.Created, document.ID);

            return document.ID;
        }

        #endregion

        #region Read

        public DocumentLibraryViewModel GetDocumentLibraryViewModel(
            string Query,
            Guid? PositionId,
            int pageNo,
            DocumentLibrary.SortColumn sortColumn,
            bool sortAscending,
            Guid? ItemId,
            ItemType? ItemType,
            bool retired,
            Guid? currentPackageId = null
        )
        {
            var model = new DocumentLibraryViewModel
            {
                Query = Query,
                PageNo = pageNo,
                PageSize = AttachmentCount,
                ItemId = ItemId,
                ItemType = ItemType,
                Retired = retired
            };

            // Get positions and generate select list items
            var positions = PositionService.GetAll();
            model.Positions = new List<SelectListItem> {
                new SelectListItem { Text = "In All Positions", Value = "" }
            };
            model.Positions.AddRange(positions.Select(p =>
                new SelectListItem { Text = "In " + p.Name, Value = p.ID.ToString() }
            ));

            // If an item ID is passed then we are adding an attachment from the library to that item
            if (ItemId.HasValue)
            {
                // Use position ID from current package when none is specified
                if (currentPackageId.HasValue)
                {
                    var currentPackage = PackageService.GetByPackageId(currentPackageId.Value);
                    PositionId = currentPackage.PositionId;
                    model.DisablePositionFilter = true;
                }
            }

            if (positions.Any())
            {
                // Get docs for selected position
                model.PositionId = PositionId.HasValue ? PositionId.Value : new Nullable<Guid>();

                var documents = DocumentLibraryService.Search(model.PositionId, model.Query, pageNo, AttachmentCount,
                    sortColumn, sortAscending, retired && !ItemId.HasValue);

                if (documents != null && documents.Any())
                {
                    model.DocumentLibraries = Mapper.Map<IEnumerable<DocumentLibrary>,
                        IEnumerable<DocumentLibraryFormViewModel>>(documents);
                }

                model.TotalCount = DocumentLibraryService.Count(model.PositionId, model.Query);
            }

            return model;
        }

        #endregion

        #region Update

        /// <summary>
        /// Update an existing document given its view and model and uploaded file.
        /// </summary>
        public DocumentLibrary UpdateDocument(
            DocumentLibrary document,
            DocumentLibraryFormViewModel viewModel,
            HttpPostedFileBase theFile
        )
        {
            // Get attachment from the database
            document.Attachment = document.Attachment ?? new Attachment();

            // Update the attachment from the submitted data
            document.Attachment.Name = viewModel.Name;
            document.Attachment.SearchTags = viewModel.SearchTags;

            if (theFile != null)
            {
                // Copy the submitted file input stream to the memory stream
                System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
                theFile.InputStream.CopyTo(memoryStream);

                // Create a file from the submitted memory stream
                File file = new File()
                {
                    BinaryData = memoryStream.ToArray(),
                    FileType = System.IO.Path.GetExtension(theFile.FileName)
                };

                document.Attachment.FileId = file.ID;
                document.Attachment.Data = file;
                document.Attachment.FileName = theFile.FileName;

                document.Attachment.FilePath = null;
            }
            else if (!string.IsNullOrEmpty(viewModel.FilePath))
            {
                document.Attachment.FilePath = viewModel.FilePath;

                document.Attachment.FileId = null;
                document.Attachment.Data = null;
                document.Attachment.FileName = null;
            }

            document.SearchTags = viewModel.SearchTags;
            document.Name = viewModel.Name;
            // Need to remove positions before we add new ones. Data integrity error otherwise
            foreach (var position in document.Positions.ToList())
            {
                document.Positions.Remove(position);
            }
            document.Positions = viewModel.PositionIDs.Select(id => PositionService.GetByPositionId(id)).ToList();

            DocumentLibraryService.UpdateDocumentLibrary(document);

            AuditService.RecordActivity<DocumentLibrary>(AuditLog.EventType.Modified, document.ID);

            return document;
        }

        /// <summary>
        /// Add a document to an item.
        /// </summary>
        public void Add(Guid id, Guid itemId, ItemType itemType)
        {
            switch (itemType)
            {
                case ItemType.Contact: DocumentLibraryService.AddToContact(id, itemId); break;
                case ItemType.Meeting: DocumentLibraryService.AddToMeeting(id, itemId); break;
                case ItemType.Task: DocumentLibraryService.AddToTask(id, itemId); break;
                case ItemType.Topic: DocumentLibraryService.AddToTopic(id, itemId); break;
                case ItemType.Project: DocumentLibraryService.AddToProject(id, itemId); break;
                case ItemType.ProjectInstance: DocumentLibraryService.AddToProjectInstance(id, itemId); break;
                case ItemType.Staff: DocumentLibraryService.AddToStaff(id, itemId); break;
            }
        }

        #endregion

        #region Unlink

        /// <summary>
        /// Remove a document link from an item.
        /// </summary>
        public void UnlinkDocument(Guid id, Guid itemId, ItemType type)
        {
            switch (type)
            {
                case ItemType.Contact:
                    var contact = ContactService.GetById(itemId);
                    contact.DocumentLibraries = RemoveDocument(contact.DocumentLibraries.ToList(), id);
                    ContactService.Update(contact);
                    break;
                case ItemType.Meeting:
                    var meeting = MeetingService.GetByMeetingId(itemId);
                    meeting.DocumentLibraries = RemoveDocument(meeting.DocumentLibraries.ToList(), id);
                    MeetingService.Update(meeting);
                    break;
                case ItemType.Task:
                    var task = TaskService.GetByTaskId(itemId);
                    task.DocumentLibraries = RemoveDocument(task.DocumentLibraries.ToList(), id);
                    TaskService.Update(task);
                    break;
                case ItemType.Topic:
                    var topic = TopicService.GetByTopicId(itemId);
                    topic.DocumentLibraries = RemoveDocument(topic.DocumentLibraries.ToList(), id);
                    TopicService.Update(topic);
                    break;
                case ItemType.Position:
                    var position = PositionService.GetByPositionId(itemId);
                    position.DocumentLibraries = RemoveDocument(position.DocumentLibraries.ToList(), id);
                    PositionService.Update(position);
                    break;
                case ItemType.Project:
                    var project = ProjectService.GetProjectById(itemId);
                    project.DocumentLibraries = RemoveDocument(project.DocumentLibraries.ToList(), id);
                    ProjectService.Update(project);
                    break;
                case ItemType.ProjectInstance:
                    var projectInstance = ProjectService.GetProjectInstanceById(itemId);
                    projectInstance.DocumentLibraries = RemoveDocument(projectInstance.DocumentLibraries.ToList(), id);
                    ProjectService.Update(projectInstance);
                    break;
                case ItemType.Staff:
                    var staff = PeopleService.GetStaff(itemId);
                    staff.DocumentLibraries = RemoveDocument(staff.DocumentLibraries.ToList(), id);
                    //PeopleService.UpdatePerson<Staff>(staff);
                    break;
            }
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Populate the document libary form view model with the data required for rendering the form.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public DocumentLibraryFormViewModel PopulateDocLibFormViewModel(DocumentLibraryFormViewModel viewModel)
        {
            var positions = PositionService.GetAll(Position.SortColumn.Name).ToList();
            viewModel.PositionSet = positions
                .Select(p => new SelectListItem
                {
                    Text = p.Name,
                    Value = p.ID.ToString()
                });

            var document = DocumentLibraryService.GetByID(viewModel.ID);

            if (document != null && document.Positions != null && document.Positions.Any())
            {
                viewModel.PositionIDs = document.Positions.Select(p => p.ID).ToList();
            }
            else
            {
                viewModel.PositionIDs = positions.Select(p => p.ID).ToList();
            }

            if (document != null && document.Attachment != null)
            {
                viewModel.FilePath = document.Attachment.FilePath;
            }

            viewModel.FileTypes = AttachmentHelper.AllowedFileTypes;

            return viewModel;
        }

        /// <summary>
        /// Remove the document with a given ID from a list of documents, returning the
        /// new modified list.
        /// </summary>
        private List<DocumentLibrary> RemoveDocument(List<DocumentLibrary> documents, Guid id)
        {
            var document = documents.FirstOrDefault(d => d.ID == id);

            if (document != null)
            {
                documents.Remove(document);
            }

            return documents;
        }

        #endregion
    }
}