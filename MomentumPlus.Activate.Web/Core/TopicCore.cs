﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using MomentumPlus.Activate.Services;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Extensions;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.People;

namespace MomentumPlus.Activate.Web.Core
{
    public class TopicCore : BaseCore
    {
        #region Question References

        // Compliance
        private static readonly Guid DocumentNameReference = new Guid("728b6729-ef17-4ed4-840e-04bde52b6080");
        private static readonly Guid RegulationAndComplianceNotesReference = new Guid("6d5632c2-277d-4a0c-a20a-2b3f0f8955da");
        private static readonly Guid DocumentLocationReference = new Guid("54f0841b-1b05-4507-914a-afcfc733a3de");
        private static readonly Guid PrimaryContactReference = new Guid("2586eaa1-3f70-4e7b-afcb-75727e04e2b5");
        private static readonly Guid ReferenceWebsiteReference = new Guid("06a155fa-2cc6-4aac-9e22-f9811371ec86");

        // Experience
        private static readonly Guid WhenReference = new Guid("1f6e203d-1f1e-44e5-ab02-cbc363e72093");
        private static readonly Guid KeyContactReference = new Guid("c2ba967d-9fa6-4f6e-9abd-653625a0892a");

        // Location
        private static readonly Guid LocationReference = new Guid("96f47783-2dc2-4b2b-8712-b922a145c4d8");
        private static readonly Guid AddressReference = new Guid("efd39684-aab5-45b5-bf03-8d5c7096a65a");
        private static readonly Guid MapLinkReference = new Guid("460db77b-20c9-4177-a280-b44ade0cd588");

        // Process
        private static readonly Guid WorkaroundNotesReference = new Guid("725bab9f-e792-43a4-ba20-af405b4f30d1");
        private static readonly Guid ProcessInteractionReference = new Guid("6547d569-f698-4955-8568-7ebc36cddda3");
        private static readonly Guid FitForPurposeReference = new Guid("906b79cf-cbb0-4178-a238-607bafd3d816");
        private static readonly Guid FitForPurposeReasonNIReference = new Guid("7422b231-b9c0-4089-bc84-a2f27695536f");
        private static readonly Guid ProjectReferenceReference = new Guid("3a150fe1-5901-4464-893a-9a048d15f244");

        // Project
        private static readonly Guid ProjectIDReference = new Guid("30739ac6-c0e3-4ceb-b946-96a1ba670357");
        private static readonly Guid ProjectSponsorReference = new Guid("70c233c4-0ff3-4e5e-9623-2317df6b9207");
        private static readonly Guid ProjectManagerReference = new Guid("8c3bbc57-4d61-4feb-bd13-edbecc0e88b7");
        private static readonly Guid BudgetedSpendReference = new Guid("0d386c5f-eb5d-4ecd-8b44-75ea85db269f");
        private static readonly Guid ProjectStatusReference = new Guid("098a23ea-f9d9-4480-a910-bd1a9ac5b6ac");
        private static readonly Guid PerceivedSuccessReference = new Guid("061205c0-4dc1-4988-b275-d600b8ac04e8");
        private static readonly Guid ProjectEndDateReference = new Guid("c083faa6-fdc6-453e-b934-0a68b6ff917c");

        // Skills
        private static readonly Guid TrainingFrequencyReference = new Guid("c4149dd4-ee5f-44fd-a7ac-4f8b3fa859c3");
        private static readonly Guid PerceivedStandardOfTrainingReference = new Guid("bccac6ba-f725-4abe-b882-515fb7d64872");

        // Strategic
        private static readonly Guid NoteTitleReference = new Guid("4c598520-d6fd-437c-8a14-8a5cf08ccc45");
        private static readonly Guid ReviewFrequencyReference = new Guid("6de49fb8-51d2-40e8-906f-990453e77702");
        private static readonly Guid QualityReference = new Guid("aad84a0b-d73e-4e4c-b2e9-01d0ee3ea6a4");

        // Systems
        private static readonly Guid NameOfSystemReference = new Guid("1c0dde96-366c-4b35-9720-718ed5d33d44");
        private static readonly Guid SystemFrequencyOfUseReference = new Guid("7b3307a8-0796-4c53-8366-927e5fea04f8");
        private static readonly Guid ExpertUserReference = new Guid("a5101f6c-79e1-489f-9c8a-fe60dca31a2d");
        private static readonly Guid ImportanceRatingReference = new Guid("ed414418-34de-4d0e-bb2b-66ddf10fed0f");
        private static readonly Guid WebsiteAddressReference = new Guid("ba39daef-5866-473b-a8ac-81bc1feda209");

        // Tools
        private static readonly Guid AssetIDReference = new Guid("885c86dd-466a-4d02-868e-5a959775d073");
        private static readonly Guid EquipmentFrequencyOfUseReference = new Guid("8ea31bc2-03a7-424a-9577-8d201b347151");
        private static readonly Guid ConditionReference = new Guid("bb9bec72-4314-4da0-a826-ce872eea7e75");
        private static readonly Guid ExperiencedContactReference = new Guid("6fd3fb57-c24e-4ead-b57d-c1ffb3b3484b");

        // Various
        private static readonly Guid LODContactReference = new Guid("ece46528-b220-41a0-9dc9-8f31df47d1c1");
        private static readonly Guid TrainingRequiredReference = new Guid("617e48ab-c801-4434-81bf-67996580ea5e");

        // Unused
        //private static readonly Guid HandoverTopicReference = new Guid("66915a28-a5ea-402d-a38f-2d8c5cc73d3c");
        //private static readonly Guid HandoverTaskReference = new Guid("9ceb7961-d7be-47ee-b7ee-b5aec0249859");
        //private static readonly Guid EmployeeCopyReference = new Guid("200bb83e-dd51-4c27-907b-b002051d6ffb");
        //private static readonly Guid ProcessNotesReference = new Guid("24a19719-858f-4a2c-8df5-414ac0934232");
        //private static readonly Guid ProjectNameReference = new Guid("42a75579-49aa-4bdd-a5c4-ddc9bf1f172a");
        //private static readonly Guid NotesReference = new Guid("fa5743fd-22f0-41c2-bb77-1aa3625dd92f");
        //private static readonly Guid TimeRequiredReference = new Guid("531d96fc-dc3a-4922-a7d7-f4ede019a263");
        //private static readonly Guid ImpactOfDocumentReference = new Guid("87f058e1-78a1-48f6-9076-4e93fe6d250c");

        #endregion

        #region Dependency Injection

        private IAuditService AuditService;
        private IHandoverService HandoverService;
        private IPackageService PackageService;
        private ITaskService TaskService;
        private ITopicService TopicService;
        private ITopicGroupService TopicGroupService;
        private ITopicQuestionService TopicQuestionService;
        private IWorkplaceService WorkplaceService;

        public TopicCore(
            IAuditService auditService,
            IHandoverService handoverService,
            IMasterListService masterListService,
            IPackageService packageService,
            ITaskService taskService,
            ITopicService topicService,
            ITopicGroupService topicGroupService,
            ITopicQuestionService topicQuestionService,
            IWorkplaceService workplaceService
        )
            : base(masterListService)
        {
            AuditService = auditService;
            HandoverService = handoverService;
            PackageService = packageService;
            TaskService = taskService;
            TopicService = topicService;
            TopicGroupService = topicGroupService;
            TopicQuestionService = topicQuestionService;
            WorkplaceService = workplaceService;
        }

        #endregion

        #region Create

        /// <summary>
        /// Get a new topic view model for a given topic group and user.
        /// </summary>
        public TopicViewModel GetNewTopicViewModel(Guid topicId, User currentUser)
        {
            var topicGroup = TopicGroupService.GetByTopicGroupId(topicId);
            var package = PackageService.GetByTopicGroupId(topicId);

            var topicViewModel = GetModel(topicGroup);
            topicViewModel.FrameworkItem = package == null;

            return topicViewModel;
        }

        /// <summary>
        /// Create a new topic in the given topic group from the given view model.
        /// </summary>
        public Topic CreateTopic(TopicGroup topicGroup, TopicViewModel topicViewModel)
        {
            // Create new topic and populate the fields and properties
            var topic = new Topic();
            SetAnswers(topicGroup, topic, topicViewModel);
            UpdateTopicFields(topic, topicViewModel);

            // Create topic and update audit log
            TopicService.Add(topicGroup.ID, topic);
            AuditService.RecordActivity<Topic>(AuditLog.EventType.Created, topic.ID);
            AuditService.RecordActivity<Topic>(AuditLog.EventType.Viewed, topic.ID);

            return topic;
        }

        #endregion

        #region Read

        public TopicViewModel GetTopicViewModel(Topic topic, bool prevTopic, User currentUser)
        {
            var topicGroup = TopicGroupService.GetByTopicId(topic.ID);
            var package = PackageService.GetByTopicGroupId(topicGroup.ID);

            var topicViewModel = GetModel(topicGroup, topic);
            GetAnswers(topicGroup, topic, topicViewModel);
            topicViewModel.FrameworkItem = package == null;

            if (package != null)
            {
                var onboardingPeriod = HandoverService.GetOnboardingPeriodForPackage(package.ID);

                if (onboardingPeriod != null && onboardingPeriod.ViewPreviousPackageContent)
                {
                    Topic onboardingTopic = null;

                    if (topic.SourceId.HasValue && package.AncestorPackageId.HasValue)
                    {
                        onboardingTopic = TopicService.GetByTopicSourceId(topic.SourceId.Value, package.AncestorPackageId.Value);
                    }

                    // Show onboarding topics for cloning
                    if (onboardingTopic != null)
                    {
                        topicViewModel.HasPrevTopic = true;
                        topicViewModel.ReadOnly = RoleAuthHelper.IsReadOnly("Topic_Edit_Get");

                        if (prevTopic)
                        {
                            var onboardingTopicViewModel = GetModel(topicGroup, onboardingTopic);
                            GetAnswers(topicGroup, onboardingTopic, onboardingTopicViewModel);

                            var leaverPackage = PackageService.GetByTopicId(onboardingTopic.ID);
                            onboardingTopicViewModel.LeaverName = leaverPackage.UserOwner.FirstName + " " + leaverPackage.UserOwner.LastName;
                            onboardingTopicViewModel.ReadOnly = true;
                            onboardingTopicViewModel.IsPrevTopic = true;

                            topicViewModel.PrevTopic = onboardingTopicViewModel;
                        }
                    }
                } 
            }

            topicViewModel.ReadOnly = RoleAuthHelper.IsReadOnly("Topic_Edit_Get");

            return topicViewModel;
        }

        #endregion

        #region Update

        /// <summary>
        /// Update an existing topic based on the given view model.
        /// </summary>
        public void UpdateTopic(
            Package package,
            TopicGroup topicGroup,
            Topic topic,
            TopicViewModel topicViewModel,
            User currentUser
        )
        {
            SetAnswers(topicGroup, topic, topicViewModel);
            UpdateTopicFields(topic, topicViewModel);

            // Clone from leaver
            if (topicViewModel.CloneVoiceMessages.HasValue)
            {
                var leaverTopic = TopicService.GetByTopicId(topicViewModel.CloneVoiceMessages.Value);
                var clonedMessages = leaverTopic.VoiceMessages.Select(v =>
                {
                    v = v.DeepClone();
                    v.ParentTopic = topic;
                    v.ParentTopicId = topic.ID;
                    return v;
                });
                clonedMessages.Each(v => topic.VoiceMessages.Add(v));
            }

            if (topicViewModel.CloneAttachments.HasValue)
            {
                var leaverTopic = TopicService.GetByTopicId(topicViewModel.CloneAttachments.Value);
                var clonedAttachments = leaverTopic.Attachments.Select(a =>
                {
                    a = a.DeepClone();
                    a.ParentTopic = topic;
                    a.ParentTopicId = topic.ID;
                    return a;
                });
                clonedAttachments.Each(a => topic.Attachments.Add(a));
            }

            if (topicViewModel.CloneTasks.HasValue)
            {
                var leaverTopic = TopicService.GetByTopicId(topicViewModel.CloneTasks.Value);
                var clonedTasks = leaverTopic.Tasks.Select(t =>
                {
                    t = t.DeepClone();
                    t.ParentTopic = topic;
                    t.ParentTopicId = topic.ID;
                    return t;
                });
                clonedTasks.Each(t => topic.Tasks.Add(t));
            }

            // Make the changes and update the audit log
            TopicService.Update(topic);
            AuditService.RecordActivity<Topic>(AuditLog.EventType.Modified, topic.ID);
        }

        #endregion

        #region Helpers

        #region GetModel

        /// <summary>
        /// Get the the view model for a topic in a given group.
        /// </summary>
        /// <param name="topicGroup">The topic group entity.</param>
        /// <param name="topic">
        /// An optional topic entity that will be used to populate the model.
        /// </param>
        /// <returns>A new <c>TopicViewModel</c> instance.</returns>
        public TopicViewModel GetModel(TopicGroup topicGroup, Topic topic = null)
        {
            TopicViewModel topicViewModel = null;

            // Create the correct type of model based on the topic group type
            switch (topicGroup.TopicGroupType)
            {
                case TopicGroup.TopicType.Compliance:
                    topicViewModel = new ComplianceTopicViewModel();
                    break;
                case TopicGroup.TopicType.Experience:
                case TopicGroup.TopicType.EnjoyedLeast:
                case TopicGroup.TopicType.Achievements:
                case TopicGroup.TopicType.Changes:
                case TopicGroup.TopicType.OtherExperiences:
                case TopicGroup.TopicType.BestThings:
                case TopicGroup.TopicType.TeamAchievements:
                    topicViewModel = new ExperienceTopicViewModel();
                    break;
                case TopicGroup.TopicType.Location:
                    topicViewModel = new LocationTopicViewModel();
                    break;
                case TopicGroup.TopicType.Process:
                    topicViewModel = new ProcessTopicViewModel();
                    break;
                case TopicGroup.TopicType.Skills:
                    topicViewModel = new SkillsTopicViewModel();
                    break;
                case TopicGroup.TopicType.Strategic:
                    topicViewModel = new StrategicTopicViewModel();
                    break;
                case TopicGroup.TopicType.Systems:
                    topicViewModel = new SystemsTopicViewModel();
                    break;
                case TopicGroup.TopicType.Tools:
                    topicViewModel = new ToolsTopicViewModel();
                    break;
            }

            if (topicViewModel != null)
            {
                topicViewModel.TopicGroupId = topicGroup.ID;

                if (topic != null)
                {
                    Mapper.Map<Topic, TopicViewModel>(topic, topicViewModel);

                    topicViewModel.NotesSubjectivity = topic.CountSubjectivity(MasterListService);
                    topicViewModel.NotesWordCount = topic.CountWords();
                    topicViewModel.HasOpenTasks = topic.Tasks.Any(t => !t.ReadyForReview);
                }

                GetLabels(topicGroup, topicViewModel);
                PopulateTopicViewModel(topicGroup, topicViewModel);
            }

            return topicViewModel;
        }

        #endregion

        #region GetLabel

        /// <summary>
        /// Get the label for a given question.
        /// </summary>
        /// <param name="questionReference">The question reference ID.</param>
        /// <returns>The question label string.</returns>
        public string GetLabel(Guid questionReference)
        {
            var question = TopicQuestionService.GetByReference(questionReference);
            return question != null ? question.Label : string.Empty;
        }

        #endregion

        #region GetLabels

        /// <summary>
        /// Populate the labels in the view model based on the topic group type.
        /// </summary>
        public void GetLabels(TopicGroup topicGroup, TopicViewModel topicViewModel)
        {
            switch (topicGroup.TopicGroupType)
            {
                case TopicGroup.TopicType.Compliance:
                    GetLabels(topicViewModel as ComplianceTopicViewModel);
                    break;
                case TopicGroup.TopicType.Experience:
                case TopicGroup.TopicType.EnjoyedLeast:
                case TopicGroup.TopicType.Achievements:
                case TopicGroup.TopicType.Changes:
                case TopicGroup.TopicType.OtherExperiences:
                case TopicGroup.TopicType.BestThings:
                case TopicGroup.TopicType.TeamAchievements:
                    GetLabels(topicViewModel as ExperienceTopicViewModel);
                    break;
                case TopicGroup.TopicType.Location:
                    GetLabels(topicViewModel as LocationTopicViewModel);
                    break;
                case TopicGroup.TopicType.Process:
                    GetLabels(topicViewModel as ProcessTopicViewModel);
                    break;
                case TopicGroup.TopicType.Skills:
                    GetLabels(topicViewModel as SkillsTopicViewModel);
                    break;
                case TopicGroup.TopicType.Strategic:
                    GetLabels(topicViewModel as StrategicTopicViewModel);
                    break;
                case TopicGroup.TopicType.Systems:
                    GetLabels(topicViewModel as SystemsTopicViewModel);
                    break;
                case TopicGroup.TopicType.Tools:
                    GetLabels(topicViewModel as ToolsTopicViewModel);
                    break;
            }
        }

        /// <summary>
        /// Populate the labels on the view model for a Compliance topic.
        /// </summary>
        public void GetLabels(ComplianceTopicViewModel complianceTopicViewModel)
        {
            complianceTopicViewModel.DocumentNameLabel = GetLabel(DocumentNameReference);
            complianceTopicViewModel.RegulationAndComplianceNotesLabel = GetLabel(RegulationAndComplianceNotesReference);
            complianceTopicViewModel.DocumentLocationLabel = GetLabel(DocumentLocationReference);
            complianceTopicViewModel.PrimaryContactLabel = GetLabel(PrimaryContactReference);
            complianceTopicViewModel.ReferenceWebsiteLabel = GetLabel(WebsiteAddressReference);
            complianceTopicViewModel.TrainingRequiredLabel = GetLabel(TrainingRequiredReference);
        }

        /// <summary>
        /// Populate the labels on the view model for a Experience topic.
        /// </summary>
        public void GetLabels(ExperienceTopicViewModel experienceTopicViewModel)
        {
            experienceTopicViewModel.WhenLabel = GetLabel(WhenReference);
            experienceTopicViewModel.KeyContactLabel = GetLabel(KeyContactReference);
        }

        /// <summary>
        /// Populate the labels on the view model for a Location topic.
        /// </summary>
        public void GetLabels(LocationTopicViewModel locationTopicViewModel)
        {
            locationTopicViewModel.LocationLabel = GetLabel(LocationReference);
            locationTopicViewModel.AddressLabel = GetLabel(AddressReference);
            locationTopicViewModel.MapLinkLabel = GetLabel(MapLinkReference);
        }

        /// <summary>
        /// Populate the labels on the view model for a Process topic.
        /// </summary>
        public void GetLabels(ProcessTopicViewModel processTopicViewModel)
        {
            processTopicViewModel.WorkaroundNotesLabel = GetLabel(WorkaroundNotesReference);
            processTopicViewModel.ProcessInteractionLabel = GetLabel(ProcessInteractionReference);
            processTopicViewModel.ContactLabel = GetLabel(LODContactReference);
            processTopicViewModel.FitForPurposeLabel = GetLabel(FitForPurposeReference);
            processTopicViewModel.FitForPurposeReasonLabel = GetLabel(FitForPurposeReasonNIReference);
            processTopicViewModel.ProjectReferenceLabel = GetLabel(ProjectReferenceReference);
        }

        /// <summary>
        /// Populate the labels on the view model for a Project topic.
        /// </summary>
        public void GetLabels(ProjectTopicViewModel projectTopicViewModel)
        {
            projectTopicViewModel.ProjectIdLabel = GetLabel(ProjectIDReference);
            projectTopicViewModel.ProjectSponsorLabel = GetLabel(ProjectSponsorReference);
            projectTopicViewModel.ProjectManagerLabel = GetLabel(ProjectManagerReference);
            projectTopicViewModel.BudgetedSpendLabel = GetLabel(BudgetedSpendReference);
            projectTopicViewModel.StatusLabel = GetLabel(ProjectStatusReference);
            projectTopicViewModel.SuccessLabel = GetLabel(PerceivedSuccessReference);
            projectTopicViewModel.ProjectEndDateLabel = GetLabel(ProjectEndDateReference);
        }

        /// <summary>
        /// Populate the labels on the view model for a Skills topic.
        /// </summary>
        public void GetLabels(SkillsTopicViewModel skillsTopicViewModel)
        {
            skillsTopicViewModel.TrainingFrequencyLabel = GetLabel(TrainingFrequencyReference);
            skillsTopicViewModel.ContactLabel = GetLabel(LODContactReference);
            skillsTopicViewModel.TrainingStandardLabel = GetLabel(PerceivedStandardOfTrainingReference);
            skillsTopicViewModel.TrainingRequiredLabel = GetLabel(TrainingRequiredReference);
        }

        /// <summary>
        /// Populate the labels on the view model for a Strategic topic.
        /// </summary>
        public void GetLabels(StrategicTopicViewModel strategicTopicViewModel)
        {
            strategicTopicViewModel.NoteTitleLabel = GetLabel(NoteTitleReference);
            strategicTopicViewModel.ReviewFrequencyLabel = GetLabel(ReviewFrequencyReference);
            strategicTopicViewModel.QualityLabel = GetLabel(QualityReference);
        }

        /// <summary>
        /// Populate the labels on the view model for a Systems topic.
        /// </summary>
        public void GetLabels(SystemsTopicViewModel systemsTopicViewModel)
        {
            systemsTopicViewModel.SystemIdLabel = GetLabel(NameOfSystemReference);
            systemsTopicViewModel.FrequencyOfUseLabel = GetLabel(SystemFrequencyOfUseReference);
            systemsTopicViewModel.ExpertUserLabel = GetLabel(ExpertUserReference);
            systemsTopicViewModel.ImportanceRatingLabel = GetLabel(ImportanceRatingReference);
            systemsTopicViewModel.WebsiteAddressLabel = GetLabel(WebsiteAddressReference);
        }

        /// <summary>
        /// Populate the labels on the view model for a Tools topic.
        /// </summary>
        public void GetLabels(ToolsTopicViewModel toolsTopicViewModel)
        {
            toolsTopicViewModel.AssetIdLabel = GetLabel(AssetIDReference);
            toolsTopicViewModel.FrequencyOfUseLabel = GetLabel(EquipmentFrequencyOfUseReference);
            toolsTopicViewModel.ConditionLabel = GetLabel(ConditionReference);
            toolsTopicViewModel.ContactLabel = GetLabel(ExperiencedContactReference);
        }

        #endregion

        #region SetAnswer

        /// <summary>
        /// Set the answer for a question on a given topic.
        /// </summary>
        public void SetAnswer(Topic topic, Guid questionReference, string text = null, Guid? value = null)
        {
            if (topic.Fields == null)
            {
                topic.Fields = new List<TopicField>();
            }

            // Find the field for the question with the given reference
            var field = topic.Fields.FirstOrDefault(f => f.Question != null && f.Question.Reference == questionReference);

            if (field != null)
            {
                var answer = field.Answers.FirstOrDefault();

                if (answer == null)
                {
                    answer = new TopicAnswer();
                }

                answer.Text = text;
                answer.Value = value;
            }
            else
            {
                topic.Fields.Add(new TopicField
                {
                    TopicQuestionId = TopicQuestionService.GetByReference(questionReference).ID,
                    Answers = new List<TopicAnswer>
                    {
                        new TopicAnswer
                        {
                            Text = text,
                            Value = value
                        }
                    }
                });
            }
        }

        #endregion

        #region SetAnswers

        /// <summary>
        /// Populate the fields in the given topic based on the topic group type and the answers
        /// in the topic view model.
        /// </summary>
        public void SetAnswers(TopicGroup topicGroup, Topic topic, TopicViewModel topicViewModel)
        {
            switch (topicGroup.TopicGroupType)
            {
                case TopicGroup.TopicType.Compliance:
                    SetAnswers(topic, topicViewModel as ComplianceTopicViewModel);
                    break;
                case TopicGroup.TopicType.Experience:
                case TopicGroup.TopicType.EnjoyedLeast:
                case TopicGroup.TopicType.Achievements:
                case TopicGroup.TopicType.Changes:
                case TopicGroup.TopicType.OtherExperiences:
                case TopicGroup.TopicType.BestThings:
                case TopicGroup.TopicType.TeamAchievements:
                    SetAnswers(topic, topicViewModel as ExperienceTopicViewModel);
                    break;
                case TopicGroup.TopicType.Location:
                    SetAnswers(topic, topicViewModel as LocationTopicViewModel);
                    break;
                case TopicGroup.TopicType.Process:
                    SetAnswers(topic, topicViewModel as ProcessTopicViewModel);
                    break;
                case TopicGroup.TopicType.Skills:
                    SetAnswers(topic, topicViewModel as SkillsTopicViewModel);
                    break;
                case TopicGroup.TopicType.Strategic:
                    SetAnswers(topic, topicViewModel as StrategicTopicViewModel);
                    break;
                case TopicGroup.TopicType.Systems:
                    SetAnswers(topic, topicViewModel as SystemsTopicViewModel);
                    break;
                case TopicGroup.TopicType.Tools:
                    SetAnswers(topic, topicViewModel as ToolsTopicViewModel);
                    break;
            }
        }

        #region SetAnswers (Compliance)

        /// <summary>
        /// Populate the fields for a Compliance topic.
        /// </summary>
        public void SetAnswers(Topic topic, ComplianceTopicViewModel complianceTopicViewModel)
        {
            SetAnswer(topic, DocumentNameReference, text: complianceTopicViewModel.DocumentName);
            SetAnswer(topic, RegulationAndComplianceNotesReference, text: complianceTopicViewModel.RegulationAndComplianceNotes);
            SetAnswer(topic, DocumentLocationReference, text: complianceTopicViewModel.DocumentLocation, value: complianceTopicViewModel.DocumentLocationId);
            SetAnswer(topic, PrimaryContactReference, text: complianceTopicViewModel.PrimaryContact, value: complianceTopicViewModel.PrimaryContactId);
            SetAnswer(topic, ReferenceWebsiteReference, text: complianceTopicViewModel.ReferenceWebsite, value: complianceTopicViewModel.ReferenceWebsiteId);
            SetAnswer(topic, TrainingRequiredReference, text: complianceTopicViewModel.TrainingRequired.ToString());
        }

        #endregion

        #region SetAnswers (Experience)

        /// <summary>
        /// Populate the fields for a Experience topic.
        /// </summary>
        public void SetAnswers(Topic topic, ExperienceTopicViewModel experienceTopicViewModel)
        {
            SetAnswer(topic, WhenReference, text: experienceTopicViewModel.When.ToString());
            SetAnswer(topic, KeyContactReference, text: experienceTopicViewModel.KeyContact, value: experienceTopicViewModel.KeyContactId);
        }

        #endregion

        #region SetAnswers (Location)

        /// <summary>
        /// Populate the fields for a Location topic.
        /// </summary>
        public void SetAnswers(Topic topic, LocationTopicViewModel locationTopicViewModel)
        {
            SetAnswer(topic, LocationReference, text: locationTopicViewModel.Location, value: locationTopicViewModel.LocationId);
            SetAnswer(topic, AddressReference, text: locationTopicViewModel.Address);
            SetAnswer(topic, MapLinkReference, text: locationTopicViewModel.MapLink);
        }

        #endregion

        #region SetAnswers (Process)

        /// <summary>
        /// Populate the fields for a Process topic.
        /// </summary>
        public void SetAnswers(Topic topic, ProcessTopicViewModel processTopicViewModel)
        {
            SetAnswer(topic, WorkaroundNotesReference, text: processTopicViewModel.WorkaroundNotes);
            SetAnswer(topic, ProcessInteractionReference, text: processTopicViewModel.ProcessInteraction.ToString());
            SetAnswer(topic, LODContactReference, text: processTopicViewModel.Contact, value: processTopicViewModel.ContactId);
            SetAnswer(topic, FitForPurposeReference, text: processTopicViewModel.FitForPurpose.ToString());
            SetAnswer(topic, FitForPurposeReasonNIReference, text: processTopicViewModel.FitForPurposeReason);
            SetAnswer(topic, ProjectReferenceReference, text: processTopicViewModel.ProjectReference, value: processTopicViewModel.ProjectReferenceId);
        }

        #endregion

        #region SetAnswers (Project)

        /// <summary>
        /// Populate the fields for a Project topic.
        /// </summary>
        public void SetAnswers(Topic topic, ProjectTopicViewModel projectTopicViewModel)
        {
            SetAnswer(topic, ProjectIDReference, value: projectTopicViewModel.ProjectId);
            SetAnswer(topic, ProjectSponsorReference, text: projectTopicViewModel.ProjectSponsor);
            SetAnswer(topic, ProjectManagerReference, text: projectTopicViewModel.ProjectManager);
            SetAnswer(topic, BudgetedSpendReference, text: projectTopicViewModel.BudgetedSpend.ToString());
            SetAnswer(topic, ProjectStatusReference, text: projectTopicViewModel.Status.ToString());
            SetAnswer(topic, PerceivedSuccessReference, text: projectTopicViewModel.Success.ToString());
            SetAnswer(topic, ProjectEndDateReference, text: projectTopicViewModel.ProjectEndDate.ToString());
        }

        #endregion

        #region SetAnswers (Skills)

        /// <summary>
        /// Populate the fields for a Skills topic.
        /// </summary>
        public void SetAnswers(Topic topic, SkillsTopicViewModel skillsTopicViewModel)
        {
            SetAnswer(topic, TrainingFrequencyReference, text: skillsTopicViewModel.TrainingFrequency.ToString());
            SetAnswer(topic, LODContactReference, text: skillsTopicViewModel.Contact, value: skillsTopicViewModel.ContactId);
            SetAnswer(topic, PerceivedStandardOfTrainingReference, text: skillsTopicViewModel.TrainingStandard.ToString());
            SetAnswer(topic, TrainingRequiredReference, text: skillsTopicViewModel.TrainingRequired.ToString());
        }

        #endregion

        #region SetAnswers (Strategic)

        /// <summary>
        /// Populate the fields for a Strategic topic.
        /// </summary>
        public void SetAnswers(Topic topic, StrategicTopicViewModel strategicTopicViewModel)
        {
            SetAnswer(topic, NoteTitleReference, text: strategicTopicViewModel.NoteTitle);
            SetAnswer(topic, ReviewFrequencyReference, text: strategicTopicViewModel.ReviewFrequency.ToString());
            SetAnswer(topic, QualityReference, text: strategicTopicViewModel.Quality.ToString());
        }

        #endregion

        #region SetAnswers (Systems)

        /// <summary>
        /// Set the answers for a Systems topic.
        /// </summary>
        public void SetAnswers(Topic topic, SystemsTopicViewModel systemsTopicViewModel)
        {
            SetAnswer(topic, NameOfSystemReference, value: systemsTopicViewModel.SystemId);
            SetAnswer(topic, SystemFrequencyOfUseReference, text: systemsTopicViewModel.FrequencyOfUse.ToString());
            SetAnswer(topic, ExpertUserReference, text: systemsTopicViewModel.ExpertUser, value: systemsTopicViewModel.ExpertUserId);
            SetAnswer(topic, ImportanceRatingReference, text: systemsTopicViewModel.ImportanceRating.ToString());
            SetAnswer(topic, WebsiteAddressReference, text: systemsTopicViewModel.WebsiteAddress, value: systemsTopicViewModel.WebsiteAddressId);
        }

        #endregion

        #region SetAnswers (Tools)

        /// <summary>
        /// Populate the fields for a Tools topic.
        /// </summary>
        public void SetAnswers(Topic topic, ToolsTopicViewModel toolsTopicViewModel)
        {
            SetAnswer(topic, AssetIDReference, value: toolsTopicViewModel.AssetId);
            SetAnswer(topic, EquipmentFrequencyOfUseReference, text: toolsTopicViewModel.FrequencyOfUse.ToString());
            SetAnswer(topic, ConditionReference, text: toolsTopicViewModel.Condition.ToString());
            SetAnswer(topic, ExperiencedContactReference, text: toolsTopicViewModel.Contact, value: toolsTopicViewModel.ContactId);
        }

        #endregion

        #endregion

        #region GetAnswer

        /// <summary>
        /// Find the text answer to a specific question in a collection of fields.
        /// </summary>
        /// <typeparam name="T">The answer type.</typeparam>
        /// <param name="questionReference">The question reference ID.</param>
        /// <param name="fields">A collection of topic fields.</param>
        /// <returns>The strongly typed answer.</returns>
        public object GetTextAnswer<T>(Guid questionReference, ICollection<TopicField> fields)
        {
            // Find the field for the question with the given reference
            var field = fields.FirstOrDefault(f => f.Question.Reference == questionReference);

            if (field != null)
            {
                var answer = field.Answers.FirstOrDefault();

                if (answer != null)
                {
                    // Attempt to convert the string to the given type
                    if (!string.IsNullOrEmpty(answer.Text))
                    {
                        try
                        {
                            return (T)Convert.ChangeType(answer.Text, typeof(T));
                        }
                        catch
                        {
                            try
                            {
                                return (T)Enum.Parse(typeof(T), answer.Text);
                            }
                            catch
                            {
                                try
                                {
                                    return DateTime.Parse(answer.Text);
                                }
                                catch
                                {
                                }
                            }
                        }
                    }
                }
            }

            return default(T);
        }

        /// <summary>
        /// Find the value answer to a specific question in a collection of fields.
        /// </summary>
        public Guid? GetValueAnswer(Guid questionReference, ICollection<TopicField> fields)
        {
            // Find the field for the question with the given reference
            var field = fields.FirstOrDefault(f => f.Question.Reference == questionReference);

            if (field != null)
            {
                var answer = field.Answers.FirstOrDefault();

                if (answer != null)
                {
                    return answer.Value;
                }
            }

            return null;
        }

        #endregion

        #region GetAnswers

        /// <summary>
        /// Populate a view model with answers based on the topic type.
        /// </summary>
        public void GetAnswers(TopicGroup topicGroup, Topic topic, TopicViewModel topicViewModel)
        {
            switch (topicGroup.TopicGroupType)
            {
                case TopicGroup.TopicType.Compliance:
                    (topicViewModel as ComplianceTopicViewModel).DocumentName = (string)GetTextAnswer<string>(DocumentNameReference, topic.Fields);
                    (topicViewModel as ComplianceTopicViewModel).RegulationAndComplianceNotes = (string)GetTextAnswer<string>(RegulationAndComplianceNotesReference, topic.Fields);
                    (topicViewModel as ComplianceTopicViewModel).DocumentLocation = (string)GetTextAnswer<string>(DocumentLocationReference, topic.Fields);
                    (topicViewModel as ComplianceTopicViewModel).DocumentLocationId = GetValueAnswer(DocumentLocationReference, topic.Fields);
                    (topicViewModel as ComplianceTopicViewModel).PrimaryContact = (string)GetTextAnswer<string>(PrimaryContactReference, topic.Fields);
                    (topicViewModel as ComplianceTopicViewModel).PrimaryContactId = GetValueAnswer(PrimaryContactReference, topic.Fields);
                    (topicViewModel as ComplianceTopicViewModel).ReferenceWebsite = (string)GetTextAnswer<string>(ReferenceWebsiteReference, topic.Fields);
                    (topicViewModel as ComplianceTopicViewModel).ReferenceWebsiteId = GetValueAnswer(ReferenceWebsiteReference, topic.Fields);
                    (topicViewModel as ComplianceTopicViewModel).TrainingRequired = (bool)GetTextAnswer<bool>(TrainingRequiredReference, topic.Fields);
                    break;
                case TopicGroup.TopicType.Experience:
                case TopicGroup.TopicType.EnjoyedLeast:
                case TopicGroup.TopicType.Achievements:
                case TopicGroup.TopicType.Changes:
                case TopicGroup.TopicType.OtherExperiences:
                case TopicGroup.TopicType.BestThings:
                case TopicGroup.TopicType.TeamAchievements:
                    (topicViewModel as ExperienceTopicViewModel).When = (DateTime?)GetTextAnswer<DateTime?>(WhenReference, topic.Fields);
                    (topicViewModel as ExperienceTopicViewModel).KeyContact = (string)GetTextAnswer<string>(KeyContactReference, topic.Fields);
                    (topicViewModel as ExperienceTopicViewModel).KeyContactId = GetValueAnswer(KeyContactReference, topic.Fields);
                    break;
                case TopicGroup.TopicType.Location:
                    (topicViewModel as LocationTopicViewModel).Location = (string)GetTextAnswer<string>(LocationReference, topic.Fields);
                    (topicViewModel as LocationTopicViewModel).LocationId = GetValueAnswer(LocationReference, topic.Fields);
                    (topicViewModel as LocationTopicViewModel).Address = (string)GetTextAnswer<string>(AddressReference, topic.Fields);
                    (topicViewModel as LocationTopicViewModel).MapLink = (string)GetTextAnswer<string>(MapLinkReference, topic.Fields);
                    break;
                case TopicGroup.TopicType.Process:
                    (topicViewModel as ProcessTopicViewModel).WorkaroundNotes = (string)GetTextAnswer<string>(WorkaroundNotesReference, topic.Fields);
                    (topicViewModel as ProcessTopicViewModel).ProcessInteraction = (Topic.ProcessInteraction)GetTextAnswer<Topic.ProcessInteraction>(ProcessInteractionReference, topic.Fields);
                    (topicViewModel as ProcessTopicViewModel).Contact = (string)GetTextAnswer<string>(LODContactReference, topic.Fields);
                    (topicViewModel as ProcessTopicViewModel).ContactId = GetValueAnswer(LODContactReference, topic.Fields);
                    (topicViewModel as ProcessTopicViewModel).FitForPurpose = (Topic.ProcessFitForPurpose)GetTextAnswer<Topic.ProcessFitForPurpose>(FitForPurposeReference, topic.Fields);
                    (topicViewModel as ProcessTopicViewModel).FitForPurposeReason = (string)GetTextAnswer<string>(FitForPurposeReasonNIReference, topic.Fields);
                    (topicViewModel as ProcessTopicViewModel).ProjectReference = (string)GetTextAnswer<string>(ProjectReferenceReference, topic.Fields);
                    (topicViewModel as ProcessTopicViewModel).ProjectReferenceId = GetValueAnswer(ProjectReferenceReference, topic.Fields);
                    (topicViewModel as ProcessTopicViewModel).LinkedProject = !string.IsNullOrEmpty((topicViewModel as ProcessTopicViewModel).ProjectReference);
                    break;
                case TopicGroup.TopicType.Skills:
                    (topicViewModel as SkillsTopicViewModel).TrainingFrequency = (Topic.TrainingFrequency)GetTextAnswer<Topic.TrainingFrequency>(TrainingFrequencyReference, topic.Fields);
                    (topicViewModel as SkillsTopicViewModel).Contact = (string)GetTextAnswer<string>(LODContactReference, topic.Fields);
                    (topicViewModel as SkillsTopicViewModel).ContactId = GetValueAnswer(LODContactReference, topic.Fields);
                    (topicViewModel as SkillsTopicViewModel).TrainingStandard = (Topic.TrainingStandard)GetTextAnswer<Topic.TrainingStandard>(PerceivedStandardOfTrainingReference, topic.Fields);
                    (topicViewModel as SkillsTopicViewModel).TrainingRequired = (bool)GetTextAnswer<bool>(TrainingRequiredReference, topic.Fields);
                    break;
                case TopicGroup.TopicType.Strategic:
                    (topicViewModel as StrategicTopicViewModel).NoteTitle = (string)GetTextAnswer<string>(NoteTitleReference, topic.Fields);
                    (topicViewModel as StrategicTopicViewModel).ReviewFrequency = (Topic.StategyReviewFrequency)GetTextAnswer<Topic.StategyReviewFrequency>(ReviewFrequencyReference, topic.Fields);
                    (topicViewModel as StrategicTopicViewModel).Quality = (Topic.StategyQuality)GetTextAnswer<Topic.StategyQuality>(QualityReference, topic.Fields);
                    break;
                case TopicGroup.TopicType.Systems:
                    (topicViewModel as SystemsTopicViewModel).SystemId = GetValueAnswer(NameOfSystemReference, topic.Fields);
                    (topicViewModel as SystemsTopicViewModel).FrequencyOfUse = (Topic.SystemFrequencyOfUse)GetTextAnswer<Topic.SystemFrequencyOfUse>(SystemFrequencyOfUseReference, topic.Fields);
                    (topicViewModel as SystemsTopicViewModel).ExpertUser = (string)GetTextAnswer<string>(ExpertUserReference, topic.Fields);
                    (topicViewModel as SystemsTopicViewModel).ExpertUserId = GetValueAnswer(ExpertUserReference, topic.Fields);
                    (topicViewModel as SystemsTopicViewModel).ImportanceRating = (Topic.SystemImportanceRating)GetTextAnswer<Topic.SystemImportanceRating>(ImportanceRatingReference, topic.Fields);
                    (topicViewModel as SystemsTopicViewModel).WebsiteAddress = (string)GetTextAnswer<string>(WebsiteAddressReference, topic.Fields);
                    (topicViewModel as SystemsTopicViewModel).WebsiteAddressId = GetValueAnswer(WebsiteAddressReference, topic.Fields);
                    break;
                case TopicGroup.TopicType.Tools:
                    (topicViewModel as ToolsTopicViewModel).AssetId = GetValueAnswer(AssetIDReference, topic.Fields);
                    (topicViewModel as ToolsTopicViewModel).FrequencyOfUse = (Topic.EquipmentFrequencyOfUse)GetTextAnswer<Topic.EquipmentFrequencyOfUse>(EquipmentFrequencyOfUseReference, topic.Fields);
                    (topicViewModel as ToolsTopicViewModel).Condition = (Topic.EquipmentCondition)GetTextAnswer<Topic.EquipmentCondition>(ConditionReference, topic.Fields);
                    (topicViewModel as ToolsTopicViewModel).Contact = (string)GetTextAnswer<string>(ExperiencedContactReference, topic.Fields);
                    (topicViewModel as ToolsTopicViewModel).ContactId = GetValueAnswer(ExperiencedContactReference, topic.Fields);
                    break;
            }
        }

        #endregion

        #region PopulateTopicViewModel

        public void PopulateTopicViewModel(TopicGroup topicGroup, TopicViewModel topicViewModel)
        {
            topicViewModel.ModuleName = topicGroup.ParentModule.Type.GetDescription();
            topicViewModel.TopicGroupName = topicGroup.Name;

            var package = PackageService.GetByTopicGroupId(topicGroup.ID);


            if (package != null)
            {
                topicViewModel.IsLeavingPackageState = HandoverService.IsLeaving(package.ID);
                //topicViewModel.IsLeavingPackageState = package.PackageStatus == Package.Status.Triggered;
            }
            else
            {
                topicViewModel.IsLeavingPackageState = false;
            }


            // Topic group specific dropdowns
            switch (topicGroup.TopicGroupType)
            {
                // Compliance
                case TopicGroup.TopicType.Compliance:
                    // Document Locations
                    (topicViewModel as ComplianceTopicViewModel).DocumentLocations = WorkplaceService.GetAll().Select(w =>
                        new SelectListItem
                        {
                            Text = w.Name,
                            Value = w.ID.ToString()
                        });

                    // Primary Contacts
                    (topicViewModel as ComplianceTopicViewModel).PrimaryContacts = GetMasterListSelect(MasterList.ListType.OrganisationContacts);

                    // Reference Websites
                    (topicViewModel as ComplianceTopicViewModel).ReferenceWebsites = GetMasterListSelect(MasterList.ListType.Websites);

                    break;

                // Experiences
                case TopicGroup.TopicType.Experience:
                case TopicGroup.TopicType.EnjoyedLeast:
                case TopicGroup.TopicType.Achievements:
                case TopicGroup.TopicType.Changes:
                case TopicGroup.TopicType.OtherExperiences:
                case TopicGroup.TopicType.BestThings:
                case TopicGroup.TopicType.TeamAchievements:
                    // Key Contacts
                    (topicViewModel as ExperienceTopicViewModel).KeyContacts = GetMasterListSelect(MasterList.ListType.OrganisationContacts);

                    break;

                // Location
                case TopicGroup.TopicType.Location:
                    (topicViewModel as LocationTopicViewModel).Locations = WorkplaceService.GetAll().Select(w =>
                        new SelectListItem
                        {
                            Text = w.Name,
                            Value = w.ID.ToString()
                        });

                    break;

                // Process
                case TopicGroup.TopicType.Process:
                    // L&OD Contacts
                    (topicViewModel as ProcessTopicViewModel).Contacts = GetMasterListSelect(MasterList.ListType.OrganisationContacts);

                    // L&OD Contacts
                    (topicViewModel as ProcessTopicViewModel).ProjectReferences = GetMasterListSelect(MasterList.ListType.Projects);

                    break;

                // Skills
                case TopicGroup.TopicType.Skills:
                    // L&OD Contacts
                    (topicViewModel as SkillsTopicViewModel).Contacts = GetMasterListSelect(MasterList.ListType.OrganisationContacts);

                    break;


                // Systems
                case TopicGroup.TopicType.Systems:
                    // System names
                    (topicViewModel as SystemsTopicViewModel).Systems = GetMasterListSelect(MasterList.ListType.Systems);

                    // Expert users
                    (topicViewModel as SystemsTopicViewModel).ExpertUsers = GetMasterListSelect(MasterList.ListType.ExpertUsers);

                    // Website addresses
                    (topicViewModel as SystemsTopicViewModel).WebsiteAddresses = GetMasterListSelect(MasterList.ListType.Websites);

                    break;

                // Tools
                case TopicGroup.TopicType.Tools:
                    // Asset IDs
                    (topicViewModel as ToolsTopicViewModel).Assets = GetMasterListSelect(MasterList.ListType.Assets);

                    (topicViewModel as ToolsTopicViewModel).Contacts = GetMasterListSelect(MasterList.ListType.OrganisationContacts);

                    break;
            }
        }

        #endregion

        #region UpdateTopicFields

        /// <summary>
        /// Update the properties of the given topic with the corresponding
        /// properties on the view model.
        /// </summary>
        public void UpdateTopicFields(Topic topic, TopicViewModel topicViewModel)
        {

            if (topic.SourceId != null)
            {
                TopicService.UpdateAllTemplateTopicsName(topic, topicViewModel.Name);
            }


            topic.SearchTags = topicViewModel.SearchTags;
            topic.Name = topicViewModel.Name;
            topic.Notes = topicViewModel.Notes;
            topic.EmployeeCopy = topicViewModel.EmployeeCopy;
            topic.ReadyForReview = topicViewModel.ReadyForReview;
            topic.ProcessGroupId = topicViewModel.ProcessGroupId;



            /*if (MomentumPlus.Activate.Web.Helpers.AccountConfig.Settings.ProLinkEnabled && topicViewModel is ProcessTopicViewModel)
            {
                //topic.IsProLinkCore = (topicViewModel as ProcessTopicViewModel).IsProLinkCore;
            }*/
        }

        #endregion

        #endregion
    }
}