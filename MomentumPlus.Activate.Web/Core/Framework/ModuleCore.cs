﻿using System;
using MomentumPlus.Activate.Services;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Areas.Framework.Models;
using System.Linq;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Core.Framework
{
    public class ModuleCore
    {
        #region Constants

        /// <summary>
        /// Number of topics groups/topics/tasks to display on a single page.
        /// </summary>
        private const int PageSize = 10;

        #endregion

        #region Dependency Injection

        private IModuleService ModuleService;
        private ITaskService TaskService;
        private ITopicService TopicService;

        public ModuleCore(
            IModuleService moduleService,
            ITaskService taskService,
            ITopicService topicService
        )
        {
            ModuleService = moduleService;
            TaskService = taskService;
            TopicService = topicService;
        }

        #endregion

        #region Read

        /// <summary>
        /// Get the framework view model for the given parameters.
        /// </summary>
        public FrameworkViewModel GetFrameworkViewModel(
            Module.ModuleType moduleType,
            Guid? topicGroup,
            int topicPageNo,
            Guid? topic,
            int taskPageNo,
            bool retiredTopics,
            bool retiredTasks
        )
        {
            var viewModel = new FrameworkViewModel
            {
                PageSize = PageSize,
                TopicPageNo = topicPageNo,
                TaskPageNo = taskPageNo,
                RetiredTopics = retiredTopics,
                RetiredTasks = retiredTasks
            };

            // Get module containing the framework defaults
            var module = ModuleService.GetFrameworkModel(moduleType);
            viewModel.TopicGroups = (from t in module.TopicGroups orderby t.Name select t).ToList();

            if (topicGroup.HasValue)
            {
                // Get topics in the given topic group and to view model
                viewModel.ActiveTopicGroup = topicGroup.Value;
                viewModel.Topics = TopicService.GetByTopicGroupId(topicGroup.Value,
                    topicPageNo, PageSize, Topic.SortColumn.Name, true, retiredTopics);

                viewModel.TopicCount = TopicService.Count(topicGroup.Value, retiredTopics);

                if (topic.HasValue)
                {
                    var topicEntity = TopicService.GetByTopicId(topic.Value);

                    if (topicEntity != null)
                    {
                        // Get tasks in the given topic and add to view model
                        viewModel.ActiveTopic = topic.Value;
                        viewModel.Tasks = TaskService.GetAllByTopicId(topic.Value, taskPageNo,
                            PageSize, null, Task.SortColumn.Name, true, retiredTasks);

                        viewModel.TaskCount = TaskService.CountByTopicId(topic.Value, retiredTasks);
                    }
                }
            }

            return viewModel;
        }

        #endregion
    }
}