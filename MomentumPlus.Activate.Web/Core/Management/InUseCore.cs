﻿using System.Diagnostics;
using System.Linq;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models.Home;
using System.Collections.Generic;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Web.Core.Management
{
    public class InUseCore : BaseCore
    {
        private IPackageService packageService;

        public InUseCore(
            IMasterListService masterListService,
            IPackageService packageService
        )
            : base(masterListService)
        {
            this.packageService = packageService;
        }

        /// <summary>
        /// Active Packages not yet Triggered 
        /// Filter where packages have Triggers and Trigger approved is false
        /// </summary>
        /// <param name="pageNo"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public InUseOnboarderViewModel GetOnboarders(int pageNo, int pageSize, InUseOnboarderItemViewModel.SortColumn sortColumn, bool sortAscending)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            var packages = packageService.GetByPackageStatus(Package.Status.InProgress, false).ToList();

            IEnumerable<InUseOnboarderItemViewModel> onboarderItems = (from p in packages
                                                                      where
                                                                      p.UserOwner != null
                                                                      select new InUseOnboarderItemViewModel
                                                                      {
                                                                          ApprovedHRManager = p.Handover != null && p.Handover.LeavingPeriod.Locked.HasValue && p.Handover.OnboardingPeriod.Locked.HasValue,
                                                                          ApprovedLineManager = p.Handover != null && p.Handover.LeavingPeriod.Locked.HasValue && p.Handover.OnboardingPeriod.Locked.HasValue,
                                                                          NameOnboarder = p.Handover != null && p.Handover.OnboardingPeriod.Package != null && p.Handover.OnboardingPeriod.Package.UserOwner != null ? string.Format("{0} {1}", p.Handover.OnboardingPeriod.Package.UserOwner.FirstName, p.Handover.OnboardingPeriod.Package.UserOwner.LastName) : "Unassiged",
                                                                          NameExit = string.Format("{0} {1}", p.UserOwner.FirstName, p.UserOwner.LastName),
                                                                          LastUpdated = p.UserLastViewed,
                                                                          CurrentUserIsHr = p.UserHRManager.ID == AppSession.Current.CurrentUser.ID,
                                                                          CurrentUserIsLm = p.UserLineManager.ID == AppSession.Current.CurrentUser.ID,
                                                                          PackageId = p.ID,
                                                                          HasTrigger = p.Handover != null
                                                                      }).ToList();

            int totalCount =
                onboarderItems.Count();

            //SortAndPage<InUseOnboarderItemViewModel>(pageNo, pageSize, sortColumn.ToString(), sortAscending, ref onboarderItems);
            SortAndPage(ref onboarderItems, oi => oi, sortColumn.ToString(), sortAscending, pageNo, pageSize);

            InUseOnboarderViewModel inUseOnboarderViewModel
                = new InUseOnboarderViewModel
                {
                    PageNo = pageNo,
                    PageSize = pageSize,
                    TotalCount = totalCount,
                    Items = onboarderItems
                };

            stopwatch.Stop();
            WriteCorePerfomanceLog("GetOnboarders",
                stopwatch.Elapsed.Milliseconds);

            return inUseOnboarderViewModel;
        }
    }
}