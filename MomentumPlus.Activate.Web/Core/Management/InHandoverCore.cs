﻿using System;
using System.Diagnostics;
using System.Linq;
using MomentumPlus.Activate.Services;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Helpers;
using MomentumPlus.Activate.Web.Models.Home;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.Moderation;

namespace MomentumPlus.Activate.Web.Core.Management
{
    public class InHandoverCore : BaseCore
    {
        private IPackageService packageService;
        private IMasterListService masterListService;

        public InHandoverCore(
            IPackageService packageService,
            IMasterListService masterListService
        )
            : base(masterListService)
        {
            this.packageService = packageService;
            this.masterListService = masterListService;

        }

        public InHandoverExitingViewModel GetExiting(int pageNo, int pageSize, InHandoverExitingItemViewModel.SortColumn sortColumn, bool sortAscending)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            var packages =
              packageService.GetAllEagerLoad();

            IQueryable<InHandoverExitingItemViewModel> triggerItems = (from p in packages
                                                                       where
                                                                       p.Handover != null &&
                                                                       p.Handover.LeavingPeriod != null &&
                                                                       p.Handover.LeavingPeriod.Locked.HasValue
                                                                       select new InHandoverExitingItemViewModel
                                                                       {
                                                                           NameOnboarder = p.Handover.OnboardingPeriod.Package != null ? GetName(p.Handover.OnboardingPeriod.Package.UserOwner) : "-",
                                                                           NameExit = GetName(p.UserOwner),
                                                                           NumberOfModules = p.CountModules(),
                                                                           NumberOfModulesComplete = p.CountModulesComplete(),
                                                                           StatusCompletePercent = p.CountModulesCompletePercent(),
                                                                           Subjectivity = p.CountSubjectivity(masterListService),
                                                                           AverageContent = p.CountWords(),
                                                                           DaysToGo = GetDaysToGo(p.Handover.LeavingPeriod.LastDay),
                                                                           Finish = p.Handover.LeavingPeriod.LastDay,
                                                                           CurrentUserIsHr = p.UserHRManager.ID == AppSession.Current.CurrentUser.ID,
                                                                           CurrentUserIsLm = p.UserLineManager.ID == AppSession.Current.CurrentUser.ID,
                                                                           PackageId = p.ID
                                                                       }).AsQueryable();


            int totalCount =
              triggerItems.Count();

            SortAndPage<InHandoverExitingItemViewModel>(pageNo, pageSize, sortColumn.ToString(), sortAscending, ref triggerItems);

            InHandoverExitingViewModel viewModel =
                new InHandoverExitingViewModel
                {
                    Items = triggerItems,
                    PageNo = pageNo,
                    PageSize = pageSize,
                    TotalCount = totalCount,
                    SortAscending = sortAscending
                };

            stopwatch.Stop();
            WriteCorePerfomanceLog("GetExiting",
                stopwatch.Elapsed.Milliseconds);

            return viewModel;
        }



        public InHandoverOnboardingViewModel GetOnboarding(int pageNo, int pageSize, InHandoverOnboardingItemViewModel.SortColumn sortColumn, bool sortAscending)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            var packages =
             packageService.GetAllWithPermissions().ToList();

            IQueryable<InHandoverOnboardingItemViewModel> onboardingItems = (from p in packages
                                                                             where
                                                                             p.Handover != null &&
                                                                             p.Handover.LeavingPeriod.LastDay.HasValue &&
                                                                             p.Handover.OnboardingPeriod.Package != null &&
                                                                             p.Handover.OnboardingPeriod.Package.UserOwner != null &&
                                                                             p.Handover.LeavingPeriod.Locked.HasValue &&
                                                                             p.Handover.OnboardingPeriod.Locked.HasValue
                                                                             select new InHandoverOnboardingItemViewModel
                                                                             {
                                                                                 NameNewStart = GetName(p.Handover.OnboardingPeriod.Package.UserOwner),
                                                                                 Started = p.Handover.OnboardingPeriod.Package.StartDate.HasValue ? p.Handover.OnboardingPeriod.Package.StartDate.Value : DateTime.Now,
                                                                                 CurrentUserIsHr = p.UserHRManager.ID == AppSession.Current.CurrentUser.ID,
                                                                                 CurrentUserIsLm = p.UserLineManager.ID == AppSession.Current.CurrentUser.ID,
                                                                                 PackageId = p.Handover.OnboardingPeriod.Package.ID
                                                                             }).AsQueryable();

            int totalCount =
              onboardingItems.Count();

            SortAndPage<InHandoverOnboardingItemViewModel>(pageNo, pageSize, sortColumn.ToString(), sortAscending, ref onboardingItems);

            InHandoverOnboardingViewModel onboardingView =
                new InHandoverOnboardingViewModel
                {
                    Items = onboardingItems,
                    PageNo = pageNo,
                    PageSize = pageSize,
                    TotalCount = totalCount,
                    SortAscending = sortAscending
                };

            stopwatch.Stop();
            WriteCorePerfomanceLog("GetOnboarding",
                stopwatch.Elapsed.Milliseconds);

            return onboardingView;
        }


        #region helpers

        private int GetModulesComplete(Package package)
        {
            int countModules = (from m in package.Modules
                                where !m.IsFrameworkDefault
                                from tg in m.TopicGroups
                                from to in tg.Topics
                                where to.IsApproved()
                                select m).Count();

            if (package.Contacts.Count > 0)
            {
                var contactsAllApproved =
                    package.Contacts.All(Moderated.Approved().Compile());

                if (contactsAllApproved)
                    countModules++;
            }

            return countModules;
        }











        #endregion



    }
}
