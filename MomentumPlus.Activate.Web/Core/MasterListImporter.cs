﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using CsvHelper;
using CsvHelper.Configuration;
using MomentumPlus.Activate.Services;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.DataSource;

namespace MomentumPlus.Activate.Web.Core
{
    public class MasterListImporter : BaseImporter, IImporter, IDisposable
    {
        private List<MasterList> masterLists;

        #region CSV object

        public class MasterListRow : IEquatable<MasterListRow>
        {
            [Required]
            public string MasterListType { get; set; }
            [Required]
            [StringLength(50, MinimumLength = 3)]
            public string Name { get; set; }

            public override int GetHashCode()
            {
                return
                    string.Format("{0}{1}",
                    Name.ToLower(),
                    MasterListType.ToString().ToLowerRemoveSpaces()).GetHashCode();
            }

            public bool Equals(MasterListRow other)
            {
                bool equal = this.Name.ToLower() == other.Name.ToLower() &&
                             this.MasterListType.ToString().ToLowerRemoveSpaces() == other.MasterListType.ToString().ToLowerRemoveSpaces();

                return equal;
            }
        }

        #endregion

        #region CSV object mapping

        public sealed class MasterListRowMap : CsvClassMap<MasterListRow>
        {
            public MasterListRowMap()
            {
                Map(m => m.MasterListType).Name("MasterListType");
                Map(m => m.Name).Index(1).Name("Name");
            }
        }

        #endregion

        #region Public methods

        public List<string> Import(Stream csv)
        {
            IEnumerable<MasterListRow> rows =
                new List<MasterListRow>();

            using (StreamReader streamReader = new StreamReader(csv))
            {
                CsvReader csvReader =
                    new CsvReader(streamReader);

                csvReader.Configuration.RegisterClassMap<MasterListRowMap>();
                csvReader.Configuration.Delimiter = delimiter;
                csvReader.Configuration.IgnoreReadingExceptions = true;
                csvReader.Configuration.TrimFields = true;

                csvReader.Configuration.ReadingExceptionCallback = (ex, row) =>
                {
                    ProcessReadError(ex, row, ref errors);
                };

                rows =
                    csvReader.GetRecords<MasterListRow>().ToList();
            }

            if (errors.Count == 0)
                errors.AddRange(ValidateDomainIntegrity((List<MasterListRow>)rows));

            if (errors.Count == 0)
                errors.AddRange(ValidateDuplicates((List<MasterListRow>)rows));

            if (errors.Count == 0)
            {
                masterLists =
                     unitOfWork.MasterListRepository.Find(m => m.Deleted == null).ToList();

                int rowIndex = 0;
                foreach (MasterListRow row in rows)
                {
                    MasterList.ListType? listTypeParsed =
                        GetMasterListType(row.MasterListType);

                    if (listTypeParsed.HasValue)
                    {
                        bool existsInDb =
                            masterLists.Any(m => m.Deleted == null && String.Equals(m.Name.Trim(), row.Name.Trim(), StringComparison.CurrentCultureIgnoreCase)
                                && m.Type.ToString() == row.MasterListType.ToString());

                        if (!existsInDb)
                        {
                            MasterList masterList = new MasterList
                            {
                                Name = row.Name,
                                Type = listTypeParsed.Value
                            }; // add additional fields here

                            unitOfWork.MasterListRepository.Add(masterList);
                        }
                        else
                        {
                            errors.Add(
                                    string.Format("Row {0}: MasterList with Name '{1}' and MasterListType '{2}' already exists in the database",
                                    rowIndex + 1, row.Name, row.MasterListType));
                        }
                    }
                    else
                    {
                        errors.Add(
                            string.Format("Row {0}: MasterList Type '{1}' is not valid.",
                                        rowIndex + 1, row.MasterListType));
                    }

                    rowIndex++;
                }
            }

            if (errors.Count == 0)
            {
                unitOfWork.Save();

                MasterListService masterListService =
                    new MasterListService(new MomentumContext());

                masterListService.RefreshCache();
            }

            return errors;
        }

        private MasterList.ListType? GetMasterListType(string value)
        {
            MasterList.ListType? ret = null;

            foreach (MasterList.ListType listType in
                Enum.GetValues(typeof(MasterList.ListType)))
            {
                if (listType.ToString().ToLowerRemoveSpaces() == value.ToLowerRemoveSpaces())
                {
                    ret = listType;
                    break;
                }
            }

            return ret;
        }

        #endregion

        public void Dispose()
        {
            masterLists = null;
            unitOfWork = null;

            GC.Collect();
        }
    }
}