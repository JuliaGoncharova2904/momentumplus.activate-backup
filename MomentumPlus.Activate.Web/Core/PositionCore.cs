﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.People;

namespace MomentumPlus.Activate.Web.Core
{
    public class PositionCore
    {
        #region Constants

        /// <summary>
        /// Number of positions to display in the Position list.
        /// </summary>
        private const int PageSize = 8;

        #endregion

        #region Dependency Injection

        private IPackageService PackageService;
        private IPositionService PositionService;
        private ITemplateService TemplateService;
        private IWorkplaceService WorkplaceService;

        /// <summary>
        /// Construct a new <c>PositionCore</c> instance
        /// with the given service implementations.
        /// </summary>
        /// <param name="positionService"></param>
        public PositionCore(
            IPackageService packageService,
            IPositionService positionService,
            ITemplateService templateService,
            IWorkplaceService workplaceService
        )
        {
            PackageService = packageService;
            PositionService = positionService;
            TemplateService = templateService;
            WorkplaceService = workplaceService;
        }

        #endregion

        #region Create

        /// <summary>
        /// Create a new position entity from the given view model.
        /// </summary>
        public Position CreatePosition(PositionViewModel viewModel)
        {
            var newPosition = PositionService.NewPosition();
            Mapper.Map<PositionViewModel, Position>(viewModel, newPosition);
            PositionService.Add(newPosition);
            return newPosition;
        }

        #endregion

        #region Read

        /// <summary>
        /// Get the positions view model for the given parameters.
        /// </summary>
        public PositionsViewModel GetPositionsViewModel(
            Guid? id,
            string Query,
            int PageNo,
            Position.SortColumn SortColumn,
            bool SortAscending,
            Package.Status? packageStatus,
            bool retired,
            User currentUser
        )
        {
            var positions = PositionService.Search(Query, PageNo, PageSize, SortColumn, SortAscending);
            var positionsCount = PositionService.SearchCount(Query);

            var positionViewModel = Mapper.Map<IEnumerable<Position>, IEnumerable<PositionViewModel>>(positions);
            var positionsViewModel = new PositionsViewModel
            {
                Positions = positionViewModel,
                SortAscending = SortAscending,
                SelectedPackageStatus = packageStatus,
                Retired = retired,
                PageSize = PageSize,
                PageNo = PageNo,
                TotalCount = positionsCount
            };

            if (id != null)
            {
                var packages = PackageService.GetByPositionId(id.Value, packageStatus, retired);
                var packageViewModel = Mapper.Map<IEnumerable<Package>, IEnumerable<PackageViewModel>>(packages);
                positionsViewModel.Packages = packageViewModel;
                positionsViewModel.SelectedPositionId = id.Value;
            }
            else
            {
                IEnumerable<Package> packages = new List<Package>();
                var packageViewModel = Mapper.Map<IEnumerable<Package>, IEnumerable<PackageViewModel>>(packages);
                positionsViewModel.Packages = packageViewModel;
            }

            if (currentUser != null &&
                (currentUser.Role.RoleParsed == Role.RoleEnum.HRManager ||
                 currentUser.Role.RoleParsed == Role.RoleEnum.Administrator ||
                 currentUser.Role.RoleParsed == Role.RoleEnum.iHandoverAgent ||
                 currentUser.Role.RoleParsed == Role.RoleEnum.iHandoverAdmin))
            {
                positionsViewModel.IsAuthorisedToAdd = true;
            }

            return positionsViewModel;
        }

        #endregion

        #region Update

        /// <summary>
        /// Update an existing position given its view model.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public Position UpdatePosition(PositionViewModel viewModel)
        {
            var position = PositionService.GetByPositionId(viewModel.ID);
            Mapper.Map<PositionViewModel, Position>(viewModel, position);
            PositionService.Update(position);
            return position;
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Populate a position view model with the values required
        /// to render the position form.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public PositionViewModel PopulatePositionViewModel(PositionViewModel viewModel)
        {
            IEnumerable<Template> templates = TemplateService.GetAll();
            IEnumerable<Workplace> workplaces = WorkplaceService.GetAll();

            // Generate values for Default Template dropdown
            viewModel.Templates = new List<SelectListItem>
            (
                templates.Select(t => new SelectListItem
                {
                    Text = t.Name,
                    Value = t.ID.ToString()
                })
            ).OrderBy(t => t.Text);

            // Generate values for Default Workplace dropdown
            viewModel.Workplaces = new List<SelectListItem>
            (
                workplaces.Select(w => new SelectListItem
                {
                    Text = w.Name,
                    Value = w.ID.ToString()
                })
            ).OrderBy(w => w.Text);

            return viewModel;
        }

        #endregion
    }
}