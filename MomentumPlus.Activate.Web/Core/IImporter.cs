﻿using System.Collections.Generic;
using System.IO;

namespace MomentumPlus.Activate.Web.Core
{
    public interface IImporter
    {
        List<string> Import(Stream csv);
    }
}