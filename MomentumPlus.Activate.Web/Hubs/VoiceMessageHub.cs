﻿using System;
using System.Linq;
using Microsoft.AspNet.SignalR;
using MomentumPlus.Activate.Web.Models;
using MomentumPlus.Core.DataSource;

namespace MomentumPlus.Activate.Web.Hubs
{
    public class VoiceMessageHub : Hub
    {
        #region IOC
        #endregion

        #region Methods
        public void Ping(string ping, string pong)
        {
            Clients.Caller.AddNewMessageToPage("an alert has been requested");
        }

        public void Send(Guid ItemID, ItemType ItemType)
        {
            using (var context = new MomentumContext())
            {
                var Message = context.VoiceMessages
                   .Where(vm => vm.ID == ItemID)
                   .FirstOrDefault();
                if (Message != null && Message.RequiresNotification == true)
                {
                    Clients.Caller.AddNewMessageToPage(ItemID, "You have a new recording", ItemID);
                    Message.RequiresNotification = false;
                    context.SaveChanges();
                }
            }
            //var message = VoiceMessageService.GetByID(ItemID);

            //if (message != null && message.RequiresNotification == true)
            //{
            //    Clients.Caller.AddNewMessageToPage(ItemID, "You have a new recording", ItemID);
            //    message.RequiresNotification = false;
            //    VoiceMessageService.Update(message);
            //}
        }
        #endregion
    }
}

//using System;
//using System.Linq;
//using Microsoft.AspNet.SignalR;
//using MomentumPlus.Activate.Data;
//using MomentumPlus.Activate.Web.Models;

//namespace MomentumPlus.Activate.Web.Hubs
//{
//    public class VoiceMessageHub : Hub
//    {
//        #region Methods
//        public void Ping(string ping, string pong)
//        {
//            Clients.Caller.AddNewMessageToPage("an alert has been requested");
//        }

//        public void Send(Guid ItemID, ItemType ItemType)
//        {
//            using (var context = new MomentumContext())
//            {
//                var Message = context.VoiceMessages
//                   .Where(vm => vm.ID == ItemID)
//                   .FirstOrDefault();
//                if (Message!= null && Message.RequiresNotification == true)
//                {
//                    Clients.Caller.AddNewMessageToPage(ItemID, "You have a new recording", ItemID);
//                    Message.RequiresNotification = false;
//                    context.SaveChanges();
//                }
//            }
//        }
//        #endregion
//    }
//}