﻿using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Activate.Services.Interfaces;
using System.Data.Entity;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Services
{
    /// <summary>
    /// Old Keyword Service - obsolete
    /// </summary>
    [Obsolete]
    public class KeywordService : ServiceBase, IKeywordService
    {
        public KeywordService(MomentumContext context) : base(context) { }

        #region IKeywordService Members

        public IEnumerable<Keyword> GetAll()
        {
            var keywords = MContext.Keyword.Where(k => !k.Deleted.HasValue);

            return keywords;
        }

        public Keyword GetByKeywordId(Guid keywordId)
        {
            var keyword = 
                MContext.Keyword.Find(keywordId);

            return keyword;
        }

        public Keyword Add(Keyword keyword)
        {
            keyword =
                MContext.Keyword.Add(keyword);

            MContext.SaveChanges();

            return keyword;
        }

        public Keyword Update(Keyword keyword)
        {
            MContext.Entry(keyword).State = EntityState.Modified;

            MContext.SaveChanges();

            return keyword;
        }

        public IEnumerable<Keyword> Search(string query, int pageNo, int pageSize, Keyword.SortColumn sortColumn, bool sortAscending)
        {
            var keywords = 
                MContext.Keyword.Where(k => !k.Deleted.HasValue && k.Name.Contains(query));

            SortAndPage<Keyword>(pageNo, pageSize, sortColumn.ToString(), sortAscending, ref keywords);

            return keywords.ToList();
        }

        public bool Exists(string name)
        {
            return MContext.Keyword.Any(k => !k.Deleted.HasValue && k.Name == name);;
        }

        public void Delete(Guid keywordId)
        {
            var keyword = MContext.Keyword.Find(keywordId);
            MContext.Keyword.Remove(keyword);

            MContext.SaveChanges();
        }

        #endregion
    }
}
