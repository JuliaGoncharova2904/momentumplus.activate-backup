﻿using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Services
{
    /// <summary>
    /// Topic Question Service
    /// </summary>
    public class TopicQuestionService : ServiceBase, ITopicQuestionService
    {
        public TopicQuestionService(MomentumContext context) : base(context) { }

        #region ITopicQuestionService Members

        /// <summary>
        /// Gets all Topic Questions
        /// </summary>
        /// <returns>Collection of TopicQuestion</returns>
        public IEnumerable<TopicQuestion> GetAll()
        {
            var questions = 
                MContext.TopicQuestion.Where(q => !q.Deleted.HasValue);

            return questions.ToList();
        }

        /// <summary>
        /// Gets all Topic Questions by Reference
        /// </summary>
        /// <param name="reference"></param>
        /// <returns>TopicQuestion</returns>
        public TopicQuestion GetByReference(Guid reference)
        {
            var question =
                 MContext.TopicQuestion.FirstOrDefault(q => !q.Deleted.HasValue && q.Reference == reference);

            return question;
        }

        #endregion
    }
}
