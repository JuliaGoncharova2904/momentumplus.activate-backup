﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Services
{
    /// <summary>
    /// Topic Group Service
    /// </summary>
    public class TopicGroupService : ServiceBase, ITopicGroupService
    {
        public TopicGroupService(MomentumContext context) : base(context) { }

        #region ITopicGroupService Members

        /// <summary>
        /// Get Topic Group by child Topic Id
        /// </summary>
        /// <param name="topicId">Topic Id</param>
        /// <returns>TopicGroup</returns>
        public TopicGroup GetByTopicId(Guid topicId)
        {
            var topicGroups = MContext.TopicGroup.Where(tg => !tg.Deleted.HasValue);
            //var topicGroups = MContext.TopicGroup;

            var topicGroup = (from t in topicGroups
                              from tt in t.Topics
                              where tt.ID == topicId
                              select t).FirstOrDefault();

            return topicGroup;
        }

        /// <summary>
        /// Get Topic Grouo by Topic Group ID
        /// </summary>
        /// <param name="topicGroupId">Topic Group id</param>
        /// <returns>TopicGroup</returns>
        public TopicGroup GetByTopicGroupId(Guid topicGroupId)
        {
            TopicGroup topicGroup =
                    MContext.TopicGroup.Find(topicGroupId);

            return topicGroup;
        }

        /// <summary>
        /// Update Topic Group
        /// </summary>
        /// <param name="topicGroup">TopicGroup to update</param>
        /// <returns>TopicGroup</returns>
        public TopicGroup Update(TopicGroup topicGroup)
        {
            MContext.Entry(topicGroup).State = EntityState.Modified;

            UpdateTitle(topicGroup);

            MContext.SaveChanges();

            return topicGroup;
        }


        public TopicGroup GetByReferenceId(int referenceId)
        {
            TopicGroup topicGroup =
                    MContext.TopicGroup.FirstOrDefault(g => g.ReferenceId == referenceId);

            return topicGroup;
        }



        /// <summary>
        /// Update Topic Group title
        /// </summary>
        /// <param name="topicGroupChanged">TopicGroup to update</param>
        private void UpdateTitle(TopicGroup topicGroupChanged)
        {
            var modules = MContext.Module.Where(m => !m.Deleted.HasValue);
            var topicGroups = (from m in modules
                               from tg in m.TopicGroups
                               where tg.TopicGroupType == topicGroupChanged.TopicGroupType
                                        && tg.ID != topicGroupChanged.ID
                               select tg);

            foreach (TopicGroup topicGroup in topicGroups)
            {
                topicGroup.Title
                    = topicGroupChanged.Title;
            }
        }

        /// <summary>
        /// Clone Topic Group
        /// </summary>
        /// <param name="topicGroupId">Topic Group id to clone</param>
        /// <param name="deepClone">Clone child entities</param>
        /// <returns>TopicGroup</returns>
        public TopicGroup Clone(Guid topicGroupId, bool deepClone = true)
        {
            var topicGroupClone = MContext.TopicGroup.Where(tg => !tg.Deleted.HasValue && tg.ID == topicGroupId).
                    Include("Topics.Tasks.Attachments.Data").
                    Include("Topics.Tasks.VoiceMessages.Data").
                    Include("Topics.Tasks.DocumentLibraries.Attachment").
                    Include("Topics.Attachments.Data").
                    Include("Topics.VoiceMessages.Data").
                    Include("Topics.DocumentLibraries.Attachment").
                FirstOrDefault();

            if (!deepClone)
                topicGroupClone.Topics.Clear();

            return Clone(topicGroupClone);
        }

        /// <summary>
        /// Clone Topic Group
        /// </summary>
        /// <param name="topicGroupSource">TopicGroup to clone</param>
        /// <returns>TopicGroup</returns>
        private TopicGroup Clone(TopicGroup topicGroupSource)
        {
            return topicGroupSource.DeepClone();
        }

        /// <summary>
        /// Get Topic Groups by Package Id
        /// </summary>
        /// <param name="guid">Package Id</param>
        /// <returns>Collection of TopicGroup</returns>
        public IEnumerable<TopicGroup> GetByPackageId(Guid guid)
        {
            var package = MContext.Package.Find(guid);
            var topicGroups = from m in package.Modules
                              from t in m.TopicGroups
                              select t;
            return topicGroups;
        }

        #endregion

        /// <summary>
        /// Checks to see if any Topics exist with the name 
        /// provided
        /// </summary>
        /// <param name="topicGroupId">Topic Group Id to check</param>
        /// <param name="name">Topic name to match</param>
        /// <returns></returns>
        public bool HasTopicWithName(Guid topicGroupId, string name)
        {
            return MContext.Topic.Any(t => t.ParentTopicGroupId == topicGroupId
                && !t.Deleted.HasValue
                && t.Name == name);
        }
    }
}
