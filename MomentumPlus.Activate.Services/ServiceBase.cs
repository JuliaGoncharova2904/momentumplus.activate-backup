﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.ComponentModel;
using System.Data.Entity;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using NLog;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models.People;
using MomentumPlus.Core.Comparers;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Services
{
    /// <summary>
    /// Base class for all services
    /// </summary>
    public abstract class ServiceBase : IDisposable
    {
        protected Logger _logger = LogManager.GetLogger("PerformanceMonitoring");

        protected MomentumContext MContext;
        protected User User = null;
        protected RestrictedAccess UserEntities;

        /// <summary>
        /// Base constructor
        /// </summary>
        /// <param name="context"></param>
        public ServiceBase(MomentumContext context)
        {
            MContext = context;
        }

        /// <summary>
        /// Base Service constructor with 
        /// user context
        /// </summary>
        /// <param name="context"></param>
        /// <param name="user">User context</param>
        public ServiceBase(MomentumContext context, User user)
        {
            MContext = context;

            this.User = user;

            this.UserEntities =
                new RestrictedAccess(context, user, false);
        }

        /// <summary>
        /// Sort and Page query helper
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="pageNo">Page number</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="sortColumn">Sort column</param>
        /// <param name="sortAscending">Sort ascending/descending</param>
        /// <param name="objects">Collection to page</param>
        protected static void SortAndPage<T>(int pageNo, int pageSize, string sortColumn, bool sortAscending, ref IQueryable<T> objects)
        {
            if (objects != null && objects.Count() > 1)
            {
                var param = Expression.Parameter(typeof(T), "p");
                var prop = Expression.Property(param, sortColumn.ToString());
                var exp = Expression.Lambda(prop, param);
                string method = sortAscending ? "OrderBy" : "OrderByDescending";
                Type[] types = new Type[] { objects.ElementType, exp.Body.Type };
                var mce = Expression.Call(typeof(Queryable), method, types, objects.Expression, exp);
                objects = objects.Provider.CreateQuery<T>(mce);

                objects = objects.Skip(pageSize * (pageNo - 1)).Take(pageSize);
            }
        }

        /// <summary>
        /// Sort and Page query helper 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="objects">Objects to sort</param>
        /// <param name="sortObject">Field to sort by</param>
        /// <param name="sortColumnName">Column to sort by</param>
        /// <param name="sortAscending">Sort acending</param>
        /// <param name="pageNo">Page number</param>
        /// <param name="pageSize">Page size</param>
        protected static void SortAndPage<T, TKey>(ref IQueryable<T> objects, Func<T, TKey> sortObject, string sortColumnName, bool sortAscending, int pageNo, int pageSize)
        {
            SortAndPage(ref objects, sortObject, new PropertyComparer<TKey>(sortColumnName), sortAscending, pageNo, pageSize);
        }

        /// <summary>
        /// Sort and Page query helper
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="objects">Objects to sort</param>
        /// <param name="sortObject">Field to sort by</param>
        /// <param name="comparer">Comparer for ordering</param>
        /// <param name="sortAscending">Sort ascending</param>
        /// <param name="pageNo">Page number</param>
        /// <param name="pageSize">Page size</param>
        protected static void SortAndPage<T, TKey>(ref IQueryable<T> objects, Func<T, TKey> sortObject, IComparer<TKey> comparer, bool sortAscending, int pageNo, int pageSize)
        {
            objects = (sortAscending
                        ? objects.OrderBy(sortObject, comparer)
                        : objects.OrderByDescending(sortObject, comparer))
                      .AsQueryable();

            objects = objects.Skip(pageSize * (pageNo - 1)).Take(pageSize);
        }

        /// <summary>
        /// Sorts Topic Groups in a hard coded order 
        /// defined by the client
        /// </summary>
        /// <param name="topicGroups">Topic groups to sort</param>
        protected void SortTopicGroups(ref IQueryable<TopicGroup> topicGroups)
        {
            topicGroups = SortTopicGroups(topicGroups);
        }

        /// <summary>
        /// Sorts Topic Grouos in a hard coded order
        /// defined by the client
        /// </summary>
        /// <param name="topicGroups"></param>
        /// <returns></returns>
        protected IQueryable<TopicGroup> SortTopicGroups(IQueryable<TopicGroup> topicGroups)
        {
            if (topicGroups != null && topicGroups.Count() > 0)
            {
                var topicGroupsOrdered = (from tg in topicGroups
                                          orderby
                                          tg.TopicGroupType == TopicGroup.TopicType.Experience,
                                          tg.TopicGroupType == TopicGroup.TopicType.EnjoyedLeast,
                                          tg.TopicGroupType == TopicGroup.TopicType.Achievements,
                                          tg.TopicGroupType == TopicGroup.TopicType.Changes,
                                          tg.TopicGroupType == TopicGroup.TopicType.OtherExperiences,
                                          tg.TopicGroupType == TopicGroup.TopicType.BestThings,
                                          tg.TopicGroupType == TopicGroup.TopicType.TeamAchievements,
                                          tg.TopicGroupType == TopicGroup.TopicType.Location,
                                          tg.TopicGroupType == TopicGroup.TopicType.Compliance,
                                          tg.TopicGroupType == TopicGroup.TopicType.Strategic,
                                          tg.TopicGroupType == TopicGroup.TopicType.Tools,
                                          tg.TopicGroupType == TopicGroup.TopicType.Skills,
                                          tg.TopicGroupType == TopicGroup.TopicType.Systems,
                                          tg.TopicGroupType == TopicGroup.TopicType.Process
                                          select tg);

                return topicGroupsOrdered;
            }
            else
            {
                return topicGroups;
            }
        }

        /// <summary>
        /// Gets the description of a Enum
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        protected string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

            if (attributes != null &&
                attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }

        /// <summary>
        /// Resets base Entity fields
        /// </summary>
        /// <param name="baseModel"></param>
        protected void ResetBaseFields(BaseModel baseModel)
        {
            baseModel.SourceId = baseModel.ID;
            baseModel.ID = Guid.NewGuid();
            baseModel.Created = DateTime.Now;
            baseModel.Modified = null;
        }

        /// <summary>
        /// Deep clones an entity
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        protected T DeepClone<T>(T obj)
        {
            using (var ms = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(ms, obj);
                ms.Position = 0;

                return (T)formatter.Deserialize(ms);
            }
        }

        /// <summary>
        /// Serialises object to XML
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        protected string SerializeToXML<T>(T obj)
        {
            string xml = null;

            using (MemoryStream memStm = new MemoryStream())
            {
                var serializer = new DataContractSerializer(typeof(T));
                serializer.WriteObject(memStm, obj);

                memStm.Seek(0, SeekOrigin.Begin);

                using (var streamReader = new StreamReader(memStm))
                {
                    xml = streamReader.ReadToEnd();
                }
            }

            return xml;
        }

        /// <summary>
        /// Deserialises object from Xml
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xml"></param>
        /// <returns></returns>
        protected T Deserialize<T>(string xml)
        {
            using (Stream stream = new MemoryStream())
            {
                byte[] data = System.Text.Encoding.UTF8.GetBytes(xml);
                stream.Write(data, 0, data.Length);
                stream.Position = 0;
                DataContractSerializer deserializer = new DataContractSerializer(typeof(T));
                return (T)deserializer.ReadObject(stream);
            }
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    MContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Gets Line Manager by Package
        /// </summary>
        /// <param name="package"></param>
        /// <returns></returns>
        public Guid GetLineManagerContactId(Package package)
        {
            var contact = package.Contacts.FirstOrDefault(c => String.Equals(c.Email, package.UserLineManager.Email, StringComparison.InvariantCultureIgnoreCase));

            var ContactService = new ContactService(MContext);

            if (contact == null)
            {
              
                contact = ContactService.GetAllFramework().FirstOrDefault(c => c.ID == package.UserLineManagerId);
                if (contact != null)
                {
                    MContext.Entry(contact).State = EntityState.Detached;

                    contact = contact.DeepClone();
                    contact.PackageId = package.ID;
                    ContactService.Add(contact);
                }
                else
                {
                    var firstOrDefault = ContactService.GetByPackageId(package.ID).FirstOrDefault();
                    if (firstOrDefault != null)
                    {
                        return firstOrDefault.ID;
                    }
                }
            }

            return contact.ID;
        }
    }
}
