﻿using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Activate.Services.Interfaces;
using SystemMP = MomentumPlus.Core.Models.System;
using System.Data.Entity;
using MomentumPlus.Core.DataSource;

namespace MomentumPlus.Activate.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class SystemService : ServiceBase, ISystemService
    {
        public SystemService(MomentumContext context) : base(context) { }

        #region ISystemService Members

        public IEnumerable<SystemMP> GetAll()
        {
            var systems = MContext.SystemMP.Where(s => !s.Deleted.HasValue);

            return systems;
        }

        public SystemMP GetBySystemId(Guid systemId)
        {
            var system =
                 MContext.SystemMP.Find(systemId);

            return system;
        }

        public SystemMP Add(SystemMP system)
        {
            system = MContext.SystemMP.Add(system);

            MContext.SaveChanges();

            return system;
        }

        public SystemMP Update(SystemMP system)
        {
            MContext.Entry(system).State = EntityState.Modified;

            MContext.SaveChanges();

            return system;
        }

        public IEnumerable<SystemMP> Search(string query, int pageNo, int pageSize, SystemMP.SortColumn sortColumn, bool sortAscending)
        {
            var systems =
                MContext.SystemMP.Where(s => !s.Deleted.HasValue && s.Name.Contains(query));

            SortAndPage<SystemMP>(pageNo, pageSize, sortColumn.ToString(), sortAscending, ref systems);

            return systems.ToList();
        }

        public bool Exists(string name)
        {
            bool exists = false;

            var system = 
                MContext.SystemMP.FirstOrDefault(s => !s.Deleted.HasValue && s.Name == name);

            if (system != null)
                exists = true;

            return exists;
        }

        public void Delete(Guid systemId)
        {
            var system = MContext.SystemMP.Find(systemId);

            MContext.SystemMP.Remove(system);

            MContext.SaveChanges();
        }

        #endregion
    }
}
