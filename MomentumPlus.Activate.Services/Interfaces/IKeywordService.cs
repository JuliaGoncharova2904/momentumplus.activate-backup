﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Activate.Services.Interfaces
{
    [Obsolete]
    public interface IKeywordService
    {
        IEnumerable<Keyword> GetAll();

        Keyword GetByKeywordId(Guid systemId);

        IEnumerable<Keyword> Search(string query, int pageNo, int pageSize, Keyword.SortColumn sortColumn, bool sortAscending);

        Keyword Add(Keyword keyword);

        Keyword Update(Keyword keyword);

        bool Exists(string name);

        void Delete(Guid keywordId);
    }
}
