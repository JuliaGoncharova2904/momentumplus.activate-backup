﻿using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.People;
using MomentumPlus.Core.Models.Requests;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Activate.Services.Interfaces
{
    public interface IPeopleService
    {
        T UpdatePerson<T>(T person) where T : Person;

        Contact GetStaff(Guid id);

        IEnumerable<Contact> GetStaffsByPackageId(Guid packageId);

        IEnumerable<Contact> GetStaffsByPackageId(Guid packageId, int pageNo, int pageSize, Person.SortColumn? sortColumn, bool sortAscending = true, bool deleted = false);

        int CountStaffsByPackageId(Guid packageId, bool deleted);

        IEnumerable<Contact> SearchStaffs(Guid packageId, string term, int pageNo, int pageSize, Person.SortColumn? sortColumn, bool sortAscending = true, bool deleted = false);

        StaffTransferRequest GetStaffTransferRequestById(Guid id);

        void TransferStaff(StaffTransferRequest request, Guid? DestincationContactId = null);

        int CountStaffsApproved(Guid id);

        DateTime? GetDateLastEntry(Guid id);
    }
}
