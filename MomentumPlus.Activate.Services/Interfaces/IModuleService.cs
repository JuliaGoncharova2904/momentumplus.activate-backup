﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Activate.Services.Interfaces
{
    public interface IModuleService
    {
        IEnumerable<Module> GetByPackageId(Guid packageId, int pageNo, int pageSize, Module.SortColumn? sortColumn, bool sortAscending);

        Module GetByModuleId(Guid id);

        Module GetByModuleType(Module.ModuleType moduleType);

        Module GetByModuleType(Module.ModuleType moduleType, Guid packageId);

        List<TopicGroup> GetTemplateGroupsByModyleType(Module.ModuleType moduleType, Guid packageId);

        Module Update(Module module);

        int GetTopicTaskCount(Guid moduleId);

        DateTime? GetDateLastEntry(Guid moduleId);

        int GetTopicCount(Guid moduleId);

        int GetTaskCount(Guid moduleId);

        Module Clone(Guid moduleId);

        Module GetFrameworkModel(Module.ModuleType type);

        string ExportToXml(Guid moduleId);

        void ImportFromXml(string xml);
    }
}
