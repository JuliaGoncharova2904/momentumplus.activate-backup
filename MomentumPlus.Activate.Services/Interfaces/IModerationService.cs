﻿using MomentumPlus.Core.Models.Moderation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MomentumPlus.Activate.Services.Interfaces
{
    public interface IModerationService
    {

        Moderated AddModerated(Moderated moderated);

        Moderated UpdateModerated(Moderated moderated);

        void RemoveModerated(Moderated moderated);

        Moderated GetById(Guid id);

        T GetById<T>(Guid id) where T : Moderated;

        IEnumerable<T> GetAllNotApproved<T>(Guid packageId) where T : Moderated;

        IEnumerable<T> GetAllNotModerated<T>(Guid? userId = null, Guid? packageId = null) where T : Moderated;

        IEnumerable<Moderated> GetAllNotModerated(Guid? userId = null);

        void AddModeration(Moderated entity, Moderation moderation);

        Moderation UpdateModeration(Moderation moderation);

        void RemoveModeration(Moderation moderation);

        IQueryable<Moderated> GetAll(Guid packageId, bool deleted = false); 

        int CountAllModerated(Guid packageId);

        int CountApproved(Guid packageId);

        int CountRejected(Guid packageId);

    }
}
