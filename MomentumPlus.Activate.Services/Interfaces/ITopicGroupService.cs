﻿using System;
using System.Collections.Generic;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Services.Interfaces
{
    public interface ITopicGroupService
    {
        TopicGroup GetByTopicGroupId(Guid topicGroupId);

        TopicGroup GetByTopicId(Guid topicId);

        TopicGroup Update(TopicGroup topicGroup);

        TopicGroup Clone(Guid topicGroupId, bool deepClone);

        IEnumerable<TopicGroup> GetByPackageId(Guid guid);

        bool HasTopicWithName(Guid topicGroupId, string name);
    }
}
