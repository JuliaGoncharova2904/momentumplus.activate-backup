﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Activate.Services.Interfaces
{
    public interface ITemplateService
    {
        Template GetByTemplateId(Guid id);

        Guid Add(string name, int durationDays, string notes);
        
        IEnumerable<TemplateItem> GetById(Guid templateId, Module.ModuleType moduleType);
        
        void Save(Guid templateId, List<TemplateItem> templateItemsUI, Module.ModuleType moduleType, bool updatePackages = false);

        IEnumerable<TemplateItem> GetTemplateItems(Guid templateId, Module.ModuleType type);

        IEnumerable<TemplateItem> GetTemplateItemsFromAllModules(Guid templateId);

        IEnumerable<Template> GetAll(bool deleted = false);

        void Retire(Guid templateId);

        void Restore(Guid templateId);

        void TransferTemplateItemsCritical(Guid templateIdSource, IEnumerable<int> sourceReferenceIds, Guid templateIdDest);

        void TransferTemplateItemsExperiences(Guid templateIdSource, IEnumerable<int> sourceReferenceIds, Guid templateDest);

        void TransferTemplateItemsProjects(Guid templateIdSource, IEnumerable<int> sourceReferenceIds, Guid templateIdDest);

        void UpdateTemplate(Guid templateId, string name, int durationDays, string notes);

        Template UpdateTemplate(Template template);

        int Count(bool deleted = false);
    }
}
