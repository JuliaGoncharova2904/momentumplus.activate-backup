﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Activate.Services.Interfaces
{
    public interface IPositionService
    {
        Position NewPosition();

        Position Add(Position position);

        IEnumerable<Position> GetAll(Position.SortColumn? sortColumn = Position.SortColumn.Name, bool sortAscending = true);

        int Count();

        Position GetByPositionId(Guid positionId);

        Position Update(Position position);

        void Delete(Guid positionId);

        bool Exists(string positionName);

        IEnumerable<Position> Search(string query, int pageNo, int pageSize, Position.SortColumn sortColumn, bool sortAscending);

        int SearchCount(string query);

        IEnumerable<Attachment> SearchAttachments(string searchString, Guid packageId);

        IEnumerable<VoiceMessage> SearchVoiceMessages(string searchString, Guid packageId);
    }
}
