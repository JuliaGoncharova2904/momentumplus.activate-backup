﻿using MomentumPlus.Core.Models.Handover;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Activate.Services.Interfaces
{
    public interface IHandoverService
    {
        /// <summary>
        ///     Add a new Handover.
        /// </summary>
        /// <param name="handover"></param>
        /// <returns></returns>
        Handover Add(Handover handover);

        /// <summary>
        ///     Get a Handover by ID.
        /// </summary>
        /// <param name="handoverId"></param>
        /// <returns></returns>
        Handover GetById(Guid handoverId);

        /// <summary>
        ///     Update a Handover.
        /// </summary>
        /// <param name="handover"></param>
        /// <returns></returns>
        Handover Update(Handover handover);

        /// <summary>
        ///     Delete a Handover by ID.
        /// </summary>
        /// <param name="handoverId"></param>
        void Delete(Guid handoverId);

        /// <summary>
        ///     Lock the Leaving Period for a Handover.
        /// </summary>
        /// <param name="handover"></param>
        void LockLeavingPeriod(Handover handover);

        /// <summary>
        ///     Unlock the Leaving Period for a Handover.
        /// </summary>
        /// <param name="handover"></param>
        void UnlockLeavingPeriod(Handover handover);

        /// <summary>
        ///     Lock the Onboarding Period for a Handover.
        /// </summary>
        /// <param name="handover"></param>
        void LockOnboardingPeriod(Handover handover);

        /// <summary>
        ///     Unlock the Onboarding Period for a Handover.
        /// </summary>
        /// <param name="handover"></param>
        void UnlockOnboardingPeriod(Handover handover);

        /// <summary>
        ///     Check if a package is in the onboarding period.
        /// </summary>
        /// <param name="packageId"></param>
        /// <returns></returns>
        bool IsOnboarding(Guid packageId);

        bool HasOnboarded(Guid packageId);

        /// <summary>
        ///     Check if a package is in the leaving period.
        /// </summary>
        /// <param name="packageId"></param>
        /// <returns></returns>
        bool IsLeaving(Guid packageId);

        bool HasLeft(Guid packageId);

        /// <summary>
        ///     Get the Onboarding Period for a given Package.
        /// </summary>
        /// <param name="packageId"></param>
        /// <returns></returns>
        OnboardingPeriod GetOnboardingPeriodForPackage(Guid packageId);

        /// <summary>
        ///     Get the Leaving Period for a given Package.
        /// </summary>
        /// <param name="packageId"></param>
        /// <returns></returns>
        LeavingPeriod GetLeavingPeriodForPackage(Guid packageId);

        /// <summary>
        ///     Accept a Handover.
        /// </summary>
        /// <param name="id"></param>
        void Accept(Guid id);

        /// <summary>
        ///     Get all active Handovers.
        /// </summary>
        /// <returns></returns>
        IEnumerable<Handover> GetActiveHandovers();

        bool IsLeaverPeriodLocked(Guid packageId);

        bool IsOnboarderPeriodLocked(Guid packageId);

        /// <summary>
        /// Gets the onboarder review date for the Critial Module
        /// </summary>
        /// <param name="packageId"></param>
        /// <returns></returns>
        DateTime? GetOnboarderReviewDateCritical(Guid packageId);

        DateTime? GetOnboarderReviewDateCritical(Handover handover);

        /// <summary>
        /// Gets the onboarder review date for the Experiences Module
        /// </summary>
        /// <param name="packageId"></param>
        /// <returns></returns>
        DateTime? GetOnboarderReviewDateExperiences(Guid packageId);

        DateTime? GetOnboarderReviewDateExperiences(Handover handover);

        /// <summary>
        /// Gets the onboarder review date for the Relationships Module
        /// </summary>
        /// <param name="packageId"></param>
        /// <returns></returns>
        DateTime? GetOnboarderReviewDateRelationships(Guid packageId);

        DateTime? GetOnboarderReviewDateRelationships(Handover handover);

        /// <summary>
        /// Gets the onboarder review date for the Projects Module
        /// </summary>
        /// <param name="packageId"></param>
        /// <returns></returns>
        DateTime? GetOnboarderReviewDateProjects(Guid packageId);

        DateTime? GetOnboarderReviewDateProjects(Handover handover);

        /// <summary>
        /// Gets the onboarder review date for the People Module
        /// </summary>
        /// <param name="packageId"></param>
        /// <returns></returns>
        DateTime? GetOnboarderReviewDatePeople(Guid packageId);

        DateTime? GetOnboarderReviewDatePeople(Handover handover);

        DateTime? GetLeaverReviewDateCritical(Handover handover);

        DateTime? GetLeaverReviewDateExperiences(Guid packageId);

        DateTime? GetLeaverReviewDateExperiences(Handover handover);

        DateTime? GetLeaverReviewDateRelationships(Guid packageId);

        DateTime? GetLeaverReviewDateRelationships(Handover handover);

        DateTime? GetLeaverReviewDateProjects(Guid packageId);

        DateTime? GetLeaverReviewDateProjects(Handover handover);

        DateTime? GetLeaverReviewDatePeople(Guid packageId);

        DateTime? GetLeaverReviewDatePeople(Handover handover);

        DateTime? GetLeaverReviewDateCoreTasks(Guid packageId);

        DateTime? GetLeaverReviewDateCoreTasks(Handover handover);

        double GetReadyItemsPrecentage(Guid packageId);

        double GetApprovedItemsPercentage(Guid packageId);

        int? GetOnboardingDuration(Guid packageId);

        int GetTotalItemsCount(Guid packageId);

        int GetApprovedItemsCount(Guid packageId);

        int GetRejectedItemsCount(Guid packageId);
    }
}