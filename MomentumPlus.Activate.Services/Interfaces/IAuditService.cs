﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Activate.Services.Interfaces
{
    public interface IAuditService
    {
        void RecordActivity<T>(AuditLog.EventType eventType, Guid? entityId);

        AuditLog LastViewed<T>(Guid userId, Guid entityId);

        List<AuditLog> LastViewed<T>(Guid userId, List<Guid> entityIds);
    }
}
