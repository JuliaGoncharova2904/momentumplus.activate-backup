﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MomentumPlus.Activate.Services.Interfaces
{
    public interface IProjectService
    {
        IEnumerable<ProjectInstance> GetProjectInstances(Guid packageId);

        IEnumerable<ProjectInstance> GetProjectInstances(Guid packageId, int? statusReferenceId,
            ProjectInstance.SortColumn? sortColumn, bool sortAscending, bool deleted = false, bool? approved = true);

        IEnumerable<ProjectInstance> GetProjectInstances(Guid packageId, int? statusReferenceId, int pageNo, int pageSize, ProjectInstance.SortColumn? sortColumn, bool sortAscending, bool deleted = false, bool? approved = true);

        int CountProjectIntances(Guid packageId, int? statusReferenceId, bool deleted = false, bool? approved = true);

        ProjectInstance GetProjectInstanceById(Guid ID);

        ProjectInstance GetProjectInstanceByPackageAndProjectId(Guid packageId, Guid projectId);

        ProjectInstance Add(ProjectInstance project);

        ProjectInstance Update(ProjectInstance project);

        IEnumerable<Project> GetProjects(bool deleted = false);
        
        IEnumerable<Project> GetProjects(int? statusReferenceId, int pageNo, int pageSize, Project.SortColumn? sortColumn, bool sortAscending, bool deleted = false);

        int CountProjects(int? statusReferenceId = null, bool deleted = false);

        Project GetProjectById(Guid ID);

        Project Add(Project project);

        Project Update(Project project);

 //       Project Clone(Project project);

        IQueryable<ProjectStatus> GetProjectStatuses();

        IQueryable<ProjectRole> GetProjectRoles();

        void Retire(Guid projectId);

        void Retire(Project project);

        void Restore(Guid projectId);

        void Restore(Project project);

        void RetireInstance(Guid projectId);

        void RetireInstance(ProjectInstance project);

        void RestoreInstance(Guid projectId);

        void RestoreInstance(ProjectInstance project);

        void DeleteInstance(Guid projectId);

        void DeleteInstance(ProjectInstance project);

        void CreateProjectInstance(ProjectInstance projectInstance, string suggestedName, string suggestedId);

        IEnumerable<ProjectInstance> GetProjectInstancesByPeriodId(Guid periodId);

        int CountProjectsApproved(Guid id);

        DateTime? GetDateLastEntry(Guid id);

        ProjectInstance GetByMeetingId(Guid meetingId);

        ProjectInstance GetByTaskId(Guid taskId);
    }
}
