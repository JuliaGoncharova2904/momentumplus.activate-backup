﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Activate.Services.Interfaces
{
    public interface IFileService
    {
        File AddFile(File file);

        File Update(File file);

        File GetFile(Guid fileId);

        IEnumerable<File> GetAll();

        void DeleteFile(Guid fileId);
    }
}
