﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Activate.Services.Interfaces
{
    public interface IWorkplaceService
    {
        IEnumerable<Workplace> GetAll();

        Workplace GetByWorkplaceId(Guid workplaceId);

        Workplace Update(Workplace workplace);

        void Delete(Guid workplaceId);

        bool Exists(string positionName);

        Workplace Add(Workplace workplace);

        IEnumerable<Workplace> Search(string query, int pageNo, int pageSize, Workplace.SortColumn sortColumn, bool sortAscending);

        int SearchCount(string query);
    }
}
