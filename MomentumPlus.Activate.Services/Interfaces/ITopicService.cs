﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Activate.Services.Interfaces
{
    public interface ITopicService
    {
        IEnumerable<Topic> GetByTopicGroupId(Guid topicGroupId, int pageNo, int pageSize, Topic.SortColumn sortColumn, bool sortAscending, bool deleted = false);

        IEnumerable<Topic> GetByPackageId(Guid packageId, int pageNo, int pageSize, Topic.SortColumn sortColumn, bool sortAscending);

        IEnumerable<Topic> GetByPackageId(Guid packageId);

        IEnumerable<Topic> GetOnboardingByPackageId(Guid packageId);

        Topic GetByTopicId(Guid topicId);

        Topic GetByTopicSourceId(Guid topicSourceId, Guid packageId);

        Topic GetByTaskId(Guid topicId);

        int Count(Guid topicGroupId, bool deleted = false);

        int CountByPackageId(Guid packageId);

        Topic Add(Guid topicGroupId, Topic topic);

        Topic Update(Topic topic);

        void Delete(Guid topicId);

        void UpdateAllTemplateTopicsName(Topic topic , string newName);


        void Delete(Topic topic);

        void Retire(Guid topicId);

        void Restore(Guid topicId);

        Topic Clone(Guid topicId, bool deepClone = true);


        Topic GetByReferenceId(int topicSourceId);

        IEnumerable<Topic> Search(string searchString, Guid packageId);

        IEnumerable<Attachment> SearchAttachments(string searchString, Guid packageId);

        IEnumerable<VoiceMessage> SearchVoiceMessages(string searchString, Guid packageId);

        bool Exists(Guid topicGroupId, string topicName);

        bool Exists(Guid topicId, Guid topicGroupId, string topicName);


    }
}
