﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Activate.Services.Interfaces
{
    public interface IRoleService
    {
        IEnumerable<Role> GetAll();

        Role GetByRoleId(Guid roleId);

    }
}
