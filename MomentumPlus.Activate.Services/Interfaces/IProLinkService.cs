﻿using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.ProLink;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Activate.Services.Interfaces
{
    public interface IProLinkService
    {
        Function Add(Function function);

        ProcessGroup Add(ProcessGroup processGroup);

        bool Exists(Guid functionId, string processGroupName);

        Function GetFunction(Guid id);

        ProcessGroup GetProcessGroup(Guid id);

        IEnumerable<Function> GetAllFrameworkFunctions();

        IEnumerable<Topic> GetAllBusinessProcessTopics();

        Function Update(Function function);

        ProcessGroup Update(ProcessGroup processGroup);

        ProcessGroup UpdateById(Guid id, string name, string description);

        //     void Delete(ProcessGroup processGroup);
    }
}
