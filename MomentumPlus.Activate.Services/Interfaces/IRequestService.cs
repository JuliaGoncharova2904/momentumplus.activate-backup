﻿using MomentumPlus.Core.Models.Requests;
using System;
using System.Linq;

namespace MomentumPlus.Activate.Services.Interfaces
{
    public interface IRequestService
    {

        Request AddRequest(Request request);

        IQueryable<Request> GetRequests(Guid packageId);

        IQueryable<T> GetRequests<T>(Guid packageId, bool approved = false) where T : Request;

        Request GetRequest(Guid id);

        T GetRequest<T>(Guid id) where T : Request;

        Request GetRequest(int referenceId);

        T GetRequest<T>(int referenceId) where T : Request;

        Request UpdateRequest(Request request);

        void DeleteRequest(Guid id);

        void DeleteRequest(Request request);

    }
}
