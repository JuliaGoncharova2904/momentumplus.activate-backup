﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Activate.Services.Interfaces
{
    public interface IVoiceMessageService
    {
        IEnumerable<VoiceMessage> GetAll();
        VoiceMessage GetByID(Guid ID);
        VoiceMessage Add(VoiceMessage voiceMessage);
        VoiceMessage Update(VoiceMessage voiceMessage);
        VoiceMessage GetVoiceMessageByCoreServiceGuid(Guid CoreServiceGuid);
        void Delete(Guid voiceMessageID);   

    }
}
