﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Activate.Services.Interfaces
{
    public interface IExitSurveyService
    {
        IEnumerable<ExitSurvey> GetAllExitSurvey();

        ExitSurveyTemplate GetTemplateByExitSurvey(Guid exitSurveyId);

        ExitSurvey GetSurveyByID(Guid ID);

        bool IsExistSurvey(Guid ID);

        ExitSurvey GetExitSurveyByPackageId(Guid packageId); 

        IEnumerable<ExitSurvey> GetAll(bool deleted = false);

        ExitSurveyTemplate GetExitSurveyTemplateById(Guid exitSurveyTemplateId);

        ExitSurveyTemplate AddExitSurveyTemplate(ExitSurveyTemplate model);
        ExitSurveyQuestion GetQuestionById(Guid Id);

        IEnumerable<ExitSurveyQuestion> GetAllQuestion(bool deleted = false);

        ExitSurveyQuestion AddQuestion(ExitSurveyQuestion question);

        ExitSurveySection AddSection(ExitSurveySection section);

        ExitSurvey AddExitSurvey(ExitSurvey exitSurvey);

        ExitSurvey UpdateExitSurvey(ExitSurvey exitSurvey);

        ExitSurveySection UpdateExitSurveySection(ExitSurveySection section);

        ExitSurveyTemplate UpdateExitSurveyTemplate(ExitSurveyTemplate template);

        ExitSurveyQuestion UpdateQuestion(ExitSurveyQuestion question);

        void DelQuestion(ExitSurveyQuestion question);

        void DelExitSurvey(ExitSurvey exitSurvey);

        void DelExitSurveySection(ExitSurveySection section);

        ExitSurveySection GetExitSurveySectionById(Guid sectionId);

        List<ExitSurveyQuestion> GetQuestionBySection(Guid sectionId);
    }
}
