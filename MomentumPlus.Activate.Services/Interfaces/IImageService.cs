﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Activate.Services.Interfaces
{
    public interface IImageService
    {

        IEnumerable<Image> GetDefaultImages(bool retired = false);

        Image GetImage(Guid ID);

    }
}
