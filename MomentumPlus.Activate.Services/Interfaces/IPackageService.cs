﻿using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.People;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Activate.Services.Interfaces
{
    public interface IPackageService
    {
        Package GetPackageByExitSurveyId(Guid exitSurveyId);

        IEnumerable<Package> GetAll(bool deleted = false);

        IEnumerable<Package> GetAllWithPermissions(bool deleted = false);

        IEnumerable<Package> GetAllEagerLoad(bool deleted = false, User userOverride = null);

        IEnumerable<Package> GetAllPackagesByUser(User userOverride);

        IEnumerable<Package> GetPackagesByUserId(Guid id, bool deleted = false);

        IEnumerable<Package> GetPackagesByUserId(Guid id, Package.Status? packageStatus, bool deleted = false);

        IEnumerable<Package> GetByPositionId(Guid positionId, Package.Status? packageStatus, bool deleted = false);

        IEnumerable<Package> GetByWorkplaceId(Guid workplaceId, Package.Status? packageStatus, bool deleted = false);

        IEnumerable<Package> GetByPackageStatus(Package.Status status, bool deleted = false, User userOverride = null);

        bool IsOngoing(Guid packageId);

        IEnumerable<Package> GetAllFinalisePackage(bool deleted = false);

        Package GetByPackageId(Guid packageId);

        Package GetByTaskId(Guid taskId);

        Package GetByTopicId(Guid topicId);

        Package GetByTopicGroupId(Guid topicGroupId);

        Package GetByModuleId(Guid moduleId);

        Package GetByContactId(Guid contactId);

        Package GetByMeetingId(Guid meetingId);

        Package GetByProjectInstanceId(Guid projectInstanceId);

        Package GetByStaffId(Guid staffId);

        IEnumerable<Package> GetAncestorPackages(Package package);

        int Count(bool deleted = false);

        Package Add(Package package, Guid templateId, IEnumerable<Guid> projectIds = null, IEnumerable<Guid> contactIds = null);

        Package Update(Package package);

        void Retire(Guid packageId);

        void Restore(Guid packageId);

        string GetPackageName(Guid id);

        Package NewPackage();

        IEnumerable<ToDoItem> GetToDoItems(Guid packageId, int pageNo, int toDoPageSize, bool dismissed = false);

        IEnumerable<ToDoItem> GetAllToDoItems(Guid packageId);

        int GetToDoItemsCount(Guid packageId, bool dismissed = false);

        IEnumerable<Package> GetByUserOwnerId(Guid id);

        void CreateMidAndEndReviewMeetings(Package package, int fiscalYearStartMonth);

        void AddToDo(ToDoItem toDo);

        void UpdateToDo(ToDoItem toDo);

        bool HasExitSurveyTemplate(Guid packageId);
    }
}
