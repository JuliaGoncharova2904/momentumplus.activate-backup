﻿using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.People;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Activate.Services.Interfaces
{
    public interface IContactService
    {
        IEnumerable<Contact> GetAll(bool deleted = false);

        IEnumerable<Contact> GetAllFramework(bool deleted = false);

        IEnumerable<Contact> GetFrameworkContacts(int pageNo, int pageSize, Person.SortColumn? sortColumn, bool sortAscending, bool deleted = false);

        IEnumerable<Contact> GetByPackageId(Guid packageId, bool deleted = false);

        IEnumerable<Contact> GetByPackageId(Guid packageId, int pageNo, int pageSize, Person.SortColumn sortColum, bool sortAscending, bool deleted = false);

        int CountByPackageId(Guid packageId, bool deleted = false);

        Contact GetById(Guid id);

        Contact Add(Contact contact);

        Contact Update(Contact contact);

        void Delete(Guid contactId);

        void Delete(Contact contact);

        IEnumerable<Contact> GetByPackageId(Guid packageId, IEnumerable<string> groupNamesFilter, bool deleted = false);

        IEnumerable<Contact> GetStaffContacts(Guid packageId);

        IEnumerable<Contact> GetStaffContacts(Guid packageId, Person.SortColumn? sortColumn, bool sortAscending = true, bool deleted = false);

        IEnumerable<Contact> GetTeamContacts(Guid packageId, Person.SortColumn? sortColumn, bool sortAscending = true, bool deleted = false);

        IEnumerable<Contact> GetStaffContacts(Guid packageId, int pageNo, int pageSize, Person.SortColumn? sortColumn, bool sortAscending = true, bool deleted = false);

        IEnumerable<Contact> SearchStaffContacts(Guid packageId, string term, int pageNo, int pageSize, Person.SortColumn? sortColumn, bool sortAscending = true, bool deleted = false);

        IEnumerable<Contact> SearchPeople(Guid packageId, string term, int pageNo, int pageSize, Person.SortColumn? sortColumn, bool sortAscending = true, bool deleted = false);

        int CountStaffContacts(Guid packageId, string term, bool deleted = false);

        int CountPeopleContacts(Guid packageId, string term, bool deleted = false);

        IEnumerable<string> GetCompanies(Guid packageId, bool deleted = false);

        Contact Clone(Contact contact);

        Contact Clone(Guid contactId);

        IEnumerable<Attachment> SearchAttachments(string searchString, Guid packageId);

        IEnumerable<VoiceMessage> SearchVoiceMessages(string searchString, Guid packageId);

        void Retire(Guid contactId);

        void Restore(Guid contactId);

        //Contact GetByPersonId(Guid id, bool framework = false);

        void Detach(Contact contact);

        IEnumerable<Contact> GetByPeriodId(Guid periodId);

        Contact GetByMeetingId(Guid meetingId);

        Contact GetByTaskId(Guid taskId);

        bool EmailExists(string email);

        bool EmailExistsWidthCore(string email, Guid packageId);
    }
}
