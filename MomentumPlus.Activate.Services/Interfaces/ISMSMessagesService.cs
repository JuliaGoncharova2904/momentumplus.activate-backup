﻿using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.People;

namespace MomentumPlus.Activate.Services.Interfaces
{
    public interface ISMSMessagesService
    {
        SMSMessages SendSMS(User SMSToSendUser, string SMSToSendNumber, string SMSFromSendNumber, string message);

        //WSBatch sendSMS(Guid userID, string clientID, string messageBody, DateTime? scheduledDateTime, List<string> recipients, string textOriginator, string emailForReplies);
    }
}
