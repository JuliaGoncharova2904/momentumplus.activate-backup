﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Activate.Services.Interfaces
{
    public interface IToDoItemService
    {
  //      void Add(ToDoItem toDo);

        ToDoItem Update(ToDoItem toDo);

        void Dismiss(Guid toDoId);

//        ToDoItem GetByToDoID(Guid toDoId);

     //   IEnumerable<ToDoItem> List(bool dismissed = false);

  //      IEnumerable<ToDoItem> GetByPackageId(Guid packageId, bool dismissed = false);

 //       IEnumerable<ToDoItem> Search(string searchTerm, bool dismissed = false);

        IEnumerable<ToDoItem> GetByPeriodId(Guid periodId);

        void Delete(ToDoItem toDoItem);
    }
}
