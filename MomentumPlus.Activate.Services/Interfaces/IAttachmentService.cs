﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Activate.Services.Interfaces
{
    public interface IAttachmentService
    {
        /// <summary>
        /// Get all Attachment entities
        /// </summary>
        /// <param name="deleted">If true will return only deleted items</param>
        /// <returns></returns>
        IEnumerable<Attachment> GetAll(bool deleted = false);

        /// <summary>
        /// Get all attachments for a position - paged and sorted
        /// </summary>
        /// <param name="positionId">Position GUID</param>
        /// <param name="pageNo">Page number</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="sortColumn">Sort column</param>
        /// <param name="sortAscending">Sort order</param>
        /// <param name="deleted">If true will return only deleted items</param>
        /// <returns></returns>
        IEnumerable<Attachment> GetAll(Guid positionId, int pageNo, int pageSize, Attachment.SortColumn sortColumn, bool sortAscending, bool deleted = false);

        /// <summary>
        /// Search attachments for a position - sorted and paged
        /// </summary>
        /// <param name="positionId">Position GUID</param>
        /// <param name="query">Search query</param>
        /// <param name="pageNo">Page number</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="sortColumn">Sort column</param>
        /// <param name="sortAscending">Sort order</param>
        /// <param name="deleted">If true will return only deleted items</param>
        /// <returns></returns>
        IEnumerable<Attachment> Search(Guid positionId, string query, int pageNo, int pageSize, Attachment.SortColumn sortColumn, bool sortAscending, bool deleted = false);

        /// <summary>
        /// Get the most recently added attachments for a given package
        /// </summary>
        /// <param name="packageId">The package GUID</param>
        /// <param name="count">Number of attachments to return</param>
        /// <returns></returns>
        IEnumerable<BaseModel> GetRecentByPackage(Guid packageId, int count = 9);

        /// <summary>
        /// Get the number of attachments for a position
        /// </summary>
        /// <param name="positionId">Position GUID</param>
        /// <returns>Attachment count</returns>
        int Count(Guid positionId);

        /// <summary>
        /// Create a new Attachment in the database
        /// </summary>
        /// <param name="attachment"></param>
        /// <returns></returns>
        Attachment Add(Attachment attachment);

        /// <summary>
        /// Add an attachment
        /// </summary>
        /// <param name="positionId"></param>
        /// <param name="attachment"></param>
        /// <returns></returns>
        Attachment Add(Guid positionId, Attachment attachment);

        /// <summary>
        /// Add an Attachment to both a Position and a Contact
        /// </summary>
        /// <param name="positionId">The Position GUID</param>
        /// <param name="attachment">The Attachment object</param>
        /// <param name="contactId">The Contact GUID</param>
        /// <returns></returns>
        Attachment Add(Guid positionId, Attachment attachment, Guid contactId);

        /// <summary>
        /// Update an attachment in the database
        /// </summary>
        /// <param name="attachment">The Attachment object</param>
        /// <returns></returns>
        Attachment Update(Attachment attachment);

        /// <summary>
        /// Delete an Attachment from the database
        /// </summary>
        /// <param name="attachmentId">The Attachment GUID</param>
        void Delete(Guid attachmentId);

        /// <summary>
        /// Get a non-deleted Attachment
        /// </summary>
        /// <param name="attachmentId">The attachment GUID</param>
        /// <returns>The Attachment object</returns>
        Attachment GetByID(Guid attachmentID);

        /// <summary>
        /// Retire an attachment
        /// </summary>
        /// <param name="attachmentId">The Attachment GUID</param>
        void Retire(Guid attachmentId);

        /// <summary>
        /// Resume an attachment
        /// </summary>
        /// <param name="attachmentId">The Attachment GUID</param>
        void Restore(Guid attachmentId);
    }
}
