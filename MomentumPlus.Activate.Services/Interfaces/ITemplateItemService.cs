﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Activate.Services.Interfaces
{
    public interface ITemplateItemService
    {
        Template Add(Guid templateId, TemplateItem templateItem, Module.ModuleType type);

        TemplateItem Update(TemplateItem templateItem);

        void Delete(TemplateItem templateItem);

        IEnumerable<TemplateItem> Get(Guid templateId, Module.ModuleType type);
    }
}
