﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Activate.Services.Interfaces
{
    public interface IMeetingService
    {
        IEnumerable<Meeting> GetByPackageId(Guid packageId, Meeting.SortColumn sortColumn, bool sortAscending, bool deleted = false, List<Guid> meetingTypeIdsFilter = null, DateTime? createdAfter = null);
        
        IEnumerable<Meeting> GetByPackageId(Guid packageId, int pageNo, int pageSize, Meeting.SortColumn sortColumn, bool sortAscending, bool deleted = false, List<Guid> meetingTypeIdsFilter = null, DateTime? createdAfter = null);

        Meeting GetByProjectInstanceId(Guid id);

        int CountByPackageId(Guid packageId, bool deleted = false, List<Guid> meetingTypeIdsFilter = null);

        IEnumerable<Meeting> GetByPackageId(Guid packageId);

        IEnumerable<Meeting> GetOnboarding(Guid packageId);

        Meeting GetByMeetingId(Guid meetingId);

        Meeting Update(Meeting meeting);

        IEnumerable<Attachment> SearchAttachments(string searchString, Guid packageId);

        IEnumerable<VoiceMessage> SearchVoiceMessages(string searchString, Guid packageId);

        IEnumerable<MeetingType> GetMeetingTypes();

        MeetingType GetMeetingTypeById(Guid meetingTypeId);

        IEnumerable<Meeting> GetByPeriodId(Guid periodId);

        void Delete(Meeting meeting);
    }
}
