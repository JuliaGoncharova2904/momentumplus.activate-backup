﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Activate.Services.Interfaces
{
    public interface IDocumentLibraryService
    {
        IEnumerable<DocumentLibrary> GetAll(bool deleted = false);

        DocumentLibrary GetByID(Guid documentId);

        DocumentLibrary Add(DocumentLibrary documentLibrary);

        int Count(Guid? positionId, string query, bool deleted = false);

        DocumentLibrary UpdateDocumentLibrary(DocumentLibrary document);

        IEnumerable<DocumentLibrary> Search(Guid? positionId, string query, int pageNo, int pageSize, DocumentLibrary.SortColumn sortColumn, bool sortAscending, bool deleted = false);

        void AddToContact(Guid documentId, Guid contactId);

        void AddToMeeting(Guid documentId, Guid meetingId);

        void AddToTask(Guid documentId, Guid taskId);

        void AddToTopic(Guid documentId, Guid topicId);

        void AddToTopicGroup(Guid documentId, Guid topicGroupId);

        void AddToProject(Guid documentId, Guid projectId);

        void AddToProjectInstance(Guid documentId, Guid projectInstanceId);

        void AddToStaff(Guid documentId, Guid staffId);

        IEnumerable<BaseModel> GetRecentByPackageId(Guid packageId, int count = 9);
    }
}
