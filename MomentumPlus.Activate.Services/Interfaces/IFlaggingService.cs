﻿using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.Moderation;
using System;

namespace MomentumPlus.Activate.Services.Interfaces
{
    public interface IFlaggingService
    {

        PackageFlag GetOnboardingModuleFlag(Guid packageId, bool isAdmin = false);

        PackageFlag GetCriticalModuleFlag(Guid packageId, bool isAdmin = false);

        PackageFlag GetExperiencesModuleFlag(Guid packageId, bool isAdmin = false);

        PackageFlag GetRelationshipsModuleFlag(Guid packageId, bool isAdmin = false);

        PackageFlag GetTasksModuleFlag(Guid packageId, bool isAdmin = false);

        PackageFlag GetProjectsModuleFlag(Guid packageId, bool isAdmin = false);

        PackageFlag GetPeopleModuleFlag(Guid packageId, bool isAdmin = false);

        PackageFlag GetTopicGroupFlag(Guid topicGroupId);

        PackageFlag GetTopicGroupFlag(TopicGroup topicGroup);

        PackageFlag GetTopicFlag(Guid topicId);

        PackageFlag GetTopicFlag(Topic topic);

        PackageFlag GetTaskFlag(Task task, Guid? packageId);

        PackageFlag GetTaskFlag(Guid taskId, Guid? packageId);

        PackageFlag GetFlag(Reviewable moderated);

        PackageFlag GetExitSurveyModuleFlag(Guid packageId, bool isAdmin = false);

        PackageFlag GetPackageFlagForCriticalModule(Guid packageId, bool isAdmin = false);
    }

    public class PackageFlag : IComparable
    {

        public string Status { get; set; }

        public string PackageStatus { get; set; }

        public Package Package { get; set; }

        public int ReadyForReviewCounter { get; set; }

        public int ApprovedCounter { get; set; }

        public int DeclinedCounter { get; set; }

        public int AllCounter { get; set; }

        public bool ReadyForReview { get; set; }

        public bool HasNewItems { get; set; }

        public bool ExitSurvey { get; set; }

        public bool HasItems { get; set; }

        public int CompareTo(object obj)
        {
            if (obj is PackageFlag)
            {
                var priority = GetPriority(((PackageFlag)obj).Status);
                return GetPriority(Status).CompareTo(priority);
            }
            return -1;
        }

        // yeah this is bad :( maybe enum would be better in this case
        private int GetPriority(string status)
        {
            int result = 0;
            switch (status)
            {
                case "overdue":
                    result = 4;
                    break;
                case "attention-required":
                    result = 3;
                    break;
                case "update-required":
                    result = 2;
                    break;
                case "approved":
                    result = 1;
                    break;
            }
            return result;
        }
    }
}
