﻿using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.Moderation;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Activate.Services
{
    public interface ITaskService
    {
        void Add(Task task);

        IEnumerable<Task> GetAllByTopicId(Guid topicId, int pageNo, int pageSize, bool? completed, Task.SortColumn sortColumn, bool sortAscending, bool deleted = false);

        IEnumerable<Task> GetAllByTopicId(Guid topicId);

        IEnumerable<Task> GetAllByProjectInstanceId(Guid projectIntanceId);

        IEnumerable<Task> GetAllByContactId(Guid contactId);

        Task GetByTaskId(Guid taskId);

        int CountByTopicId(Guid topicId, bool deleted = false);

        Task Update(Task task);

        void Retire(Guid topicId);

        void Restore(Guid topicId);

        Task Clone(Guid taskId);

        IEnumerable<Task> Search(string searchString, Guid packageId);

        IEnumerable<Attachment> SearchAttachments(string searchString, Guid packageId);

        IEnumerable<VoiceMessage> SearchVoiceMessages(string searchString, Guid packageId);

        IEnumerable<Task> GetAllByPackageId(Guid packageId, bool approvedOnly = false);

        IEnumerable<Task> GetCoreTasksByModuleType(Guid id, Module.ModuleType moduleType);

        void UpdateTaskWeekDue(int earliestStartWeek);

        bool IsTaskNameUnique(string taskName, Reviewable parent);

        int? GetLatestForPackage(Guid id);

        void SetFixedDateTime(Guid packageId, DateTime onboardingStart, int onboardinDuration);

        void ResetFixedDateTime(Guid packageId);
    }
}
