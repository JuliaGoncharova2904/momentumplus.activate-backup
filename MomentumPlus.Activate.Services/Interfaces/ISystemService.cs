﻿using System;
using System.Collections.Generic;
using SystemMP = MomentumPlus.Core.Models.System;

namespace MomentumPlus.Activate.Services.Interfaces
{
    public interface ISystemService 
    {
        IEnumerable<SystemMP> GetAll();

        SystemMP GetBySystemId(Guid systemId);

        IEnumerable<SystemMP> Search(string query, int pageNo, int pageSize, SystemMP.SortColumn sortColumn, bool sortAscending);

        SystemMP Add(SystemMP system);

        SystemMP Update(SystemMP system);

        bool Exists(string name);

        void Delete(Guid systemId);
    }
}
