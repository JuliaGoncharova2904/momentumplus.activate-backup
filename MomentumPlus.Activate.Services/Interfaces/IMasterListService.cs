﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Activate.Services.Interfaces
{
    public interface IMasterListService
    {
        IEnumerable<MasterList> GetAll(MasterList.ListType listType);

        MasterList GetByMasterListId(Guid masterListId);

        IEnumerable<MasterList> Search(MasterList.ListType listType, string query, int pageNo, int pageSize, MasterList.SortColumn sortColumn, bool sortAscending);

        int SearchCount(MasterList.ListType listType, string query);

        MasterList Add(MasterList masterList);

        MasterList Update(MasterList masterList);

        bool Exists(MasterList.ListType listType, string name);

        void Delete(Guid masterListId);

        Guid? GetDefaultCompanyId();

        MasterList GetCompany(Guid companyId);
    }
}
