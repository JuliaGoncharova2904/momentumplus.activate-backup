﻿using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.People;
using System;
using System.Collections.Generic;
using System.Web.Security;

namespace MomentumPlus.Activate.Services.Interfaces
{
    public interface IUserService
    {
        IEnumerable<User> GetAll(bool deleted = false);
        
        IEnumerable<User> GetAllIncludingRetired();

        int Count(bool deleted = false);

        MembershipUser GetUser();

        IEnumerable<User> GetAllByRole(Role.RoleEnum theEnum, bool deleted = false);

        IEnumerable<User> GetAll(int pageNo, int pageSize, User.SortColumn sortColumn, bool sortAscending, bool deleted = false);

        User GetByUsername(string username);

        User GetByUserId(Guid userId);

        User GetByPasswordResetGuid(Guid resetGuid);

        User CreateUser(User user);

        bool ChangePassword(Guid userId, string newPassword);

        bool ChangePassword(Guid userId, string currentPassword, string newPassword);

        bool Exists(string username);

        User UpdateUser(User user);

        User IncrementUserSMS(User user);

        void DeleteUser(Guid userId);

        void Retire(Guid userId);

        void Restore(Guid userId);

        Role GetRolesForUser(string username);

        User ChangeLogedInStatus(string username);

        User UpdateUserAppSession(string username, string appSessionID);

        IEnumerable<User> Search(string query, int pageNo, int pageSize, User.SortColumn sortColumn, bool sortAscending, bool deleted = false);

        int SearchCount(string query, bool deleted = false);

        void ResetPassword(Guid user, string newPassword);
    }
}
