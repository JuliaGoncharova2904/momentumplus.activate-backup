﻿using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Services.Interfaces
{
    public interface IAccountConfigService
    {
        /// <summary>
        /// Get the single account config entity
        /// </summary>
        /// <returns></returns>
        AccountConfig Get();

        /// <summary>
        /// Update the account config in the database
        /// </summary>
        /// <param name="accountConfig"></param>
        /// <returns></returns>
        AccountConfig Update(AccountConfig accountConfig);
    }
}
