﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Activate.Services.Interfaces
{
    public interface ITopicQuestionService 
    {
        IEnumerable<TopicQuestion> GetAll();

        TopicQuestion GetByReference(Guid reference);
    }
}
