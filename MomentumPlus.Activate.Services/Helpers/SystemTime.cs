﻿using System;
using System.Web;

namespace MomentumPlus.Activate.Services.Helpers
{
    public static class SystemTime
    {
        #region private properties

        private static readonly string KeyOverwrittenDateTime = "KeyOverwrittenDateTime";

        private static DateTime? OverwrittenDateTime
        {
            get
            {
                if (HttpContext.Current.Session == null || HttpContext.Current.Session[KeyOverwrittenDateTime] == null)
                    return null;

                return (DateTime) HttpContext.Current.Session[KeyOverwrittenDateTime];
            }
            set { HttpContext.Current.Session[KeyOverwrittenDateTime] = value; }
        }

        #endregion

        #region Public Properties

        public static DateTime Now
        {
            get { return GetTime(); }
            set { SetTime(value); }
        }

        #endregion

        #region Public Methods 

        /// <summary>
        /// Sets SystemTime.Now to DateTime.Now
        /// </summary>
        public static void Clear()
        {
            OverwrittenDateTime = null;
        }

        #endregion

        #region Private Methods

        private static DateTime GetTime()
        {
            if (OverwrittenDateTime.HasValue)
                return OverwrittenDateTime.Value;

            return DateTime.Now;
        }

        private static void SetTime(DateTime newDateTime)
        {
            OverwrittenDateTime = newDateTime;
        }

        #endregion
    }
}