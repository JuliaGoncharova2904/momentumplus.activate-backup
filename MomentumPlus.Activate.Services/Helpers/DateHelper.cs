﻿using System;
using MomentumPlus.Activate.Services.Extensions;
using MomentumPlus.Core.Models.Handover;

namespace MomentumPlus.Activate.Services.Helpers
{
    public static class DateHelper
    {
        /// <summary>
        ///     Calculate the start point of a leaving period. If the Last Day and Notice Period
        ///     are not set then this method will return null.
        /// </summary>
        /// <param name="leavingPeriod"></param>
        /// <returns></returns>
        public static DateTime? CalculateStartPoint(LeavingPeriod leavingPeriod)
        {
            DateTime? startPoint = null;

            if (leavingPeriod.LastDay.HasValue)
            {
                var lastDay = leavingPeriod.LastDay.Value;

                switch (leavingPeriod.NoticePeriod)
                {
                    case LeavingPeriod.Notice.OneWeek:
                        startPoint = lastDay.AddDays(-7).PreviousBusinessDayOrSelf().IsSameDayOrEarly();
                        break;
                    case LeavingPeriod.Notice.OneMonth:
                        startPoint = lastDay.AddMonths(-1).PreviousBusinessDayOrSelf().IsSameDayOrEarly();
                        break;
                    case LeavingPeriod.Notice.ThreeMonths:
                        startPoint = lastDay.AddMonths(-3).PreviousBusinessDayOrSelf().IsSameDayOrEarly();
                        break;
                    case LeavingPeriod.Notice.SixMonths:
                        startPoint = lastDay.AddMonths(-6).PreviousBusinessDayOrSelf().IsSameDayOrEarly();
                        break;
                    case LeavingPeriod.Notice.TwelveMonths:
                        startPoint = lastDay.AddMonths(-12).PreviousBusinessDayOrSelf().IsSameDayOrEarly();
                        break;
                }
            }

            return startPoint;
        }


        /// <summary>
        ///     Check if date is same day or early and return date
        /// </summary>
        /// <param name="startPoint"></param>
        /// <returns></returns>
        public static DateTime? IsSameDayOrEarly(this DateTime startPoint)
        {
            if (startPoint.Year == DateTime.Now.Year && startPoint.Month == DateTime.Now.Month
                && startPoint.Day == DateTime.Now.Day || startPoint < DateTime.Now.Date)
            {
                return DateTime.Now.Date;
            }
            return startPoint;
        }


        /// <summary>
        ///     Calculate the mid point of a leaving period. If the Last Day and Start Point are not set
        ///     then this method will return null.
        /// </summary>
        /// <param name="leavingPeriod"></param>
        /// <returns></returns>
        public static DateTime? CalculateMidPoint(LeavingPeriod leavingPeriod)
        {
            if (leavingPeriod.LastDay.HasValue && leavingPeriod.StartPoint.HasValue)
            {
                var lastDay = leavingPeriod.LastDay.Value;
                var startPoint = leavingPeriod.StartPoint.Value;

                var difference = (double)(lastDay - startPoint).Days;
                var midPoint = lastDay.AddDays(-Math.Round(difference / 2));
                return midPoint.PreviousBusinessDayOrSelf();
            }

            return null;
        }

        /// <summary>
        ///     Calculate the mid point of an onboarding period. If the Last Day and Start Point are not set
        ///     then this method will return null.
        /// </summary>
        /// <param name="onboardingPeriod"></param>
        /// <returns></returns>
        public static DateTime? CalculateMidPoint(OnboardingPeriod onboardingPeriod)
        {
            if (onboardingPeriod.LastDay.HasValue && onboardingPeriod.StartPoint.HasValue)
            {
                var lastDay = onboardingPeriod.LastDay.Value;
                var startPoint = onboardingPeriod.StartPoint.Value;

                var difference = (double)(lastDay - startPoint).Days;
                var midPoint = lastDay.AddDays(-Math.Round(difference / 2));
                return midPoint.NextBusinessDayOrSelf();
            }

            return null;
        }

        /// <summary>
        ///     Calculate the last day of an onboarding period. If the Start Point and Duration are
        ///     not set then this method will return null.
        /// </summary>
        /// <param name="onboardingPeriod"></param>
        /// <returns></returns>
        public static DateTime? CalculateLastDay(OnboardingPeriod onboardingPeriod)
        {
            if (onboardingPeriod.StartPoint.HasValue && onboardingPeriod.Duration.HasValue)
            {
                var startPoint = onboardingPeriod.StartPoint.Value;
                var duration = onboardingPeriod.Duration.Value;
                return startPoint.AddDays(duration * 7);
            }

            return null;
        }
    }
}
