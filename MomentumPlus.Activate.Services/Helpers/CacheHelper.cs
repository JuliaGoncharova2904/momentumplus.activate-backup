﻿using System;
using System.Web;
using System.Web.Caching;

namespace MomentumPlus.Activate.Services.Helpers
{
    public class CacheHelper
    {
        #region private properties

        private static TimeSpan slidingExpiration = Cache.NoSlidingExpiration;
        
        private static DateTime absoluteExpirartion = DateTime.Now.AddMinutes(15);

        #endregion

        #region public helper methods

        public static T GetCached<T>(string key, Func<T> initializer)
        {
            return GetCached(key, initializer, slidingExpiration, absoluteExpirartion);
        }

        #endregion

        #region private members

        private static T GetCached<T>(string key, Func<T> initializer, TimeSpan slidingExpiration, DateTime absoluteExpiration)
        {
            var httpContext =
                HttpContext.Current;

            if (httpContext != null)
            {
                key =
                    string.Intern(key);
                
                lock (key)
                {
                    var obj = 
                        httpContext.Cache[key];

                    if (obj == null)
                    {
                        obj = initializer();
                        httpContext.Cache.Add(
                            key, obj, null, absoluteExpiration, slidingExpiration, System.Web.Caching.CacheItemPriority.Default, null);
                    }

                    if (obj == null && (typeof(T)).IsValueType)
                        return default(T);
                    
                    return (T)obj;
                }
            }
            else
            {
                return initializer();
            }
        }

        #endregion
    }
}
