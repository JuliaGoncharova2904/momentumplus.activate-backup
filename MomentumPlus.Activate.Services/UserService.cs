﻿using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Activate.Services.Interfaces;
using System.Data.Entity;
using System.Web.Security;
using MomentumPlus.Core.Authorization;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models.People;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Services
{
    /// <summary>
    /// User Service
    /// </summary>
    public class UserService : ServiceBase, IUserService
    {
        public UserService(MomentumContext context) : base(context) { }

        #region IUserService Members

        /// <summary>
        /// Get Users by Role
        /// </summary>
        /// <param name="roleEnum">Role type</param>
        /// <param name="deleted"></param>
        /// <returns></returns>
        public IEnumerable<User> GetAllByRole(Role.RoleEnum roleEnum, bool deleted = false)
        {
            string roleName = 
                GetEnumDescription(roleEnum);

            var users =
                MContext.Users.Where(f => f.Deleted.HasValue == deleted && f.Role.RoleName == roleName);

            return users.ToList();
        }

        /// <summary>
        /// Get Current User
        /// </summary>
        /// <returns></returns>
        public MembershipUser GetUser()
        {
            return Membership.GetUser();
        }

        /// <summary>
        /// Get all including retirted
        /// </summary>
        /// <returns></returns>
        public IEnumerable<User> GetAllIncludingRetired()
        {
            var users = MContext.Users;

            return users.ToList();
        }

        /// <summary>
        /// Get all Users 
        /// </summary>
        /// <param name="deleted"></param>
        /// <returns></returns>
        public IEnumerable<User> GetAll(bool deleted = false)
        {
            var users =
                MContext.Users.Where(f => f.Deleted.HasValue == deleted);

            return users.ToList();
        }

        /// <summary>
        /// Count Users
        /// </summary>
        /// <param name="deleted"></param>
        /// <returns></returns>
        public int Count(bool deleted = false)
        {
            return GetAll(deleted).Count();
        }

        /// <summary>
        /// Get all User
        /// </summary>
        /// <param name="pageNo">Page number to start with</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="sortColumn">Sort column for results</param>
        /// <param name="sortAscending">Sort ascending</param>
        /// <param name="deleted">Return deleted</param>
        /// <returns></returns>
        public IEnumerable<User> GetAll(int pageNo, int pageSize, User.SortColumn sortColumn, bool sortAscending, bool deleted = false)
        {
            var users =
                MContext.Users.Where(f => f.Deleted.HasValue == deleted);

            SortAndPage<User>(pageNo, pageSize, sortColumn.ToString(), sortAscending, ref users);

            return users.ToList();
        }

        /// <summary>
        /// Get User by Username
        /// </summary>
        /// <param name="username">Username</param>
        /// <returns>User</returns>
        public User GetByUsername(string username)
        {
            var user =
                MContext.Users.Include(u => u.Role).Include(u => u.Contact).FirstOrDefault(u => !u.Deleted.HasValue && u.Username == username);

            return user;
        }

        /// <summary>
        /// Get User by Id
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <returns>User</returns>
        public User GetByUserId(Guid userId)
        {
            var user =
                MContext.Users.Include(u => u.Role).Include(u => u.Contact).FirstOrDefault(u => u.ID == userId);

            return user;
        }

        /// <summary>
        /// Get User by password reset guid
        /// </summary>
        /// <param name="resetGuid">Password reset guid</param>
        /// <returns>User</returns>
        public User GetByPasswordResetGuid(Guid resetGuid)
        {
            var user =
                MContext.Users.Where(u => !u.Deleted.HasValue && u.PasswordResetGuid == resetGuid).FirstOrDefault();

            return user;
        }

        /// <summary>
        /// Register a new User
        /// </summary>
        /// <param name="user"></param>
        /// <returns>User</returns>
        public User CreateUser(User user)
        {
            WebSecurity.Register(user);

            return user;
        }

        /// <summary>
        /// Reset User's password
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="newPassword"></param>
        public void ResetPassword(Guid userId, string newPassword)
        {
            var user = MContext.Users.Find(userId);
            string newHashedPassword = Crypto.HashPassword(newPassword);

            user.Password = newHashedPassword;
            user.PasswordResetGuid = null;
            UpdateUser(user);
        }

        /// <summary>
        /// Change User's password
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        public bool ChangePassword(Guid userId, string newPassword)
        {
            var user = MContext.Users.Find(userId);

            ResetPassword(user.ID, newPassword);

            return true;
        }

        /// <summary>
        /// Change User's password
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="currentPassword">Current password</param>
        /// <param name="newPassword">New password</param>
        /// <returns></returns>
        public bool ChangePassword(Guid userId, string currentPassword, string newPassword)
        {
            var user = MContext.Users.Find(userId);

            return WebSecurity.ChangePassword(user.Username, currentPassword, newPassword);
        }

        /// <summary>
        /// Does user exist?
        /// </summary>
        /// <param name="username">Username</param>
        /// <returns>True if exists</returns>
        public bool Exists(string username)
        {
            bool exists = false;

            var user =
                MContext.Users.Where(u => !u.Deleted.HasValue && u.Username == username).FirstOrDefault();

            if (user != null)
                exists = true;

            return exists;
        }

        /// <summary>
        /// Update user
        /// </summary>
        /// <param name="user">Updates a User</param>
        /// <returns>User</returns>
        public User UpdateUser(User user)
        {
            MContext.Entry(user).State = EntityState.Modified;

            MContext.SaveChanges();

            return user;
        }

        /// <summary>
        /// Increments the number of messages
        /// the user has sent
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public User IncrementUserSMS(User user)
        {
            user.LastSMSMessage = DateTime.Now;
            user.SMSMessagesToday++;


            return UpdateUser(user);
        }

        /// <summary>
        /// Deletes a User
        /// </summary>
        /// <param name="userId">User Id</param>
        public void DeleteUser(Guid userId)
        {
            var user = MContext.Users.Find(userId);
            MContext.Users.Remove(user);
            MContext.SaveChanges();
        }

        /// <summary>
        /// Gets the Roles associated with the
        /// user
        /// </summary>
        /// <param name="username">Username</param>
        /// <returns>Role</returns>
        public Role GetRolesForUser(string username)
        {
            var user = MContext.Users.Include(u => u.Role)
                                     .FirstOrDefault(u => !u.Deleted.HasValue && u.Username == username);
                
            return user.Role;
        }

        /// <summary>
        /// Toggle logged in status
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public User ChangeLogedInStatus(string username)
        {
            var user =
                MContext.Users.Where(u => !u.Deleted.HasValue && u.Username == username)
                .Include(u => u.Role).FirstOrDefault();

            if (user.IsLoggedIn)
                user.IsLoggedIn = false;
            else
                user.IsLoggedIn = true;

            MContext.SaveChanges();

            return user;
        }
        
        /// <summary>
        /// Update the Use's session Id
        /// </summary>
        /// <param name="username"></param>
        /// <param name="appSessionID"></param>
        /// <returns></returns>
        public User UpdateUserAppSession(string username, string appSessionID)
        {
            var user =
                MContext.Users.Where(u => !u.Deleted.HasValue && u.Username == username)
                .Include(u => u.Role).FirstOrDefault();

            user.LastSessionID = appSessionID;

            MContext.SaveChanges();

            return user;

        }

        /// <summary>
        /// Search for Users
        /// </summary>
        /// <param name="query">Search query</param>
        /// <param name="pageNo">Page number for the results</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="sortColumn">Sort column</param>
        /// <param name="sortAscending">Sort ascending</param>
        /// <param name="deleted">Return deleted</param>
        /// <returns></returns>
        public IEnumerable<User> Search(string query, int pageNo, int pageSize, User.SortColumn sortColumn, bool sortAscending, bool deleted = false)
        {
            IEnumerable<User> searchResults =
                SearchBase(query, pageNo, pageSize, sortColumn, sortAscending, deleted);

            return searchResults;
        }

        /// <summary>
        /// Search count
        /// </summary>
        /// <param name="query">Search query</param>
        /// <param name="deleted">Return deleted</param>
        /// <returns></returns>
        public int SearchCount(string query, bool deleted = false)
        {
            IEnumerable<User> searchResults = SearchBase(query, deleted);

            return searchResults == null ? 0 : searchResults.Count();
        }

        #endregion

        /// <summary>
        /// Helper search method
        /// </summary>
        /// <param name="query">Search query</param>
        /// <param name="deleted">Return deleted</param>
        /// <returns></returns>
        private IQueryable<User> SearchBase(string query, bool deleted = false)
        {
            IQueryable<User> users =
                 MContext.Users.Include(u => u.Contact).Where(u => u.Deleted.HasValue == deleted);

            if (!string.IsNullOrWhiteSpace(query))
            {
                users =
                    users.Where(t => (t.Contact.FirstName + t.Contact.LastName).Contains(query)).AsQueryable();
            }

            return users;
        }

        /// <summary>
        /// Helper search method
        /// </summary>
        /// <param name="query">Search query</param>
        /// <param name="pageNo">Page number</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="sortColumn">Sort column</param>
        /// <param name="sortAscending">Sort ascendinfg</param>
        /// <param name="deleted">Return deleted</param>
        /// <returns></returns>
        private IQueryable<User> SearchBase(string query, int pageNo, int pageSize, User.SortColumn sortColumn, bool sortAscending, bool deleted = false)
        {
            IQueryable<User> users = SearchBase(query, deleted);
            SortAndPage(ref users, u => u, sortColumn.ToString(), sortAscending, pageNo, pageSize);
            return users;
        }

        /// <summary>
        /// Hide a User 
        /// </summary>
        /// <param name="userId">User Id</param>
        public void Retire(Guid userId)
        {
            UpdateDeletedFields(
                DateTime.Now, userId);
        }

        /// <summary>
        /// Restore a User
        /// </summary>
        /// <param name="userId">User id</param>
        public void Restore(Guid userId)
        {
            UpdateDeletedFields(
                null, userId);
        }

        /// <summary>
        /// Retire/resume
        /// </summary>
        /// <param name="deleted">Set deleted/restore</param>
        /// <param name="userId">User to update</param>
        private void UpdateDeletedFields(DateTime? deleted, Guid userId)
        {
            var user = 
                MContext.Users.Find(userId);

            user.Deleted = 
                deleted;

            MContext.SaveChanges();
        }
    }
}
