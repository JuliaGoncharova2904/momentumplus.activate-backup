﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models.ProLink;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Services
{
    /// <summary>
    /// Handles Pro-link (Processes/Functions)
    /// </summary>
    public class ProLinkService : ServiceBase, IProLinkService
    {
        public ProLinkService(MomentumContext context) : base(context) { }

        /// <summary>
        /// Adds a Function
        /// </summary>
        /// <param name="function">Function to add</param>
        /// <returns>Function</returns>
        public Function Add(Function function)
        {
            function = MContext.Function.Add(function);
            MContext.SaveChanges();

            return function;
        }

        /// <summary>
        /// Adds a Process Group
        /// </summary>
        /// <param name="processGroup">Process Group to add</param>
        /// <returns></returns>
        public ProcessGroup Add(ProcessGroup processGroup)
        {
            processGroup = MContext.ProcessGroup.Add(processGroup);
            MContext.SaveChanges();

            return processGroup;
        }


        /// <summary>
        ///  Checks if a Process Group exists by name 
        /// </summary>
        /// <param name="functionId">Function Id</param>
        /// <param name="processGroupName">Process Group Name</param>
        /// <returns></returns>
        public bool Exists(Guid functionId, string processGroupName)
        {
            var topics = MContext.ProcessGroup.Where(p => !p.Deleted.HasValue && functionId == p.FunctionId &&  p.Name.Equals(processGroupName, StringComparison.Ordinal)).ToList();
            return topics.Any();
        }



        /// <summary>
        /// Gets a Function
        /// </summary>
        /// <param name="id">Function Id</param>
        /// <returns></returns>
        public Function GetFunction(Guid id)
        {
            return MContext.Function.Find(id);
        }

        /// <summary>
        /// Gets a Process Group
        /// </summary>
        /// <param name="id">Process Group Id</param>
        /// <returns>ProcessGroup</returns>
        public ProcessGroup GetProcessGroup(Guid id)
        {
            return MContext.ProcessGroup.Find(id);
        }

        /// <summary>
        /// Get all Framework functions
        /// </summary>
        /// <returns>Collection of Function</returns>
        public IEnumerable<Function> GetAllFrameworkFunctions()
        {
            var functions = MContext.Function.Where(f => f.IsFramework)
                                                            .Include(f => f.ProcessGroups)
                                                            .Include(f => f.ProcessGroups.Select(pg => pg.Topics));

            return functions;
        }

        /// <summary>
        /// Get all business Process Topics
        /// </summary>
        /// <returns>Collection of Topic</returns>
        public IEnumerable<Topic> GetAllBusinessProcessTopics()
        {
            var topics = MContext.Topic.Where(t => t.ParentTopicGroup.ParentModule.IsFrameworkDefault && t.ParentTopicGroup.TopicGroupType == TopicGroup.TopicType.Process)
                                                   .Include(t => t.ProcessGroup);

            return topics;
        }

        /// <summary>
        /// Updates a Function
        /// </summary>
        /// <param name="function"></param>
        /// <returns>Function</returns>
        public Function Update(Function function)
        {
            MContext.Entry(function).State = EntityState.Modified;
            MContext.SaveChanges();

            return function;
        }

        /// <summary>
        /// Updates a Process Group
        /// </summary>
        /// <param name="processGroup"></param>
        /// <returns>Process Group</returns>
        public ProcessGroup Update(ProcessGroup processGroup)
        {
            MContext.Entry(processGroup).State = EntityState.Modified;
            MContext.SaveChanges();

            return processGroup;
        }


        /// <summary>
        /// Updates a Process Group
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <returns>Process Group</returns>
        public ProcessGroup UpdateById(Guid id, string name, string description)
        {
            var updatedProcessGroup = MContext.ProcessGroup.SingleOrDefault(p => p.ID == id);
            if (updatedProcessGroup != null)
            {
                updatedProcessGroup.Name = name;
                updatedProcessGroup.Description = description;
                MContext.SaveChanges();
            }

            return updatedProcessGroup;
        }



    }
}
