﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using MomentumPlus.Activate.Services.Interfaces;
using System.Linq.Expressions;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Services
{
    /// <summary>
    /// Modile Service
    /// for Critical, Experiences, Projects
    /// </summary>
    public class ModuleService : ServiceBase, IModuleService
    {
        public ModuleService(MomentumContext context) : base(context) { }

        #region IModuleService Members

        /// <summary>
        /// 
        /// </summary>
        /// <param name="packageId"></param>
        /// <param name="pageNo"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortAscending"></param>
        /// <returns></returns>
        public IEnumerable<Module> GetByPackageId(Guid packageId, int pageNo, int pageSize, Module.SortColumn? sortColumn, bool sortAscending)
        {
            var package =
                MContext.Package.Where(p => !p.Deleted.HasValue && p.ID == packageId).Include(p => p.Modules).FirstOrDefault();

            var modules =
                package.Modules.AsQueryable();

            if (sortColumn != null)
            {
                SortAndPage<Module>(pageNo, pageSize, sortColumn.ToString(), sortAscending, ref modules);
            }
            else
            {
                foreach (Module module in modules)
                {
                    if (module.TopicGroups != null)
                    {
                        module.TopicGroups =
                            SortTopicGroups(module.TopicGroups.AsQueryable()).ToList();
                    }
                }

            }

            return modules.ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="moduleId"></param>
        /// <returns></returns>
        public Module GetByModuleId(Guid moduleId)
        {
            var module =
                MContext.Module.Find(moduleId);

            return module;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="moduleType"></param>
        /// <returns></returns>
        public Module GetByModuleType(Module.ModuleType moduleType)
        {
            Module module = null;

            module = MContext.Module.Where(p => !p.Deleted.HasValue && p.Type == moduleType && p.IsFrameworkDefault).FirstOrDefault();

            if (module != null && module.TopicGroups != null)
            {
                module.TopicGroups =
                    SortTopicGroups(module.TopicGroups.AsQueryable()).ToList();
            }

            return module;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="moduleType"></param>
        /// <param name="packageId"></param>
        /// <returns></returns>
        public Module GetByModuleType(Module.ModuleType moduleType, Guid packageId)
        {
            Module module = null;

            var package = MContext.Package.Find(packageId);

            if (package != null)
            {
                module =
                    package.Modules.Where(m => m.Type == moduleType).FirstOrDefault();
            }

            if (module != null && module.TopicGroups != null)
            {
                module.TopicGroups =
                    SortTopicGroups(module.TopicGroups.AsQueryable()).ToList();
            }

            return module;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="moduleType"></param>
        /// <param name="packageId"></param>
        /// <returns></returns>
        public List<TopicGroup> GetTemplateGroupsByModyleType(Module.ModuleType moduleType, Guid packageId)
        {
            Module module = null;
            List<TopicGroup> topicGroups = new List<TopicGroup>();
            TemplateService templateService = new TemplateService(MContext);
            TopicGroupService topicGroupService = new TopicGroupService(MContext);
            TopicService topicService = new TopicService(MContext);

            var package = MContext.Package.Find(packageId);

            if (package != null)
            {
                module =
                    package.Modules.Where(m => m.Type == moduleType).FirstOrDefault();
            }

            if (module != null && module.TopicGroups != null)
            {
                if (package.Position.TemplateId != null)
                {
                    var templateItemsTopicGroups = templateService.GetTemplateItems(package.Position.TemplateId.Value, moduleType).Where(t => t.Type == TemplateItem.TemplateItemType.TopicGroup && t.Enabled);
                    var templateItemsTopics = templateService.GetTemplateItems(package.Position.TemplateId.Value, moduleType).Where(t => t.Type == TemplateItem.TemplateItemType.Topic && t.Enabled);

                    foreach (var templateItemsTopicGroup in templateItemsTopicGroups)
                    {
                        var topicGroup = topicGroupService.GetByPackageId(package.ID).FirstOrDefault(tg => tg.Name.Equals(templateItemsTopicGroup.Name));

                        if (topicGroup != null)
                        {
                            var userTopics = topicGroup.Topics.Where(t => !t.Deleted.HasValue && t.SourceId == null);
                            var newTopicGroup = new TopicGroup
                            {
                                Topics = new List<Topic>(),
                                ID = topicGroup.ID,
                                Name = topicGroup.Name,
                                ReferenceId = topicGroup.ReferenceId,
                                Created = topicGroup.Created,
                                Title = topicGroup.Title,
                                Ordinal = topicGroup.Ordinal,
                                SourceTemplate = topicGroup.SourceTemplate,
                                TopicGroupType = topicGroup.TopicGroupType,
                                SourceTemplateId = topicGroup.SourceTemplateId,
                                Deleted = topicGroup.Deleted,
                                HandoverGate = topicGroup.HandoverGate,
                                Modified = topicGroup.Modified,
                                Notes = topicGroup.Notes,
                                SourceId = topicGroup.SourceId,
                                ParentModule = topicGroup.ParentModule,
                                ParentModuleId = topicGroup.ParentModuleId,
                                PeriodId = topicGroup.PeriodId,
                                SearchTags = topicGroup.SearchTags,
                                ReviewBy = topicGroup.ReviewBy,
                                TopicGroupCategory = topicGroup.TopicGroupCategory,
                                UserLastViewed = topicGroup.UserLastViewed

                            };

                            foreach (var templateItemsTopic in templateItemsTopics)
                            {
                                var templateTopic = topicService.GetByReferenceId(templateItemsTopic.SourceReferenceId);

                                if (templateTopic != null && templateTopic.ParentTopicGroup.Name.Equals(topicGroup.Name))
                                {
                                    var realTopic = topicService.GetTopicByGroupIdAndName(topicGroup.ID, templateTopic.Name);
                                    if (realTopic != null)
                                    {
                                        newTopicGroup.Topics.Add(realTopic);
                                    }
                                }
                            }
                            foreach (var userTopic in userTopics)
                            {
                                newTopicGroup.Topics.Add(userTopic);
                            }
                            topicGroups.Add(newTopicGroup);
                        }
                    }

                }
            }

            return topicGroups;
        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="module"></param>
        /// <returns></returns>
        public Module Update(Module module)
        {
            MContext.Entry(module).State = EntityState.Modified;

            MContext.SaveChanges();

            return module;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="moduleId"></param>
        /// <returns></returns>
        public DateTime? GetDateLastEntry(Guid moduleId)
        {
            Module module = MContext.Module.Find(moduleId);

            var topics = (from tg in module.TopicGroups
                          from to in tg.Topics
                          select to)
                          .OrderBy(top => top.Created);

            var tasks = (from tg in module.TopicGroups
                         from to in tg.Topics
                         from ta in to.Tasks
                         select ta).OrderBy(tas => tas.Created);

            if (ExtensionMethods.IsNullOrEmpty(tasks) && ExtensionMethods.IsNullOrEmpty(topics))
            {
                // both are empty, return nothing
                return null;
            }
            if (ExtensionMethods.IsNullOrEmpty(topics))
            {
                // set the most recent task
                return tasks.Last().Created;
            }
            if (ExtensionMethods.IsNullOrEmpty(tasks))
            {
                //set the most recent topic
                return topics.Last().Created;
            }

            //now that we know neither are empty
            if (!ExtensionMethods.IsNullOrEmpty(tasks) && !ExtensionMethods.IsNullOrEmpty(topics))
            {
                var difference = DateTime.Compare(
                    Convert.ToDateTime(tasks.Last().Created),
                    Convert.ToDateTime(topics.Last().Created));

                if (difference < 0)
                {
                    //tasks is earlier than topics
                    return tasks.First().Created;
                }
                else if (difference == 0)
                {
                    //they're both the same, send back either
                    return tasks.Last().Created;
                }
                else
                {
                    //topics is earlier than tasks
                    return topics.Last().Created;
                }
            }

            //compiler is complaining that not all paths return a value
            return null;
        }

        public int GetTopicTaskCount(Guid moduleId)
        {
            Module module = MContext.Module.Find(moduleId);

            var topicsCount = (from tg in module.TopicGroups
                               from to in tg.Topics
                               select to).Count();

            var tasksCount = (from tg in module.TopicGroups
                              from to in tg.Topics
                              from ta in to.Tasks
                              select ta).Count();

            return topicsCount + tasksCount;
        }

        [Obsolete]
        public int GetTopicCount(Guid moduleId)
        {
            Module module = MContext.Module.Find(moduleId);

            var topicsCount = (from tg in module.TopicGroups
                               from to in tg.Topics
                               select to).Count();
            return topicsCount;
        }

        [Obsolete]
        public int GetTaskCount(Guid moduleId)
        {
            Module module = MContext.Module.Find(moduleId);

            var tasksCount = (from tg in module.TopicGroups
                              from to in tg.Topics
                              from ta in to.Tasks
                              select ta).Count();

            return tasksCount;
        }


        public Module Clone(Guid moduleId)
        {
            var moduleSource =
                GetDeep(moduleId);
            _logger.Debug("Creating Package - Module - Get Deep");

            return Clone(moduleSource);

        }

        [Obsolete]
        public Module Clone(int moduleReferenceId)
        {
            var moduleSource = GetDeep(m => m.ReferenceId == moduleReferenceId);

            return Clone(moduleSource);
        }

        private Module GetDeep(Guid moduleId)
        {
            return GetDeep(m => m.ID == moduleId);
        }

        private Module GetDeep(Expression<Func<Module, bool>> expression)
        {
            var result = MContext.Module.Where(expression).
                     Include("TopicGroups.Topics.Tasks.Attachments.Data").
                     Include("TopicGroups.Topics.Tasks.VoiceMessages.Data").
                     Include("TopicGroups.Topics.Tasks.DocumentLibraries.Attachment").
                     Include("TopicGroups.Topics.Attachments.Data").
                     Include("TopicGroups.Topics.VoiceMessages.Data").
                     Include("TopicGroups.Topics.DocumentLibraries.Attachment").
                     FirstOrDefault();

            return result;
        }

        private Module Clone(Module moduleSource)
        {
            return moduleSource.DeepClone();
        }

        public Module GetFrameworkModel(Module.ModuleType type)
        {
            var module = GetDeep(m => m.IsFrameworkDefault && m.Type == type);

            module.TopicGroups =
                SortTopicGroups(module.TopicGroups.AsQueryable()).ToList(); // todo: revise

            return module;
        }

        [Obsolete]
        public List<Module> GetAllFrameworkModules()
        {
            var modules =
                MContext.Module.Where(m => m.IsFrameworkDefault);

            return modules.ToList();
        }

        [Obsolete]
        public string ExportToXml(Guid moduleId)
        {
            Module module =
                Clone(moduleId);

            string xml =
                SerializeToXML<Module>(module);

            return xml;
        }

        [Obsolete]
        public void ImportFromXml(string xml)
        {
            Module module =
                Deserialize<Module>(xml);

            module.Notes = "Imported";

            MContext.Module.Add(module);

            MContext.SaveChanges();
        }

        #endregion
    }
}
