﻿using System;
using MomentumPlus.Activate.Services.Helpers;

namespace MomentumPlus.Activate.Services.Extensions
{
    public static class DateTimeExtensions
    {
        /// <summary>
        ///     Return the previous business day if the date falls on a weekend.
        ///     Otherwise returns the date itself.
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static DateTime PreviousBusinessDayOrSelf(this DateTime dateTime)
        {
            var date = dateTime;

            while (date.IsWeekend())
            {
                date = date.AddDays(-1);
            }

            return date;
        }

        /// <summary>
        ///     Return the next business day if the date falls on a weekend.
        ///     Otherwise returns the date itself.
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static DateTime NextBusinessDayOrSelf(this DateTime dateTime)
        {
            var date = dateTime;

            //while (date.IsWeekend())
            //{
            //    date = date.AddDays(1);
            //}

            while (date.IsSaturday())
            {
                date = date.AddDays(-1);
            }

            while (date.IsSunday())
            {
                date = date.AddDays(1);
            }

            return date;
        }

        /// <summary>
        /// Get the date of the next Friday or the given date
        /// if it is a Friday.
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static DateTime Friday(this DateTime dateTime)
        {

            var date = dateTime;

            while (date.DayOfWeek != DayOfWeek.Friday)
            {
                date = date.AddDays(1);
            }

            return date;

        }

        /// <summary>
        /// Get the number of days in the year of the given date.
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static int GetDaysInYear(this DateTime dateTime)
        {
            var currentYear = dateTime.Year;

            var thisYear = new DateTime(currentYear, 1, 1);
            var nextYear = new DateTime(currentYear + 1, 1, 1);

            return (nextYear - thisYear).Days;
        }

        /// <summary>
        ///     Returns true if the date falls on a Saturday or Sunday.
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        private static bool IsWeekend(this DateTime dateTime)
        {
            return dateTime.DayOfWeek == DayOfWeek.Saturday || dateTime.DayOfWeek == DayOfWeek.Sunday;
        }


        /// <summary>
        ///     Returns true if the date falls on a Saturday.
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        private static bool IsSaturday(this DateTime dateTime)
        {
            return dateTime.DayOfWeek == DayOfWeek.Saturday;
        }


        /// <summary>
        ///     Returns true if the date falls on a Sunday.
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        private static bool IsSunday(this DateTime dateTime)
        {
            return dateTime.DayOfWeek == DayOfWeek.Sunday;
        }


    }
}