﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace MomentumPlus.Activate.Services.Extensions
{
    public static class EnumExtensions
    {
        public static string GetEnumDescription<TEnum>(this TEnum value) where TEnum : struct, IConvertible
        {
            Type type = typeof(TEnum);

            if (!type.IsEnum)
                throw new ArgumentException("TEnum must be an enumerated type");

            string name = value.ToString();

            FieldInfo field = type.GetField(name);
            object[] customAttribute = field.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return customAttribute.Length > 0 ? ((DescriptionAttribute)customAttribute[0]).Description : name;
        }
    }
}
