﻿using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Activate.Services.Interfaces;
using System.Data.Entity;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Services
{
    /// <summary>
    /// Voice Message Service
    /// </summary>
    public class VoiceMessageService : ServiceBase, IVoiceMessageService
    {
        public VoiceMessageService(MomentumContext context) : base(context) { }

        /// <summary>
        /// Get all Voice Messages
        /// </summary>
        /// <returns></returns>
        public IEnumerable<VoiceMessage> GetAll()
        {
            var voiceMessages =
                MContext.VoiceMessages.Where(m => !m.Deleted.HasValue);
            return voiceMessages;
        }

        /// <summary>
        /// Get Voice Message by Id
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public VoiceMessage GetByID(Guid ID)
        {
            var voiceMessage =
                MContext.VoiceMessages.Find(ID);

            return voiceMessage;
        }

        /// <summary>
        /// Add Voice Message
        /// </summary>
        /// <param name="voiceMessage"></param>
        /// <returns></returns>
        public VoiceMessage Add(VoiceMessage voiceMessage)
        {
            MContext.VoiceMessages.Add(voiceMessage);

            MContext.SaveChanges();

            return voiceMessage;
        }

        /// <summary>
        /// Update Voice Message
        /// </summary>
        /// <param name="voiceMessage"></param>
        /// <returns></returns>
        public VoiceMessage Update(VoiceMessage voiceMessage)
        {
            MContext.Entry(voiceMessage).State = EntityState.Modified;
            MContext.SaveChanges();
            
            return voiceMessage;
        }

        /// <summary>
        /// Get Voice Messages by Core Service Guid
        /// </summary>
        /// <param name="CoreServiceGuid"></param>
        /// <returns></returns>
        public VoiceMessage GetVoiceMessageByCoreServiceGuid(Guid CoreServiceGuid)
        {
            var voiceMessage =
                MContext.VoiceMessages.FirstOrDefault(f => f.CoreService_Guid == CoreServiceGuid);

            return voiceMessage;
        }

        /// <summary>
        /// Delete a Voice Message
        /// </summary>
        /// <param name="voiceMessageID"></param>
        public void Delete(Guid voiceMessageID)
        {
            var message = MContext.VoiceMessages.Find(voiceMessageID);
            MContext.VoiceMessages.Remove(message);

            MContext.SaveChanges();
        }

    }
}
