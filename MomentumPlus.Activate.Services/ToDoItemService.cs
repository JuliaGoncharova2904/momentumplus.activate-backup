﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Services.Helpers;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Services
{
    public class ToDoItemService : ServiceBase, IToDoItemService
    {
        public ToDoItemService(MomentumContext context) : base(context) { }

        /// <summary>
        /// Update TO DO item
        /// </summary>
        /// <param name="toDo"></param>
        /// <returns></returns>
        public ToDoItem Update(ToDoItem toDo)
        {
            MContext.Entry(toDo).State = EntityState.Modified;
            MContext.SaveChanges();
            return toDo;
        }

        /// <summary>
        /// Dismiss TO DO item
        /// </summary>
        /// <param name="toDoId"></param>
        public void Dismiss(Guid toDoId)
        {
            ToDoItem toDo = MContext.ToDoItem.Find(toDoId);
            toDo.DismissDate = SystemTime.Now;
            Update(toDo);
        }

        /// <summary>
        /// Get by Period Id
        /// </summary>
        /// <param name="periodId">Period Id</param>
        /// <returns></returns>
        public IEnumerable<ToDoItem> GetByPeriodId(Guid periodId)
        {
            return MContext.ToDoItem.Where(i => i.PeriodId == periodId);
        }

        /// <summary>
        /// Delete TO DO item
        /// </summary>
        /// <param name="toDoItem">toDoItem to delete</param>
        public void Delete(ToDoItem toDoItem)
        {
            MContext.ToDoItem.Remove(toDoItem);
            MContext.SaveChanges();
        }
    }
}