﻿using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Services
{
    /// <summary>
    /// Services
    /// </summary>
    public class RoleService : ServiceBase, IRoleService
    {
        public RoleService(MomentumContext context) : base(context) { }

        #region IRoleService Members

        /// <summary>
        /// Get all Roles
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Role> GetAll()
        {
            var roles = MContext.Role;

            return roles.ToList();
        }

        /// <summary>
        /// Gets Role by Id
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public Role GetByRoleId(Guid roleId)
        {
            var role = MContext.Role.Find(roleId);

            return role;
        }

        #endregion
    }
}
