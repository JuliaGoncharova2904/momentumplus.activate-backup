﻿using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Activate.Services.Interfaces;
using System.Data.Entity;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models.Moderation;

namespace MomentumPlus.Activate.Services
{
    /// <summary>
    /// Moderation Service
    /// </summary>
    public class ModerationService : ServiceBase, IModerationService
    {

        public ModerationService(MomentumContext context) : base(context) { }

        #region IModerationService Members

        /// <summary>
        /// Add a new Moderated entity to the database
        /// </summary>
        /// <param name="moderated">The Moderated object</param>
        /// <returns>The added Moderated object</returns>
        public Moderated AddModerated(Moderated moderated)
        {
            moderated = MContext.Moderated.Add(moderated);
            MContext.SaveChanges();

            return moderated;
        }

        /// <summary>
        /// Update an existing Moderated entity to the database
        /// </summary>
        /// <param name="moderated">The Moderated object</param>
        /// <returns>The updated Moderated object</returns>
        public Moderated UpdateModerated(Moderated moderated)
        {
            MContext.Entry(moderated).State = EntityState.Modified;
            MContext.SaveChanges();

            return moderated;
        }

        /// <summary>
        /// Delete a Moderated entity from the database
        /// </summary>
        /// <param name="moderated">The Moderated entity</param>
        public void RemoveModerated(Moderated moderated)
        {
            MContext.Moderated.Remove(moderated);
            MContext.SaveChanges();
        }

        /// <summary>
        /// Get a Moderated entity by ID
        /// </summary>
        /// <param name="id">The entity GUID</param>
        /// <returns>The Moderated entity</returns>
        public Moderated GetById(Guid id)
        {
            return MContext.Moderated.Find(id);
        }

        /// <summary>
        /// Get a specific type of Moderated entity
        /// </summary>
        /// <typeparam name="T">The entity type</typeparam>
        /// <param name="id">The entity GUID</param>
        /// <returns></returns>
        public T GetById<T>(Guid id) where T : Moderated
        {
            return MContext.Moderated.OfType<T>().FirstOrDefault(m => m.ID == id);
        }

        /// <summary>
        /// Get all active, unapproved Moderated entities of a specific type in a Package
        /// </summary>
        /// <typeparam name="T">The entity type</typeparam>
        /// <param name="packageId">The Package GUID</param>
        /// <returns></returns>
        public IEnumerable<T> GetAllNotApproved<T>(Guid packageId) where T : Moderated
        {
            return
                MContext.Moderated.OfType<T>()
                    .Where(m => m.PackageId == packageId && !m.Deleted.HasValue)
                    .Where(Moderated.NotApproved<T>());
        }

        /// <summary>
        /// Get all active, unapproved Moderated entities of a specific type in a Package that can be moderated
        /// by a specific User.
        /// </summary>
        /// <typeparam name="T">The entity type</typeparam>
        /// <param name="userId">The User GUID</param>
        /// <param name="packageId">The Package GUID</param>
        /// <returns></returns>
        public IEnumerable<T> GetAllNotModerated<T>(Guid? userId = null, Guid? packageId = null) where T : Moderated
        {
            IEnumerable<Guid> userCanModeratePackages = null;

            if (packageId.HasValue)
            {
                userCanModeratePackages = new[] { packageId.Value };
            }
            else if(userId.HasValue)
            {
                userCanModeratePackages = MContext.Package.Where( p => !p.Deleted.HasValue && (p.UserHRManagerId == userId.Value || p.UserLineManagerId == userId.Value) )
                                                                      .Select( p => p.ID);
            }

            IQueryable<T> result = null;
            
            if(userCanModeratePackages != null)
            {
                result = MContext.Moderated.OfType<T>().Where(e => !e.Deleted.HasValue && !e.Moderations.Any() && userCanModeratePackages.Contains(e.PackageId.Value));
            }
            else
            {
                result = MContext.Moderated.OfType<T>().Where(e => !e.Deleted.HasValue && !e.Moderations.Any());
            }

            return result;
        }

        /// <summary>
        /// Get all active, ready for review entities in a Package that can be reviewed by a specific User
        /// </summary>
        /// <typeparam name="T">A type that inherits from Reviewable</typeparam>
        /// <param name="userId">The User GUID</param>
        /// <param name="packageId">The Package GUID</param>
        /// <returns></returns>
        public IEnumerable<T> GetReadyForReview<T>(Guid userId, Guid? packageId = null) where T : Reviewable
        {
            IEnumerable<Guid> packageIds = null;

            if(!packageId.HasValue)
            {
                packageIds = MContext.Package.Where( p => !p.Deleted.HasValue && (p.UserHRManagerId == userId || p.UserLineManagerId == userId && p.Handover != null))
                                                                        .Select( p => p.ID);
            }
            else
            {
                packageIds = new[] { packageId.Value };
            }


            var result = MContext.Moderated.OfType<T>().Where(e => !e.Deleted.HasValue && e.ReadyForReview && !e.Moderations.Any() && packageIds.Contains(e.PackageId.Value));

            return result;
        }

        /// <summary>
        /// Get all not moderated entities for a given user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<Moderated> GetAllNotModerated(Guid? userId = null)
        {
            return GetAllNotModerated<Moderated>(userId);
        }

        /// <summary>
        /// Add a new Moderation for a Moderated entity
        /// </summary>
        /// <param name="entity">The Moderated entity</param>
        /// <param name="moderation">The Moderation object</param>
        public void AddModeration(Moderated entity, Moderation moderation)
        {
            entity.Moderations.Add(moderation);

            MContext.Entry(entity).State = EntityState.Modified;
            MContext.SaveChanges();
        }

        /// <summary>
        /// Update an existing Moderation in the database
        /// </summary>
        /// <param name="moderation"></param>
        /// <returns></returns>
        public Moderation UpdateModeration(Moderation moderation)
        {
            MContext.Entry(moderation).State = EntityState.Modified;
            MContext.SaveChanges();

            return moderation;
        }

        /// <summary>
        /// Delete a Moderation from the database
        /// </summary>
        /// <param name="moderation"></param>
        public void RemoveModeration(Moderation moderation)
        {
            MContext.Moderation.Remove(moderation);
            MContext.SaveChanges();
        }

        /// <summary>
        /// Get all Moderated entities in a Package
        /// </summary>
        /// <param name="packageId">The Package GUID</param>
        /// <param name="deleted">If true will return only deleted entities</param>
        /// <returns></returns>
        public IQueryable<Moderated> GetAll(Guid packageId, bool deleted = false)
        {
            return MContext.Moderated.Where(m => m.PackageId == packageId && m.Deleted.HasValue == deleted);
        }

        /// <summary>
        /// Get the total number of Moderated entities in a Package
        /// </summary>
        /// <param name="packageId"></param>
        /// <returns></returns>
        public int CountAllModerated(Guid packageId)
        {
            return GetAll(packageId).Count();
        }

        /// <summary>
        /// Get the total number of approved entities in a Package
        /// </summary>
        /// <param name="packageId"></param>
        /// <returns></returns>
        public int CountApproved(Guid packageId)
        {
            return GetAll(packageId).Count(Moderated.Approved());
        }

        /// <summary>
        /// Get the total number of rejected entities in a Package
        /// </summary>
        /// <param name="packageId"></param>
        /// <returns></returns>
        public int CountRejected(Guid packageId)
        {
            return GetAll(packageId).Count(Moderated.Declined());
        }

        #endregion
    }
}
