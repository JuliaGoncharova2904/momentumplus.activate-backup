﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Services.Extensions;
using MomentumPlus.Activate.Services.Helpers;
using System.Web.Mvc;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models.People;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Services
{
    /// <summary>
    /// Package Service
    /// </summary>
    public class PackageService : ServiceBase, IPackageService
    {
        private readonly IMeetingService MeetingService;

        public PackageService(MomentumContext context, IMeetingService meetingService, User user)
            : base(context, user)
        {
            MeetingService = meetingService;
        }

        #region IPackageService Members

        public Package GetPackageByExitSurveyId(Guid exitSurveyId)
        {
            ExitSurveyTemplate exitSurveyTemplate = MContext.ExitSurveyTemplate.Where(t => t.ExitSurveyId == exitSurveyId).FirstOrDefault();

            if (exitSurveyTemplate != null)
            {
                return MContext.Package.Where(p => p.ExitSurveyTemplateId == exitSurveyTemplate.ID).FirstOrDefault();
            }

            return null;
        }

        /// <summary>
        /// Gets all active Packages
        /// </summary>
        /// <param name="deleted">Include deleted Packages</param>
        /// <returns>Packages</returns>
        public IEnumerable<Package> GetAll(bool deleted = false)
        {
            var packages =
               MContext.Package.Where(p => p.Deleted.HasValue == deleted);

            return packages.ToList();
        }

        /// <summary>
        /// Gets all active Packages where ExitSurvey = "true"
        /// </summary>
        /// <param name="deleted">Include deleted Packages</param>
        /// <returns>Packages</returns>
        public IEnumerable<Package> GetAllExit(bool deleted = false)
        {
            var packages =
                MContext.Package.Where(p => p.Deleted.HasValue == deleted && p.ExitSurvey.HasValue && p.ExitSurvey.Value);
            return packages.ToList();
        }


        /// <summary>
        /// Gets all Packages for the current user
        /// </summary>
        /// <param name="deleted">Include deleted Packages</param>
        /// <returns>Packages</returns>
        public IEnumerable<Package> GetAllWithPermissions(bool deleted = false)
        {
            var packages =
                GetAllQueryableForCurrentUser(deleted);
            return packages;
        }

        /// <summary>
        /// Get Packages by the Package owner user Id
        /// </summary>
        /// <param name="id">User Id filter</param>
        /// <param name="deleted">Include deleted Packages</param>
        /// <returns>Packages</returns>
        public IEnumerable<Package> GetPackagesByUserId(Guid id, bool deleted = false)
        {
            var packages =
                MContext.Package.Where(p => p.Deleted.HasValue == deleted && p.UserOwner.ID == id).ToList();

            return packages;
        }

        /// <summary>
        /// Get Packages by the Package owner user Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="packageStatus">Package status</param>
        /// <param name="deleted">Include deleted Packages</param>
        /// <returns>Packages</returns>
        public IEnumerable<Package> GetPackagesByUserId(Guid id, Package.Status? packageStatus, bool deleted = false)
        {
            var packages =
                  GetAllQueryableForCurrentUser(packageStatus, deleted).Where(p => p.UserOwnerId == id).ToList();

            return packages;
        }

        /// <summary>
        /// Get all Packages, eager loads down to Task level
        /// </summary>
        /// <param name="deleted">Include deleted packages</param>
        /// <param name="userOverride">User filter</param>
        /// <returns>Packages</returns>
        public IEnumerable<Package> GetAllEagerLoad(bool deleted = false, User userOverride = null)
        {
            var packages =
                GetAllQueryableForCurrentUser(deleted, userOverride).Include("Modules.TopicGroups.Topics.Tasks.Attachments").Include("Modules.TopicGroups.Topics.Tasks.VoiceMessages").Include("Trigger.PickupPackage.UserOwner").Include("UserOwner").Include("Contacts").Include("Modules.TopicGroups.Topics.Fields.Question").Include("Modules.TopicGroups.Topics.Fields.Answers").Include("Position").Include("Workplace").ToList();

            return packages;
        }

        /// <summary>
        /// Gets all Packages by User
        /// </summary>
        /// <param name="userOverride">User</param>
        /// <returns>Packages</returns>
        public IEnumerable<Package> GetAllPackagesByUser(User userOverride)
        {
            var packages =
                GetAllQueryableForCurrentUser(false, userOverride);

            return packages;
        }

        /// <summary>
        /// Gets all Packages filtered by Position Id
        /// </summary>
        /// <param name="positionId">Position Id</param>
        /// <param name="packageStatus">Package status filter</param>
        /// <param name="deleted">Include deleted Packages</param>
        /// <returns>Packages</returns>
        public IEnumerable<Package> GetByPositionId(Guid positionId, Package.Status? packageStatus, bool deleted = false)
        {
            var packages =
                    GetAllQueryableForCurrentUser(packageStatus, deleted);

            packages = packages.Where(p => p.PositionId == positionId);

            return packages;
        }

        /// <summary>
        /// Filtered by current user
        /// </summary>
        /// <param name="status">Package status filter</param>
        /// <param name="deleted"></param>
        /// <param name="userOverride">User</param>
        /// <returns>Packages</returns>
        public IEnumerable<Package> GetByPackageStatus(Package.Status status, bool deleted = false, User userOverride = null)
        {
            return GetAllQueryableForCurrentUser(status, deleted, userOverride);
        }

        public bool IsOngoing(Guid packageId)
        {
            var package = GetByPackageId(packageId);

            return package.PackageStatus == Package.Status.InProgress ? true : false;
        }

        public IEnumerable<Package> GetAllFinalisePackage(bool deleted = false)
        {
            var packages = GetAllQueryableForCurrentUser(deleted);

            packages = packages.Where(p => p.Handover.LeavingPeriod.Locked.HasValue && p.Handover.LeavingPeriod.LastDay.Value < SystemTime.Now);

            return packages;
        }

        /// <summary>
        /// Gets Packages by Workplace Id
        /// </summary>
        /// <param name="workplaceId">Workplace filter Id</param>
        /// <param name="packageStatus">Package status Id</param>
        /// <param name="deleted">Include deleted Packages</param>
        /// <returns>Packages</returns>
        public IEnumerable<Package> GetByWorkplaceId(Guid workplaceId, Package.Status? packageStatus, bool deleted = false)
        {
            var packages =
                  GetAllQueryableForCurrentUser(packageStatus, deleted);

            packages = packages.Where(p => p.WorkplaceId == workplaceId);

            return packages;
        }

        /// <summary>
        /// Gets the abcestor Packages
        /// </summary>
        /// <param name="package">Package to get Ancestor</param>
        /// <returns>Packages</returns>
        public IEnumerable<Package> GetAncestorPackages(Package package)
        {
            var ancestors = new List<Package>();

            if (package.AncestorPackageId.HasValue)
            {
                var ancestor = GetByPackageId(package.AncestorPackageId.Value);
                ancestors.Add(ancestor);
                ancestors.AddRange(GetAncestorPackages(ancestor));
            }

            return ancestors;
        }

        /// <summary>
        /// Counts the number of Packages
        /// </summary>
        /// <param name="deleted">Include deleted</param>
        /// <returns>Number of Packages</returns>
        public int Count(bool deleted = false)
        {
            var packages = GetAllQueryableForCurrentUser(deleted);


            var handoverService = DependencyResolver.Current.GetService<IHandoverService>();

            foreach (var pack in packages)
            {
                if (handoverService.HasLeft(pack.ID))
                {
                    packages = packages.Where(p => p.ID != pack.ID);
                }
            }

            return packages.Count();
        }

        /// <summary>
        /// Gets a Package by Id
        /// </summary>
        /// <param name="packageId"></param>
        /// <returns></returns>
        public Package GetByPackageId(Guid packageId)
        {
            Package package =
                MContext.Package.Find(packageId);

            return package;
        }

        /// <summary>
        /// Gets Package by Task Id
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public Package GetByTaskId(Guid taskId)
        {
            var packages =
                MContext.Package.Where(p => !p.Deleted.HasValue);

            Package package = packages.Where(p => p.Modules.Any(m => m.TopicGroups.Any(tg => tg.Topics.Any(to => to.Tasks.Any(t => t.ID == taskId))))).FirstOrDefault() ?? 
                packages.Where(p => p.Contacts.Any(c => c.Tasks.Any(t => t.ID == taskId))).FirstOrDefault() ??
                packages.Where(p => p.Projects.Any(pr => pr.Tasks.Any(t => t.ID == taskId))).FirstOrDefault();

            return package;
        }

        /// <summary>
        /// Gets Package by Topic Id
        /// </summary>
        /// <param name="topicId">Topic id that belongs to the desired Package</param>
        /// <returns>Package</returns>
        public Package GetByTopicId(Guid topicId)
        {
            var packages =
                MContext.Package.Where(p => !p.Deleted.HasValue);

            Package package = (from p in packages
                               from m in p.Modules
                               from tg in m.TopicGroups
                               from to in tg.Topics
                               where to.ID == topicId
                               select p).FirstOrDefault();


            return package;
        }

        /// <summary>
        /// Gets Package by Topic Group Id
        /// </summary>
        /// <param name="topicGroupId">Topic Group id that belongs to the desired Package</param>
        /// <returns>Package</returns>
        public Package GetByTopicGroupId(Guid topicGroupId)
        {
            var packages =
                MContext.Package.Where(p => !p.Deleted.HasValue);

            Package package = (from p in packages
                               from m in p.Modules
                               from tg in m.TopicGroups
                               where tg.ID == topicGroupId
                               select p).FirstOrDefault();

            return package;
        }

        /// <summary>
        /// Gets Package by Module Id
        /// </summary>
        /// <param name="moduleId">Module id that belongs to the desired Package</param>
        /// <returns></returns>
        public Package GetByModuleId(Guid moduleId)
        {
            var packages =
                MContext.Package.Where(p => !p.Deleted.HasValue);

            Package package = (from p in packages
                               from m in p.Modules
                               where m.ID == moduleId
                               select p).FirstOrDefault();

            return package;
        }

        /// <summary>
        /// Gets Package by Contact Id
        /// </summary>
        /// <param name="contactId">Contact Id that belongs to the desired Package</param>
        /// <returns>Package</returns>
        public Package GetByContactId(Guid contactId)
        {
            var packages =
               MContext.Package.Where(p => !p.Deleted.HasValue);

            Package package = (from p in packages
                               from c in p.Contacts
                               where c.ID == contactId
                               select p).FirstOrDefault();

            return package;
        }

        /// <summary>
        /// Gets Package by Meeting Id
        /// </summary>
        /// <param name="meetingId">Meeting that belongs to the desired Package</param>
        /// <returns>Package</returns>
        public Package GetByMeetingId(Guid meetingId)
        {
            var packages =
               MContext.Package.Where(p => !p.Deleted.HasValue);

            Package package = (from p in packages
                               from c in p.Contacts
                               from m in c.Meetings
                               where m.ID == meetingId
                               select p).FirstOrDefault();

            return package;
        }

        /// <summary>
        /// Gets Package by Project Id
        /// </summary>
        /// <param name="projectInstanceId">Project Id that belongs to the desired Package</param>
        /// <returns>Package</returns>
        public Package GetByProjectInstanceId(Guid projectInstanceId)
        {
            var packages =
               MContext.Package.Where(p => !p.Deleted.HasValue);

            Package package = (from p in packages
                               from i in p.Projects
                               where i.ID == projectInstanceId
                               select p).FirstOrDefault();

            return package;
        }

        /// <summary>
        /// Gets Package by Contact that is contained within a Package
        /// </summary>
        /// <param name="staffId">Contact Id</param>
        /// <returns>Package Id</returns>
        public Package GetByStaffId(Guid staffId)
        {
            var packages =
               MContext.Package.Where(p => !p.Deleted.HasValue);

            Package package = (from p in packages
                               from c in p.Contacts
                               where c.ID == staffId
                               select p).FirstOrDefault();

            return package;
        }

        /// <summary>
        /// Creates a new Package based on the Template provided
        /// </summary>
        /// <param name="package">Package container</param>
        /// <param name="templateId">Template to generate Package from</param>
        /// <param name="projectIds">Ids of Projects to add to the Package</param>
        /// <param name="contactIds">Ids of Contacts to add to the Package</param>
        /// <returns></returns>
        public Package Add(Package package, Guid templateId, IEnumerable<Guid> projectIds = null, IEnumerable<Guid> contactIds = null)
        {
            package.ID = Guid.NewGuid();

            ModuleService moduleService = new ModuleService(MContext);
            TemplateService templateService = new TemplateService(MContext);

            // Get the Template to generate the Package from
            var template = 
                templateService.GetByTemplateId(templateId);

            // Generate Critial Module from template
            var templateItemsCritical = 
                template.TemplateItemsCritical;

            if (templateItemsCritical.Any())
            {
                var moduleFramework =
                    moduleService.GetFrameworkModel(Module.ModuleType.Critical);

                var moduleCritical =
                    moduleService.Clone(moduleFramework.ID);

                moduleCritical.IsFrameworkDefault = false;

                FilterFrameworkModel(templateItemsCritical.ToList(), ref moduleCritical);

                package.Modules.Add(moduleCritical);
            }

            // Generate Experience Module from template
            var templateItemsExperiences = 
                template.TemplateItemsExperiences;

            if (templateItemsExperiences.Any())
            {
                var moduleFramework =
                        moduleService.GetFrameworkModel(Module.ModuleType.Experiences);

                var moduleExperiences =
                    moduleService.Clone(moduleFramework.ID);

                moduleExperiences.IsFrameworkDefault = false;

                FilterFrameworkModel(templateItemsExperiences.ToList(), ref moduleExperiences);

                package.Modules.Add(moduleExperiences);
            }

            // Generate Project Module from template
            if (projectIds != null)
            {
                var projects = MContext.Project.Where(p => !p.Deleted.HasValue && projectIds.Contains(p.ID)).ToList();

                package.Projects = projects.Select(p =>
                {
                    var i = p.CreateInstance(package.ID);
                    return i;
                }).ToList();

                var contacts = projects.Select(p => p.ProjectManager);
                contactIds = contactIds ?? new List<Guid>();

                foreach (var contact in contacts)
                {
                    if (!contactIds.Contains(contact.ID))
                    {
                        (contactIds as List<Guid>).Add(contact.ID);
                    
                    }
                }
            }

            // Generate Relationships via the Contact Ids provided
            if (contactIds != null)
            {
                package.Contacts = MContext.Contact.Where(c => !c.Deleted.HasValue && contactIds.Contains(c.ID))
                                                               .ToList()
                                                               .Select(c =>
                                                               {
                                                                   c = c.DeepClone();
                                                                   c.Package = null;
                                                                   c.PackageId = package.ID;
                                                                   return c;
                                                               })
                                                               .ToList();
            }

            // Clone Functions (Pro-link and attach them to Packages
            var frameworkFunctions = MContext.Function
                .Include(f => f.ProcessGroups)
                .Where(f => !f.Deleted.HasValue && f.IsFramework)
                .AsNoTracking()
                .ToList();

            var clonedFunctions = 
                frameworkFunctions.Select(f => f.DeepClone()).ToList();

            foreach (var function in clonedFunctions)
            {
                function.Attach(package);
            }

            // Set Module enabed flags (for tabs)
            package.ExperiencesEnabled = template.ExperiencesEnabled;
            package.PeopleManagementEnabled = template.PeopleManagementEnabled;
            package.ProjectsEnabled = template.ProjectsEnabled;

            ReattachDocumentItems(package);

            // Create Package and commit 
            package = MContext.Package.Add(package);
            MContext.SaveChanges();

            return package;
        }

        public Package GetByProjectId(Guid projectId)
        {
            var packages =
                MContext.Package.Where(p => !p.Deleted.HasValue);

            Package package = (from p in packages
                               from pr in p.Projects
                               where pr.ID == projectId
                               select p).FirstOrDefault();

            return package;
        }

        #region Reattach

        /// <summary>
        /// Reattaches Document Items detached during cloning
        /// This is a temporally fix which needs to be address properly be the solution
        /// Ideally the repo should detect such items and attach them accordingly
        /// </summary>
        /// <param name="package"></param>
        private void ReattachDocumentItems(Package package)
        {
            foreach (Module module in package.Modules)
            {
                foreach (TopicGroup topicGroup in module.TopicGroups)
                {
                    foreach (Topic topic in topicGroup.Topics)
                    {
                        ReattachDocumentLibraries(topic);

                        foreach (Task task in topic.Tasks)
                        {
                            ReattachDocumentLibraries(task);
                        }
                    }
                }
            }
        }

        private void ReattachDocumentLibraries(Topic topic)
        {
            if (topic.DocumentLibraries != null && topic.DocumentLibraries.Count > 0)
            {
                List<DocumentLibrary> items = new List<DocumentLibrary>();

                foreach (DocumentLibrary item in topic.DocumentLibraries)
                {
                    items.Add(MContext.DocumentLibrary.Find(item.ID));
                }

                topic.DocumentLibraries.Clear();

                foreach (DocumentLibrary item in items)
                {
                    topic.DocumentLibraries.Add(item);
                }
            }
        }

        private void ReattachDocumentLibraries(Task task)
        {
            if (task.DocumentLibraries != null && task.DocumentLibraries.Count > 0)
            {
                List<DocumentLibrary> items = new List<DocumentLibrary>();

                foreach (DocumentLibrary item in task.DocumentLibraries)
                {
                    items.Add(MContext.DocumentLibrary.Find(item.ID));
                }

                task.DocumentLibraries.Clear();

                foreach (DocumentLibrary item in items)
                {
                    task.DocumentLibraries.Add(item);
                }
            }
        }

        #endregion

        /// <summary>
        /// Filters the Module by the Template Items
        /// </summary>
        /// <param name="templateItems">Template items used to filter the module</param>
        /// <param name="moduleDefault">Module to filter</param>
        public void FilterFrameworkModel(List<TemplateItem> templateItems, ref Module moduleDefault)
        {
            // Remove cloned topic groups that are 
            // not in the template items
            var topicGroupItems =
                 templateItems.Where(t => t.Type == TemplateItem.TemplateItemType.TopicGroup);

            // select all items in topioc groups where items
            var topicGroupsToRemove = (from tg in moduleDefault.TopicGroups
                                       where topicGroupItems.All(r => r.SourceReferenceId != tg.ReferenceId)
                                       select tg).ToList();

            for (int i = topicGroupsToRemove.Count() - 1; i >= 0; i--)
            {
                moduleDefault.TopicGroups.Remove(topicGroupsToRemove.ToList()[i]);
            }


            // Remove cloned topics that are 
            // not in the template items
            var topicItems =
               templateItems.Where(t => t.Type == TemplateItem.TemplateItemType.Topic);

            // select all items in topioc groups where items
            var topicsToRemove = (from tg in moduleDefault.TopicGroups
                                  from to in tg.Topics
                                  where topicItems.All(r => r.SourceReferenceId != to.ReferenceId)
                                  select to).ToList();

            foreach (TopicGroup topicGroup in moduleDefault.TopicGroups)
            {
                for (int i = topicsToRemove.Count() - 1; i >= 0; i--)
                {
                    topicGroup.Topics.Remove(topicsToRemove[i]);
                }

            }

            // Remove cloned tasks that are 
            // not in the template items
            var taskItems =
              templateItems.Where(t => t.Type == TemplateItem.TemplateItemType.Task);

            // select all items in topioc groups where items
            var tasksToRemove = (from tg in moduleDefault.TopicGroups
                                 from to in tg.Topics
                                 from ta in to.Tasks
                                 where taskItems.All(r => r.SourceReferenceId != ta.ReferenceId)
                                 select ta).ToList();

            foreach (TopicGroup topicGroup in moduleDefault.TopicGroups)
            {
                foreach (Topic topic in topicGroup.Topics)
                {
                    for (int i = tasksToRemove.Count() - 1; i >= 0; i--)
                    {
                        topic.Tasks.Remove(tasksToRemove[i]);
                    }
                }
            }
        }

        /// <summary>
        /// Updates a Package
        /// </summary>
        /// <param name="package">Package to update</param>
        /// <returns>Package</returns>
        public Package Update(Package package)
        {
            MContext.Entry(package).State = EntityState.Modified;

            MContext.SaveChanges();

            return package;
        }

        /// <summary>
        /// Retired a Package
        /// </summary>
        /// <param name="packageId">Package Id of Package to retire</param>
        public void Retire(Guid packageId)
        {
            UpdateDeletedFields(
                DateTime.Now, packageId);
        }

        /// <summary>
        /// Restores a Package from a retired state
        /// </summary>
        /// <param name="packageId">Id of Package to restore</param>
        public void Restore(Guid packageId)
        {
            UpdateDeletedFields(
                null, packageId);
        }


        /// <summary>
        /// Gets that Package name
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string GetPackageName(Guid id)
        {
            var package = GetByPackageId(id);

            return package.DisplayName;
        }

        /// <summary>
        /// Returns To Do items
        /// </summary>
        /// <param name="packageId">Package Id of the to do list items</param>
        /// <param name="pageNo">Page number</param>
        /// <param name="toDoPageSize">Page size</param>
        /// <param name="dismissed">Return dismissed</param>
        /// <returns></returns>
        public IEnumerable<ToDoItem> GetToDoItems(Guid packageId, int pageNo, int toDoPageSize, bool dismissed = false)
        {
            var toDoList = MContext.ToDoItem.Where(t => t.PackageId == packageId &&
                                                   t.VisibleDate <= SystemTime.Now &&
                                                   t.DismissDate.HasValue == dismissed).OrderByDescending(t => t.VisibleDate).ThenByDescending(t => t.Title).ToList();

            return toDoList.Skip(toDoPageSize * (pageNo - 1)).Take(toDoPageSize).Any() ? toDoList.Skip(toDoPageSize * (pageNo - 1)).Take(toDoPageSize) : toDoList.Skip(toDoPageSize * (pageNo - 2)).Take(toDoPageSize);
        }

        /// <summary>
        /// Gets a complate list of To Do list items by Package Id
        /// </summary>
        /// <param name="packageId">Package Id of the to do list items</param>
        /// <returns>List of ToDo items</returns>
        public IEnumerable<ToDoItem> GetAllToDoItems(Guid packageId)
        {
            var toDoList = MContext.ToDoItem.Where(t => t.PackageId == packageId &&
                                                        t.VisibleDate <= SystemTime.Now)
                                                        .OrderByDescending(t => t.VisibleDate);

            return toDoList.ToList();
        }

        /// <summary>
        /// Returns a count of To Do list items
        /// </summary>
        /// <param name="packageId">Package Id of the to do list items</param>
        /// <param name="dismissed">Return dismissed</param>
        /// <returns>Count of To Do list items</returns>
        public int GetToDoItemsCount(Guid packageId, bool dismissed = false)
        {
            return MContext.ToDoItem.Count(t => t.PackageId == packageId &&
                                                   t.VisibleDate <= SystemTime.Now &&
                                                   t.DismissDate.HasValue == dismissed);
        }

        /// <summary>
        /// Creates a new Package
        /// </summary>
        /// <returns></returns>
        public Package NewPackage()
        {
            return MContext.Package.Create();
        }

        /// <summary>
        /// Gets by Owner Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IEnumerable<Package> GetByUserOwnerId(Guid id)
        {
            return MContext.Package.Where(p => p.UserOwnerId == id);
        }

        #endregion

        #region private helper methods

        /// <summary>
        /// Retire/restore Package Id
        /// </summary>
        /// <param name="deleted">True to retire, False to restore</param>
        /// <param name="packageId">Package Id to retire/restore</param>
        private void UpdateDeletedFields(DateTime? deleted, Guid packageId)
        {
            var package =
                MContext.Package.Find(packageId);

            package.Deleted = deleted;

            foreach (Module module in package.Modules)
            {
                module.Deleted = deleted;

                foreach (TopicGroup topicGroup in module.TopicGroups)
                {
                    topicGroup.Deleted = deleted;

                    foreach (Topic topic in topicGroup.Topics)
                    {
                        topic.Deleted = deleted;

                        foreach (Attachment attachment in topic.Attachments)
                        {
                            attachment.Deleted = deleted;
                            if (attachment.Data != null)
                                attachment.Data.Deleted = deleted;
                        }

                        foreach (VoiceMessage voiceMessage in topic.VoiceMessages)
                        {
                            voiceMessage.Deleted = deleted;
                            if (voiceMessage.Data != null)
                                voiceMessage.Data.Deleted = deleted;
                        }

                        foreach (Task task in topic.Tasks)
                        {
                            task.Deleted = deleted;

                            foreach (Attachment attachment in task.Attachments)
                            {
                                attachment.Deleted = deleted;
                                if (attachment.Data != null)
                                    attachment.Data.Deleted = deleted;
                            }

                            foreach (VoiceMessage voiceMessage in task.VoiceMessages)
                            {
                                voiceMessage.Deleted = deleted;
                                if (voiceMessage.Data != null)
                                    voiceMessage.Data.Deleted = deleted;
                            }
                        }
                    }
                }
            }

            MContext.SaveChanges();
        }

        /// <summary>
        /// Gets a list of IQueryable Packages 
        /// </summary>
        /// <param name="deleted">Return deleted</param>
        /// <param name="userOverride">User Id to filter Packages by</param>
        /// <returns></returns>
        private IQueryable<Package> GetAllQueryableForCurrentUser(bool deleted = false, User userOverride = null)
        {
            return GetAllQueryableForCurrentUser(null, deleted, userOverride);
        }

        /// <summary>
        /// Gets all Packages based on current User Role
        /// - Administrator - All Packages
        /// - Executive - All Packages
        /// - iHandover Agent -  All Packages
        /// - iHandover Admin - All Packages
        /// - HR Manager - Packages where they are set as HR Manager
        /// - Line Manager - Packages where they are set as Line Manager
        /// - User - Only their own Packages
        /// </summary>
        /// <param name="packageStatus"></param>
        /// <param name="deleted"></param>
        /// <param name="userOverride"></param>
        /// <returns></returns>
        private IQueryable<Package> GetAllQueryableForCurrentUser(Package.Status? packageStatus, bool deleted = false, User userOverride = null)
        {
            ValidatePermissions();

            IQueryable<Package> packages = null;

            var user = User;

            if (userOverride != null)
            {
                user = userOverride;
            }

            switch (user.Role.RoleParsed)
            {
                case Role.RoleEnum.Administrator:
                    packages = MContext.Package.Where(p => p.Deleted.HasValue == deleted); // get everything
                    break;
                case Role.RoleEnum.Executive:
                    packages = MContext.Package.Where(p => p.Deleted.HasValue == deleted); // get everything
                    break; 
                case Role.RoleEnum.iHandoverAgent:
                    packages = MContext.Package.Where(p => p.Deleted.HasValue == deleted); // get everything
                    break;
                case Role.RoleEnum.iHandoverAdmin:
                    packages = MContext.Package.Where(p => p.Deleted.HasValue == deleted); // get everything
                    break;
                case Role.RoleEnum.HRManager:
                    packages = MContext.Package.Where(p => p.Deleted.HasValue == deleted && (p.UserHRManagerId == user.ID)); // only get Packages where they the HR Manager
                    break;
                case Role.RoleEnum.LineManager:
                    packages = MContext.Package.Where(p => p.Deleted.HasValue == deleted && (p.UserLineManagerId == user.ID)); // only get Packages reports where they are the Line Manager
                    break;
                case Role.RoleEnum.User:
                    packages = MContext.Package.Where(p => p.Deleted.HasValue == deleted && p.UserOwnerId == user.ID); // only get Packages where they are the owner
                    break;
                default:
                    throw new Exception(string.Format("Unknown Role '{0}'", user.Role.RoleName));
            }
            if (packageStatus.HasValue)
            {
                var onboarding = MContext.OnboardingPeriod.Where(o => o.Locked.HasValue && o.LastDay.Value >= SystemTime.Now).Select(o => o.PackageId);
                var leaving = MContext.LeavingPeriod.Where(o => o.Locked.HasValue).Select(o => o.ID);
                switch (packageStatus.Value)
                {
                    case Package.Status.New:
                        packages = packages.Where(p => onboarding.Contains(p.ID));
                        break;
                    case Package.Status.InProgress:
                        packages = packages.Where(p => !onboarding.Contains(p.ID) && !leaving.Contains(p.Handover.LeavingPeriodId.Value));
                        break;
                    case Package.Status.Triggered:
                        packages = packages.Where(p => leaving.Contains(p.Handover.LeavingPeriodId.Value));
                        break;
                }
            }

            return packages;
        }

        /// <summary>
        /// Creates mid and end review meetings
        /// </summary>
        /// <param name="package">Package to create meetings</param>
        /// <param name="fiscalYearStartMonth">Fiscal year start</param>
        public void CreateMidAndEndReviewMeetings(Package package, int fiscalYearStartMonth)
        {
            var lineManagerId = GetLineManagerContactId(package);

            var fiscalYearStart = new DateTime(SystemTime.Now.Year, fiscalYearStartMonth, 1);

            CreateMidReviewMeeting(package, lineManagerId, fiscalYearStart);
            CreateEndReviewMeeting(package, lineManagerId, fiscalYearStart);

            MContext.SaveChanges();
        }

        /// <summary>
        /// Creates mid review meeting
        /// </summary>
        /// <param name="package">Package to create meetings</param>
        /// <param name="lineManagerContactId">Line manager id</param>
        /// <param name="fiscalYearStart">Fiscal year start</param>
        private void CreateMidReviewMeeting(Package package, Guid lineManagerContactId, DateTime fiscalYearStart)
        {
            var midReviewMeetingType = MeetingService.GetMeetingTypeById(new Guid("ba7b2460-2150-4557-9fba-80c93da8c9b6"));
            
            if (MeetingExists(package.ID, midReviewMeetingType.ID, fiscalYearStart))
                return;

            var now = SystemTime.Now;
            var daysInYear = now.GetDaysInYear();
            var midYear = fiscalYearStart.AddDays((int)(daysInYear / 2) - 1).NextBusinessDayOrSelf();
            
            var meetingStart = midYear.AddHours(10);
            var meetingEnd = meetingStart.AddMinutes(60);
            
            var midReviewMeeting = new Meeting()
            {
                MeetingTypeId = midReviewMeetingType.ID,
                Name = midReviewMeetingType.Title,
                ParentContactId = lineManagerContactId,
                Start = meetingStart,
                End = meetingEnd
            };

            MContext.Meeting.Add(midReviewMeeting);
        }

        /// <summary>
        /// Create end review meetings
        /// </summary>
        /// <param name="package">Package to create meeting</param>
        /// <param name="lineManagerContactId">Line manager contact Id</param>
        /// <param name="fiscalYearStart">Fiscal year start</param>
        private void CreateEndReviewMeeting(Package package, Guid lineManagerContactId, DateTime fiscalYearStart)
        {
            var endReviewMeetingType = MeetingService.GetMeetingTypeById(new Guid("{5ECA1061-FEA1-473F-92FC-B320F97E50B9}"));

            if (MeetingExists(package.ID, endReviewMeetingType.ID, fiscalYearStart))
                return;

            var now = SystemTime.Now;
            var daysInYear = now.GetDaysInYear();
            var endYear = fiscalYearStart.AddDays(daysInYear - 1).NextBusinessDayOrSelf();

            var meetingStart = endYear.AddDays(-14).Friday().AddHours(10);
            var meetingEnd = meetingStart.AddMinutes(60);

            var endReviewMeeting = new Meeting()
            {
                MeetingTypeId = endReviewMeetingType.ID,
                Name = endReviewMeetingType.Title,
                ParentContactId = lineManagerContactId,
                Start = meetingStart,
                End = meetingEnd
            };

            MContext.Meeting.Add(endReviewMeeting);
        }

        /// <summary>
        /// Checks if a meeting exists
        /// </summary>
        /// <param name="packageId">Package id to check</param>
        /// <param name="meetingTypeId">Meeting type id</param>
        /// <param name="fiscalYearStart">Fiscal year start</param>
        /// <returns></returns>
        private bool MeetingExists(Guid packageId, Guid meetingTypeId, DateTime fiscalYearStart)
        {
            var fiscalYearEnd = fiscalYearStart.AddDays(SystemTime.Now.GetDaysInYear() - 1);
            fiscalYearStart = fiscalYearStart.AddMonths(1); // offset to avoid anomalies when end-meeting was moved into beginning of next fiscal year
            return MContext.Meeting.Any(m => !m.Deleted.HasValue
                && m.ParentContact.PackageId == packageId
                && m.MeetingTypeId == meetingTypeId
                && m.Start >= fiscalYearStart
                && m.Start <= fiscalYearEnd);
        }

        /// <summary>
        /// Gets a list of Onboarding Packages
        /// </summary>
        /// <returns>List of Packages</returns>
        public IEnumerable<Package> GetOnboarding()
        {
            var packages = MContext.Package.Where(
                p => !p.Deleted.HasValue &&
                p.Handover != null &&
                p.Handover.OnboardingPeriod != null &&
                p.Handover.OnboardingPeriod.Locked.HasValue &&
                p.Handover.OnboardingPeriod.LastDay.Value >= SystemTime.Now
            );

            return packages.ToList();
        }

        /// <summary>
        /// Gets a list of Ongoing Pagackages
        /// </summary>
        /// <returns>List of Packages</returns>
        public IEnumerable<Package> GetOngoing()
        {
            var packages = MContext.Package.Where(p => !p.Deleted.HasValue && p.Handover == null);

            return packages.ToList();
        }
        
        /// <summary>
        /// Gets a list of Packages in a leaving state
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Package> GetLeaving()
        {
            var packages = MContext.Package.Where(
                p => !p.Deleted.HasValue &&
                p.Handover != null &&
                p.Handover.LeavingPeriod != null &&
                p.Handover.LeavingPeriod.Locked.HasValue &&
                p.Handover.LeavingPeriod.LastDay.Value >= SystemTime.Now
            );

            return packages.ToList();
        }

        #endregion

        #region Permissions Validation

        /// <summary>
        /// Validates package permissions
        /// </summary>
        private void ValidatePermissions()
        {
            if (User == null)
                throw new AccessViolationException("A User must be logged in to access this method.");

            if (User.Role == null)
                throw new AccessViolationException("A User must have have a Role to accrss this method.");
        }

        #endregion

        /// <summary>
        /// Adds todo item
        /// </summary>
        /// <param name="toDo"></param>
        public void AddToDo(ToDoItem toDo)
        {
            MContext.ToDoItem.Add(toDo);
            MContext.SaveChanges();
        }

        /// <summary>
        /// Updates a todo item
        /// </summary>
        /// <param name="toDo"></param>
        public void UpdateToDo(ToDoItem toDo)
        {
            MContext.Entry(toDo).State = EntityState.Modified;
            MContext.SaveChanges();
        }

        public bool HasExitSurveyTemplate(Guid packageId)
        {
            return MContext.Package.Any(p => p.ID == packageId && p.ExitSurveyTemplateId.HasValue);
        }
    }
}
