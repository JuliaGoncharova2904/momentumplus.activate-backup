﻿using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Activate.Services.Interfaces;
using System.Data.Entity;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.People;
using MomentumPlus.Core.Comparers;
using MomentumPlus.Core.Models.Moderation;

namespace MomentumPlus.Activate.Services
{
    /// <summary>
    /// Contact Service
    /// </summary>
    public class ContactService : ServiceBase, IContactService
    {



        public ContactService(MomentumContext context) : base(context)
        {
        }

        #region IContactService Members

        /// <summary>
        /// Get all Contact entities
        /// </summary>
        /// <param name="deleted">If true will only return deleted Contacts</param>
        /// <returns></returns>
        public IEnumerable<Contact> GetAll(bool deleted = false)
        {
            var contacts =
                MContext.Contact.Where(c => c.Deleted.HasValue == deleted);

            return contacts;
        }

        /// <summary>
        /// Get all framework Contacts
        /// </summary>
        /// <param name="deleted">If true will only return deleted framework Contacts</param>
        /// <returns></returns>
        public IEnumerable<Contact> GetAllFramework(bool deleted = false)
        {
            IQueryable<Contact> contacts = MContext.Contact.Where(c => c.IsFramework && c.Deleted.HasValue == deleted);
            return contacts;
        }

        /// <summary>
        /// Get framework Contacts - paged and sorted
        /// </summary>
        /// <param name="pageNo">The page number</param>
        /// <param name="pageSize">The page size</param>
        /// <param name="sortColumn">The sort column</param>
        /// <param name="sortAscending">Sort order</param>
        /// <param name="deleted">If true will only return deleted framework Contacts</param>
        /// <returns></returns>
        public IEnumerable<Contact> GetFrameworkContacts(int pageNo, int pageSize, Person.SortColumn? sortColumn, bool sortAscending, bool deleted = false)
        {
            IQueryable<Contact> contacts = MContext.Contact.Where(c => c.IsFramework && c.Deleted.HasValue == deleted);

            if (sortColumn.HasValue)
            {
                SortAndPage(ref contacts, c => c, sortColumn.ToString(), sortAscending, pageNo, pageSize);
            }

            return contacts;
        }

        /// <summary>
        /// Get Contacts for a given Package
        /// </summary>
        /// <param name="packageId">The Package GUID</param>
        /// <param name="deleted">If true will only return deleted Contacts</param>
        /// <returns></returns>
        public IEnumerable<Contact> GetByPackageId(Guid packageId, bool deleted = false)
        {
            var package = MContext.Package.Find(packageId);

            var contacts
                = package.Contacts.Where(c => c.Deleted.HasValue == deleted);

            return contacts;
        }

        /// <summary>
        /// Get Contacts for a given Package
        /// </summary>
        /// <param name="packageId">The Package GUID</param>
        /// <param name="pageNo">The page number</param>
        /// <param name="pageSize">The page size</param>
        /// <param name="sortColumn">The sort column</param>
        /// <param name="sortAscending">Sort order</param>
        /// <param name="deleted">If true will only return deleted Contacts</param>
        /// <returns></returns>
        public IEnumerable<Contact> GetByPackageId(Guid packageId, int pageNo, int pageSize, Person.SortColumn sortColumn, bool sortAscending, bool deleted = false)
        {
            var contacts = GetByPackageId(packageId, deleted).AsQueryable();

            //SortAndPage<Contact>(pageNo, pageSize, sortColumn.ToString(), sortAscending, ref contacts);
            SortAndPage(ref contacts, e => e, sortColumn.ToString(), sortAscending, pageNo, pageSize);

            return contacts;
        }

        /// <summary>
        /// Get the number of Contacts in a given package
        /// </summary>
        /// <param name="packageId">The Package GUID</param>
        /// <param name="deleted">If true will only return deleted Contacts</param>
        /// <returns></returns>
        public int CountByPackageId(Guid packageId, bool deleted = false)
        {
            return MContext.Contact.Count(c => c.PackageId == packageId && c.Deleted.HasValue == deleted);
        }

        /// <summary>
        /// Get Contacts for a given Package - filtered by company
        /// </summary>
        /// <param name="packageId">The Package GUID</param>
        /// <param name="groupNamesFilter">A list of company names to filter by</param>
        /// <param name="deleted">If true will only return deleted Contacts</param>
        /// <returns></returns>
        public IEnumerable<Contact> GetByPackageId(Guid packageId, IEnumerable<string> groupNamesFilter, bool deleted = false)
        {
            IEnumerable<Contact>
                contacts = new List<Contact>();

            if (groupNamesFilter != null && groupNamesFilter.Any())
            {
                contacts = (from c in MContext.Contact.Where(c => c.PackageId == packageId)
                            where c.Company != null
                            where groupNamesFilter.Contains(c.Company.Name)
                            select c).MyDistinct(c => c.ID);
            }

            contacts = from c in contacts where c.Deleted.HasValue == deleted select c;

            return contacts;
        }

        /// <summary>
        /// Get non-deleted Contacts in a Package that are managed by the Package owner
        /// </summary>
        /// <param name="packageId">The Package GUID</param>
        /// <returns></returns>
        public IEnumerable<Contact> GetStaffContacts(Guid packageId)
        {
            IQueryable<Contact> contacts = MContext.Contact.Where(c => c.PackageId == packageId && !c.Deleted.HasValue && c.Managed)
                                                                       .Include(c => c.Company)
                                                                       .Include(c => c.Moderations);

            return contacts;
        }

        /// <summary>
        /// Get Contacts in a Package that are managed by the Package owner
        /// </summary>
        /// <param name="packageId">The Package GUID</param>
        /// <param name="sortColumn">Column to sort by</param>
        /// <param name="sortAscending">Sort order</param>
        /// <param name="deleted">If true will only return deleted Contacts</param>
        /// <returns></returns>
        public IEnumerable<Contact> GetStaffContacts(Guid packageId, Person.SortColumn? sortColumn, bool sortAscending = true, bool deleted = false)
        {
            IQueryable<Contact> contacts = MContext.Contact.Where(c => c.PackageId == packageId && c.Deleted.HasValue == deleted && c.Managed)
                                                                       .Include(c => c.Moderations)
                                                                       .Include(c => c.Attachments)
                                                                       .Include(c => c.DocumentLibraries)
                                                                       .Include(c => c.VoiceMessages)
                                                                       .Include(c => c.Company);

            return contacts;
        }


        public IEnumerable<Contact> GetTeamContacts(Guid packageId, Person.SortColumn? sortColumn, bool sortAscending = true, bool deleted = false)
        {
            IQueryable<Contact> staffContacts = MContext.Contact.Where(c => c.PackageId == packageId && c.Deleted.HasValue == deleted && c.Managed)
                                                                       .Include(c => c.Moderations)
                                                                       .Include(c => c.Attachments)
                                                                       .Include(c => c.DocumentLibraries)
                                                                       .Include(c => c.VoiceMessages)
                                                                       .Include(c => c.Company);


            var packageSee = MContext.Package.FirstOrDefault(s => s.ID == packageId);

            var userService = new UserService(MContext);

            List<Contact> packageContacts = new List<Contact>();


            if (packageSee != null)
            {
                var meetingService = new MeetingService(MContext, userService.GetByUserId(packageSee.UserOwnerId));
                var packageService = new PackageService(MContext, meetingService, userService.GetByUserId(packageSee.UserOwnerId));

                var packagesOnboarding = packageService.GetByPackageStatus(Package.Status.New, false).ToList();
                var packagesLeaving = packageService.GetByPackageStatus(Package.Status.Triggered, false).ToList();
                var packagesOngoaning = packageService.GetByPackageStatus(Package.Status.InProgress, false).ToList();


                var packages = packagesOnboarding.Union(packagesLeaving).Where(c => c.ID != packageId);

                packages = packages.Union(packagesOngoaning).Where(c => c.ID != packageId);

                packageContacts.AddRange(packages.Select(package => new Contact
                {
                    PackageId = packageId,
                    Moderations = new List<Moderation> {new Moderation
                    {
                        Decision = ModerationDecision.Approved,
                        Moderated = new ModeratedStaff
                        {
                           Moderations = new List<Moderation> {new Moderation
                           {
                               Decision = ModerationDecision.Approved
                           } }
                        }
                            
                    } },
                    ContactPackageId = package.ID,
                    IsBasedOnPackage = true,
                    ContactPackageStatus = package.PackageStatus,
                    ImageId = package.UserOwner.ImageId,
                    OtherCompany = package.UserOwner.OtherCompany,
                    OtherCompanyName = package.UserOwner.OtherCompanyName,
                    ContactRole = package.UserOwner.Role.RoleParsed,
                    FirstName = package.UserOwner.FirstName,
                    LastName = package.UserOwner.LastName,
                    Email = package.UserOwner.Email,
                    Phone = package.UserOwner.Phone,
                    User = package.UserOwner,
                    Deleted = package.Deleted,
                    Managed = false,
                    UserRole = packageService.GetByPackageId(packageId).UserOwner.Role.RoleParsed,
                    //IntroRequired = package.UserOwner.Contact.IntroRequired,
                    Company = package.UserOwner.Company,
                    Position = package.UserOwner.Position
                }).Where(c => c.ContactPackageId != packageSee.ID));
            }


            var contacts = packageContacts.Concat(staffContacts).OrderBy(c => c.Name);


            return contacts;
        }



        /// <summary>
        /// Get Contacts in a Package that are managed by the Package owner - sorted and paged
        /// </summary>
        /// <param name="packageId">The Package GUID</param>
        /// <param name="pageNo">The page number</param>
        /// <param name="pageSize">The page size</param>
        /// <param name="sortColumn">The sort column</param>
        /// <param name="sortAscending">Sort order</param>
        /// <param name="deleted">If true will only return deleted Contacts</param>
        /// <returns></returns>
        public IEnumerable<Contact> GetStaffContacts(Guid packageId, int pageNo, int pageSize, Person.SortColumn? sortColumn, bool sortAscending = true, bool deleted = false)
        {
            IQueryable<Contact> contacts = GetStaffContacts(packageId, sortColumn, sortAscending, deleted).AsQueryable();

            if (sortColumn.HasValue)
            {
                SortAndPage(ref contacts, c => c, sortColumn.ToString(), sortAscending, pageNo, pageSize);
            }

            return contacts;
        }

        /// <summary>
        /// Search Contacts in a Package that are managed by the Package owner - sorted and paged
        /// </summary>
        /// <param name="packageId">The Package GUID</param>
        /// <param name="term">The search query</param>
        /// <param name="pageNo">The page number</param>
        /// <param name="pageSize">The page size</param>
        /// <param name="sortColumn">The sort column</param>
        /// <param name="sortAscending">Sort order</param>
        /// <param name="deleted">If true will only return deleted Contacts</param>
        /// <returns></returns>
        public IEnumerable<Contact> SearchStaffContacts(Guid packageId, string term, int pageNo, int pageSize, Person.SortColumn? sortColumn, bool sortAscending = true, bool deleted = false)
        {
            term = term.ToLower();
            var contacts = MContext.Contact.Where(c => c.PackageId == packageId
                                                       && c.Managed
                                                       && c.Deleted.HasValue == deleted
                                                       && (string.Concat(c.FirstName, " ", c.LastName)
                                                               .ToLower()
                                                               .Contains(term)
                                                             || c.Position.ToLower().Contains(term)
                                                             || c.Company.Name.ToLower().Contains(term))

                ).Include(c => c.Company);

            if (sortColumn.HasValue)
            {
                SortAndPage(ref contacts, c => c, new PersonComparer<Person>(sortColumn.ToString()), sortAscending, pageNo, pageSize);
            }

            return contacts;
        }



        /// Search Contacts in a Package that are managed by the Package owner - sorted and paged
        /// </summary>
        /// <param name="packageId">The Package GUID</param>
        /// <param name="term">The search query</param>
        /// <param name="pageNo">The page number</param>
        /// <param name="pageSize">The page size</param>
        /// <param name="sortColumn">The sort column</param>
        /// <param name="sortAscending">Sort order</param>
        /// <param name="deleted">If true will only return deleted Contacts</param>
        /// <returns></returns>
        public IEnumerable<Contact> SearchPeople(Guid packageId, string term, int pageNo, int pageSize, Person.SortColumn? sortColumn, bool sortAscending = true, bool deleted = false)
        {
            term = term.ToLower();

            var packageSee = MContext.Package.FirstOrDefault(s => s.ID == packageId);

            var userService = new UserService(MContext);

            List<Contact> packageContacts = new List<Contact>();


            if (packageSee != null)
            {
                var meetingService = new MeetingService(MContext, userService.GetByUserId(packageSee.UserOwnerId));
                var packageService = new PackageService(MContext, meetingService, userService.GetByUserId(packageSee.UserOwnerId));

                var packagesOnboarding = packageService.GetByPackageStatus(Package.Status.New, false).ToList();
                var packagesLeaving = packageService.GetByPackageStatus(Package.Status.Triggered, false).ToList();
                var packagesOngoaning = packageService.GetByPackageStatus(Package.Status.InProgress, false).ToList();


                var packages = packagesOnboarding.Union(packagesLeaving).Where(c => c.ID != packageId);

                packages = packages.Union(packagesOngoaning).Where(c => c.ID != packageId);

                packageContacts.AddRange(packages.Select(package => new Contact
                {
                    PackageId = packageId,
                    ContactPackageId = package.ID,
                    IsBasedOnPackage = true,
                    ContactPackageStatus = package.PackageStatus,
                    ImageId = package.UserOwner.ImageId,
                    OtherCompany = package.UserOwner.OtherCompany,
                    OtherCompanyName = package.UserOwner.OtherCompanyName,
                    ContactRole = package.UserOwner.Role.RoleParsed,
                    FirstName = package.UserOwner.FirstName,
                    LastName = package.UserOwner.LastName,
                    Email = package.UserOwner.Email,
                    Phone = package.UserOwner.Phone,
                    User = package.UserOwner,
                    Deleted = package.Deleted,
                    Managed = false,
                    UserRole = packageService.GetByPackageId(packageId).UserOwner.Role.RoleParsed,
                    //IntroRequired = package.UserOwner.Contact.IntroRequired,
                    Company = package.UserOwner.Company,
                    Position = package.UserOwner.Position
                }).Where(c => c.ContactPackageId != packageSee.ID));
            }


            var teamContacts = packageContacts.AsQueryable().Where(c => c.PackageId == packageId
                                                           && c.Deleted.HasValue == deleted
                                                           && (string.Concat(c.FirstName, " ", c.LastName)
                                                               .ToLower()
                                                               .Contains(term)
                                                               || c.Position.ToLower().Contains(term)
                                                               || c.Company.Name.ToLower().Contains(term))

                    ).Include(c => c.Company).Include(p => p.Package);


            var userContacts = MContext.Contact.Where(c => c.PackageId == packageId
                                                           && c.Managed
                                                           && c.Deleted.HasValue == deleted
                                                           && (string.Concat(c.FirstName, " ", c.LastName)
                                                               .ToLower()
                                                               .Contains(term)
                                                               || c.Position.ToLower().Contains(term)
                                                               || c.Company.Name.ToLower().Contains(term))

                    ).Include(c => c.Company).Include(p => p.Package);

            var contacts = teamContacts.Concat(userContacts);

            if (sortColumn.HasValue)
            {
                SortAndPage(ref contacts, c => c, new PersonComparer<Person>(sortColumn.ToString()), sortAscending, pageNo, pageSize);
            }

            return contacts;
        }



        /// <summary>
        /// Get the number of Contacts in a Package that are managed by the Package owner - sorted and paged
        /// </summary>
        /// <param name="packageId">The Package GUID</param>
        /// <param name="term">The search query</param>
        /// <param name="deleted">If true will only count deleted Contacts</param>
        /// <returns></returns>
        public int CountPeopleContacts(Guid packageId, string term, bool deleted = false)
        {
            term = term.ToLower();

            var count = 0;


            var packageSee = MContext.Package.FirstOrDefault(s => s.ID == packageId);

            var userService = new UserService(MContext);

            if (packageSee != null)
            {
                var meetingService = new MeetingService(MContext, userService.GetByUserId(packageSee.UserOwnerId));
                var packageService = new PackageService(MContext, meetingService,
                    userService.GetByUserId(packageSee.UserOwnerId));


                var packagesOnboarding = packageService.GetByPackageStatus(Package.Status.New, false).ToList();
                var packagesLeaving = packageService.GetByPackageStatus(Package.Status.Triggered, false).ToList();
                var packagesOngoaning = packageService.GetByPackageStatus(Package.Status.InProgress, false).ToList();


                var packages = packagesOnboarding.Union(packagesLeaving).Where(c => c.ID != packageId);

                packages = packages.Union(packagesOngoaning).Where(c => c.ID != packageId);



                count = packages.Count();
            }



            count = count + MContext.Contact.Count(c => c.PackageId == packageId
                && c.Managed
                && c.Deleted.HasValue == deleted
                && (string.Concat(c.FirstName, " ", c.LastName).ToLower().Contains(term)
                    || c.Position.ToLower().Contains(term)
                    || c.Company.Name.ToLower().Contains(term)));

            return count;
        }



        /// <summary>
        /// Get the number of Contacts in a Package that are managed by the Package owner - sorted and paged
        /// </summary>
        /// <param name="packageId">The Package GUID</param>
        /// <param name="term">The search query</param>
        /// <param name="deleted">If true will only count deleted Contacts</param>
        /// <returns></returns>
        public int CountStaffContacts(Guid packageId, string term, bool deleted = false)
        {
            term = term.ToLower();

            return MContext.Contact.Count(c => c.PackageId == packageId
                && c.Managed
                && c.Deleted.HasValue == deleted
                && (string.Concat(c.FirstName, " ", c.LastName).ToLower().Contains(term)
                    || c.Position.ToLower().Contains(term)
                    || c.Company.Name.ToLower().Contains(term)));
        }


        /// <summary>
        /// Get the names of all the companies for the Contacts in a Package
        /// </summary>
        /// <param name="packageId">The Package GUID</param>
        /// <param name="deleted">If true will only count deleted Contacts</param>
        /// <returns></returns>
        public IEnumerable<string> GetCompanies(Guid packageId, bool deleted = false)
        {

            var companies = (from c in MContext.Contact.Where(c => c.PackageId == packageId)
                             where c.Deleted.HasValue == deleted
                             where c.Company != null
                             select c.Company.Name).OrderBy(c => c).Distinct();

            return companies;
        }

        /// <summary>
        /// Get a Contact by ID
        /// </summary>
        /// <param name="id">The Contact GUID</param>
        /// <returns>A Contact object</returns>
        public Contact GetById(Guid id)
        {
            var contact =
                MContext.Contact.Find(id);

            return contact;
        }

        /// <summary>
        /// Create a new Contact in the database
        /// </summary>
        /// <param name="contact">A Contact object</param>
        /// <returns></returns>
        public Contact Add(Contact contact)
        {
            contact = MContext.Contact.Add(contact);

            MContext.SaveChanges();

            return contact;
        }


        /// <summary>
        /// Does contact width email exist?
        /// </summary>
        /// <param name="email">Email</param>
        /// <returns>True if exists</returns>
        public bool EmailExists(string email)
        {
            bool exists = false;

            var contact =
                MContext.Contact.FirstOrDefault(c => c.Email == email);

            if (contact != null)
                exists = true;

            return exists;
        }


        /// <summary>
        /// Does contact width email exist?
        /// </summary>
        /// <param name="email">Email</param>
        /// <param name="packageId">PackageId</param>
        /// <returns>True if exists</returns>
        public bool EmailExistsWidthCore(string email, Guid packageId)
        {
            bool exists = false;

            var contact =
                MContext.Contact.FirstOrDefault(c => c.Email == email && c.IsFramework || c.Email == email && c.IsFramework == false && c.Package.ID == packageId);

            if (contact != null)
                exists = true;

            return exists;
        }




        /// <summary>
        /// Update a new Contact in the database
        /// </summary>
        /// <param name="contact">A Contact object</param>
        /// <returns></returns>
        public Contact Update(Contact contact)
        {
            MContext.Entry(contact).State = EntityState.Modified; // Shouldn't be necesary

            MContext.SaveChanges();

            return contact;
        }

        /// <summary>
        /// Delete a Contact from the database
        /// </summary>
        /// <param name="contactId">The Contact GUID</param>
        public void Delete(Guid contactId)
        {
            var contact = MContext.Contact.Find(contactId);
            Delete(contact);
        }

        /// <summary>
        /// Delete a Contact from the database
        /// </summary>
        /// <param name="contact">The Contact object</param>
        public void Delete(Contact contact)
        {
            if (contact.Meetings != null)
            {
                MContext.Meeting.RemoveRange(contact.Meetings);
                contact.Meetings.Clear();
            }
            MContext.Attachment.RemoveRange(contact.Attachments);
            MContext.Contact.Remove(contact);

            MContext.SaveChanges();
        }

        /// <summary>
        /// Clone a Contact object
        /// </summary>
        /// <param name="contactId">The Contact GUID</param>
        /// <returns>The cloned Contact</returns>
        public Contact Clone(Guid contactId)
        {
            var contact =
                MContext.Contact.AsNoTracking().FirstOrDefault(c => c.ID == contactId);

            return Clone(contact);
        }

        /// <summary>
        /// Clone a Contact object
        /// </summary>
        /// <param name="contact">The Contact object</param>
        /// <returns>The cloned Contact</returns>
        public Contact Clone(Contact contact)
        {
            var contactClone =
                DeepClone<Contact>(contact);

            ResetBaseFields((BaseModel)contactClone);

            foreach (Meeting meeting in contact.Meetings)
            {
                ResetBaseFields((BaseModel)meeting);

                foreach (Attachment attachment in meeting.Attachments)
                {
                    ResetBaseFields((BaseModel)attachment);
                }

                foreach (VoiceMessage voiceMessage in meeting.VoiceMessages)
                {
                    ResetBaseFields((BaseModel)voiceMessage);
                }
            }

            return contactClone;
        }

        /// <summary>
        /// Search for Attachments in a Package
        /// </summary>
        /// <param name="searchString">The search query</param>
        /// <param name="packageId">The Package GUID</param>
        /// <returns></returns>
        public IEnumerable<Attachment> SearchAttachments(string searchString, Guid packageId)
        {
            RestrictedAccess restrictedAccess =
                new RestrictedAccess(MContext, packageId);

            IQueryable<Attachment> searchResults = (from a in restrictedAccess.ContactAttachments
                                                    where a.Name.Contains(searchString) ||
                                                    a.SearchTags.Contains(searchString)
                                                    select a);
            return searchResults.ToList();
        }

        /// <summary>
        /// Search for voice messages in a Package
        /// </summary>
        /// <param name="searchString">The search query</param>
        /// <param name="packageId">The Package GUID</param>
        /// <returns></returns>
        public IEnumerable<VoiceMessage> SearchVoiceMessages(string searchString, Guid packageId)
        {
            RestrictedAccess restrictedAccess =
                new RestrictedAccess(MContext, packageId);

            IQueryable<VoiceMessage> searchResults = (from v in restrictedAccess.ContactVoiceMessages
                                                      where v.Name.Contains(searchString) ||
                                                      v.SearchTags.Contains(searchString)
                                                      select v);
            return searchResults.ToList();
        }

        /// <summary>
        /// Retire a Contact
        /// </summary>
        /// <param name="contactId">The Contact GUID</param>
        public void Retire(Guid contactId)
        {
            var contact = MContext.Contact.Find(contactId);
            contact.Retire();
            MContext.SaveChanges();

        }

        /// <summary>
        /// Restore a Contact
        /// </summary>
        /// <param name="contactId">The Contact GUID</param>
        public void Restore(Guid contactId)
        {
            var contact = MContext.Contact.Find(contactId);
            contact.Restore();
            MContext.SaveChanges();
        }

        /// <summary>
        /// Detach a Contact from the database context so it is not tracked
        /// </summary>
        /// <param name="contact">The Contact object</param>
        public void Detach(Contact contact)
        {
            MContext.Entry(contact).State = EntityState.Detached;
        }

        /// <summary>
        /// Get the Contacts created for a given Handover period
        /// </summary>
        /// <param name="periodId">The Period GUID</param>
        /// <returns></returns>
        public IEnumerable<Contact> GetByPeriodId(Guid periodId)
        {
            return MContext.Contact.Where(c => c.PeriodId == periodId);
        }

        /// <summary>
        /// Get a Contact for a given Meeting
        /// </summary>
        /// <param name="meetingId">The meeting GUID</param>
        /// <returns>A Contact object</returns>
        public Contact GetByMeetingId(Guid meetingId)
        {
            return MContext.Meeting.Where(m => m.ID == meetingId).Select(m => m.ParentContact).FirstOrDefault();
        }

        /// <summary>
        /// Get a Contact for a given Task
        /// </summary>
        /// <param name="taskId">The Task GUID</param>
        /// <returns>A Contact object</returns>
        public Contact GetByTaskId(Guid taskId)
        {
            return MContext.Task.Where(m => m.ID == taskId).Select(m => m.Contact).FirstOrDefault();
        }

        #endregion
    }
}
