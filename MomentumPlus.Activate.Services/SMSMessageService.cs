﻿using System;
using System.Collections.Generic;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Services.SMSWS;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.People;

namespace MomentumPlus.Activate.Services
{
    public class SMSMessageService : ServiceBase, ISMSMessagesService
    {
        public SMSMessageService(MomentumContext context) : base(context) { }

        /// <summary>
        /// Sends a SMS
        /// </summary>
        /// <param name="SMSToSendUser">User recipient</param>
        /// <param name="SMSToSendNumber">Number recipient</param>
        /// <param name="SMSFromSendNumber">SMS from number</param>
        /// <param name="message">SMS message</param>
        /// <returns>SMS Message with status</returns>
        public SMSMessages SendSMS(User SMSToSendUser, string SMSToSendNumber, string SMSFromSendNumber, string message)
        {
            var sendingSMS = new SMSMessages();

            var userID = SMSToSendUser.ID;
            var applicationID = "bSqwrD079b4=";

            DateTime? timeNow = DateTime.Now.AddSeconds(5);
            List<string> theRecipient = new List<string>();
            theRecipient.Add(SMSToSendNumber);
            var sendingNumber = SMSFromSendNumber;// "84840";


            try
            {
                sendSMS(
                    userID,
                    applicationID,
                    message,
                    timeNow,
                    theRecipient,
                    sendingNumber,
                    null
                    );
                sendingSMS.SentDate = DateTime.Now;
                sendingSMS.ToUser = SMSToSendUser.LastName + ", " + SMSToSendUser.FirstName;
                sendingSMS.User = sendingSMS.User;
                sendingSMS.ToNumber = SMSToSendUser.Mobile;
                sendingSMS.Originator = sendingNumber;

                sendingSMS.Success = true;
            }
            catch
            {
                sendingSMS.Success = false;
            }
            MContext.SMSMessages.Add(sendingSMS);
            MContext.SaveChanges();
            return sendingSMS;
        }


        /// <summary>
        /// Sends an SMS
        /// </summary>
        /// <param name="userID">User id for the IML SMS service</param>
        /// <param name="clientID">Client id for the IML SMS service</param>
        /// <param name="messageBody">SMS body</param>
        /// <param name="scheduledDateTime">Date time to send the message</param>
        /// <param name="recipients">Recipients for the SMS</param>
        /// <param name="textOriginator">SMS originator</param>
        /// <param name="emailForReplies">Email address to receive replies</param>
        /// <returns></returns>
        private WSBatch sendSMS(Guid userID, string clientID, string messageBody, DateTime? scheduledDateTime, List<string> recipients, string textOriginator, string emailForReplies)
        {
           List<RecipientSimple> recipientsSimple = new List<RecipientSimple>();
           foreach (string recipient in recipients)
           {
               RecipientSimple rs = new RecipientSimple();
               rs.recipientMobileNo = recipient;
               rs.messageText = messageBody;
               recipientsSimple.Add(rs);
           }

           try
           {
               using (SMSWS.MainSoapClient client = new MainSoapClient())
               {
                    WSBatch wsBatch = null;

                   if(string.IsNullOrEmpty(emailForReplies)) // Send, with no reply functionality
                       wsBatch = client.sendDiscreteSMS(clientID, userID.ToString(), textOriginator, scheduledDateTime, null,  recipientsSimple.ToArray(), true, null, string.Empty, null);
                   else // Send, with email reply functionality, 2 days
                       wsBatch = client.sendDiscreteSMS(clientID, userID.ToString(), textOriginator, scheduledDateTime, null, recipientsSimple.ToArray(), true, ResponseType.Email, emailForReplies, 2880);

                   return wsBatch;
               }
           }
           catch (Exception ex)
           {
               throw new Exception("SMS Scheduler Web Service error with method 'sendDiscreteSMS', " + ex.Message);
           }
       }

    }
}