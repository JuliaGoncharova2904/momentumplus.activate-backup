﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Services
{
    /// <summary>
    /// Topic Sevice
    /// </summary>
    public class TopicService : ServiceBase, ITopicService
    {
        public TopicService(MomentumContext context) : base(context) { }

        #region ITopicService Members

        /// <summary>
        /// Gets Topics by parent Topic Group Id
        /// </summary>
        /// <param name="topicGroupId"></param>
        /// <param name="pageNo"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortAscending"></param>
        /// <param name="deleted"></param>
        /// <returns>Collection of Topics</returns>
        public IEnumerable<Topic> GetByTopicGroupId(Guid topicGroupId, int pageNo, int pageSize, Topic.SortColumn sortColumn, bool sortAscending, bool deleted = false)
        {
            var topicGroups =
                MContext.TopicGroup.Where(tp => !tp.Deleted.HasValue && tp.ID == topicGroupId).Include(t => t.Topics).FirstOrDefault();

            var topics =
                topicGroups.Topics.AsQueryable();

            topics = from t in topics where t.Deleted.HasValue == deleted select t;

            SortAndPage<Topic>(pageNo, pageSize, sortColumn.ToString(), sortAscending, ref topics);

            return topics.ToList();
        }

        /// <summary>
        /// Gets Topics by parent Topic Group Id
        /// </summary>
        /// <param name="topicGroupId"></param>
        /// <param name="deleted"></param>
        /// <returns>Collection of Topics</returns>
        public IEnumerable<Topic> GetByTopicGroupId(Guid topicGroupId, bool deleted = false)
        {
            var topicGroups =
                MContext.TopicGroup.Where(tp => !tp.Deleted.HasValue && tp.ID == topicGroupId).Include(t => t.Topics).FirstOrDefault();

            var topics =
                topicGroups.Topics.AsQueryable();

            topics = from t in topics where t.Deleted.HasValue == deleted select t;

            return topics.ToList();
        }


        /// <summary>
        /// Gets Topics by parent Topic Group Id and Name
        /// </summary>
        /// <param name="topicGroupId"></param>
        ///         /// <param name="name"></param>
        /// <param name="deleted"></param>
        /// <returns>Collection of Topics</returns>
        public Topic GetTopicByGroupIdAndName(Guid topicGroupId,string name, bool deleted = false)
        {
            var topicGroups =
                MContext.TopicGroup.Where(tp => !tp.Deleted.HasValue && tp.ID == topicGroupId).Include(t => t.Topics).FirstOrDefault();

            var topics =
                topicGroups.Topics.AsQueryable();

            topics = from t in topics where t.Deleted.HasValue == deleted && t.Name.Equals(name) select t;

            return topics.FirstOrDefault();
        }



        /// <summary>
        /// Counts Topic Groups
        /// </summary>
        /// <param name="topicGroupId"></param>
        /// <param name="deleted"></param>
        /// <returns></returns>
        public int Count(Guid topicGroupId, bool deleted = false)
        {
            int count =
                MContext.TopicGroup.Where(tp => !tp.Deleted.HasValue && tp.ID == topicGroupId)
                    .Include(t => t.Topics)
                    .FirstOrDefault()
                    .Topics.Where(t => t.Deleted.HasValue == deleted)
                    .Count();

            return count;
        }

        /// <summary>
        /// Gets a collection of Topics by Package Id
        /// </summary>
        /// <param name="packageId"></param>
        /// <returns>Collection of Topics</returns>
        public IEnumerable<Topic> GetByPackageId(Guid packageId)
        {
            var topics
                = GetByPackageIdQueryable(packageId);

            return topics.ToList();
        }

        /// <summary>
        /// Gets a collection of Topics by 
        /// Package Id
        /// </summary>
        /// <param name="packageId"></param>
        /// <param name="pageNo"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortAscending"></param>
        /// <returns>Collection of Topics</returns>
        public IEnumerable<Topic> GetByPackageId(Guid packageId, int pageNo, int pageSize, Topic.SortColumn sortColumn, bool sortAscending)
        {
            var topics =
                GetByPackageIdQueryable(packageId);

            SortAndPage<Topic>(pageNo, pageSize, sortColumn.ToString(), sortAscending, ref topics);

            return topics.ToList();
        }

        /// <summary>
        /// Counts number of Topics that 
        /// belong to a Package
        /// </summary>
        /// <param name="packageId"></param>
        /// <returns></returns>
        public int CountByPackageId(Guid packageId)
        {
            var topics =
                GetByPackageIdQueryable(packageId);

            return topics.Count();
        }

        /// <summary>
        /// Gets onboarding Topics by Package
        /// </summary>
        /// <param name="packageId"></param>
        /// <returns>Collection of Topics</returns>
        public IEnumerable<Topic> GetOnboardingByPackageId(Guid packageId)
        {
            var package =
                MContext.Package.Find(packageId);

            IEnumerable<Topic> topics = null;

            if (package != null && package.Handover != null && package.Handover.OnboardingPeriod != null && package.Handover.OnboardingPeriod.PackageId.HasValue)
                topics = GetByPackageIdQueryable(package.Handover.OnboardingPeriod.PackageId.Value).ToList();

            return topics;
        }

        /// <summary>
        /// Gets a Topic by Id
        /// </summary>
        /// <param name="topicId">Topic id</param>
        /// <returns>Topic</returns>
        public Topic GetByTopicId(Guid topicId)
        {
            var topic =
               MContext.Topic.Find(topicId);

            return topic;
        }

        /// <summary>
        /// Gets a Topic based on the Frameword/source 
        /// Id
        /// </summary>
        /// <param name="topicSourceId">Source Topic (framework) id</param>
        /// <param name="packageId">Package Id of the Topic to retrieve</param>
        /// <returns></returns>
        public Topic GetByTopicSourceId(Guid topicSourceId, Guid packageId)
        {
            var package = MContext.Package.Find(packageId);

            var topic = (from m in package.Modules
                         from tg in m.TopicGroups
                         from to in tg.Topics
                         where !to.Deleted.HasValue && to.SourceId == topicSourceId
                         select to).FirstOrDefault();

            return topic;
        }



        /// <summary>
        /// Gets a Topic 
        /// </summary>
        /// <param name="topicSourceId">Source Topic id</param>
        /// <returns></returns>
        public Topic GetByReferenceId(int topicSourceId)
        {
            var topic = MContext.Topic.FirstOrDefault(t => t.ReferenceId == topicSourceId);

            return topic;
        }



        /// <summary>
        /// Gets the parent Topic for the task
        /// </summary>
        /// <param name="taskId">Task Id</param>
        /// <returns>Topic</returns>
        public Topic GetByTaskId(Guid taskId)
        {
            var task =
                MContext.Task.Find(taskId);

            var topic =
                MContext.Topic.Find(task.ParentTopicId.Value);

            return topic;
        }

        /// <summary>
        /// Search Topics
        /// </summary>
        /// <param name="searchString">Sear</param>
        /// <param name="packageId"></param>
        /// <returns>Collection of Topics</returns>
        public IEnumerable<Topic> Search(string searchString, Guid packageId)
        {
            RestrictedAccess restrictedAccess =
                new RestrictedAccess(MContext, packageId);

            IEnumerable<Topic> searchResults = (from t in restrictedAccess.Topics
                                                where t.Name.Contains(searchString) ||
                                                t.SearchTags.Contains(searchString)
                                                select t);

            return searchResults.ToList();
        }

        /// <summary>
        /// Search Topic Attachments
        /// </summary>
        /// <param name="searchString"></param>
        /// <param name="packageId"></param>
        /// <returns>Collection of Attachments</returns>
        public IEnumerable<Attachment> SearchAttachments(string searchString, Guid packageId)
        {
            RestrictedAccess restrictedAccess =
                new RestrictedAccess(MContext, packageId);

            IQueryable<Attachment> searchResults = (from a in restrictedAccess.TopicAttachments
                                                    where a.Name.Contains(searchString) ||
                                                    a.SearchTags.Contains(searchString)
                                                    select a);
            return searchResults.ToList();
        }

        /// <summary>
        /// Search Topic Voice Messages
        /// </summary>
        /// <param name="searchString"></param>
        /// <param name="packageId"></param>
        /// <returns>Collection of Voice Messages</returns>
        public IEnumerable<VoiceMessage> SearchVoiceMessages(string searchString, Guid packageId)
        {
            RestrictedAccess restrictedAccess =
              new RestrictedAccess(MContext, packageId);

            IQueryable<VoiceMessage> searchResults = (from v in restrictedAccess.TopicVoiceMessages
                                                      where v.Name.Contains(searchString) ||
                                                      v.SearchTags.Contains(searchString)
                                                      select v);
            return searchResults.ToList();
        }

        /// <summary>
        /// Add a Topic to a Topic Group
        /// </summary>
        /// <param name="topicGroupId"></param>
        /// <param name="topic"></param>
        /// <returns>Topic</returns>
        public Topic Add(Guid topicGroupId, Topic topic)
        {
            TopicGroup topicGroup =
                MContext.TopicGroup.Where(t => !t.Deleted.HasValue && t.ID == topicGroupId).Include(t => t.Topics).FirstOrDefault();

            topicGroup.Topics.Add(topic);

            MContext.SaveChanges();

            return topic;
        }

        /// <summary>
        /// Update a Topic
        /// </summary>
        /// <param name="topic"></param>
        /// <returns>Topic</returns>
        public Topic Update(Topic topic)
        {
            MContext.Entry(topic).State = EntityState.Modified;

            MContext.SaveChanges();

            return topic;
        }


        public void UpdateAllTemplateTopicsName(Topic topic, string newName)
        {
            //var templateItem = new TemplateItemService(MContext);

            var tempateItems =
                MContext.TemplateItem.Where(i => i.Type == TemplateItem.TemplateItemType.Topic && !i.Deleted.HasValue && i.Name.Equals(topic.Name));

            foreach (var tempateItem in tempateItems)
            {
                tempateItem.Name = newName;
            }

            MContext.SaveChanges();

        }

        /// <summary>
        /// Deletes a Topic
        /// </summary>
        /// <param name="topicId">Topic Id of Topic to delete</param>
        public void Delete(Guid topicId)
        {
            var topic = MContext.Topic
                .Include(t => t.Fields.Select(f => f.Question))
                .Include(t => t.Fields.Select(f => f.Answers))
                .FirstOrDefault(t => t.ID == topicId);

            Delete(topic);
        }

        /// <summary>
        /// Deletes a Topic
        /// </summary>
        /// <param name="topic">Topic to delete</param>
        public void Delete(Topic topic)
        {
            if (topic == null) return;

            MContext.Topic.Remove(topic);
            MContext.SaveChanges();
        }

        /// <summary>
        /// Clone a Topic
        /// </summary>
        /// <param name="topicId">Topic Id of Topic to clone</param>
        /// <param name="deepClone">Clone child entities</param>
        /// <returns>Topic clone</returns>
        public Topic Clone(Guid topicId, bool deepClone = true)
        {
            var topicSource =
                MContext.Topic.Where(t => !t.Deleted.HasValue && t.ID == topicId)
                .Include("Tasks.Attachments.Data")
                .Include("Tasks.VoiceMessages.Data")
                .Include("Tasks.DocumentLibraries.Attachment")
                .Include("Attachments.Data")
                .Include("VoiceMessages.Data")
                .Include("DocumentLibraries.Attachment")
                .FirstOrDefault();

            if (!deepClone)
                topicSource.Tasks.Clear();

            return Clone(topicSource);
        }

        /// <summary>
        /// Clone a Topic
        /// </summary>
        /// <param name="topicSource">Topic to clone</param>
        /// <returns>Topic clone</returns>
        private Topic Clone(Topic topicSource)
        {
            return topicSource.DeepClone();
        }

        /// <summary>
        /// Checks if a Topic exists by name
        /// </summary>
        /// <param name="topicGroupId">Topic Group id to search</param>
        /// <param name="topicName">Topic name to match</param>
        /// <returns></returns>
        public bool Exists(Guid topicGroupId, string topicName)
        {

            //var topics = MContext.Topic.Where(t => !t.Deleted.HasValue && t.ParentTopicGroupId == topicGroupId && string.Compare(t.Name, topicName, false) == 0);

            var topics = MContext.Topic.Where(t => !t.Deleted.HasValue && t.ParentTopicGroupId == topicGroupId && t.Name.Equals(topicName, StringComparison.Ordinal)).ToList();
            return topics.Any();
        }


        /// <summary>
        /// Checks if a Topic exists by name 
        /// </summary>
        /// <param name="topicId">Topic id</param>
        /// <param name="topicGroupId">Topic Group id to search</param>
        /// <param name="topicName">Topic name to match</param>
        /// <returns></returns>
        public bool Exists(Guid topicId, Guid topicGroupId, string topicName)
        {
            var topics = MContext.Topic.Where(t => !t.Deleted.HasValue && t.ParentTopicGroupId == topicGroupId && topicId != t.ID && t.Name.Equals(topicName, StringComparison.Ordinal)).ToList();
            return topics.Any();
        }




        #endregion

        #region base quiries

        /// <summary>
        /// Gets child Topics by Package Id
        /// </summary>
        /// <param name="packageId">Gets Topics by Package</param>
        /// <returns>Collection of Topics</returns>
        private IQueryable<Topic> GetByPackageIdQueryable(Guid packageId)
        {
            RestrictedAccess ra = new RestrictedAccess(MContext, packageId);

            var package = ra.GetPackage(packageId);

            var topics = from m in package.Modules
                         from tc in m.TopicGroups
                         from t in tc.Topics
                         select t;

            return topics.AsQueryable();
        }

        #endregion

        /// <summary>
        /// Retire a Topic
        /// </summary>
        /// <param name="topicId">Topic Id of Topic to retire</param>
        public void Retire(Guid topicId)
        {
            UpdateDeletedFields(
                DateTime.Now, topicId);
        }

        /// <summary>
        /// Restore a Topic
        /// </summary>
        /// <param name="topicId">Topic Id of Topic to restore</param>
        public void Restore(Guid topicId)
        {
            UpdateDeletedFields(
                null, topicId);
        }

        /// <summary>
        /// Delete/restore topic
        /// </summary>
        /// <param name="deleted">Set to delete, null to restore</param>
        /// <param name="topicId">Topic Id of Topic to restore</param>
        private void UpdateDeletedFields(DateTime? deleted, Guid topicId)
        {
            var topic = MContext.Topic
                            .Include(t => t.Attachments.Select(a => a.Data))
                            .Include(t => t.VoiceMessages.Select(v => v.Data))
                            .Include(t => t.DocumentLibraries.Select(d => d.Attachment.Data))
                            .Include(t => t.Tasks.Select(ta => ta.Attachments.Select(a => a.Data)))
                            .Include(t => t.Tasks.Select(ta => ta.VoiceMessages.Select(v => v.Data)))
                            .Include(t => t.Tasks.Select(ta => ta.DocumentLibraries.Select(d => d.Attachment.Data)))
                            .FirstOrDefault(t => t.ID == topicId);

            topic.Deleted = deleted;

            foreach (Attachment attachment in topic.Attachments)
            {
                attachment.Deleted = deleted;
                if (attachment.Data != null)
                    attachment.Data.Deleted = deleted;
            }

            foreach (VoiceMessage voiceMessage in topic.VoiceMessages)
            {
                voiceMessage.Deleted = deleted;
                if (voiceMessage.Data != null)
                    voiceMessage.Data.Deleted = deleted;
            }

            foreach (DocumentLibrary documentLibraryItem in topic.DocumentLibraries)
                documentLibraryItem.Deleted = deleted;

            foreach (Task task in topic.Tasks)
            {
                task.Deleted = deleted;

                foreach (Attachment attachment in task.Attachments)
                {
                    attachment.Deleted = deleted;
                    if (attachment.Data != null)
                        attachment.Data.Deleted = deleted;
                }

                foreach (VoiceMessage voiceMessage in task.VoiceMessages)
                {
                    voiceMessage.Deleted = deleted;
                    if (voiceMessage.Data != null)
                        voiceMessage.Data.Deleted = deleted;
                }

                foreach (DocumentLibrary documentLibraryItem in task.DocumentLibraries)
                    documentLibraryItem.Deleted = deleted;
            }

            MContext.SaveChanges();
        }

    }
}
