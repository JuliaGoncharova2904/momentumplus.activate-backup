﻿using System;
using System.Linq;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.People;

namespace MomentumPlus.Activate.Services
{
    /// <summary>
    /// Helper for getting Package items
    /// </summary>
    public class RestrictedAccess
    {
        #region public methods

        public Package GetPackage(Guid id)
        {
            var package =
                packages.Where(p => p.ID == id).FirstOrDefault();

            return package;
        }

        #endregion

        #region private methods

        private readonly bool deleted;
        private readonly IQueryable<Package> packages;
        private readonly IQueryable<ProjectInstance> projects;
        private readonly IQueryable<Contact> contacts;
        private readonly MomentumContext MContext;
        private readonly bool frameworkMode;
        private readonly User user;

        private IPackageService PackageService;

        #endregion

        #region public properties

        /// <summary>
        /// Topics that belong to the current instance
        /// </summary>
        public IQueryable<Topic> Topics
        {
            get { return GetTopics(); }
        }

        /// <summary>
        /// Tasks that belong to the current instance
        /// </summary>
        public IQueryable<Task> Tasks
        {
            get { return GetTasks(); }
        }

        /// <summary>
        /// Attachments that belong to the current instance
        /// </summary>
        public IQueryable<Attachment> TaskAttachments
        {
            get { return GetTaskAttachments(); }
        }

        /// <summary>
        /// Topic attachments that belong to the current intance
        /// </summary>
        public IQueryable<Attachment> TopicAttachments
        {
            get { return GetTopicAttachments(); }
        }

        /// <summary>
        /// Contact Attachments that belong to the current 
        /// instance
        /// </summary>
        public IQueryable<Attachment> ContactAttachments
        {
            get { return GetContactAttachments(); }
        }

        /// <summary>
        /// Meeting Attachments that belong to the current
        /// instance
        /// </summary>
        public IQueryable<Attachment> MeetingAttachments
        {
            get { return GetMeetingAttachments(); }
        }

        /// <summary>
        /// Position Attachments that belong the the current
        /// instance
        /// </summary>
        public IQueryable<Attachment> PositionAttachments
        {
            get { return GetPositionAttachments(); }
        }

        /// <summary>
        /// Task Voice Messages that belong to the current
        /// instance
        /// </summary>
        public IQueryable<VoiceMessage> TaskVoiceMessages
        {
            get { return GetTaskVoiceMessages(); }
        }

        /// <summary>
        /// Topic Voice Messages that belong to the current 
        /// instance
        /// </summary>
        public IQueryable<VoiceMessage> TopicVoiceMessages
        {
            get { return GetTopicVoiceMessages(); }
        }

        /// <summary>
        /// Contact Voice Messages that belong to the current
        /// instance
        /// </summary>
        public IQueryable<VoiceMessage> ContactVoiceMessages
        {
            get { return GetContactVoiceMessages(); }
        }

        /// <summary>
        /// Meeting Voice Messages that belong to the 
        /// current instance
        /// </summary>
        public IQueryable<VoiceMessage> MeetingVoiceMessages
        {
            get { return GetMeetingVoiceMessages(); }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Contructor for that creates context
        /// for the Restricted Access 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="packageId"></param>
        /// <param name="frameworkMode"></param>
        public RestrictedAccess(MomentumContext context, Guid packageId, bool frameworkMode = false)
        {
            MContext = context;

            this.frameworkMode =
                frameworkMode;

            packages =
                context.Package.Where(p => p.ID == packageId);

            projects =
                context.ProjectInstance.Where(p => p.PackageId == packageId);

            contacts =
                context.Contact.Where(p => p.PackageId == packageId);
        }

        /// <summary>
        /// Contreuctor that creates context
        /// for the Restricted Access
        /// </summary>
        /// <param name="context"></param>
        /// <param name="user"></param>
        /// <param name="deleted"></param>
        /// <param name="frameworkMode"></param>
        public RestrictedAccess(MomentumContext context, User user, bool deleted = false, bool frameworkMode = false)
        {
            this.deleted =
                deleted;

            MContext = context;

            this.user =
                user;

            this.frameworkMode =
                frameworkMode;
        }

        #endregion

        #region private get query builders

        /// <summary>
        /// Gets Modules
        /// </summary>
        /// <returns></returns>
        private IQueryable<Module> GetModules()
        {
            var modules = (from p in packages
                from m in p.Modules
                where m.Deleted.HasValue == deleted &&
                      m.IsFrameworkDefault == frameworkMode
                select m);

            return modules;
        }

        /// <summary>
        /// Get Projects query helper
        /// </summary>
        /// <returns></returns>
        private IQueryable<ProjectInstance> GetProjects()
        {
            var projectInstances = (from p in projects
                where p.Deleted.HasValue == deleted
                select p);

            return projectInstances;
        }

        /// <summary>
        /// Get Positions query helper
        /// </summary>
        /// <returns></returns>
        private IQueryable<Position> GetPositions()
        {
            var positions = (from p in packages
                where p.Deleted.HasValue == deleted
                select p.Position).AsQueryable();

            return positions;
        }

        /// <summary>
        /// Get Topic Groups query helper
        /// </summary>
        /// <returns></returns>
        private IQueryable<TopicGroup> GetTopicGroups()
        {
            var modules = GetModules();

            var topicGroups = (from m in modules
                from tg in m.TopicGroups
                where tg.Deleted.HasValue == deleted
                select tg);

            return topicGroups;
        }

        /// <summary>
        /// Get Topics query helper
        /// </summary>
        /// <returns></returns>
        private IQueryable<Topic> GetTopics()
        {
            var topicGroups = GetTopicGroups();

            var topics = (from tg in topicGroups
                from to in tg.Topics
                where to.Deleted.HasValue == deleted
                select to);

            return topics;
        }

        /// <summary>
        /// Get Tasks query helper
        /// </summary>
        /// <returns></returns>
        private IQueryable<Task> GetTasks()
        {
            var topics = GetTopics();
            var projectsInstances = GetProjects();

            var tasks = (from to in topics
                from ta in to.Tasks
                where ta.Deleted.HasValue == deleted
                select ta)
                .Union
                (from p in projectsInstances
                    from tp in p.Tasks
                    where tp.Deleted.HasValue == deleted
                    select tp)
                .Union
                (from c in contacts
                    from t in c.Tasks
                    where t.Deleted.HasValue == deleted
                    select t);

            return tasks;
        }

        /// <summary>
        /// Get Contacts query helper
        /// </summary>
        /// <returns></returns>
        private IQueryable<Contact> GetContacts()
        {
            var contactList = (from p in packages
                from a in p.Contacts
                where a.Deleted.HasValue == deleted
                select a);

            return contactList;
        }

        /// <summary>
        /// Get Meetings query helper
        /// </summary>
        /// <returns></returns>
        private IQueryable<Meeting> GetMeetings()
        {
            var contacts = GetContacts();

            var meetings = (from c in contacts
                from m in c.Meetings
                where m.Deleted.HasValue == deleted
                select m);

            return meetings;
        }

        /// <summary>
        /// Get Task Attachments query helpers
        /// </summary>
        /// <returns></returns>
        private IQueryable<Attachment> GetTaskAttachments()
        {
            var tasks = GetTasks();

            var attachments = (from ta in tasks
                from a in ta.Attachments
                where a.Deleted.HasValue == deleted
                select a);

            return attachments;
        }

        /// <summary>
        /// Get Topic Attachments query helpers
        /// </summary>
        /// <returns></returns>
        private IQueryable<Attachment> GetTopicAttachments()
        {
            var topics = GetTopics();

            var attachments = (from to in topics
                from a in to.Attachments
                where a.Deleted.HasValue == deleted
                select a);

            return attachments;
        }

        /// <summary>
        /// Get Contact Attachments query helpers
        /// </summary>
        /// <returns></returns>
        private IQueryable<Attachment> GetContactAttachments()
        {
            var tasks = GetContacts();

            var attachments = (from ta in tasks
                from a in ta.Attachments
                where a.Deleted.HasValue == deleted
                select a);

            return attachments;
        }

        /// <summary>
        /// Get Meeting Attachments query helpers
        /// </summary>
        /// <returns></returns>
        private IQueryable<Attachment> GetMeetingAttachments()
        {
            var meetings = GetMeetings();

            var attachments = (from m in meetings
                from a in m.Attachments
                where a.Deleted.HasValue == deleted
                select a);

            return attachments;
        }

        /// <summary>
        /// Get Position Attachments query helper
        /// </summary>
        /// <returns></returns>
        private IQueryable<Attachment> GetPositionAttachments()
        {
            var positions = GetPositions();

            var attachments = (from m in positions
                from a in m.Attachments
                where a.Deleted.HasValue == deleted
                select a);

            return attachments;
        }

        /// <summary>
        /// Get Task Voice Messages query helper
        /// </summary>
        /// <returns></returns>
        private IQueryable<VoiceMessage> GetTaskVoiceMessages()
        {
            var tasks = GetTasks();

            var voiceMessages = (from ta in tasks
                from v in ta.VoiceMessages
                where v.Deleted.HasValue == deleted
                select v);

            return voiceMessages;
        }

        /// <summary>
        /// Get Topic Voice Messages query helper
        /// </summary>
        /// <returns></returns>
        private IQueryable<VoiceMessage> GetTopicVoiceMessages()
        {
            var topics = GetTopics();

            var voiceMessages = (from to in topics
                from v in to.VoiceMessages
                where v.Deleted.HasValue == deleted
                select v);

            return voiceMessages;
        }

        /// <summary>
        /// Get Contact Voice Messages query helper
        /// </summary>
        /// <returns></returns>
        private IQueryable<VoiceMessage> GetContactVoiceMessages()
        {
            var tasks = GetContacts();

            var voiceMessages = (from ta in tasks
                from v in ta.VoiceMessages
                where v.Deleted.HasValue == deleted
                select v);

            return voiceMessages;
        }

        /// <summary>
        /// Get Meetings Voice Messages helper
        /// </summary>
        /// <returns></returns>
        private IQueryable<VoiceMessage> GetMeetingVoiceMessages()
        {
            var meetings = GetMeetings();

            var voiceMessages = (from m in meetings
                from v in m.VoiceMessages
                where v.Deleted.HasValue == deleted
                select v);

            return voiceMessages;
        }

        #endregion
    }
}