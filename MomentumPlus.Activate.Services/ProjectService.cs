﻿using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Activate.Services.Interfaces;
using System.Linq.Expressions;
using System.Data.Entity;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.Moderation;
using MomentumPlus.Core.Models.Requests;
using MomentumPlus.Core.Extensions;

namespace MomentumPlus.Activate.Services
{
    /// <summary>
    /// Handles Project Module
    /// </summary>
    public class ProjectService : ServiceBase, IProjectService
    {
        public ProjectService(MomentumContext context) : base(context) { }

        /// <summary>
        /// Gets Project instances by Package Id
        /// </summary>
        /// <param name="packageId">Package Id</param>
        /// <returns>Collection of Project</returns>
        public IEnumerable<ProjectInstance> GetProjectInstances(Guid packageId)
        {
            var projects = MContext.ProjectInstance
                .Include(p => p.Moderations)
                .Include("Project.Status")
                .Include("Project.Role")
                .Include("Project.ProjectManager")
                .Where(
                    p => !p.Deleted.HasValue && p.PackageId == packageId);

            return projects;
        }

        /// <summary>
        /// Gets Project instances by Package Id
        /// </summary>
        /// <param name="packageId">Package Id</param>
        /// <param name="statusReferenceId">Status Reference Id</param>
        /// <param name="sortColumn">Sort column for results</param>
        /// <param name="sortAscending">Sort order for results</param>
        /// <param name="deleted">Return deleted</param>
        /// <param name="approved">Retuen approved</param>
        /// <returns>Collection of Project</returns>
        public IEnumerable<ProjectInstance> GetProjectInstances(Guid packageId, int? statusReferenceId, ProjectInstance.SortColumn? sortColumn, bool sortAscending, bool deleted = false, bool? approved = true)
        {
            var projects = MContext.ProjectInstance
                .Include(p => p.Moderations)
                .Include("Project.Status")
                .Include("Project.Role")
                .Include("Project.ProjectManager")
                .Where(
                    p =>
                        p.Deleted.HasValue == deleted &&
                        (!statusReferenceId.HasValue || p.Project.Status.ReferenceId == statusReferenceId.Value)
                        && p.PackageId == packageId);

            if (approved.HasValue)
                projects = approved.Value
                    ? projects.Where(Moderated.Approved<ProjectInstance>())
                    : projects.Where(Moderated.Declined<ProjectInstance>());
                                                                            
            return projects;
        }

        /// <summary>
        /// Get Project instances by Package Id
        /// </summary>
        /// <param name="packageId">Package Id</param>
        /// <param name="statusReferenceId">Status Reference Id</param>
        /// <param name="pageNo">Page no.</param>
        /// <param name="pageSize">Size of the page</param>
        /// <param name="sortColumn">Results sort column</param>
        /// <param name="sortAscending">Sort ascending</param>
        /// <param name="deleted">Return deleted</param>
        /// <param name="approved">Return approves</param>
        /// <returns>Collection of Project instances</returns>
        public IEnumerable<ProjectInstance> GetProjectInstances(Guid packageId, int? statusReferenceId, int pageNo, int pageSize, ProjectInstance.SortColumn? sortColumn, bool sortAscending, bool deleted = false, bool? approved = true)
        {
            var projects = GetProjectInstances(packageId, statusReferenceId, sortColumn, sortAscending, deleted,
                approved).AsQueryable();

            if (sortColumn != null)
            {

                if(!typeof(ProjectInstance).HasPropery(sortColumn.ToString()))
                    SortAndPage(ref projects, p => p.Project, sortColumn.ToString(), sortAscending, pageNo, pageSize);
                else
                    SortAndPage(ref projects, p => p, sortColumn.ToString(), sortAscending, pageNo, pageSize);

                
            }

            return projects;
        }

        /// <summary>
        /// Count Project instances by Package
        /// </summary>
        /// <param name="packageId">Package Id</param>
        /// <param name="statusReferenceId">Status Reference Id</param>
        /// <param name="deleted">Return deleted</param>
        /// <param name="approved">Return approved</param>
        /// <returns>Number of Project instances</returns>
        public int CountProjectIntances(Guid packageId, int? statusReferenceId, bool deleted = false, bool? approved = true)
        {
            return GetProjectInstances(packageId, statusReferenceId, ProjectInstance.SortColumn.Name, true, deleted,
                approved).Count();
        }

        /// <summary>
        /// Add a Project
        /// </summary>
        /// <param name="pInstance"></param>
        /// <returns></returns>
        public ProjectInstance Add(ProjectInstance pInstance)
        {
            pInstance = MContext.ProjectInstance.Add(pInstance);

            MContext.SaveChanges();

            return pInstance;
        }

        /// <summary>
        /// Get a Project instance by Project id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ProjectInstance GetProjectInstanceById(Guid id)
        {
            var project = MContext.ProjectInstance.Find(id);

            return project;
        }

        /// <summary>
        /// Get Project instance by Package and Project Id
        /// </summary>
        /// <param name="packageId"></param>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public ProjectInstance GetProjectInstanceByPackageAndProjectId(Guid packageId, Guid projectId)
        {
            var project = GetProjectInstances(packageId).FirstOrDefault(p => p.ProjectId == projectId);

            return project;
        }

        /// <summary>
        /// Update a Project instance
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        public ProjectInstance Update(ProjectInstance project)
        {
            MContext.Entry(project).State = EntityState.Modified;

            MContext.SaveChanges();
 
            return project;
        }

        /// <summary>
        /// Get Projects
        /// </summary>
        /// <param name="deleted"></param>
        /// <returns></returns>
        public IEnumerable<Project> GetProjects(bool deleted = false)
        {
            return MContext.Project.Where(p => p.Deleted.HasValue == deleted)
                                    .Include(p => p.Status)
                                    .Include(p => p.Role)
                                    .Include(p => p.Image)
                                    .AsQueryable();
        }
        
        /// <summary>
        /// Get Projects
        /// </summary>
        /// <param name="statusReferenceId"></param>
        /// <param name="pageNo"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortAscending"></param>
        /// <param name="deleted"></param>
        /// <returns></returns>
        public IEnumerable<Project> GetProjects(int? statusReferenceId, int pageNo, int pageSize, Project.SortColumn? sortColumn, bool sortAscending, bool deleted = false)
        {
            var projects = MContext.Project.Where(p => p.Deleted.HasValue == deleted && (!statusReferenceId.HasValue || p.Status.ReferenceId == statusReferenceId.Value))
                                                        .Include(p => p.Status)
                                                        .Include(p => p.Role)
                                                        .Include(p => p.Image)
                                                        .AsQueryable();

            if (sortColumn != null)
            {
                SortAndPage(ref projects, p => p, sortColumn.ToString(), sortAscending, pageNo, pageSize);
            }

            return projects;
        }

        /// <summary>
        /// Count Projects
        /// </summary>
        /// <param name="statusReferenceId">Status Reference Id</param>
        /// <param name="deleted">Return deleted</param>
        /// <returns>Number of Projects</returns>
        public int CountProjects(int? statusReferenceId = null, bool deleted = false)
        {
            return MContext.Project.Count(p => p.Deleted.HasValue == deleted && (!statusReferenceId.HasValue || p.Status.ReferenceId == statusReferenceId.Value));
        }

        /// <summary>
        /// Get Project by Id
        /// </summary>
        /// <param name="id">Project Id</param>
        /// <returns>Project</returns>
        public Project GetProjectById(Guid id)
        {
            var project = MContext.Project.Find(id);

            return project;
        }

        /// <summary>
        /// Add Project
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        public Project Add(Project project)
        {
            project = MContext.Project.Add(project);

            MContext.SaveChanges();

            return project;
        }

        /// <summary>
        /// Update a Project
        /// </summary>
        /// <param name="project">Project to update</param>
        /// <returns>Project</returns>
        public Project Update(Project project)
        {
            MContext.Entry(project).State = EntityState.Modified;

            MContext.SaveChanges();

            return project;
        }

        /// <summary>
        /// Gets a Project instance with all the child entities
        /// pre-loaded
        /// </summary>
        /// <param name="expression">Expression</param>
        /// <param name="deleted">Return deleted</param>
        /// <returns>Project</returns>
        private Project GetDeep(Expression<Func<Project, bool>> expression, bool deleted = false)
        {
            var result = MContext.Project.Where(expression).Where(p => p.Deleted.HasValue == deleted).
                     Include(e => e.Tasks.Select(e1 => e1.Attachments.Select(e2 => e2.Data))).
                     Include(e => e.Tasks.Select(e1 => e1.VoiceMessages.Select(e2 => e2.Data))).
                     Include(e => e.Tasks.Select(e1 => e1.DocumentLibraries.Select(e2 => e2.Attachment))).
                     Include(e => e.Meetings).
                     Include(e => e.Attachments.Select(e1 => e1.Data)).
                     Include(e => e.VoiceMessages.Select(e1 => e1.Data)).
                     Include(e => e.DocumentLibraries.Select(e1 => e1.Attachment)).
                     FirstOrDefault();

            return result;
        }

        /// <summary>
        /// Gets a Project will all the child
        /// entities pre-loaded
        /// </summary>
        /// <param name="expression">Expression</param>
        /// <param name="deleted">Return deleted</param>
        /// <returns>Project</returns>
        private ProjectInstance GetDeepInstance(Expression<Func<ProjectInstance, bool>> expression, bool deleted = false)
        {
            var result = MContext.ProjectInstance.Where(expression).Where(p => p.Deleted.HasValue == deleted).
                     Include(e => e.Tasks.Select(e1 => e1.Attachments.Select(e2 => e2.Data))).
                     Include(e => e.Tasks.Select(e1 => e1.VoiceMessages.Select(e2 => e2.Data))).
                     Include(e => e.Tasks.Select(e1 => e1.DocumentLibraries.Select(e2 => e2.Attachment))).
                     Include(e => e.Meetings).
                     Include(e => e.Attachments.Select(e1 => e1.Data)).
                     Include(e => e.VoiceMessages.Select(e1 => e1.Data)).
                     Include(e => e.DocumentLibraries.Select(e1 => e1.Attachment)).
                     FirstOrDefault();

            return result;
        }

        /// <summary>
        /// Retires a Project
        /// </summary>
        /// <param name="projectId">Project Id</param>
        public void Retire(Guid projectId)
        {
            Retire(GetDeep(p => p.ID == projectId));
        }

        /// <summary>
        /// Retires a Project
        /// </summary>
        /// <param name="project">Project</param>
        public void Retire(Project project)
        {
            project.Retire();

            MContext.SaveChanges();
        }

        /// <summary>
        /// Restores a retired Package
        /// </summary>
        /// <param name="projectId">Project Id</param>
        public void Restore(Guid projectId)
        {
            Restore(GetDeep(p => p.ID == projectId, true));
        }

        /// <summary>
        /// Restores a retired Packages
        /// </summary>
        /// <param name="project">Project</param>
        public void Restore(Project project)
        {
            project.Restore();

            MContext.SaveChanges();
        }

        /// <summary>
        /// Retires a Project
        /// </summary>
        /// <param name="projectId">Project Id</param>
        public void RetireInstance(Guid projectId)
        {
            RetireInstance(GetDeepInstance(p => p.ID == projectId));
        }

        /// <summary>
        /// Retires a Project
        /// </summary>
        /// <param name="project">Project instance</param>
        public void RetireInstance(ProjectInstance project)
        {
            project.Retire();

            MContext.SaveChanges();
        }

        /// <summary>
        /// Restores a Project
        /// </summary>
        /// <param name="projectId">Project Id</param>
        public void RestoreInstance(Guid projectId)
        {
            RestoreInstance(GetDeepInstance(p => p.ID == projectId, true));
        }

        /// <summary>
        /// Restores a Project
        /// </summary>
        /// <param name="project">Project instance</param>
        public void RestoreInstance(ProjectInstance project)
        {
            project.Restore();

            MContext.SaveChanges();
        }

        /// <summary>
        /// Gets all Project Statuses 
        /// </summary>
        /// <returns>Project Statuses</returns>
        public IQueryable<ProjectStatus> GetProjectStatuses()
        {
            return MContext.ProjectStatus.OrderBy(s => s.Order);
        }

        /// <summary>
        /// Gets all Project roles
        /// </summary>
        /// <returns>Project Roles</returns>
        public IQueryable<ProjectRole> GetProjectRoles()
        {
            return MContext.ProjectRole.OrderBy(r => r.Order);
        }

        /// <summary>
        /// Deletes a Project
        /// </summary>
        /// <param name="id">Project instance id</param>
        public void DeleteInstance(Guid id)
        {
            var projectInstance = MContext.ProjectInstance.Find(id);

            DeleteInstance(projectInstance);
        }

        /// <summary>
        /// Deletes a Project
        /// </summary>
        /// <param name="project">Project instance to deleted</param>
        public void DeleteInstance(ProjectInstance project)
        {
            if (project.NewProjectRequest != null)
            {
                MContext.Request.Remove(project.NewProjectRequest);
            }

            foreach (var attachment in project.Attachments.ToList())
            {
                MContext.Attachment.Remove(attachment);
            }

            foreach (var voiceMessage in project.VoiceMessages.ToList())
            {
                MContext.VoiceMessages.Remove(voiceMessage);
            }

            foreach (var meeting in project.Meetings.ToList())
            {
                MContext.Meeting.Remove(meeting);
            }

            MContext.ProjectInstance.Remove(project);

            MContext.SaveChanges();
        }

        /// <summary>
        /// Creates a Project
        /// </summary>
        /// <param name="projectInstance">Project Instance to create</param>
        /// <param name="suggestedName">Suggested name</param>
        /// <param name="suggestedId">Suggested Project Id</param>
        public void CreateProjectInstance(ProjectInstance projectInstance, string suggestedName, string suggestedId)
        {
            MContext.ProjectInstance.Add(projectInstance);

            if (!projectInstance.ProjectId.HasValue)
            {
                var projectRequest = new NewProjectRequest()
                {
                    PackageId = projectInstance.PackageId,
                    ProjectInstance = projectInstance,
                    SuggestedName = suggestedName,
                    SuggetestedProjectId = suggestedId
                };

                MContext.Request.Add(projectRequest);
            }

            MContext.SaveChanges();
        }

        /// <summary>
        /// Gets Projects by Period Ids
        /// </summary>
        /// <param name="periodId">Period Id</param>
        /// <returns></returns>
        public IEnumerable<ProjectInstance> GetProjectInstancesByPeriodId(Guid periodId)
        {
            return MContext.ProjectInstance.Where(p => p.PeriodId == periodId);
        }

        /// <summary>
        /// Returns the number of approved
        /// Projects
        /// </summary>
        /// <param name="id">Package Id</param>
        /// <returns></returns>
        public int CountProjectsApproved(Guid id)
        {
            return GetProjectInstances(id).Where(p => p.IsApproved()).ToList().Count();
        }

        /// <summary>
        /// Gets last created Project
        /// </summary>
        /// <param name="id">Pacage Id</param>
        /// <returns></returns>
        public DateTime? GetDateLastEntry(Guid id)
        {
            var projects = GetProjectInstances(id).OrderBy(s => s.Created);
            return projects.Any() ? projects.Last().Created : null;
        }

        /// <summary>
        /// Gets Project by associated Meeting
        /// </summary>
        /// <param name="meetingId">Meeting id</param>
        /// <returns></returns>
        public ProjectInstance GetByMeetingId(Guid meetingId)
        {
            return MContext.Meeting.Where(m => m.ID == meetingId).Select(m => m.ProjectInstance).FirstOrDefault();
        }

        /// <summary>
        /// Gets Project by associated Task
        /// </summary>
        /// <param name="taskId">Task id</param>
        /// <returns></returns>
        public ProjectInstance GetByTaskId(Guid taskId)
        {
            return MContext.Task.Where(m => m.ID == taskId).Select(m => m.ProjectInstace).FirstOrDefault();
        }
    }
}
