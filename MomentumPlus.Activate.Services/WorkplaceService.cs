﻿using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Activate.Services.Interfaces;
using System.Data.Entity;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Services
{
    /// <summary>
    /// Workplace Service
    /// </summary>
    public class WorkplaceService : ServiceBase, IWorkplaceService
    {
        public WorkplaceService(MomentumContext context) : base(context) { }

        #region IWorkplaceService Members

        /// <summary>
        /// Add a Workplace
        /// </summary>
        /// <param name="workplace"></param>
        /// <returns></returns>
        public Workplace Add(Workplace workplace)
        {
            workplace =
                MContext.Workplace.Add(workplace);

            MContext.SaveChanges();

            return workplace;
        }

        /// <summary>
        /// Gets all active Workplaces
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Workplace> GetAll()
        {
            var workplaces =
                MContext.Workplace.Where(w => !w.Deleted.HasValue);

            return workplaces.ToList();
        }

        /// <summary>
        /// Gets Workplace by Id
        /// </summary>
        /// <param name="workplaceId"></param>
        /// <returns></returns>
        public Workplace GetByWorkplaceId(Guid workplaceId)
        {
            var workplace =
                MContext.Workplace.Find(workplaceId);

            return workplace;
        }

        /// <summary>
        /// Updates a Workplace
        /// </summary>
        /// <param name="workplace"></param>
        /// <returns></returns>
        public Workplace Update(Workplace workplace)
        {
            MContext.Entry(workplace).State = EntityState.Modified;

            MContext.SaveChanges();

            return workplace;
        }

        /// <summary>
        /// Deletes a Workplace
        /// </summary>
        /// <param name="workplaceId"></param>
        public void Delete(Guid workplaceId)
        {
            var workplace = MContext.Workplace.Find(workplaceId);

            MContext.Workplace.Remove(workplace);

            MContext.SaveChanges();
        }

        /// <summary>
        /// Checks to see if a Workplace exists
        /// </summary>
        /// <param name="workplaceName"></param>
        /// <returns></returns>
        public bool Exists(string workplaceName)
        {
            bool exists = false;

            var workplace =
                MContext.Workplace.FirstOrDefault(p => !p.Deleted.HasValue && p.Name == workplaceName);

            if (workplace != null)
                exists = true;

            return exists;
        }

        /// <summary>
        /// Search for a Workplace
        /// </summary>
        /// <param name="query"></param>
        /// <param name="pageNo"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortAscending"></param>
        /// <returns></returns>
        public IEnumerable<Workplace> Search(string query, int pageNo, int pageSize, Workplace.SortColumn sortColumn, bool sortAscending)
        {
            IEnumerable<Workplace> searchResults =
                 SearchBase(query, pageNo, pageSize, sortColumn, sortAscending);

            return searchResults;
        }

        /// <summary>
        /// Count number of search results based on the query
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public int SearchCount(string query)
        {
            IEnumerable<Workplace> searchResults = SearchBase(query);

            return searchResults == null ? 0 : searchResults.Count();
        }

        /// <summary>
        /// Search Workplaces
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        private IQueryable<Workplace> SearchBase(string query)
        {
            IQueryable<Workplace> workplaces =
                 MContext.Workplace.Where(w => !w.Deleted.HasValue);

            if (!string.IsNullOrWhiteSpace(query))
            {
                workplaces =
                    workplaces.Where(p => p.Name.Contains(query)).AsQueryable();
            }

            return workplaces;
        }

        /// <summary>
        /// Pager helper
        /// </summary>
        /// <param name="query"></param>
        /// <param name="pageNo"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortAscending"></param>
        /// <returns></returns>
        private IQueryable<Workplace> SearchBase(string query, int pageNo, int pageSize, Workplace.SortColumn sortColumn, bool sortAscending)
        {
            IQueryable<Workplace> workplaces = SearchBase(query);

            SortAndPage<Workplace>(pageNo, pageSize, sortColumn.ToString(), sortAscending, ref workplaces);

            return workplaces;
        }

        #endregion
    }
}
