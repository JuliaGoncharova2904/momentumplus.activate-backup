﻿using System.Linq;
using MomentumPlus.Activate.Services.Interfaces;
using System.Data.Entity;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models;

using System;

namespace MomentumPlus.Activate.Services
{
    /// <summary>
    /// Account Configuration Service
    /// </summary>
    public class AccountConfigService: ServiceBase, IAccountConfigService
    {
        public AccountConfigService(MomentumContext context) : base(context) { }

        #region IAccountConfigService Members

        /// <summary>
        /// Get the single account config entity
        /// </summary>
        /// <returns></returns>
        public AccountConfig Get()
        {
            var accountConfig = MContext.AccountConfig.Include(s => s.Logo).FirstOrDefault();

            return accountConfig;
        }

        /// <summary>
        /// Update the account config in the database
        /// </summary>
        /// <param name="accountConfig"></param>
        /// <returns></returns>
        public AccountConfig Update(AccountConfig accountConfig)
        {
            MContext.Entry(accountConfig).State = EntityState.Modified;
            MContext.SaveChanges();

            return accountConfig;
        }

        #endregion
    }
}
