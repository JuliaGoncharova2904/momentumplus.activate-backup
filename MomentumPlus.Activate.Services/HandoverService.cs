﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using MomentumPlus.Activate.Services.Extensions;
using MomentumPlus.Activate.Services.Helpers;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models.People;
using MomentumPlus.Core.Models.Handover;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.Moderation;

namespace MomentumPlus.Activate.Services
{
    /// <summary>
    /// Handover Service
    /// </summary>
    public class HandoverService : ServiceBase, IHandoverService
    {
        public HandoverService(MomentumContext context, IContactService contactService,
            IMeetingService meetingService, IPackageService packageService, IToDoItemService toDoItemService,
            IPeopleService peopleService, IProjectService projectService, ITaskService taskService)
            : base(context)
        {
            ContactService = contactService;
            MeetingService = meetingService;
            PackageService = packageService;
            ToDoItemService = toDoItemService;
            PeopleService = peopleService;
            ProjectService = projectService;
            TaskService = taskService;

            LeaverOpeningMeetingForExitTypeId = new Guid("eb32c795-c669-46e7-bcb6-9a1144ec5e00");
            LeaverMidPointReviewMeetingTypeId = new Guid("54ac3645-f398-44ee-b4f7-d7275a372ad0");
            LeaverHandoverMeetingTypeId = new Guid("39e04c11-a5d5-4843-b0fb-1a1f9fdf048f");

            OnboardingKickOffHandoverMeetingTypeId = new Guid("5112aa67-b3ec-4a0d-a918-c76326db0890");
            OnboardingMidPointReviewMeetingTypeId = new Guid("a768ea83-1814-4245-b76f-80862dc49479");
            OnboardingEndReviewMeetingTypeId = new Guid("6a515ca0-fd67-4bf2-9d43-c2225cb1c554");

            MidYearReviewMeetingTypeId = new Guid("ba7b2460-2150-4557-9fba-80c93da8c9b6");

            DefaultMeetingStartTimeHour = 10;
            DefaultMeetingDurationMinutes = 60;
        }

        private IContactService ContactService { get; set; }
        private IMeetingService MeetingService { get; set; }
        private IPackageService PackageService { get; set; }
        private IToDoItemService ToDoItemService { get; set; }
        private IPeopleService PeopleService { get; set; }
        private IProjectService ProjectService { get; set; }
        private ITaskService TaskService { get; set; }
        private Guid LeaverOpeningMeetingForExitTypeId { get; set; }
        private Guid LeaverMidPointReviewMeetingTypeId { get; set; }
        private Guid LeaverHandoverMeetingTypeId { get; set; }
        private Guid OnboardingKickOffHandoverMeetingTypeId { get; set; }
        private Guid OnboardingMidPointReviewMeetingTypeId { get; set; }
        private Guid OnboardingEndReviewMeetingTypeId { get; set; }
        private Guid MidYearReviewMeetingTypeId { get; set; }
        private int DefaultMeetingStartTimeHour { get; set; }
        private int DefaultMeetingDurationMinutes { get; set; }

        #region IHandoverService Members

        /// <summary>
        /// Adds a Handover 
        /// </summary>
        /// <param name="handover">Handover to add</param>
        /// <returns>Handover</returns>
        public Handover Add(Handover handover)
        {
            handover = MContext.Handover.Add(handover);
            MContext.SaveChanges();
            return handover;
        }

        /// <summary>
        /// Gets a Handover by Id
        /// </summary>
        /// <param name="handoverId">Id of Handover to return</param>
        /// <returns>Handover</returns>
        public Handover GetById(Guid handoverId)
        {
            var handover = MContext.Handover.Find(handoverId);
            return handover;
        }

        /// <summary>
        /// Updates a Handover
        /// </summary>
        /// <param name="handover">Handover to update</param>
        /// <returns>Handover</returns>
        public Handover Update(Handover handover)
        {
            MContext.Entry(handover).State = EntityState.Modified;
            MContext.SaveChanges();
            return handover;
        }

        /// <summary>
        /// Deletes a Handover
        /// </summary>
        /// <param name="handoverId">Id of Handover to delete</param>
        public void Delete(Guid handoverId)
        {
            var handover = MContext.Handover.Find(handoverId);

            if (handover.LeavingPeriod.Locked.HasValue)
            {
                UnlockLeavingPeriod(handover);
            }

            if (handover.OnboardingPeriod.Locked.HasValue)
            {
                UnlockOnboardingPeriod(handover);
            }

            MContext.Handover.Remove(handover);
            MContext.SaveChanges();
        }

        /// <summary>
        /// Locks the leaving period for a Handover
        /// </summary>
        /// <param name="handover">Handover leaving period to lock</param>
        public void LockLeavingPeriod(Handover handover)
        {
            // Set dates
            handover.LeavingPeriod.StartPoint = DateHelper.CalculateStartPoint(handover.LeavingPeriod);
            handover.LeavingPeriod.MidPoint = DateHelper.CalculateMidPoint(handover.LeavingPeriod);
            handover.LeavingPeriod.Locked = DateTime.Now;
            
            var contactId = GetLineManagerContactId(handover.Package);
            var lineManagerPackageId = GetOldestPackageId(handover.Package.UserLineManager);
            var hrManagerPackageId = GetOldestPackageId(handover.Package.UserHRManager);

            A1(handover, contactId);
            A2(handover);
            A3(handover);
            A3a(handover);
            A4(handover);
            A4a(handover);
            A5(handover, lineManagerPackageId, hrManagerPackageId);

            handover.Package.PackageStatus = Package.Status.Triggered;

            SetLeaverReviewByDates(handover.Package);

            MContext.SaveChanges();
        }

        /// <summary>
        /// Unlocks the leaving period for a Handover
        /// </summary>
        /// <param name="handover">Handover leaving period to unlock</param>
        public void UnlockLeavingPeriod(Handover handover)
        {
            // Clear dates
            handover.LeavingPeriod.StartPoint = null;
            handover.LeavingPeriod.MidPoint = null;
            handover.LeavingPeriod.Locked = null;
            handover.LeavingPeriod.Priorities.Saved = null;

            UndoPeriod(handover.LeavingPeriodId.Value);

            handover.Package.PackageStatus = Package.Status.InProgress;

            ClearLeaverReviewByDates(handover.Package);

            MContext.SaveChanges();
        }

        /// <summary>
        /// Locks the handover Onboarding period
        /// </summary>
        /// <param name="handover">Handover onboarding period to lock</param>
        public void LockOnboardingPeriod(Handover handover)
        {
            // Set dates
            handover.OnboardingPeriod.LastDay = DateHelper.CalculateLastDay(handover.OnboardingPeriod);
            handover.OnboardingPeriod.MidPoint = DateHelper.CalculateMidPoint(handover.OnboardingPeriod);
            handover.OnboardingPeriod.Locked = DateTime.Now;

            var onboarderPackage = PackageService.GetByPackageId(handover.OnboardingPeriod.PackageId.Value);
            onboarderPackage.AncestorPackageId = handover.Package.ID;

            // Carry over relationships
            if (handover.OnboardingPeriod.CarryOverRelationships)
            {
                // Only carry over active, approved contacts that don't already exist in
                // the target package (de-dupe by email address).
                var approvedContacts =
                    handover.Package.Contacts.Where(c => !c.Deleted.HasValue)
                        .Where(Moderated.Approved<Contact>().Compile())
                        .Where(a => !onboarderPackage.Contacts.Any(b => string.Equals(a.Email, b.Email, StringComparison.CurrentCultureIgnoreCase)))
                        .ToList();

                // Keep a record of the mapping from source contact to cloned contact so that
                // indirect relationships can be repaired following the clone.
                var sourceMap = new Dictionary<Guid, Guid>();

                foreach (
                    var contact in
                       approvedContacts)
                {
                    var clonedContact = contact.DeepClone();

                    sourceMap[contact.ID] = clonedContact.ID;

                    clonedContact.PeriodId = handover.OnboardingPeriodId;
                    clonedContact.Package = onboarderPackage;
                    clonedContact.PackageId = onboarderPackage.ID;

                    // Set start and end dates on the cloned meetings based on target week
                    foreach (var meeting in clonedContact.Meetings)
                    {
                        var targetWeek = meeting.TargetWeek ?? 1;
                        if (handover.OnboardingPeriod != null && handover.OnboardingPeriod.Duration != null && targetWeek > handover.OnboardingPeriod.Duration)
                            targetWeek = handover.OnboardingPeriod.Duration.Value;

                        meeting.Start =
                            handover.OnboardingPeriod.StartPoint.Value.AddDays((targetWeek - 1) * 7)
                                .Friday().AddHours(9);
                        meeting.End = meeting.Start;
                    }

                    MContext.Contact.Add(clonedContact);
                }

                MContext.SaveChanges();

                // Fix indirect relationship links
                foreach (var contact in onboarderPackage.Contacts.Where(contact =>
                    contact.IndirectRelationshipContactId.HasValue &&
                    sourceMap.ContainsKey(contact.IndirectRelationshipContactId.Value)))
                {
                    contact.IndirectRelationshipContactId = sourceMap[contact.IndirectRelationshipContactId.Value];
                }

                MContext.SaveChanges();
            }

            // Carry over projects
            if (handover.OnboardingPeriod.CarryOverProjects)
            {
                foreach (
                    var project in
                        handover.Package.Projects.Where(p => !p.Deleted.HasValue)
                            .Where(Moderated.Approved<ProjectInstance>().Compile()).ToList())
                {
                    var clonedProjectInstance = project.DeepClone();
                    clonedProjectInstance.PeriodId = handover.OnboardingPeriodId;
                    clonedProjectInstance.Package = onboarderPackage;
                    clonedProjectInstance.PackageId = onboarderPackage.ID;

                    foreach (var meeting in clonedProjectInstance.Meetings.ToList())
                    {
                        var targetWeek = meeting.TargetWeek ?? 1;
                        if (handover.OnboardingPeriod.Duration.HasValue & targetWeek > handover.OnboardingPeriod.Duration)
                            targetWeek = handover.OnboardingPeriod.Duration.Value;

                        meeting.Start =
                            handover.OnboardingPeriod.StartPoint.Value.AddDays((targetWeek - 1) * 7)
                                .Friday().AddHours(9);
                        meeting.End = meeting.Start;
                    }

                    MContext.ProjectInstance.Add(clonedProjectInstance);
                }

                MContext.SaveChanges();
            }

            TaskService.SetFixedDateTime(handover.Package.ID, handover.OnboardingPeriod.StartPoint.Value, handover.OnboardingPeriod.Duration.Value);

            var contactId = GetLineManagerContactId(handover.OnboardingPeriod.Package);
            var lineManagerPackageId = GetOldestPackageId(handover.Package.UserLineManager);
            var hrManagerPackageId = GetOldestPackageId(handover.Package.UserHRManager);

            A0(handover, contactId);
            // ...
            A6(handover, contactId);
            A7(handover);
            A8(handover);
            A9(handover);
            A10(handover, lineManagerPackageId, hrManagerPackageId);
            
            MContext.SaveChanges();
        }

        /// <summary>
        /// Unlocks the onboarding period
        /// </summary>
        /// <param name="handover">Handover onboarding perioud to unlock</param>
        public void UnlockOnboardingPeriod(Handover handover)
        {
            // Clear dates
            handover.OnboardingPeriod.LastDay = null;
            handover.OnboardingPeriod.MidPoint = null;
            handover.OnboardingPeriod.Locked = null;
            handover.OnboardingPeriod.Priorities.Saved = null;

            var onboarderPackage = PackageService.GetByPackageId(handover.OnboardingPeriod.PackageId.Value);
            onboarderPackage.AncestorPackageId = null;

            UndoPeriod(handover.OnboardingPeriodId.Value);

            MContext.SaveChanges();

            TaskService.ResetFixedDateTime(handover.Package.ID);
        }

        /// <summary>
        // Gets the onboarding period for the Package
        /// </summary>
        /// <param name="packageId">Package id to inspect</param>
        /// <returns>Object that contains the onboarding period details</returns>
        public OnboardingPeriod GetOnboardingPeriodForPackage(Guid packageId)
        {
            var onboardingPeriod = MContext.OnboardingPeriod.FirstOrDefault(p => p.PackageId == packageId);
            return onboardingPeriod;
        }

        /// <summary>
        /// Gets the leaving period for the Package
        /// </summary>
        /// <param name="packageId">Package id to inspect</param>
        /// <returns>Object that contains the leaving period</returns>
        public LeavingPeriod GetLeavingPeriodForPackage(Guid packageId)
        {
            var leavingPeriod = MContext.Handover.FirstOrDefault(p => p.ID == packageId);
            return leavingPeriod == null ? null : leavingPeriod.LeavingPeriod;
        }

        /// <summary>
        /// Returns true if the Package is in a onboarding state
        /// </summary>
        /// <param name="packageId">Package to inspect</param>
        /// <returns>True of the package if onboarding</returns>
        public bool IsOnboarding(Guid packageId)
        {
            var onboardingPeriod = GetOnboardingPeriodForPackage(packageId);
            return onboardingPeriod != null && onboardingPeriod.Locked.HasValue && onboardingPeriod.LastDay.Value >= SystemTime.Now;
        }

        /// <summary>
        /// Returns true if the Package is in a onboarded state
        /// </summary>
        /// <param name="packageId">Package to inspect</param>
        /// <returns>True if the package has onboarded</returns>
        public bool HasOnboarded(Guid packageId)
        {
            var onboardingPeriod = GetOnboardingPeriodForPackage(packageId);
            return onboardingPeriod != null && onboardingPeriod.Locked.HasValue && onboardingPeriod.LastDay.Value < SystemTime.Now;
        }

        /// <summary>
        /// Returns true is the Package is in a leaving state
        /// </summary>
        /// <param name="packageId">Package to inspect</param>
        /// <returns>True if the package is in a leaving state</returns>
        public bool IsLeaving(Guid packageId)
        {
            var leavingPeriod = GetLeavingPeriodForPackage(packageId);
            return leavingPeriod != null && leavingPeriod.Locked.HasValue && leavingPeriod.LastDay.Value >= SystemTime.Now;
        }

        /// <summary>
        /// Returns true if the leaving period has been breached
        /// </summary>
        /// <param name="packageId">Package to inspect</param>
        /// <returns>True if the leaving period has passed</returns>
        public bool HasLeft(Guid packageId)
        {
            var leavingPeriod = GetLeavingPeriodForPackage(packageId);
            return leavingPeriod != null && leavingPeriod.Locked.HasValue && leavingPeriod.LastDay.Value < SystemTime.Now;
        }

        /// <summary>
        /// Accept a Hadover
        /// </summary>
        /// <param name="id">Handover id</param>
        public void Accept(Guid id)
        {
            var handover = MContext.Handover.Find(id);

            if (handover != null)
            {
                handover.Accepted = DateTime.Now;
                MContext.SaveChanges();
            }
        }

        /// <summary>
        /// Returns active Handovers
        /// </summary>
        /// <returns>Active Handovers</returns>
        public IEnumerable<Handover> GetActiveHandovers()
        {
            return MContext.Handover.Where(h =>
                (
                (h.LeavingPeriod.Locked.HasValue)) &&
                (h.OnboardingPeriod.LastDay.HasValue && h.OnboardingPeriod.LastDay >= SystemTime.Now) ||
                (h.LeavingPeriod.LastDay >= SystemTime.Now && h.LeavingPeriod.Locked.HasValue)
                )
                .Distinct().ToList();
        }

        #endregion

        /// <summary>
        /// Get the ID of the oldest package for a given User.
        /// </summary>
        /// <param name="user">User to inspect</param>
        /// <returns>Id of the oldest package</returns>
        private Guid? GetOldestPackageId(User user)
        {
            var packages = PackageService.GetByUserOwnerId(user.ID).ToList();
            if(packages.Any())
            { 
                var package = packages.Aggregate((p1, p2) => p1.Created < p2.Created ? p1 : p2);
                return package.ID;
            }
            return null;
        }

        /// <summary>
        /// Set Gate A1 - Leaver Period Lock
        /// </summary>
        /// <param name="handover">Handover to update</param>
        /// <param name="contactId">Contact to associate metting</param>
        private void A1(Handover handover, Guid contactId)
        {
            var leavingPeriod = handover.LeavingPeriod;
            var gate = Handover.Gate.A1.ToString();

            // Create opening meeting on leaver start point
            if (leavingPeriod.StartPoint.HasValue)
            {
                var startPoint = leavingPeriod.StartPoint.Value;
                var meetingStart = startPoint.Date.AddHours(DefaultMeetingStartTimeHour).PreviousBusinessDayOrSelf();
                var meetingEnd = meetingStart.AddMinutes(DefaultMeetingDurationMinutes);

                var meetingType = MeetingService.GetMeetingTypeById(LeaverOpeningMeetingForExitTypeId);

                var leaverOpeningMeetingForExit = new Meeting
                {
                    Start = meetingStart,
                    End = meetingEnd,
                    MeetingTypeId = LeaverOpeningMeetingForExitTypeId,
                    Name = meetingType.Title,
                    PeriodId = leavingPeriod.ID,
                    HandoverGate = gate,
                    ParentContactId = contactId
                };

                MContext.Meeting.Add(leaverOpeningMeetingForExit);
            }

            // Create mid-point review meeting
            if (leavingPeriod.MidPoint.HasValue)
            {
                var midPoint = leavingPeriod.MidPoint.Value;
                var meetingStart = midPoint.Date.AddHours(DefaultMeetingStartTimeHour).PreviousBusinessDayOrSelf();
                var meetingEnd = meetingStart.AddMinutes(DefaultMeetingDurationMinutes);

                var meetingType = MeetingService.GetMeetingTypeById(LeaverMidPointReviewMeetingTypeId);

                var leaverMidPointReviewProgressMeeting = new Meeting
                {
                    Start = meetingStart,
                    End = meetingEnd,
                    MeetingTypeId = LeaverMidPointReviewMeetingTypeId,
                    Name = meetingType.Title,
                    PeriodId = leavingPeriod.ID,
                    HandoverGate = gate,
                    ParentContactId = contactId
                };

                MContext.Meeting.Add(leaverMidPointReviewProgressMeeting);
            }

            // Create leaver handover meeting
            if (leavingPeriod.LastDay.HasValue)
            {
                var lastDay = leavingPeriod.LastDay.Value;
                var meetingStart =
                    lastDay.Date.AddDays(-1).PreviousBusinessDayOrSelf().AddHours(DefaultMeetingStartTimeHour).PreviousBusinessDayOrSelf();
                var meetingEnd = meetingStart.AddMinutes(DefaultMeetingDurationMinutes);

                var meetingType = MeetingService.GetMeetingTypeById(LeaverHandoverMeetingTypeId);

                var leaverHandoverMeeting = new Meeting
                {
                    Start = meetingStart,
                    End = meetingEnd,
                    MeetingTypeId = LeaverHandoverMeetingTypeId,
                    Name = meetingType.Title,
                    PeriodId = leavingPeriod.ID,
                    HandoverGate = gate,
                    ParentContactId = contactId
                };

                MContext.Meeting.Add(leaverHandoverMeeting);
            }
        }

        /// <summary>
        /// Set Gate A2 - Leaver Start Point
        /// </summary>
        /// <param name="handover">Handover to update</param>
        private void A2(Handover handover)
        {
            var leavingPeriod = handover.LeavingPeriod;

            if (leavingPeriod.StartPoint.HasValue)
            {
                var startPoint = leavingPeriod.StartPoint.Value;
                var gate = Handover.Gate.A2.ToString();

                // Create to-do list item for employee to accept handover
                var acceptHandoverToDoItem = new ToDoItem
                {
                    PackageId = handover.ID,
                    Title = "Confirm Handover Commencement",
                    VisibleDate = startPoint,
                    PeriodId = leavingPeriod.ID,
                    HandoverGate = gate,
                    Url = string.Format("/Handover/Accept/{0}", handover.ID)
                };

                // Create to-do list item for employee to update critical
                var updateCriticalToDoItem = new ToDoItem
                {
                    PackageId = handover.ID,
                    Title = "Update Essentials Topics/Tasks",
                    VisibleDate = startPoint,
                    Url = "/Home/Critical",
                    PeriodId = leavingPeriod.ID,
                    HandoverGate = gate
                };

                // Create to-do list item for employee to update relationships
                var updateRelationshipsToDoItem = new ToDoItem
                {
                    PackageId = handover.ID,
                    Title = "Update Relationships",
                    VisibleDate = startPoint,
                    Url = "/Home/Relationships",
                    PeriodId = leavingPeriod.ID,
                    HandoverGate = gate
                };

                // Create to-do list item for employee to update modules
                var updateModulesToDoItem = new ToDoItem
                {
                    PackageId = handover.ID,
                    Title = "Update All Modules",
                    VisibleDate = startPoint,
                    PeriodId = leavingPeriod.ID,
                    HandoverGate = gate
                };

                MContext.ToDoItem.Add(acceptHandoverToDoItem);
                MContext.ToDoItem.Add(updateCriticalToDoItem);
                MContext.ToDoItem.Add(updateRelationshipsToDoItem);
                MContext.ToDoItem.Add(updateModulesToDoItem);
            }
        }

        /// <summary>
        /// Set Gate A3 - Leaver Period Mid-Point
        /// </summary>
        /// <param name="handover">Handover to update</param>
        private void A3(Handover handover)
        {
            var leavingPeriod = handover.LeavingPeriod;

            if (leavingPeriod.MidPoint.HasValue)
            {
                var midPoint = leavingPeriod.MidPoint.Value;
                var gate = Handover.Gate.A3.ToString();

                // Create to-do list item for employee to update critical
                var updateCriticalToDoItem = new ToDoItem
                {
                    PackageId = handover.ID,
                    Title = "Update Essentials Topics/Tasks",
                    VisibleDate = midPoint,
                    Url = "/Home/Critical",
                    PeriodId = leavingPeriod.ID,
                    HandoverGate = gate
                };

                // Create to-do list item for employee to update relationships
                var updateRelationshipsToDoItem = new ToDoItem
                {
                    PackageId = handover.ID,
                    Title = "Update Relationships",
                    VisibleDate = midPoint,
                    Url = "/Home/Relationships",
                    PeriodId = leavingPeriod.ID,
                    HandoverGate = gate
                };

                // Create to-do list item for employee to update modules
                var updateModulesToDoItem = new ToDoItem
                {
                    PackageId = handover.ID,
                    Title = "Update All Modules",
                    VisibleDate = midPoint,
                    PeriodId = leavingPeriod.ID,
                    HandoverGate = gate
                };

                MContext.ToDoItem.Add(updateCriticalToDoItem);
                MContext.ToDoItem.Add(updateRelationshipsToDoItem);
                MContext.ToDoItem.Add(updateModulesToDoItem);
            }
        }

        /// <summary>
        /// Set Gate A3a - Leaver Period Mid-Point Review Meeting Elapsed
        /// </summary>
        /// <param name="handover">Handover to update</param>
        [Obsolete]
        private void A3a(Handover handover)
        {
            // Deferred to EPIC X
        }

        /// <summary>
        /// Set Gate A4 - Exit Survey Submitted
        /// </summary>
        /// <param name="handover">Handover to update</param>
        [Obsolete]
        private void A4(Handover handover)
        {
            // Deferred to EPIC 3
        }

        /// <summary>
        /// Set Gate A4a - Exit Interview Elapse
        /// </summary>
        /// <param name="handover">Handover to update</param>
        [Obsolete]
        private void A4a(Handover handover)
        {
            // Deferred to EPIC 3
        }

        /// <summary>
        /// Set Gate A5 - Leaver End (Handover Meeting)
        /// </summary>
        /// <param name="handover">Hanodlver to update</param>
        /// <param name="lineManagerPackageId">Line manager to review to do items</param>
        /// <param name="hrManagerPackageId">HR manager to review to do items</param>
        private void A5(Handover handover, Guid? lineManagerPackageId, Guid? hrManagerPackageId)
        {
            var leavingPeriod = handover.LeavingPeriod;

            if (leavingPeriod.LastDay.HasValue)
            {
                var lastDay = leavingPeriod.LastDay.Value;
                var gate = Handover.Gate.A5.ToString();
                var title = "Review Employee Package for Handover";
                var url = string.Format("/PackageSelector/ExaminePackage/{0}", handover.ID);

                if(lineManagerPackageId.HasValue)
                { 
                    var reviewPackageToDoItemLineManager = new ToDoItem
                    {
                        Title = title,
                        VisibleDate = lastDay,
                        PeriodId = leavingPeriod.ID,
                        HandoverGate = gate,
                        PackageId = lineManagerPackageId,
                        Url = url
                    };
                    MContext.ToDoItem.Add(reviewPackageToDoItemLineManager);
                }

                if(hrManagerPackageId.HasValue)
                { 
                    var reviewPackageToDoItemHrManager = new ToDoItem
                    {
                        Title = title,
                        VisibleDate = lastDay,
                        PeriodId = leavingPeriod.ID,
                        HandoverGate = gate,
                        PackageId = hrManagerPackageId,
                        Url = url
                    };
                    MContext.ToDoItem.Add(reviewPackageToDoItemHrManager);
                }
            }
        }

        /// <summary>
        /// Set Gate A0 - Ongoing/Pre-Handover (deffered)
        /// </summary>
        /// <param name="handover">Handover to update</param>
        /// <param name="contactId">Contact id to set meeting</param>
        [Obsolete]
        private void A0(Handover handover, Guid contactId)
        {
            // Deffered
        }

        /// <summary>
        /// Set Gate A6 - Onboarder Lock
        /// </summary>
        /// <param name="handover">Hanodover to update</param>
        /// <param name="contactId">Contact to generate handover meetings against</param>
        private void A6(Handover handover, Guid contactId)
        {
            var onboardingPeriod = handover.OnboardingPeriod;
            var gate = Handover.Gate.A6.ToString();

            // Create opening meeting on onboarder start point
            if (onboardingPeriod.StartPoint.HasValue)
            {
                var startPoint = onboardingPeriod.StartPoint.Value;
                var meetingStart = startPoint.Date.AddHours(DefaultMeetingStartTimeHour).PreviousBusinessDayOrSelf();
                var meetingEnd = meetingStart.AddMinutes(DefaultMeetingDurationMinutes);

                var meetingType = MeetingService.GetMeetingTypeById(OnboardingKickOffHandoverMeetingTypeId);

                var onboarderKickOffMeeting = new Meeting
                {
                    Start = meetingStart,
                    End = meetingEnd,
                    MeetingTypeId = OnboardingKickOffHandoverMeetingTypeId,
                    Name = meetingType.Title,
                    PeriodId = onboardingPeriod.ID,
                    HandoverGate = gate,
                    ParentContactId = contactId
                };

                MContext.Meeting.Add(onboarderKickOffMeeting);
            }

            // Create onboarder review meeting at mid-point
            if (onboardingPeriod.MidPoint.HasValue)
            {
                var midPoint = onboardingPeriod.MidPoint.Value;
                var meetingStart = midPoint.Date.AddHours(DefaultMeetingStartTimeHour).PreviousBusinessDayOrSelf();
                var meetingEnd = meetingStart.AddMinutes(DefaultMeetingDurationMinutes);

                var meetingType = MeetingService.GetMeetingTypeById(OnboardingMidPointReviewMeetingTypeId);

                var onboarderMidPointReviewMeeting = new Meeting
                {
                    Start = meetingStart,
                    End = meetingEnd,
                    MeetingTypeId = OnboardingMidPointReviewMeetingTypeId,
                    Name = meetingType.Title,
                    PeriodId = onboardingPeriod.ID,
                    HandoverGate = gate,
                    ParentContactId = contactId
                };

                MContext.Meeting.Add(onboarderMidPointReviewMeeting);
            }

            // Create onboarder end review meeting at last day
            if (onboardingPeriod.LastDay.HasValue)
            {
                var lastDay = onboardingPeriod.LastDay.Value;
                var meetingStart = lastDay.Date.AddHours(DefaultMeetingStartTimeHour).PreviousBusinessDayOrSelf();
                var meetingEnd = meetingStart.AddMinutes(DefaultMeetingDurationMinutes);

                var meetingType = MeetingService.GetMeetingTypeById(OnboardingEndReviewMeetingTypeId);

                var onboarderEndReviewMeeting = new Meeting
                {
                    Start = meetingStart,
                    End = meetingEnd,
                    MeetingTypeId = OnboardingEndReviewMeetingTypeId,
                    Name = meetingType.Title,
                    PeriodId = onboardingPeriod.ID,
                    HandoverGate = gate,
                    ParentContactId = contactId
                };

                MContext.Meeting.Add(onboarderEndReviewMeeting);
            }
        }

        /// <summary>
        /// Set Gate A7
        /// </summary>
        /// <param name="handover">Handnover to update</param>
        [Obsolete]
        private void A7(Handover handover)
        {
            // Deferred to future EPIC
        }

        /// <summary>
        /// Set Gate A8 - Onboarder Start-Point
        /// </summary>
        /// <param name="handover">Handover to update</param>
        private void A8(Handover handover)
        {
            var onboardingPeriod = handover.OnboardingPeriod;

            if (onboardingPeriod.StartPoint.HasValue)
            {
                var startPoint = onboardingPeriod.StartPoint.Value;
                var gate = Handover.Gate.A8.ToString();
                var url = string.Format("/Reports/ViewHandoverReport/{0}?ancestor={1}", onboardingPeriod.PackageId, true);

                // Create to-do list item for employee to print handover report
                var printHandoverReportToDoItem = new ToDoItem
                {
                    PackageId = onboardingPeriod.PackageId,
                    Title = "Print Handover Report",
                    VisibleDate = startPoint,
                    PeriodId = onboardingPeriod.ID,
                    HandoverGate = gate,
                    Url = url
                };

                MContext.ToDoItem.Add(printHandoverReportToDoItem);
            }
        }

        /// <summary>
        /// Set Gate A9 - Onboarder Mid-Point
        /// </summary>
        /// <param name="handover">Handover to update</param>
        [Obsolete]
        private void A9(Handover handover)
        {
            // Defferred 
        }

        /// <summary>
        /// Sets Gate A10 - Onboarder End
        /// </summary>
        /// <param name="handover">Handover to update</param>
        /// <param name="lineManagerPackageId">Link manager to approve To Do list items</param>
        /// <param name="hrManagerPackageId">HR manager to approve To Do list items</param>
        private void A10(Handover handover, Guid? lineManagerPackageId, Guid? hrManagerPackageId)
        {
            var onboardingPeriod = handover.OnboardingPeriod;

            if (onboardingPeriod.LastDay.HasValue)
            {
                var lastDay = onboardingPeriod.LastDay.Value;
                var gate = Handover.Gate.A10.ToString();
                var title = "Approve and Finalise Package";
                var url = string.Format("/PackageSelector/ExaminePackage/{0}", onboardingPeriod.PackageId);


                // Create to-do list item for HR/LM to approve and finalise package
                if(lineManagerPackageId.HasValue)
                {
                    var finalisePackageToDoItemLineManager = new ToDoItem
                    {
                        Title = title,
                        VisibleDate = lastDay,
                        PeriodId = onboardingPeriod.ID,
                        HandoverGate = gate,
                        PackageId = lineManagerPackageId,
                        Url = url
                    };
                    MContext.ToDoItem.Add(finalisePackageToDoItemLineManager);
                }

                if(hrManagerPackageId.HasValue)
                {
                    var finalisePackageToDoItemHrManager = new ToDoItem
                    {
                        Title = title,
                        VisibleDate = lastDay,
                        PeriodId = onboardingPeriod.ID,
                        HandoverGate = gate,
                        PackageId = hrManagerPackageId,
                        Url = url
                    };             
                    MContext.ToDoItem.Add(finalisePackageToDoItemHrManager);
                }
            }
        }

        /// <summary>
        /// Set Gate A11 - Exit and Onboarding Review
        /// </summary>
        /// <param name="handover">Handover to update</param>
        [Obsolete]
        private void A11(Handover handover)
        {
            // Deferred to future EPIC
        }

        /// <summary>
        /// Delete all items created as part of the lock
        /// for a given period.
        /// </summary>
        /// <param name="periodId">Period id</param>
        private void UndoPeriod(Guid periodId)
        {
            var contacts = ContactService.GetByPeriodId(periodId);
            var meetings = MeetingService.GetByPeriodId(periodId);
            var toDoItems = ToDoItemService.GetByPeriodId(periodId);
            var projectInstances = ProjectService.GetProjectInstancesByPeriodId(periodId);

            foreach (var contact in contacts.ToList())
            {
                ContactService.Delete(contact);
            }

            foreach (var meeting in meetings.ToList())
            {
                MeetingService.Delete(meeting);
            }

            foreach (var toDoItem in toDoItems.ToList())
            {
                ToDoItemService.Delete(toDoItem);
            }

            foreach (var projectInstance in projectInstances.ToList())
            {
                ProjectService.DeleteInstance(projectInstance);
            }
        }

        /// <summary>
        /// Get the onboarder review date for Critical Module
        /// </summary>
        /// <param name="packageId">Package to inspect</param>
        /// <returns>Review date</returns>
        public DateTime? GetOnboarderReviewDateCritical(Guid packageId)
        {
            DateTime? result = null;

            var handover = MContext.Handover
                .AsNoTracking()
                .Include(h => h.OnboardingPeriod.Priorities)
                .FirstOrDefault(h => h.OnboardingPeriod.PackageId == packageId);

            if (handover != null)
            {
                result = GetOnboarderReviewDateCritical(handover);
            }

            return result;
        }

        /// <summary>
        /// Gets the onboarder review date for Critical Module
        /// </summary>
        /// <param name="handover">Handover to inspect</param>
        /// <returns>Review date</returns>
        public DateTime? GetOnboarderReviewDateCritical(Handover handover)
        {
            DateTime? result = null;

            var onboarderPriorities = (OnboarderPriorities)handover.OnboardingPeriod.Priorities;

            var weekNum = onboarderPriorities.CriticalDue;

            result = handover.OnboardingPeriod.StartPoint;

            if (result != null)
            {
                result = result.Value.AddDays((weekNum - 1) * 7).Friday();
            }

            return result;
        }

        /// <summary>
        /// Gets the onboarder review date for the Experiences Module
        /// </summary>
        /// <param name="packageId">Package to inspect</param>
        /// <returns>Review date</returns>
        public DateTime? GetOnboarderReviewDateExperiences(Guid packageId)
        {
            DateTime? result = null;

            var handover = MContext.Handover
                .AsNoTracking()
                .Include(h => h.OnboardingPeriod.Priorities)
                .FirstOrDefault(h => h.OnboardingPeriod.PackageId == packageId);

            if (handover != null)
            {
                result = GetOnboarderReviewDateExperiences(handover);
            }

            return result;
        }

        /// <summary>
        /// Get onboarder reivew data for the Experiences Module
        /// </summary>
        /// <param name="handover">Hanodover to inspect</param>
        /// <returns>Review date</returns>
        public DateTime? GetOnboarderReviewDateExperiences(Handover handover)
        {
            DateTime? result = null;

            var onboarderPriorities = (OnboarderPriorities)handover.OnboardingPeriod.Priorities;

            if (onboarderPriorities.ExperiancesEnabled)
            {
                var weekNum = onboarderPriorities.ExperiencesDue;

                result = handover.OnboardingPeriod.StartPoint;

                if (result != null)
                {
                    result = result.Value.AddDays((weekNum - 1) * 7).Friday();
                }
            }

            return result;
        }

        /// <summary>
        /// Gets the onboarder review date for the Relationships module
        /// </summary>
        /// <param name="packageId">Package to inspect</param>
        /// <returns>Review date</returns>
        public DateTime? GetOnboarderReviewDateRelationships(Guid packageId)
        {
            DateTime? result = null;

            var handover = MContext.Handover
                .AsNoTracking()
                .Include(h => h.OnboardingPeriod.Priorities)
                .FirstOrDefault(h => h.OnboardingPeriod.PackageId == packageId);

            if (handover != null)
            {
                result = GetOnboarderReviewDateRelationships(handover);
            }

            return result;
        }

        /// <summary>
        /// Gets the onboarder review date for the Relationship Module
        /// </summary>
        /// <param name="handover">Handover to inspect</param>
        /// <returns>Review date</returns>
        public DateTime? GetOnboarderReviewDateRelationships(Handover handover)
        {
            DateTime? result = null;

            var onboarderPriorities = (OnboarderPriorities)handover.OnboardingPeriod.Priorities;

            var weekNum = onboarderPriorities.RelationshipsDue;

            result = handover.OnboardingPeriod.StartPoint;

            if (result != null)
            {
                result = result.Value.AddDays((weekNum - 1) * 7).Friday();
            }

            return result;
        }

        /// <summary>
        /// Gets the onboarder review date for the Projects Module
        /// </summary>
        /// <param name="packageId">Package to inspect</param>
        /// <returns>Review date</returns>
        public DateTime? GetOnboarderReviewDateProjects(Guid packageId)
        {
            DateTime? result = null;

            var handover = MContext.Handover
                .AsNoTracking()
                .Include(h => h.OnboardingPeriod.Priorities)
                .FirstOrDefault(h => h.OnboardingPeriod.PackageId == packageId);

            if (handover != null)
            {
                result = GetOnboarderReviewDateProjects(handover);
            }

            return result;
        }

        /// <summary>
        /// Gets the onboarder review date for the Projects Module
        /// </summary>
        /// <param name="handover">Hanodver to inspect</param>
        /// <returns>Review date</returns>
        public DateTime? GetOnboarderReviewDateProjects(Handover handover)
        {
            DateTime? result = null;

            var onboarderPriorities = (OnboarderPriorities)handover.OnboardingPeriod.Priorities;

            if (onboarderPriorities.ProjectsEnabled)
            {
                var weekNum = onboarderPriorities.ProjectsDue;

                result = handover.OnboardingPeriod.StartPoint;

                if (result != null)
                {
                    result = result.Value.AddDays((weekNum - 1) * 7).Friday();
                }
            }

            return result;
        }

        /// <summary>
        /// Gets the onboarder review date for the People Module
        /// </summary>
        /// <param name="packageId">Package to inspect</param>
        /// <returns>Review date</returns>
        public DateTime? GetOnboarderReviewDatePeople(Guid packageId)
        {
            DateTime? result = null;

            var handover = MContext.Handover
                .AsNoTracking()
                .Include(h => h.OnboardingPeriod.Priorities)
                .FirstOrDefault(h => h.OnboardingPeriod.PackageId == packageId);

            if (handover != null)
            {
                result = GetOnboarderReviewDatePeople(handover);
            }

            return result;
        }

        /// <summary>
        /// Gets the onboarder review date for the People Module
        /// </summary>
        /// <param name="handover">Handover to inspect</param>
        /// <returns>Review date</returns>
        public DateTime? GetOnboarderReviewDatePeople(Handover handover)
        {
            DateTime? result = null;

            var onboarderPriorities = (OnboarderPriorities)handover.OnboardingPeriod.Priorities;

            if (onboarderPriorities.PeopleEnabled)
            {
                var weekNum = onboarderPriorities.PeopleDue;

                result = handover.OnboardingPeriod.StartPoint;

                if (result != null)
                {
                    result = result.Value.AddDays((weekNum - 1) * 7).Friday();
                }
            }

            return result;
        }

     
        //public DateTime? GetLeaverReviewDateCritical(Guid packageId)
        //{
        //    DateTime? result = null;

        //    var handover = MContext.Handover
        //        .AsNoTracking()
        //        .Include(h => h.LeavingPeriod.Priorities)
        //        .FirstOrDefault(h => h.Package.ID == packageId);

        //    if (handover != null)
        //    {
        //        result = GetLeaverReviewDateCritical(handover);
        //    }

        //    return result;
        //}

        /// <summary>
        /// Gets the leaver review date for the Critical Module
        /// </summary>
        /// <param name="handover">Handover to inspect</param>
        /// <returns>Review date</returns>
        public DateTime? GetLeaverReviewDateCritical(Handover handover)
        {
            DateTime? result = null;

            var leaverPriorities = (LeaverPriorities)handover.LeavingPeriod.Priorities;

            var duePeriod = leaverPriorities.CriticalDue;

            switch(duePeriod)
            {
                case DuePeriod.Start:
                    result = handover.LeavingPeriod.StartPoint;
                    break;
                case DuePeriod.Mid:
                    result = handover.LeavingPeriod.MidPoint;
                    break;
                case DuePeriod.End:
                    result = handover.LeavingPeriod.LastDay;
                    break;
            }

            return result;
        }

        /// <summary>
        /// Gets the leaver review date for the Experiences Module
        /// </summary>
        /// <param name="packageId">Package to inspect</param>
        /// <returns>Review date</returns>
        public DateTime? GetLeaverReviewDateExperiences(Guid packageId)
        {
            DateTime? result = null;

            var handover = MContext.Handover
                .AsNoTracking()
                .Include(h => h.LeavingPeriod.Priorities)
                .FirstOrDefault(h => h.Package.ID == packageId);

            if (handover != null)
            {
                result = GetOnboarderReviewDateExperiences(handover);
            }

            return result;
        }

        /// <summary>
        /// Gets the leaver review date for the Experiences Module
        /// </summary>
        /// <param name="handover">Handover to inspect</param>
        /// <returns>Review date</returns>
        public DateTime? GetLeaverReviewDateExperiences(Handover handover)
        {
            DateTime? result = null;

            var leaverPriorities = (LeaverPriorities)handover.LeavingPeriod.Priorities;

            if (leaverPriorities.ExperiancesEnabled)
            {
                var duePeriod = leaverPriorities.ExperiencesDue;

                switch (duePeriod)
                {
                    case DuePeriod.Start:
                        result = handover.LeavingPeriod.StartPoint;
                        break;
                    case DuePeriod.Mid:
                        result = handover.LeavingPeriod.MidPoint;
                        break;
                    case DuePeriod.End:
                        result = handover.LeavingPeriod.LastDay;
                        break;
                }
            }

            return result;
        }

        /// <summary>
        /// Gets the leaver review date for the Releationships Module
        /// </summary>
        /// <param name="packageId">Package to inspect</param>
        /// <returns>Review date</returns>
        public DateTime? GetLeaverReviewDateRelationships(Guid packageId)
        {
            DateTime? result = null;

            var handover = MContext.Handover
                .AsNoTracking()
                .Include(h => h.LeavingPeriod.Priorities)
                .FirstOrDefault(h => h.Package.ID == packageId);

            if (handover != null)
            {
                result = GetLeaverReviewDateRelationships(handover);
            }

            return result;
        }

        /// <summary>
        /// Gets the leaver review date for the Relationships Module
        /// </summary>
        /// <param name="handover">Handover to inspect</param>
        /// <returns>Review date</returns>
        public DateTime? GetLeaverReviewDateRelationships(Handover handover)
        {
            DateTime? result = null;

            var leaverPriorities = (LeaverPriorities)handover.LeavingPeriod.Priorities;

            var duePeriod = leaverPriorities.RelationshipsDue;

            switch (duePeriod)
            {
                case DuePeriod.Start:
                    result = handover.LeavingPeriod.StartPoint;
                    break;
                case DuePeriod.Mid:
                    result = handover.LeavingPeriod.MidPoint;
                    break;
                case DuePeriod.End:
                    result = handover.LeavingPeriod.LastDay;
                    break;
            }

            return result;
        }

        /// <summary>
        /// Gets the leaver review date for the Projects Module
        /// </summary>
        /// <param name="packageId">Package to inspect</param>
        /// <returns>Review date</returns>
        public DateTime? GetLeaverReviewDateProjects(Guid packageId)
        {
            DateTime? result = null;

            var handover = MContext.Handover
                .AsNoTracking()
                .Include(h => h.LeavingPeriod.Priorities)
                .FirstOrDefault(h => h.Package.ID == packageId);

            if (handover != null)
            {
                result = GetLeaverReviewDateProjects(handover);
            }

            return result;
        }

        /// <summary>
        /// Gets the leaver review date for the Project Module
        /// </summary>
        /// <param name="handover">Handover to inspect</param>
        /// <returns>Review date</returns>
        public DateTime? GetLeaverReviewDateProjects(Handover handover)
        {
            DateTime? result = null;

            var leaverPriorities = (LeaverPriorities)handover.LeavingPeriod.Priorities;

            if (leaverPriorities.ProjectsEnabled)
            {
                var duePeriod = leaverPriorities.ProjectsDue;

                switch (duePeriod)
                {
                    case DuePeriod.Start:
                        result = handover.LeavingPeriod.StartPoint;
                        break;
                    case DuePeriod.Mid:
                        result = handover.LeavingPeriod.MidPoint;
                        break;
                    case DuePeriod.End:
                        result = handover.LeavingPeriod.LastDay;
                        break;
                }
            }

            return result;
        }

        /// <summary>
        /// Gets the leaver review date for the People Module
        /// </summary>
        /// <param name="packageId">Package to inspect</param>
        /// <returns>Review date</returns>
        public DateTime? GetLeaverReviewDatePeople(Guid packageId)
        {
            DateTime? result = null;

            var handover = MContext.Handover
                .AsNoTracking()
                .Include(h => h.LeavingPeriod.Priorities)
                .FirstOrDefault(h => h.Package.ID == packageId);

            if (handover != null)
            {
                result = GetLeaverReviewDatePeople(handover);
            }

            return result;
        }

        /// <summary>
        /// Gets the leaver review date for the People Module
        /// </summary>
        /// <param name="handover">Handover to inspect</param>
        /// <returns>Review date</returns>
        public DateTime? GetLeaverReviewDatePeople(Handover handover)
        {
            DateTime? result = null;

            var leaverPriorities = (LeaverPriorities)handover.LeavingPeriod.Priorities;

            if (leaverPriorities.PeopleEnabled)
            {
                var duePeriod = leaverPriorities.ProjectsDue;

                switch (duePeriod)
                {
                    case DuePeriod.Start:
                        result = handover.LeavingPeriod.StartPoint;
                        break;
                    case DuePeriod.Mid:
                        result = handover.LeavingPeriod.MidPoint;
                        break;
                    case DuePeriod.End:
                        result = handover.LeavingPeriod.LastDay;
                        break;
                }
            }

            return result;
        }

        public DateTime? GetLeaverReviewDateCoreTasks(Guid packageId)
        {
            DateTime? result = null;

            var handover = MContext.Handover
                .AsNoTracking()
                .Include(h => h.LeavingPeriod.Priorities)
                .FirstOrDefault(h => h.Package.ID == packageId);

            if (handover != null)
            {
                result = GetLeaverReviewDateCoreTasks(handover);
            }

            return result;
        }

        public DateTime? GetLeaverReviewDateCoreTasks(Handover handover)
        {
            DateTime? result = null;

            var leaverPriorities = (LeaverPriorities)handover.LeavingPeriod.Priorities;

            var duePeriod = leaverPriorities.CoreTasksDue;

            switch (duePeriod)
            {
                case DuePeriod.Start:
                    result = handover.LeavingPeriod.StartPoint;
                    break;
                case DuePeriod.Mid:
                    result = handover.LeavingPeriod.MidPoint;
                    break;
                case DuePeriod.End:
                    result = handover.LeavingPeriod.LastDay;
                    break;
            }

            return result;
        }


        public bool IsLeaverPeriodLocked(Guid packageId)
        {
            var handover = MContext.Handover
                .AsNoTracking()
                .Include(h => h.LeavingPeriod)
                .FirstOrDefault(h => h.Package.ID == packageId);

            return handover != null && handover.LeavingPeriod.Locked.HasValue;
        }

        public bool IsOnboarderPeriodLocked(Guid packageId)
        {
            var handover = MContext.Handover
                .AsNoTracking()
                .Include(h => h.OnboardingPeriod)
                .FirstOrDefault(h => h.Package.ID == packageId);

            return handover != null && handover.OnboardingPeriod.Locked.HasValue;
        }

        private void SetLeaverReviewByDates(Package package)
        {
            var handover = package.Handover;
            var priorities = handover.LeavingPeriod.Priorities;

            var criticalModule = package.Modules.FirstOrDefault(m => m.Type == Module.ModuleType.Critical);
            if (criticalModule != null)
            {
                var criticalReviewByDate = GetLeaverReviewDateCritical(handover);
                foreach (var topic in criticalModule.TopicGroups.SelectMany(t => t.Topics))
                {
                    topic.ReviewBy = criticalReviewByDate;
                    foreach (var task in topic.Tasks)
                    {
                        task.ReviewBy = criticalReviewByDate;
                    }
                }
            }

            var relationships = package.Contacts;
            if (relationships != null)
            {
                var relationshipsReviewByDate = GetLeaverReviewDateRelationships(handover);
                foreach (var relationship in relationships)
                {
                    relationship.ReviewBy = relationshipsReviewByDate;
                }
            }

            var experiencesModule = package.Modules.FirstOrDefault(m => m.Type == Module.ModuleType.Experiences);
            if (experiencesModule != null && priorities.ExperiancesEnabled)
            {
                var experiencesReviewByDate = GetLeaverReviewDateExperiences(handover);
                foreach (var topic in experiencesModule.TopicGroups.SelectMany(t => t.Topics))
                {
                    topic.ReviewBy = experiencesReviewByDate;
                    foreach (var task in topic.Tasks)
                    {
                        task.ReviewBy = experiencesReviewByDate;
                    }
                }
            }

            var projects = package.Projects;
            if (projects != null && priorities.ProjectsEnabled)
            {
                foreach (var project in projects)
                {
                    project.ReviewBy = GetLeaverReviewDateProjects(handover);
                }
            }

            //var people = package.Contacts.Where(c => c.Person is Staff).Select(c => c.Person as Staff);
            //if (priorities.PeopleEnabled)
            //{
            //    foreach (var person in people)
            //    {
            //        person.ReviewBy = GetLeaverReviewDatePeople(handover);
            //    }
            //}

            var coreTasks =
                package.Modules.SelectMany(m => m.TopicGroups.SelectMany(tg => tg.Topics.SelectMany(t => t.Tasks)))
                    .Where(t => t.IsCore);

            foreach (var task in coreTasks)
            {
                task.ReviewBy = GetLeaverReviewDateCoreTasks(handover);
            }
        }

        public void ClearLeaverReviewByDates(Package package)
        {
            var topics = package.Modules.SelectMany(m => m.TopicGroups.SelectMany(tg => tg.Topics));

            foreach (var topic in topics)
            {
                topic.ReviewBy = null;
                foreach (var task in topic.Tasks)
                {
                    task.ReviewBy = null;
                }
            }

            foreach (var project in package.Projects)
            {
                project.ReviewBy = null;
            }

            foreach (var contact in package.Contacts)
            {
                contact.ReviewBy = null;
            }
        }

        //private void SetOnboarderReviewByDates(Package package)
        //{
        //    var priorities
        //}

        public double GetReadyItemsPrecentage(Guid packageId)
        {
            double result = 0;

            var leaverPriorities = (LeaverPriorities) PackageService.GetByPackageId(packageId).Handover.LeavingPeriod.Priorities;

            var critical = MContext.Topic.Where(t => !t.Deleted.HasValue
                                                                     && t.ParentTopicGroup.ParentModule.Type == Module.ModuleType.Critical
                                                                     && t.ParentTopicGroup.ParentModule.Package.ID == packageId)
                                                                     .Cast<Reviewable>()
                                                                     .ToList();
            critical.AddRange(MContext.Task.Where(t => !t.Deleted.HasValue
                                                        && t.ParentTopic.ParentTopicGroup.ParentModule.Type == Module.ModuleType.Critical
                                                        && t.ParentTopic.ParentTopicGroup.ParentModule.Package.ID == packageId)
                                                        .Cast<Reviewable>());
            critical = critical.Distinct().ToList();


            var criticalTotal = critical.Count();
            var criticalReady = critical.Count(c => c.ReadyForReview);

            var relationships = MContext.Contact.Where(c => !c.Deleted.HasValue && c.PackageId == packageId);

            var relationshipsTotal = relationships.Count();
            var relationshipsReady = relationships.Count(r => r.ReadyForReview);

            var experiencesTotal = 0;
            var experiencesReady = 0;
            if (leaverPriorities.ExperiancesEnabled)
            {
                var experiences = MContext.Topic.Where(t => !t.Deleted.HasValue
                                                                     && t.ParentTopicGroup.ParentModule.Type == Module.ModuleType.Experiences
                                                                     && t.ParentTopicGroup.ParentModule.Package.ID == packageId)
                                                                     .Cast<Reviewable>()
                                                                     .ToList();
                experiences.AddRange(MContext.Task.Where(t => !t.Deleted.HasValue
                                                            && t.ParentTopic.ParentTopicGroup.ParentModule.Type == Module.ModuleType.Experiences
                                                            && t.ParentTopic.ParentTopicGroup.ParentModule.Package.ID == packageId)
                                                            .Cast<Reviewable>());

                experiencesTotal = experiences.Count();
                experiencesReady = experiences.Count(e => e.ReadyForReview);
            }

            var peopleTotal = 0;
            var peopleReady = 0;
            if (leaverPriorities.PeopleEnabled)
            {
                var people = PeopleService.GetStaffsByPackageId(packageId);

                peopleTotal = people.Count();
                peopleReady = people.Count(s => s.ReadyForReview);
            }

            var projectsTotal = 0;
            var projectsReady = 0;
            if (leaverPriorities.ProjectsEnabled)
            {
                var projects = MContext.ProjectInstance.Where(p => !p.Deleted.HasValue && p.PackageId == packageId);

                projectsTotal = projects.Count();
                projectsReady = projects.Count(p => p.ReadyForReview);
            }

            double total = criticalTotal + relationshipsTotal + experiencesTotal + peopleTotal + projectsTotal;
            double ready = criticalReady + relationshipsReady + experiencesReady + peopleReady + projectsReady;

            if (total > 0)
            {
                result = ready/total;
            }

            return result;
        }

        public double GetApprovedItemsPercentage(Guid packageId)
        {
            double result = 0;

            var leaverPriorities = (LeaverPriorities)PackageService.GetByPackageId(packageId).Handover.LeavingPeriod.Priorities;

            var critical = MContext.Topic.Where(t => !t.Deleted.HasValue
                                                                     && t.ParentTopicGroup.ParentModule.Type == Module.ModuleType.Critical
                                                                     && t.ParentTopicGroup.ParentModule.Package.ID == packageId)
                                                                     .Cast<Reviewable>()
                                                                     .ToList();
            critical.AddRange(MContext.Task.Where(t => !t.Deleted.HasValue
                                                        && t.ParentTopic.ParentTopicGroup.ParentModule.Type == Module.ModuleType.Critical
                                                        && t.ParentTopic.ParentTopicGroup.ParentModule.Package.ID == packageId)
                                                        .Cast<Reviewable>());


            var criticalTotal = critical.Count();
            var criticalApproved = critical.Count(Moderated.Approved().Compile());

            var relationships = MContext.Contact.Where(c => !c.Deleted.HasValue && c.PackageId == packageId);

            var relationshipsTotal = relationships.Count();
            var relationshipsApproved = relationships.Count(Moderated.Approved());

            var experiencesTotal = 0;
            var experiencesApproved = 0;
            if (leaverPriorities.ExperiancesEnabled)
            {
                var experiences = MContext.Topic.Where(t => !t.Deleted.HasValue
                                                                     && t.ParentTopicGroup.ParentModule.Type == Module.ModuleType.Experiences
                                                                     && t.ParentTopicGroup.ParentModule.Package.ID == packageId)
                                                                     .Cast<Reviewable>()
                                                                     .ToList();
                experiences.AddRange(MContext.Task.Where(t => !t.Deleted.HasValue
                                                            && t.ParentTopic.ParentTopicGroup.ParentModule.Type == Module.ModuleType.Experiences
                                                            && t.ParentTopic.ParentTopicGroup.ParentModule.Package.ID == packageId)
                                                            .Cast<Reviewable>());

                experiencesTotal = experiences.Count();
                experiencesApproved = experiences.Count(Moderated.Approved().Compile());
            }

            var peopleTotal = 0;
            var peopleApproved = 0;
            if (leaverPriorities.PeopleEnabled)
            {
                var people = PeopleService.GetStaffsByPackageId(packageId);

                peopleTotal = people.Count();
                peopleApproved = PeopleService.CountStaffsApproved(packageId);
            }

            var projectsTotal = 0;
            var projectsApproved = 0;
            if (leaverPriorities.ProjectsEnabled)
            {
                var projects = MContext.ProjectInstance.Where(p => !p.Deleted.HasValue && p.PackageId == packageId);

                projectsTotal = projects.Count();
                projectsApproved = projects.Count(Moderated.Approved());
            }

            double total = criticalTotal + relationshipsTotal + experiencesTotal + peopleTotal + projectsTotal;
            double approved = criticalApproved + relationshipsApproved + experiencesApproved + peopleApproved + projectsApproved;

            if (total > 0)
            {
                result = approved / total;
            }

            return result;
        }

        public int GetTotalItemsCount(Guid packageId)
        {
            int result = 0;

            var handover = PackageService.GetByPackageId(packageId).Handover;

            LeaverPriorities leaverPriorities = null;
            if (handover != null && handover.LeavingPeriod.Priorities.Saved.HasValue)
            {
                leaverPriorities = (LeaverPriorities) handover.LeavingPeriod.Priorities;
            }
            else
            {
                return result; //short circuit
            }
            

            var critical = MContext.Topic.Where(t => !t.Deleted.HasValue
                                                                     && t.ParentTopicGroup.ParentModule.Type == Module.ModuleType.Critical
                                                                     && t.ParentTopicGroup.ParentModule.Package.ID == packageId)
                                                                     .Cast<Reviewable>()
                                                                     .ToList();
            critical.AddRange(MContext.Task.Where(t => !t.Deleted.HasValue
                                                        && t.ParentTopic.ParentTopicGroup.ParentModule.Type == Module.ModuleType.Critical
                                                        && t.ParentTopic.ParentTopicGroup.ParentModule.Package.ID == packageId)
                                                        .Cast<Reviewable>());


            var criticalTotal = critical.Count();

            var relationships = MContext.Contact.Where(c => !c.Deleted.HasValue && c.PackageId == packageId);

            var relationshipsTotal = relationships.Count();

            int experiencesTotal = 0;
            if (leaverPriorities.ExperiancesEnabled)
            {
                var experiences = MContext.Topic.Where(t => !t.Deleted.HasValue
                                                                     && t.ParentTopicGroup.ParentModule.Type == Module.ModuleType.Experiences
                                                                     && t.ParentTopicGroup.ParentModule.Package.ID == packageId)
                                                                     .Cast<Reviewable>()
                                                                     .ToList();
                experiences.AddRange(MContext.Task.Where(t => !t.Deleted.HasValue
                                                            && t.ParentTopic.ParentTopicGroup.ParentModule.Type == Module.ModuleType.Experiences
                                                            && t.ParentTopic.ParentTopicGroup.ParentModule.Package.ID == packageId)
                                                            .Cast<Reviewable>());

                experiencesTotal = experiences.Count();
            }

            int peopleTotal = 0;
            if (leaverPriorities.PeopleEnabled)
            {
                //var people = MContext.Contact.Where(c => c.PackageId == packageId && c.Person is Staff && !c.Person.Deleted.HasValue);

                //peopleTotal = people.Count();
            }

            int projectsTotal = 0;
            if (leaverPriorities.PeopleEnabled)
            {
                var projects = MContext.ProjectInstance.Where(p => !p.Deleted.HasValue && p.PackageId == packageId);

                projectsTotal = projects.Count();
            }

            result = criticalTotal + relationshipsTotal + experiencesTotal + peopleTotal + projectsTotal;

            return result;
        }

        public int GetApprovedItemsCount(Guid packageId)
        {
            int result = 0;

            var handover = PackageService.GetByPackageId(packageId).Handover;

            LeaverPriorities leaverPriorities = null;
            if (handover != null && handover.LeavingPeriod.Priorities.Saved.HasValue)
            {
                leaverPriorities = (LeaverPriorities)handover.LeavingPeriod.Priorities;
            }
            else
            {
                return result; //short circuit
            }

            var critical = MContext.Topic.Where(t => !t.Deleted.HasValue
                                                                     && t.ParentTopicGroup.ParentModule.Type == Module.ModuleType.Critical
                                                                     && t.ParentTopicGroup.ParentModule.Package.ID == packageId)
                                                                     .Cast<Reviewable>()
                                                                     .ToList();
            critical.AddRange(MContext.Task.Where(t => !t.Deleted.HasValue
                                                        && t.ParentTopic.ParentTopicGroup.ParentModule.Type == Module.ModuleType.Critical
                                                        && t.ParentTopic.ParentTopicGroup.ParentModule.Package.ID == packageId)
                                                        .Cast<Reviewable>());


            var criticalReady = critical.Count(Moderated.Approved().Compile());

            var relationships = MContext.Contact.Where(c => !c.Deleted.HasValue && c.PackageId == packageId);

            var relationshipsReady = relationships.Count(Moderated.Approved());

            int experiencesReady = 0;
            if (leaverPriorities.ExperiancesEnabled)
            {
                var experiences = MContext.Topic.Where(t => !t.Deleted.HasValue
                                                                     && t.ParentTopicGroup.ParentModule.Type == Module.ModuleType.Experiences
                                                                     && t.ParentTopicGroup.ParentModule.Package.ID == packageId)
                                                                     .Cast<Reviewable>()
                                                                     .ToList();
                experiences.AddRange(MContext.Task.Where(t => !t.Deleted.HasValue
                                                            && t.ParentTopic.ParentTopicGroup.ParentModule.Type == Module.ModuleType.Experiences
                                                            && t.ParentTopic.ParentTopicGroup.ParentModule.Package.ID == packageId)
                                                            .Cast<Reviewable>());

                experiencesReady = experiences.Count(Moderated.Approved().Compile());
            }

            int peopleReady = 0;
            if (leaverPriorities.PeopleEnabled)
            {
                //var people = MContext.Contact.Where(c => c.PackageId == packageId && c.Person is Staff && !c.Person.Deleted.HasValue);

                //peopleReady = people.Count(Moderated.Approved());
            }

            int projectsReady = 0;
            if (leaverPriorities.PeopleEnabled)
            {
                var projects = MContext.ProjectInstance.Where(p => !p.Deleted.HasValue && p.PackageId == packageId);

                projectsReady = projects.Count(Moderated.Approved());
            }

            result = criticalReady + relationshipsReady + experiencesReady + peopleReady + projectsReady;


            return result;
        }

        public int GetRejectedItemsCount(Guid packageId)
        {
            int result = 0;

            var handover = PackageService.GetByPackageId(packageId).Handover;

            LeaverPriorities leaverPriorities = null;
            if (handover != null && handover.LeavingPeriod.Priorities.Saved.HasValue)
            {
                leaverPriorities = (LeaverPriorities)handover.LeavingPeriod.Priorities;
            }
            else
            {
                return result; //short circuit
            }

            var critical = MContext.Topic.Where(t => !t.Deleted.HasValue
                                                                     && t.ParentTopicGroup.ParentModule.Type == Module.ModuleType.Critical
                                                                     && t.ParentTopicGroup.ParentModule.Package.ID == packageId)
                                                                     .Cast<Reviewable>()
                                                                     .ToList();
            critical.AddRange(MContext.Task.Where(t => !t.Deleted.HasValue
                                                        && t.ParentTopic.ParentTopicGroup.ParentModule.Type == Module.ModuleType.Critical
                                                        && t.ParentTopic.ParentTopicGroup.ParentModule.Package.ID == packageId)
                                                        .Cast<Reviewable>());


            var criticalDeclined = critical.Count(Moderated.Declined().Compile());

            var relationships = MContext.Contact.Where(c => !c.Deleted.HasValue && c.PackageId == packageId);

            var relationshipsDeclined = relationships.Count(Moderated.Declined());

            int experiencesDeclined = 0;
            if (leaverPriorities.ExperiancesEnabled)
            {
                var experiences = MContext.Topic.Where(t => !t.Deleted.HasValue
                                                                     && t.ParentTopicGroup.ParentModule.Type == Module.ModuleType.Experiences
                                                                     && t.ParentTopicGroup.ParentModule.Package.ID == packageId)
                                                                     .Cast<Reviewable>()
                                                                     .ToList();
                experiences.AddRange(MContext.Task.Where(t => !t.Deleted.HasValue
                                                            && t.ParentTopic.ParentTopicGroup.ParentModule.Type == Module.ModuleType.Experiences
                                                            && t.ParentTopic.ParentTopicGroup.ParentModule.Package.ID == packageId)
                                                            .Cast<Reviewable>());

                experiencesDeclined = experiences.Count(Moderated.Declined().Compile());
            }

            int peopleDecliend = 0;
            if (leaverPriorities.PeopleEnabled)
            {
                //var people = MContext.Contact.Where(c => c.PackageId == packageId && c.Person is Staff && !c.Person.Deleted.HasValue);

                //peopleDecliend = people.Count(Moderated.Declined());
            }

            int projectsDeclined = 0;
            if (leaverPriorities.PeopleEnabled)
            {
                var projects = MContext.ProjectInstance.Where(p => !p.Deleted.HasValue && p.PackageId == packageId);

                projectsDeclined = projects.Count(Moderated.Declined());
            }

            result = criticalDeclined + relationshipsDeclined + experiencesDeclined + peopleDecliend + projectsDeclined;


            return result;
        }

        public int? GetOnboardingDuration(Guid packageId)
        {
            return GetOnboardingPeriodForPackage(packageId) == null ? null : GetOnboardingPeriodForPackage(packageId).Duration;
        }
    }
}