﻿using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Activate.Services.Helpers;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.People;

namespace MomentumPlus.Activate.Services
{
    /// <summary>
    /// Audit Service
    /// </summary>
    public class AuditService : ServiceBase, IAuditService
    {
        private IMeetingService MeetingService;

        public AuditService(MomentumContext context, IMeetingService meetingService, IContactService contactService, User user)
            : base(context, user)
        {
            MeetingService = meetingService;
        }

        #region IAuditService Members

        /// <summary>
        /// Record activity on an entity
        /// </summary>
        /// <typeparam name="T">The entity type</typeparam>
        /// <param name="eventType">The event type</param>
        /// <param name="entityId">The entity GUID</param>
        public void RecordActivity<T>(AuditLog.EventType eventType, Guid? entityId)
        {
            UpdateAuditLog<T>(eventType, entityId);

            UpdateLastViewed<T>(eventType, entityId);
        }

        /// <summary>
        /// Get the audit log entry for the time an entity was last
        /// viewed by a user
        /// </summary>
        /// <typeparam name="T">The entity type</typeparam>
        /// <param name="userId">The user GUID</param>
        /// <param name="entityId">The entity GUID</param>
        /// <returns>An AuditLog object</returns>
        public AuditLog LastViewed<T>(Guid userId, Guid entityId)
        {
            return
                LastViewed<T>(userId, new List<Guid>() { entityId }).FirstOrDefault();
        }

        /// <summary>
        /// Get the last viewed audit logs for a set of entities and user
        /// </summary>
        /// <typeparam name="T">The entity type</typeparam>
        /// <param name="userId">The user GUID</param>
        /// <param name="entityIds">A list of entity GUIDs</param>
        /// <returns>A List of AuditLog objects</returns>
        public List<AuditLog> LastViewed<T>(Guid userId, List<Guid> entityIds)
        {
            string entityType = typeof (T).ToString();
            List<AuditLog> logItems = (from al in MContext.AuditLog
                where
                    al.Event == AuditLog.EventType.Viewed &&
                    al.UserId.HasValue && al.UserId.Value == userId &&
                    al.EntityId.HasValue && entityIds.Contains(al.EntityId.Value) &&
                    al.EntityType == entityType
                orderby
                    al.EventDateTime
                select al).ToList();

            return logItems;
        }

        #endregion

        #region private methods

        /// <summary>
        /// Add new AuditLog entry for an entity
        /// </summary>
        /// <typeparam name="T">The entity type</typeparam>
        /// <param name="eventType">Event type</param>
        /// <param name="entityId">Entity GUID</param>
        private void UpdateAuditLog<T>(AuditLog.EventType eventType, Guid? entityId)
        {
            Guid? userId = null;
            if (User != null)
                userId = User.ID;

            Type typeParameterType = typeof(T);

            AuditLog auditLog = new AuditLog()
            {
                EntityType = typeof(T).ToString(),
                EventDateTime = SystemTime.Now,
                EventName = eventType.ToString(),
                Event = eventType,
                UserId = userId,
                EntityId = entityId
            };

            MContext.AuditLog.Add(auditLog);
            MContext.SaveChanges();
        }

        /// <summary>
        /// Update the LastViewed property on an entity to the current time
        /// </summary>
        /// <typeparam name="T">The entity type</typeparam>
        /// <param name="eventType">The event type</param>
        /// <param name="entityId">The entity ID</param>
        private void UpdateLastViewed<T>(AuditLog.EventType eventType, Guid? entityId)
        {
            PackageService packageService = new PackageService(MContext, MeetingService, User);
            
            var now = SystemTime.Now;
            
            if (eventType == AuditLog.EventType.Viewed && entityId.HasValue)
            {
                if (typeof(T) == typeof(Task))
                {
                    var task = MContext.Task.Find(entityId.Value);

                    if (task.IsOwner(User, packageService))
                    {
                        task.UserLastViewed = now;
                        
                        MContext.SaveChanges();
                    }
                } else if (typeof(T) == typeof(Topic))
                {
                    var topic = MContext.Topic.Find(entityId.Value);

                    if (topic.IsOwner(User, packageService))
                    {
                        topic.UserLastViewed = now;
                        MContext.SaveChanges();
                    }
                } else if (typeof(T) == typeof(TopicGroup))
                {
                    var topicGroup = MContext.TopicGroup.Find(entityId.Value);

                    if (topicGroup.IsOwner(User, packageService))
                    {
                        topicGroup.UserLastViewed = now;
                    }
                } else if (typeof(T) == typeof(Module))
                {
                    var module = MContext.Module.Find(entityId.Value);

                    if (module.IsOwner(User, packageService))
                    {
                        module.UserLastViewed = now;
                    }
                } else if (typeof(T) == typeof(Contact))
                {
                    var contact = MContext.Contact.Find(entityId.Value);

                    if (contact.IsOwner(User, packageService))
                    {
                        contact.UserLastViewed = now;
                    }
                } else if (typeof(T) == typeof(Meeting))
                {
                    var meeting = MContext.Meeting.Find(entityId.Value);

                    if (meeting.IsOwner(User, packageService))
                    {
                        meeting.UserLastViewed = now;
                    }
                }else if (typeof (T) == typeof (Package))
                {
                    var package = MContext.Package.Find(entityId.Value);

                    if (package.IsOwner(User, packageService))
                    {
                        // Update package status to In Progress on first view
                        if (!package.UserLastViewed.HasValue)
                        {
                            package.PackageStatus = Package.Status.InProgress;
                        }

                        package.UserLastViewed = now;
                    }
                }
                else if (typeof(T) == typeof(DocumentLibrary))
                {
                    var doc = MContext.DocumentLibrary.Find(entityId.Value);

                    if (doc.IsOwner(User, packageService))
                    {
                        doc.UserLastViewed = now;
                    }
                }
                else if (typeof(T) == typeof(ProjectInstance))
                {
                    var projectInstance = MContext.ProjectInstance.Find(entityId.Value);

                    if (projectInstance.IsOwner(User, packageService))
                    {
                        projectInstance.UserLastViewed = now;
                    }
                }
                else if (typeof(T) == typeof(Staff))
                {
                    // not audited
                }
                else
                {
                    throw new NotImplementedException(
                        string.Format("Audit Log does not support Entity of type {0}", typeof(T)));   
                }

                MContext.SaveChanges();
            }
        }

        #endregion
    }
}
