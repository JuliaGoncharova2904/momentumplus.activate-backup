﻿using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Activate.Services.Interfaces;
using System.Web;
using MomentumPlus.Activate.Services.Helpers;
using System.Data.Entity;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Services
{
    /// <summary>
    /// Master List Service
    /// Handles Systems, Keywords, Expert Users, 
    /// Organisation Contacts, Projects, Websites
    /// Assets, Companies, Search Tags
    /// </summary>
    public class MasterListService : ServiceBase, IMasterListService
    {
        public MasterListService(MomentumContext context) : base(context) { }

        private const string cacheKey = "MasterLists";

        #region IMasterListService Members

        /// <summary>
        /// Get all non-deleted MasterList entries
        /// </summary>
        /// <returns></returns>
        public IEnumerable<MasterList> GetAll()
        {
            var masterListItems =
                MContext.MasterList.Where(x => !x.Deleted.HasValue).ToList();

            return masterListItems;
        }

        /// <summary>
        /// Get all MasterList items of a given type
        /// </summary>
        /// <param name="listType"></param>
        /// <returns></returns>
        public IEnumerable<MasterList> GetAll(MasterList.ListType listType)
        {
            IEnumerable<MasterList> masterLists =
                CacheHelper.GetCached<IEnumerable<MasterList>>(cacheKey, GetAll);

            var masterListItems =
                masterLists.Where(m => m.Type == listType);

            return masterListItems;
        }

        /// <summary>
        /// Get a specific MasterList by ID
        /// </summary>
        /// <param name="masterListId"></param>
        /// <returns></returns>
        public MasterList GetByMasterListId(Guid masterListId)
        {
            IEnumerable<MasterList> masterLists =
                CacheHelper.GetCached<IEnumerable<MasterList>>(cacheKey, GetAll);

            var masterList =
                masterLists.Where(m => m.ID == masterListId).FirstOrDefault();

            return masterList;
        }

        /// <summary>
        /// Search master list of a given type - stored and paged
        /// </summary>
        /// <param name="listType">The master list type</param>
        /// <param name="query">The search query</param>
        /// <param name="pageNo">The page number</param>
        /// <param name="pageSize">The page size</param>
        /// <param name="sortColumn">The sort column</param>
        /// <param name="sortAscending">The sort order</param>
        /// <returns></returns>
        public IEnumerable<MasterList> Search(MasterList.ListType listType, string query, int pageNo, int pageSize, MasterList.SortColumn sortColumn, bool sortAscending)
        {
            var searchResults =
                SearchQuerable(listType, query);

            SortAndPage<MasterList>(pageNo, pageSize, sortColumn.ToString(), sortAscending, ref searchResults);

            return searchResults;
        }

        /// <summary>
        /// Get the search result count for a given master list and query
        /// </summary>
        /// <param name="listType"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        public int SearchCount(MasterList.ListType listType, string query)
        {
            return
                SearchQuerable(listType, query).Count();
        }

        /// <summary>
        /// Search master list of a given type
        /// </summary>
        /// <param name="listType"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        private IQueryable<MasterList> SearchQuerable(MasterList.ListType listType, string query)
        {
            IEnumerable<MasterList> masterLists =
                CacheHelper.GetCached<IEnumerable<MasterList>>(cacheKey, GetAll);

            IQueryable<MasterList> searchResults = null;

            if (!string.IsNullOrWhiteSpace(query))
            {
                searchResults =
                    masterLists.Where(m => m.Type == listType && (m.Name.ToLower().Contains(query.ToLower()) || !string.IsNullOrWhiteSpace(m.Notes) && m.Notes.ToLower().Contains(query.ToLower()))).AsQueryable();
            }
            else
            {
                searchResults =
                    masterLists.Where(m => m.Type == listType).AsQueryable();
            }

            return searchResults;
        }

        /// <summary>
        /// Add a new MasterList
        /// </summary>
        /// <param name="masterList">New MasterList object</param>
        /// <returns>The added MasterList object</returns>
        public MasterList Add(MasterList masterList)
        {
            masterList =
                MContext.MasterList.Add(masterList);

            MContext.SaveChanges();

            RefreshCache();

            return masterList;
        }

        /// <summary>
        /// Update an existing MasterList
        /// </summary>
        /// <param name="masterList">Existing MasterList object</param>
        /// <returns>The updated MasterList</returns>
        public MasterList Update(MasterList masterList)
        {
            MContext.Entry(masterList).State = EntityState.Modified;

            MContext.SaveChanges();

            RefreshCache();

            return masterList;
        }

        /// <summary>
        /// Returns true if there is already a master list entry with the given name in
        /// a specific master list.
        /// </summary>
        /// <param name="listType">Master list type</param>
        /// <param name="name">Master list item name</param>
        /// <returns></returns>
        public bool Exists(MasterList.ListType listType, string name)
        {
            bool exists = false;

            IEnumerable<MasterList> masterLists =
                CacheHelper.GetCached<IEnumerable<MasterList>>(cacheKey, GetAll);

            return masterLists.Any(m => m.Name == name); ;
        }

        /// <summary>
        /// Delete a MasterList from the database
        /// </summary>
        /// <param name="masterListId">The MasterList GUID</param>
        public void Delete(Guid masterListId)
        {
            var masterList = MContext.MasterList.Find(masterListId);

            masterList.Deleted = DateTime.Now;

            MContext.SaveChanges();

            RefreshCache();
        }

        /// <summary>
        /// Get the default company ID from the companies master list
        /// </summary>
        /// <returns></returns>
        public Guid? GetDefaultCompanyId()
        {
            IEnumerable<MasterList> companies = GetAll(MasterList.ListType.Companies);

            MasterList defaultCompany = companies
                .FirstOrDefault(c => !String.IsNullOrWhiteSpace(c.AdditionalField3) &&
                             Boolean.Parse(c.AdditionalField3));

            if (defaultCompany != null)
            {
                return defaultCompany.ID;
            }
            else
            {
                return Guid.Empty;
            }
        }


        /// <summary>
        /// Get company from ID
        /// </summary>
        /// <param name="companyId">Company ID</param>
        /// <returns></returns>
        public MasterList GetCompany(Guid companyId)
        {
            var company = MContext.MasterList.First(c => c.ID == companyId && c.Type == MasterList.ListType.Companies);

            return company;
        }



        #endregion

        /// <summary>
        /// Refresh the master list cache
        /// </summary>
        public void RefreshCache()
        {
            var httpContext =
                HttpContext.Current;

            if (httpContext != null)
                httpContext.Cache.Remove(cacheKey);

            CacheHelper.GetCached<IEnumerable<MasterList>>(cacheKey, GetAll);
        }
    }
}
