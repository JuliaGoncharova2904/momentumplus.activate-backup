﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models.People;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.Requests;
using MomentumPlus.Core.Models.Moderation;

namespace MomentumPlus.Activate.Services
{
    /// <summary>
    /// People Service
    /// </summary>
    public class PeopleService : ServiceBase, IPeopleService
    {
        public PeopleService(MomentumContext context) : base(context) { }

        #region IPeopleService Members

        /// <summary>
        /// Update Person
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="person"></param>
        /// <returns></returns>
        public T UpdatePerson<T>(T person) where T : Person
        {
            MContext.Entry(person).State = EntityState.Modified;
            MContext.SaveChanges();

            return person;
        }

        /// <summary>
        /// Gets a Contact
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Contact GetStaff(Guid id)
        {
            return MContext.Contact.FirstOrDefault(p => p.ID == id && p.Managed);
        }

        /// <summary>
        /// Gets a Contact by Package Id
        /// </summary>
        /// <param name="packageId"></param>
        /// <returns></returns>
        public IEnumerable<Contact> GetStaffsByPackageId(Guid packageId)
        {
            IQueryable<Contact> staffs = MContext.Contact.Include(c => c.Moderations)
                .Where(c => c.PackageId == packageId && c.Managed && !c.Deleted.HasValue)
                                                                   .Include(c => c.Company)
                                                                   .Include(c => c.Moderations);
            return staffs;
        }

        /// <summary>
        /// Gets Contacts by Package Id
        /// </summary>
        /// <param name="packageId"></param>
        /// <param name="pageNo"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortAscending"></param>
        /// <param name="deleted"></param>
        /// <returns></returns>
        public IEnumerable<Contact> GetStaffsByPackageId(Guid packageId, int pageNo, int pageSize, Person.SortColumn? sortColumn, bool sortAscending = true, bool deleted = false)
        {
            IQueryable<Contact> staffs = MContext.Contact.Where(c => c.PackageId == packageId && c.Managed && c.Deleted.HasValue == deleted)
                                                                   .Include(c => c.Company);

            if (sortColumn.HasValue)
            {
                SortAndPage(ref staffs, p => p, sortColumn.ToString(), sortAscending, pageNo, pageSize);
            }

            return staffs;
        }

        /// <summary>
        /// Count Contacts by Package Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="deleted"></param>
        /// <returns></returns>
        public int CountStaffsByPackageId(Guid id, bool deleted)
        {
            return MContext.Contact.Count(c => c.PackageId == id && c.Managed && c.Deleted.HasValue == deleted);
        }

        /// <summary>
        /// Search Contacts by Package Id
        /// </summary>
        /// <param name="packageId"></param>
        /// <param name="term"></param>
        /// <param name="pageNo"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortAscending"></param>
        /// <param name="deleted"></param>
        /// <returns></returns>
        public IEnumerable<Contact> SearchStaffs(Guid packageId, string term, int pageNo, int pageSize, Person.SortColumn? sortColumn, bool sortAscending = true, bool deleted = false)
        {
            term = term.ToLower();

            IQueryable<Contact> staffs = MContext.Contact.Where(c => c.PackageId == packageId
                && c.Managed
                && c.Deleted.HasValue == deleted
                && (string.Concat(c.FirstName, " ", c.LastName).ToLower().Contains(term))
                    || c.Position.ToLower().Contains(term)
                    || c.Company.Name.ToLower().Contains(term))
                    .Include(c => c.Company);

            if (sortColumn.HasValue)
            {
                SortAndPage(ref staffs, p => p, sortColumn.ToString(), sortAscending, pageNo, pageSize);
            }

            return staffs;
        }

        /// <summary>
        /// Get Contact Transfer Request
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public StaffTransferRequest GetStaffTransferRequestById(Guid id)
        {
            return MContext.Moderated.OfType<StaffTransferRequest>().FirstOrDefault(r => r.ID == id);
        }

        /// <summary>
        /// Transfer Contacts
        /// </summary>
        /// <param name="request">StaffTranserRequest</param>
        /// <param name="DestincationContactId">Destination Contact Id</param>
        public void TransferStaff(StaffTransferRequest request, Guid? DestincationContactId = null)
        {
            if (request.IsApproved())
            {
                if (DestincationContactId.HasValue)
                {
                    var contact = MContext.Contact.Find(DestincationContactId.Value);
                    request.DestinationContact = contact;
                }

                request.Complete();

                MContext.Entry(request).State = EntityState.Modified;

                MContext.SaveChanges();
            }
        }

        /// <summary>
        /// Count approved Contacts
        /// </summary>
        /// <param name="id">Package Id</param>
        /// <returns>Number of Contacts</returns>
        public int CountStaffsApproved(Guid id)
        {
            var staffs = GetStaffsByPackageId(id).ToList();
            var moderatedStaffs = staffs.Select(s => new ModeratedStaff()
            {
                Moderations = s.Moderations,
                ReadyForReview = s.ReadyForReview
            });
            return moderatedStaffs.Count(s => s.IsApproved());
        }

        /// <summary>
        /// Get last created Contact
        /// </summary>
        /// <param name="id">Package Id</param>
        /// <returns>Date time of the last created Contact</returns>
        public DateTime? GetDateLastEntry(Guid id)
        {
            var staff = GetStaffsByPackageId(id).OrderBy(s => s.Created);
            return staff.Any() ? staff.Last().Created : null;
        }

        #endregion
    }
}
