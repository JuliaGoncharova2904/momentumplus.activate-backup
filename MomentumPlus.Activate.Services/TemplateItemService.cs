﻿using System;
using System.Collections.Generic;
using MomentumPlus.Activate.Services.Interfaces;
using System.Data.Entity;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Services
{
    public class TemplateItemService : ServiceBase, ITemplateItemService
    {
        public TemplateItemService(MomentumContext context) : base(context) { }

        /// <summary>
        /// Add a Template
        /// </summary>
        /// <param name="templateId">Template Id</param>
        /// <param name="templateItem">Template Item</param>
        /// <param name="type">Module type</param>
        /// <returns>Template</returns>
        public Template Add(Guid templateId, TemplateItem templateItem, Module.ModuleType type)
        {
            Template template
                     = MContext.Template.Find(templateId);

            switch (type)
            {
                case Module.ModuleType.Critical:
                    template.TemplateItemsCritical.Add(templateItem);
                    break;
                case Module.ModuleType.Experiences:
                    template.TemplateItemsExperiences.Add(templateItem);
                    break;
                case Module.ModuleType.Project:
                    template.TemplateItemsProjects.Add(templateItem);
                    break;
                default:
                    throw new Exception(string.Format("Unknown Module Type '{0}'.", 
                        type.ToString()));
            }

            MContext.SaveChanges();

            return template;
        }

        /// <summary>
        /// Update a Template Item
        /// </summary>
        /// <param name="templateItem">TemplateItem to delete</param>
        /// <returns>TemplateItem</returns>
        public TemplateItem Update(TemplateItem templateItem)
        {
            MContext.Entry(templateItem).State = EntityState.Modified;

            MContext.SaveChanges();

            return templateItem;
        }

        /// <summary>
        /// Delete a Template Item
        /// </summary>
        /// <param name="templateItem">TemplateItem to delete</param>
        public void Delete(TemplateItem templateItem)
        {
            MContext.TemplateItem.Remove(templateItem);

            MContext.SaveChanges();
        }

        /// <summary>
        /// Get Template Items
        /// </summary>
        /// <param name="templateId"></param>
        /// <param name="type"></param>
        /// <returns>Collection of Template items</returns>
        public IEnumerable<TemplateItem> Get(Guid templateId, Module.ModuleType type)
        {
            IEnumerable<TemplateItem> templateItems =
                new List<TemplateItem>();

            var template = 
                MContext.Template.Find(templateId);

            switch (type)
            {
                case Module.ModuleType.Critical:
                    templateItems = template.TemplateItemsCritical;
                    break;
                case Module.ModuleType.Experiences:
                    templateItems = template.TemplateItemsExperiences;
                    break;
                case Module.ModuleType.Project:
                    templateItems = template.TemplateItemsProjects;
                    break;
                default:
                    throw new Exception(string.Format("Unknown Module Type '{0}'.",
                  type.ToString()));
            }

            return templateItems;

        }
    }
}
