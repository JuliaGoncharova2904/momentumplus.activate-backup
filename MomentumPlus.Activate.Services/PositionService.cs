﻿using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Activate.Services.Interfaces;
using System.Data.Entity;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Services
{
    /// <summary>
    /// Position Service
    /// </summary>
    public class PositionService : ServiceBase, IPositionService
    {
        public PositionService(MomentumContext context) : base(context) { }

        #region IPositionService Members

        /// <summary>
        /// Create new Position
        /// </summary>
        /// <returns>Position</returns>
        public Position NewPosition()
        {
            return MContext.Position.Create();
        }

        /// <summary>
        /// Add a Position
        /// </summary>
        /// <param name="position">Position</param>
        /// <returns>Position</returns>
        public Position Add(Position position)
        {
            position =
                MContext.Position.Add(position);

            MContext.SaveChanges();

            return position;
        }

        /// <summary>
        /// Get all Positions
        /// </summary>
        /// <param name="sortColumn">Column to sort results by</param>
        /// <param name="sortAscending">Sort Ascending</param>
        /// <returns></returns>
        public IEnumerable<Position> GetAll(Position.SortColumn? sortColumn = Position.SortColumn.Name, bool sortAscending = true)
        {
            var positions =
                MContext.Position.Where(p => !p.Deleted.HasValue);

            if (sortColumn.HasValue)
            {
                SortAndPage<Position>(1, positions.Count(), sortColumn.Value.ToString(), sortAscending, ref positions);
            }

            return positions.ToList();
        }

        /// <summary>
        /// Count Positions
        /// </summary>
        /// <returns>Return count</returns>
        public int Count()
        {
            return GetAll().Count();
        }

        /// <summary>
        /// Get Position by Id
        /// </summary>
        /// <param name="positionId">Position id</param>
        /// <returns>Position</returns>
        public Position GetByPositionId(Guid positionId)
        {
            var position =
                MContext.Position.Find(positionId);

            return position;
        }

        /// <summary>
        /// Update Position
        /// </summary>
        /// <param name="position">Position id</param>
        /// <returns>Position</returns>
        public Position Update(Position position)
        {
            MContext.Entry(position).State = EntityState.Modified;

            MContext.SaveChanges();

            return position;
        }

        /// <summary>
        /// Delete a Position
        /// </summary>
        /// <param name="positionId">Position to delete</param>
        public void Delete(Guid positionId)
        {
            var position = MContext.Position.Find(positionId);

            MContext.Position.Remove(position);

            MContext.SaveChanges();
        }

        /// <summary>
        /// Check if a Postion exists
        /// </summary>
        /// <param name="positionName">Postion name</param>
        /// <returns>True if exists</returns>
        public bool Exists(string positionName)
        {
            return MContext.Position.Any(p => !p.Deleted.HasValue && p.Name == positionName);
        }

        /// <summary>
        /// Search Positions 
        /// </summary>
        /// <param name="query">Search query</param>
        /// <param name="pageNo">Page no.</param>
        /// <param name="pageSize">Result size</param>
        /// <param name="sortColumn">Sort column</param>
        /// <param name="sortAscending">Sort ascending</param>
        /// <returns></returns>
        public IEnumerable<Position> Search(string query, int pageNo, int pageSize, Position.SortColumn sortColumn, bool sortAscending)
        {
            IEnumerable<Position> searchResults =
                SearchBase(query, pageNo, pageSize, sortColumn, sortAscending);

            return searchResults;
        }

        /// <summary>
        /// Count number of Positions
        /// </summary>
        /// <param name="query">Search query</param>
        /// <returns>Number of positions that match the query</returns>
        public int SearchCount(string query)
        {
            IEnumerable<Position> searchResults = SearchBase(query);

            return searchResults == null ? 0 : searchResults.Count();
        }

        /// <summary>
        /// Search helper method
        /// </summary>
        /// <param name="query">Search query</param>
        /// <returns></returns>
        private IQueryable<Position> SearchBase(string query)
        {
            IQueryable<Position> positions =
                 MContext.Position.Where(p => !p.Deleted.HasValue);

            if (!string.IsNullOrWhiteSpace(query))
            {
                positions =
                    positions.Where(p => p.Name.Contains(query)).AsQueryable();
            }

            return positions;
        }

        /// <summary>
        /// Search helper method
        /// </summary>
        /// <param name="query">Search query</param>
        /// <param name="pageNo">Page number</param>
        /// <param name="pageSize">Result size</param>
        /// <param name="sortColumn">Sort column</param>
        /// <param name="sortAscending">Sort ascending</param>
        /// <returns></returns>
        private IQueryable<Position> SearchBase(string query, int pageNo, int pageSize, Position.SortColumn sortColumn, bool sortAscending)
        {
            IQueryable<Position> positions = SearchBase(query);

            SortAndPage<Position>(pageNo, pageSize, sortColumn.ToString(), sortAscending, ref positions);

            return positions;
        }

        /// <summary>
        /// Search Attachments
        /// </summary>
        /// <param name="searchString">Search string</param>
        /// <param name="packageId">Package to search</param>
        /// <returns></returns>
        public IEnumerable<Attachment> SearchAttachments(string searchString, Guid packageId)
        {
            RestrictedAccess restrictedAccess =
                new RestrictedAccess(MContext, packageId);

            IQueryable<Attachment> searchResults = (from a in restrictedAccess.PositionAttachments
                                                    where a.Name.Contains(searchString) ||
                                                    a.SearchTags.Contains(searchString)
                                                    select a);
            return searchResults.ToList();    
        }

        /// <summary>
        /// Search Voice Messages
        /// </summary>
        /// <param name="searchString">Search string</param>
        /// <param name="packageId">Package to search</param>
        /// <returns></returns>
        public IEnumerable<VoiceMessage> SearchVoiceMessages(string searchString, Guid packageId)
        {
            RestrictedAccess restrictedAccess =
               new RestrictedAccess(MContext, packageId);

            IQueryable<VoiceMessage> searchResults = (from v in restrictedAccess.ContactVoiceMessages
                                                      where v.Name.Contains(searchString) ||
                                                      v.SearchTags.Contains(searchString)
                                                      select v);
            return searchResults.ToList();
        }

        #endregion
    }
}
