﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Services
{
    /// <summary>
    /// Template Service
    /// </summary>
    public class TemplateService : ServiceBase, ITemplateService
    {
        public TemplateService(MomentumContext context) : base(context) { }

        #region ITemplateService Members

        /// <summary>
        /// Get template by Template Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Template GetByTemplateId(Guid id)
        {
            var template =
               MContext.Template.Find(id);

            return template;
        }

        /// <summary>
        /// Create a new Template
        /// </summary>
        /// <param name="name"></param>
        /// <param name="durationDays"></param>
        /// <param name="notes"></param>
        /// <returns></returns>
        public Guid Add(string name, int durationDays, string notes)
        {
            Template template =
              new Template { Name = name, DurationDays = durationDays, Notes = notes, ExperiencesEnabled = true };

            template =
                MContext.Template.Add(template);

            MContext.SaveChanges();

            return template.ID;
        }

        /// <summary>
        /// Get Template Items by Template Id
        /// </summary>
        /// <param name="templateId"></param>
        /// <param name="moduleType"></param>
        /// <returns></returns>
        public IEnumerable<TemplateItem> GetById(Guid templateId, Module.ModuleType moduleType)
        {
            // Get all default items from framework
            IEnumerable<TemplateItem> templateItemsFramework
                = GetItemsFromFramework(moduleType);

            // Get any from database and set to enable in framework items
            IEnumerable<TemplateItem> templateItemsDatabase =
                GetTemplateItems(templateId, moduleType);

            foreach (TemplateItem templateItemFramework in templateItemsFramework)
            {
                var dbItem = (from t in templateItemsDatabase
                              where
                              t.SourceReferenceId == templateItemFramework.SourceReferenceId &&
                              t.SourceParentReferenceId == templateItemFramework.SourceParentReferenceId &&
                              t.Type == templateItemFramework.Type
                              select t).FirstOrDefault();

                // if dbItem is in the database,
                // mark framework item as true
                if (dbItem != null)
                    templateItemFramework.Enabled = true;
            }

            return templateItemsFramework;
        }

        /// <summary>
        /// Save the Template
        /// </summary>
        /// <param name="templateId"></param>
        /// <param name="templateItemsUI"></param>
        /// <param name="moduleType"></param>
        /// <param name="updatePackages"></param>
        public void Save(Guid templateId, List<TemplateItem> templateItemsUI, Module.ModuleType moduleType, bool updatePackages = false)
        {
            // Update Template Items in the database
            SaveTemplate(templateId, templateItemsUI, moduleType);

            // Update Package items that have been newly selected in the template
            if (updatePackages)
                UpdatePackages(templateId, templateItemsUI, moduleType);
        }

        /// <summary>
        /// Updates Packages based in the changes
        /// to the Template
        /// </summary>
        /// <param name="templateId">Packages to update</param>
        /// <param name="templateItemsUI">Template Items</param>
        /// <param name="moduleType">Only update items in Package of Module Type</param>
        private void UpdatePackages(Guid templateId, List<TemplateItem> templateItemsUI, Module.ModuleType moduleType)
        {
            // Update Project Items
            if (moduleType == Module.ModuleType.Project)
            {
                var frameworkProjects = templateItemsUI.Select(i => MContext.Project.FirstOrDefault(p => !p.Deleted.HasValue && p.ReferenceId == i.SourceReferenceId)).ToList();
                
                var packagesToUpdate = MContext.Package.Where(p => !p.Deleted.HasValue && p.Position.TemplateId == templateId)
                                                                   .Include(p => p.Projects).ToList();

                foreach (var package in packagesToUpdate)
                {
                    var newProjects = frameworkProjects.Where( i => !package.Projects.Any( p => p.ProjectId == i.ID ) );

                    if (newProjects.Any())
                    {
                        package.Projects.ToList().AddRange( newProjects.Select( p => p.CreateInstance(package.ID) ) );
                    }
                }

                MContext.SaveChanges();

                return;
            }

            TopicGroupService topicGroupService = 
                new TopicGroupService(MContext);

            TopicService topicService =
                new TopicService(MContext);

            TaskService taskService =
                new TaskService(MContext, User);


            #region new 

            // Get all live Modules that are affected
            // by the template changes
            var packages = MContext.Package.Where(p => !p.Deleted.HasValue);
            var modules = (from pa in packages
                           from mo in pa.Modules
                           where
                               pa.Position.TemplateId.HasValue &&
                               pa.Position.TemplateId.Value == templateId &&
                               mo.Type == moduleType &&
                               mo.IsFrameworkDefault == false
                           select mo).ToList();

            foreach (Module module in modules)
            {
                // Process Topic Group template items
                foreach (TemplateItem tiTopicGroup in
                    templateItemsUI.Where(i => i.Type == TemplateItem.TemplateItemType.TopicGroup))
                {
                    // Get topic group from the module, 
                    // if it does't exist, clone one!
                    TopicGroup topicGroup = null;

                    // Get the item from Framework first so we can 
                    // Identify it's Type (e.g. Business Process/Systems etc...
                    TopicGroup topicGroupFramework =
                        MContext.TopicGroup.FirstOrDefault(t => !t.Deleted.HasValue && t.ReferenceId == tiTopicGroup.SourceReferenceId);

                    // Get it from the current live model, if
                    // it does not exist, clone one from the Framework
                    topicGroup = (from tg in module.TopicGroups
                                  where tg.TopicGroupType == topicGroupFramework.TopicGroupType
                                  select tg).FirstOrDefault();

                    if (topicGroup == null) // It doesn't exist, clone from Framework
                    {
                        topicGroup =  MContext.TopicGroup
                                            .Include("Topics.Tasks.Attachments.Data")
                                            .Include("Topics.Tasks.VoiceMessages.Data")
                                            .Include("Topics.Tasks.DocumentLibraries.Attachment")
                                            .Include("Topics.Attachments.Data")
                                            .Include("Topics.VoiceMessages.Data")
                                            .Include("Topics.DocumentLibraries.Attachment")
                                            .FirstOrDefault(tg => !tg.Deleted.HasValue &&  tg.ID == topicGroupFramework.ID)
                                            .DeepClone();

                        topicGroup.ParentModule = null;
                        topicGroup.ParentModuleId = module.ID;

                        // Add the new Topic Group to the module
                        module.TopicGroups.Add(topicGroup);
                    }

                    // Process Topic Template Items that belong to the Topic Group
                    foreach (TemplateItem tiTopic in
                        templateItemsUI.Where(i => i.Type == TemplateItem.TemplateItemType.Topic
                            && i.SourceParentReferenceId == tiTopicGroup.SourceReferenceId))
                    {
                            Topic topicFramework =
                                    MContext.Topic.Where(
                                    t => !t.Deleted.HasValue && t.ReferenceId == tiTopic.SourceReferenceId).FirstOrDefault();

                            Topic topic = (from to in topicGroup.Topics
                                     where to.Name == topicFramework.Name
                                     select to).FirstOrDefault();

                            if (topic == null)
                            {
                                topic = MContext.Topic.Where(t => !t.Deleted.HasValue && t.ID == topicFramework.ID)
                                                    .Include("Attachments.Data")
                                                    .Include("VoiceMessages.Data")
                                                    .Include("DocumentLibraries.Attachment")
                                                    .FirstOrDefault()
                                                    .DeepClone();

                                topic.Tasks.Clear();

                                topic.ParentTopicGroup = null;
                                topic.ParentTopicGroupId = topicGroup.ID;

                                //topic.ReattachDocumentLibraries((UnitOfWork)UnitOfWork); // todo: move to clone   

                                // Add to Topic Group
                                topicGroup.Topics.Add(topic);
                            }

                        // Process Task Template Items that belong to the current Topic 
                        foreach (TemplateItem tiTask in
                            templateItemsUI.Where(i => i.Type == TemplateItem.TemplateItemType.Task
                                && i.SourceParentReferenceId == tiTopic.SourceReferenceId))
                        {
                                Task taskFramework =
                                    MContext.Task.Where(
                                    ta => !ta.Deleted.HasValue && ta.ReferenceId == tiTask.SourceReferenceId).FirstOrDefault();

                                Task task = (from ta in topic.Tasks
                                        where ta.Name == taskFramework.Name
                                        select ta).FirstOrDefault();

                                if (task == null)
                                {
                                    task = MContext.Task.Where(t => !t.Deleted.HasValue && t.ID == taskFramework.ID)
                                                .Include("Attachments.Data")
                                                .Include("VoiceMessages.Data")
                                                .Include("DocumentLibraries.Attachment")
                                                .FirstOrDefault()
                                                .DeepClone();

                                    //task.ReattachDocumentLibraries((UnitOfWork)UnitOfWork);  // todo: move to clone                              

                                    task.ParentTopic = null;
                                    task.ParentTopicId = topic.ID;

                                    // Add to Topic
                                    topic.Tasks.Add(task);
                                }
                        }
                    }
                }

                MContext.Entry(module).State = EntityState.Modified;
            }

            MContext.SaveChanges();
                
            #endregion
        }

        /// <summary>
        /// Saves the tempate
        /// </summary>
        /// <param name="templateId"></param>
        /// <param name="templateItemsUI"></param>
        /// <param name="moduleType"></param>
        private void SaveTemplate(Guid templateId, List<TemplateItem> templateItemsUI, Module.ModuleType moduleType)
        {
            var frameworkItems =
                GetItemsFromFramework(moduleType);

            TemplateItemService templateItemService =
                new TemplateItemService(MContext);

            var templateItemsDB =
                 templateItemService.Get(templateId, moduleType);

            foreach (TemplateItem templateItemUI in templateItemsUI.Where(x => x.Enabled))
            {
                var dbItem = (from t in templateItemsDB
                              where
                              t.SourceReferenceId == templateItemUI.SourceReferenceId &&
                              t.SourceParentReferenceId == templateItemUI.SourceParentReferenceId &&
                              t.Type == templateItemUI.Type
                              select t).FirstOrDefault();

                if (dbItem == null)
                {
                    TemplateItem templateItem = (from i in frameworkItems
                                                 where
                                                    i.SourceReferenceId == templateItemUI.SourceReferenceId &&
                                                       i.SourceParentReferenceId == templateItemUI.SourceParentReferenceId &&
                                                       i.Type == templateItemUI.Type
                                                 select i).FirstOrDefault();
                    templateItem.Enabled = true;

                    templateItemService.Add(templateId, templateItem, moduleType);
                }
            }

            templateItemsDB =
              templateItemService.Get(templateId, moduleType);

            List<TemplateItem> itemsToDelete = new List<TemplateItem>();

            foreach (TemplateItem templateItemDB in templateItemsDB)
            {
                // does db item exist in new ui list?
                var result = (from t in templateItemsUI
                              where
                            t.SourceReferenceId == templateItemDB.SourceReferenceId &&
                            t.SourceParentReferenceId == templateItemDB.SourceParentReferenceId &&
                            t.Type == templateItemDB.Type
                              select t).FirstOrDefault();

                if (result == null)
                    itemsToDelete.Add(templateItemDB);
            }

            foreach (TemplateItem item in itemsToDelete)
            {
                templateItemService.Delete(item);
            }
        }

        private IEnumerable<TemplateItem> GetItemsFromFramework(Module.ModuleType moduleType)
        {
            if (moduleType == Module.ModuleType.Project)
            {
                var projects = MContext.Project.Where(p => !p.Deleted.HasValue);

                var templateItems = projects.ToList().Select(p => new TemplateItem() {
                    Type = TemplateItem.TemplateItemType.Project,
                    Name = p.Name,
                    SourceReferenceId = p.ReferenceId
                });

                return templateItems.ToList();
            }


            // Get default template items
            var module = 
                MContext.Module.FirstOrDefault(m => !m.Deleted.HasValue && m.Type == moduleType && m.IsFrameworkDefault);

            if (module == null)
                throw new Exception(string.Format("No default Framework Items for Module with type '{0}'",
                    moduleType.ToString()));

            List<TemplateItem> templateItemsUI
            = new List<TemplateItem>();

            // Module 
            TemplateItem templateItemModule =
                new TemplateItem
                {
                    Type = TemplateItem.TemplateItemType.Module,
                    Name = module.Type.ToString(),
                    SourceReferenceId = module.ReferenceId
                };

            templateItemsUI.Add(templateItemModule);

            // Sort the Topic Groups
            module.TopicGroups = 
                SortTopicGroups(module.TopicGroups.AsQueryable()).ToList();

            // Topic Groups
            foreach (TopicGroup topicGroup in module.TopicGroups)
            {
                TemplateItem templateItemTopicGroup =
                    new TemplateItem
                    {
                        Type = TemplateItem.TemplateItemType.TopicGroup,
                        Name = topicGroup.Name,
                        SourceReferenceId = topicGroup.ReferenceId,
                        SourceParentReferenceId = module.ReferenceId,
                    };

                templateItemsUI.Add(templateItemTopicGroup);

                // Topics
                foreach (Topic topic in topicGroup.Topics)
                {
                    TemplateItem templateItemTopic =
                        new TemplateItem
                        {
                            Type = TemplateItem.TemplateItemType.Topic,
                            Name = topic.Name,
                            SourceReferenceId = topic.ReferenceId,
                            SourceParentReferenceId = topicGroup.ReferenceId,
                            ParentTopicType = topicGroup.TopicGroupType
                        };

                    templateItemsUI.Add(templateItemTopic);

                    // Tasks
                    foreach (Task task in topic.Tasks)
                    {
                        TemplateItem templateItemTask =
                            new TemplateItem
                            {
                                Type = TemplateItem.TemplateItemType.Task,
                                Name = task.Name,
                                SourceReferenceId = task.ReferenceId,
                                SourceParentReferenceId = topic.ReferenceId,
                                ParentTopicType = topicGroup.TopicGroupType,
                                ParentTopicName = topic.Name,
                                IsCore = task.IsCore,
                                Deleted = task.Deleted
                            };

                        templateItemsUI.Add(templateItemTask);
                    };
                }
            }

            return templateItemsUI;
        }

        /// <summary>
        /// Counts templates
        /// </summary>
        /// <param name="deleted"></param>
        /// <returns></returns>
        public int Count(bool deleted = false)
        {
            return GetAll(deleted).Count();
        }

        #endregion

        /// <summary>
        /// Gets Template Items
        /// </summary>
        /// <param name="templateId"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public IEnumerable<TemplateItem> GetTemplateItems(Guid templateId, Module.ModuleType type)
        {
            IEnumerable<TemplateItem> templateItems
                = new List<TemplateItem>();

            var template =
                MContext.Template.Find(templateId);

            switch (type)
            {
                case Module.ModuleType.Critical:
                    templateItems = template.TemplateItemsCritical;
                    break;
                case Module.ModuleType.Experiences:
                    templateItems = template.TemplateItemsExperiences;
                    break;
                case Module.ModuleType.Project:
                    templateItems = template.TemplateItemsProjects;
                    break;
                default:
                    throw new Exception(
                        string.Format("Unknown Module Type {0}", type.ToString()));
            }

            return templateItems;
        }


        public IEnumerable<TemplateItem> GetTemplateItemsFromAllModules(Guid templateId)
        {
            IEnumerable<TemplateItem> templateItems
                = new List<TemplateItem>();

            var template =
                MContext.Template.Find(templateId);

            templateItems =
                template.TemplateItemsCritical.Concat(template.TemplateItemsExperiences)
                    .Concat(template.TemplateItemsProjects);

            return templateItems;
        }


        /// <summary>
        /// Get all Templates
        /// </summary>
        /// <param name="deleted"></param>
        /// <returns></returns>
        public IEnumerable<Template> GetAll(bool deleted = false)
        {
            var templates =
                MContext.Template.Where(t => t.Deleted.HasValue == deleted).OrderBy(t => t.Name);

            return templates;
        }

        /// <summary>
        /// Updates Template header infomation
        /// </summary>
        /// <param name="templateId"></param>
        /// <param name="name"></param>
        /// <param name="durationDays"></param>
        /// <param name="notes"></param>
        public void UpdateTemplate(Guid templateId, string name, int durationDays, string notes)
        {
            var template = MContext.Template.Find(templateId);

            template.Name = name;
            template.DurationDays = durationDays;
            template.Notes = notes;

            MContext.SaveChanges();
        }

        /// <summary>
        /// Updates a Template
        /// </summary>
        /// <param name="template"></param>
        /// <returns></returns>
        public Template UpdateTemplate(Template template)
        {
            MContext.Entry(template).State = EntityState.Modified;
            MContext.SaveChanges();

            return template;
        }

        /// <summary>
        /// Copys Critical Template Items 
        /// from one template to another
        /// </summary>
        /// <param name="templateIdSource"></param>
        /// <param name="sourceReferenceIds"></param>
        /// <param name="templateIdDest"></param>
        public void TransferTemplateItemsCritical(Guid templateIdSource, IEnumerable<int> sourceReferenceIds, Guid templateIdDest)
        {
            var templateSource =
                MContext.Template.Find(templateIdSource);

            var templateDest =
                    MContext.Template.Find(templateIdDest);

            List<TemplateItem> itemsForTransfer =
                new List<TemplateItem>();

            foreach (int sourceReferenceId in sourceReferenceIds)
            {
                var templateItemForTransfer = (from t in templateSource.TemplateItemsCritical
                                               where t.SourceReferenceId == sourceReferenceId
                                               select t).FirstOrDefault();

                if (templateItemForTransfer != null && !templateDest.TemplateItemsExperiences.Any(t => t.SourceReferenceId == sourceReferenceId))
                    templateDest.TemplateItemsCritical.Add(templateItemForTransfer);
            }

            MContext.SaveChanges();
        }

        /// <summary>
        /// Copies Experiences Template items
        /// from one Template to another
        /// </summary>
        /// <param name="templateIdSource"></param>
        /// <param name="sourceReferenceIds"></param>
        /// <param name="templateIdDest"></param>
        public void TransferTemplateItemsExperiences(Guid templateIdSource, IEnumerable<int> sourceReferenceIds, Guid templateIdDest)
        {
            var templateSource =
                MContext.Template.Find(templateIdSource);

            var templateDest =
                    MContext.Template.Find(templateIdDest);

            List<TemplateItem> itemsForTransfer =
                new List<TemplateItem>();

            foreach (int sourceReferenceId in sourceReferenceIds)
            {
                var templateItemForTransfer = (from t in templateSource.TemplateItemsExperiences
                                               where t.SourceReferenceId == sourceReferenceId
                                               select t).FirstOrDefault();

                if (templateItemForTransfer != null && !templateDest.TemplateItemsExperiences.Any(t => t.SourceReferenceId == sourceReferenceId))
                {
                    templateDest.TemplateItemsExperiences.Add(templateItemForTransfer);
                }
            }

            MContext.SaveChanges();
        }

        /// <summary>
        /// Copys Project Template items
        /// from one Template to another
        /// </summary>
        /// <param name="templateIdSource"></param>
        /// <param name="sourceReferenceIds"></param>
        /// <param name="templateIdDest"></param>
        public void TransferTemplateItemsProjects(Guid templateIdSource, IEnumerable<int> sourceReferenceIds, Guid templateIdDest)
        {
            var templateSource =
                MContext.Template.Find(templateIdSource);

            var templateDest =
                    MContext.Template.Find(templateIdDest);

            List<TemplateItem> itemsForTransfer =
                new List<TemplateItem>();

            foreach (int sourceReferenceId in sourceReferenceIds)
            {
                var templateItemForTransfer = (from t in templateSource.TemplateItemsProjects
                                               where t.SourceReferenceId == sourceReferenceId
                                               select t).FirstOrDefault();

                if (templateItemForTransfer != null && !templateDest.TemplateItemsProjects.Any(t => t.SourceReferenceId == sourceReferenceId))
                {
                    templateDest.TemplateItemsProjects.Add(templateItemForTransfer);
                }
            }

            MContext.SaveChanges();
        }
      
        /// <summary>
        /// Retires a Template
        /// </summary>
        /// <param name="templateId"></param>
        public void Retire(Guid templateId)
        {
            UpdateDeletedFields(
                DateTime.Now, templateId);
        }

        /// <summary>
        /// Restores a Template
        /// </summary>
        /// <param name="templateId"></param>
        public void Restore(Guid templateId)
        {
            UpdateDeletedFields(
                null, templateId);
        }

        /// <summary>
        /// Set delete/restire template items
        /// </summary>
        /// <param name="deleted"></param>
        /// <param name="templateId"></param>
        private void UpdateDeletedFields(DateTime? deleted, Guid templateId)
        {
            var template =
                MContext.Template.Find(templateId);

            template.Deleted
                = deleted;

            foreach (TemplateItem templateItem in template.TemplateItemsCritical)
                templateItem.Deleted = deleted;

            foreach (TemplateItem templateItem in template.TemplateItemsExperiences)
                templateItem.Deleted = deleted;

            foreach (TemplateItem templateItem in template.TemplateItemsProjects)
                templateItem.Deleted = deleted;

            MContext.SaveChanges();
        }
    }
}
