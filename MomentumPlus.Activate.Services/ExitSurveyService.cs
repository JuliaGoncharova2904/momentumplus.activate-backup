﻿using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models;
using System.Data.Entity;

namespace MomentumPlus.Activate.Services
{
    public class ExitSurveyService : ServiceBase, IExitSurveyService
    {
        public ExitSurveyService(MomentumContext context) : base(context) { }

        public IEnumerable<ExitSurvey> GetAllExitSurvey()
        {
           return MContext.ExitSurvey.Where(e => e.Deleted.HasValue == false);
        }

        public ExitSurveyTemplate GetExitSurveyTemplateById(Guid exitSurveyTemplateId)
        {
            return MContext.ExitSurveyTemplate.FirstOrDefault(est => est.ID == exitSurveyTemplateId);
        }

        public ExitSurveyTemplate GetTemplateByExitSurvey(Guid exitSurveyId)
        {
            return MContext.ExitSurveyTemplate.FirstOrDefault(est => est.Deleted.HasValue == false && est.ExitSurveyId == exitSurveyId);
        }

        public ExitSurveyTemplate AddExitSurveyTemplate(ExitSurveyTemplate model)
        {
            MContext.ExitSurveyTemplate.Add(model);

            MContext.SaveChanges();

            return model;
        }

        public ExitSurvey GetExitSurveyByPackageId(Guid packageId)
        {
            ExitSurveyTemplate exitSurveyTemplate = MContext.ExitSurveyTemplate.FirstOrDefault(est => est.Packages.Any(p => p.ID == packageId));

            if (exitSurveyTemplate != null)
            {
                return MContext.ExitSurvey.Where(es => es.ID == exitSurveyTemplate.ExitSurveyId).FirstOrDefault();
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<ExitSurveyTemplate> GetAllExitSurveyTemplate()
        {
            return MContext.ExitSurveyTemplate.Where(est => est.Deleted.HasValue == false);
        }

        public ExitSurvey GetSurveyByID(Guid ID)
        {
            return MContext.ExitSurvey.FirstOrDefault(es => es.ID == ID);
        }

        public bool IsExistSurvey(Guid ID)
        {
            return (MContext.ExitSurvey.FirstOrDefault(es => es.ID == ID) != null) ? true : false;
        }

        public IEnumerable<ExitSurvey> GetAll(bool deleted = false)
        {
            return MContext.ExitSurvey.Where(es => es.Deleted.HasValue == deleted);
        }

        public IEnumerable<ExitSurveySection> GetSectionBySurveyId(Guid exitSurveyId)
        {
            return MContext.ExitSurveySection.Where(ess => ess.ExitSurveyId == exitSurveyId);
        }

        public List<ExitSurveyQuestion> GetQuestionBySection(Guid sectionId)
        {
            var section = MContext.ExitSurveySection.FirstOrDefault(e => e.ID == sectionId);

            return section.Questions.ToList();
        }

        public ExitSurveySection GetExitSurveySectionById(Guid sectionId)
        {
            return MContext.ExitSurveySection.FirstOrDefault(ess => ess.ID == sectionId);
        }

        public IEnumerable<ExitSurveyQuestion> GetAllQuestion(bool deleted = false)
        {
            return MContext.ExitSurveyQuestion.Where(esq => esq.Deleted.HasValue == deleted);
        }

        public ExitSurveyQuestion GetQuestionById(Guid Id)
        {
            return MContext.ExitSurveyQuestion.FirstOrDefault(q => q.ID == Id);
        }

        public ExitSurveyQuestion AddQuestion(ExitSurveyQuestion question)
        {
            question =
                 MContext.ExitSurveyQuestion.Add(question);

            MContext.SaveChanges();

            return question;
        }

        public ExitSurveySection AddSection(ExitSurveySection section)
        {
            section =
                 MContext.ExitSurveySection.Add(section);

            MContext.SaveChanges();

            return section;
        }

        public ExitSurvey AddExitSurvey(ExitSurvey exitSurvey)
        {
            exitSurvey =
                 MContext.ExitSurvey.Add(exitSurvey);

            MContext.SaveChanges();

            return exitSurvey;
        }

        public ExitSurvey UpdateExitSurvey(ExitSurvey exitSurvey)
        {
            MContext.Entry(exitSurvey).State = EntityState.Modified;

            MContext.SaveChanges();

            return exitSurvey;
        }

        public ExitSurveySection UpdateExitSurveySection(ExitSurveySection section)
        {
            MContext.Entry(section).State = EntityState.Modified;

            MContext.SaveChanges();

            return section;
        }

        public ExitSurveyTemplate UpdateExitSurveyTemplate(ExitSurveyTemplate template)
        {
            MContext.Entry(template).State = EntityState.Modified;

            MContext.SaveChanges();

            return template;
        }

        public ExitSurveyQuestion UpdateQuestion(ExitSurveyQuestion question)
        {
            ExitSurveyQuestion model = MContext.ExitSurveyQuestion.FirstOrDefault(q => q.ID == question.ID);
            model.ExitSurveyAnswers = question.ExitSurveyAnswers;

            MContext.Entry(model).State = EntityState.Modified;

            MContext.SaveChanges();

            return model;
        }

        public void DelQuestion(ExitSurveyQuestion question)
        {
            MContext.ExitSurveyQuestion.Remove(question);

            MContext.SaveChanges();

        }

        public void DelExitSurvey(ExitSurvey exitSurvey)
        {
            MContext.ExitSurvey.Remove(exitSurvey);

            MContext.SaveChanges();

        }

        public void DelExitSurveySection(ExitSurveySection section)
        {
            MContext.ExitSurveySection.Remove(section);

            MContext.SaveChanges();
        }

        public ExitSurveyAnswer AddExitSurveyAnswer(ExitSurveyAnswer answer)
        {
            answer =
                MContext.ExitSurveyAnswer.Add(answer);

            MContext.SaveChanges();
            return answer;
        }

        public ExitSurveyAnswer UpdateExitSurveyAnswer(ExitSurveyAnswer answer)
        {
            MContext.Entry(answer).State = EntityState.Modified;

            MContext.SaveChanges();

            return GetExitSurveyAnswer(answer.ID);
        }

        public ExitSurveyAnswer GetExitSurveyAnswer(Guid id)
        {
            return MContext.ExitSurveyAnswer.Where(a => a.ID == id).FirstOrDefault();
        }
    }
}
