﻿Services Project Structure
=====================
The following sections describe the key folders, files and namespaces that exist within the Services project and their function. The Services project is Class Library.

Service References
------------------
Contains references to Web Services used within the Services Project. 

Extensions
----------
Contains Extension Methods that are utilised by the Services

Helpers
-------
Contains helper methods used by the Services

Interfaces
----------
Contains the intefaces for the Services

Root Files
---------- 

- App.config - contains application settings
- NLog.config - logger configuration file 
 

