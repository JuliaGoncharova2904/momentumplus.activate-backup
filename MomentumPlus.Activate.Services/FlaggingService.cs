﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using MomentumPlus.Activate.Services.Helpers;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.People;
using MomentumPlus.Core.Models.Moderation;

namespace MomentumPlus.Activate.Services
{
    /// <summary>
    /// Flagging Service
    /// </summary>
    public class FlaggingService : ServiceBase, IFlaggingService
    {
        public const int DaysWhenRequiresAttention = 14;
        private MomentumContext context;

        public FlaggingService(MomentumContext context)
            : base(context)
        {
            this.context = context;
        }


        public PackageFlag GetCriticalModuleFlag(Guid packageId, bool isAdmin = false)
        {
            var topics = GetTopicsByPackageIdAndModuleType(packageId, Module.ModuleType.Critical);

            return new PackageFlag
            {
                ReadyForReviewCounter = isAdmin ? GetAllCount(topics) : GetReadyForReviewCount(topics),
                AllCounter = GetAllCount(topics),
                ApprovedCounter = isAdmin ? GetAllCount(topics) : GetAprovedCount(topics),
                Status = GetStatus(topics, isAdmin),
                ReadyForReview = HasReadyForReviewForTopics(topics),
                HasNewItems = HasNewItems(topics),
                Package = GetPackage(packageId),
                ExitSurvey = GetPackage(packageId).ExitSurvey.HasValue && GetPackage(packageId).ExitSurveyTemplateId.HasValue
            };
        }

        public PackageFlag GetPackageFlagForCriticalModule(Guid packageId, bool isAdmin = false)
        {
            var topicGroupService = new TopicGroupService(context);

            var topicGroups = topicGroupService.GetByPackageId(packageId).Where(tg => tg.ParentModule.Type == Module.ModuleType.Critical).ToList();

            foreach (var topicGroup in topicGroups)
            {
                topicGroup.Topics = topicGroup.Topics.ToList();
            }
            
            topicGroups.RemoveAll(tg => !tg.Topics.Any());

            var topicList = new List<Topic>();

            foreach (var topicGroup in topicGroups)
            {
                topicList.AddRange(topicGroup.Topics);
            }

            var topics = topicList.AsQueryable();

            return new PackageFlag
            {
                ReadyForReviewCounter = isAdmin ? GetAllCountForTopic(topics) : GetReadyForReviewCountForTopic(topics),
                AllCounter = GetAllCountForTopic(topics),
                ApprovedCounter = isAdmin ? GetAllCountForTopic(topics) : GetAprovedCountForTopic(topics),
                Status = GetStatusForTopic(topics, isAdmin),
                ReadyForReview = HasReadyForReview(topics),
                HasNewItems = HasNewItemsForTopics(topics),
                Package = GetPackage(packageId),
                ExitSurvey = false
            };
        }

        /// <summary>
        /// Get flag for the Critical Module in a Package
        /// </summary>
        /// <param name="packageId">The Package GUID</param>
        /// <returns>A <c>PackageFlag</c> object</returns>
        public PackageFlag GetExitSurveyModuleFlag(Guid packageId, bool isAdmin = false)
        {
            Package package = GetPackage(packageId);
            PackageFlag packageFlag = new PackageFlag();
            ExitSurveyService exitSurveyService = new ExitSurveyService(new MomentumContext());
            if (package.ExitSurveyTemplateId.HasValue)
            {
                IEnumerable<ExitSurveySection> exitSurveySectios =
                    exitSurveyService.GetSectionBySurveyId(package.ExitSurveyTemplate.ExitSurveyId)
                        .OrderBy(s => s.Number).Where(s => s.ID != null);
                int allCounter = 0;
                int readyForReview = 0;
                int approvedCounter = 0;
                foreach (var section in exitSurveySectios)
                {
                    allCounter += section.Questions.Count();
                    foreach (var item in section.Questions)
                    {
                        if (item.ExitSurveyAnswers.Any(
                            a => a.UserId == package.UserOwnerId && a.UserAnswerJSON != null &&
                                 a.IsHRManagerApproved.HasValue && a.IsHRManagerApproved.Value))
                        {
                            approvedCounter++;
                        }
                        if (item.ExitSurveyAnswers.Any(
                            a => a.UserId == package.UserOwnerId && a.UserAnswerJSON != null))
                        {
                            readyForReview++;
                        }
                    }
                }
                var status = "no-action";

                if (readyForReview == 0)
                {
                    status = "attention-required";
                }
                else if (readyForReview == allCounter)
                {
                    status = "all-readys-for-review";
                }
                else if (approvedCounter == allCounter)
                {
                    status = "approved";
                }
                if (isAdmin)
                {
                    status = "approved";
                }

                packageFlag.Status = status;
                packageFlag.AllCounter = allCounter;
                packageFlag.ApprovedCounter = approvedCounter;
                packageFlag.ReadyForReview = readyForReview == allCounter;
                packageFlag.Package = package;
                packageFlag.ReadyForReviewCounter = readyForReview;
                packageFlag.ExitSurvey = true;
                packageFlag.HasItems = package.ExitSurveyTemplateId.HasValue;
            }
            else
            {
                packageFlag.Status = "no-action";
                packageFlag.AllCounter = 0;
                packageFlag.ApprovedCounter = 0;
                packageFlag.ReadyForReview = false;
                packageFlag.Package = package;
                packageFlag.ReadyForReviewCounter = 0;
                packageFlag.ExitSurvey = false;
                packageFlag.HasItems = package.ExitSurveyTemplateId.HasValue;
            }
            return packageFlag;
        }

        /// <summary>
        /// Get flag for the Experiences Module in a Package
        /// </summary>
        /// <param name="packageId">The Package GUID</param>
        /// <returns>A <c>PackageFlag</c> object</returns>
        public PackageFlag GetExperiencesModuleFlag(Guid packageId, bool isAdmin = false)
        {
            var topics = GetTopicsByPackageIdAndModuleType(packageId, Module.ModuleType.Experiences);

            return new PackageFlag()
            {
                ReadyForReviewCounter = isAdmin ? GetAllCount(topics) : GetReadyForReviewCount(topics),
                AllCounter = GetAllCount(topics),
                ApprovedCounter = isAdmin ? GetAllCount(topics) : GetAprovedCount(topics),
                Status = GetStatus(topics, isAdmin),
                ReadyForReview = HasReadyForReview(topics),
                HasNewItems = HasNewItems(topics),
                Package = GetPackage(packageId),
                ExitSurvey = false
            };
        }

        /// <summary>
        /// Get flag for the Relationships Module in a Package
        /// </summary>
        /// <param name="packageId">The Package GUID</param>
        /// <returns>A <c>PackageFlag</c> object</returns>
        public PackageFlag GetRelationshipsModuleFlag(Guid packageId, bool isAdmin = false)
        {
            var contacts = GetContacts(packageId);

            return new PackageFlag()
            {
                ReadyForReviewCounter = isAdmin ? GetAllCount(contacts) : GetReadyForReviewCount(contacts),
                AllCounter = GetAllCount(contacts),
                ApprovedCounter = isAdmin ? GetAllCount(contacts) : GetAprovedCount(contacts),
                Status = GetStatus(contacts, isAdmin),
                ReadyForReview = HasReadyForReview(contacts),
                HasNewItems = HasNewItems(contacts),
                Package = GetPackage(packageId),
                ExitSurvey = false
            };
        }



        public PackageFlag GetOnboardingModuleFlag(Guid packageId, bool isAdmin = false)
        {
            var tasks = GetOnboardingTasks(packageId);

            var test = new PackageFlag()
            {
                ReadyForReviewCounter = isAdmin ? GetAllCount(tasks) : GetReadyForReviewCount(tasks),
                AllCounter = tasks.Count(),
                ApprovedCounter = isAdmin ? GetAllCount(tasks) : GetAprovedCount(tasks),
                Status = GetStatus(tasks, isAdmin),
                ReadyForReview = HasReadyForReview(tasks),
                HasNewItems = HasNewItems(tasks),
                Package = GetPackage(packageId),
                ExitSurvey = false
            };
            return test;
        }



        /// <summary>
        /// Get flag for the Tasks Module in a Package
        /// </summary>
        /// <param name="packageId">The Package GUID</param>
        /// <returns>A <c>PackageFlag</c> object</returns>
        public PackageFlag GetTasksModuleFlag(Guid packageId, bool isAdmin = false)
        {
            var tasks = GetTasks(packageId);
            var packageFlag = new PackageFlag()
            {
                ReadyForReview = HasReadyForReview(tasks),
                HasNewItems = HasNewItems(tasks),
                Package = GetPackage(packageId),
                ExitSurvey = false
            };

            int totalCount = 0;
            int totalReadyForReviewCounter = 0;
            int totalApprovedCounter = 0;
            var moduleTypes = new List<Module.ModuleType>()
            {
                Module.ModuleType.Critical, Module.ModuleType.Experiences
            };

            List<Task> totalTasks = new List<Task>();

            foreach (Module.ModuleType moduleType in moduleTypes)
            {
                //Other tasks
                var taskOtherItems = (from m in packageFlag.Package.Modules
                                   where m.Type == moduleType
                                   from tg in m.TopicGroups
                                   from to in tg.Topics
                                   from ta in to.Tasks
                                   where ta.IsCore == false
                                   select ta).AsQueryable();

                totalTasks.AddRange(taskOtherItems);
                totalReadyForReviewCounter += taskOtherItems.Where(t => t.ReadyForReview).Count();
                totalApprovedCounter += taskOtherItems.Where(t => t.IsApproved()).Count();
                totalCount += taskOtherItems.Count();
                //Core tasks
                var templateItemsTasks =
                new TemplateService(this.context).GetTemplateItems(packageFlag.Package.Position.TemplateId.Value, moduleType)
                    .Where(t => t.Type == TemplateItem.TemplateItemType.Task && t.Enabled && t.IsCore);
                var taskCoreItems = (from templateItemsTask in templateItemsTasks
                                 select
                                     new TaskService(this.context, User).GetAllByPackageId(packageFlag.Package.ID)
                                         .FirstOrDefault(tg => tg.Name.Equals(templateItemsTask.Name)) into task
                                 where task != null
                                 select task).ToList();
                totalTasks.AddRange(taskCoreItems);
                totalReadyForReviewCounter += taskCoreItems.Where(t => t.ReadyForReview).Count();
                totalApprovedCounter += taskCoreItems.Where(t => t.IsApproved()).Count();
                totalCount += taskCoreItems.Count();
            }
            var relationshipsTasks = (from mo in packageFlag.Package.Modules
                                      from tg in mo.TopicGroups
                                      where tg.TopicGroupCategory == TopicGroup.Category.Other
                                      from c in packageFlag.Package.Contacts
                                      from me in c.Tasks
                                      select me).MyDistinct(t => t.ID).AsQueryable();
            totalTasks.AddRange(relationshipsTasks);
            totalReadyForReviewCounter += relationshipsTasks.Where(t => t.ReadyForReview).Count();
            totalApprovedCounter += relationshipsTasks.Where(t => t.IsApproved()).Count();
            totalCount += relationshipsTasks.Count();

            var projectOtherTasks = new TaskService(this.context, User).GetAllByPackageId(packageId).AsQueryable()
                .Include(t => t.ProjectInstace.Project).Where(t => t.ProjectInstanceId != null).ToList()
                .Select(p => p).AsQueryable();
            totalTasks.AddRange(projectOtherTasks);
            totalReadyForReviewCounter += projectOtherTasks.Where(t => t.ReadyForReview).Count();
            totalApprovedCounter += projectOtherTasks.Where(t => t.IsApproved()).Count();
            totalCount += projectOtherTasks.Count();

            packageFlag.Status = GetStatus(totalTasks.AsQueryable(), isAdmin);
            packageFlag.AllCounter = totalCount;
            packageFlag.ReadyForReviewCounter = totalReadyForReviewCounter;
            packageFlag.ApprovedCounter = isAdmin ? totalCount : totalApprovedCounter;

            return packageFlag;
        }

        /// <summary>
        /// Get flag for the Projects Module in a Package
        /// </summary>
        /// <param name="packageId">The Package GUID</param>
        /// <returns>A <c>PackageFlag</c> object</returns>
        public PackageFlag GetProjectsModuleFlag(Guid packageId, bool isAdmin = false)
        {
            var projects = GetProjectInstances(packageId);

            return new PackageFlag()
            {
                ReadyForReviewCounter = isAdmin ? GetAllCount(projects) : GetReadyForReviewCount(projects),
                AllCounter = GetAllCount(projects),
                ApprovedCounter = isAdmin ? GetAllCount(projects) : GetAprovedCount(projects),
                Status = GetStatus(projects, isAdmin),
                ReadyForReview = HasReadyForReview(projects),
                HasNewItems = HasNewItems(projects),
                Package = GetPackage(packageId),
                ExitSurvey = false
            };
        }

        /// <summary>
        /// Get flag for a Project Instance
        /// </summary>
        /// <param name="projectInstanceId">The Project Instance GUID</param>
        /// <returns>A <c>PackageFlag</c> object</returns>
        public PackageFlag GetProjectModuleFlag(Guid projectInstanceId, bool isAdmin = false)
        {
            var projects = GetProjectInstance(projectInstanceId);

            return new PackageFlag()
            {
                ReadyForReviewCounter = isAdmin ? GetAllCount(projects) : GetReadyForReviewCount(projects),
                AllCounter = GetAllCount(projects),
                ApprovedCounter = isAdmin ? GetAllCount(projects) : GetAprovedCount(projects),
                Status = GetStatus(projects, isAdmin),
                ReadyForReview = HasReadyForReview(projects),
                HasNewItems = HasNewItems(projects)
            };
        }

        /// <summary>
        /// Get flag for the People Module in a Package
        /// </summary>
        /// <param name="packageId">The Package GUID</param>
        /// <returns>A <c>PackageFlag</c> object</returns>
        public PackageFlag GetPeopleModuleFlag(Guid packageId, bool isAdmin = false)
        {
            //Package package = GetPackage(packageId);
            //PackageService packageService = new PackageService(MContext, null, User);

            //var userPackageIds = packageService.GetAllPackagesByUser(package.UserOwner).Select(p => p.ID);

            var staffs = GetStaffs(packageId).Where(c => c.PackageId != packageId);

            //List<Contact> contacts = new List<Contact>();
            //foreach (var contact in staffs)
            //{
            //    if (!userPackageIds.Any(i => i == contact.PackageId))
            //    {
            //        contacts.Add(contact);
            //    }
            //}

            //staffs = contacts.AsQueryable();

            //var moderatedStaffs = staffs.Select(s => new ModeratedStaff()
            //{
            //    Moderations = s.Moderations,
            //    ReadyForReview = s.ReadyForReview
            //});

            return new PackageFlag()
            {
                ReadyForReviewCounter = isAdmin ? GetAllCount(staffs) : GetReadyForReviewCount(staffs),
                AllCounter = GetAllCount(staffs),
                ApprovedCounter = isAdmin ? GetAllCount(staffs) : GetAprovedCount(staffs),
                Status = GetStatus(staffs, isAdmin),
                ReadyForReview = HasReadyForReview(staffs),
                HasNewItems = HasNewItems(staffs),
                Package = GetPackage(packageId),
                ExitSurvey = false
            };
        }

        /// <summary>
        /// Get flag for a Topic Group
        /// </summary>
        /// <param name="topicGroupId">The Topic Group GUID</param>
        /// <returns>A <c>PackageFlag</c> object</returns>
        public PackageFlag GetTopicGroupFlag(Guid topicGroupId)
        {
            var topics = GetTopicsByTopicGroupId(topicGroupId);

            return new PackageFlag()
            {
                ReadyForReviewCounter = GetReadyForReviewCount(topics),
                AllCounter = GetAllCount(topics),
                ApprovedCounter = GetAprovedCount(topics),
                Status = GetStatus(topics),
                ReadyForReview = HasReadyForReview(topics),
                HasNewItems = HasNewItems(topics)
            };
        }

        /// <summary>
        /// Get flag for a Topic Group
        /// </summary>
        /// <param name="topicGroup">The Topic Group object</param>
        /// <returns>A <c>PackageFlag</c> object</returns>
        public PackageFlag GetTopicGroupFlag(TopicGroup topicGroup)
        {
            PackageFlag packageFlag = GetTopicGroupFlag(topicGroup.ID);
            packageFlag.HasItems = topicGroup.Topics.Any();
            return packageFlag;
        }

        /// <summary>
        /// Get flag for a Topic
        /// </summary>
        /// <param name="topicId">The Topic GUID</param>
        /// <returns>A <c>PackageFlag</c> object</returns>
        public PackageFlag GetTopicFlag(Guid topicId)
        {
            var topic = MContext.Topic.Find(topicId);

            return GetTopicFlag(topic);
        }

        /// <summary>
        /// Get flag for a Topic
        /// </summary>
        /// <param name="topic">The Topic object</param>
        /// <returns>A <c>PackageFlag</c> object</returns>
        public PackageFlag GetTopicFlag(Topic topic)
        {
            var topics = new[] { topic }.AsQueryable();

            return new PackageFlag()
            {
                ReadyForReviewCounter = GetReadyForReviewCount(topics),
                AllCounter = GetAllCount(topics),
                ApprovedCounter = GetAprovedCount(topics),
                DeclinedCounter = GetDeclinedCount(topics),
                Status = GetStatus(topics),
                ReadyForReview = HasReadyForReview(topics),
                HasNewItems = HasNewItems(topics)
            };
        }

        /// <summary>
        /// Get flag for a Task
        /// </summary>
        /// <param name="taskId">The Task GUID</param>
        /// <returns>A <c>PackageFlag</c> object</returns>
        public PackageFlag GetTaskFlag(Guid taskId, Guid? packageId)
        {
            var task = MContext.Task.Find(taskId);

            return GetTaskFlag(task, packageId);
        }

        /// <summary>
        /// Get flag for a Task
        /// </summary>
        /// <param name="task">The Task object</param>
        /// <returns>A <c>PackageFlag</c> object</returns>
        public PackageFlag GetTaskFlag(Task task, Guid? packageId)
        {
            var tasks = new[] { task }.AsQueryable();

            var pack = new PackageFlag();

            pack.ReadyForReviewCounter = GetReadyForReviewCount(tasks);
            pack.AllCounter = GetAllCount(GetTasks(task.ParentTopic != null 
                ? task.ParentTopic.ParentTopicGroup.ParentModule.Package.ID
                : packageId.Value));
            pack.ApprovedCounter = GetAprovedCount(tasks);
            pack.DeclinedCounter = GetDeclinedCount(tasks);
            pack.Status = GetStatus(tasks);
            pack.ReadyForReview = HasReadyForReview(tasks);
            pack.HasNewItems = HasNewItems(tasks);

            return pack;
        }

        /// <summary>
        /// Get the active Contacts in a package - not cached in the database context
        /// </summary>
        /// <param name="packageId">The Package GUID</param>
        /// <returns></returns>
        private IQueryable<Contact> GetContacts(Guid packageId)
        {
            return MContext.Contact.AsNoTracking()
                    .Where(c => !c.Deleted.HasValue
                        && c.Package.ID == packageId);
        }

        private IQueryable<Contact> GetStaffs(Guid packageId)
        {
            var contactService = new ContactService(MContext);

            var moderationService = new ModerationService(MContext);


            var returnList = contactService.GetTeamContacts(packageId, Person.SortColumn.FirstName).ToList();

            foreach (var contact in returnList)
            {
                if (contact.IsBasedOnPackage)
                {
                    contact.ReadyForReview = true;

                }
            }


            return returnList.AsQueryable();

        }

        /// <summary>
        /// Get the active Project Instances in a package - not cached in the database context
        /// </summary>
        /// <param name="packageId">The Package GUID</param>
        /// <returns></returns>
        private IQueryable<ProjectInstance> GetProjectInstances(Guid packageId)
        {
            return MContext.ProjectInstance.AsNoTracking()
                    .Where(p => !p.Deleted.HasValue
                        && p.PackageId == packageId);
        }

        /// <summary>
        /// Get an active Project Instance - not cached in the database context
        /// </summary>
        /// <param name="projectInstanceId">The Project Instance GUID</param>
        /// <returns></returns>
        private IQueryable<ProjectInstance> GetProjectInstance(Guid projectInstanceId)
        {
            return MContext.ProjectInstance.AsNoTracking()
                    .Where(p => !p.Deleted.HasValue
                        && p.ID == projectInstanceId);
        }

        /// <summary>
        /// Get the Topics in a Module in a Package - not cached in the database context
        /// </summary>
        /// <param name="packageId">The Package GUID</param>
        /// <param name="moduleType">The Module type</param>
        /// <returns></returns>
        private IQueryable<Topic> GetTopicsByPackageIdAndModuleType(Guid packageId, Module.ModuleType moduleType)
        {

            var moduleService = new ModuleService(MContext);

            var topicGroups = moduleService.GetTemplateGroupsByModyleType(moduleType, packageId);

            var topics = topicGroups.SelectMany(tg => tg.Topics).Where(t => !t.Deleted.HasValue);

            return topics.AsQueryable();

        }

        /// <summary>
        /// Get the Topics in a Topic Group - not cached in the database context
        /// </summary>
        /// <param name="topicGroupId">The Topic Group GUID</param>
        /// <returns></returns>
        private IQueryable<Topic> GetTopicsByTopicGroupId(Guid topicGroupId)
        {
            return MContext.Topic.AsNoTracking()
                    .Where(t => !t.Deleted.HasValue
                        && t.ParentTopicGroupId == topicGroupId);
        }


        private Package GetPackage(Guid packageId)
        {
            return MContext.Package.FirstOrDefault(p => p.ID == packageId);
        }


        /// <summary>
        /// Get the Tasks in a Package - not cached in the database context
        /// </summary>
        /// <param name="packageId">The Package GUID</param>
        /// <returns></returns>
        private IQueryable<Task> GetTasks(Guid packageId)
        {

            IQueryable<Task> allCoreUserTasks;

            IEnumerable<TemplateItem> templateItemsTasks = new List<TemplateItem>();

            var templateService = new TemplateService(MContext);


            var package = MContext.Package.FirstOrDefault(p => p.ID == packageId);

            if (package != null)
            {
                templateItemsTasks = templateService.GetTemplateItemsFromAllModules(package.Position.TemplateId.Value).Where(t => t.Type == TemplateItem.TemplateItemType.Task && t.Enabled);
            }

            allCoreUserTasks = MContext.Task.AsNoTracking()
                .Where(t => !t.Deleted.HasValue
                    && t.ParentTopic.ParentTopicGroup.ParentModule.Package.ID == packageId);


            var userCoreTasksBasedOnTemplate = from templateItemsTask in templateItemsTasks
                                               select allCoreUserTasks.FirstOrDefault(tg => tg.Name.Equals(templateItemsTask.Name)) into task
                                               where task != null
                                               select task;


            var otherTasks = from m in package.Modules
                             from tg in m.TopicGroups
                             from to in tg.Topics
                             from ta in to.Tasks
                             where ta.IsCore
                             select ta;

            var userOtherTaskssBasedOnTemplate = from templateItemsTask in templateItemsTasks
                                                 select otherTasks.FirstOrDefault(tg => tg.Name.Equals(templateItemsTask.Name)) into task
                                                 where task != null
                                                 select task;


            return userCoreTasksBasedOnTemplate.Concat(userOtherTaskssBasedOnTemplate).AsQueryable();

        }

        private IQueryable<Task> GetOnboardingTasks(Guid packageId, bool approvedOnly = true)
        {

            var package = MContext.Package.FirstOrDefault(p => p.ID == packageId);

            var ancestorPackageId = package.AncestorPackageId;

            var tasks = MContext.Task.Where(t => (t.ParentTopic.ParentTopicGroup.ParentModule.Package.ID == ancestorPackageId
                                                  || t.ProjectInstace.PackageId == ancestorPackageId
                                                  || t.Contact.PackageId == ancestorPackageId && !t.Deleted.HasValue) &&
                                                 (!approvedOnly ||
                                                  t.Moderations.Any(m => m.Decision == ModerationDecision.Approved && !t.Deleted.HasValue)));

            return tasks.AsQueryable();
        }



        /// <summary>
        /// Returns true if any of the given entities are overdue
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        private static bool HasOverdue(IQueryable<Moderated> entities)
        {
            if (entities.Count() == 1)
                return HasOverdue(entities.First());

            var now = SystemTime.Now;

            foreach (var entitie in entities)
            {
                if (entitie.ReviewBy != null)
                {
                    var datePatern = entitie.ReviewBy.Value.AddDays(-DaysWhenRequiresAttention);

                    if (!entitie.UserLastViewed.HasValue || entitie.UserLastViewed < datePatern && entitie.ReviewBy <= now)
                    {
                        return true;
                    }

                    return false;

                }
            }

            return false;
        }

        private static bool HasOverdueForTopic(IQueryable<Topic> entities)
        {
            if (entities.Count() == 1)
                return HasOverdue(entities.First());

            var now = SystemTime.Now;

            foreach (var entitie in entities)
            {
                if (entitie.ReviewBy != null)
                {
                    var datePatern = entitie.ReviewBy.Value.AddDays(-DaysWhenRequiresAttention);

                    if (!entitie.UserLastViewed.HasValue || entitie.UserLastViewed < datePatern && entitie.ReviewBy <= now)
                    {
                        return true;
                    }

                    return false;

                }
            }

            return false;
        }

        /// <summary>
        /// Returns true if the given entity is overdue
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        private static bool HasOverdue(Moderated entity)
        {
            var now = SystemTime.Now;
            return entity.ReviewBy.HasValue && (!entity.UserLastViewed.HasValue || entity.UserLastViewed < entity.ReviewBy.Value.AddDays(-DaysWhenRequiresAttention)) && entity.ReviewBy <= now;
        }

        /// <summary>
        /// Returns true if any of the given entities is declined
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        private static bool HasDeclined(IQueryable<Moderated> entities)
        {
            return entities.Any(Moderated.Declined());
        }

        private static bool HasDeclinedForTopic(IQueryable<Topic> entities)
        {
            return entities.Any(t => t.IsDeclined());
        }

        /// <summary>
        /// Returns true if any of the given entities require an update
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        private static bool HasUpdateRequired(IQueryable<Moderated> entities)
        {
            if (entities.Count() == 1)
                return HasUpdateRequired(entities.First());

            var now = SystemTime.Now;
            //return entities.Any(e => (!e.UserLastViewed.HasValue || e.UserLastViewed < DbFunctions.AddDays(e.ReviewBy, -DaysWhenRequiresAttention)) && e.ReviewBy < DbFunctions.AddDays(now, DaysWhenRequiresAttention));

            foreach (var entitie in entities)
            {
                if (entitie.ReviewBy != null)
                {
                    var datePatern1 = entitie.ReviewBy.Value.AddDays(-DaysWhenRequiresAttention);

                    var datePatern2 = now.AddDays(DaysWhenRequiresAttention);

                    if (!entitie.UserLastViewed.HasValue || entitie.UserLastViewed < datePatern1 && entitie.ReviewBy < datePatern2)
                    {
                        return true;
                    }

                    return false;
                }
            }

            return false;
        }

        private static bool HasUpdateRequiredForTopic(IQueryable<Topic> entities)
        {
            if (entities.Count() == 1)
                return HasUpdateRequired(entities.First());

            var now = SystemTime.Now;
            
            foreach (var entitie in entities)
            {
                if (entitie.ReviewBy != null)
                {
                    var datePatern1 = entitie.ReviewBy.Value.AddDays(-DaysWhenRequiresAttention);

                    var datePatern2 = now.AddDays(DaysWhenRequiresAttention);

                    if (!entitie.UserLastViewed.HasValue || entitie.UserLastViewed < datePatern1 && entitie.ReviewBy < datePatern2)
                    {
                        return true;
                    }

                    return false;
                }
            }

            return false;
        }

        /// <summary>
        /// Returns true if the given entity requires an update
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        private static bool HasUpdateRequired(Moderated entity)
        {
            var now = SystemTime.Now;
            return entity.ReviewBy.HasValue && (!entity.UserLastViewed.HasValue || entity.UserLastViewed < entity.ReviewBy.Value.AddDays(-DaysWhenRequiresAttention)) && entity.ReviewBy < now.AddDays(DaysWhenRequiresAttention);
        }

        /// <summary>
        /// Returns true if all of the given entities have been approved
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        private static bool AllApproved(IQueryable<Moderated> entities)
        {
            return entities.Any()
                && entities.All(Moderated.Approved());
        }

        private static bool AllApprovedForTopic(IQueryable<Topic> entities)
        {
            return entities.Any()
                   && entities.All(t => t.IsApproved());
        }

        private static bool AllReadyForReview(IQueryable<Reviewable> entities)
        {
            return entities.Count() == entities.Count(e => e.ReadyForReview);

        }

        private static bool AllReadyForReviewForTopic(IQueryable<Topic> entities)
        {
            return entities.Count() == entities.Count(e => e.ReadyForReview);

        }

        /// <summary>
        /// Returns true if any of the given entities are ready for review
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        private static bool HasReadyForReview(IQueryable<Reviewable> entities)
        {
            return entities.Any(e => e.ReadyForReview);
        }

        private static bool HasReadyForReviewForTopics(IQueryable<Topic> entities)
        {
            return entities.Any(e => e.ReadyForReview);
        }

        private static bool HasNewItems(IQueryable<Reviewable> entities)
        {
            var dateTemplate = SystemTime.Now.AddDays(-2).AddHours(-12);

            return entities.Any(e => e.Created >= dateTemplate);
        }

        private static bool HasNewItemsForTopics(IQueryable<Topic> entities)
        {
            var dateTemplate = SystemTime.Now.AddDays(-2).AddHours(-12);

            return entities.Any(e => e.Created >= dateTemplate);
        }

        private static string GetStatus(IQueryable<Moderated> entities)
        {
            var status = "no-action";

            if (HasDeclined(entities))
            {
                status = "attention-required";
            }
            else if (AllApproved(entities))
            {
                status = "approved";
            }
            else if (HasOverdue(entities))
            {
                status = "overdue";
            }
            else if (HasUpdateRequired(entities))
            {
                status = "update-required";
            }

            return status;
        }

        /// <summary>
        /// Gets the status of the given collection of entities
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        private static string GetStatus(IQueryable<Moderated> entities, bool isAdmin = false)
        {
            var status = "no-action";

            if (HasDeclined(entities))
            {
                status = "attention-required";
            }
            else if (AllReadyForReview((IQueryable<Reviewable>)entities) && entities.Any())
            {
                status = "all-readys-for-review";
            }

            else if (HasUpdateRequired(entities))
            {
                status = "update-required";
            }

            else if (AllApproved(entities))
            {
                status = "approved";
            }
            else if (HasOverdue(entities))
            {
                status = "overdue";
            }

            else if (!entities.Any())
            {
                status = "approved";
            }

            if (isAdmin)
            {
                status = "approved";
            }

            return status;
        }

        private static string GetStatusForTopic(IQueryable<Topic> entities, bool isAdmin = false)
        {
            var status = "no-action";

            if (HasDeclinedForTopic(entities))
            {
                status = "attention-required";
            }
            else if (AllReadyForReviewForTopic(entities) && entities.Any())
            {
                status = "all-readys-for-review";
            }

            else if (HasUpdateRequiredForTopic(entities))
            {
                status = "update-required";
            }

            else if (AllApprovedForTopic(entities))
            {
                status = "approved";
            }
            else if (HasOverdueForTopic(entities))
            {
                status = "overdue";
            }

            else if (!entities.Any())
            {
                status = "approved";
            }

            if (isAdmin)
            {
                status = "approved";
            }

            return status;
        }

        private static int GetAprovedCount(IQueryable<Moderated> entities)
        {
            int counter = 0;

            foreach (var entitie in entities)
            {
                if (entitie.IsApproved())
                {
                    counter++;
                }
            }
            return counter;
        }

        private static int GetAprovedCountForTopic(IQueryable<Topic> entities)
        {
            int counter = 0;

            foreach (var entitie in entities)
            {
                if (entitie.IsApproved())
                {
                    counter++;
                }
            }
            return counter;
        }

        private static int GetDeclinedCount(IQueryable<Moderated> entities)
        {
            int counter = 0;

            foreach (var entitie in entities)
            {
                if (entitie.IsDeclined())
                {
                    counter++;
                }
            }
            return counter;
        }

        private static int GetAllCount(IQueryable<Moderated> entities)
        {
            return entities.Count(e => !e.Deleted.HasValue);
        }

        private static int GetAllCountForTopic(IQueryable<Topic> entities)
        {
            return entities.Count(e => !e.Deleted.HasValue);
        }

        private static int GetReadyForReviewCount(IQueryable<Reviewable> entities)
        {
            return entities.Count(e => e.ReadyForReview);
        }

        private static int GetReadyForReviewCountForTopic(IQueryable<Topic> entities)
        {
            return entities.Count(e => e.ReadyForReview);
        }

        /// <summary>
        /// Get the flag for a Reviewable entity
        /// </summary>
        /// <param name="moderated"></param>
        /// <returns></returns>
        public PackageFlag GetFlag(Reviewable moderated)
        {
            var mod = new[] { moderated }.AsQueryable();

            return new PackageFlag()
            {
                ReadyForReviewCounter = GetReadyForReviewCount(mod),
                AllCounter = GetAllCount(mod),
                ApprovedCounter = GetAprovedCount(mod),
                Status = GetStatus(mod),
                ReadyForReview = HasReadyForReview(mod)
            };
        }
    }
}
