﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using MomentumPlus.Activate.Services.Extensions;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models.People;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.Moderation;

namespace MomentumPlus.Activate.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class TaskService : ServiceBase, ITaskService
    {
        public TaskService(MomentumContext context, User user) : base(context, user) { }

        #region ITaskService Members

        /// <summary>
        /// Adds a Task
        /// </summary>
        /// <param name="task"></param>
        public void Add(Task task)
        {
            MContext.Task.Add(task);

            MContext.SaveChanges();
        }

        /// <summary>
        /// Gets all Tasks by Topic Id
        /// </summary>
        /// <param name="topicId"></param>
        /// <param name="pageNo"></param>
        /// <param name="pageSize"></param>
        /// <param name="readyForReview"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortAscending"></param>
        /// <param name="deleted"></param>
        /// <returns></returns>
        public IEnumerable<Task> GetAllByTopicId(Guid topicId, int pageNo, int pageSize, bool? readyForReview, Task.SortColumn sortColumn, bool sortAscending, bool deleted = false)
        {
            var topic = MContext.Topic.Include(t => t.Tasks).FirstOrDefault(tp => tp.ID == topicId);

            var tasks = topic.Tasks.AsQueryable();

            if (readyForReview.HasValue)
            {
                if (readyForReview.Value)
                    tasks = from t in tasks where t.ReadyForReview == true select t;
                else
                    tasks = from t in tasks where t.ReadyForReview == false select t;
            }

            tasks = from t in tasks where t.Deleted.HasValue == deleted select t;

            SortAndPage<Task>(pageNo, pageSize, sortColumn.ToString(), sortAscending, ref tasks);

            return tasks.ToList();
        }

        /// <summary>
        /// Gets all Tasks by Topic Id
        /// </summary>
        /// <param name="topicId"></param>
        /// <returns></returns>
        public IEnumerable<Task> GetAllByTopicId(Guid topicId)
        {
            var tasks = MContext.Task.Include(t => t.ParentTopic).Where(t => t.ParentTopicId == topicId);

            return tasks.ToList();
        }

        /// <summary>
        /// Get all by Project Instance Id
        /// </summary>
        /// <param name="projectIntanceId"></param>
        /// <returns></returns>
        public IEnumerable<Task> GetAllByProjectInstanceId(Guid projectIntanceId)
        {
            var tasks = MContext.Task.Include(t => t.ProjectInstace).Where(t => t.ProjectInstanceId == projectIntanceId);

            return tasks.ToList();
        }
        
        /// <summary>
        /// Get tasks by Contact Id
        /// </summary>
        /// <param name="contactId"></param>
        /// <returns></returns>
        public IEnumerable<Task> GetAllByContactId(Guid contactId)
        {
            var tasks = MContext.Task.Include(t => t.Contact).Where(t => t.ContactId == contactId);

            return tasks.ToList();
        }

        /// <summary>
        /// Count tasks by Topic Id
        /// </summary>
        /// <param name="topicId"></param>
        /// <param name="deleted"></param>
        /// <returns></returns>
        public int CountByTopicId(Guid topicId, bool deleted = false)
        {
            int count = MContext.Task.Count(t => t.ParentTopicId == topicId && t.Deleted.HasValue == deleted);

            return count;
        }

        /// <summary>
        /// Get task by Task Id
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public Task GetByTaskId(Guid taskId)
        {
            Task task = MContext.Task.Find(taskId);

            return task;
        }

        /// <summary>
        /// Update a Task
        /// </summary>
        /// <param name="task"></param>
        /// <returns></returns>
        public Task Update(Task task)
        {
            MContext.Entry(task).State = EntityState.Modified;

            MContext.SaveChanges();

            return task;
        }

        /// <summary>
        /// Search for a Task
        /// </summary>
        /// <param name="searchString"></param>
        /// <param name="packageId"></param>
        /// <returns></returns>
        public IEnumerable<Task> Search(string searchString, Guid packageId)
        {
            RestrictedAccess restrictedAccess =
                new RestrictedAccess(MContext, packageId);

            IEnumerable<Task> searchResults = (from t in restrictedAccess.Tasks
                                               where t.Name.Contains(searchString) ||
                                               t.SearchTags.Contains(searchString)
                                               select t);

            return searchResults.ToList();
        }

        /// <summary>
        /// Search for Task Attachements
        /// </summary>
        /// <param name="searchString"></param>
        /// <param name="packageId"></param>
        /// <returns></returns>
        public IEnumerable<Attachment> SearchAttachments(string searchString, Guid packageId)
        {
            RestrictedAccess restrictedAccess =
                new RestrictedAccess(MContext, packageId);

            IQueryable<Attachment> searchResults = (from a in restrictedAccess.TaskAttachments
                                                    where a.Name.Contains(searchString) ||
                                                    a.SearchTags.Contains(searchString)
                                                    select a);
            return searchResults.ToList();
        }

        /// <summary>
        /// Search for Task Voice Messages
        /// </summary>
        /// <param name="searchString"></param>
        /// <param name="packageId"></param>
        /// <returns></returns>
        public IEnumerable<VoiceMessage> SearchVoiceMessages(string searchString, Guid packageId)
        {
            RestrictedAccess restrictedAccess =
                new RestrictedAccess(MContext, packageId);

            IQueryable<VoiceMessage> searchResults = (from v in restrictedAccess.TaskVoiceMessages
                                                      where v.Name.Contains(searchString) ||
                                                      v.SearchTags.Contains(searchString)
                                                      select v);
            return searchResults.ToList();
        }

        /// <summary>
        /// Clone Task
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public Task Clone(Guid taskId)
        {
            var taskSource =
                MContext.Task
                    .Include("Attachments.Data")
                    .Include("VoiceMessages.Data")
                    .Include("DocumentLibraries.Attachment")
                    .FirstOrDefault(t => t.ID == taskId);

            return Clone(taskSource);
        }

        /// <summary>
        /// Clone Task
        /// </summary>
        /// <param name="taskSource"></param>
        /// <returns></returns>
        private Task Clone(Task taskSource)
        {
            var taskClone = taskSource.DeepClone();

            return taskClone;
        }

        /// <summary>
        /// Get Tasks by Package Id
        /// </summary>
        /// <param name="packageId"></param>
        /// <param name="approvedOnly"></param>
        /// <returns></returns>
        public IEnumerable<Task> GetAllByPackageId(Guid packageId, bool approvedOnly = false)
        {
            var tasks = MContext.Task.Where(t => (t.ParentTopic.ParentTopicGroup.ParentModule.Package.ID == packageId
                                                || t.ProjectInstace.PackageId == packageId
                                                || t.Contact.PackageId == packageId) &&
                                                (!approvedOnly || t.Moderations.Any(m => m.Decision == ModerationDecision.Approved)))
                .Include(t => t.Moderations)
                .OrderBy(t => t.WeekDue)
                .ThenByDescending(t => t.Priority).ToList();

            return tasks;
        }

        #endregion

        /// <summary>
        /// Retire Tasks by Topic Id
        /// </summary>
        /// <param name="topicId"></param>
        public void Retire(Guid topicId)
        {
            UpdateDeletedFields(
                DateTime.Now, topicId);
        }

        /// <summary>
        /// Restore Tasks by Topic Id
        /// </summary>
        /// <param name="topicId"></param>
        public void Restore(Guid topicId)
        {
            UpdateDeletedFields(
                null, topicId);
        }

        /// <summary>
        /// Retire/restore Tasks
        /// </summary>
        /// <param name="deleted"></param>
        /// <param name="taskId"></param>
        private void UpdateDeletedFields(DateTime? deleted, Guid taskId)
        {
            var task = MContext.Task
                            .Include(t => t.Attachments.Select(a => a.Data))
                            .Include(t => t.VoiceMessages.Select(v => v.Data))
                            .Include(t => t.DocumentLibraries.Select(d => d.Attachment.Data))
                            .FirstOrDefault(t => t.ID == taskId);

            task.Deleted = deleted;

            foreach (Attachment attachment in task.Attachments)
            {
                attachment.Deleted = deleted;
                if (attachment.Data != null)
                    attachment.Data.Deleted = deleted;
            }

            foreach (VoiceMessage voiceMessage in task.VoiceMessages)
            {
                voiceMessage.Deleted = deleted;
                if (voiceMessage.Data != null)
                    voiceMessage.Data.Deleted = deleted;
            }

            foreach (DocumentLibrary documentLibraryItem in task.DocumentLibraries)
                documentLibraryItem.Deleted = deleted;

            MContext.SaveChanges();
        }

        /// <summary>
        /// Get core tasks
        /// </summary>
        /// <param name="id"></param>
        /// <param name="moduleType"></param>
        /// <returns></returns>
        public IEnumerable<Task> GetCoreTasksByModuleType(Guid id, Module.ModuleType moduleType)
        {
            return MContext.Task
                .Include(t => t.ParentTopic)
                .Include(t => t.Moderations)
                .Include(t => t.VoiceMessages)
                .Include(t => t.Attachments)
                .Include(t => t.DocumentLibraries)
                .Where(t => t.ParentTopic.ParentTopicGroup.ParentModule.Type == moduleType
                 && (t.IsCore
                    || (t.SourceId.HasValue
                        && t.ParentTopic.ParentTopicGroup.TopicGroupCategory == TopicGroup.Category.Core
                        && t.ParentTopic.ParentTopicGroup.ParentModule.Package.ID == id)));
        }

        /// <summary>
        /// Upate Tasks due week
        /// </summary>
        /// <param name="earliestStartWeek"></param>
        public void UpdateTaskWeekDue(int earliestStartWeek)
        {
            var tasks = MContext.Task.Where(t => !t.Deleted.HasValue && t.WeekDue < earliestStartWeek);
            foreach (var task in tasks)
            {
                task.WeekDue = earliestStartWeek;
            }
            MContext.SaveChanges();
        }

        /// <summary>
        /// Is task name unique
        /// </summary>
        /// <param name="taskName"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        public bool IsTaskNameUnique(string taskName, Reviewable parent)
        {
            var count = MContext.Task.Where(t => (t.ParentTopicId == parent.ID || t.ProjectInstanceId == parent.ID || t.ContactId == parent.ID) && t.Name.Equals(taskName));

            return count.Any();
        }

        /// <summary>
        /// Get Latest task week
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int? GetLatestForPackage(Guid id)
        {
            var tasks = GetAllByPackageId(id);
            return tasks.Any() ? (int?)tasks.Last().WeekDue : null;
        }

        /// <summary>
        /// Sets the date & time the task is due
        /// </summary>
        /// <param name="packageId"></param>
        /// <param name="onboardingStart"></param>
        /// <param name="onboardinDuration"></param>
        public void SetFixedDateTime(Guid packageId, DateTime onboardingStart, int onboardinDuration)
        {
            var tasks = MContext.Task.Where(t => !t.Deleted.HasValue && (t.ParentTopic.ParentTopicGroup.ParentModule.Package.ID == packageId
                || t.ProjectInstace.PackageId == packageId || t.Contact.PackageId == packageId)).ToList();

            foreach (var task in tasks)
            {
                var targetWeek = task.WeekDue;
                if (targetWeek > onboardinDuration)
                    task.WeekDue = targetWeek = onboardinDuration;

                task.Fixed = onboardingStart.AddDays((targetWeek - 1) * 7).Friday();
            }

            MContext.SaveChanges();
        }

        /// <summary>
        /// Clears the task due date time
        /// </summary>
        /// <param name="packageId"></param>
        public void ResetFixedDateTime(Guid packageId)
        {

            var tasks = MContext.Task.Where(t => !t.Deleted.HasValue && t.ParentTopic.ParentTopicGroup.ParentModule.Package.ID == packageId || t.ProjectInstace.PackageId == packageId || t.Contact.PackageId == packageId).ToList();

            foreach (var task in tasks)
            {
                task.Fixed = null;
            }

            MContext.SaveChanges();
        }
    }
}
