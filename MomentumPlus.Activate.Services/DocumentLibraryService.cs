﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Services
{
    /// <summary>
    /// Document Library Service
    /// </summary>
    public class DocumentLibraryService : ServiceBase, IDocumentLibraryService
    {
        public DocumentLibraryService(MomentumContext context)
            : base(context)
        {
        }

        /// <summary>
        /// Gets all items from the document library
        /// </summary>
        /// <param name="deleted">Return deleted</param>
        /// <returns>DocumentLibrary items</returns>
        public IEnumerable<DocumentLibrary> GetAll(bool deleted = false)
        {
            var documents = MContext.DocumentLibrary.Where(l => l.Deleted.HasValue == deleted);

            return documents;
        }

        /// <summary>
        /// Gets document library by Id
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns>DocumentLibrary</returns>
        public DocumentLibrary GetByID(Guid documentId)
        {
            var document = MContext.DocumentLibrary.Find(documentId);

            return document;
        }

        /// <summary>
        /// Adds a document library
        /// </summary>
        /// <param name="documentLibrary">Document library to add</param>
        /// <returns>Document Library</returns>
        public DocumentLibrary Add(DocumentLibrary documentLibrary)
        {
            documentLibrary = MContext.DocumentLibrary.Add(documentLibrary);

            MContext.SaveChanges();

            return documentLibrary;
        }

        /// <summary>
        /// Count the number of document libraries
        /// </summary>
        /// <param name="positionId">Position id filter</param>
        /// <param name="query">Query</param>
        /// <param name="deleted">Count deleted</param>
        /// <returns></returns>
        public int Count(Guid? positionId, string query, bool deleted = false)
        {
            IQueryable<DocumentLibrary> documents;

            if (positionId.HasValue)
            {
                var position =
                    MContext.Position.FirstOrDefault(p => p.ID == positionId.Value && p.Deleted.HasValue == deleted);
                documents = position.DocumentLibraries.AsQueryable();
            }
            else
            {
                var positions = MContext.Position.Where(p => p.Deleted.HasValue == deleted);
                documents = positions.SelectMany(p => p.DocumentLibraries).Distinct();
            }

            if (!string.IsNullOrEmpty(query))
            {
                var documentsTemp = from d in documents
                    where d.Name.Contains(query) || d.SearchTags.Contains(query)
                    select d;
                try
                {
                    if (documentsTemp != null && documentsTemp.Any())
                    {
                        documents = documentsTemp;
                    }
                }
                catch
                {
                    documents = null;
                }
            }

            return documents != null ? documents.Count() : 0;
        }

        /// <summary>
        /// Updates document library
        /// </summary>
        /// <param name="document"></param>
        /// <returns></returns>
        public DocumentLibrary UpdateDocumentLibrary(DocumentLibrary document)
        {
            MContext.Entry(document).State = EntityState.Modified;

            MContext.SaveChanges();

            return document;
        }

        /// <summary>
        /// Search Document Library
        /// </summary>
        /// <param name="positionId">Position id to filter</param>
        /// <param name="query">Query</param>
        /// <param name="pageNo">Page number</param>
        /// <param name="pageSize">Size opf page</param>
        /// <param name="sortColumn">Column to sort by</param>
        /// <param name="sortAscending">Sort ascending</param>
        /// <param name="deleted">Search deleted</param>
        /// <returns></returns>
        public IEnumerable<DocumentLibrary> Search(Guid? positionId, string query, int pageNo, int pageSize,
            DocumentLibrary.SortColumn sortColumn, bool sortAscending, bool deleted = false)
        {
            IQueryable<DocumentLibrary> documents;

            if (positionId.HasValue)
            {
                var position = MContext.Position.Find(positionId.Value);
                documents = position.DocumentLibraries.AsQueryable();
            }
            else
            {
                var positions = MContext.Position.Where(p => p.Deleted.HasValue == deleted);
                documents = positions.SelectMany(p => p.DocumentLibraries).Distinct();
            }

            if (!string.IsNullOrEmpty(query))
            {
                documents = from d in documents
                    where (!string.IsNullOrEmpty(d.Name) && d.Name.ToLower().Contains(query.ToLower())) ||
                          (!string.IsNullOrEmpty(d.SearchTags.ToLower()) && d.SearchTags.Contains(query.ToLower()))
                    select d;
            }

            SortAndPage(pageNo, pageSize, sortColumn.ToString(), sortAscending, ref documents);
            return documents;
        }

        /// <summary>
        /// Adds document library item to contact
        /// </summary>
        /// <param name="documentId">Document libary item to add</param>
        /// <param name="contactId">Contact to add document libraty item to</param>
        public void AddToContact(Guid documentId, Guid contactId)
        {
            var document =
                GetByID(documentId);

            var contact =
                MContext.Contact.Find(contactId);

            contact.DocumentLibraries.Add(document);

            MContext.SaveChanges();
        }

        /// <summary>
        /// Adds document library item to meeting
        /// </summary>
        /// <param name="documentId">Document item to add</param>
        /// <param name="meetingId">Meeting to add document library item to</param>
        public void AddToMeeting(Guid documentId, Guid meetingId)
        {
            var document =
                GetByID(documentId);

            var meeting =
                MContext.Meeting.Find(meetingId);

            meeting.DocumentLibraries.Add(document);

            MContext.SaveChanges();
        }

        /// <summary>
        /// Adds document library item to task
        /// </summary>
        /// <param name="documentId">Document library item to add</param>
        /// <param name="taskId">Task to add document library item to</param>
        public void AddToTask(Guid documentId, Guid taskId)
        {
            var document =
                GetByID(documentId);

            var task =
                MContext.Task.Find(taskId);

            task.DocumentLibraries.Add(document);

            MContext.SaveChanges();
        }

        /// <summary>
        /// Add document libary item to topic
        /// </summary>
        /// <param name="documentId">Document library item to add</param>
        /// <param name="topicId">Topic to add document libary item to</param>
        public void AddToTopic(Guid documentId, Guid topicId)
        {
            var document =
                GetByID(documentId);

            var topic =
                MContext.Topic.Find(topicId);

            topic.DocumentLibraries.Add(document);

            MContext.SaveChanges();
        }

        /// <summary>
        /// Add document library item to topic group
        /// </summary>
        /// <param name="documentId">Document library item to add</param>
        /// <param name="topicGroupId">Topic group to add document library item to</param>
        public void AddToTopicGroup(Guid documentId, Guid topicGroupId)
        {
            var document =
                GetByID(documentId);

            var topicGroup =
                MContext.TopicGroup.Find(topicGroupId);

            MContext.SaveChanges();
        }

        /// <summary>
        /// Add document library item to project
        /// </summary>
        /// <param name="documentId">Document library item to add</param>
        /// <param name="projectId">Project to add document library to</param>
        public void AddToProject(Guid documentId, Guid projectId)
        {
            var document =
                GetByID(documentId);

            var project =
                MContext.Project.Find(projectId);

            project.DocumentLibraries.Add(document);

            MContext.SaveChanges();
        }

        /// <summary>
        /// Add document library item to project instance
        /// </summary>
        /// <param name="documentId">Document library item to add</param>
        /// <param name="projectInstanceId">Project instance to add document library to</param>
        public void AddToProjectInstance(Guid documentId, Guid projectInstanceId)
        {
            var document =
                GetByID(documentId);

            var projectInstance =
                MContext.ProjectInstance.Find(projectInstanceId);

            projectInstance.DocumentLibraries.Add(document);

            MContext.SaveChanges();
        }

        [Obsolete]
        public void AddToStaff(Guid documentId, Guid staffId)
        {
            var document =
                GetByID(documentId);

            MContext.SaveChanges();
        }

        /// <summary>
        /// Gets recent Documment Library items
        /// </summary>
        /// <param name="packageId">Package id to filter</param>
        /// <param name="count">Number of items to return</param>
        /// <returns>Document Library items</returns>
        public IEnumerable<BaseModel> GetRecentByPackageId(Guid packageId, int count = 9)
        {
            var documents = MContext.DocumentLibrary.Where(d =>
                d.Contacts.Any(
                    c => c.PackageId == packageId) ||
                d.Meetings.Any(m => m.ParentContact.PackageId == packageId || (m.ProjectInstance != null && m.ProjectInstance.PackageId == packageId)) ||
                d.Tasks.Any(
                    ta => ta.ParentTopic.ParentTopicGroup.ParentModule.Package.ID == packageId || ta.ProjectInstace.PackageId == packageId) ||
                d.Topics.Any(
                    to => to.ParentTopicGroup.ParentModule.Package.ID == packageId) ||
                d.ProjectInstances.Any(p => p.PackageId == packageId)
                ).ToList();

            var package = MContext.Package.Find(packageId);
            var staffDocuments = new List<DocumentLibrary>();

            return documents.Concat(staffDocuments).OrderByDescending(a => a.Created).Take(count);
        }
    }
}