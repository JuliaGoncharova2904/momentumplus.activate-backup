﻿using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Activate.Services.Interfaces;
using System.Data.Entity;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models.People;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Services
{
    /// <summary>
    /// Meetings Service
    /// </summary>
    public class MeetingService : ServiceBase, IMeetingService
    {
        public MeetingService(MomentumContext context, User user) : base(context, user) { }

        #region IMeetingService Members

        /// <summary>
        /// Get the Meetings for a Package
        /// </summary>
        /// <param name="packageId">The Package GUID</param>
        /// <param name="sortColumn">The sort column</param>
        /// <param name="sortAscending">The sort order</param>
        /// <param name="deleted">If true will only return deleted Meetings</param>
        /// <param name="meetingTypeIdsFilter">A list of meeting type GUIDs to filter by</param>
        /// <param name="createdAfter">If passed only meetings created after this date will be returned</param>
        /// <returns></returns>
        public IEnumerable<Meeting> GetByPackageId(Guid packageId, Meeting.SortColumn sortColumn, bool sortAscending, bool deleted = false,
            List<Guid> meetingTypeIdsFilter = null, DateTime? createdAfter = null)
        {
            var meetings =
                GetByPackageIdBase(packageId, deleted, meetingTypeIdsFilter, createdAfter);

            if (sortColumn == Meeting.SortColumn.ParentContactName)
            {
                //meetings = meetings.OrderBy(m => m.ParentContact.Person.FirstName).ThenBy(m => m.ParentContact.Person.LastName);
            }
            else if (sortColumn == Meeting.SortColumn.MeetingType)
            {
                meetings = meetings.OrderBy(m => m.MeetingType.Title);
            }
            else
            {
                SortAndPage<Meeting>(1, meetings.Count(), sortColumn.ToString(), sortAscending, ref meetings);
            }

            return meetings;
        }

        /// <summary>
        /// Get the Meetings for a Package - sorted and paged
        /// </summary>
        /// <param name="packageId">The Package GUID</param>
        /// <param name="pageNo">The page number</param>
        /// <param name="pageSize">The page size</param>
        /// <param name="sortColumn">The sort column</param>
        /// <param name="sortAscending">The sort order</param>
        /// <param name="deleted">If true will only return deleted Meetings</param>
        /// <param name="meetingTypeIdsFilter">A list of meeting type GUIDs to filter by</param>
        /// <param name="createdAfter">If passed only meetings created after this date will be returned</param>
        /// <returns></returns>
        public IEnumerable<Meeting> GetByPackageId(Guid packageId, int pageNo, int pageSize, Meeting.SortColumn sortColumn, bool sortAscending, bool deleted = false, List<Guid> meetingTypeIdsFilter = null, DateTime? createdAfter = null)
        {
            var meetings =
                GetByPackageIdBase(packageId, deleted, meetingTypeIdsFilter, createdAfter);

            if (sortColumn == Meeting.SortColumn.ParentContactName)
            {
                meetings = meetings.OrderBy(m => m.Name).Skip(pageSize * (pageNo - 1)).Take(pageSize);
            }
            else if (sortColumn == Meeting.SortColumn.MeetingType)
            {
                meetings = meetings.OrderBy(m => m.MeetingType.Title).Skip(pageSize*(pageNo - 1)).Take(pageSize);
            }
            else
            {
                SortAndPage<Meeting>(pageNo, pageSize, sortColumn.ToString(), sortAscending, ref meetings);
            }

            return meetings.ToList();
        }

        /// <summary>
        /// Get all active meetings by Package
        /// </summary>
        /// <param name="packageId">The Package GUID</param>
        /// <returns></returns>
        public IEnumerable<Meeting> GetByPackageId(Guid packageId)
        {
            var meetings
                = GetByPackageIdBase(packageId);

            return meetings.ToList();
        }

        /// <summary>
        /// Get a Project Meeting
        /// </summary>
        /// <param name="id">A Project Instance GUID</param>
        /// <returns></returns>
        public Meeting GetByProjectInstanceId(Guid id)
        {
            return MContext.Meeting.FirstOrDefault(m => m.ProjectInstanceId == id);
        }

        /// <summary>
        /// Get the total number of Meetings in a Package
        /// </summary>
        /// <param name="packageId">The Package GUID</param>
        /// <param name="deleted">If true will return the number of deleted Meetings</param>
        /// <param name="meetingTypeIdsFilter">A list of meeting type GUIDs to filter by</param>
        /// <returns></returns>
        public int CountByPackageId(Guid packageId, bool deleted = false, List<Guid> meetingTypeIdsFilter = null)
        {
            var meetings
                = GetByPackageIdBase(packageId, deleted, meetingTypeIdsFilter);

            return meetings.Count();
        }

        [Obsolete]
        public IEnumerable<Meeting> GetOnboarding(Guid packageId)
        {
            IEnumerable<Meeting> meetings = null;

            var package
                = MContext.Package.Find(packageId);

            if (package != null)
            {
                meetings = (from c in package.Contacts
                            from m in c.Meetings
                            select m).ToList();
            }

            return meetings;
        }

        /// <summary>
        /// Get a meeting by ID
        /// </summary>
        /// <param name="meetingId">The Meeting GUID</param>
        /// <returns></returns>
        public Meeting GetByMeetingId(Guid meetingId)
        {
            var meeting =
                MContext.Meeting.Find(meetingId);

            return meeting;
        }

        /// <summary>
        /// Update an existing Meeting in the database
        /// </summary>
        /// <param name="meeting">A Meeting object</param>
        /// <returns>The updated Meeting</returns>
        public Meeting Update(Meeting meeting)
        {
            MContext.Entry(meeting).State = EntityState.Modified;

            MContext.SaveChanges();

            return meeting;
        }

        /// <summary>
        /// Search for attachments on meetings in a package
        /// </summary>
        /// <param name="searchString">The search query</param>
        /// <param name="packageId">The Package GUID</param>
        /// <returns></returns>
        public IEnumerable<Attachment> SearchAttachments(string searchString, Guid packageId)
        {
            RestrictedAccess queryBuilder =
               new RestrictedAccess(MContext, packageId);

            IQueryable<Attachment> searchResults = (from a in queryBuilder.MeetingAttachments
                                                    where a.Name.Contains(searchString) ||
                                                    a.SearchTags.Contains(searchString)
                                                    select a);
            return searchResults.ToList(); 
        }

        /// <summary>
        /// Search for voice messages on meetings in a package
        /// </summary>
        /// <param name="searchString">The search query</param>
        /// <param name="packageId">The Package GUID</param>
        /// <returns></returns>
        public IEnumerable<VoiceMessage> SearchVoiceMessages(string searchString, Guid packageId)
        {
            RestrictedAccess queryBuilder =
                new RestrictedAccess(MContext, packageId);

            IQueryable<VoiceMessage> searchResults = (from v in queryBuilder.MeetingVoiceMessages
                                                      where v.Name.Contains(searchString) ||
                                                      v.SearchTags.Contains(searchString)
                                                      select v);
            return searchResults.ToList();
        }

        /// <summary>
        /// Get all meeting types
        /// </summary>
        /// <returns></returns>
        public IEnumerable<MeetingType> GetMeetingTypes()
        {
            var meetingTypes = (from m in MContext.MeetingType
                                orderby m.Order
                                select m);

            return meetingTypes.ToList();
        }

        /// <summary>
        /// Get a meeting type by ID
        /// </summary>
        /// <param name="meetingTypeId">The MeetingType GUID</param>
        /// <returns></returns>
        public MeetingType GetMeetingTypeById(Guid meetingTypeId)
        {
            return MContext.MeetingType.Find(meetingTypeId);
        }

        /// <summary>
        /// Get all Meetings created when a Period was locked
        /// </summary>
        /// <param name="periodId">The Period GUID</param>
        /// <returns></returns>
        public IEnumerable<Meeting> GetByPeriodId(Guid periodId)
        {
            return MContext.Meeting.Where(m => m.PeriodId == periodId);
        }

        /// <summary>
        /// Delete a Meeting from the database
        /// </summary>
        /// <param name="meeting"></param>
        public void Delete(Meeting meeting)
        {
            MContext.Meeting.Remove(meeting);
            MContext.SaveChanges();
        }

        #endregion

        #region Private helper methods

        /// <summary>
        /// Helper methid to get meetings for a Package
        /// </summary>
        /// <param name="packageId"></param>
        /// <param name="deleted"></param>
        /// <param name="meetingTypeIdsFilter"></param>
        /// <param name="createdAfter"></param>
        /// <returns></returns>
        private IQueryable<Meeting> GetByPackageIdBase(Guid packageId, bool deleted = false,
          List<Guid> meetingTypeIdsFilter = null, DateTime? createdAfter = null)
        {
            var meetings = MContext.Meeting.Where(m => m.Deleted.HasValue == deleted
                                                       && (m.ParentContact.PackageId == packageId
                                                            || m.ProjectInstance.PackageId == packageId));

            if (meetingTypeIdsFilter != null)
                meetings = meetings.Where(m => meetingTypeIdsFilter.Contains(m.MeetingType.ID));

            if (createdAfter.HasValue)
                meetings = meetings.Where(m => m.Start > createdAfter.Value);

            return meetings;
        }

        #endregion
    }
}
