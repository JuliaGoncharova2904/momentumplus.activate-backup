﻿using System;
using System.Linq;
using MomentumPlus.Activate.Services.Interfaces;
using System.Data.Entity;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models.Requests;
using MomentumPlus.Core.Models.Moderation;

namespace MomentumPlus.Activate.Services
{
    /// <summary>
    /// Request Service
    /// </summary>
    public class RequestService : ServiceBase, IRequestService
    {
        public RequestService(MomentumContext context) : base(context) { }

        #region IRequestService Members

        /// <summary>
        /// Adds a Request
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Request AddRequest(Request request)
        {
            request = MContext.Moderated.Add(request) as Request;
            MContext.SaveChanges();

            return request;
        }

        /// <summary>
        /// Gets a collection of Requests by
        /// Package Id
        /// </summary>
        /// <param name="packageId"></param>
        /// <returns></returns>
        public IQueryable<Request> GetRequests(Guid packageId)
        {
            return GetRequests<Request>(packageId);
        }

        /// <summary>
        /// Gets a collection of Requests
        /// by Package Id
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="packageId"></param>
        /// <param name="approved"></param>
        /// <returns></returns>
        public IQueryable<T> GetRequests<T>(Guid packageId, bool approved = false) where T : Request
        {
            IQueryable<T> result = MContext.Moderated.OfType<T>().Where(r => r.PackageId == packageId);

            result = approved ? result.Where(Moderated.Approved<T>()) : result.Where(Moderated.NotApproved<T>());

            return result;
        }

        /// <summary>
        /// Gets a Request by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Request GetRequest(Guid id)
        {
            return GetRequest<Request>(id);
        }

        /// <summary>
        /// Gets a Request by Id
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        public T GetRequest<T>(Guid id) where T : Request
        {
            return MContext.Moderated.OfType<T>().FirstOrDefault(m => m.ID == id);
        }

        /// <summary>
        /// Gets a Request by reference Id
        /// </summary>
        /// <param name="referenceId"></param>
        /// <returns></returns>
        public Request GetRequest(int referenceId)
        {
            return MContext.Moderated.OfType<Request>().FirstOrDefault( r => r.ReferenceId == referenceId );
        }

        /// <summary>
        /// Gets a Request by Reference Id
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="referenceId"></param>
        /// <returns></returns>
        public T GetRequest<T>(int referenceId) where T : Request
        {
            return MContext.Moderated.OfType<T>().FirstOrDefault(r => r.ReferenceId == referenceId);
        }

        /// <summary>
        /// Updates a Request
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Request UpdateRequest(Request request)
        {
            MContext.Entry(request).State = EntityState.Modified;
            MContext.SaveChanges();

            return request;
        }

        /// <summary>
        /// Deletes a Request by Request Id
        /// </summary>
        /// <param name="id"></param>
        public void DeleteRequest(Guid id)
        {
            var request = MContext.Moderated.Find(id) as Request;
            DeleteRequest(request);
        }

        /// <summary>
        /// Deletes a Request
        /// </summary>
        /// <param name="request"></param>
        public void DeleteRequest(Request request)
        {
            MContext.Moderated.Remove(request);
            MContext.SaveChanges();
        }

        #endregion
    }
}
