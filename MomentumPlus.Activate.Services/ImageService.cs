﻿using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Services
{
    /// <summary>
    /// Image Service
    /// </summary>
    public class ImageService : ServiceBase, IImageService
    {
        public ImageService(MomentumContext context) : base(context) { }

        #region IImageService Members

        /// <summary>
        /// Gets the default images used by the Projects model
        /// </summary>
        /// <param name="retired"></param>
        /// <returns></returns>
        public IEnumerable<Image> GetDefaultImages(bool retired = false)
        {
            return MContext.File.OfType<Image>().Where(i => i.IsDefault && i.Deleted.HasValue == retired);
        }

        /// <summary>
        /// Gets image by Id
        /// </summary>
        /// <param name="ID">Image Id</param>
        /// <returns>Image</returns>
        public Image GetImage(Guid ID)
        {
            return MContext.File.OfType<Image>().FirstOrDefault(i => i.ID == ID);
        }

        #endregion
    }
}
