﻿using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Activate.Services.Interfaces;
using System.Data.Entity;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Services
{
    /// <summary>
    /// File Service
    /// </summary>
    public class FileService : ServiceBase, IFileService
    {
        public FileService(MomentumContext context) : base(context) { }

        #region IFileService Members

        /// <summary>
        /// Add a new File to the database
        /// </summary>
        /// <param name="file">A new File object</param>
        /// <returns>The added File</returns>
        public File AddFile(File file)
        {
            file = MContext.File.Add(file);
            MContext.SaveChanges();

            return file;
        }

        /// <summary>
        /// Update an existing file in the database
        /// </summary>
        /// <param name="file">An existing File object</param>
        /// <returns>The updated File object</returns>
        public File Update(File file)
        {
            MContext.Entry(file).State = EntityState.Modified;
            MContext.SaveChanges();
            return file;
        }

        /// <summary>
        /// Retrieve a File from the database
        /// </summary>
        /// <param name="fileId">The File GUID</param>
        /// <returns>The File</returns>
        public File GetFile(Guid fileId)
        {
            var file = 
                MContext.File.Find(fileId);

            return file;
        }

        /// <summary>
        /// Get all non-deleted Files from the database
        /// </summary>
        /// <returns></returns>
        public IEnumerable<File> GetAll()
        {
            var files =
                MContext.File.Where(f => !f.Deleted.HasValue);

            return files;
        }

        /// <summary>
        /// Delete a file from the database
        /// </summary>
        /// <param name="fileId">The File GUID</param>
        public void DeleteFile(Guid fileId)
        {
            var file = MContext.File.Find(fileId);
            MContext.File.Remove(file);

            MContext.SaveChanges();
        }
        

        #endregion
    }
}
