﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Activate.Services
{
    /// <summary>
    /// Attachement Service
    /// </summary>
    public class AttachmentService : ServiceBase, IAttachmentService
    {
        public AttachmentService(MomentumContext context) : base(context)
        {
        }

        /// <summary>
        /// Update the Deleted property on an Attachment and related File
        /// </summary>
        /// <param name="deleted"></param>
        /// <param name="attachmentId"></param>
        private void UpdateDeletedFields(DateTime? deleted, Guid attachmentId)
        {
            var attachment = MContext.Attachment.Find(attachmentId);
            attachment.Deleted = deleted;

            if (attachment.Data != null)
            {
                attachment.Data.Deleted = deleted;
            }
        }

        #region IAttachmentService Members

        /// <summary>
        /// Get all Attachment entities
        /// </summary>
        /// <param name="deleted">If true will return only deleted items</param>
        /// <returns></returns>
        public IEnumerable<Attachment> GetAll(bool deleted = false)
        {
            var attachments = 
                MContext.Attachment.Where(a => a.Deleted.HasValue == deleted);

            return attachments;
        }

        /// <summary>
        /// Get all attachments for a position - paged and sorted
        /// </summary>
        /// <param name="positionId">Position GUID</param>
        /// <param name="pageNo">Page number</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="sortColumn">Sort column</param>
        /// <param name="sortAscending">Sort order</param>
        /// <param name="deleted">If true will return only deleted items</param>
        /// <returns></returns>
        public IEnumerable<Attachment> GetAll(Guid positionId, int pageNo, int pageSize,
            Attachment.SortColumn sortColumn, bool sortAscending, bool deleted = false)
        {
            var position = MContext.Position.Find(positionId);

            var attachments =
                position.Attachments.AsQueryable();

            attachments = from a in attachments where a.Deleted.HasValue == deleted select a;

            SortAndPage(pageNo, pageSize, sortColumn.ToString(), sortAscending, ref attachments);

            return attachments.ToList();
        }

        /// <summary>
        /// Search attachments for a position - sorted and paged
        /// </summary>
        /// <param name="positionId">Position GUID</param>
        /// <param name="query">Search query</param>
        /// <param name="pageNo">Page number</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="sortColumn">Sort column</param>
        /// <param name="sortAscending">Sort order</param>
        /// <param name="deleted">If true will return only deleted items</param>
        /// <returns></returns>
        public IEnumerable<Attachment> Search(Guid positionId, string query, int pageNo, int pageSize,
            Attachment.SortColumn sortColumn, bool sortAscending, bool deleted = false)
        {
            var position = MContext.Position.Find(positionId);

            var attachments = position.Attachments.AsQueryable();
            attachments = from a in attachments where a.Deleted.HasValue == deleted select a;

            if (!string.IsNullOrEmpty(query))
            {
                attachments = from a in attachments
                    where a.Name.Contains(query) || a.SearchTags.Contains(query)
                    select a;
            }

            SortAndPage(pageNo, pageSize, sortColumn.ToString(), sortAscending, ref attachments);

            return attachments;
        }

        /// <summary>
        /// Get the most recently added attachments for a given package
        /// </summary>
        /// <param name="packageId">The package GUID</param>
        /// <param name="count">Number of attachments to return</param>
        /// <returns></returns>
        public IEnumerable<BaseModel> GetRecentByPackage(Guid packageId, int count = 9)
        {
            // Get all the attachments in the package - from project instances, tasks, topics, contacts, and meetings
            var attachments = MContext.Attachment.Where(a =>
                (a.ProjectInstance.PackageId == packageId) ||
                (a.ParentTask.ProjectInstace.PackageId == packageId) || 
                (a.ParentTask.ParentTopic.ParentTopicGroup.ParentModule.Package.ID == packageId) ||
                (a.ParentTopic.ParentTopicGroup.ParentModule.Package.ID == packageId) ||
                (a.Contact.PackageId == packageId) ||
                (a.Meeting.ParentContact.PackageId == packageId) || 
                (a.Meeting.ProjectInstance != null && a.Meeting.ProjectInstance.PackageId == packageId)
                ).ToList();

            var package = MContext.Package.Find(packageId);
            var staffAttachments = new List<Attachment>();

            return attachments.Concat(staffAttachments).OrderByDescending(a => a.Created).Take(count);
        }

        /// <summary>
        /// Get the number of attachments for a position
        /// </summary>
        /// <param name="positionId">Position GUID</param>
        /// <returns>Attachment count</returns>
        public int Count(Guid positionId)
        {
            var count =
                MContext.Position.Find(positionId).Attachments.Count();

            return count;
        }

        /// <summary>
        /// Add an attachment
        /// </summary>
        /// <param name="positionId"></param>
        /// <param name="attachment"></param>
        /// <returns></returns>
        public Attachment Add(Guid positionId, Attachment attachment)
        {
            var position =
                MContext.Position.Find(positionId);

            position.Attachments.Add(attachment);

            MContext.SaveChanges();

            return attachment;
        }

        /// <summary>
        /// Create a new Attachment in the database
        /// </summary>
        /// <param name="attachment"></param>
        /// <returns></returns>
        public Attachment Add(Attachment attachment)
        {
            MContext.Attachment.Add(attachment);
            MContext.SaveChanges();

            return attachment;
        }

        /// <summary>
        /// Add an Attachment to both a Position and a Contact
        /// </summary>
        /// <param name="positionId">The Position GUID</param>
        /// <param name="attachment">The Attachment object</param>
        /// <param name="contactId">The Contact GUID</param>
        /// <returns></returns>
        public Attachment Add(Guid positionId, Attachment attachment, Guid contactId)
        {
            var position =
                MContext.Position.Find(positionId);
            var contact =
                MContext.Contact.Find(contactId);
            position.Attachments.Add(attachment);
            contact.Attachments.Add(attachment);

            MContext.SaveChanges();
            return attachment;
        }

        /// <summary>
        /// Update an attachment in the database
        /// </summary>
        /// <param name="attachment">The Attachment object</param>
        /// <returns></returns>
        public Attachment Update(Attachment attachment)
        {
            MContext.Entry(attachment).State = EntityState.Modified;
            MContext.SaveChanges();

            return attachment;
        }

        /// <summary>
        /// Delete an Attachment from the database
        /// </summary>
        /// <param name="attachmentId">The Attachment GUID</param>
        public void Delete(Guid attachmentId)
        {
            var attachement = MContext.Attachment.Find(attachmentId);
            MContext.Attachment.Remove(attachement);

            MContext.SaveChanges();
        }

        /// <summary>
        /// Get a non-deleted Attachment
        /// </summary>
        /// <param name="attachmentId">The attachment GUID</param>
        /// <returns>The Attachment object</returns>
        public Attachment GetByID(Guid attachmentId)
        {
            var attachment = MContext.Attachment.FirstOrDefault(f => f.ID == attachmentId && !f.Deleted.HasValue);

            return attachment;
        }

        /// <summary>
        /// Retire an attachment
        /// </summary>
        /// <param name="attachmentId">The Attachment GUID</param>
        public void Retire(Guid attachmentId)
        {
            UpdateDeletedFields(DateTime.Now, attachmentId);
        }

        /// <summary>
        /// Resume an attachment
        /// </summary>
        /// <param name="attachmentId">The Attachment GUID</param>
        public void Restore(Guid attachmentId)
        {
            UpdateDeletedFields(null, attachmentId);
        }

        #endregion
    }
}