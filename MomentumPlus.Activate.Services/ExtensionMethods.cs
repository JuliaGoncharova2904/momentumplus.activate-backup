﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using MomentumPlus.Activate.Services.Interfaces;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.People;
using MomentumPlus.Core.Models.Moderation;
using MomentumPlus.Core.Models.Requests;

namespace MomentumPlus.Activate.Services
{
    /// <summary>
    /// Service Extension Methods
    /// </summary>
    public static class ExtensionMethods
    {
        #region Subjectivity

        /// <summary>
        /// Get all the subjective words from the keywords master list
        /// </summary>
        /// <param name="MasterListService"></param>
        /// <returns></returns>
        public static List<string> GetSubjectiveWords(IMasterListService MasterListService)
        {
            var listedWords = new List<string>();
            IEnumerable<MasterList> keywords =
                GetKeywordsMasterList(MasterListService).ToList();

            foreach (var keyword in keywords)
            {
                listedWords.Add(keyword.Name);
            }
            return listedWords;
        }

        /// <summary>
        /// Get the subjectivity count for a topic
        /// </summary>
        /// <param name="topic"></param>
        /// <param name="MasterListService"></param>
        /// <returns></returns>
        public static int CountSubjectivity(this Topic topic, IMasterListService MasterListService)
        {
            int count = 0;

            IEnumerable<MasterList> keywords =
                GetKeywordsMasterList(MasterListService);

            // Add subjectivity of notes
            count += GetSubjectivity(topic.Notes + " ", keywords);

            // Add subjectivity of answers
            if (topic.Fields != null)
            {
                foreach (TopicField topicField in topic.Fields)
                {
                    if (topicField.Question != null && topicField.Answers != null && topicField.Question.IsFreeText)
                    {
                        foreach (TopicAnswer answer in topicField.Answers)
                        {
                            count +=
                                answer.Text.GetSubjectivity(keywords);
                        }
                    }
                }
            }

            return count;
        }

        /// <summary>
        /// Get the subjectivity count for a task
        /// </summary>
        /// <param name="task"></param>
        /// <param name="MasterListService"></param>
        /// <returns></returns>
        public static int CountSubjectivity(this Task task, IMasterListService MasterListService)
        {
            int count = 0;

            IEnumerable<MasterList> keywords =
                GetKeywordsMasterList(MasterListService);

            count += GetSubjectivity(task.Notes + " ", keywords);
            count += GetSubjectivity(task.Handover + " ", keywords);

            return count;
        }

        /// <summary>
        /// Get the subjectivity count for a Contact
        /// </summary>
        /// <param name="contact"></param>
        /// <param name="MasterListService"></param>
        /// <returns></returns>
        public static int CountSubjectivity(this Contact contact, IMasterListService MasterListService)
        {
            int count = 0;

            count += CountSubjectivityRelationship(contact, MasterListService);
            count += CountSubjectivityPeople(contact, MasterListService);

            return count;
        }

        /// <summary>
        /// Get the subjectivity count for a relationship
        /// </summary>
        /// <param name="contact"></param>
        /// <param name="MasterListService"></param>
        /// <returns></returns>
        public static int CountSubjectivityRelationship(this Contact contact, IMasterListService MasterListService)
        {
            int count = 0;

            IEnumerable<MasterList> keywords =
                GetKeywordsMasterList(MasterListService);

            count += GetSubjectivity(contact.Notes + " ", keywords);

            return count;
        }

        /// <summary>
        /// Get the subjectivity count for a person
        /// </summary>
        /// <param name="contact"></param>
        /// <param name="MasterListService"></param>
        /// <returns></returns>
        public static int CountSubjectivityPeople(this Contact contact, IMasterListService MasterListService)
        {
            int count = 0;

            IEnumerable<MasterList> keywords =
                GetKeywordsMasterList(MasterListService);
            
            count += GetSubjectivity(contact.FutureTraining + " ", keywords);
            count += GetSubjectivity(contact.ManagementNotes + " ", keywords);

            return count;
        }

        /// <summary>
        /// Get the subjectivity count for a staff
        /// </summary>
        /// <param name="staff"></param>
        /// <param name="MasterListService"></param>
        /// <returns></returns>
        public static int CountSubjectivity(this Staff staff, IMasterListService MasterListService)
        {
            int count = 0;

            IEnumerable<MasterList> keywords =
                GetKeywordsMasterList(MasterListService);

            count += GetSubjectivity(staff.FutureTraining + " ", keywords);
            count += GetSubjectivity(staff.ManagementNotes + " ", keywords);

            return count;
        }

        /// <summary>
        /// Get the subjectivity count for a project instance
        /// </summary>
        /// <param name="project"></param>
        /// <param name="MasterListService"></param>
        /// <returns></returns>
        public static int CountSubjectivity(this ProjectInstance project, IMasterListService MasterListService)
        {
            int count = 0;

            IEnumerable<MasterList> keywords =
                GetKeywordsMasterList(MasterListService);

            count += GetSubjectivity(project.Notes + " ", keywords);

            return count;
        }

        #endregion

        #region Cloning

        /// <summary>
        /// Clone a File object
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static File Clone(this File file)
        {
            File fileClone = DeepClone<File>(file);
            fileClone.SourceId = file.ID;
            fileClone.ID = Guid.NewGuid();
            fileClone.Created = DateTime.Now;
            fileClone.Modified = null;

            return fileClone;
        }

        #endregion

        #region Word Count

        /// <summary>
        /// Get the word count for a Topic
        /// </summary>
        /// <param name="topic"></param>
        /// <returns></returns>
        public static int CountWords(this Topic topic)
        {
            int count = 0;

            count += CountWords(topic.Notes);

            if (topic.Fields != null)
            {
                foreach (TopicField topicField in topic.Fields)
                {
                    if (topicField.Question != null && topicField.Answers != null && topicField.Question.IsFreeText)
                    {
                        foreach (TopicAnswer answer in topicField.Answers)
                        {
                            count +=
                                CountWords(answer.Text);
                        }
                    }
                }
            }

            return count;
        }

        /// <summary>
        /// Get the word count for a Task
        /// </summary>
        /// <param name="task"></param>
        /// <returns></returns>
        public static int CountWords(this Task task)
        {
            int count = 0;

            count += CountWords(task.Handover);

            return count;
        }

        /// <summary>
        /// Get the word count for a Contact
        /// </summary>
        /// <param name="contact"></param>
        /// <returns></returns>
        public static int CountWords(this Contact contact)
        {
            int count = 0;

            count += CountWordsRelationship(contact);
            count += CountWordsPeople(contact);

            return count;
        }

        /// <summary>
        /// Get the word count for a relationship
        /// </summary>
        /// <param name="contact"></param>
        /// <returns></returns>
        public static int CountWordsRelationship(this Contact contact)
        {
            int count = 0;

            count += CountWords(contact.Notes);
            
            return count;
        }

        /// <summary>
        /// Get the word count for a person
        /// </summary>
        /// <param name="staff"></param>
        /// <returns></returns>
        public static int CountWordsPeople(this Contact staff)
        {
            int count = 0;

            count += CountWords(staff.FutureTraining);
            count += CountWords(staff.ManagementNotes);

            return count;
        }

        /// <summary>
        /// Get the word count for a Package
        /// </summary>
        /// <param name="package"></param>
        /// <returns></returns>
        public static int CountWords(this Package package)
        {
            int count = 0;

            // Contacts
            foreach (Contact contact in package.Contacts)
            {
                count +=
                    contact.CountWordsRelationship();
            }

            // Modules/topic groups/topics/tasks
            foreach (Module module in package.Modules)
            {
                foreach (TopicGroup topicGroup in module.TopicGroups)
                {
                    foreach (Topic topic in topicGroup.Topics)
                    {
                        count +=
                            topic.CountWords();

                        foreach (Task task in topic.Tasks)
                        {
                            count +=
                                task.CountWords();
                        }
                    }
                }
            }


            return count;
        }

        /// <summary>
        /// Get the word count for a Module
        /// </summary>
        /// <param name="module"></param>
        /// <returns></returns>
        public static int CountWords(this Module module)
        {
            int count = 0;
            foreach (TopicGroup topicGroup in module.TopicGroups)
            {
                foreach (Topic topic in topicGroup.Topics)
                {
                    count +=
                        topic.CountWords();

                    foreach (Task task in topic.Tasks)
                    {
                        count +=
                            task.CountWords();
                    }
                }
            }
            return count;
        }

        /// <summary>
        /// Get the word count for a Project instance
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        public static int CountWords(this ProjectInstance project)
        {
            var count = project.Tasks.Sum(task => task.CountWords());
            count += project.Notes.CountWords();
            return count;
        }

        /// <summary>
        /// Get the average word count across Topics and Tasks in a Module
        /// </summary>
        /// <param name="module"></param>
        /// <returns></returns>
        public static int GetModuleWordAverage(this Module module)
        {
            int totalTopicsTasks = 0;

            int count = 0;
            foreach (TopicGroup topicGroup in module.TopicGroups)
            {
                foreach (Topic topic in topicGroup.Topics)
                {
                    totalTopicsTasks++;
                    count +=
                        topic.CountWords();

                    foreach (Task task in topic.Tasks)
                    {
                        totalTopicsTasks++;
                        count +=
                            task.CountWords();
                    }
                }
            }
            try
            {
                return count / totalTopicsTasks;
            }
            catch (Exception e)
            {
                //divided by zero, just return 0
                return 0;
            }
        }

        /// <summary>
        /// Get the subjectivity count for a Package
        /// </summary>
        /// <param name="package"></param>
        /// <param name="MasterListService"></param>
        /// <returns></returns>
        public static int CountSubjectivity(this Package package, IMasterListService MasterListService)
        {
            int count = 0;

            // Contacts
            foreach (Contact contact in package.Contacts)
            {
                count +=
                    contact.CountSubjectivity(MasterListService);
            }

            // Modules/topic groups/topics/tasks
            foreach (Module module in package.Modules)
            {
                foreach (TopicGroup topicGroup in module.TopicGroups)
                {
                    foreach (Topic topic in topicGroup.Topics)
                    {
                        count +=
                            topic.CountSubjectivity(MasterListService);

                        foreach (Task task in topic.Tasks)
                        {
                            count +=
                                task.CountSubjectivity(MasterListService);
                        }
                    }
                }
            }

            return count;
        }

        /// <summary>
        /// Returns true if all Topics in a Module have been moderated
        /// </summary>
        /// <param name="module"></param>
        /// <returns></returns>
        public static bool IsComplete(this Module module)
        {
            var isComplete = !(from tg in module.TopicGroups
                               from to in tg.Topics
                               select to).Any(t => !t.Moderations.Any());


            return isComplete;
        }

        /// <summary>
        /// Returns true if an item is approved
        /// </summary>
        /// <param name="baseApproval"></param>
        /// <returns></returns>
        [Obsolete]
        public static bool Approved(this BaseApproval baseApproval)
        {
            bool approved = false;

            if (baseApproval.ApprovedHRManager.HasValue && baseApproval.ApprovedLineManager.HasValue)
                approved = true;

            return approved;
        }

        /// <summary>
        /// Get the number of attachments and voice messages in a module
        /// </summary>
        /// <param name="module"></param>
        /// <returns></returns>
        public static int GetNumberOfAttachmentsAndVoiceMessages(this Module module)
        {
            int number = 0;

            foreach (TopicGroup topicGroup in module.TopicGroups)
            {
                foreach (Topic topic in topicGroup.Topics)
                {
                    number += topic.Attachments.Count();
                    number += topic.DocumentLibraries.Count();
                    number += topic.VoiceMessages.Count();

                    foreach (Task task in topic.Tasks)
                    {
                        number += GetNumberOfAttachmentsAndVoiceMessages(task);
                    }
                }
            }
            return number;
        }

        /// <summary>
        /// Get the number of attachments and voice messages in a Contact
        /// </summary>
        /// <param name="contact"></param>
        /// <returns></returns>
        public static int GetNumberOfAttachmentsAndVoiceMessages(this Contact contact)
        {
            int number = 0;

            number += contact.Attachments.Count();
            number += contact.DocumentLibraries.Count();
            number += contact.VoiceMessages.Count();

            return number;
        }

        /// <summary>
        /// Get the nuber of attachments and voice messages in a person
        /// </summary>
        /// <param name="person"></param>
        /// <returns></returns>
        public static int GetNumberOfAttachmentsAndVoiceMessages(this Staff person)
        {
            int number = 0;

            number += person.Attachments.Count();
            number += person.DocumentLibraries.Count();
            number += person.VoiceMessages.Count();

            return number;
        }

        /// <summary>
        /// Get the number of attachments and voice messages in a Project instance
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        public static int GetNumberOfAttachmentsAndVoiceMessages(this ProjectInstance project)
        {
            int number = 0;

            number += project.Attachments.Count();
            number += project.DocumentLibraries.Count();
            number += project.VoiceMessages.Count();

            number += project.Tasks.Sum(task => GetNumberOfAttachmentsAndVoiceMessages(task));

            return number;
        }

        /// <summary>
        /// Get the number of attachments and voice messages in a Task
        /// </summary>
        /// <param name="task"></param>
        /// <returns></returns>
        public static int GetNumberOfAttachmentsAndVoiceMessages(this Task task)
        {
            int number = 0;

            number += task.Attachments.Count();
            number += task.VoiceMessages.Count();
            number += task.DocumentLibraries.Count();

            return number;
        }


        /// <summary>
        /// Get the number of attachments and voice messages for Contacts in a Package
        /// </summary>
        /// <param name="package"></param>
        /// <returns></returns>
        public static int GetNumberOfContactAttachmentsAndVoiceMessages(this Package package)
        {
            int number = 0;

            foreach (var contact in package.Contacts)
            {
                number += GetNumberOfAttachmentsAndVoiceMessages(contact);
            }

            return number;
        }

        /// <summary>
        /// Get the word count across all Contacts in a Package
        /// </summary>
        /// <param name="package"></param>
        /// <returns></returns>
        public static int CountContactWords(this Package package)
        {
            int number = 0;

            foreach (var contact in package.Contacts)
            {
                number += CountWordsRelationship(contact);
            }

            return number;
        }

        /// <summary>
        /// Get the number of Contacts in a Package
        /// </summary>
        /// <param name="package"></param>
        /// <returns></returns>
        public static int GetContactCount(this Package package)
        {
            int number = 0;

            number = package.Contacts.Count();

            return number;
        }

        /// <summary>
        /// Get the number of approved Contacts in a Package
        /// </summary>
        /// <param name="package"></param>
        /// <returns></returns>
        public static int CountContactItemsApproves(this Package package)
        {
            return package.Contacts.Count(Moderated.Approved().Compile());
        }

        /// <summary>
        /// Get the average word count for Contacts in a Package
        /// </summary>
        /// <param name="package"></param>
        /// <returns></returns>
        public static int GetContactWordAverage(this Package package)
        {
            int number = 0;
            int count = 0;

            foreach (var contact in package.Contacts)
            {
                number++;
                count += contact.CountWordsRelationship();
            }

            try
            {
                return count / number;
            }
            catch (Exception e)
            {
                //divided by zero, just return 0
                return 0;
            }
        }

        /// <summary>
        /// Returns true if a Task has attachments or document library links
        /// </summary>
        /// <param name="task"></param>
        /// <returns></returns>
        public static bool HasAttachments(this Task task)
        {
            bool hasAttachments = false;

            if (task.Attachments.Count > 0)
                hasAttachments = true;

            if (task.DocumentLibraries.Count > 0)
                hasAttachments = true;

            return hasAttachments;
        }

        /// <summary>
        /// Returns true if a Task has voice messages
        /// </summary>
        /// <param name="task"></param>
        /// <returns></returns>
        public static bool HasVoiceMessages(this Task task)
        {
            bool hasTasks = false;

            if (task.VoiceMessages.Count > 0)
                hasTasks = true;

            return hasTasks;
        }

        /// <summary>
        /// Returns true if a Topic has attachments or document library links
        /// </summary>
        /// <param name="topic"></param>
        /// <returns></returns>
        public static bool HasAttachments(this Topic topic)
        {
            bool hasAttachments = false;

            if (topic.Attachments.Count > 0)
                hasAttachments = true;

            if (topic.DocumentLibraries.Count > 0)
                hasAttachments = true;

            return hasAttachments;
        }

        /// <summary>
        /// Returns true if a Topic has voice messages
        /// </summary>
        /// <param name="topic"></param>
        /// <returns></returns>
        public static bool HasVoiceMessages(this Topic topic)
        {
            bool hasVoiceMessages = false;

            if (topic.VoiceMessages.Count > 0)
                hasVoiceMessages = true;

            return hasVoiceMessages;
        }

        /// <summary>
        /// Get the number of approved Topics and Tasks within a Module
        /// </summary>
        /// <param name="module"></param>
        /// <returns></returns>
        public static int CountItemsApproved(this Module module)
        {
            int number = 0;
            // for each 
            foreach (TopicGroup topicGroup in module.TopicGroups)
            {
                foreach (Topic topic in topicGroup.Topics)
                {
                    if (topic.IsApproved())
                    {
                        number++;
                    }
                    foreach (Task task in topic.Tasks)
                    {
                        if (task.IsApproved())
                        {
                            number++;
                        }
                    }

                }
            }
            return number;
        }

        /// <summary>
        /// Get the number of approved items in a Project Instance. Includes
        /// Tasks and the Project itself.
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        public static int CountItemsApproved(this ProjectInstance project)
        {
            int number = 0;
            // for each 
            foreach (var task in project.Tasks)
            {
                if (task.IsApproved())
                {
                    number++;
                }
            }

            if (project.IsApproved())
                number++;

            return number;
        }

        /// <summary>
        /// Get the number of completed Modules in a Package
        /// </summary>
        /// <param name="package"></param>
        /// <returns></returns>
        public static int CountModulesComplete(this Package package)
        {
            int countModules = (from m in package.Modules
                                where m.IsComplete()
                                select m).Count();

            if (package.Contacts.Count > 0)
            {
                var contactsAllApproved =
                    !package.Contacts.Any(c => !c.Moderations.Any(m => m.Decision == ModerationDecision.Approved));

                if (contactsAllApproved)
                    countModules++;
            }

            return countModules;
        }

        /// <summary>
        /// Get the percentage of complete Modules in a Package
        /// </summary>
        /// <param name="package"></param>
        /// <returns></returns>
        public static int CountModulesCompletePercent(this Package package)
        {
            return
                package.CountModulesComplete() * 100 / package.CountModules();
        }

        /// <summary>
        /// Get the total number of Modules within a Package
        /// </summary>
        /// <param name="package"></param>
        /// <returns></returns>
        public static int CountModules(this Package package)
        {
            int countModules =
            (from m in package.Modules
             where !m.IsFrameworkDefault
             select m).Count();

            if (package.Contacts.Count > 1)
                countModules++;

            return countModules;
        }

        #endregion

        #region Owner

        /// <summary>
        /// Check if a user is the owner of a particular entity
        /// </summary>
        /// <param name="entity">The entity inheriting from BaseModel</param>
        /// <param name="user">The User for which to check ownership</param>
        /// <param name="packageService">An IPackageService instance</param>
        /// <param name="includeOthers">If true will also check if the User is a manager or administrator</param>
        /// <returns></returns>
        public static bool IsOwner(this BaseModel entity, User user, IPackageService packageService, bool includeOthers = false)
        {
            Package package = null;

            if (entity is Package)
            {
                package = entity as Package;
            }

            if (entity is Module)
            {
                package = packageService.GetByModuleId(entity.ID);
            }

            if (entity is TopicGroup)
            {
                package = packageService.GetByTopicGroupId(entity.ID);
            }

            if (entity is Topic)
            {
                package = packageService.GetByTopicId(entity.ID);
            }

            if (entity is Task)
            {
                package = packageService.GetByTaskId(entity.ID);
            }

            if (entity is Contact)
            {
                package = packageService.GetByContactId(entity.ID);
            }

            if (entity is Meeting)
            {
                package = packageService.GetByMeetingId(entity.ID);
            }

            if (entity is ProjectInstance)
            {
                package = packageService.GetByProjectInstanceId(entity.ID);
            }

            if (entity is Request)
            {
                package = (entity as Request).Package;
            }

            if (entity is Staff)
            {
                package = packageService.GetByStaffId(entity.ID);
            }

            if (package != null)
            {
                if (includeOthers)
                {
                    if (user.Role.RoleParsed == Role.RoleEnum.Administrator)
                    {
                        return true;
                    }
                    else
                    {
                        return package.UserOwnerId == user.ID ||
                               package.UserLineManagerId == user.ID ||
                               package.UserHRManagerId == user.ID ||
                               (package.Handover != null && package.Handover.OnboardingPeriod.Package != null &&
                                package.Handover.OnboardingPeriod.Package.UserOwnerId == user.ID);
                    }
                }
                else
                {
                    return package.UserOwnerId == user.ID;
                }
            }

            return false;
        }

        #endregion

        #region helpers

        /// <summary>
        /// Deep clone an object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        private static T DeepClone<T>(T obj)
        {
            using (var ms = new System.IO.MemoryStream())
            {
                var formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                formatter.Serialize(ms, obj);
                ms.Position = 0;

                return (T)formatter.Deserialize(ms);
            }
        }

        /// <summary>
        /// Get a list of all the words in a string
        /// </summary>
        /// <param name="Text"></param>
        /// <returns></returns>
        private static IEnumerable<string> WordList(this string Text)
        {
            List<string> wordList =
                new List<string>();

            if (!string.IsNullOrEmpty(Text))
            {
                int cIndex = 0;
                int nIndex;
                while ((nIndex = Text.IndexOf(' ', cIndex + 1)) != -1)
                {
                    int sIndex = (cIndex == 0 ? 0 : cIndex + 1);
                    yield return Text.Substring(sIndex, nIndex - sIndex);
                    cIndex = nIndex;
                }
                yield return Text.Substring(cIndex + 1);
            }
            else
            {
                yield return null;
            }
        }

        /// <summary>
        /// Count the number of words in a string
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private static int CountWords(this string value)
        {
            int count = 0;

            if (!string.IsNullOrWhiteSpace(value))
            {
                MatchCollection collection =
                    Regex.Matches(value, @"[\S]+");

                count =
                    collection.Count;
            }

            return count;
        }

        /// <summary>
        /// Inspects a Task and returns a list of subjective words
        /// </summary>
        /// <param name="task">Task to inspect</param>
        /// <param name="MasterListService">MasterList service to access the subjectivity items</param>
        /// <returns>A list of subjective words present within the task</returns>
        public static List<string> InspectSubjectivity(this Task task, IMasterListService MasterListService)
        {
            IEnumerable<MasterList> keywords =
                GetKeywordsMasterList(MasterListService);
            List<string> SubjectiveWords = new List<string>();
            var text = String.Format("{0} {1} ", task.Notes, task.Handover);
            if (!string.IsNullOrEmpty(text))
            {
                foreach (string word in text.WordList())
                {
                    foreach (MasterList keyword in keywords)
                    {
                        if (word.ToLower().Contains(keyword.Name.ToLower()))
                        {
                            SubjectiveWords.Add(keyword.Name);
                        }
                    }
                }
            }
            return SubjectiveWords;
        }

        /// <summary>
        /// Inspects a Contact and returns a list of subjective words
        /// </summary>
        /// <param name="contact">Contact to inspect</param>
        /// <param name="MasterListService">MasterList service to access the subjectivity items</param>
        /// <returns>A list of subjective words present within the contact</returns>
        public static List<string> InspectSubjectivityPeople(this Contact contact, IMasterListService MasterListService)
        {
            IEnumerable<MasterList> keywords =
                GetKeywordsMasterList(MasterListService);
            List<string> SubjectiveWords = new List<string>();
            var text = String.Format("{0} {1} ", contact.FutureTraining, contact.ManagementNotes);
            if (!string.IsNullOrEmpty(text))
            {
                foreach (string word in text.WordList())
                {
                    foreach (MasterList keyword in keywords)
                    {
                        if (word.ToLower().Contains(keyword.Name.ToLower()))
                        {
                            SubjectiveWords.Add(keyword.Name);
                        }
                    }
                }
            }
            return SubjectiveWords;
        }

        /// <summary>
        /// Inspects a Releationship and returns a list of subjective words
        /// </summary>
        /// <param name="relationship">Relationship to inspect</param>
        /// <param name="MasterListService">MasterList service to access the subjectivity items</param>
        /// <returns>A list of subjective words present within the relationship</returns>
        public static List<string> InspectSubjectivityRelationship(this Contact relationship, IMasterListService MasterListService)
        {
            IEnumerable<MasterList> keywords =
                GetKeywordsMasterList(MasterListService);
            List<string> SubjectiveWords = new List<string>();
            var text = relationship.Notes;
            if (!string.IsNullOrEmpty(text))
            {
                foreach (string word in text.WordList())
                {
                    foreach (MasterList keyword in keywords)
                    {
                        if (word.ToLower().Contains(keyword.Name.ToLower()))
                        {
                            SubjectiveWords.Add(keyword.Name);
                        }
                    }
                }
            }
            return SubjectiveWords;
        }

        /// <summary>
        /// Inspects a Project and returns a list of subjective words
        /// </summary>
        /// <param name="project">Project to inspect</param>
        /// <param name="MasterListService">MasterList service to access the subjectivity items</param>
        /// <returns>A list of subjective words present within the project</returns>
        public static List<string> InspectSubjectivity(this ProjectInstance project, IMasterListService MasterListService)
        {
            IEnumerable<MasterList> keywords =
                GetKeywordsMasterList(MasterListService);
            List<string> SubjectiveWords = new List<string>();
            var text = project.Notes + " ";
            if (!string.IsNullOrEmpty(text))
            {
                foreach (string word in text.WordList())
                {
                    foreach (MasterList keyword in keywords)
                    {
                        if (word.ToLower().Contains(keyword.Name.ToLower()))
                        {
                            SubjectiveWords.Add(keyword.Name);
                        }
                    }
                }
            }
            return SubjectiveWords;
        }

        /// <summary>
        /// Inspects a Topic and returns a list of subjective words
        /// </summary>
        /// <param name="topic">Topic to inspect</param>
        /// <param name="MasterListService">MasterList service to access the subjectivity items</param>
        /// <returns>A list of subjective words present within the topic</returns>
        public static List<string> InspectSubjectivity(this Topic topic, IMasterListService MasterListService)
        {
            IEnumerable<MasterList> keywords =
                GetKeywordsMasterList(MasterListService);
            List<string> SubjectiveWords = new List<string>();

            var text = topic.Notes + " ";
            if (!string.IsNullOrEmpty(text))
            {
                foreach (string word in text.WordList())
                {
                    foreach (MasterList keyword in keywords)
                    {
                        if (word.ToLower().Contains(keyword.Name.ToLower()))
                        {
                            SubjectiveWords.Add(keyword.Name);
                        }
                    }
                }
            }

            if (topic.Fields != null)
            {
                foreach (TopicField topicField in topic.Fields)
                {
                    if (topicField.Question != null && topicField.Answers != null && topicField.Question.IsFreeText)
                    {
                        foreach (TopicAnswer answer in topicField.Answers)
                        {
                            text = answer.Text;
                            if (!string.IsNullOrWhiteSpace(text))
                            {
                                foreach (string word in text.WordList())
                                {
                                    foreach (MasterList keyword in keywords)
                                    {
                                        if (word.ToLower().Contains(keyword.Name.ToLower()))
                                        {
                                            SubjectiveWords.Add(keyword.Name);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return SubjectiveWords;
        }

        /// <summary>
        /// Get the subjectivity count for a string
        /// </summary>
        /// <param name="text">Test to inspect</param>
        /// <param name="keywordsMasterList">List of subjectivity keywords</param>
        /// <returns></returns>
        private static int GetSubjectivity(this string text, IEnumerable<MasterList> keywordsMasterList)
        {
            int subjectivity = 0;

            if (text != null)
            {
                foreach (string word in text.WordList())
                {
                    foreach (MasterList keyword in keywordsMasterList)
                    {
                        if (word.ToLower().Contains(keyword.Name.ToLower())) // only counts single match, todo
                        {
                            subjectivity++;
                        }

                    }
                }
            }

            return subjectivity;
        }

        /// <summary>
        /// Get all master list entries of type keyword
        /// </summary>
        /// <param name="MasterListService"></param>
        /// <returns></returns>
        private static IEnumerable<MasterList> GetKeywordsMasterList(IMasterListService MasterListService)
        {
            var masterLists =
                MasterListService.GetAll(MasterList.ListType.Keywords);

            return masterLists;
        }

        /// <summary>
        /// Distinct items
        /// </summary>
        /// <typeparam name="T">Return type/input</typeparam>
        /// <typeparam name="V">Out result type</typeparam>
        /// <param name="query">Query</param>
        /// <param name="f">function</param>
        /// <returns>Distinct items</returns>
        public static IEnumerable<T> MyDistinct<T, V>(this IEnumerable<T> query,
                                                      Func<T, V> f)
        {
            return query.GroupBy(f).Select(x => x.First());
        }

        /// <summary>
        /// Truncate a string to a given length and add an ellipsis if necessary
        /// </summary>
        /// <param name="value">String to truncate</param>
        /// <param name="maxChars">Chars to begin the truncate</param>
        /// <returns>Returns tuncated string</returns>
        public static string Truncate(this string value, int maxChars)
        {
            if (!string.IsNullOrEmpty(value))
            {
                return value.Length <= maxChars ? value : value.Substring(0, maxChars) + "...";
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Returns true if a given IEnumerable object is null or empty
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="enumerable">IEnumarable to inspect</param>
        /// <returns></returns>
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> enumerable)
        {
            return enumerable == null || !enumerable.Any();
        }

        #endregion

        #region string

        /// <summary>
        /// Convert a string to lowercase and remove spaces
        /// </summary>
        /// <param name="value">Value to convert</param>
        /// <returns>Returns lowercase string without spaces</returns>
        public static string ToLowerRemoveSpaces(this string value)
        {
            return value.ToLower().Replace(" ", string.Empty);
        }

        #endregion
    }
}
