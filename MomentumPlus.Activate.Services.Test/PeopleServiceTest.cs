﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MomentumPlus.Activate.Services.Interfaces;
using System.IO;
using System.Data.Entity;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Models;
using MomentumPlus.Core.Models.People;

namespace MomentumPlus.Activate.Services.Test
{
    [TestClass]
    public class PeopleServiceTest
    {
        [ClassInitialize()]
        public static void ClassInitialize(TestContext testContext)
        {
            AppDomain.CurrentDomain.SetData("DataDirectory", Path.Combine(AppDomain.CurrentDomain.BaseDirectory, string.Empty));
            Database.SetInitializer<TestMomentumContext>(new DropCreateDatabaseAlways<TestMomentumContext>());
        }

        [TestMethod]
        public void TestPeopleServiceSearchStaffs()
        {
            using (MomentumContext context = new TestMomentumContext())
            {

                IPeopleService peopleService = new PeopleService(context);

                Guid packageId = Guid.NewGuid();

                Package pack = new Package()
                {
                    Name = "TestPackage",
                    ID = packageId,
                    Position = new Position() { Name = "TestPosition" },
                    UserHRManager = new User() { Username = "TestUser@test.com", Password = "123" },
                    UserLineManager = new User() { Username = "TestUser2@test.com", Password = "123" },
                    UserOwner = new User() { Username = "TestUser3@test.com", Password = "123" },
                    Workplace = new Workplace() { Name = "TestWorkplace" },
                };

                Contact[] contacts = new Contact[] { 
                    new Contact() {
                        PackageId = packageId,
                        //Person = new Staff() 
                        //{
                        //   FirstName = "Test",
                        //   LastName = "Test",
                        //   Position = "Position"
                        //},                    
                    }
                };

                //uof.PackageRepository.Add(pack);
                //uof.ContactRepository.Add(contacts);
                //uof.Save();

                IEnumerable<Contact> staffs = peopleService.SearchStaffs(packageId, "Test", 0, 10, Person.SortColumn.FirstName);

                Assert.AreEqual(1, staffs.Count());

                staffs = peopleService.SearchStaffs(packageId, "Test", 0, 10, Person.SortColumn.Position);

                Assert.AreEqual(1, staffs.Count());

                staffs = peopleService.SearchStaffs(packageId, "Test", 0, 10, Person.SortColumn.Company);

                Assert.AreEqual(1, staffs.Count());
            }

        }

        [TestMethod]
        public void TestPeopleServiceGetStaffsByPackageId()
        {
            using (MomentumContext context = new TestMomentumContext())
            {


                IPeopleService peopleService = new PeopleService(context);

                Guid packageId = Guid.NewGuid();

                Package pack = new Package()
                {
                    Name = "TestPackage",
                    ID = packageId,
                    Position = new Position() { Name = "TestPosition" },
                    UserHRManager = new User() { Username = "TestUser@test.com", Password = "123" },
                    UserLineManager = new User() { Username = "TestUser2@test.com", Password = "123" },
                    UserOwner = new User() { Username = "TestUser3@test.com", Password = "123" },
                    Workplace = new Workplace() { Name = "TestWorkplace" },
                };

                Contact[] contacts = new Contact[] { 
                    new Contact() {
                        PackageId = packageId,
                        //Person = new Staff() 
                        //{
                        //   FirstName = "Test",
                        //   LastName = "Test",
                        //   Position = "Position"
                        //},                    
                    },
                    new Contact() {
                        PackageId = packageId,
                        //Person = new Person() 
                        //{
                        //   FirstName = "Test",
                        //   LastName = "Test",
                        //   Position = "Position"
                        //},                    
                    }
                };

                //uof.PackageRepository.Add(pack);
                //uof.ContactRepository.Add(contacts);
                //uof.Save();

                IEnumerable<Contact> staffs = peopleService.GetStaffsByPackageId(packageId, 0, 0, null);

                Assert.AreEqual(1, staffs.Count());
            }
        }
    }
}
